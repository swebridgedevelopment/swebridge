/*
 *  This File contains an example on how to use the SWE Bridge in a standalone
 *  application.
 *
 *  In order to use the SWE Bridge as a module in another software project it is possible
 *  to use it as a git submodule. For more info visit the example submodule project:
 *      https://bitbucket.org/swebridgedevelopment/submodule/
 *
 *  @author: Enoc Martínez
 *  @institution: Universitat Politècnica de Catalunya (UPC)
 *  @contact: enoc.martinez@upc.edu
 */

#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#include "swe_bridge.h"
}
#else
#include "swe_bridge.h"
#endif

#include <stdlib.h>
#if SWE_BRIDGE_STANDALONE


int main(int argc, char** argv) {
	if (swe_bridge_setup(argc, argv) != swe_ok) {
		errmsg("Couldn't setup SWE Brige");
		exit_platform();
	}
	swe_bridge_init();
	swe_bridge_start();
	swe_bridge_exit();
	exit_platform();
	return 0;
}

#endif // SWE_BRIDGE_STANDALONE //




