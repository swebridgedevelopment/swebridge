# SWE BRIDGE RELEASE NOTES

## Release v0.9.5

### Fixed Issues
- Fixing compatibility issues with SPL and JSON UDP sender and new OBSEA infrastructure

## Release v0.9.4

### New Featues
- Adding JSON UDP Sender, a module to send json-structured data trhough UDP sockets

### Fixed issues
- Fixed segmentation fault when flushing uninitialized SPL

### Changes
- Scheduler timeouts are calculated based on time rather than ticks
 
## Release v0.9.3

### New Featues
- Adding support for 24bit hydrophones
- Adding delayAfter / delayBefore to add arbitrary delays between processes
- Multiple inheritance from the same parent process

### Fixed issues
- Issue 155: Deep sleep now working also with 
- Issue 157: Now CSV files are not overwritten
- Issue 154: Valid timestamp is not checked anumore in insert_result module
- Issue 153: get timezeone through a platform wrapper
 
### Changes
- Removing wrappers for bytes_in_buff with all its dependencies

## Release v0.9.2

### New Featues
- Adding Makefile
- Adding scheduled measure: trigger process at a given time of day

### Fixed Issues
- Issue 150: CSV Generator millisecond timestamp not working
- Issue 152: implementing zabbix sender with binary encodings

## Release v0.9.1

### New Featues
- Minor fixes for compiling with raspberry pi
- Sound Pressure Level equations reformulated (same result, nicer math)
- launch_autotest.sh script fixed, now SWE Bridge binary has to be specified

## Release v0.9.0

### New Featues
- Support for hydrophones
- Real-time underwater noise measurement algorithms (SPL, SEL and RMS)
- Added Hydrophone Stream module
- Added Sound Pressure Level module
- Added WAV Generator module
- Added WAV Reader module
- Added CSV generator


## Release v0.8.7

### New Featues
- Issue 131: Optimizing memory in binary DataArrays
- Improved memory control

### Fixed Issues
- Issue 134: Wrong ascii size calculation
- Issue 133: Memory corruption in set_substrings
- Issue 132: Memory leak in swe_fopen

### Changes
- Major changes in ResponseStructure

## Release v0.8.6

### Fixed issues
-  Issue 125 : Setting scheduler trigger to true when starting scheduler


## Release v0.8.5

### New Features
- Issue 113: Execution flags set automatically (except init flag)
- Issue 116: Add junkEraser option to empty buffer dynamically

### Changes
- Issue 112: Connecting SchedulerProcesses before calling connectors 

### Fixed issues
- Issue 114: Fixed bug in expected response
- Issue 117: Fixed bug in ascii data type sizes
- Issue 118: Fixed bug in input command blockSeparator


## Release v0.8.4

### New Features
- Issue 109: Empty buffer before query option in instrument command module
- Initialization processes can include delays now
- Add test12 to test initialization with delays

### Changes
- Issue 110: Deep sleep min time configured with command-line parameter instead of a define
- Issue 111: Adding ADC 8-15 channels 
- Scheduler functions re-arranged to avoid code duplication

### Fixed issues
- Issue 108: Deep sleep execution flag bug solved
- Issue 107: Checking invalid init process chain connections


## Release v0.8.3

### Fixed issues
- Issue 106: Fixing wrong paths in raspberry pi and add signal handlers


## Release v0.8.2

### New Features
- Issue 105: Catching SIGINT, SIGQUIT and SIGTERM to exit gracefully (close all files and interfaces)

### Fixed issues
- Issue 104: Wrong return at set log level fixed 


## Release v0.8.1

### New Features
- Added notification events for output and temp folders
- Implemented NilValues structure to force the SWEBridge to ignore certain values
- Added 'ignore first' option to subsamling to ignore first n measurements for each cycle

### Fixed issues
- Issue 100: When an invalid GPS position is recieved instead of discarding the measure, latitude and longitude are set to NaN


## Release v0.8.0

### New Features
- system_notify wrapper no notify events to the system
- memory wrappers
- vprintf wrappers
- simple interface to control de SWE Bridge as a library (swe_bridge_start, swe_bridge_pause, etc.)
- Wrappers to get the SWE Bridge status at any time

### Changes
- reorganized project files inc and src folders not used anymore
- swebridge/resources/platforms folder moved to swebridge/platforms
- changed "typeof" to "_\_typeof_\_" for compiler compatibility

### Fixed issues
- Issue 98: Bug in burst measurement

