/*
 *  This File contains the implementation of a parser which is capable of dealing with
 *  ASCII binary and XML streams. It's the generic parser used in further processes in
 *  the SWE Bridge
 *
 *  @author: Enoc Martínez
 *  @institution: Universitat Politècnica de Catalunya (UPC)
 *  @contact: enoc.martinez@upc.edu
 */

#include "common/stream_parser.h"
#include "common/swe_data_model.h"
#include "common/swe_utils.h"
#include "core/simple_process.h"
#include "swe_conf.h"

#include <ctype.h>


#define DEBUG_THIS_MODULE OFF

#if DEBUG_THIS_MODULE
/*
 * Wrap debug message functions
 */
#define DBGM(...) dmsg(__VA_ARGS__) // DEBUG this module
#else
#define DBGM(...)
#endif



parser_retcode ascii_stream_parser(StreamBuffer* buff, ResponseStructure* structure);
parser_retcode binary_stream_parser(StreamBuffer* buff, ResponseStructure* structure);
parser_retcode xml_stream_parser(StreamBuffer* buff, ResponseStructure* structure);

/*
 * Response Structure generation
 */
ResponseStructure* response_structure_process_field(ResponseStructure* data, Field* infield, Encoding* encoding);
ResponseStructure* response_structure_process_data_stream(ResponseStructure* data, DataStream* ds);
ResponseStructure* response_structure_process_data_array(ResponseStructure* data, DataArray* array, Encoding* encoding);
ResponseStructure* response_structure_process_data_record(ResponseStructure* data, DataRecord* array, Encoding* encoding);
ResponseStructure* response_structure_process_basic_type(ResponseStructure* data, Field* field, Encoding* encoding);

// Basic type //
AsciiResponse* response_structure_process_basic_type_ascii(AsciiResponse* resp, Field* field, Encoding* encoding);
BinaryResponse* response_structure_process_basic_type_binary(BinaryResponse* resp, Field* field, Encoding* encoding, uint element_count);
XmlResponse* response_structure_process_basic_type_xml(XmlResponse* resp, Field* field, Encoding* encoding);

// Data Array //
AsciiResponse* response_structure_process_data_array_ascii(AsciiResponse* asciiresp, DataArray* da, Encoding* inencoding);
BinaryResponse* response_structure_process_data_array_binary(BinaryResponse* binresp, DataArray* da, Encoding* inencoding);
XmlResponse* response_structure_process_data_array_xml(XmlResponse* xmlresp, DataArray* da, Encoding* inencoding);


// ASCII Parser Functions //
int go_to_token(StreamBuffer* buff, char* token, int token_length, uchar allow_empty_fields);
int print_stream_buffer(StreamBuffer* buffer);
int copy_ascii_data(AsciiResponse* asciiresp, uint index, void* point, uint size);
int skip_leading_tokens(StreamBuffer* buff, AsciiResponse* structure);
int ascii_erase_junk(StreamBuffer* buff, const char* start_token, uint token_length);
int check_ascii_nilvalues(AsciiResponse* rs, uint index, void* point, uint size);
AsciiResponse* response_structure_add_start_token (AsciiResponse* data, Encoding* encoding);
AsciiResponse* response_structure_add_token_separator(AsciiResponse* data, Encoding* encoding);
AsciiResponse* response_structure_add_block_separator(AsciiResponse* data, Encoding* encoding);


// Binary Parser Functions //
int copy_binary_data(uchar* from, uchar* to, uint size, endianness formatfrom, endianness formatto);
int copy_binary_data_multiple(uchar* from, uchar* to, swe_data_encoding encoding, endianness efrom, endianness eto, uint element_count);

/*
 * XML parser functions
 */
int xml_parser_locate_tag_data(StreamBuffer* b, char** path, int* data_length);
int print_stream_buffer_xml(StreamBuffer* buffer);



/*
 * Parses the buffer according of what's inside the ResponseStructure
 */
parser_retcode stream_parser(StreamBuffer* buff, ResponseStructure* structure) {
	if (buff == NULL) {
		errmsg("stream_parser: Input buffer is NULL");
	}
	else if (buff == NULL) {
		errmsg("stream_parser: ResponseStructure is NULL");
	}

	if (structure->encoding == SWE_TextEncoding){
		return ascii_stream_parser(buff, structure);
	}
	else if (structure->encoding == SWE_BinaryEncoding) {
		return binary_stream_parser(buff, structure);
	}
	else if (structure->encoding == SWE_XMLEncoding) {
		return xml_stream_parser(buff, structure);
	}
	else {
		errmsg("Unkwnown ResponseStructrue encoding");
	}
	return unknown_parser_error;
}


//-----------------------------------------------------------------------------//
//--------------------------------- ASCII Parser-------------------------------//
//-----------------------------------------------------------------------------//

/*
 * Parses ASCII streams
 */
parser_retcode ascii_stream_parser(StreamBuffer* buff, ResponseStructure* generic_structure){
	// Step 1: Check if all the tokens are within the buffer
	int n=0; // structure elements index
	int ret;
	int length, token_length;
	char* start_token = NULL;
	char* current_token = NULL;
	AsciiResponse* structure = (AsciiResponse*)generic_structure->response;

	imsg("Parsing ASCII Stream (%d bytes in buffer)", buff->count);
	DBGM("Parser: Expecting %d elements", structure->count);
	print_stream_buffer(buff);


	int index_list[structure->count];

	// If the first element is a token, loop until it is found
	if ( structure->roles[0] == fixed_token ){
		start_token = (char*)structure->values[0];
		token_length = strlen(start_token);
		ret = go_to_token(buff, start_token, token_length, structure->allow_empty_fields);
		if ( ret == -1 ) {
			// If the token is not found, exit //
			dmsg("Start token not found, exit");
			if ( buff->erase_junk == TRUE  ) {
				if (ascii_erase_junk(buff, start_token, token_length) < 0 ) {
					return unknown_parser_error;
				}
			}
			buff->index = 0; // reset index
			return stream_not_complete;
		}
		else {
			dmsg("Start token found");
			index_list[0] = ret - token_length;
			n++;
		}
	}
	else if(structure->ignore_leading_tokens == TRUE){
		skip_leading_tokens(buff, structure);
	}
	while( n < structure->count ) {
		// If it's a token, look for it //
		if (structure->roles[n] == fixed_token) {
			current_token = (char*)structure->values[n];
			token_length = strlen(current_token);

			if ((ret = go_to_token(buff, current_token, token_length, structure->allow_empty_fields)) < 0) {
				// If the token is not found, exit //
				buff->index = 0; // reset index
				DBGM("Parser: Not all elements found (found %d of %d)!", n, structure->count);
				return stream_not_complete;
			}
			index_list[n] = buff->index - token_length;
		}
		// If it's a data element, store the position where it begins //
		else {
			index_list[n] = buff->index;
		}
		n++;
	}
	int i;

	for ( i = 0 ;  i < structure->count ; i++) {
		if (structure->roles[i] == enabled_data) {
			int old = buff->index;
			buff->index = index_list[i];
			buff->index = old;
		}
	}

	// Check out that we don't have duplicated positions //
	for ( n = 1 ;  n < structure->count ; n++) {
		if ( structure->allow_empty_fields == TRUE &&
			 (structure->roles[n] == disabled_data || structure->roles[n-1] == disabled_data) ) {
			// Ignore disabled data if allow_empty_fields is set! //
		}
		else {
			if (index_list[n] == index_list[n-1]) {
				errmsg("Error, field %d is empty!", n);
				return unknown_parser_error;
			}
		}
	}


	// Loop through the structure (except the last element)
	for ( n = 0; n < structure->count -1; n++) {
		// Look for data elements
		if (structure->roles[n] == enabled_data ) {

			length = index_list[n+1] - index_list[n];

			if (copy_ascii_data(structure, n, &buff->buffer[index_list[n]], length) != swe_ok ){
				warnmsg("Couldn't copy the data to the structure");
				return stream_with_errors;
			}

			// If the collapse white spaces option is active, apply it
			if (structure->collapse_white_spaces) {

				collapse_white_spaces((char*)structure->values[n], length);
			}

			if (structure->check_data_integrity){
				if (structure->encodings[n] == ascii_quantity_data && check_ascii_data_integrity(structure->values[n]) == FALSE){
					return stream_with_errors;
				}
			} else if (structure->encodings[n] == ascii_quantity_data) {
				// If check data integrity is not active, extract any number in the value //
				if (  check_string_for_ascii_quantity((char*)structure->values[n]) < 0){
					errmsg("ascii_quantity_data not passed! element %d returned %d", n);
					return stream_with_errors;
				}
			}
		}
	}
	// Process the last element //
	n = structure->count -1;
	if (structure->roles[n] == enabled_data || structure->roles[n] == disabled_data){
		// If last element is data (no end token), assume that everything left is the data
		length = buff->count - buff->index;
		if (structure->roles[n] == enabled_data) {

			if (copy_ascii_data(structure, n, &buff->buffer[index_list[n]], length) != swe_ok ){
				warnmsg("Couldn't copy the data to the structure");
				return stream_with_errors;
			}
			buff->index += length;
		}
	}
	return stream_successfully_parsed;
}

/*
 * Check if data is NilValues
 */
int check_ascii_nilvalues(AsciiResponse* rs, uint index, void* point, uint size){
	int i;
	if (rs->nilvalues != NULL) {
		NilValues* nilvalues = rs->nilvalues[index]; // Get the NilValues for the current element
		if ( nilvalues != NULL ) {
			for ( i=0 ; i<nilvalues->n ; i++) {
				char* current_nilvalue = (char*)nilvalues->values[i];
				char datavalue[256];
				memcpy(datavalue, point, size);
				datavalue[size] = 0;
				// If sizes do not match don't bother with strcmp
				if (size != strlen(current_nilvalue)) {
					continue;
				}
				if (!memcmp(nilvalues->values[i], datavalue, size)){
					warnmsg("NilValue found \"%s\"", datavalue);
					return swe_wrong_response;
				}
			}
		}
	}
	return swe_ok;
}



/*
 * Saves the data (pointed by point wize size) to the structure index. If
 */
int copy_ascii_data(AsciiResponse* asciiresp, uint index, void* point, uint size){
	uint element_length = asciiresp->element_sizes[index];
	swe_data_encoding encoding = asciiresp->encodings[index];
	if ( element_length == 0 ) {
		errmsg("slot_length is 0 in datastructure");
		return swe_invalid_arguments;
	}

	if (encoding == ascii_quantity_data || encoding == ascii_count_data || encoding == ascii_boolean_data || encoding == ascii_text_data ) {
		element_length--; // Keep the last element for a \0 (string)
	}

	if ( size >= element_length ) {
		errmsg("Data size (%d) exceeds element_length (%d)", size, element_length);
		return swe_invalid_arguments;
	}

	memcpy(asciiresp->values[index], point, size); // copy the data

	// Check against NilValues //
	TRY_RET(check_ascii_nilvalues(asciiresp, index, point, size));

	if (encoding == ascii_quantity_data || encoding == ascii_count_data || encoding == ascii_boolean_data || encoding == ascii_text_data ) {
		asciiresp->values[index][size] = 0; // Add a 0 at the end of the ASCII string
	}
	DBGM("PARSER: Data copied-> %s", asciiresp->values[index]);
	return swe_ok;
}


/*
 * Increases the buffer until the token is found.
 * If the token is found, the StreamBuffer is set to the position after and returns TRUE
 *     [lookformy-token] => [lookformy-token]
 *      ^                              ^
 * If it is not found, the buffer position remains untouched and returns FALSE
 *
 * If the flag allow_empty_fields is set to FALSE, all the tokens will be skipped
 */
int go_to_token(StreamBuffer* buff, char* token, int token_length, uchar allow_empty_fields) {
	uint n = 0;
	if (token_length > buff->count) {
		// If token is bigger than buffer return
		return -1;
	}

	while ((buff->index + n) <= (buff->count - token_length)) {
		// Check if there's a token at this position
		if (token_length == 1) { // single char token, no need for memcmp
			char c = (char)buff->buffer[buff->index + n];
			if ( c == token[0]){
				buff->index += n + token_length;

				if (allow_empty_fields == FALSE) {
					// If multiple tokens are together, skip them all!
					while ((char)buff->buffer[buff->index] == token[0] ) {
							if (buff->index >= buff->count) {
								break;
							}
						buff->index++;
					}
				}
				return buff->index;
			}
		}
		// Comparing tokens with more than 1 char
		else {
			uchar* b = &buff->buffer[buff->index + n];

			if (!memcmp(b, token, token_length)) {
				buff->index += n + token_length;


				if (allow_empty_fields == FALSE) {
					// If multiple tokens are together, skip them all!
					while ( buff->index + token_length <= buff->count  &&
							!memcmp(&buff->buffer[buff->index], token, token_length) ) {
						buff->index += token_length;
					}
				}
				return buff->index;
			}
		}
		n++;
	}
	return -1;
}


/*
 * This function skips the first token e.g.
 * "   0.12 45.54 0.12\r\n" ===> "0.12 45.54 0.12\r\n"
 */
int skip_leading_tokens(StreamBuffer* buff, AsciiResponse* structure){
	// Avoid leading token separators //
	char* token = NULL;
	int length;
	int k = 0;
	uchar found = FALSE;
	uchar skipped = FALSE;


	// Find the first token //
	while ( k < structure->count && found == FALSE) {
		if (structure->roles[k] == fixed_token) {
			token = (char*)structure->values[k];
			found = TRUE;
		}
		k++;
	}
	if ( found == FALSE ) {
		errmsg("Leading token not found!!");
		return swe_error;
	}
	length = strlen(token);
	// Skip buffer until a non-token is found //

	if (length == 1) {
		// Faster implementation for one-char tokens
		while ( buff->buffer[buff->index] == token[0] && // while the first char is the token
				buff->index < buff->count) { // and we are within the buffer data
			buff->index++;
			skipped = TRUE;
		}
	}
	else {
		while ( ((buff->count - buff->index) > length ) &&
				(!memcmp(token, &buff->buffer[buff->index], length))){
			buff->index += length;
			skipped = TRUE;
		}
	}
	if (skipped == TRUE){
		dmsg("Skipping leading tokens...");
	}
	return swe_ok;
}

/*
 * This function erases all the junk data in buffer before start token.
 */
int ascii_erase_junk(StreamBuffer* buff, const char* start_token, uint token_length) {
	// If the buffer is smaller than the token length do nothing
	if ( buff->count < token_length ) {
		return swe_ok;
	}

	// Keep some bytes to avoid discarding part of the start tokne //
	memmove(buff->buffer, &buff->buffer[buff->count - token_length], token_length);
	buff->count =token_length;
	return swe_ok;
}



//-----------------------------------------------------------------------------//
//--------------------------------- Binary Parser------------------------------//
//-----------------------------------------------------------------------------//

parser_retcode binary_stream_parser(StreamBuffer* buff, ResponseStructure* generic_structure){
	int i;
	BinaryResponse* structure = (BinaryResponse*)generic_structure->response;
	imsg("Parsing Binary Stream (%d bytes in buffer)", buff->count);
	if (buff->count == structure->length) {

		print_binary_array(buff->buffer, buff->count);

		for (i=0 ; i< structure->count ; i++) {
			uchar* buffpoint = &buff->buffer[buff->index];
			uchar* value = structure->values[i];
			uint size = structure->element_sizes[i];
			data_role role = structure->roles[i];
			uint element_count = structure->element_counts[i];
			swe_data_encoding encoding = structure->encodings[i];

			// If data is enabled, copy it to the structure //
			if( role == enabled_data) {
				// arranging values //
				if ( element_count == 1 ) {
					copy_binary_data(buffpoint, value, size, structure->stream_endianness, structure->platform_endianness);
				} else {
					copy_binary_data_multiple(buffpoint, value, encoding, structure->stream_endianness, structure->platform_endianness, element_count);
				}
			}
			// If data is disabled ignore it//
			else if (role == disabled_data) {
				dmsg("Ignoring disabled field");
			} else {
				errmsg("Unknown role");
				return unknown_parser_error;
			}
			buff->index += size;
		}
	}
	else if (buff->count < structure->length) {
		dmsg("Expected %d bytes in buffer, got %d", structure->length, buff->count);
		return stream_not_complete;
	}
	else {
		errmsg("Expected %d bytes, got %d. Too many bytes in buffer, restarting buffer", structure->length, buff->count);

		empty_buffer(buff);
		return stream_with_errors;
	}

	if ( buff->count > buff->index) {
		readjust_buffer(buff);
	}

	return stream_successfully_parsed;
}

/*
 * Copies data 'from' to 'to'. It takes into account from and to endianness, to be sure that the copied data is correct
 */
int copy_binary_data(uchar* from, uchar* to, uint size, endianness efrom, endianness eto){
	int i, n;
	// if both have the same endianness, use memcpy
	if (efrom == eto) {
		memcpy(to, from, size);
	}
	// to -> little endian, from -> big endian //
	else if ( eto == little_endian ) {
		n = size -1;
		i = 0;
		while ( n >= 0 ) {
			to[n] = from[i];
			i++;
			n--;
		}
	}
	// to -> little endian, from -> big endian //
	else {
		n = size -1;
		i = 0;
		while ( n >= 0 ) {
			to[i] = from[n];
			i++;
			n--;
		}
	}
	return swe_ok;
}

/*
 * This function copies multiples instances of the same data type to the response structure.
 * It is used in DataArrays
 */
int copy_binary_data_multiple(uchar* from, uchar* to, swe_data_encoding encoding, endianness efrom, endianness eto, uint element_count){
	int i;
	uint size = get_size_by_encoding(encoding);
	for ( i=0 ; i<element_count ; i++ ) {
		copy_binary_data(from, to, size, efrom, eto);
		from += size;
		to += size;
	}

	return swe_ok;
}

//-----------------------------------------------------------------------------//
//--------------------------------- XML Parser---------------------------------//
//-----------------------------------------------------------------------------//

/*
 * First tries to locate all the tags and stores their position
 * If all the tags are found, the value within the tags are extracted
 */
parser_retcode xml_stream_parser(StreamBuffer* buff, ResponseStructure* generic_structure){
	int i, ret;
	XmlResponse* structure = (XmlResponse*)generic_structure->response;
	int tags_positions[structure->count];
	int data_lengths[structure->count];
	imsg("Parsing XML Stream (%d bytes in buffer)", buff->count);
	print_stream_buffer_xml(buff);
	// Try to locate all the tags //
	for ( i=0; structure->tags[i] != NULL; i++ ) {
		char** path = structure->tags[i];
		ret = xml_parser_locate_tag_data(buff, path, &data_lengths[i]);
		if (ret < 0 ) {
			return stream_not_complete;
		}
		tags_positions[i] = ret; // Store the tag
	}
	// If all the tags where located, get their values //
	for ( i=0; structure->tags[i] != NULL; i++ ) {

		char* destination = (char*)structure->values[i];
		if (data_lengths[i] > structure->element_sizes[i]) {
			errmsg("Can't copy data! data has length %d but destination %d", data_lengths[i], structure->element_sizes[i]);
			return stream_with_errors;
		}
		memcpy(destination, &buff->buffer[tags_positions[i]], data_lengths[i]);
	}
	// if everything has been successfully parsed, discard the rest of the buffer
	cdmsg(CYN,"empty buffer...");
	empty_buffer(buff);
	return stream_successfully_parsed;
}


/*
 * Looks for a tag and returns the buffer position where the data starts:
 * <myTag>54532.324</myTag>
 *        ^
 * If the tag is not found a -1 is returned
 * The length of data is stored at data_length pointer
 */
int xml_parser_locate_tag_data(StreamBuffer* b, char** path, int* data_length){
	if (ARRAY_LENGTH(path) != 1 ) {
		errmsg("Unimplemented paths with more than one tag!");
		return -1;
	}
	char* tag = path[0];
	int i = b->index;
	int taglength = strlen(tag);
	int start_tag_position = 0;
	uchar start_tag = FALSE;

	// If the count is less than two times the taglentgh we are completely sure that
	// the stream is not complete
	if (b->count < 2*taglength) return -1;


	// Loop until the start of the tag is found //
	while ( (start_tag == FALSE ) && ( i < (b->count - taglength) )) {
		if ( (b->buffer[i] == '<') &&  (b->buffer[i + 1] != '/' )) {
			// Start any tag here //
			if( !memcmp(tag, &b->buffer[b->index + i + 1], taglength )) {
				// Start tag detected //
				start_tag = TRUE;
				i += taglength; // skip tag name

				// Skip current tag  (spaces and attributes)//
				while ( b->buffer[i++] != '>') {
					if (i >= b->count ) {
						return stream_not_complete; // End of buffer!
					}
				}
				start_tag_position = i; // store the position
				break; // Exit while
			}
		}
		i++; // Increment
	}
	// Loop until the end of the tag is found //
	if (start_tag == TRUE) {
		while ( i < (b->count - taglength ) ) {
			if ( (b->buffer[i] == '<') &&  (b->buffer[i + 1] == '/' )) {
				*data_length = i - start_tag_position;
				return start_tag_position;
			}
			i++;
		}
	}
	return -1;
}



/*
 * Shows the content of the buffer and its current position
 */
int print_stream_buffer(StreamBuffer* buffer){
	char temp[buffer->size +1];
	memset(temp, 0, buffer->size +1);




	dmsgn("Buffer (count %d) [", buffer->count);
	// Check if buffer is empty //

	if (buffer->index > 0) {
		memcpy(temp, buffer->buffer, buffer->index);
		fmt_dmsg(NRM, "%s", temp);
	}

	if (buffer->index < buffer->count ) {
		temp[0] = buffer->buffer[buffer->index];
		temp[1] = 0;
		if (temp[0] == ' ') temp[0] = '_';
		fmt_dmsg(GRN,"%s", temp);
	}


	if (buffer->count > buffer->index + 1) {
		memcpy(temp, &buffer->buffer[buffer->index+1], buffer->count - buffer->index);
		temp[buffer->count - buffer->index] = 0;
		fmt_dmsg(NRM, "%s", temp);
		dmsg("]");
	} else {
		dmsgn("]");
		dmsg("(index out of buffer)");
	}

	return swe_ok;
}

int print_stream_buffer_xml(StreamBuffer* buffer){
	char temp[buffer->size +1];
	memset(temp, 0, buffer->size +1);

	dmsg("Buffer (count %d)\n----------------------------------", buffer->count);
	// Check if buffer is empty //

	if (buffer->index > 0) {
		memcpy(temp, buffer->buffer, buffer->index);
		cdmsgn(NRM, "%s", temp);
	}

	if (buffer->index < buffer->count ) {
		temp[0] = buffer->buffer[buffer->index];
		temp[1] = 0;
		if (temp[0] == ' ') temp[0] = '_';
		cdmsgn(GRN,"%s", temp);
	}


	if (buffer->count > buffer->index + 1) {
		memcpy(temp, &buffer->buffer[buffer->index+1], buffer->count - buffer->index);
		temp[buffer->count - buffer->index] = 0;
		cdmsgn(NRM, "%s", temp);
		dmsg("\n----------------------------------");
	} else {
		dmsg("\n----------------------------------");
		dmsg("(index out of buffer)");
	}

	return swe_ok;
}


//-----------------------------------------------------------------------------//
//--------------------------------- Stream Buffer -----------------------------//
//-----------------------------------------------------------------------------//

/*
 * Stream buffer constructor
 */
StreamBuffer* stream_buffer_constructor(uint size, uchar erase_junk){
	StreamBuffer* b = swe_malloc(sizeof(StreamBuffer));
	b->buffer = swe_malloc(size*sizeof(uchar));
	b->size = size;
	b->erase_junk = erase_junk;
	return b;
}

/*
 * discard everything within the buf
 */
int empty_buffer(StreamBuffer* buff){
	memset(buff->buffer, 0, buff->size);
	buff->count = 0;
	buff->index = 0;
	return swe_ok;
}

/*
 * This function copies the unprocessed buffer bytes to the beginning,
 * eliminating the information that has already been processed.
 * Return the number of bytes still in buffer
 */
int readjust_buffer(StreamBuffer* buffer) {
	if (buffer->index < buffer->count) {
		memmove(buffer->buffer, &buffer->buffer[buffer->index], buffer->count - buffer->index);

		buffer->count -= buffer->index;
		buffer->index = 0;
		buffer->buffer [buffer->count] = 0;
		return buffer->count;
	}
	buffer->index = 0;
	buffer->count = 0;
	return 0;
}



/*
 * Fills the Instrument Command buffer with some data from the Instrument
 * returns the number of bytes read
 * 0 if there's no data to be read
 * < 0 error
 */
int fill_buffer(StreamBuffer* buffer, Interface* iface){

	int bytes_read, total_bytes_read = 0;
	uint bytes_to_read = buffer->size;
	uchar read = TRUE;
	// If there's some data in the buffer, move it to the beginning
	if (buffer->index != 0) {
		readjust_buffer(buffer);
	}
//	dmsg("buffer read default %d bytes", buffer->read_size);

	// Try to read until no bytes are left
	while (read == TRUE){
		// Get the number of bytes to read (if size - count is less than default read size, don't use ic_default_buffer_read_size to avoid overflow)
		if (bytes_to_read > (buffer->size - buffer->count)) bytes_to_read = buffer->size - buffer->count;
		dmsg("Reading %d bytes", bytes_to_read);
		uchar* buff_point  = &buffer->buffer[buffer->count];
		bytes_read = iface_recv(iface, buff_point, bytes_to_read,  0);
		if (bytes_read < 0 ){
			errmsg("Error reading from interface in instrument command, returned %d", bytes_read);
			return swe_iface_error;
		}
		buffer->count += bytes_read; // advance the count
		total_bytes_read += bytes_read;

		if (buffer->count < buffer->size) {
			buffer->buffer[buffer->count] = 0; // Add a 0 at the end
		}

		if (bytes_read < bytes_to_read) {
			read = FALSE; // No more bytes to be read

		} else if (buffer->size == buffer->count) {
			read = FALSE; //The buffer is full

		}
	}
	return total_bytes_read; // return the number of bytes read
}




//------------------------------------------------------------------------------//
//---------------------- Response Structure Generation -------------------------//
//------------------------------------------------------------------------------//

/*
 * If this flag is set, the DataRecord Structures will be understood as data blocks, so
 * a block separator is going to be added at the end of each DataRecord containing DataComponents
 * (Quantity, Text, Count, etc.)
 *
 * This behavior is not described in the standard but it has been detected
 */
static uchar data_record_as_block = TRUE;

const char* response_structure_names[17] = {
	"unknown_data  ",

	// ASCII //
	"ascii_quantity",
	"ascii_count   ",
	"ascii_boolean ",
	"ascii_text    ",

	// Binary //
	"signed_byte   ",
	"unsigned_byte ",
	"signed_short  ",
	"signed_int    ",
	"unsigned_int  ",
	"signed_long   ",
	"unsigned_long ",
	"half_precision",
	"float         ",
	"double        ",
	"long_double   ",
	"string        ", // ? equivalent to ascii_data
};


/*
 * Generate SWE Response Structure from a DataStream Field
 */
ResponseStructure* response_structure_constructor(Field* root, uchar data_block_flag) {
	int i;
	if ( root == NULL ) {
		return NULL;
	}
	else if (root->type != SWE_DataStream) {
		errmsg("Expected DataStream in function %s",  __func__);
		return NULL;
	}
	set_memory_table(MEM_SCHEDULER);

	data_record_as_block = data_block_flag; // set the data_block_flag

	ResponseStructure *response = swe_malloc(sizeof(ResponseStructure));

	response = response_structure_process_field(response, root, NULL);
	if (response == NULL) {
		return NULL;
	}

	// Count the number of tokens in the structure
	if ( response->encoding == SWE_TextEncoding) {
		AsciiResponse* asciiresp = (AsciiResponse*)response->response;
		asciiresp->ntokens = 0;
		for (i=0; i < asciiresp->count; i++){
			if (asciiresp->roles[i] == fixed_token) asciiresp->ntokens++;
		}
		// Change the DUMMY_POINTER_ADDRESS used during constructor to NULL
		for (i=0 ; i < asciiresp->count ; i++ ){
			if (asciiresp->values[i] == DUMMY_POINTER_ADDRESS) {
				asciiresp->values[i] = NULL;
			}
			if (asciiresp->configuration_fields[i] == DUMMY_POINTER_ADDRESS) {
				asciiresp->configuration_fields[i] = NULL;
			}
			if (asciiresp->nilvalues[i] == DUMMY_POINTER_ADDRESS) {
				asciiresp->nilvalues[i] = NULL;
			}
		}
	}

	// If the response is binary, the size should be fixed //
	else if ( response->encoding == SWE_BinaryEncoding) {
		BinaryResponse* binresp = (BinaryResponse*)response->response;

		// Change the DUMMY_POINTER_ADDRESS used during constructor to NULL
		for (i=0 ; i < binresp->count ; i++ ){
			if (binresp->values[i] == DUMMY_POINTER_ADDRESS) {
				binresp->values[i] = NULL;
			}
			if (binresp->configuration_fields[i] == DUMMY_POINTER_ADDRESS) {
				binresp->configuration_fields[i] = NULL;
			}
		}
	}
	else if ( response->encoding == SWE_XMLEncoding) {
		XmlResponse* xmlresp = (XmlResponse*)response->response;
		// Change the DUMMY_POINTER_ADDRESS used during constructor to NULL
		for (i=0 ; i < xmlresp->count ; i++ ){
			if (xmlresp->values[i] == DUMMY_POINTER_ADDRESS) {
				xmlresp->values[i] = NULL;
			}
			if (xmlresp->configuration_fields[i] == DUMMY_POINTER_ADDRESS) {
				xmlresp->configuration_fields[i] = NULL;
			}
		}
	}
	return response;
}


/*
 * Takes a SWE Data Field, processes it and attaches the result to the
 * SWE Data structure
 */
ResponseStructure* response_structure_process_field(ResponseStructure* data, Field* infield, Encoding* encoding) {
	if (infield == NULL || data == NULL) {
		return NULL;
	}

	if (infield->type == SWE_DataStream) {
		data = CATCH_NULL(response_structure_process_data_stream(data, (DataStream*)infield->component));
	}
	else if (infield->type == SWE_DataRecord) {
		data = CATCH_NULL( response_structure_process_data_record(data, (DataRecord*)infield->component, encoding));
	}
	else if (infield->type == SWE_DataArray) {
		data = CATCH_NULL( response_structure_process_data_array(data, (DataArray*)infield->component, encoding));
	}
	else if (check_if_basic(infield->type) == TRUE) {
		data = CATCH_NULL(response_structure_process_basic_type (data, infield, encoding));
	}
	else {
		errmsg("Unknown type!!");
		return NULL;
	}
	return data;
}




/*
 * Process DataStream, the parent element of the overall response structure.
 * Here the encoding type is determined and the memory is allocated for each response type
 */
ResponseStructure* response_structure_process_data_stream(ResponseStructure* response, DataStream* ds){
	// check start token //
	if (ds->encoding == NULL) {
		errmsg("Encoding is NULL!");
		return NULL;
	}
	// Assign the encoding type //
	response->encoding = ds->encoding->type;

	if ( ds->encoding->type == SWE_TextEncoding ) {
		// Allocate memory //
		AsciiResponse* asciiresp = swe_malloc(sizeof(AsciiResponse));
		response->response = (void*)asciiresp;
		asciiresp->parent = response; // Assign the parent structure pointer
	}

	// Check if it is BinaryEncoding  or TextEncoding //
	else if (ds->encoding->type == SWE_BinaryEncoding) {
		BinaryEncoding* be = (BinaryEncoding*) ds->encoding->encoding;
		BinaryResponse* binresp = swe_malloc(sizeof(BinaryResponse));
		response->response = (void*)binresp;
		binresp->stream_endianness = be->byteOrder;
		// Get platform endianess //
		binresp->platform_endianness = get_platform_endianness();

		binresp->parent = response; // Assign the parent structure pointer
		dmsg("BinaryEncoding byte order is %s", endianness_text(be->byteOrder));
	}
	else if (ds->encoding->type == SWE_XMLEncoding) {
		// Allocate memory //
		XmlResponse* xmlresp = swe_malloc(sizeof(XmlResponse));
		response->response = (void*)xmlresp;
		xmlresp->parent = response; // Assign the parent structure pointer
	}


	response = CATCH_NULL(response_structure_process_field(response, ds->field, ds->encoding));


	if (ds->encoding->type == SWE_TextEncoding )  {
		AsciiResponse * asciiresp = (AsciiResponse*)response->response;
		if (asciiresp) {
			// If the previous element isn't a fixed token add block separator
			if (asciiresp->roles[asciiresp->count-1] != fixed_token) {
				asciiresp = CATCH_NULL(response_structure_add_block_separator(asciiresp, ds->encoding));
			}
		}
	}
	return response;
}


/*
 * Process data record
 *
 * Block separator is only added at the end of the DataRecord if
 */
ResponseStructure* response_structure_process_data_record(ResponseStructure* generic_response, DataRecord* dr, Encoding* encoding){
	int i, len;
	if (generic_response == NULL || dr == NULL ) return NULL;
	if (dr->fields == NULL ) return NULL;

	len = ARRAY_LENGTH(dr->fields);

	if (encoding->type == SWE_TextEncoding ) {
		AsciiResponse* asciiresp = (AsciiResponse*)generic_response->response;

		// Check if a Block Separator is needed before adding new fields//
		if (asciiresp->count > 0) {
			// If the previous element isn't a fixed token add block separator
			if (asciiresp->roles[asciiresp->count-1] != fixed_token) {
				if ( data_record_as_block == TRUE) {
					// If data record is understood as block
					asciiresp = CATCH_NULL(response_structure_add_block_separator(asciiresp, encoding));
				} else {
					// formal usage
					asciiresp = CATCH_NULL(response_structure_add_token_separator(asciiresp, encoding));
				}
			}
		}
		// Add start token if TextEncoding //
		asciiresp = CATCH_NULL(response_structure_add_start_token(asciiresp, encoding)); // Adding start token
	}

	// Loop through the DataRecord Fields
	for ( i=0 ; i < len ; i++ ){
		generic_response = CATCH_NULL(response_structure_process_field(generic_response, dr->fields[i], encoding));

		// If TextEncoding add token separator //
		if ( (encoding->type == SWE_TextEncoding) && (check_if_basic(dr->fields[i]->type)) == TRUE && (i < (len-1)) ) {
			AsciiResponse* asciiresp = (AsciiResponse*)generic_response->response;
			asciiresp = CATCH_NULL(response_structure_add_token_separator(asciiresp, encoding));
		}
	}
	// Add the block separator
	//data = CATCH_NULL(response_structure_add_block_separator(data, encoding));
	return generic_response;
}


/*
 * Process an array
 */
ResponseStructure* response_structure_process_data_array(ResponseStructure* resp, DataArray* da, Encoding* inencoding){
	if ( resp->encoding == SWE_TextEncoding ) {
		CATCH_NULL(response_structure_process_data_array_ascii((AsciiResponse*)resp->response, da, inencoding));
	} else if ( resp->encoding == SWE_BinaryEncoding ) {
		CATCH_NULL(response_structure_process_data_array_binary((BinaryResponse*)resp->response, da, inencoding));

	} else if ( resp->encoding == SWE_XMLEncoding ) {
		CATCH_NULL(response_structure_process_data_array_xml((XmlResponse*)resp->response, da, inencoding));
	}
	return resp;
}

/*
 * Process an ascii DataArray
 */
AsciiResponse* response_structure_process_data_array_ascii(AsciiResponse* asciiresp, DataArray* da, Encoding* inencoding){
	int i, j, array_count;
	Encoding* encoding = inencoding;

	if (da->fields == NULL ) return NULL;

	// Check if a Block Separator is needed before adding new fields //

	if (asciiresp->count > 0) {

		// If the previous element isn't a fixed token add block separator
		if (asciiresp->roles[asciiresp->count-1] != fixed_token) {

			if ( data_record_as_block == TRUE) {
				// If data record is understood as block
				asciiresp = CATCH_NULL(response_structure_add_block_separator(asciiresp, encoding));
			}
			else {
				asciiresp = CATCH_NULL(response_structure_add_token_separator(asciiresp, encoding));
			}
		}
	}

	if (da->encoding != NULL ){
		// If there's a different encoding in DataArray, use it instead of the DataStream encoding
		encoding = da->encoding;
	}

	TRY_RET_NULL(field_count_value_get(da->arrayCount, &array_count));
	for (j = 0 ; j < array_count ; j++ ) { // For every element in the array
		// Loop through the DataRecord Fields
		for ( i=0 ; da->fields[i]!=NULL ; i++ ){
			asciiresp->parent = CATCH_NULL(response_structure_process_field(asciiresp->parent, da->fields[i], encoding));
		}
	}
	return asciiresp;
}

/*
 * Process a binary DataArray.
 */
BinaryResponse* response_structure_process_data_array_binary(BinaryResponse* binresp, DataArray* da, Encoding* inencoding){
	int i, j, array_count, nfields;
	Encoding* encoding = inencoding;

	if (da->fields == NULL ) return NULL;
	nfields = ARRAY_LENGTH(da->fields);
	// Check if a Block Separator is needed before adding new fields //

	if (da->encoding != NULL ){
		// If there's a different encoding in DataArray, use it instead of the DataStream encoding
		encoding = da->encoding;
	}
	TRY_RET_NULL(field_count_value_get(da->arrayCount, &array_count));

	// If we only have a BasicType as the array content, use the element_count parameter
	if ( nfields == 1 ) {
		Field* field = da->fields[0];
		if (check_if_basic(field->type)) {
			binresp = CATCH_NULL(response_structure_process_basic_type_binary(binresp, da->fields[0], encoding, array_count));
			return binresp;
		}

		// If it's a DataRecord with only 1 BasicType use the same strategy
		if (field->type == SWE_DataRecord){
			DataRecord* dr = (DataRecord*)field->component;
			if (ARRAY_LENGTH(dr->fields) == 1 ) {
				warnmsg("DataRecord with only 1 field! This structure is valid but redundant");
				binresp = CATCH_NULL(response_structure_process_basic_type_binary(binresp, dr->fields[0], encoding, array_count));
				return binresp;
			}
		}
	}

	// If we have multiple fields in the array or the field is not a basic type concatenate
	// them individually generating a new entry in the response structure for each one of them
	for (j = 0 ; j < array_count ; j++ ) { // For every element in the array
		// If we have
		for ( i=0 ; da->fields[i]!=NULL ; i++ ){
			binresp->parent = CATCH_NULL(response_structure_process_field(binresp->parent, da->fields[i], encoding));
		}
	}
	return binresp;
}

/*
 * Process an XML Data array
 */
XmlResponse* response_structure_process_data_array_xml(XmlResponse* xmlresp, DataArray* da, Encoding* inencoding){
	errmsg("Unimplemented XML Data Array");
	return NULL;
}




/*
 * This function calls the process basic type function for each encoding type
 */
ResponseStructure* response_structure_process_basic_type(ResponseStructure* generic_structure, Field* field, Encoding* encoding) {
	if (encoding->type == SWE_TextEncoding) {
		AsciiResponse* asciiresp = (AsciiResponse*)generic_structure->response;
		CATCH_NULL(response_structure_process_basic_type_ascii(asciiresp, field, encoding));
		return generic_structure;
	}
	else if (encoding->type == SWE_BinaryEncoding) {
		BinaryResponse* binresp = (BinaryResponse*)generic_structure->response;
		CATCH_NULL(response_structure_process_basic_type_binary(binresp, field, encoding, 1));
		return generic_structure;
	}
	else if (encoding->type == SWE_XMLEncoding) {
		XmlResponse* xmlresp = (XmlResponse*)generic_structure->response;
		CATCH_NULL(response_structure_process_basic_type_xml(xmlresp, field, encoding));
		return generic_structure;
	} else {
		errmsg("Unknown encoding");
	}
	return NULL;
}


/*
 * Process an BasicType (text, count, quantity, etc.) in ASCII encoding and attaches it to the AsciiResponse structure
 */
AsciiResponse* response_structure_process_basic_type_ascii(AsciiResponse* resp, Field* field, Encoding* encoding){
	swe_component_type type = field->type;
	int newsize = 0; // size of the new SWE Data element
	uchar* newvalue = NULL; // new swe data element
	swe_data_encoding newtype;
	data_role newrole = enabled_data;
	NilValues* nilvalues = (NilValues*)DUMMY_POINTER_ADDRESS;


	if ( field->static_field == TRUE ) {
		char temp[256];
		TRY_RET_NULL(field_component_value_get_ascii(field, temp, 256));
		newtype = ascii_text_data;
		newrole = fixed_token;
		newvalue = (uchar*)set_string(temp);
		newsize = strlen(temp);
	}
	else {

		newsize = 0;
		switch (type) {
			case SWE_Quantity:
				newtype = ascii_quantity_data;
				break;
			case SWE_Count:
				newtype = ascii_count_data;
				break;
			case SWE_Boolean:
				newtype = ascii_boolean_data;
				break;
			case SWE_Text:
				newtype = ascii_text_data;
				break;
			case SWE_Time:
				warnmsg("Unimplemented SWE Time type, assuming text");
				newtype = ascii_text_data;
				break;
			default:
				return NULL;
				break;
		}
		if (field->status == ENABLED) {
			// Size and value for enabled data will be set when creating SchedulerData structure!
			newrole = enabled_data;
			newsize = get_size_by_encoding(newtype);
			newvalue = swe_malloc(newsize);

			// If enabled check set the NilValues
			char** field_nilvalues = field_component_nilvalues_get(field);
			if (field_nilvalues != NULL) {
				int i;
				// If not empty, fill the NilValues
				nilvalues = swe_malloc(sizeof(NilValues));
				nilvalues->n = ARRAY_LENGTH(field_nilvalues);
				nilvalues->values = swe_malloc(sizeof(uchar*) * nilvalues->n);
				for ( i=0; i<nilvalues->n ; i++ ){
					char* value = set_string(field_nilvalues[i]);
					nilvalues->values[i] = (uchar*) value;
				}

			} else {
				// Otherwise leave it NULL
				nilvalues = (void*)DUMMY_POINTER_ADDRESS;
			}

		} else {
			// If the field is disabled do not allocate memory
			newrole = disabled_data;
			// Assign a non-NULL dummy pointer to new value. It can't be null because the
			// data->values is a NULL-terminated array and will disrupt the rest of the
			// cod (i.e. ATTACH_TO_ARRAY macros)
			newvalue = (void*)DUMMY_POINTER_ADDRESS;
		}
	}



	resp->values = ATTACH_TO_ARRAY(resp->values, newvalue);
	resp->roles = ATTACH_TO_LIST(resp->roles, newrole, resp->count);
	resp->encodings = ATTACH_TO_LIST(resp->encodings, newtype, resp->count);
	resp->element_sizes = ATTACH_TO_LIST(resp->element_sizes, newsize, resp->count);
	resp->element_counts = ATTACH_TO_LIST(resp->element_counts, 1, resp->count);
	resp->configuration_fields = ATTACH_TO_ARRAY(resp->configuration_fields, field);
	resp->nilvalues = ATTACH_TO_ARRAY(resp->nilvalues, nilvalues);
	resp->length +=  newsize;
	resp->count++;


	return resp;
}


/*
 * Processes a Basic Type (Quantity, Count, Text or Boolean) in binary mode.
 * If element_count is greater than 1 it means that we have several consecutive elements of the same type
 */
BinaryResponse* response_structure_process_basic_type_binary(BinaryResponse* resp, Field* field, Encoding* encoding, uint element_count){
	int newsize = 0; // size of the new SWE Data element
	uchar* newvalue = NULL; // new swe data element
	swe_data_encoding newtype;
	data_role newrole = enabled_data;

	if (field->status == ENABLED) {
		newrole = enabled_data;
	} else {
		newrole = disabled_data;
	}

	dmsgn("Searching encoding for field \"%s\"...", field->name);
	BinaryEncoding* be = (BinaryEncoding*)encoding->encoding;
	BinaryEncodingComponent* c = get_binary_encoding_component_by_field(field, be);
	if ( c == NULL ) {
		errmsg("Encoding not found!");
		return NULL;
	}
	newtype = c->type;

	dmsg("found, type: \"%s\" (%d)", swe_data_encoding_text(newtype),(int)newtype);

	// Get the element size
	if ((newsize = get_size_by_encoding(newtype)) < 1 ){
		errmsg("Encoding not found");
		return NULL;
	}
	newsize = newsize * element_count;
	newvalue = swe_malloc(newsize); // allocate memory

	resp->values = ATTACH_TO_ARRAY(resp->values, newvalue);
	resp->roles = ATTACH_TO_LIST(resp->roles, newrole, resp->count);
	resp->encodings = ATTACH_TO_LIST(resp->encodings, newtype, resp->count);
	resp->element_sizes = ATTACH_TO_LIST(resp->element_sizes, newsize, resp->count);
	resp->element_counts = ATTACH_TO_LIST(resp->element_counts, element_count, resp->count);
	resp->configuration_fields = ATTACH_TO_ARRAY(resp->configuration_fields, field);
	resp->length +=  newsize;
	resp->count++;
	return resp;
}



/*
 * Process a XML Basic type
 */
XmlResponse* response_structure_process_basic_type_xml(XmlResponse* resp, Field* field, Encoding* encoding){
	swe_component_type type = field->type;
	int newsize = 0; // size of the new SWE Data element
	uchar* newvalue = NULL; // new swe data element
	swe_data_encoding newtype;
	data_role newrole = enabled_data;
	XMLEncoding* xe = (XMLEncoding*)encoding->encoding;

	// Use the ASCII  encodings to determine data slot size //
	if (field->status != ENABLED) {
		// ignoring field
		return resp;
	}
	// Find the encoding
	XMLEncodingComponent* c = get_xml_encoding_component_by_field(field, xe);
	if (c->dataType == NULL) {
		errmsg("xml dataType for field %s is NULL", field->name);
		return NULL;
	}
	switch (type) {
		case SWE_Quantity:
			newtype = ascii_quantity_data;
			break;
		case SWE_Count:
			newtype = ascii_count_data;
			break;
		case SWE_Boolean:
			newtype = ascii_boolean_data;
			break;
		case SWE_Text:
			newtype = ascii_text_data;
			break;
		case SWE_Time:
			warnmsg("Unimplemented SWE Time type, assuming text");
			newtype = ascii_text_data;
			break;
		default:
			return NULL;
			break;
	}
	if ((newsize = get_size_by_encoding(newtype)) < 1 ){
		errmsg("Encoding not found");
		return NULL;
	}
	// Attach the tag to the tag array //
	if (xe->components) {
		// Split paths //
		char** path = set_substrings(c->dataType, "/");
		resp->tags = ATTACH_TO_ARRAY(resp->tags, path);
	}


	newrole = enabled_data;
	newvalue = swe_malloc(sizeof(newsize));


	resp->values = ATTACH_TO_ARRAY(resp->values, newvalue);
	resp->roles = ATTACH_TO_LIST(resp->roles, newrole, resp->count);
	resp->encodings = ATTACH_TO_LIST(resp->encodings, newtype, resp->count);
	resp->element_sizes = ATTACH_TO_LIST(resp->element_sizes, newsize, resp->count);
	resp->element_counts = ATTACH_TO_LIST(resp->element_counts, 1, resp->count);
	resp->configuration_fields = ATTACH_TO_ARRAY(resp->configuration_fields, field);
	resp->length +=  newsize;
	resp->count++;
	return resp;
}


/*
 * Add Start Token
 */
AsciiResponse* response_structure_add_start_token (AsciiResponse* asciiresp, Encoding* encoding){
	TextEncoding *te= (TextEncoding*) encoding->encoding;

	if (te->startToken != NULL) {
		void* newvalue = set_string(te->startToken); // new swe data element
		asciiresp->values = ATTACH_TO_ARRAY(asciiresp->values, newvalue);
		asciiresp->roles = ATTACH_TO_LIST(asciiresp->roles, fixed_token, asciiresp->count);
		asciiresp->encodings = ATTACH_TO_LIST(asciiresp->encodings, ascii_text_data, asciiresp->count);
		asciiresp->element_sizes = ATTACH_TO_LIST(asciiresp->element_sizes, strlen(newvalue), asciiresp->count);
		// add a dummy pointer to mark the configuration field as not valid
		asciiresp->nilvalues = ATTACH_TO_ARRAY(asciiresp->nilvalues, (NilValues*)DUMMY_POINTER_ADDRESS);
		asciiresp->configuration_fields = ATTACH_TO_ARRAY(asciiresp->configuration_fields, (Field*)DUMMY_POINTER_ADDRESS);
		asciiresp->count++;

	}
	return asciiresp;
}


/*
 * Adds a token separator
 */
AsciiResponse* response_structure_add_token_separator(AsciiResponse* asciiresp, Encoding* encoding){
	TextEncoding *te = (TextEncoding*)encoding->encoding;

	if (te->tokenSeparator != NULL) {
		void* newvalue = set_string(te->tokenSeparator); // new swe data element
		asciiresp->values = ATTACH_TO_ARRAY(asciiresp->values, newvalue);
		asciiresp->roles = ATTACH_TO_LIST(asciiresp->roles, fixed_token, asciiresp->count);
		asciiresp->encodings = ATTACH_TO_LIST(asciiresp->encodings, ascii_text_data, asciiresp->count);
		asciiresp->element_sizes = ATTACH_TO_LIST(asciiresp->element_sizes, strlen(newvalue), asciiresp->count);
		asciiresp->nilvalues = ATTACH_TO_ARRAY(asciiresp->nilvalues, (NilValues*)DUMMY_POINTER_ADDRESS);
		// add a dummy pointer to mark the configuration field as not valid
		asciiresp->configuration_fields = ATTACH_TO_ARRAY(asciiresp->configuration_fields, (Field*)DUMMY_POINTER_ADDRESS);

		asciiresp->count++;

		// If token separator is whitesapce (' ', '\r' '\n') set the collapse_leading_token to true
		if (strlen(te->tokenSeparator) == 1  && isspace(te->tokenSeparator[0])) {
			dmsg("Ignoring leading token separator");
			asciiresp->ignore_leading_tokens = TRUE;
		} else { // If token separator is not whitespace, allow empty fields
			asciiresp->allow_empty_fields = TRUE;
		}
	}
	return asciiresp;
}


/*
 * Add BlockSeparator
 */
AsciiResponse* response_structure_add_block_separator(AsciiResponse* asciiresp, Encoding* encoding){
	TextEncoding *te = (TextEncoding*) encoding->encoding;

	if (te->blockSeparator != NULL) {
		void* newvalue = set_string(te->blockSeparator); // new swe data element
		asciiresp->values = ATTACH_TO_ARRAY(asciiresp->values, newvalue);
		asciiresp->roles = ATTACH_TO_LIST(asciiresp->roles, fixed_token, asciiresp->count);
		asciiresp->encodings = ATTACH_TO_LIST(asciiresp->encodings, ascii_text_data, asciiresp->count);
		asciiresp->element_sizes = ATTACH_TO_LIST(asciiresp->element_sizes, strlen(newvalue), asciiresp->count);
		asciiresp->nilvalues = ATTACH_TO_ARRAY(asciiresp->nilvalues, (NilValues*)DUMMY_POINTER_ADDRESS);

		// add a dummy pointer to mark the configuration field as not valid
		asciiresp->configuration_fields = ATTACH_TO_ARRAY(asciiresp->configuration_fields, (Field*)DUMMY_POINTER_ADDRESS);
		asciiresp->count++;

	}
	return asciiresp;
}


//-------------------------SHOW RESPONSE STRUCTURE -------------------------------//

static const char* data_role_name[3] = {
		"enabled_data  ", // data stored within tokens (variable length)
		"disabled_data ", // ascii data which doesn't have to be stored,
		"fixed_token   " // fixed ascii token that should match with values (i.e. a token separator or a block separator)
};

#define ROLE_NAME(type) data_role_name[(int)type]


/*
 * Prinths the response structure
 */
int print_response_structure(ResponseStructure* response){
	int i, count;
	char temp[100];

	dmsg("\n------------------------------------------------------------");
	dmsg("-------------------Response Stucture------------------------");
	dmsg("------------------------------------------------------------");
	dmsg("Num | Type                | Role          | Values");
	dmsg("------------------------------------------------------------");


	if ( response->encoding == SWE_TextEncoding ) {
		AsciiResponse* resp = (AsciiResponse*)response->response;
		for ( i=0 ; i < resp->count ; i++ ){
			count = 0;
			dmsgn("%02d  | ", i); // index
			count += dmsgn("%s", swe_data_encoding_text(resp->encodings[i]),resp->encodings[i]);
			memset(temp, ' ', 20-count);
			temp[20-count] = 0;
			dmsgn("%s|", temp);
			dmsgn(" %s", ROLE_NAME(resp->roles[i]));

			// Check if role is data //
			if (resp->roles[i] == enabled_data) {
				dmsgn("| [] (%d bytes)",  resp->element_sizes[i]);
			}
			// Check if role is disabled data //
			else if (resp->roles[i] == disabled_data) {
				dmsgn("| disabled");
			}
			// Check if role is token //
			else if (resp->roles[i] == fixed_token ){
				fmt_dmsg(NRM, "| [%s]", (char*)resp->values[i]);
			}
			else {
				errmsg("unknown type %d", (int)resp->roles[i]);
			}

			if (resp->nilvalues[i]!=NULL){
				int j;
				dmsgn(" NilValues: ");
				for (j=0 ; j<resp->nilvalues[i]->n ; j++ ){
					dmsgn("%s, ", resp->nilvalues[i]->values[j]);
				}
			}
			dmsg("");
		}
	}
	else if ( response->encoding == SWE_BinaryEncoding ) {
		BinaryResponse* resp = (BinaryResponse*)response->response;
		for ( i=0 ; i < resp->count ; i++ ){
			count = 0;
			dmsgn("%02d  | ", i); // index
			count += dmsgn("%s", swe_data_encoding_text(resp->encodings[i]),resp->encodings[i]);
			memset(temp, ' ', 20-count);
			temp[20-count] = 0;
			dmsgn("%s|", temp);
			dmsgn(" %s", ROLE_NAME(resp->roles[i]));

			// Check if role is data //
			if (resp->roles[i] == enabled_data) {
				if ( resp->element_counts[i] == 1 ) {
					dmsgn("| [] (%d bytes)",  resp->element_sizes[i]);
				} else {
					uint instance_size = get_size_by_encoding(resp->encodings[i]);
					dmsgn("| [] (%u bytes, %u x %u bytes)",  resp->element_sizes[i], resp->element_counts[i], instance_size);
				}
			}
			// Check if role is disabled data //
			else if (resp->roles[i] == disabled_data) {
				dmsgn("| disabled");
			}
			// Check if role is token //
			else if (resp->roles[i] == fixed_token ){
				fmt_dmsg(NRM, "| [%s]", (char*)resp->values[i]);
			}
			else {
				errmsg("unknown type %d", (int)resp->roles[i]);
			}

//			if (resp->nilvalues[i]!=NULL){
//				int j;
//				dmsgn(" NilValues: ");
//				for (j=0 ; j<resp->nilvalues[i]->n ; j++ ){
//					dmsgn("%s, ", resp->nilvalues[i]->values[j]);
//				}
//			}
			dmsg("");
		}
	}
	else if ( response->encoding == SWE_XMLEncoding ) {
		XmlResponse* resp = (XmlResponse*)response->response;
		for ( i=0 ; i < resp->count ; i++ ){
			count = 0;
			dmsgn("%02d  | ", i); // index
			count += dmsgn("%s", swe_data_encoding_text(resp->encodings[i]),resp->encodings[i]);
			memset(temp, ' ', 20-count);
			temp[20-count] = 0;
			dmsgn("%s|", temp);
			dmsgn(" %s", ROLE_NAME(resp->roles[i]));

			// Check if role is data //
			if (resp->roles[i] == enabled_data) {
				dmsgn("| [] (%d bytes)",  resp->element_sizes[i]);
			}
			// Check if role is disabled data //
			else if (resp->roles[i] == disabled_data) {
				dmsgn("| disabled");
			}
			// Check if role is token //
			else if (resp->roles[i] == fixed_token ){
				fmt_dmsg(NRM, "| [%s]", (char*)resp->values[i]);
			}
			else {
				errmsg("unknown type %d", (int)resp->roles[i]);
			}

//			if (resp->nilvalues[i]!=NULL){
//				int j;
//				dmsgn(" NilValues: ");
//				for (j=0 ; j<resp->nilvalues[i]->n ; j++ ){
//					dmsgn("%s, ", resp->nilvalues[i]->values[j]);
//				}
//			}
			dmsg("");
		}
	}
	dmsg("------------------------------------------------------------\n");
	return swe_ok;
}



/*
 * Calculates a buffer size according to the Scheduler Data structure and the
 * the fixed sizes of each data type (ASCII_QUANTITY_BYTES, ASCII_COUNT_BYTES,
 * ASCII_BOOLEAN_BYTES and ASCII_TEXT_BYTES). A safety coefficient is then
 * applied to the buffer.
 */
int calculate_buffer_size(ResponseStructure* resp){
	int i;
	int buffsize = 0;
	float safety_coefficient = BUFFER_SAFETY_COEFFICIENT;


	if ( resp->encoding == SWE_TextEncoding ) {
		AsciiResponse* asciiresp = (AsciiResponse*)resp->response;
		for (i=0 ; i < asciiresp->count ; i++) {
			if (asciiresp->element_sizes[i] !=0 ) { // If the current element has a defined size add it to the buffer
				buffsize += (int)asciiresp->element_sizes[i];
			}
			else { // If the current element doesn't have a defined size, it means that we have to calculate
				   // its size depending on the type
				switch(asciiresp->encodings[i]) {
					case  ascii_quantity_data:
						buffsize += ASCII_QUANTITY_BYTES;
						break;
					case  ascii_count_data:
						buffsize += ASCII_COUNT_BYTES;
						break;
					case  ascii_boolean_data:
						buffsize += ASCII_BOOLEAN_BYTES;
						break;
					case  ascii_text_data:
						buffsize += ASCII_TEXT_BYTES;
						break;
					default :
						errmsg("Unimplemented binary types!");
						return swe_unimplemented;
						break;
				}
			}
		}
		buffsize = MAX(buffsize, MINIMUM_STREAM_BUFFER_SIZE);
		dmsg("Minimum buffer size %d", buffsize);


		buffsize = (int) ( (float) buffsize * safety_coefficient);
		dmsg("Applying safety coefficient %f, new buffer size %d",safety_coefficient,  buffsize);
	}

	else if ( resp->encoding == SWE_BinaryEncoding ) {
		BinaryResponse* binresp = (BinaryResponse*)resp->response;
		buffsize = binresp->length;
		buffsize = (int) ( (float) buffsize * safety_coefficient);
		dmsg("Applying safety coefficient %f, new buffer size %d",safety_coefficient,  buffsize);
	}


	// If XML allocate 10 times more space to store all the XML tags
	if (resp->encoding == SWE_XMLEncoding) {

		XmlResponse* asciiresp = (XmlResponse*)resp->response;
		for (i=0 ; i < asciiresp->count ; i++) {
			if (asciiresp->element_sizes[i] !=0 ) { // If the current element has a defined size add it to the buffer
				buffsize += (int)asciiresp->element_sizes[i];
			}
			else { // If the current element doesn't have a defined size, it means that we have to calculate
				   // its size depending on the type
				switch(asciiresp->encodings[i]) {
					case  ascii_quantity_data:
						buffsize += ASCII_QUANTITY_BYTES;
						break;
					case  ascii_count_data:
						buffsize += ASCII_COUNT_BYTES;
						break;
					case  ascii_boolean_data:
						buffsize += ASCII_BOOLEAN_BYTES;
						break;
					case  ascii_text_data:
						buffsize += ASCII_TEXT_BYTES;
						break;
					default :
						errmsg("Unimplemented binary types!");
						return swe_unimplemented;
						break;
				}
			}
		}
		buffsize = MAX(buffsize, MINIMUM_STREAM_BUFFER_SIZE);
		dmsg("Minimum buffer size %d", buffsize);


		buffsize = (int) ( (float) buffsize * safety_coefficient);
		dmsg("Applying safety coefficient %f, new buffer size %d",safety_coefficient,  buffsize);

		buffsize = buffsize * 10;
		warnmsg("XML response!, forcing 10 times more space in buffer (%d bytes)", buffsize);
	}

	return buffsize;

}

//--------- ASCII Response Setters ------------//

/*
 * Sets the Ignore Leading tokens option
 */
int set_ignore_leading_tokens(ResponseStructure* resp, uchar value) {
	if (resp->encoding != SWE_TextEncoding) {
		return swe_unimplemented;
	}
	AsciiResponse * asciiresp = (AsciiResponse*)resp->response;
	asciiresp->ignore_leading_tokens = value;
	return swe_ok;
}

/*
 * Sets the Allow Empty Fields
 */
int set_allow_empty_fields(ResponseStructure* resp, uchar value) {
	if (resp->encoding != SWE_TextEncoding) {
		return swe_unimplemented;
	}
	AsciiResponse * asciiresp = (AsciiResponse*)resp->response;
	asciiresp->allow_empty_fields = value;
	return swe_ok;
}

/*
 * Sets the Check data integrity
 */
int set_check_data_integrity(ResponseStructure* resp, uchar value) {
	if (resp->encoding != SWE_TextEncoding) {
		return swe_unimplemented;
	}
	AsciiResponse * asciiresp = (AsciiResponse*)resp->response;
	asciiresp->check_data_integrity = value;
	return swe_ok;
}

/*
 * Sets the Collapse White Spaces
 */
int set_collapse_white_spaces(ResponseStructure* resp, uchar value) {
	if (resp->encoding != SWE_TextEncoding) {
		return swe_unimplemented;
	}
	AsciiResponse * asciiresp = (AsciiResponse*)resp->response;
	asciiresp->collapse_white_spaces = value;
	return swe_ok;
}
