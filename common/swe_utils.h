/*
 ============================================================================
									SWE_utils.h
 ============================================================================
 Defines some general purpose functions used in the project
 ============================================================================
 by SARTI
 Author: Enoc Martínez
 ============================================================================
 */

#ifndef SWE_UTILS_H_
#define SWE_UTILS_H_

#include "swe_conf.h"
#include "resources/logging.h"
#include "swe_data_model.h"


//-----------------------------------------------------------------//
//---------------------- SWE BRIDGE OPTIONS -----------------------//
//-----------------------------------------------------------------//

/*
 *  Option used to use a local configuration file instead of parsing command-line options. This
 *  option will drop all other command-line options
 */
#define USE_CONFIG_FILE "-conf" // <file>


/*
 * Parse a SensorML file
 */
#define USE_LOCAL_FILE_OPTION "-file" // <file>

/*
 * Retrieve an EXI-encoded SensorML file from the Instrument's PUCK memory
 */
#define USE_PUCK_OPTION "-puck" // <serial port> (baudrate)

/*
 * Retrieve an XML/EXI-encoded SensorML file from the Instrument's PUCK memory
 */
#define WRITE_PUCK_MEM_OPTION "-write-puck" // <filename> <tag> (baudrate)

/*
 * Pass arguments to the platform init function
 */
#define PLATFORM_OPTIONS "-platform" // (variable number of arguments, depends on the platform)

/*
 * Change the log level:
 * 0 : Debug + Info + Warnings + Errors
 * 1 : Info + Warnings + Errors
 * 2 : Warnings + Errors
 * 3 : Errors only
 */
#define SET_LOG_LEVEL_OPTION "-log" // <level>

/*
 * Save logs into specified location
 */
#define SAVE_LOG_OPTION "-save-log" // <level>

/*
 * Show help and exit
 */
#define SHOW_HELP_OPTION "-help"

/*
 * Runs a system check to see if all the functions are correctly implemented
 */
#define SYSTEM_CHECK_OPTION "-system-check"

/*
 * Use platform deep sleep option instead of platform sleep
 */
#define DEEP_SLEEP_OPTION "-deep-sleep"




// List of chars used to separate arguments. Only used when using a configuration file
#define ARGUMENT_SEPARATOR_LIST " \n\r\t"

/*
 * Declare SWE Bridge Option
 */
typedef enum {
	local_file_option=0,
	puck_option,
	write_puck_option,
	platform_options,
	show_help_option,
	set_log_level_option,
	save_log_option,
	system_check_option,
	deep_sleep_option
}SWE_Bridge_Option_Type;




/*
 * SWE Bridge Options structure
 */
typedef struct{
	char** values;
	SWE_Bridge_Option_Type type;
	uchar flag; // if flag = TRUE option is set
}SWE_Bridge_Option;

// TODO avoid hardcoding buffer size
#define PARSE_OPTIONS_BUFFER 500
SWE_Bridge_Option** parse_options(int argc, char** argv);
#define PRINT_OPTION_NAME(x) #x




//-----------------------------------------------------------------//
//----------------------- STRING MANIPULATION ---------------------//
//-----------------------------------------------------------------//

int string_to_int(const char* string);
int format_string(char* from, char* to, int to_lentgh );
uchar string_in_list(const char* string, const char* list[]);
char** get_substrings(char* buffer, const char* token, uint* nFields);
char** set_substrings(char* buffer, const char* token);
char* hex_to_string(char* inputString);
int compare_strings(const char* a, const char* b);
int extract_string(char* in_str, char* buffer, char delimit);
char* fast_strcat(char* to, char* from);
char* convert_xml_special_chars(char* instring);
uchar get_true_false(const char* in, int *errorcode) ;
int string_to_int(const char* string);
uchar check_ascii_data_integrity(uchar* data_in);
int collapse_white_spaces(char* buffer, uint bufflen);
int collapse_white_spaces2(char* buffer, uint bufflen);
int check_string_for_ascii_quantity(char *string);
const char* get_errorcode(int errorcode);
int to_lower_case(char* string);


//-----------------------------------------------------------------//
//------------------------ OTHERS FUNCTIONS -----------------------//
//-----------------------------------------------------------------//


// Base64 Functions //
char*  base64_encode(const uchar *data, uint input_length, uint *output_length);
uchar* base64_decode(const char *data, uint *output_length);

// Date & Time //
int ascii_datetime(char* buffer, float64 timestamp, uchar msecs);
int datetime_from_timestamp(DateTime* datetime, float64 timestamp);
int ascii_datetime_utc( char* buffer, float64 timestamp);
int datetime_from_timestamp_utc(DateTime* datetime, float64 timestamp);
float64 datetime_to_timestamp(DateTime datetime);


// Checks the system resources //
int system_compatibility_check();
int generate_md5_code(char* filename, char* output_buff, int buff_len);

//-----------------------------------------------------------------//
//------------------------ Type Manipulation ----------------------//
//-----------------------------------------------------------------//

int int32_to_little_endian(int32_t value, uchar* buff);
int int32_to_big_endian(int32_t value, uchar* buff);
int32_t to_synchsafe(int32_t in);


//-----------------------------------------------------------------//
//------------------------ MATH MACROS/FUNCTIONS ------------------//
//-----------------------------------------------------------------//

#ifndef MIN
#define	MIN(a,b)	((a) < (b) ? (a) : (b))
#endif
#ifndef MAX
#define MAX(a,b)	((a) > (b) ? (a) : (b))
#endif

#ifndef PWR
#define PWR(_base, _exponent) ({ \
		int _result = 1; \
		int _exponent_copy = _exponent; \
		while(_exponent_copy > 0) { \
			_result *= _base; \
			_exponent_copy--; \
		} \
		_result; \
	})
#endif

ulong greatest_common_divisor(ulong a, ulong b);



//-----------------------------------------------------------------//
//--------------------------- "TRY" MACROS ------------------------//
//-----------------------------------------------------------------//

extern const char* swe_error_strings[];

/*
 * Executes a function and returns error if the return value is negative
 */
#define TRY_RET(func) do { \
	int __temp_error = func;\
	if (__temp_error < 0) { \
		errmsg("Errorcode %d: \"%s\" at %s, line %d",__temp_error,  swe_error_strings[abs(__temp_error)], __FILE__, __LINE__);\
		return __temp_error; \
	}\
} while(0)

/*
 * Similar to TRY_RET, but with functions that return a pointer (NULL if they fail).
 * WARNING: This macro can only be used within functions that return int values!
 */
#define TRY_NULL(func) ({ \
	void* __temp_return = func;\
	if (__temp_return == NULL) { \
		errmsg("returned NULL pointer at %s, line %d",  __FILE__, __LINE__);\
		return swe_error; \
	}\
	__temp_return; \
})

/*
 * Executes the function _myfunc and stores its value on _myresult. If _myfunc returns NULL, an error
 * is displayed and return NULL
 */
#define CATCH_NULL(_myfunc) ({ \
		void * _myresult; \
		if ( (_myresult = _myfunc) == NULL )  { \
			errmsg("Error (result is NULL)"); \
			return NULL; \
		}\
		_myresult; \
	})

/*
 * Similar to TRY_RET, but with instead of returning the function's errorcode, returns NULL
 * WARNING: This macro can only be used within functions that return pointers!
 * WARNING: The argument func should be a int-returning fuction!
 */
#define TRY_RET_NULL(func) do { \
	int __temp_error = func;\
	if (__temp_error < 0) { \
		errmsg("Errorcode: \"%s\" at %s, line %d",  swe_error_strings[abs(__temp_error)], __FILE__, __LINE__);\
		return NULL; \
	}\
} while(0)


//-----------------------------------------------------------------//
//--------------------- ARRAY MANIPULATION MACROS -----------------//
//-----------------------------------------------------------------//

/*
 * Returns the number of elements in a NULL-terminated array
 */
#define ARRAY_LENGTH(pointer) ({int __index; \
	if(pointer!=NULL) for (__index=0; pointer[__index]!=NULL; __index++);\
	else __index=0; \
	__index;})

/*
 * returns a pointer to the last non-null element of an array
 */
#define CURRENT_ELEMENT(pointer) ({\
	int element_index=ARRAY_LENGTH(pointer); \
	__typeof__(*pointer) new_pointer; \
	if(element_index > -1) { \
	element_index--; new_pointer=pointer[element_index]; }\
	else new_pointer=NULL; \
	new_pointer;})

/*
 *  returns a null-terminated array of "type" elements, the first slot is allocated and the second is null
 * example -> int**a= NEW_ARRAY(int);
 */
#define NEW_ARRAY(type) ({type** pointer; \
	pointer=swe_malloc(2*sizeof(type*)); \
	pointer[0]=swe_malloc(sizeof(type)); \
	pointer[1]=NULL; \
	pointer; })



/*
 * takes a null-terminated array and attaches a new element at the end
 * if the array is NULL a new NULL-terminated array is create
 */
#define ATTACH_TO_ARRAY(pointer, new_element) ({ \
	int _internal_index=ARRAY_LENGTH(pointer); \
	if (_internal_index == 0 ) { /* Create new array */ \
		pointer=swe_malloc(2*sizeof(*pointer)); \
		pointer[0] = new_element; \
		pointer[1] = NULL ;  \
	} \
	else { /* Attach to array */ \
		/* last size + 2, one for the new element and another for the null pointer*/ \
		pointer=swe_realloc(pointer, (_internal_index+2)*sizeof(*pointer)); \
		pointer[_internal_index]=new_element; \
		pointer[_internal_index+1]=NULL; \
	} \
	pointer; /*return the pointer */ \
})

/*
 * Takes a NULL terminated array and removes the last element, reallocating for
 * n - elements
 */
#define REMOVE_LAST_ARRAY_ELEMENT(pointer) ({ \
		int _internal_index=ARRAY_LENGTH(pointer); \
		pointer[_internal_index-1] = NULL ; \
		pointer = swe_realloc(pointer, (sizeof(*pointer)*(_internal_index))); \
		pointer; \
})

/*
 * Removes the element from the array. Other elements are reallocated
 */
#define REMOVE_FROM_ARRAY(_array, _element) ({ \
	int _i, _element_index=-1; \
	int _n = ARRAY_LENGTH(_array); \
	for (_i = 0; _array[_i]!=NULL ; _i++) { \
		if (_array[_i] == _element) { \
			_element_index = _i; \
			break; \
		} \
	} \
	if (_element_index < 0) { \
		warnmsg("Can't remove from array, element not found"); \
	} \
	else if ( _n == 1) { /* If it was the last element */ \
		swe_free(_array); /* free the array */ \
	}\
	else { \
	/* move down all pointers starting from the deleted element */ \
		for (_i = _element_index ; _array[_i]!=NULL ; _i++ ) { \
			_array[_i] = _array[_i+1]; \
		} \
		/* If it's the last element force NULL */ \
		_array[_n - 1] = NULL; \
		_array = swe_realloc(_array, (sizeof(*_array)*(_n))); \
	} \
	_array; \
})


/*
 * Attaches an existing array at the end of another one
 */
#define ATTACH_ARRAY_TO_ARRAY(_my_array, _new_array) ({ \
	int _i; \
	for ( _i=0 ; _new_array[_i] != NULL ; i++) { \
		_my_array = ATTACH_TO_ARRAY(_my_array, _new_array[_i]); \
	} \
})


/*
 * Frees every element in the array and then frees the array itself
 */
#define FREE_ARRAY(_pointer) ({ \
	if (_pointer!=NULL) { \
		int _index; \
		for ( _index=0 ; _pointer[_index]!= NULL ; _index++ ){ \
			swe_free(_pointer[_index]); \
		} \
		swe_free(_pointer); \
	} \
})


/*
 * This macro is similar to ATTACH_TO_ARRAY, but instead of using an array of pointers uses
 * a list of elements (type* instead of type**).
 *
 * type* attach_to_list (type* list, type new_element );
 *
 */
#define ATTACH_TO_LIST(pointer, _new_element, _oldsize) ({ \
	if (_oldsize == 0 ) { /* Create new list */ \
		pointer=swe_malloc(sizeof(*pointer)); \
		pointer[0] = _new_element; \
	} \
	else { /* Attach to the list */ \
		pointer=swe_realloc(pointer, (_oldsize+1)*sizeof(*pointer)); \
		pointer[_oldsize]=_new_element; \
	} \
	pointer; /*return the pointer */ \
})


//-----------------------------------------------------------------//
//------------------------ Other MACROS MACROS --------------------//
//-----------------------------------------------------------------//
/*
 * Checks if argument is NULL an returns _ret_value
 */
#define CHECK_NULL(_pointer, _ret_value) ({ \
	if (_pointer == NULL ) { /* Create new list */ \
		errmsg("Argument \"%s\" in function %s is NULL", #_pointer, __func__);\
		return _ret_value; \
	} \
})

#endif












