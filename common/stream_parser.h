/*
 *  This File contains the implementation of a parser which is capable of dealing with
 *  ASCII binary and XML streams. It's the generic parser used in further processes in
 *  the SWE Bridge
 *
 *  @author: Enoc Martínez
 *  @institution: Universitat Politècnica de Catalunya (UPC)
 *  @contact: enoc.martinez@upc.edu
 */

#ifndef COMMON_INC_STREAM_PARSER_H_
#define COMMON_INC_STREAM_PARSER_H_

#include "common/swe_data_model.h"
#include "resources/resources.h"
#include "swe_conf.h"

typedef struct{
	uchar* buffer; // buffer
	uint size; // size of the buffer
	uint count; //number of elements (bytes) in buffer
	uint index; // index of the buffer

	uchar erase_junk; // If set to TRUE the buffer will erase non-valid data dynamically
	               	  // This option is used to avoid buffer overflow when the response
	                  // is proceeded by lots of junk data. This option can only be used
					  // if the start Token is set
}StreamBuffer;


/*
 * Return values for the parser function
 */
typedef enum {
	stream_successfully_parsed = 0,
	stream_not_complete,
	stream_with_errors,
	unknown_parser_error,
}parser_retcode;


typedef enum {
	enabled_data = 0, // relevant data
	disabled_data, // data which is not relevant
	fixed_token // fixed token
}data_role;


/*
 * List of elements not valid within a BasicType
 */
typedef struct{
	uchar** values;
	uchar n;
}NilValues;

/*
 * The Response structure is used to parse the incoming data streams from an instrument
 */
typedef struct {
	data_stream_encoding encoding; // Flag to quickly determine if the response is binary or ascii
	void* response;
}ResponseStructure;



/*
 * Macro that defines a common set of parameters to be used in all responses
 *
 * count: Total number of elements in the structure
 * values: array of "slots" where data will be stored
 * n:
 * element_sizes: list containing the size of each element
 * configuration_fields: Array of fields containing config data
 * roles: list of the roles of each element, role can be fixed token, disabled data, enabled data, etc.
 * encodings: list of encodings for each element
 * length: total length
 * element_count: When greater than 1 means that there is an array of elements
 */
#define RESPONSE_COMMON_PARAMETERS() \
		uint count; \
		uchar** values; \
		uchar n; \
		uint* element_sizes;  \
		Field** configuration_fields;   \
		data_role* roles; \
		swe_data_encoding* encodings; \
		uint length; \
		ResponseStructure* parent; \
		uint *element_counts;



/*
 * This macro takes the variables declares as local variables the common parameters of a response
 * structure
 */
#define GET_RESPONSE_COMMON_PARAMETERS(_resp) \
		uint count; \
		uchar** values; \
		uint* element_sizes;  \
		Field** configuration_fields;   \
		data_role* roles; \
		swe_data_encoding* encodings; \
		uint length; \
		uint* element_counts; \
		if (_resp->encoding == SWE_TextEncoding) { \
			AsciiResponse*_formatted_response = (AsciiResponse*)_resp->response; \
			count =_formatted_response->count; \
			values = _formatted_response->values; \
	 		element_sizes = _formatted_response->element_sizes; \
			configuration_fields = _formatted_response->configuration_fields; \
			roles = _formatted_response->roles; \
			encodings = _formatted_response->encodings; \
			length = _formatted_response->length; \
			element_counts = _formatted_response->element_counts; \
		} \
		else if (_resp->encoding == SWE_BinaryEncoding){ \
			BinaryResponse*_formatted_response = (BinaryResponse*)_resp->response; \
			count =_formatted_response->count; \
			values = _formatted_response->values; \
			element_sizes = _formatted_response->element_sizes; \
			configuration_fields = _formatted_response->configuration_fields; \
			roles = _formatted_response->roles; \
			encodings = _formatted_response->encodings; \
			length = _formatted_response->length; \
			element_counts = _formatted_response->element_counts; \
		} \
		else if (_resp->encoding == SWE_XMLEncoding){ \
			XmlResponse*_formatted_response = (XmlResponse*)_resp->response; \
			count =_formatted_response->count; \
			values = _formatted_response->values; \
			element_sizes = _formatted_response->element_sizes; \
			configuration_fields = _formatted_response->configuration_fields; \
			roles = _formatted_response->roles; \
			encodings = _formatted_response->encodings; \
			length = _formatted_response->length; \
			element_counts = _formatted_response->element_counts; \
		}

/*
 * ASCII Response Structure
 */
typedef struct {
	// Common parameters //
	RESPONSE_COMMON_PARAMETERS();

	// ASCII parameters //
	uchar check_data_integrity;
	uchar collapse_white_spaces;
	uchar ignore_leading_tokens; // Ignore leading token separators
	uchar allow_empty_fields; // Allows fields to be empty, e.g. 43.12,,43.234\r\n understood as 3 fields
	uint ntokens;
	NilValues** nilvalues;

}AsciiResponse;


/*
 * Binary Response
 */
typedef struct{
	// Common parameters //
	RESPONSE_COMMON_PARAMETERS();

	// Binary Parameters //
	endianness stream_endianness; //bigEndian or littleEndian
	endianness platform_endianness; //bigEndian or littleEndian

}BinaryResponse;


/*
 * XML Response
 */
typedef struct {
	RESPONSE_COMMON_PARAMETERS();

	char*** tags; // NULL-terminated array of the arrays of tags where data is stored:

}XmlResponse;




StreamBuffer* stream_buffer_constructor(uint size, uchar erase_junk);
int empty_buffer(StreamBuffer* buff);
int fill_buffer(StreamBuffer* buffer, Interface* iface);
int readjust_buffer(StreamBuffer* buffer);

parser_retcode stream_parser(StreamBuffer* buff, ResponseStructure* structure);
ResponseStructure* response_structure_constructor(Field* root, uchar data_block_flag);
int print_response_structure(ResponseStructure* data);


// Setters

int set_ignore_leading_tokens(ResponseStructure* resp, uchar value);
int set_allow_empty_fields(ResponseStructure* resp, uchar value);
int set_check_data_integrity(ResponseStructure* resp, uchar value);
int set_collapse_white_spaces(ResponseStructure* resp, uchar value);


#define BUFFER_SAFETY_COEFFICIENT 1.5
#define MINIMUM_STREAM_BUFFER_SIZE 256 //
int calculate_buffer_size(ResponseStructure* resp);

/*
 * Macro to free the buffer
 */
#define free_buffer(_buff) do { \
	swe_free(_buff->buffer);\
	swe_free(_buff);\
	_ptr = NULL; \
}while(0)



/*stream_successfully_parsed = 0,
stream_not_complete,
stream_with_errors,
unknown_parser_error*/
/*
 * Executes a function and returns error if the return value is negative
 */
#define PARSER_TRY(func) do { \
	int __temp_error = func;\
	if (__temp_error == stream_successfully_parsed) {} \
	else { \
		if (__temp_error ==  stream_not_complete) { \
			warnmsg("stream_not_complete");\
		}\
		else if(__temp_error == stream_with_errors) {\
			errmsg("stream_with_errors file %s, line %d", __FILE__, __LINE__);\
		} \
		else { \
			errmsg("unknown_parser_error file %s, line %d", __FILE__, __LINE__);\
		}\
		return __temp_error; \
	}\
} while(0)


#endif /* COMMON_INC_STREAM_PARSER_H_ */
