/*
 ============================================================================
									SWE_utils.c
 ============================================================================
 Defines some general purpose functions used in the project
 ============================================================================
 by SARTI
 Author: Enoc Martínez
 ============================================================================
 */

#include "common/swe_utils.h"
#include "common/md5.h"
#include "resources/resources.h"
#include "swe_conf.h"
#include "swe_bridge.h"
#include "resources/logging.h"

#include <math.h>

//---- Private function declarations ----//
char** get_options_from_file(char* filename, int* argc);
uchar is_char_separator(char _c, char* _list);


//-----------------------------------------------------------------//
//---------------------- SWE BRIDGE OPTIONS -----------------------//
//-----------------------------------------------------------------//

/*
 * This macro loops until through all the arguments related to an option and
 * attaches them into the option structure. Stops when a new option is found
 * (argument with leading '-')
 */
#define PROCESS_OPTION_ARGUMENTS(_argc, _argv, _index, _structure) do {\
	while ( (_argc > _index + 1) && (_argv[_index + 1 ][0] != '-')) { \
		_structure->values = ATTACH_TO_ARRAY(_structure->values, _argv[_index+1]); \
		_index++; \
	} \
}while(0); \


/*
 * Compares an incoming option string and if it matches processes a new option
 */
#define PROCESS_OPTION(option_string, option_type) do{ \
		if (!compare_strings(option_string, argv[i]) || !compare_strings("-"option_string, argv[i])){ \
			option_processed = TRUE; \
			o->type = option_type; \
			PROCESS_OPTION_ARGUMENTS(argc, argv, i, o); \
		} \
}while(0)


/*
 * Get a set of options from a configuration file
 */
char** get_options_from_file(char* filename, int* argc){
	char** options = NULL;
	char *buffer, *bufferbckp;
	uchar was_last_separator = TRUE; // flag to determine if the last char was a separator
	swe_file* file = NULL;
	int nargs = 0;
	int i = 0, j = 0;

	if (filename == NULL ) {
		errmsg("%s received NULL pointer", __func__);
	}

	imsg("reading options from configuration file %s", filename);
	if ((file = swe_fopen(filename, swe_read_file)) == NULL) {
		errmsg("Couldn't open options file!");
		return NULL;
	}

	int fsize = PARSE_OPTIONS_BUFFER;
	/*dmsg("options file size: %d bytes", fsize);
	if (fsize < 0 ) {
		return NULL;
	}*/
	buffer = swe_malloc(sizeof(char)*fsize + 1);

	if ((swe_fread(buffer, sizeof(char)*fsize, file)) < 0){
		errmsg("could not read file %s", filename);
		return NULL;
	}

	swe_fclose(file); // Close the file
	bufferbckp = buffer;

	//---- count number of arguments----//
	while (buffer[0] != 0 ) { // loop until end of file
		if (is_char_separator(buffer[0], ARGUMENT_SEPARATOR_LIST)) {
			was_last_separator = TRUE;
			buffer[0] = 0;

		} else{
			if (was_last_separator) {
				// first non-separator after a separator determines a new argument
				nargs++;
			}
			was_last_separator = FALSE;
		}
		buffer++;
	}

	dmsg("conf file  has %d args", nargs);
	options = swe_malloc(sizeof(char*)*nargs); // allocate memory for options
	buffer = bufferbckp; // restore buffer position

	//---- Separate arguments ----//
	for (i = 0; i<fsize ; i++) {
		if (buffer[i] != 0 ) {
			// look for first argument
			while(buffer[i]==0) { // skip all separators
				i++;
				if(i > fsize) break;
			}
			// first non-char is new arg
			options[j] = &buffer[i]; // assign arg
			j++;

			while(buffer[i]!=0 && i < fsize) { // skip all non-separators
				i++;
				if(i > fsize) break;
			}
		}
	}
	*argc = nargs; // assign number of arguments
	return options;
}


/*
 * This functions takes argc and argv and arranges them into
 * an array of SWE_Bridge_Options
 */
SWE_Bridge_Option** parse_options(int inargc, char** argv){
	SWE_Bridge_Option **opts = NULL; // temporal array
	int start_count = STARTING_ARGUMENT;
	int argc = inargc; //assign to a local variable to modify it

	int i; // input argument index

	/*
	 * Check if the options are defined by a command-line arguments
	 * or by a configuration file
	 */
#if FORCE_CONFIG_FILE
	dmsg("Parsing arguments from file %s", STATIC_CONFIG_FILE);
	argv=get_options_from_file(STATIC_CONFIG_FILE, &argc); // use config file when the environment forces it
	if (argv == NULL ) {
		errmsg("could not parse options file");
		return NULL;
	}
	dmsg("found %d options in conf file", argc);
	for (i = 0 ; i < argc; i++) {
		dmsg("option %d: \"%s\"", i +1 ,argv[i] );
	}
#else
	/*
	 * Check if there's an argument indicating the usage of a config file
	 */
	if (inargc == 0) return NULL;
	// if there's any config-file argument
	for ( i=start_count; i < argc ; i++) {
		if (!compare_strings(argv[i], USE_CONFIG_FILE)){
			i++;
			if ( i > (argc - 1)) {
				errmsg("expected argument after option %s", USE_CONFIG_FILE);
				return NULL;
			}
			// force using file instead of in-line arguments
			argv=get_options_from_file(argv[i], &argc);

			if (argv == NULL) {
				return NULL;
			}

			start_count = 0; // if arguments are from file force them to start at index 0

		}
	}
#endif

	for (i=start_count; i<argc; i++){ //loop through inputs arguments
		// Check that the option is valid
		uchar option_processed = FALSE;

		if ( argv[i][0]!= '-') {
			warnmsg("Ignoring misplaced argument \"%s\"", argv[i]);
			i++;
			continue; // next loop iteration
		}
		SWE_Bridge_Option* o = swe_malloc(sizeof(SWE_Bridge_Option));

		PROCESS_OPTION(USE_LOCAL_FILE_OPTION, local_file_option);
		PROCESS_OPTION(USE_PUCK_OPTION, puck_option);
		PROCESS_OPTION(WRITE_PUCK_MEM_OPTION, write_puck_option);
		PROCESS_OPTION(PLATFORM_OPTIONS, platform_options);
		PROCESS_OPTION(SHOW_HELP_OPTION, show_help_option);
		PROCESS_OPTION(DEEP_SLEEP_OPTION, deep_sleep_option);
		PROCESS_OPTION(SET_LOG_LEVEL_OPTION, set_log_level_option);
		PROCESS_OPTION(SAVE_LOG_OPTION, save_log_option);
		PROCESS_OPTION(SYSTEM_CHECK_OPTION, system_check_option);

		if (!option_processed) {
			warnmsg("Ignoring invalid option \"%s\"", argv[i]);
			swe_free(o);
		} else {
			ATTACH_TO_ARRAY(opts, o); // Attaching option to array ( if empty, the array is initialized)
		}
	}
	return opts;
}

const char* swe_error_strings[] = SWE_ERROR_STRINGS ;


/*
 * Returns a pointer to an errorcode
 */
const char* get_errorcode(int errorcode){
	if (errorcode > ARRAY_LENGTH(swe_error_strings)) {
		warnmsg("Trying to get an error string out of bounds!");
		return NULL;
	}
	return swe_error_strings[abs(errorcode)];
}


/*
 * converts the input string to a lower-case string, e.g. "ThisIs3aString" -> "thisis3astring"
 * all characters that are not letters are ignored.
 */

int to_lower_case(char* string){
	int i;
	if (strlen(string) < 0){
		return swe_invalid_arguments;
	}
	for ( i=0 ; i<strlen(string) ; i++){
		if (  string[i] >= 65 &&  string[i] <= 90 ) { // if between 'A' and 'Z'
			string[i] += 32; // adds the difference between 'A' and 'a' in the ASCII table
		}
	}
	return swe_ok;
}



//-----------------------------------------------------------------//
//----------------------- STRING MANIPULATION ---------------------//
//-----------------------------------------------------------------//

/*
 *  Changes <CR>, <NL> and <TAB> for ASCII-human readable '\n' '\r' and '\t'
 */
int format_string(char* from, char* to, int to_lentgh ){
	uint i, j, len;

	if(from==NULL){
		return swe_error;
	}
	j=0;

	len=strlen(from);
	memset(to, 0, to_lentgh);
	for (i=0; i< len; i++){
		if(from[i]=='\n'){
			to[j]=92; // '\'
			j++;
			to[j]='n';
		}
		else if (from[i]=='\r'){
			to[j]=92;// '\'
			j++;
			to[j]='r';
		}
		else if (from[i]=='\t'){
			to[j]=92;// '\'
			j++;
			to[j]='t';
		}
		else{
			to[j]=from[i];
		}
		j++;
		if(j==to_lentgh){
			break;
		}
	}
	to[j]=0;
	return swe_ok;
}


/*
 * This function gets the substrings delimited by the string token. A pointer
 * to an string array is returned. The number of strings found is stored in
 * the nStrings pointer. This function modifies the 'buffer' string, setting
 * to '0' the token substrings. The substrings returned are pointing at the
 * buffer memory, so freeing the buffer will automatically erase all the
 * substrings.
 *
 * example
 *  buffer* "string1---string2"
 *  token* "---"
 *
 *  after:
 *  buffer* "string1\0\0\0strings2"
 *    char[0]^            ^
 *                 char[1]^
 *
 *  char*[0]="string1"        (pointer to buffer[0])
 *  char*[1] "string2"        (pointer to buffer[10])
 *
 */
char** get_substrings(char* buffer, const char* token, uint* nStrings){
	if(buffer==NULL || token==NULL || nStrings==NULL){
		errmsg( "get_substrings: NULL pointer received");
		return NULL;
	}
	uint sLen=strlen(buffer);
	uint tLen=strlen(token);

	uint strpos[sLen];
	uint nstrs;
	char** strings=NULL;
	uint i;

	i=0;
	nstrs=1;
	strpos[0]=0; //there's at least 1 string
	while(i<(sLen-tLen)){
		if(!memcmp(&buffer[i], token, tLen)){ //found token
			void* point=&buffer[i];
			memset(point, 0, tLen); //erase the separator

			i+=tLen;
			if(i<sLen){ //check that the function still is inside the string
				strpos[nstrs]=i;
				nstrs++;
			}
		} else{
			i++;
		}
	}
	strings=swe_malloc((nstrs+1)*sizeof(char*)); //store one more element, to end the array with NULL
	strings=memset(strings, 0, (nstrs+1)*sizeof(char*));
	for(i=0; i<nstrs; i++){
		strings[i]=&buffer[strpos[i]];
	}
	*nStrings=nstrs;
	return strings;
}

/*
 * Similar to get_substrings, but each substring is individually allocated
 */
char** set_substrings(char* instring, const char* token){
	uint nstrings;
	int i;
	int len = strlen(instring);
	char temp[len + 1];
	memcpy(temp, instring, len);
	temp[len]=0;

	char** strings = get_substrings(temp, token, &nstrings);

	// Allocate a copy of each string to prevent freeing the memory which itis pointing to
	for (i=0 ; i< nstrings ; i++) {
		strings[i] = set_string(strings[i]);
	}

	return strings;
}

/*
 * Takes a string and converts it from an array of hex values to a ASCII string.
 */
char* hex_to_string(char* inputString){
	int i=0, j=0, count=0;
	char* ret;
	uchar endConversion=0;
	char* buffer;
	char* result;

	buffer=set_string(inputString);
	result=swe_malloc(sizeof(char)*strlen(inputString));
	while(!endConversion){
		if((inputString[i]==' ')||(inputString[i]==0)){
			buffer[j]=0;
			result[count]=(char)strtol (buffer, NULL, 16);
			count++;
			j=0;
		}
		else {
			buffer[j]=inputString[i];
			j++;
		}
		if(inputString[i]==0) {
			endConversion=1;
			result[count]=0;
		}
		i++;
	}
	ret=set_string(result);
	swe_free(buffer);
	swe_free(result);
	return ret;
}



/****************************************************************************
 *								compare_strings
 ****************************************************************************
 * strcmp wrapper, avoid segfault if input arguments are NULL
 ****************************************************************************/

int compare_strings(const char* a, const char* b){
	if(a==NULL || b==NULL) {
		return -1;
	}
	return strcmp(a, b);
}

int extract_string(char* in_str, char* buffer, char delimit){
	int i, j=0;
	uchar in=FALSE;
	for(i=0; i<strlen(in_str); i++){
		if(in_str[i]==delimit && in==FALSE){
			in=TRUE;
			continue;
		}
		if(in==TRUE){
			if(in_str[i]==delimit){
				break;
			}
			buffer[j]=in_str[i];
			j++;
		}
	}
	buffer[j]=0;
	return swe_ok;
}

/*
 * Prints a binary chunk in a grid for debugging purposes
 */
void print_binary_chunk(void* buff, int len, int columns){
	char* p=buff;
	int line=0;

	int i;
	dmsgn("     ");
	for(i=1; i< columns +1 ; i++) {
		cdmsgn(BLU,"%02d ", i);
	}
	dmsgn("\n");
	for (i = 0; i <  len; i ++) {
		if(i % columns == 0){
			line++;
			cdmsgn(BLU,"\n%02d - ", line);
		}
		dmsgn("%02x ",( 0xFF)& p[i]);
	}
	dmsgn("\n");

	// ascii //

	p=buff;
	dmsgn("\n     ");
	for(i=1; i< columns +1 ; i++) {
		cdmsgn(BLU,"%02d ", i);
	}
	line=0;
	for (i = 0; i <  len; i ++) {
		if(i % columns == 0){
			line++;
			cdmsgn(BLU,"\n%02d - ", line);
		}

		if(p[i]=='\n'){
			dmsgn("\\n ");
		} else if(p[i]=='\r'){
			dmsgn("\\r ");
		}
		else if(p[i]=='\t'){
			dmsgn("\\t ");
		}

		else if(p[i] > 31  && p[i] <  126) {
			dmsgn("%c  ", p[i]);

		}
		else {
			cdmsgn(MAG,"%02x ", (0xFF)&p[i]);
		}
	}
	dmsgn("\n\n");
}

/*
 * Prints a binary array in hex (0x00, 0x3F, ...)
 */
int print_binary_array(void *invalue, uint size){
	int i;
	dmsgn("[");
	uchar * value = (uchar*)invalue;
	for (i=0; i < size ; i++ ){
		dmsgn("%02hhx ", value[i]);
	}
	dmsg("]");
	return swe_ok;
}




/****************************************************************************
 *								fast_strcat
 ****************************************************************************
 * Concatenates two strings and returns a pointer to the end of the last string
 * This is used to concatenate different strings without needing to run across
 * the whole string each time. i.e.
 *
 * p=fast_strcat(p, "this is");
 * p=fast_strcat(p, " an example");
 ****************************************************************************/
char* fast_strcat(char* to, char* from){
	if(from==NULL || to==NULL){
		warnmsg( "fast_strcat received a NULL pointer");
		return NULL;
	}
	// Go to the last element of the string
	while(*to!=0){
		to++;
	}
	//while(*to++ = *from++);
	while(*from!=0){
		*to=*from;
		to++;
		from++;
	}

	return to;
}

/*
 * Checks if the string is contained in a null-terminated stgring list
 */

uchar string_in_list (const char* string, const char* list[]) {
	int i;
	if (string == NULL || list == NULL ) {
		return FALSE;
	}
	for (i=0; list[i]!=NULL; i++ ) {
		if (!compare_strings(string, list[i])) {
			return TRUE;
		}
	}
	return FALSE;
}



/*
 * Checks if the char c is in the char array list.
 */
uchar is_char_separator(char _c, char* _list){
	int _index=0;
	for (_index = 0; _list[_index] != 0 ; _index++) {
		if (_c == _list[_index] ) {
			return TRUE;
		}
	}
	return FALSE;
}

static const char* true_messages[]={"true", "TRUE", "True", "enabled", "Enabled", "ENABLED", "ON", "on", "On", "1",  NULL};
static const char* false_messages[]={"false", "FALSE", "False", "disabled", "Disabled", "DISABLED", "OFF", "off", "Off", "0",  NULL};

/*
 * Checks an input string and if it matches any of the true_messages strings, return TRUE,
 * otherwise return FALSE; If the errorcode is not NULL will return swe_ok on success and
 * swe_invalid_arguments in case the input string doesn't match any pattern
 */
uchar get_true_false(const char* in, int *errorcode) {
	if ( errorcode != NULL) {
		*errorcode = swe_ok;
	}
	if (in == NULL) {
		errmsg("get_true_false: NULL input, returning default value (FALSE)");
		return FALSE;
	}
	if (string_in_list(in, true_messages)){
		return TRUE;
	}
	if (string_in_list(in, false_messages)){
		return FALSE;
	}
	warnmsg("get_true_false: Input string [%s] did not match true or false, returning default value (FALSE)", in);
	if ( errorcode != NULL) {
		*errorcode = swe_invalid_arguments;
	}
	return FALSE;
}

/*
 * Converts a string into an integer, the string can be in decimal "255", in hex "0x00FF" or
 * in binary: "0b11111111"
 */
int string_to_int(const char* string) {
	int result = -1;
	int len = strlen(string);
	char s[len + 1];
	char* point;
	memcpy(s, string, len);
	s[len] = 0;

	point = s; // point is used to ignore the first chars when binary or hexa

	collapse_white_spaces(s, len);

	if (!memcmp(s, "0b", 2)  || !memcmp(s, "0B", 2)){
		point += 2; // ignore first 2 chars
		result = strtol(s, NULL, 2);
	}
	else if (!memcmp(s, "0x", 2) || !memcmp(s, "0X", 2)) {
		point += 2; // ignore first 2 chars
		result = strtoul(s, NULL, 16);
	} else { // base 10 by default
		result = strtol(point, NULL, 10);
	}
	return result;
}


/*
 * This function converts XML special chars to their ASCII equivalents, i.e.
 * "&#x0D;&#x0A;" --> "\r\n"
 */

char* convert_xml_special_chars(char* instring){
	char* input = instring;
	uchar modified = FALSE; // flag that indicates if the input string has been modified
	int len; // remaining length
	if (input==NULL) {
		warnmsg("%s NULL argument received", __func__);
		return NULL;
	}
	if ((len=strlen(input)) < 4){
		// too small to have a '&#x' token
		return input;
	}

	// allocate space in the buffer
	char* buff=swe_malloc(sizeof(char)*strlen(input));
	int i=0; //output buff count

	// Scan for "&#" token
	while(input[0] != 0){
		if ( input[0] == '&' &&	 len > 3 ) {
			if (!memcmp(input, "&#x", 3)) {
				char *start;
				char numbuff[25];
				int nlen=0;

				// skip token
				input+=3;len-=3;

				// loop until ';' is found or string ends
				start=input;
				while(input[0] != ';' && input[0] != 0 ) {
					input++; len--; nlen++;
				}
				if (input[0] == 0){
					errmsg("End of string within a special char!");
					return NULL;

				} else if (nlen == 0) {
					errmsg("special char is empty!");
					return NULL;
				}
				// Skip the ; char //
				input++; len--;

				memcpy(numbuff, start, nlen);
				numbuff[nlen]=0;

				// Convert to char //
				buff[i]= strtol(numbuff, NULL, 16);
				i++;
				modified = TRUE;
				continue; // next iteration
			}
		}
		buff[i] = input[0];
		i++;
		input++;
		len--;
	}
	if ( modified == TRUE ) {
		char* ret = set_string(buff); // allocate buffer with the exact memory space
		swe_free(buff); // liberate buff
		swe_free(instring); // liberate input
		return ret;
	}
	swe_free(buff);
	return instring;
}


/*
 * Converts a float64 timestamp to DateTime structure. Timestamp is assumed to be in UTC
 */
int datetime_from_timestamp_utc(DateTime* datetime, float64 timestamp) {
	if ( datetime == NULL ) {
		return swe_invalid_arguments;
	}
	struct timeval stdtime;
	struct tm isoctime;
	stdtime.tv_sec = (uint)timestamp;
	stdtime.tv_usec = 1e6*(timestamp - (uint)timestamp);
	isoctime = swe_localtime((const time_t*)&stdtime.tv_sec);
	datetime->year   = isoctime.tm_year + 1900;
	datetime->month  = isoctime.tm_mon + 1;
	datetime->day    = isoctime.tm_mday;
	datetime->hour   = isoctime.tm_hour;
	datetime->minute = isoctime.tm_min;
	datetime->second = isoctime.tm_sec;
	datetime->useconds = (int)stdtime.tv_usec;

 	// Get timezone //
	datetime->timezone = 0; // converts seconds to hours

	return swe_ok;
}



/*
 * Converts a float64 timestamp to DateTime structure. Timestamp is assumed to be in
 * system's timezone
 */
int datetime_from_timestamp(DateTime* datetime, float64 timestamp) {
	if ( datetime == NULL ) {
		return swe_invalid_arguments;
	}
	struct timeval stdtime;
	struct tm isoctime;
	stdtime.tv_sec = (uint)timestamp;
	stdtime.tv_usec = 1e6*(timestamp - (uint)timestamp);
	isoctime = swe_localtime((const time_t*)&stdtime.tv_sec);
	datetime->year   = isoctime.tm_year + 1900;
	datetime->month  = isoctime.tm_mon + 1;
	datetime->day    = isoctime.tm_mday;
	datetime->hour   = isoctime.tm_hour;
	datetime->minute = isoctime.tm_min;
	datetime->second = isoctime.tm_sec;
	datetime->useconds = (int)stdtime.tv_usec;

 	// Get timezone //
	datetime->timezone = get_timezone_seconds()/3600; // converts seconds to hours

	return swe_ok;
}


/*
 * Conerts from DateTime structure to a float64 timestamp (epoch)
 */
float64 datetime_to_timestamp(DateTime datetime) {
	struct tm isoctime;
	isoctime.tm_year = datetime.year  - 1900;
	isoctime.tm_mon = datetime.month  - 1;
	isoctime.tm_mday = datetime.day;
	isoctime.tm_hour = 	datetime.hour;
	isoctime.tm_min = datetime.minute;
	isoctime.tm_sec = datetime.second;
	return (float64)swe_mktime(&isoctime);
}




/*
 * Stores in buffer a datetime in ascii format:
 *    2017-10-20T13:30:00.123z
 *    or
 *    2017-10-20T13:30:00.123+00:01
 */
int ascii_datetime( char* buffer, float64 timestamp, uchar msecs){
	char temp[256];
	buffer[0] = 0;
	DateTime time;

	CHECK_NULL(buffer, swe_invalid_arguments);

	if (datetime_from_timestamp(&time, timestamp) != swe_ok) {
		return swe_timer_error;
	}

	sprintf(temp, "%04d-%02d-%02dT%02d:%02d:%02d",
			time.year,
			time.month,
			time.day,
			time.hour,
			time.minute,
			time.second);
	strcat(buffer, temp);

	if ( msecs ) {
		sprintf(temp, ".%03d", time.useconds / 1000 );
		strcat(buffer, temp);
	}

	if ( time.timezone == 0 ) {
		strcat(buffer, "z");
	}
	else if ( time.timezone > 0 ) {
		sprintf(temp, "+%02d:00", time.timezone);
		strcat(buffer, temp);
	} else {
		sprintf(temp, "%02d:00", time.timezone);
		strcat(buffer, temp);
	}
	return strlen(buffer);
}


/*
 * Stores in buffer a datetime in ascii format:
 *    2017-10-20T13:30:00.123z
 *    or
 *    2017-10-20T13:30:00.123+00:01
 *
 *    timestamp is in UTC (regardless of the system timezone)
 */
int ascii_datetime_utc( char* buffer, float64 timestamp){
	if ( buffer == NULL ) {
		return swe_invalid_arguments;
	}
	int chars;
	DateTime time;
	if (datetime_from_timestamp_utc(&time, timestamp) != swe_ok) {
		return swe_timer_error;
	}
	if ( time.timezone == 0 ) {
		chars=sprintf(buffer, "%04d-%02d-%02dT%02d:%02d:%02dZ",
			time.year,
			time.month,
			time.day,
			time.hour,
			time.minute,
			time.second);
	} else {
		chars=sprintf(buffer, "%04d-%02d-%02dT%02d:%02d:%02d+%02d:00",
			time.year,
			time.month,
			time.day,
			time.hour,
			time.minute,
			time.second,
			time.timezone);
	}
	return chars;
}


/*
 * This functions checks data integrity according to an ASCII float:
 *
 * numbers from 0..9 and one decimal separator '.' and '-'
 */
uchar check_ascii_data_integrity(uchar* data_in){
	char* data = (char*) data_in;
	while( data[0] != 0 ) { // Loop through the string
		char c = data[0];
		//fdmsg("checking \"%s\"", data);
		if ( ( (c >= '0' ) && (c <= '9') ) || c == '.' || c == '-' || c == '+') {
			data++;
		} else {
			errmsg("invalid char found! '%c' (%#02x)", c, (int)c);

			return FALSE;
		}
	}
	return TRUE;
}


/*
 * This functions takes a ascii string and removes all white spaces at the beginning and the ending of the
 * string. The input string is modified by this function.s
 */
int collapse_white_spaces(char* buffer, uint bufflen){
	if (buffer == NULL) {
		return swe_invalid_arguments;
	}
	else if(buffer[0] == 0) {
		warnmsg("collapse_white_spaces received an empty string");
		return swe_ok;
	}
	char temp[bufflen];
	uchar newbuffsize;
	memset(temp, 0, bufflen);
	int i=0, offset=0;
	// Copy the beginning of the string
	while(buffer[offset] == ' ' || buffer[offset] == '\t' || buffer[offset] == '\r' || buffer[offset] == '\n'){
		offset++; // skip spaces at the beginning
	}

	// copy from i to the end of the string
	while(buffer[offset+i] != 0) {
		temp[i] = buffer[offset+i];
		i++;
	}

	// set the last element to 0
	temp[i]=0;
	newbuffsize = i;
	i--; // starting with the last char
	while(temp[i] == ' ' || temp[i] == '\t' || temp[i] == '\r' || temp[i] == '\n'){
		temp[i] = 0; // set 0 to spaces
		i--; // skip spaces at the beginning
	}
	memcpy(buffer, temp, newbuffsize);
	buffer[newbuffsize] = 0;
	// copy temporal buffer to input buffer
	return swe_ok;
}

/*
 * This functions takes a ascii string and removes all white spaces in a string
 */
int collapse_white_spaces2(char* string, uint bufflen){
	int i, j;
	int len = strlen(string);
	char b[bufflen];
	memset(b, 0, bufflen);

	j = 0;
	for ( i=0 ; i<len ; i++){
		if (string[i] == ' ' || string[i] == '\t' || string[i] == '\r' || string[i] == '\n'){
			// skip
		} else{
			b[j] = string[i];
			j++;
		}
	}
	strcpy(string, b);
	return swe_ok;
}



/*
 * This functions takes a string as input and removes everything that is a not vlaid number:
 * "Temp: 25ºC" -----> "2"5
 * "Value=-43.123 mV" -----> "43.123"
 * "123a+x43" ---> error!
 *
 * Returns a positive value if success, -1 on error
 */
int check_string_for_ascii_quantity(char *string){
	int i,j=0;
	uchar numFlag = 0, charFlag = 0, dotFlag = 0;

    for(i=0; string[i] ;i++){
    	if(string[i] == '-'){
            // If already collecting chars numbers cannot have other sign //
    		if(numFlag){
    			j=0;
    			break;
    		}
    	}
    	if(string[i] == '.'){
            // Dot only after char number //
			if(!numFlag){
				j=0;
				break;
			}
            // If already dot cannot have other dot //
			if(dotFlag){
				j=0;
				break;
			}
			dotFlag = 1;
		}
        if((string[i] >= '0' && string[i] <= '9')||(string[i] == '.')||(string[i] == '-')){
            // If already collecting chars numbers cannot have other chars. //
        	if(charFlag){
        		j=0;
        		break;
        	}
        	numFlag = 1;
        	string[j] = string[i];
            j++;
        }else{
            // If already collecting chars numbers cannot have other chars. //
        	if(numFlag)
        		charFlag = 1;
        }
    }
    // Cannot end with dot //
    if(string[j-1] == '.'){
    	j=0;
    }
    string[j] = '\0';

    if ( j== 0) {
    	return -1;
    }

    return j;
}

//-----------------------------------------------------------------//
//------------------------ Type Manipulation ----------------------//
//-----------------------------------------------------------------//

/*
 * Writes a int32 integer to a buffer in little endian byte order
 * the buffer is expected to have at least 4 bytes
 */
int int32_to_little_endian(int32_t value, uchar* buff){
	buff[0]  = (uint8_t)(value);
	buff[1]  = (uint8_t)(value >> 8);
	buff[2]  = (uint8_t)(value >> 16);
	buff[3]  = (uint8_t)(value >> 24);
	return swe_ok;
}

/*
 * Writes a int32 integer to a buffer in big endian byte order
 * the buffer is expected to have at least 4 bytes
 */
int int32_to_big_endian(int32_t value, uchar* buff){
	buff[0]  = (uint8_t)((value >> 24) & 0xFF);
	buff[1]  = (uint8_t)((value >> 16) & 0xFF);
	buff[2]  = (uint8_t)((value >> 8) & 0xFF);
	buff[3]  = (uint8_t)((value & 0xFF));
	return swe_ok;
}

/*
 * Convert an integer to a 4 bytes "synchsafe" ID3 integer
 * synchsafe is a strange 7 bit integer...
 */
int32_t to_synchsafe(int32_t in) {
	int32_t out, mask = 0x7F;

	while (mask ^ 0x7FFFFFFF) {
		out = in & ~mask;
		out <<= 1;
		out |= in & mask;
		mask = ((mask + 1) << 8) - 1;
		in = out;
	}
	return out;
}




//-----------------------------------------------------------------//
//------------------------ MATH MACROS/FUNCTIONS ------------------//
//-----------------------------------------------------------------//
/*
 * Returns the greatest common divisor
 */
ulong greatest_common_divisor(ulong a, ulong b)
{
	long int m, n;
    int r;

	if(a<b){
		m=b;
		n=a;
	}
	else{
		m=a;
		n=b;
	}

    if(m == 0 && n == 0) {
    	errmsg("Can't calculate greatest common divisor of 0!");
        return 0;
    }

    if(m < 0) m = -m;
    if(n < 0) n = -n;

    while(n) {
        r = m % n;
        m = n;
        n = r;
    }
    return m;
}



//-----------------------------------------------------------------//
//------------------------ OTHERS FUNCTIONS -----------------------//
//-----------------------------------------------------------------//

static char base64_encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};


static int base64_mod_table[] = {0, 2, 1};


/*
 * This function encodes an array of bytes into Base64
 * WARNING: This function returns allocated memory
 */
char *base64_encode(const uchar *data, uint input_length, uint *output_length) {
	int i, j;
    *output_length = 4 * ((input_length + 2) / 3);

    char *encoded_data = swe_malloc(*output_length);
    if (encoded_data == NULL) return NULL;

    for (i = 0, j = 0; i < input_length;) {

        uint32_t octet_a = i < input_length ? (uchar)data[i++] : 0;
        uint32_t octet_b = i < input_length ? (uchar)data[i++] : 0;
        uint32_t octet_c = i < input_length ? (uchar)data[i++] : 0;

        uint32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;

        encoded_data[j++] = base64_encoding_table[(triple >> 3 * 6) & 0x3F];
        encoded_data[j++] = base64_encoding_table[(triple >> 2 * 6) & 0x3F];
        encoded_data[j++] = base64_encoding_table[(triple >> 1 * 6) & 0x3F];
        encoded_data[j++] = base64_encoding_table[(triple >> 0 * 6) & 0x3F];
    }

    for (i = 0; i < base64_mod_table[input_length % 3]; i++)
        encoded_data[*output_length - 1 - i] = '=';

    return encoded_data;
}

/*
 * This function decodes base64 into a binary array
 * WARNING: This function returns allocated memory
 */
uchar *base64_decode(const char *data, uint *output_length) {
	int i, j;
	if (data == NULL || output_length == NULL){
		errmsg("NULL pointer received");
		return NULL;
	}

	// Build decoding table //
	char *decoding_table = swe_malloc(256);
	uint input_length = strlen(data);
    for ( i = 0; i < 64; i++)
        decoding_table[(uchar) base64_encoding_table[i]] = i;

    // Decode //
    if (input_length % 4 != 0) {
    	swe_free(decoding_table);
    	return NULL;
    }

    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=') (*output_length)--;
    if (data[input_length - 2] == '=') (*output_length)--;

    uchar *decoded_data = swe_malloc(*output_length);
    if (decoded_data == NULL) {
    	swe_free(decoding_table);
    	return NULL;
    }

    for (i = 0, j = 0; i < input_length;) {

    	uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[(uint32_t)data[i++]];
    	uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[(uint32_t)data[i++]];
    	uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[(uint32_t)data[i++]];
    	uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[(uint32_t)data[i++]];

    	uint32_t triple = (sextet_a << 3 * 6)
        + (sextet_b << 2 * 6)
        + (sextet_c << 1 * 6)
        + (sextet_d << 0 * 6);

        if (j < *output_length) decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }

    return decoded_data;
}






/*
 * Generates the MD5 code for an input file
 */
int generate_md5_code(char* filename, char* output_buff, int buff_len){
	swe_file* file; /* File object */
	uchar buffer[MD5_DIGEST_LENGTH];
	MD5_CTX mdContext;
	dmsg( "start md5");
	dmsg( "filename %s", filename);
	if((file=swe_fopen(filename, swe_read_file))==NULL){
		errmsg( "couldn't read file \"%s\"", filename);
		return swe_filesystem_error;
	}

	MD5_Init (&mdContext);

	int nbytes=1;
	while (nbytes>0) {
	/* Read a chunk of file */
		nbytes=swe_fread(buffer, MD5_DIGEST_LENGTH*sizeof(uchar), file);

		if(nbytes<0){
			errmsg( "error in swe_fread, returned %d ", nbytes);
			return swe_error;
		}
		MD5_Update (&mdContext, buffer, nbytes);
	}
	/* Close open files */
	if(swe_fclose(file)!= swe_ok){
		errmsg( "error closing file");
		return swe_error;
	}
	MD5_Final ((uchar*)output_buff,&mdContext);
	sprintf((char*) output_buff, "%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
			buffer[0],buffer[1],buffer[2],buffer[3],buffer[4],buffer[5],buffer[6],buffer[7],buffer[8],
			buffer[9],buffer[10],buffer[11],buffer[12],buffer[13],buffer[14],buffer[15]);
	dmsg( "%s", output_buff);
	return swe_ok;
}


//-----------------------------------------------------------------//
//------------------- SYSTEM COMPATIBILITY CHECK ------------------//
//-----------------------------------------------------------------//

/*
 * test MACROS
 */
#define SWE_BRIDGE_TEST_FILE "swe_bridge_test.xml"
#define SWE_BRIDGE_TEST_FILE_PATH SWE_BRIDGE_TEST_FOLDER PATH_SEPARATOR SWE_BRIDGE_TEST_FILE // concatenate the path

#define TEST_FUNC(func) do { \
	int __temp_error = func;\
	if (__temp_error < 0) { \
		errmsg("code %d: \"%s\" at %s, line %d",__temp_error,  swe_error_strings[abs(__temp_error)], __FILE__, __LINE__);\
	} else {\
		cimsg(GRN, "OK"); \
	} \
} while(0)

#define TEST_NULL_FUNC(func) do { \
	if ( (func) == NULL) { \
		errmsg("returned NULL pointer");\
	} else {\
		cimsg(GRN, "OK"); \
	} \
} while(0)


uint dummycount = 0;
void test_timer_handler(){
	dummycount++;
	dmsgn(".");
}

/*
 * System compatibility check will try to access to all the resources that the SWE Bridge
 *  needs and use them. This function can help to detect possible configuration or
 *  implementation errors
 */
int system_compatibility_check(){
	// const char* folder = SWE_BRIDGE_TEST_FOLDER;
	const char* filename1 = SWE_BRIDGE_TEST_FOLDER PATH_SEPARATOR "temp_file.txt";
	const char* filename2 = SWE_BRIDGE_TEST_FOLDER PATH_SEPARATOR "hola-quetal_180312_232323.txt";
	const char *test1 = "glider\n" "profiler\n" "banana\n";
	const char *test2 = "obsea\n" "Kentucky\n";
	const char* totaltest="glider\n" "profiler\n" "banana\n" "obsea\n" "kentucky\n";
	swe_bridge_set_status(system_check_option, NULL);
	msg_title(BLU, "System Compatibility Check");
	cimsgn( NRM,"version: ");cimsg( GRN,"%s", (char*)SOFT_VERSION);
	cimsgn( NRM,"Compilation date: "); cimsg( GRN,"%s", (char*)COMPILE_DATE);
	imsg("Compiled for platform \"%s\"", PLATFORM_NAME);

	imsg("\nChecking all the resources required for the execution of the SWE Bridge");
	int i=0;

	//---------- FILESSYTEM --------//
	msg_sub_title(CYN, "Checking File System");
	imsgn("Creating folder %s");
	TEST_FUNC(swe_create_dir(SWE_BRIDGE_TEST_FOLDER));
	imsgn("Creating file \%s\" in write mode...", filename1);
	swe_file *file;
	TEST_NULL_FUNC(file=swe_fopen(filename1, swe_write_file));
	imsgn("Writing...");
	TEST_FUNC(swe_fwrite(test1, strlen(test1), file));
	imsgn("Closing...");
	TEST_FUNC(swe_fclose(file));
	imsgn("Opening in append mode...");
	TEST_NULL_FUNC(file=swe_fopen(filename1, swe_append_file));
	imsgn("Writing again...");
	TEST_FUNC(swe_fwrite(test2, strlen(test2), file));
	imsgn("Closing...");
	TEST_FUNC(swe_fclose(file));
	imsgn("Moving file to %s...", filename2);
	TEST_FUNC(swe_fmove(filename1, filename2));
	imsgn("Opening in read mode...");
	TEST_NULL_FUNC(file=swe_fopen(filename2, swe_read_file));

	imsgn("Getting the file size...");
	uint size = 0;
	TEST_FUNC(size=swe_file_size(file));
	if (size != strlen(totaltest)) {
		errmsg("File size does not match! it is %d but should be %d", size, strlen(totaltest));
	}
	imsg("file size is %d (as expected)", size);
	char c;
	int f;
	imsgn("swe_eof...");
	TEST_FUNC(f=swe_eof(file));

	if (f == TRUE ) {
		errmsg("Wrong EOF detected!");
	}
	imsgn("swe_getc...");
	dmsg("Reading 10 chars");
	for (i = 0; i<10; i++) {
		TEST_FUNC(c=swe_getc(file));
	}
	imsgn("swe_eof again...");
	TEST_FUNC(f=swe_eof(file));
	if (f == TRUE ) {
		errmsg("Wrong EOF detected!");
	}
	dmsg("Reading to the end of the file");
	uchar end = FALSE;
	while (!end) {
		c=swe_getc(file);
		end=swe_eof(file);
	}
	imsgn("Closing...");
	TEST_FUNC(swe_fclose(file));

	//---------- MEMORY --------//
	msg_sub_title(CYN, "Checking Memory");
	imsgn("Allocating 10 kBytes...");
	char* hola;
	TEST_NULL_FUNC((hola=swe_malloc((1024*10))));
	imsgn("memcpy...");
	TEST_NULL_FUNC(memcpy(hola, "123456789", 9));
	imsgn("realloc 20 kBytes...");
	TEST_NULL_FUNC(hola = swe_realloc(hola, 20*1024));
	imsgn("memcmp...");
	TEST_FUNC(memcmp(hola, "123456789", 9));
	imsgn("free...");
	swe_free(hola);
	cdmsg(GRN, "OK");

	//---------- TIMER --------//
	msg_sub_title(CYN, "Checking Timer");
	imsg("Getting date time...");
	float64 timestamp;
	char date_str[64];
	timestamp = swe_get_epoch_time();
	imsg("Epoch time %f", timestamp);
	TEST_FUNC(ascii_datetime(date_str, timestamp, TRUE));
	imsg("date time %s", date_str);
	imsg("sleep 471 msecs");
	delayms(471);
	timestamp = swe_get_epoch_time();
	imsg("Epoch time %f", timestamp);
	TEST_FUNC(ascii_datetime(date_str, timestamp, TRUE));
	imsg("date time %s", date_str);



	ulong usecs = 1000000;
	imsgn("We should be locked here for 3 secs (without timer)...");
	i=0;
	while(i < 3) {
		delayms(1000);
		dmsg("ticks %u", dummycount);
		i++;
	}
	cimsg(GRN, "OK");
	imsgn("Setting a timer to %u second...", usecs/1000000 );
	TEST_FUNC(swe_set_timer(usecs, test_timer_handler));
	imsgn("Starting the timer...");
	TEST_FUNC(swe_start_timer());
	imsgn("We should be locked here for 3 secs...");
	while(dummycount < 3*(usecs/1000000)) {

	}
	cimsg(GRN, "OK");
	imsgn("Stopping the timer...");
	TEST_FUNC(swe_stop_timer());
	imsg("");
	imsg("NOTE: The communications interfaces have not been tested!!");
	return swe_ok;
}
