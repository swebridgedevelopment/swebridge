/*
 * types_manipulation.c
 *
 *  Created on: Feb 27, 2018
 *      Author: enoc
 */


#include "common/sensorml.h"
#include "common/swe_data_model.h"
#include "common/swe_utils.h"
#include "resources/mem_control.h"
#include "swe_conf.h"


int link_swe_field(Field** list, Field* field);

/*
 * Inherit "from" properties to "to"
 */
int inherit_input(Input* to, Input* from);
int inherit_output(Output* to, Output* from);
int inherit_parameter(Parameter* to, Parameter* from);
// Free Functions //
// int free_physical_system(PhysicalSystem* ps);
int free_input(Input* i);
int free_output(Output* i);
int free_parameter(Parameter* i);

/*
 * This macro checks if a field needs to be linked
 */
#define NEEDS_TO_BE_LINKED(_field) ({ \
	uchar _needs_to_be_linked = FALSE; \
	if(_field->href != NULL && _field->href[0]=='#') _needs_to_be_linked = TRUE ; \
	_needs_to_be_linked; \
})


/*
 * Create a new SimpleProcess. The argument is the parent phyisical system
 */
SimpleProcess* simple_process_create(PhysicalSystem* ps) {
	SimpleProcess* process = swe_malloc(sizeof(SimpleProcess));
	process->physical_system = (void*)ps;
	return process;
}


/*
 * Takes a list of parameters and a list of possible definitions. If any parameters children
 * matches a definition, return parameter
 */
Parameter* get_parameter_by_definition(Parameter** parameters, const char* definitions[]) {
	int i, j;
	CHECK_NULL(parameters, NULL);
	CHECK_NULL(definitions, NULL);

	// Loop through parameter list
	for ( i=0 ; parameters[i]!=NULL; i++ ) {
		Field* field = parameters[i]->field;
		if ( !check_if_basic(field->type)) {
			warnmsg("Not implemented for complex types!");
			continue;
		}
		const char* field_definition = field_component_definition_get(field);
		if (field_definition == NULL) {
			continue;
		}
		for ( j=0 ; definitions[j]!=NULL ; j++ ) {
			if (!strcmp(field_definition, definitions[j])) {
				return parameters[i];
			}
		}
	}
	errmsg("Could not find parameter %s", definitions[0]);
	return NULL;
}


/*
 * Takes a list of capabilties and a list of possible definitions. If any parameters children
 * matches a definition, return parameter
 */
Capability* get_capability_by_definition(Capability** capabilties, const char* definitions[]) {
	int i, j;
	if (capabilties == NULL || definitions == NULL ){
		errmsg("Invalid arguments");
		return NULL;
	}
	// Loop through parameter list
	for ( i=0 ; capabilties[i]!=NULL; i++ ) {
		Field* field = capabilties[i]->field;
		if ( !check_if_basic(field->type)) {
			warnmsg("Not implemented for complex types!");
			continue;
		}
		const char* field_definition = field_component_definition_get(field);
		if (field_definition == NULL) {
			continue;
		}
		for ( j=0 ; definitions[j]!=NULL ; j++ ) {
			if (!strcmp(field_definition, definitions[j])) {
				return capabilties[i];
			}
		}
	}
	return NULL;
}




/*
 * This function searches for SensorML elements that reference others. Fields
 * with title starting with # are reference to other fields (reference to id)
 *
 * ...
 * <swe:elementCount xlink:href="#arrayCount"/>
 * ...
 * <sml:parameter name="arrayCountParameter" >
 *     <swe:Count id="arrayCount">
 *         <swe:value>10</swe:value>
 *     </swe:Count>
 * </sml:parameter>
 *
 */
int link_referenced_fields(PhysicalSystem* system){
	int i, j;
	imsg("Looking for fields that need to be linked...");
	// Loop through all SimpleProcesses //
	for (i=0; system->processes[i] != NULL ; i++) {
		SimpleProcess* process = system->processes[i];
		if (process->field_list == NULL) {
			dmsg("Process %s does not have any field, ignoring", process->name);
			continue;
		}
		cimsg(GRN, "SimpleProcess %s", process->name);
		print_field_list(process->field_list);

		// Loop through the Field List
		dmsg("Looking for fields to be linked");
		for (j=0; process->field_list[j] != NULL ; j++ ){
			Field* field = process->field_list[j];
			dmsg("Field %d name %s, href %s", j, field->name, field->href);
			if (NEEDS_TO_BE_LINKED(field)) {
				cdmsg(GRN, "Looking for href %s...", field->href);
				TRY_RET(link_swe_field(process->field_list, field));
			}
		}
	}
	return swe_ok;
}

/*
 * Checks if a list of PhysicalSystems contains non-relevant elements and
 * deletes them
 */
PhysicalSystem** clear_unused_physical_systems(PhysicalSystem** list) {
	int i;
	imsg("checking for unused PhysicalSystems...");
	// Check arguments
	if (list == NULL) {
		warnmsg(" %s - List is empty!", __func__);
		return NULL;
	}

	// Loop through PhysicalSystems
	for ( i=0 ; list[i] != NULL ; i++ ){
		// If PhysicalSystem doesn't have any processes, erase it
		if(list[i]->processes == NULL) {
			imsg("removing PhysicalSystem %d (%s)", i, list[i]->id);
			list =remove_physical_system(list, i) ;
			i--; // do not skip next (the list length has been reduced)
		}
		else {
			imsg("Keeping PhysicalSystem %d (%s)", i, list[i]->id);
		}
	}
	return list;
}

/*
 * Removes a PhysicalSystem from a list
 */
PhysicalSystem** remove_physical_system(PhysicalSystem** list, int index){
	PhysicalSystem* toberemoved = list[index];
	int i, j;
	int len = ARRAY_LENGTH(list);
	PhysicalSystem **result = swe_malloc(sizeof(PhysicalSystem*)*(len-1));
	for (i=0; i < len ; i++) {
		if (i != index) {
			result[j] = list[i];
			j++;
		}
	}
	free_physical_system(toberemoved);
	return result;
}

/*
 * Takes a swe field which has to be linked and searches from a field list if
 * any of them is referenced. If a match is found, it is linked
 */
int link_swe_field(Field** list, Field* field){
	int i=0;
	char* target = field->href + 1; //skip the # char
	for (i=0; list[i]!=NULL; i++ ) {
		Field* currentfield = list[i];
		char* id = get_field_id(currentfield);
		if (id != NULL && !compare_strings(id, target)){
			cdmsg(GRN,"Found field %s", currentfield->title);
			field->component = currentfield->component;
			field->type = currentfield->type;
			field->linked = TRUE;
			return swe_ok;
		}
	}
	errmsg("Could not find field %s!", target);
	return swe_element_not_found;
}


/*
 * Inherit all inputs from one process to another.
 */
int inherit_inputs(SimpleProcess *process_to, SimpleProcess *process_from){
	int i;
	int nfrom = ARRAY_LENGTH(process_from->inputs);
	int nto = ARRAY_LENGTH(process_to->inputs);

	if ( nfrom == 0 ) {
		// nothing to inherit
		return swe_ok;
	}

	// Allocate memory
	if ( nto == 0 && nfrom > 0 ){
		process_to->inputs = swe_malloc((nfrom + 1) * sizeof(Input*));
	}


	for (i=0; process_from->inputs[i] != NULL ; i++ ){
		if ( process_to->inputs[i] == NULL) {
			process_to->inputs[i] = swe_malloc(sizeof(Input));
		}
		TRY_RET(inherit_input(process_to->inputs[i], process_from->inputs[i]));
	}
	return swe_ok;
}


/*
 * Inherit all outputs from one process to another.
 */
int inherit_outputs(SimpleProcess *process_to, SimpleProcess *process_from){
	int i;
	int nfrom = ARRAY_LENGTH(process_from->outputs);
	int nto = ARRAY_LENGTH(process_to->outputs);

	if ( nfrom == 0 ) {
		// nothing to inherit
		return swe_ok;
	}

	// Allocate memory
	if ( nto == 0 && nfrom > 0 ){
		process_to->outputs = swe_malloc((nfrom + 1) * sizeof(Output*));
	}


	for (i=0; process_from->outputs[i] != NULL ; i++ ){
		if ( process_to->outputs[i] == NULL) {
			process_to->outputs[i] = swe_malloc(sizeof(Output));
		}
		TRY_RET(inherit_output(process_to->outputs[i], process_from->outputs[i]));
	}
	return swe_ok;
}

/*
 * Inherit all parameters from one process to another.
 */
int inherit_parameters(SimpleProcess *process_to, SimpleProcess *process_from){
	int i;
	int nfrom = ARRAY_LENGTH(process_from->parameters);
	int nto = ARRAY_LENGTH(process_to->parameters);

	if ( nfrom == 0 ) {
		// nothing to inherit
		return swe_ok;
	}

	// Allocate memory
	if ( nto == 0 && nfrom > 0 ){
		process_to->parameters = swe_malloc((nfrom + 1) * sizeof(Parameter*));
	}


	for (i=0; process_from->parameters[i] != NULL ; i++ ){
		if ( process_to->parameters[i] == NULL) {
			process_to->parameters[i] = swe_malloc(sizeof(Parameter));
		}
		TRY_RET(inherit_parameter(process_to->parameters[i], process_from->parameters[i]));
	}
	return swe_ok;
}


/*
 * Inherit "from" properties to "to"
 */
int inherit_input(Input* to, Input* from){
	if ( to == NULL || from == NULL) {
		errmsg("NULL pointer");
		return swe_invalid_arguments;
	}
	INHERIT_STRING(to, from, title);
	INHERIT_STRING(to, from, name);
	INHERIT_STRING(to, from, href);


	if ( from->field != NULL ) {
		if ( to->field == NULL ) {
			to->field = swe_malloc(sizeof(Field));
		}
		TRY_RET(inherit_field(to->field, from->field));
	}

	return swe_ok;
}

/*
 * Inherit output
 */
int inherit_output(Output* to, Output* from) {
	return inherit_input((Input*)to, (Input*)from);
}

/*
 * Inherit output
 */
int inherit_parameter(Parameter* to, Parameter* from) {
	return inherit_input((Input*)to, (Input*)from);
}


/******************************************************************************
 *                       Free Functions
 *****************************************************************************/
/*
 * Free Data Interface
 */
int free_data_interface(DataInterface* di) {
	int i;

	swe_free(di->id);

	for ( i=0 ; di->fields[i]!=NULL ; i++ ){
		free_field(di->fields[i]);
	}

	swe_free(di->fields);
	swe_free(di);
	return swe_ok;
}

int free_simple_process(SimpleProcess *sp){
	int i;
	swe_free(sp->typeOf);
	swe_free(sp->uid);
	swe_free(sp->name);


	if (sp->inputs != NULL) {
		for ( i=0 ; sp->inputs[i]!=NULL ; i++) free_input(sp->inputs[i]);
		swe_free(sp->inputs);

	}

	if (sp->outputs != NULL) {
		for ( i=0 ; sp->outputs[i]!=NULL ; i++) free_output(sp->outputs[i]);
		swe_free(sp->outputs);
	}


	if (sp->parameters != NULL) {
		for ( i=0 ; sp->parameters[i]!=NULL ; i++) free_parameter(sp->parameters[i]);
		swe_free(sp->parameters);

	}

	if (sp->settings != NULL) {
		for (i= 0; sp->settings[i]!=NULL ; i++) {
			swe_free(sp->settings[i]->value);
			swe_free(sp->settings[i]->ref);
			swe_free(sp->settings[i]);
		}
		swe_free(sp->settings);
	}
   // field_list already cleared
	if ( ! sp->linked_fields ) {
		swe_free(sp->field_list);
	}
	swe_free(sp);

	return swe_ok;
}


int free_physical_system(PhysicalSystem* ps) {
	int i;
	swe_free(ps->id);
	swe_free(ps->uid);
	swe_free(ps->feature_of_interest_href);
	swe_free(ps->feature_of_interest_title);
	swe_free(ps->position);


	if (ps->interface_conf != NULL) free_data_interface(ps->interface_conf);
	for ( i=0 ; ps->processes[i]!=NULL ; i++ ){
		free_simple_process(ps->processes[i]);
	}
	swe_free(ps->processes);
	swe_free(ps);
	return swe_ok;
}


int free_aggregate_process(AggregateProcess* ap) {
	int i;
	swe_free(ap->uid);
	swe_free(ap->id);

	for (i=0 ; ap->processes[i]!=NULL; i++) {
		free_simple_process(ap->processes[i]);
	}
	swe_free(ap->processes);


	if (ap->connections != NULL ){
		for (i=0 ; ap->connections[i]!=NULL ; i++) {
			swe_free(ap->connections[i]->source);
			swe_free(ap->connections[i]->destination);
			swe_free(ap->connections[i]);
		}
		swe_free(ap->connections);

	}

	if (ap->instrument != NULL ){
		free_physical_system(ap->instrument);
	}

	swe_free(ap);



	return swe_ok;
}


int free_input(Input* i) {
	swe_free(i->title);
	swe_free(i->name);
	swe_free(i->href);
	free_field(i->field);
	swe_free(i);
	return  swe_ok;
}

int free_output(Output* i) {
	return free_input((Input*)i);
}

int free_parameter(Parameter* i) {
	return free_input((Input*)i);
}

/******************************************************************************
 *                       SHOW SensorML Functions
 *****************************************************************************/

/*
 * Macro that prints spaces according to the representation level
 */
#define PRINT_SPACES(level) do{ \
		int _index=0; \
		for (_index=0; _index<level; _index++) {imsgn("|---");} \
	}while(0)


#define PRINT_ATTRIBUTE(field, name, level) if(field->name != NULL ) ({ \
	PRINT_SPACES(level); \
	imsgn("%s=\"", #name); \
	cimsgn(BLU, "%s", field->name);\
	imsg("\"");\
})

int print_input( Input* i, int level){
	PRINT_SPACES(level);
	cimsg(GRN, "Input");
	level++;
	PRINT_ATTRIBUTE(i, name, level);
	PRINT_ATTRIBUTE(i, title, level);
	PRINT_ATTRIBUTE(i, href, level);
	if(i->field != NULL) print_data_structure(i->field, level);
	return swe_ok;
}

int print_output( Output* o, int level){
	PRINT_SPACES(level);
	cimsg(GRN, "Output");
	level++;
	PRINT_ATTRIBUTE(o, name, level);
	PRINT_ATTRIBUTE(o, title, level);
	PRINT_ATTRIBUTE(o, href, level);
	if(o->field != NULL) print_data_structure(o->field, level);
	return swe_ok;
}

int print_parameter( Parameter* o, int level){
	PRINT_SPACES(level);
	cimsg(GRN, "Parameter");
	level++;
	PRINT_ATTRIBUTE(o, name, level);
	PRINT_ATTRIBUTE(o, title, level);
	PRINT_ATTRIBUTE(o, href, level);
	if(o->field != NULL) print_data_structure(o->field, level);
	return swe_ok;
}

int print_capability( Capability* o, int level){
	PRINT_SPACES(level);
	cimsg(GRN, "Capability");
	level++;
	PRINT_ATTRIBUTE(o, name, level);
	PRINT_ATTRIBUTE(o, title, level);
	PRINT_ATTRIBUTE(o, href, level);
	if(o->field != NULL) print_data_structure(o->field, level);
	dmsg("");
	return swe_ok;
}

int print_data_interface(DataInterface* iface, int level){
	int i;
	PRINT_SPACES(level);
	cimsg(GRN, "Interface");
	for (i=0; iface->fields[i]!=NULL; i++){
		print_data_structure(iface->fields[i], level+1);
	}
	return swe_ok;
}

int print_simple_process(SimpleProcess* s, int level){
	int i;
	PRINT_SPACES(level);
	cimsg(GRN, "SimpleProcess");
	level++;
	// Print Attributes //

	PRINT_ATTRIBUTE(s, typeOf, level);
	PRINT_ATTRIBUTE(s, uid, level);
	PRINT_ATTRIBUTE(s, name, level);
	// Print Inputs //
	if (s->inputs != NULL) {
		for (i=0 ; s->inputs[i]!=NULL ; i++) {
			print_input(s->inputs[i], level);
		}
	}
	// Print Outputs //
	if (s->outputs != NULL) {
		for (i=0 ; s->outputs[i]!=NULL ; i++) {
			print_output(s->outputs[i], level);
		}
	}
	// Print Parameters //
	if (s->parameters != NULL) {
		for (i=0 ; s->parameters[i]!=NULL ; i++) {
			print_parameter(s->parameters[i], level);
		}
	}

	// Print Settings //
	if (s->settings != NULL) {
		PRINT_SPACES(level);
		cimsg(CYN, "Settings");
		level++;
		for (i=0 ; s->settings[i]!=NULL ; i++) {
			Setting* set = s->settings[i];
			PRINT_ATTRIBUTE(set, ref, level);
			PRINT_ATTRIBUTE(set, value, level);
		}
	}
	return swe_ok;
}

/*
 * Print Connection
 */
int print_connection(Connection* c, int level){
	PRINT_SPACES(level);
	cimsg(CYN, "Connection");
	level++;
	PRINT_SPACES(level);
	imsgn("source=\"");
	cimsgn(BLU,"%s", c->source);
	imsg("\"");
	PRINT_SPACES(level);
	imsgn("destination=\"");
	cimsgn(BLU,"%s", c->destination);
	imsg("\"");

	return swe_ok;
}

/*
 * Shows a PhysicalSystem
 */
int print_physical_system(PhysicalSystem* p){
	int level=0, i;
	cimsg(GRN, "PhysicalSystem");
	level++;
	PRINT_ATTRIBUTE(p, id, level);
	PRINT_ATTRIBUTE(p, uid, level);

	if ( p->identifiers != NULL ) {
		for ( i=0 ; p->identifiers[i]!=NULL ; i++ ) {
			Identifier* iden = p->identifiers[i];
			PRINT_SPACES(level);
			cimsg(GRN, "Identifier");
			level++;
			PRINT_SPACES(level);
			imsgn("  definition=\""); cimsgn(MAG, "%s",  iden->definition); imsg("\"");
			PRINT_SPACES(level);
			imsgn("label=\""); cimsgn(BLU, "%s",  iden->label); imsg("\"");
			PRINT_SPACES(level);
			imsgn("value=\""); cimsgn(BLU, "%s",  iden->value); imsg("\"");
			level--;
		}
	}

	if ( p->classifiers != NULL ) {
		for ( i=0 ; p->classifiers[i]!=NULL ; i++ ) {
			Classifier* class = p->classifiers[i];
			PRINT_SPACES(level);
			cimsg(GRN, "Classifier");
			level++;
			PRINT_SPACES(level);
			imsgn("  definition=\""); cimsgn(MAG, "%s",  class->definition); imsg("\"");
			PRINT_SPACES(level);
			imsgn("label=\""); cimsgn(BLU, "%s",  class->label); imsg("\"");
			PRINT_SPACES(level);
			imsgn("value=\""); cimsgn(BLU, "%s",  class->value); imsg("\"");
			level--;
		}
	}

	if ( p->parameters != NULL ) {
		for ( i=0 ; p->parameters[i]!=NULL ; i++ ) {
			level++;
			print_parameter(p->parameters[i], level);
			level--;
		}
	}

	if ( p->capabilities != NULL ) {
		for ( i=0 ; p->capabilities[i]!=NULL ; i++ ) {
			level++;
			print_capability(p->capabilities[i], level);
			level--;
		}
	}

	if ( p->feature_of_interest_href  != NULL || p->feature_of_interest_title != NULL) {
		PRINT_SPACES(level);
		cimsg(GRN, "Feature of Interest");
		level++;
		if (p->feature_of_interest_href != NULL ){
			PRINT_SPACES(level);
			imsgn("ref=\""); \
			cimsgn(BLU, "%s",p->feature_of_interest_href);\
		}
		if (p->feature_of_interest_title != NULL ){
			PRINT_SPACES(level);
			imsgn("title=\""); \
			cimsgn(BLU, "%s",p->feature_of_interest_title);\
			imsg("\"");
		}
		level--;
	}

	if (p->attached_to != NULL ) {
		PRINT_SPACES(level);
		cimsgn(GRN, "attachedTo");
		imsgn(" title=\"");
		cimsgn(MAG, "%s",p->attached_to->title);
		imsgn("\"  href=\"");
		cimsgn(MAG, "%s",p->attached_to->href);
		imsg("\"");

	}

	if (p->position != NULL) {
		PRINT_SPACES(level);
		cimsg(GRN, "Position");
		level++;
		PRINT_SPACES(level);
		imsgn("latitude=\"");
		cimsgn(BLU, "%f",p->position->latitude);
		imsg("\"");
		PRINT_SPACES(level);
		imsgn("longitude=\"");
		cimsgn(BLU, "%f",p->position->longitude);
		imsg("\"");
		PRINT_SPACES(level);
		imsgn("depth=\"");
		cimsgn(BLU, "%f",p->position->depth);
		imsg("\"");



		level--;
	}
	if (p->interface_conf != NULL) {
		print_data_interface(p->interface_conf, level);
	}

	if (p->processes != NULL) {

		for (i=0; p->processes[i]!= NULL ; i++) {
			print_simple_process(p->processes[i], level);
		}
	}
	imsg("\n");
	return swe_ok;
}


/*
 * Shows an AggregatProcess
 */
int print_aggregate_process(AggregateProcess* a){
	int level=0, i;
	if (a == NULL) {
		return swe_invalid_arguments;
	}
	cimsg(GRN, "AggregateProcess");
	level++;
	PRINT_ATTRIBUTE(a, id, level);
	PRINT_ATTRIBUTE(a, uid, level);
	if (a->processes != NULL) {
		for (i=0; a->processes[i]!= NULL ; i++) {
			print_simple_process(a->processes[i], level);
		}
	}
	if (a->connections != NULL) {
		for (i=0; a->connections[i]!= NULL ; i++) {
			print_connection(a->connections[i], level);
		}
	}
	imsg("\n");
	return swe_ok;
}


int print_instrument_with_mission(PhysicalSystem* p, AggregateProcess* a){
	print_physical_system(p);
	print_aggregate_process(a);
	return swe_ok;
}
