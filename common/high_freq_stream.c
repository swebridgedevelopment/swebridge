
#include "swe_conf.h"
#if ENABLE_HIGH_FREQUENCY_MODULES


#include "swe_conf.h"
#include "common/high_freq_stream.h"
#include "common/swe_data_model.h"
#include "core/simple_process.h"
#include "common/swe_utils.h"
#include <ctype.h>
#include <math.h>

#define DEBUG_THIS_MODULE OFF

#if DEBUG_THIS_MODULE
/*
 * Wrap debug message functions
 */
#define DBGM(...) dmsg(__VA_ARGS__) // DEBUG this module
#else
#define DBGM(...)
#endif



/*
 * Response Structure generation
 */
HighFreqStreamResponse* new_high_freq_stream_process_field(HighFreqStreamResponse* data, Field* infield, Encoding* encoding);
HighFreqStreamResponse* new_high_freq_stream_process_data_stream(HighFreqStreamResponse* data, DataStream* ds);
HighFreqStreamResponse* new_high_freq_stream_process_data_array(HighFreqStreamResponse* data, DataArray* array, Encoding* encoding);
HighFreqStreamResponse* new_high_freq_stream_process_data_record(HighFreqStreamResponse* data, DataRecord* array, Encoding* encoding);
HighFreqStreamResponse* new_high_freq_stream_process_basic_type(HighFreqStreamResponse* data, Field* field, Encoding* encoding);



BinaryElement* create_binary_field(Field* field, Encoding* encoding, int count);
int print_binary_response(HighFreqStreamResponse* b);

//------------------------------------------------------------------------------//
//---------------------- High Frequency Stream  Buffer -------------------------//
//------------------------------------------------------------------------------//

/*
 * Creates a HighFreqStreamBuffer and returns it. This buffer will store data
 * from incoming high freq streams. It will also keep track of the packets to
 * see if any packet has been lost.
 *
 * resp: response structure
 * params: hydrophone parameters,
 * uint: number of samples to store in buffer
 * max_packets_lost: maximum amount of packets that will be considered as acceptable
 *                   to loose in the execution.
 * coeff: conversion coefficient from samples to pressure
 *
 */
HighFreqStreamBuffer* high_freq_stream_buffer_constructor(HighFreqStreamResponse* resp,
		HydrophoneParameters* params, uint nsamples, uint max_packets_lost, float64 coeff){

	HighFreqStreamBuffer* buff = NULL;
	int i;

	buff = swe_malloc(sizeof(HighFreqStreamBuffer));

	buff->data = swe_malloc(nsamples*sizeof(float64));
	buff->average = 0;

	buff->packet_count = 0;
	buff->packet_count_max = 0; // By default disabled
	buff->max_packets_lost = max_packets_lost;

	for ( i=0 ; i<resp->count ; i++ ){
		BinaryElement* element = resp->elements[i];
		// Look for the stream counter
		if (element->role == stream_counter) {
			uint size_bits = element->element_size * 8;
			buff->packet_count_max = pow(2, size_bits);
			dmsg("Max value in packet count is %u", buff->packet_count_max);
		}

		if ( element->role == stream_samples ){
			buff->samples_per_packet = element->count;
			buff->sample_size = element->element_size;
			buff->encoding = element->type;
			dmsg("Each packet has %u samples", buff->samples_per_packet);
			dmsg("Each samples has %u bytes", buff->sample_size);
		}
	}

	buff->length = buff->sample_size * nsamples; // total buffer size in bytes
	buff->data = swe_malloc(buff->length);


	buff->sampling_rate = params->sampling_rate;
	buff->timestamps = swe_malloc(nsamples*sizeof(float64));

	buff->conversion_coefficient = coeff;
	buff->time_between_packets =  ((float64)buff->samples_per_packet) /  buff->sampling_rate;

	return buff;
}



/*
 * Creates a HighFreqStreamBuffer based on a WAV file. This buffer will store data
 * from incoming high freq streams. It will also keep track of the packets to
 * see if any packet has been lost.
 *
 * WavHeader: structure with the info from a WAV file
 * nsamples: Length (in samples) of the buffer
 * coeff: Coefficient to convert from samples to pressure
 *
 */
HighFreqStreamBuffer* high_freq_stream_buffer_from_wave(WavReadFile* wavfile, uint nsamples, float64 coeff ){
	HighFreqStreamBuffer* buff = NULL;
	WavHeader* header = &wavfile->header;

	// Allocate data for the whole buffer
	buff = swe_malloc(sizeof(HighFreqStreamBuffer));

	// Setup general params

	buff->length = nsamples*sizeof(float64);
	buff->data = swe_malloc(buff->length);
	buff->nbytes = 0;
	buff->nsamples = 0;
	buff->sample_size = header->sample_length;

	if ( buff->sample_size  == 2 ){
		buff->encoding = signed_short_data;
	}
	else if (buff->sample_size == 3 ) {
		dmsg("Got 24-bit integer, but converting it to 32-bit to make our life easier...");
		buff->sample_size = 4;
		buff->encoding = signed_int_data;
	}
	else if (  buff->sample_size  == 4 ){
		buff->encoding = signed_int_data;
	}else {
		errmsg("Invalid number of bytes per sample %d", buff->sample_size);
		return NULL;
	}


	buff->average = 0;
	buff->sampling_rate = (float64)header->sample_rate;
	buff->timestamps = swe_malloc(1*sizeof(float64));
	buff->conversion_coefficient = coeff;

	buff->samples_per_packet = nsamples;
	buff->time_between_packets = ((float64)buff->samples_per_packet) /  buff->sampling_rate;
	buff->packet_count = 0;
	buff->packet_count_max = -1; // By default disabled
	buff->max_packets_lost = 10;
	return buff;
}





/*
 * Put to the buffer an empty packet. This function should only be used
 * to maintain the timing when a packet is missing
 */
int put_empty_packet(HighFreqStreamBuffer* buff){
	// Only put an empty packet if there's space in the buffer
	if (buff->nbytes < buff->length) {
		dmsg("putting empty packet...");
		// float64 value = buff->average;
		uint max_count = buff->nbytes + buff->samples_per_packet*buff->sample_size;

		for ( ; buff->nbytes<max_count ; buff->nbytes++ ) {
			buff->data[buff->nbytes] = 0;
		}
		buff->nsamples += buff->samples_per_packet;
		buff->timestamps[buff->timestamp_count] = -1.0;
		buff->timestamp_count += 1;
	} else {
		warnmsg("Skipping put empty packet, buffer is full!");
	}
	return swe_ok;
}

/*
 * This function compares the packet count with previous counts and determines if the
 * incoming packet should be stored, if it should be ignored or there is an error
 * in the communications interface. The packet count in the HighFreqStreamBuffer
 * structure is updated.
 *
 * Returns 0 if packet is OK, 1 if packet should be ignored or -1 if there has been an
 * error. If missing_packets is set to a positive value, it means that n packets
 * where lost in between the last one and the current one. The buffer should be
 * filled with 0's to maintain timing.
 */
int check_packet_count(HighFreqStreamBuffer* buff, uint current_count, uint* missing_packets){
	uint last_count = buff->packet_count;
	uint count_max = buff->packet_count_max;
	uint max_packets_lost = buff->max_packets_lost;
	int distance;
	uint expected_count = (last_count + 1 ) % count_max;
	DBGM("Got %u, expected %u",current_count, expected_count);
	*missing_packets=0;
	if ( expected_count == current_count ) {
		// This is the expected packet!
		buff->packet_count = current_count; // update packet count
		*missing_packets = 0;
		return 0; // Everything is OK!
	}

	distance = current_count - expected_count;

	// If we are close to the max, avoid corner effects...
	if ( abs(distance) > (count_max - 10) ) {
		distance = ( distance - count_max ) % count_max;
	}

	// We have lost too many packets return error
	if ( abs(distance) > max_packets_lost) {
		errmsg("expected packet %u, got %u", expected_count, current_count);
		return -1;
	}

	// Check if it's a delayed packet
	if ( distance < 0 ) {

		warnmsg("Delayed packet %d", current_count);
		// This packet should have arrived earlier. Ignore it
		return 1;
	}
	else {
		// This packet is valid, but we have some missing packets before
		// this one
		*missing_packets = distance;
		warnmsg("Missing %d packets (got %u expected %u)", *missing_packets, current_count, expected_count);
		buff->packet_count = current_count;
		return 0;
	}
	return swe_ok;
}



/*
 * This function copies bytes from one pointer to another rearranging byte order
 * (from big endian to little endian or vice-versa)
 */
void swap_bytes_and_copy(void* to, const void* from, size_t bytes) {
	uint i = 0;
	uchar* pfrom = (uchar*)from;
	uchar* pto = (uchar*)to;
	for ( i = 0 ; i< bytes ; i++ ) {
		pto[i] = pfrom[bytes - i -1];
	}
}



/*
 * Takes a single data block (input) from the BlockRingBuffer, processes it according to the
 * response structure (HighFreqStreamResonse) and stores the result data (and timestamp)
 * to the data buffer (HighFreqStreamBuffer)
 *
 * If scaling factor is greater than 0, data from the RingBuffer is multiplied with the
 * scaling factor.
 *
 */
int process_incoming_packet(HighFreqStreamBuffer* streambuff, // Processed response
		HighFreqStreamResponse* resp, // Response structure
		uchar* input, // RingBuffer where data is located
		float64 timestamp,
		uchar check_count){
	int i, j;
	uchar* data = &streambuff->data[streambuff->nbytes]; // Pointer to the data buffer

	// Loop through the response //
	for (i=0; i<resp->count; i++) {
		BinaryElement* be = resp->elements[i];
		// Skip unused bytes //
		if (be->role == unused) {
			input += be->total_size;
		}
		// Update the stream counter //
		else if (be->role == stream_counter) {
			uint count = 0;
			// Copy the element //
			if (resp->swap_bytes) { // change byte order
				swap_bytes_and_copy(&count, input, be->element_size);
			} else { // copy as is
				memcpy(&count, input, be->element_size);
			}
			if (check_count) {
				uint missing_packets;
				int ret;
				// Check the packet count
				ret = check_packet_count(streambuff, count, &missing_packets);
				// Ignore the packet //
				if ( ret > 0 ) {
					cdmsgn(YEL, " Ignoring delayed packet!");
					return 0;
				}
				else if (ret < 0) {
					return swe_iface_error;
				}
				// Put missing packets //
				if ( (ret == 0) && (missing_packets > 0)) {
					while(missing_packets--){
						put_empty_packet(streambuff);
					}
				}
			} else {
				dmsg("Not checking first packet count, init count is %u", count);
				streambuff->packet_count = count;
			}

			input += be->element_size;


		}
		else if (be->role == stream_samples) { // Convert element by element
			// Loop through each sample //
			for (j=0 ; j<be->count; j++ ) {
				if (resp->swap_bytes) {
					swap_bytes_and_copy(data, input, be->element_size);
				} else {
					memcpy(data, input, be->element_size);
				}
				input += be->element_size;
				data += be->element_size;
				streambuff->nbytes += be->element_size; // update the byte count
			}
			streambuff->nsamples += be->count; // update the number of samples
		}
	}
	// If everything is OK copy the timestamp
	streambuff->timestamps[streambuff->timestamp_count] = timestamp;
	streambuff->timestamp_count++;
	return swe_ok;
}


/*
 * Frees the memory of a HighFreqStreamBuffer
 */
int high_freq_stream_buffer_destructor(HighFreqStreamBuffer* buff) {
	swe_free(buff->data);
	swe_free(buff->timestamps);
	swe_free(buff);
	return swe_ok;
}


//------------------------------------------------------------------------------//
//--------------------------- Tiemstamp management -----------------------------//
//------------------------------------------------------------------------------//


/*
 * Calculates a timestamp for a sample separated "offset" samples from initial timestamp. Samples
 * are spaced according to "sampling rate". If success result is stored in "out_timestamp"
 * and swe_ok is returned. Otherwise error is returned
 *
 */
int calculate_sample_timestamp(HighFreqStreamBuffer* incoming, uint offset, float64* out_timestamp){
	float64 init_timestamp = incoming->timestamps[0];
	float64 delta_t = 1/incoming->sampling_rate;
	// If first timestamp is not valid //
	if (incoming->timestamps[0] < 0 ) {
		uint packets_ahead = 0;
		// Look for the first timestamp that is valid!
		while ( (packets_ahead < incoming->timestamp_count) ){
			packets_ahead++;
			if ( incoming->timestamps[packets_ahead] > 0 ) {
				// Found valid timestamp //

				// Calculate timestamp according to df and number of samples //
				init_timestamp = incoming->timestamps[packets_ahead] - delta_t * incoming->samples_per_packet * (float64)packets_ahead;
			}
		}
	}
	if ( init_timestamp < 0 ) {
		errmsg("Coulg not find any valid timestamp");
		return swe_timer_error;
	}
	*out_timestamp = init_timestamp + ((float64)offset) *delta_t;
	return swe_ok;
}


//------------------------------------------------------------------------------//
//---------------------- Response Structure Generation -------------------------//
//------------------------------------------------------------------------------//


/*
 * Generate SWE Response Structure from a DataStream Field
 */
HighFreqStreamResponse* high_freq_stream_response_constructor(Field* root){
	HighFreqStreamResponse *data;
	endianness platform_endianness;
	int i;
	DBGM("Creating high frequency stream response structure");
	if ( root == NULL ) {
		errmsg("NULL pointer received");
		return NULL;
	}
	else if (root->type != SWE_DataStream) {
		errmsg("Expected DataStream");
		return NULL;
	}
	set_memory_table(MEM_SCHEDULER);
	data = swe_malloc(sizeof(HighFreqStreamResponse));
	data = CATCH_NULL(new_high_freq_stream_process_field(data, root, NULL));

	// Calculate total size
	for ( i=0 ; data->elements[i]!=NULL ; i++ ) {
		data->size += data->elements[i]->total_size;
	}

	platform_endianness = get_platform_endianness();
	// Check if the endianness of platform and data stream are not aligned
	if ( platform_endianness != data->endianness ) {
		data->swap_bytes = TRUE; // Activate byte swapping
	}

	return data;
}


/*
 * Takes a SWE Data Field, processes it and attaches the result to the
 * SWE Data structure
 */
HighFreqStreamResponse* new_high_freq_stream_process_field(HighFreqStreamResponse* data, Field* infield, Encoding* encoding) {
	if (infield == NULL || data == NULL) {
		errmsg("NULL pointer received");
		return NULL;
	}

	if (infield->type == SWE_DataStream) {
		// DataStream is only used to determine the encoding

		data = CATCH_NULL(new_high_freq_stream_process_data_stream(data, (DataStream*)infield->component));
	}
	else if (infield->type == SWE_DataRecord) {
		data = CATCH_NULL( new_high_freq_stream_process_data_record(data, (DataRecord*)infield->component, encoding));
	}
	else if (infield->type == SWE_DataArray) {
		data = CATCH_NULL( new_high_freq_stream_process_data_array(data, (DataArray*)infield->component, encoding));
	}
	else if (check_if_basic(infield->type) == TRUE) {
		data = CATCH_NULL(new_high_freq_stream_process_basic_type (data, infield, encoding));
	}
	else {
		errmsg("Unknown type!!");
		return NULL;
	}

	return data;
}




/*
 * Process data record
 */
HighFreqStreamResponse* new_high_freq_stream_process_data_stream(HighFreqStreamResponse* data, DataStream* ds){
	// check start token //
	if (ds->encoding == NULL) {
		errmsg("Encoding is NULL!");
		return NULL;
	}
	// Assign the byte order
	BinaryEncoding* be = (BinaryEncoding*) ds->encoding->encoding;
	data->endianness = be->byteOrder;
	data = CATCH_NULL(new_high_freq_stream_process_field(data, ds->field, ds->encoding));
	return data;
}


/*
 * Process data record
 *
 * Block separator is only added at the end of the DataRecord if
 */
HighFreqStreamResponse* new_high_freq_stream_process_data_record(HighFreqStreamResponse* data, DataRecord* dr, Encoding* encoding){
	int i;
	for ( i=0 ; i < ARRAY_LENGTH(dr->fields) ; i++ ){
		data = CATCH_NULL(new_high_freq_stream_process_field(data, dr->fields[i], encoding));
	}
	return data;
}


/*
 * Process an array
 */
HighFreqStreamResponse* new_high_freq_stream_process_data_array(HighFreqStreamResponse* data, DataArray* da, Encoding* inencoding){
	int i, j, array_count;
	Encoding* encoding = inencoding;
	if (data == NULL || da == NULL ) return NULL;
	if (da->fields == NULL ) return NULL;


	// Check if a Block Separator is needed before adding new fields//
	if (encoding->type == SWE_TextEncoding) {
		errmsg("Unimplemented");
		return NULL;
	}

	if (da->encoding != NULL ){
		// If there's a different encoding in DataArray, use it instead of the DataStream encoding
		encoding = da->encoding;
	}

	TRY_RET_NULL(field_count_value_get(da->arrayCount, &array_count));

	/*
	 *  If we only have one field in the DataArray create a new element with count N
	 */
	if (ARRAY_LENGTH(da->fields) == 1) {

		BinaryElement* be = create_binary_field(da->fields[0], encoding, array_count);
		data->elements = ATTACH_TO_ARRAY(data->elements, be);
		data->count++;
	}
	/*
	 * Otherwise create a different element for each one
	 */
	else {
		for (j=0; j < array_count ; j++ ) {
			for (i=0 ; da->fields[i] != NULL ; i++) {
				data = new_high_freq_stream_process_field(data, da->fields[i], encoding);
			}
		}
	}

	return data;
}



/*
 * Process Basic Type
 */
HighFreqStreamResponse* new_high_freq_stream_process_basic_type(HighFreqStreamResponse* data, Field* field, Encoding* encoding) {
	BinaryElement* e = CATCH_NULL(create_binary_field(field, encoding, 1));
	data->elements = ATTACH_TO_ARRAY(data->elements, e);
	data->count++;
	return data;
}


/*
 * Creates a Binary Field
 */
BinaryElement* create_binary_field(Field* field, Encoding* encoding, int count) {
	int newsize; // size of the new SWE Data element
	BinaryEncoding* be = (BinaryEncoding*)encoding->encoding;
	BinaryEncodingComponent* c = get_binary_encoding_component_by_field(field, be);
	BinaryElement* element; // new binary element
	if ( c == NULL ) {
		errmsg("Encoding not found!");
		return NULL;
	}

	element = swe_malloc(sizeof(BinaryElement));
	if (field->status == TRUE) {
		element->role = stream_samples;
	} else {
		element->role = unused;
	}

	element->type = c->type;

	if ((newsize = get_size_by_encoding(c->type)) < 1 ){
		errmsg("Encoding not found");
		return NULL;
	}

	element->name = set_string(field->name);
	// By default the element has size 1
	element->count = count;
	element->element_size = newsize;
	element->total_size = count*newsize;

	return element;
}


/*
 * Shows the BinaryResponseStructure
 */
int print_binary_response(HighFreqStreamResponse* b){
	int i;
	for (i=0 ; i< b->count ; i++ ) {
		BinaryElement* be = b->elements[i];
		dmsg("%s", be->name);
		dmsgn("    |----> ");
		cdmsg(BLU, "%s", swe_component_type_text(be->type));
		dmsg("    |----> Count: %d", be->count);
	}
	return 0;
}



/*
 * This function takes the reference (name) of the data and frame fields
 * and updates the HighFreqStreamResponse role fields
 */
int assign_high_freq_stream_roles(HighFreqStreamResponse* response, const char* frame_count_ref ){
	int i;
	uchar data_found = FALSE;
	for ( i=0 ;  i < response->count  ; i++ ) {
		BinaryElement* be = response->elements[i];
		if ( be->count > 5 ) { // If length is bigger than 50 assume it's the data samples
			be->role = stream_samples;
			data_found = TRUE;
			response->samples_per_packet = be->count;
		}
		else if (!strcmp(frame_count_ref, be->name)) {
			be->role = stream_counter;
		}
		else {
			be->role = unused;
		}
	}
	if (data_found == FALSE) {
		errmsg("Could not find data samples field");
		return swe_invalid_arguments;
	}
	return swe_ok;
}



//---------------------------------------------------------------------------//
//---------------------------- Sample Buffer  -------------------------------//
//---------------------------------------------------------------------------//

/*
 * Creates a nes Sample Buffer for n samples
 */
SampleBuffer* sample_buffer_create(uint n){
	SampleBuffer* sb = swe_malloc(sizeof(SampleBuffer));
	sb->size = n;
	sb->samples = swe_malloc(n*sizeof(float64));
	sb->timestamp = -1.0;
	sb->nsamples = 0;
	sb->nleft = sb->size;
	return sb;
}


/*
 * Put N samples to sample buffer. If the buffer was empty the timestamp is also stored.
 * If the buffer already contained samples, timestamp is ignored
 */
int sample_buffer_put(SampleBuffer* sb, float64* from, uint n, float64 timestamp) {
	if ( (sb->nsamples + n) > sb->size) {
		return swe_buffer_overflow;
	}
	// Copy samples //
	memcpy(&sb->samples[sb->nsamples], from, n*sizeof(float64));

	if ( sb->nsamples == 0 ) {
		sb->timestamp = timestamp;
	}
	sb->nsamples += n;
	sb->nleft = sb->size - sb->nsamples;
	return swe_ok;
}


/*
 * Get all samples from SampleBuffer. If timestamp is not NULL, the first timestamp will be stored.
 * The number of sample read is stored in n
 */
int sample_buffer_get(SampleBuffer* sb, float64* to, uint to_length, float64* timestamp, uint *n) {
	if ( to_length < sb->nsamples ) {
		errmsg("output buffer too small");
		return swe_buffer_overflow;
	}
	memcpy(to, sb->samples, sizeof(float64)*sb->nsamples);
	if ( timestamp != NULL ) {
		*timestamp = sb->timestamp;
	}
	if ( n != NULL ){
		*n = sb->nsamples;
	}
	sample_buffer_reset(sb);
	return swe_ok;
}


/*
 * Reset SampleBuffer
 */
int sample_buffer_reset(SampleBuffer* sb){
	if (sb) {
		sb->nsamples = 0;
		sb->timestamp = -1.0;
		sb->nleft = sb->size ;
	}
	return swe_ok;
}


/*
 * This function takes a filled buffer and shifts the samples for overlapping FFTs
 * e.g. overlap 50% buff="1 2 3 4 5 6" --> buff="4 5 6 x x x"
 * e.g. overlap 33% buff="1 2 3 4 5 6" --> buff="5 6 x x x x"
 *
 * sampling rate is the sampling rate of the incoming data
 * overlap is the quantity to overlap, e.g. 50% -> 0.5
 */
int sample_buffer_overlap(SampleBuffer* sb, float64 sampling_rate, float64 overlap){
	if (sb->nsamples != sb->size) {
		errmsg("Buffer is not completely full!");
		return swe_error;
	}
	uint overlapped_samples = (uint)(((float64)sb->nsamples)*overlap); // number of samples to copy
	uint icopy = sb->size - overlapped_samples; // point where to start copying
	uint rest = sb->size - icopy;  // Point where to erase samples

	memcpy(sb->samples, &sb->samples[icopy], overlapped_samples*sizeof(float64)); // move to the beginning
	memset(&sb->samples[icopy], 0, rest*sizeof(float64)); // Set the rest to 0

	sb->nsamples = overlapped_samples; // update the index
	sb->nleft = sb->size - sb->nsamples; // update the index

	// Correct the initial timestamp
	sb->timestamp += ((float64)icopy) / sampling_rate;
	return swe_ok;
}


/*
 * Gets all data in "from" and puts it to "to"
 */
int sample_buffer_to_buffer(SampleBuffer* from, SampleBuffer* to) {
	//int sample_buffer_get(SampleBuffer* sb, float64* to, uint to_length, float64* timestamp, uint *n) {
	if ( to->nleft < from->nsamples ){
		return swe_buffer_overflow;
	}
	float64 data[from->nsamples];
	float64 timestamp;
	uint n = from->nsamples;
	TRY_RET(sample_buffer_get(from, data, n, &timestamp, NULL));
	TRY_RET(sample_buffer_put(to, data, n, timestamp));
	return swe_ok;
}



#endif // ENABLE_HIGH_FREQUENCY_MODULES //
