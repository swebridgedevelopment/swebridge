#ifndef COMMON_INC_NEW_STREAM_PARSER_H_
#define COMMON_INC_NEW_STREAM_PARSER_H_

#include "swe_conf.h"
#if ENABLE_HIGH_FREQUENCY_MODULES

#include "common/swe_data_model.h"
#include "resources/resources.h"
#include "resources/hydrophone.h"
#include "resources/formats/wave.h"


/*
 * This structure creates a buffer to store high frequency incoming streaming data such as audio.
 * It stores incoming samples in a buffer, alongside the timestamps for each packet
 */
typedef struct {
	// Data Samples //
	uchar *data;          // Buffer of length 'size' where the data is stored
	uint length;            // Size of the total buffer in bytes
	uint nbytes;            // Number of bytes within the data buffer
	uint nsamples;          // Number of bytes within the data buffer
	uint sample_size;		// Size of each sample in bytes (usually 2 or 3 bytes)
	swe_data_encoding encoding; // Encoding of the elements in data

	int average;           // This variable stores the average of the buffer in the previous
	                        // iteration. It is used to fill the buffer when a packet is missing
	                        // avoiding glitches.

	float64 conversion_coefficient; // This variable stores the coefficient to convert from samples
	                                // to pressure in µPa


	// Packet Counter //
	uint packet_count;      // Current packet counter (if -1 not initialized)
	int packet_count_max;  // max packet counter, e.g. 256 for 8-bit, 65536 for 16-bit, etc.
	                        // set to -1 if packet count not used
	uint max_packets_lost;  // Maximum number of packets that we can loose "normally". If more than
	                       // n packets is missing an error will be raised

	// Timestamping //
	float64 sampling_rate;   // Stream sampling rate
	float64* timestamps;     // Packet timestamp, should assigned to the first sample of each packet.
	                         // To calculate intermediate timestamps use the sampling rate.
	uint timestamp_count;    // index to store timestamps, should always be equal to count / samples_per_packet
	uint samples_per_packet; // Samples per packet (used to calculate the number of timestamps)

	float64 time_between_packets; // theoretical time between packets

	uint remaining_empty_packets;   // If set to a non-zero value indicates the number of empty packets
									// that need to be put at the beginning of the buffer once its data is processed.
	                                // This can happen when putting the missing packets would cause a buffer
	      	  	  	  	  	  	  	// overflow.

}HighFreqStreamBuffer;


/*
 * Structure to host samples
 */
typedef struct {
	uint nsamples;    // Number of samples currently in buffer
	uint nleft;   // Number of samples left to fill the buffer (always size - nsamples)
	uint size; // Number of bytes allocated
	float64* samples; // samples
	float64 timestamp;// timestamp of the first unprocessed sample
}SampleBuffer;




/*
 * Role of each binary stream element
 */
typedef enum {
	unused = 0,
	stream_samples, // sample data
	stream_counter  // stream count to detect missing packets
}binary_stream_role;


/*
 * BinaryElements
 */
typedef struct {
	swe_data_encoding type;  // type: double, int, etc.
	binary_stream_role role; // flag to determine if the data has to be kept
	uint count;              // Number of elements in data array.
	uint total_size;         // total size
	uchar element_size;      // size of each one of the elements
	char* name;
}BinaryElement;


/*
 * This structures stores the model for a high frequency streaming. Binary data is assumed
 * This structure does not store data, only stores de data model
 */
typedef struct {
	uint count; // number of elements
	BinaryElement **elements; // list of elements
	endianness endianness; // stream endianness
	uint size; // total size in bytes
	uint samples_per_packet; // number of samples for each packet
	uchar swap_bytes; // this flag determines if the bytes have to be swapped before processing
	                  // data (i.e. when platform endianness and stream endiannes are different)
}HighFreqStreamResponse;


HighFreqStreamResponse* high_freq_stream_response_constructor(Field* root);



int assign_high_freq_stream_roles(HighFreqStreamResponse* response, const char* frame_count_ref );
HighFreqStreamBuffer* high_freq_stream_buffer_constructor(HighFreqStreamResponse* resp,
		HydrophoneParameters* params, uint nsamples, uint max_packets_lost, float64 coeff);
HighFreqStreamBuffer* high_freq_stream_buffer_from_wave(WavReadFile* wavfile, uint nsamples, float64 coeff );


int put_empty_packet(HighFreqStreamBuffer* buff);
int binary_buffer_to_data(HighFreqStreamResponse* rs, HighFreqStreamBuffer* ds, uchar* input, float64* scaling_factor);
int calculate_sample_timestamp(HighFreqStreamBuffer* incoming, uint offset, float64* out_timestamp);

// SampleBuffer//
SampleBuffer* sample_buffer_create(uint n);
int sample_buffer_put(SampleBuffer* sb, float64* from, uint n, float64 timestamp);
int sample_buffer_get(SampleBuffer* sb, float64* to, uint to_length, float64* timestamp, uint *n);
int sample_buffer_reset(SampleBuffer* sb);
int sample_buffer_overlap(SampleBuffer* sb, float64 sampling_rate, float64 overlap);
int sample_buffer_to_buffer(SampleBuffer* from, SampleBuffer* to);


#endif // ENABLE_HIGH_FREQUENCY_MODULES //
#endif /* COMMON_INC_NEW_STREAM_PARSER_H_ */


