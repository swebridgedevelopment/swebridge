/*
 *  This file contains a minimal implementation of the SWE Data Common standard. It is not
 *  intended to be a full implementation, but a lightweight and fast version enough for the SWE
 *  Bridge project.
 *
 *  @author: Enoc Martínez
 *  @institution: Universitat Politècnica de Catalunya (UPC)
 *  @contact: enoc.martinez@upc.edu
 */

#include "common/swe_utils.h"
#include "resources/mem_control.h"
#include "swe_conf.h"

// Print Functions //
int print_swe_data_field(Field* field, int level);


// Free functions //
int free_data_array(DataArray* da);
int free_data_stream(DataStream* ds);
int free_data_record(DataRecord* dr);
int free_encoding(Encoding* e);
int free_binary_encoding(BinaryEncoding* be);

uchar compare_field_with_component_path(Field* field, char** path);





//---------------------------------------------------------------------------//
//----------------- SWE Data Component generic operations -------------------//
//---------------------------------------------------------------------------//

/*
 * This function allocates data for the SWE_Data_Element
 */
Field* new_swe_data_field(int level, Field* parent) {
	Field* e=swe_malloc(sizeof(Field));
	e->level = level;
	e->parent = parent;
	return e;
}


/*
 * Checks if the component type is complex (with more nested fields)
 */
uchar check_if_complex(swe_component_type type){
	if ( type == SWE_DataRecord ||
		 type == SWE_DataStream ||
		 type == SWE_DataArray ) {
		return TRUE;
	}
	return FALSE;
}


/*
 * Check if a component type is simple (without nested fields)
 */
uchar check_if_basic(swe_component_type type){
	if ( type == SWE_Quantity ||
		 type == SWE_Count ||
		 type == SWE_Text ||
		 type == SWE_Boolean ||
		 type == SWE_Category) {
		return TRUE;
	}
	return FALSE;
}


/*
 * This function allocates the memory for a Field according to its type
 * and sets the type flag
 */
int field_component_type_set(Field* field, swe_component_type type){
	field->type = type;
	if (type == SWE_Count) {
		field->component=swe_malloc(sizeof(Count));
	}
	else if (type == SWE_Quantity) {
		field->component=swe_malloc(sizeof(Quantity));
	}
	else if (type == SWE_Text) {
		field->component=swe_malloc(sizeof(Text));
	}
	else if (type == SWE_Category) {
		field->component=swe_malloc(sizeof(Category));
	}
	else if (type == SWE_Boolean) {
		field->component=swe_malloc(sizeof(Boolean));
	}
	else if (type == SWE_Time) {
		field->component=swe_malloc(sizeof(Time));
	}
	else if (type == SWE_DataArray) {
		field->component=swe_malloc(sizeof(DataArray));
	}
	else if (type == SWE_DataRecord) {
		field->component=swe_malloc(sizeof(DataRecord));
	}
	else if (type == SWE_DataStream) {
		field->component=swe_malloc(sizeof(DataStream));
	}
	else {
		errmsg("Unknown component type");
		return swe_invalid_arguments;
	}
	return swe_ok;
}


/*
 * Sets the component label
 */
int field_component_label_set(Field* field, const char* label){
	if (field == NULL || field->component == NULL ) {
		warnmsg("empty field!");
		return swe_error;
	}
	else if (field->type == SWE_Text) {
		Text* component = (Text*) field->component;
		component->label = set_string(label);
	}
	else if (field->type == SWE_Quantity) {
		Quantity* component = (Quantity*) field->component;
		component->label = set_string(label);
	}
	else if (field->type == SWE_Count) {
		Count* component = (Count*) field->component;
		component->label = set_string(label);
	}
	else if (field->type == SWE_Boolean) {
		Boolean* component = (Boolean*) field->component;
		component->label = set_string(label);
	}
	else if (field->type == SWE_Category) {
		Category* component = (Category*) field->component;
		component->label = set_string(label);
	}
	else if (field->type == SWE_Time) {
		Time* component = (Time*) field->component;
		component->label = set_string(label);
	}
	else {
		errmsg("Can't set label for a component with type %s", swe_component_type_text(field->type));
	}
	return swe_ok;
}





/*
 * This macro takes a type (Text, Quantity, etc) as its value and casts the
 * field's components to a type
 */
#define RETURN_COMPONENT_LABEL(_type) \
	if ( field->type == SWE_ ## _type ) { \
		_type *_component = (_type*)field->component; \
		return _component->label; \
	}

/*
 * Returns the field's component label
 */
const char* field_component_label_get(Field* field){
	if (field == NULL || field->component == NULL ) {
		warnmsg("empty field!");
		return NULL;
	}
	RETURN_COMPONENT_LABEL(Text);
	RETURN_COMPONENT_LABEL(Category);
	RETURN_COMPONENT_LABEL(Quantity);
	RETURN_COMPONENT_LABEL(Count);
	RETURN_COMPONENT_LABEL(Boolean);
	RETURN_COMPONENT_LABEL(Time);
	return NULL;
}


/*
 * This macro takes a type (Text, Quantity, etc) as its value and casts the
 * field's components to a type
 */
#define RETURN_COMPONENT_DEFINITION(_type) \
	if ( field->type == SWE_ ## _type ) { \
		_type *_component = (_type*)field->component; \
		return _component->definition; \
	}
/*
 * Returns the field's component label
 */
const char* field_component_definition_get(Field* field){
	if (field == NULL || field->component == NULL ) {
		warnmsg("empty field!");
		return NULL;
	}
	RETURN_COMPONENT_DEFINITION(Text);
	RETURN_COMPONENT_DEFINITION(Category);
	RETURN_COMPONENT_DEFINITION(Quantity);
	RETURN_COMPONENT_DEFINITION(Count);
	RETURN_COMPONENT_DEFINITION(Boolean);
	RETURN_COMPONENT_DEFINITION(Time);
	return NULL;
}



/*
 * Adds a new NilValue to the component
 */
int field_component_nilvalue_set(Field* field, char* nilvalue){
	if (field == NULL ) {
		warnmsg("empty field!");
		return swe_error;
	}
	else if (field->type == SWE_Text) {
		Text* component = (Text*) field->component;
		component->nilvalues = ATTACH_TO_ARRAY(component->nilvalues, nilvalue);
	}
	else if (field->type == SWE_Quantity) {
		Quantity* component = (Quantity*) field->component;
		component->nilvalues = ATTACH_TO_ARRAY(component->nilvalues, nilvalue);
	}
	else if (field->type == SWE_Count) {
		Count* component = (Count*) field->component;
		component->nilvalues = ATTACH_TO_ARRAY(component->nilvalues, nilvalue);
	}
	else if (field->type == SWE_Boolean) {
		Boolean* component = (Boolean*) field->component;
		component->nilvalues = ATTACH_TO_ARRAY(component->nilvalues, nilvalue);
	}
	else if (field->type == SWE_Category) {
		Category* component = (Category*) field->component;
		component->nilvalues = ATTACH_TO_ARRAY(component->nilvalues, nilvalue);
	}
	else if (field->type == SWE_Time) {
		Time* component = (Time*) field->component;
		component->nilvalues = ATTACH_TO_ARRAY(component->nilvalues, nilvalue);
	}
	else {
		errmsg("Can't set nilvalues for a component with type %s", swe_component_type_text(field->type));
	}
	return swe_ok;
}

/*
 * This macro takes a type (Text, Quantity, etc) as its value and casts the
 * field's components to a type
 */
#define RETURN_COMPONENT_NILVALUES(_type) \
	if ( field->type == SWE_ ## _type ) { \
		_type *_component = (_type*)field->component; \
		return _component->nilvalues; \
	}


/*
 * Returns the Nilvalues of a Field's component
 */
char** field_component_nilvalues_get(Field* field){
	RETURN_COMPONENT_NILVALUES(Text);
	RETURN_COMPONENT_NILVALUES(Time);
	RETURN_COMPONENT_NILVALUES(Quantity);
	RETURN_COMPONENT_NILVALUES(Count);
	RETURN_COMPONENT_NILVALUES(Boolean);
	RETURN_COMPONENT_NILVALUES(Category);
	errmsg("swe component type %d not valid", field->type);
	return NULL;
}


/*
 * Sets the SWE component value from a string. The static flag is set in the field
 * so these field should be regarded as readonly
 */
int field_component_value_set_static(Field* field, const char* value){
	if (field == NULL ) {
		warnmsg("empty field!");
		return swe_error;
	}
	else if (field->type == SWE_Text) {
		Text* component = (Text*) field->component;
		component->value = set_string(value);
		component->maxsize = strlen(value);
	}
	else if (field->type == SWE_Quantity) {
		Quantity* component = (Quantity*)  field->component;
		component->value = strtod(value, NULL);
	}
	else if (field->type == SWE_Count) {
		Count* component = (Count*)  field->component;
		component->value = string_to_int(value);
	}
	else if (field->type == SWE_Boolean) {
		int errorcode=0;
		Boolean* component = (Boolean*)  field->component;
		component->value = get_true_false(value, &errorcode);
	}
	else if (field->type == SWE_Category) {
		Category* component = (Category*)  field->component;
		component->value = set_string(value);

	}
	else if ( field->type == SWE_Time ) {
		Time* component = (Time*)  field->component;
		component->value = strtod(value, NULL); // assuming epoch time
	}
	else {
		errmsg("Can't set nilvalues for a component with type %s", swe_component_type_text(field->type));
	}
	field->static_field = TRUE;
	return swe_ok;
}



/*
 * Gets the field's component's value and converts it to plain text
 */
int field_component_value_get_ascii(Field* field, char* buffer, int bufflen){
	if (field == NULL ) {
		warnmsg("empty field!");
		return swe_error;
	}
	else if (field->type == SWE_Text) {
		return field_text_value_get(field, buffer, bufflen);
	}
	else if (field->type == SWE_Category) {
		return field_category_value_get(field, buffer, bufflen);
	}
	else if (field->type == SWE_Count) {
		int value;
		TRY_RET(field_count_value_get(field, &value));
		sprintf(buffer, "%i", value);
	}
	else if (field->type == SWE_Boolean) {
		uchar value;
		TRY_RET(field_boolean_value_get(field, &value));
		sprintf(buffer, "%u", value);
	}
	else if (field->type == SWE_Quantity) {
		float64 value;
		TRY_RET(field_quantity_value_get(field, &value));
		sprintf(buffer, "%f", value);
	}
	else if ( field->type == SWE_Time ) {
		float64 value;
		TRY_RET(field_quantity_value_get(field, &value));
		sprintf(buffer, "%f", value);
	}
	else {
		errmsg("Can't set nilvalues for a component with type %s", swe_component_type_text(field->type));
	}
	return swe_ok;
}


//---------------------------------------------------------------------------//
//----------------------- SWE Data Component Get Values ---------------------//
//---------------------------------------------------------------------------//
/*
 * This macro takes a type (Text, Quantity, etc) as its value and casts the
 * field's components to a type
 */
#define FIELD_COMPONENT_VALUE_GETTER(_type) \
	if ( field->type != SWE_ ## _type ) { \
		errmsg("expected field with "#_type" swe data component"); \
		return swe_invalid_arguments; \
	} \
	if (field == NULL || field->component == NULL) { \
		return swe_invalid_arguments; \
	} \
	_type *component = (_type*)field->component \
//	*buffer = component->value;


/*
 * Copies the value of the Text SWE Data Component hosted within field to the buffer
 */
int field_text_value_get(Field* field, char* buffer, int bufflen){
	dmsg("name %s", field->name);
	dmsg("God type %d", field->type);
	FIELD_COMPONENT_VALUE_GETTER(Text);
	if ( bufflen < strlen(component->value)) {
		errmsg("buffer too small");
		return swe_buffer_overflow;
	}
	strcpy(buffer, component->value);
	return swe_ok;
}

/*
 * Copies the value of the Category SWE Data Component hosted within field to the buffer
 */
int field_category_value_get(Field* field, char* buffer, int bufflen){
	FIELD_COMPONENT_VALUE_GETTER(Category);
	if ( bufflen < strlen(component->value)) {
		errmsg("buffer too small");
		return swe_buffer_overflow;
	}
	strcpy(buffer, component->value);
	return swe_ok;
}

/*
 * Copies the value of the Text SWE Data Component hosted within field to the buffer
 */
int field_quantity_value_get(Field* field, float64 *buffer){
	FIELD_COMPONENT_VALUE_GETTER(Quantity);
	*buffer = component->value;
	return swe_ok;
}

/*
 * Copies the value of the Text SWE Data Component hosted within field to the buffer
 */
int field_time_value_get(Field* field, float64 *buffer){
	FIELD_COMPONENT_VALUE_GETTER(Time);
	*buffer = component->value;
	return swe_ok;
}


/*
 * Copies the value of the Text SWE Data Component hosted within field to the buffer
 */
int field_count_value_get(Field* field, int *buffer){
	FIELD_COMPONENT_VALUE_GETTER(Count);
	*buffer = component->value;
	return swe_ok;
}


/*
 * Copies the value of the Text SWE Data Component hosted within field to the buffer
 */
int field_boolean_value_get(Field* field, uchar *buffer){
	FIELD_COMPONENT_VALUE_GETTER(Boolean);
	*buffer = component->value;
	return swe_ok;
}




//---------------------------------------------------------------------------//
//------------------ SWE Data Component Path Manipulation -------------------//
//---------------------------------------------------------------------------//

/*
 * Takes a Field and a new level and returns the appropriate Field
 * container for that level
 */
Field* get_swe_data_parent(Field* current, int inputlevel){
	if (current == NULL) {
		errmsg("get_current_swe_data_container NULL argument");
		return NULL;
	}

	//print_swe_data_field(current);
	uchar found = FALSE;
	Field* parent = current;
	while (!found) {
		// Parent has to be complex //
		uchar complex = check_if_complex(parent->type);
		// Parent must be in a lower level //
		uchar lower = (inputlevel > parent->level);

		if (complex && lower) {
			found = TRUE;
		} else {
			// Keep looking
			parent = parent->parent; // decrease in hierarchy
			if (parent == NULL) {
				errmsg("Root element reached");
			}
		}

	}
	return parent;
}


int attach_swe_field_to_parent(Field* field, Field* parent){
	// Now cdata is the proper container for the new field
	if (parent->type ==  SWE_DataRecord) { // If DataRecord attach the new field to the array
		DataRecord *dr = (DataRecord*)parent->component;
		dr->fields=ATTACH_TO_ARRAY(dr->fields, field);
	}
	else if (parent->type == SWE_DataArray) { // If DataArray attach the new field to the array
		DataArray *da = (DataArray*)parent->component;
		da->fields=ATTACH_TO_ARRAY(da->fields, field);
	}
	else if (parent->type == SWE_DataStream ) { // If DataStream the input field is the input element
		DataStream *ds = (DataStream*)parent->component;
		ds->field = field;
	}
	else {
		errmsg("Unexpected type (can't contain more fields)");
	}
	return swe_ok;
}


/*
 * Takes a type and a Data Field and looks for the last instance of "type"
 */
Field* find_last_swe_field(Field* current, swe_component_type type){
	while (current->type != type) {
		if (current->level == 0) {
			errmsg("Element with type %s not found!", swe_data_encoding_text(type));
			return NULL;
		}
		current=current->parent;
	}
	return current;
}


/*
 * Returns the ID of a field
 */
char* get_field_id(Field* field) {
	if (field == NULL || field->component == NULL ) {
		warnmsg("empty field!");
		return NULL;
	}
	else if (field->type == SWE_Text) {
		Text* component = (Text*) field->component;
		return component->id;
	}
	else if (field->type == SWE_Quantity) {
		Quantity* component = (Quantity*) field->component;
		return component->id;
	}
	else if (field->type == SWE_Count) {
		Count* component = (Count*) field->component;
		return component->id;
	}
	else if (field->type == SWE_Boolean) {
		Boolean* component = (Boolean*) field->component;
		return component->id;
	}
	else if (field->type == SWE_Category) {
		Category* component = (Category*) field->component;
		return component->id;
	}
	else if (field->type == SWE_DataRecord) {
		DataRecord* dr = (DataRecord*) field->component;
		return dr->id;

	} else if (field->type == SWE_DataStream) {
		DataStream* ds = (DataStream*) field->component;
		return ds->id;

	} else if (field->type == SWE_DataArray) {
		DataArray* da = (DataArray*) field->component;
		return da->id;
	}
	return NULL;
}


//---------------------------------------------------------------------------//
//-------------------------------- Endianness -------------------------------//
//---------------------------------------------------------------------------//


/*
 * Returns the ByteOrder
 */
endianness get_byte_order(char* string) {
	if (!compare_strings(BIG_ENDIAN_ATTRIUTE, string)){
		return big_endian;
	} else if (!compare_strings(LITTLE_ENDIAN_ATTRIUTE, string)){
		return little_endian;
	}
	return unknown_order;
}

/*
 * Test the determine if the platform is big endian or little endian
 */
endianness get_platform_endianness() {
    union {
        uint32_t i;
        char c[4];
    } e = { 0x01000000 };

    if ( e.c[0] > 0 ) {
    	return big_endian;
    }
    return little_endian;
}


/*
 * Returns the byteEncoding
 */
byte_encoding get_byte_encoding(char* string) {
	if (!compare_strings(RAW_ATTRIBUTE, string)){
		return raw;
	} else if (!compare_strings(BASE64_ATTRIBUTE, string)){
		return base64;
	}
	return unknown_encoding;
}


/*
 * Returns the BinaryEncodingComponent for a specific filed. If not found NULL is returned
 */
BinaryEncodingComponent* get_binary_encoding_component_by_field(Field* field, BinaryEncoding* be){
	int i;
	if (field->name == NULL) {
		errmsg("Can't look for a field without name");
		return NULL;
	}
	for (i=0; be->components[i] != NULL; i++) {
		BinaryEncodingComponent* c = be->components[i];
		if (compare_field_with_component_path(field, c->path) == TRUE){
			return c;
		}
	}
	errmsg("BinaryEncoding for field %s not found", field->name);
	return NULL;
}


/*
 * Returns the XMLEncodingComponent for a specific filed. If not found NULL is returned
 */
XMLEncodingComponent* get_xml_encoding_component_by_field(Field* field, XMLEncoding* xe){
	int i;
	if (field->name == NULL) {
		errmsg("Can't look for a field without name");
		return NULL;
	}
	for (i=0; xe->components[i] != NULL; i++) {
		XMLEncodingComponent* c = xe->components[i];
		if (compare_field_with_component_path(field, c->path) == TRUE){
			return c;
		}
	}
	errmsg("BinaryEncoding for field %s not found", field->name);
	return NULL;
}


/*
 * Inherit a field from, to "to". All strings and ids are copied, but the "component" field ownership is transferred
 *
 */
int inherit_field(Field* to, Field* from){

	if ( to == NULL || from == NULL ) {
		errmsg("Null pointer");
		return swe_invalid_arguments;
	}

	INHERIT_STRING(to, from, title);
	INHERIT_STRING(to, from, href);
	INHERIT_STRING(to, from, name);
	to->type = from->type;

	if (to->component == NULL && from->component != NULL ) {
		// Copy pointer to component
		to->component = from->component;

		if ( from->linked == FALSE ) {
			// transfer ownership
			to->linked = FALSE;
			from->linked = TRUE;
		} else {
			// if neither to nor from are owners, both are linked fields
			to->linked = TRUE;
		}
	}

	if ( to->parent == NULL ) {
		to->parent = from->parent;
	}

	if ( to->level == 0 ) {
		to->level = from->level;
	}

	to->static_field = from->static_field;
	return swe_ok;
}


/*
 * Compares a component with the PATH rturns TRUE if match, FALSE otherwise
 */
uchar compare_field_with_component_path(Field* field, char** path){
	int i;
	if (field->name == NULL){
		errmsg("Field with NULL name can't be compared (type %s)", swe_component_type_text(field->type));
		return FALSE;
	}

	// Compare path backwards
	for (i = ARRAY_LENGTH(path) - 1 ; i>= 0 ; i--){
		if (!compare_strings(path[i], field->name)){
			if (i > 0) {
				// Get the parent to see if the name also coincides
				field = (Field*)field->parent;
				if (field == NULL) { // We have reached top level, abort
					return FALSE;
				}
				while (field->name == NULL ){
					field = (Field*)field->parent;
					if (field == NULL) { // We have reached top level, abort
						return FALSE;
					}
				}
				// At this point we have the parent
			}

		} else {
			// if they do not match, abort
			return FALSE;
		}
	}
	return TRUE;
}


//---------------------------------------------------------------------------//
//-------------------------- Data Stream Encodings --------------------------//
//---------------------------------------------------------------------------//

#define OGC_DATA_TYPES_OFFSET 5 // this has to be kept in track with the ASCII encodings

static const char* ogc_binary_data_urls[] = {
		OGC_SIGNED_BYTE,
		OGC_UNSIGNED_BYTE,
		OGC_SIGNED_SHORT,
		OGC_UNSIGNED_SHORT,
		OGC_SIGNED_INT,
		OGC_UNSIGNED_INT,
		OGC_SIGNED_LONG,
		OGC_UNSIGNED_LONG,
		OGC_FLOAT_16,
		OGC_FLOAT_32,
		OGC_DOUBLE, /* same as OGC Float 64 */
		OGC_FLOAT_128,
		OGC_STRING_UTF8,
		OGC_SIGNED_INT_24,
		NULL
};

/*
 * Takes a string and returns the ogc binary data type that string is defining
 */
swe_data_encoding get_data_type(const char* string) {
	int i;
	swe_data_encoding type = unknown_data;
	for (i=0; ogc_binary_data_urls[i]!=NULL; i++) { //loop through the ogc data types array
		if (!strcmp(ogc_binary_data_urls[i], string)){
			type  = (swe_data_encoding) (i + OGC_DATA_TYPES_OFFSET);
			break;
		}
	}
	// Double can also be called float64
	if (type == unknown_data &&
		!compare_strings(string, OGC_FLOAT_64)) {
		type = double_data;
	}
	return type;
}





//---------------------------------------------------------------------------//
//------------------------------ Free Structures ----------------------------//
//---------------------------------------------------------------------------//


/*
 * Frees the AbstractSimpleComponent inherited values
 */
#define free_abstract_simple_component(_field) \
	swe_free(_field->id); \
	swe_free(_field->id); \
	swe_free(_field->id)


int free_text(Text* f) {
	free_abstract_simple_component(f);
	swe_free(f->value);
	return swe_ok;
}


int free_quantity(Quantity* f) {
	free_abstract_simple_component(f);
	return swe_ok;
}


int free_count(Count* f) {
	free_abstract_simple_component(f);
	return swe_ok;
}


int free_boolean(Boolean* f) {
	free_abstract_simple_component(f);
	return swe_ok;
}


int free_category(Category* f) {
	free_abstract_simple_component(f);
	return free_text((Text*)f);
}

/*
 * Frees DataArray
 */
int free_data_array(DataArray* da) {
	int i;
	if (da == NULL) return swe_ok;

	swe_free(da->definition);
	swe_free(da->id);
	for ( i=0 ; da->fields[i]!=NULL ; i++) {
		free_field(da->fields[i]);
	}
	if (da->encoding != NULL) {
		free_encoding(da->encoding);
	}
	free_field(da->arrayCount);
	swe_free(da);
	return swe_ok;
}


int free_data_stream(DataStream* ds) {
	if (ds == NULL) return swe_ok;

	swe_free(ds->id);
	swe_free(ds->name);
	free_field(ds->field);
	free_encoding(ds->encoding);
	swe_free(ds);
	return swe_ok;
}



/*
 * This functions frees a data field, however it does not free its sons
 */
int free_field(Field* field) {
	if (field == NULL) return swe_ok;

	if ( field->linked == TRUE ) {
		// If it is a link to another field, just free itself
		SDM_DEBUG(cdmsg(BLU, "Freeing linked field with name %s title %s href %s", field->name, field->title, field->href));
		swe_free(field->name);
		swe_free(field->title);
		swe_free(field->href);
		swe_free(field);
		return swe_ok;
	}
	SDM_DEBUG(cdmsg(BLU, "Freeing field with name %s title %s href %s", field->name, field->title, field->href));
	swe_free(field->name);
	swe_free(field->title);
	swe_free(field->href);

	if (field->type == SWE_Text) {
		free_text((Text*)field->component);
	}
	else if (field->type == SWE_Quantity) {
		free_quantity((Quantity*)field->component);
	}
	else if(field->type == SWE_Count) {
		free_count((Count*)field->component);
	}
	else if(field->type == SWE_Boolean) {
		free_boolean((Boolean*)field->component);
	}
	else if(field->type == SWE_Category) {
		free_category((Category*)field->component);
	}
	else if(field->type == SWE_DataRecord) {
		free_data_record((DataRecord*)field->component);
	}
	else if(field->type == SWE_DataRecord) {
		free_data_record((DataRecord*)field->component);
	}
	else if(field->type == SWE_DataRecord) {
		free_data_record((DataRecord*)field->component);
	}

	swe_free(field);
	return swe_ok;
}



int free_data_record(DataRecord* dr) {
	if (dr == NULL) return swe_ok;

	int i;
	swe_free(dr->definition);
	swe_free(dr->id);

	for (i=0; dr->fields[i]!=NULL ; i++) {
		free_field(dr->fields[i]);
	}
	swe_free(dr->fields);
	swe_free(dr);
	return swe_ok;
}

/*
 * Free Encoding
 */
int free_encoding(Encoding* e) {
	if (e== NULL) return swe_ok;
	if (e->type == SWE_TextEncoding) {
		TextEncoding* te = (TextEncoding*)e->encoding;
		swe_free(te->blockSeparator);
		swe_free(te->tokenSeparator);
		swe_free(te->startToken);
	}
	else if (e->type == SWE_BinaryEncoding) {
		free_binary_encoding((BinaryEncoding*)e->encoding);
	}
	else if (e->type == SWE_XMLEncoding) {

	}
	else {
		errmsg("Unimplemented XML encoding");
		return swe_unimplemented;
	}
	swe_free(e->encoding);
	swe_free(e);
	return swe_ok;
}

/*
 * Free Binary Encoding
 */
int free_binary_encoding(BinaryEncoding* be) {
	int i;
	swe_free(be->id);
	for ( i=0 ; be->components[i]!=NULL ; i++){
		swe_free(be->components[i]);
	}
	swe_free(be->components);

	return swe_ok;
}

/*
 * Free Binary Encoding Component
 */
int free_binary_encoding_component(BinaryEncodingComponent* c) {
	int i;
	swe_free(c->ref);
	swe_free(c->id);
	for ( i=0 ; c->path[i]!=NULL ; i++){
		swe_free(c->path[i]);
	}
	swe_free(c->path);
	swe_free(c);
	return swe_ok;
}


//---------------------------------------------------------------------------//
//-------------------------------- Get by name ------------------------------//
//---------------------------------------------------------------------------//

/*
 * Returns a Field from a list by name
 */
Field* get_field_by_name(Field** list, const char* name){
	int i;
	if (list == NULL || name == NULL){
		errmsg("invalid arguments");
		return NULL;
	}

	for (i=0 ; list[i] != NULL; i++) {
		Field* f = list[i];
		if (!compare_strings(f->name, name)){
			return f;
		}
	}
	warnmsg("Field %s not found", name);
	return NULL;
}


/*
 * Searches for a field with "name" and stores its value in the value pointer
 */
int get_count_value_by_name(Field** list, const char* name, int* value){
	Field* field = TRY_NULL(get_field_by_name(list, name));
	TRY_RET(field_count_value_get(field, value));
	return swe_ok;
}


/*
 * Searches for a field with "name" and stores its value in the value pointer
 */
int get_quantity_value_by_name(Field** list, const char* name, float64* value){
	Field* field = TRY_NULL(get_field_by_name(list, name));
	TRY_RET(field_quantity_value_get(field, value));
	return swe_ok;
}


/*
 * Searches for a field with "name" and stores its value in the value pointer
 */
int get_time_value_by_name(Field** list, const char* name, float64* value){
	Field* field = TRY_NULL(get_field_by_name(list, name));
	TRY_RET(field_time_value_get(field, value));
	return swe_ok;
}


/*
 * Searches for a field with "name" and stores its value in the value pointer
 */
int get_boolean_value_by_name(Field** list, const char* name, uchar* value){
	Field* field = TRY_NULL(get_field_by_name(list, name));
	TRY_RET(field_boolean_value_get(field, value));
	return swe_ok;
}


/*
 * Searches for a field with "name" and stores its value in the value pointer
 */
int get_text_value_by_name(Field** list, const char* name, char* buffer, int bufflen){
	Field* field = TRY_NULL(get_field_by_name(list, name));
	TRY_RET(field_text_value_get(field, buffer, bufflen));
	return swe_ok;
}


/*
 * Searches for a field with "name" and stores its value in the value pointer
 */
int get_category_value_by_name(Field** list, const char* name, char* buffer, int bufflen){
	Field* field = TRY_NULL(get_field_by_name(list, name));
	TRY_RET(field_category_value_get(field, buffer, bufflen));
	return swe_ok;
}










/*
 * Get element size
 */
uint get_size_by_encoding(swe_data_encoding e) {
	uint size = 0;
	switch(e) {
		// ASCII Data Types //
		case ascii_quantity_data:
			size = ASCII_QUANTITY_BYTES;
			break;
		case ascii_count_data:
			size = ASCII_COUNT_BYTES;
			break;
		case ascii_boolean_data:
			size = ASCII_BOOLEAN_BYTES;
			break;
		case ascii_text_data:
			size = ASCII_TEXT_BYTES;
			break;
		// Binary Data Types //
		case signed_byte_data:
			size = sizeof(signed_byte);
			break;
		case unsigned_byte_data:
			size = sizeof(unsigned_byte);
			break;
		case signed_short_data:
			size = sizeof(signed_short);
			break;
		case unsigned_short_data:
			size = sizeof(unsigned_short);
			break;
		case signed_int_data:
			size = sizeof(signed_int);
			break;
		case unsigned_int_data:
			size = sizeof(unsigned_int);
			break;
		case signed_long_data:
			size = sizeof(signed_long);
			break;
		case unsigned_long_data:
			size = sizeof(unsigned_long);
			break;
		case half_precision_float_data:
			size = 0;
			errmsg("Unimplemented half precision float type!");
			break;
		case float_data:
			size = sizeof(float32);
			break;
		case double_data:
			size = sizeof(float64);
			break;
		case long_double_data:
			size = sizeof(float128);
			break;
		case string_data:
			warnmsg("Undefined size for binary string");
			size = 0;
			break;
		case signed_int_24_data:
			size = sizeof(signed_int_24);
			break;
		default:
			break;
	}

	return size;
}

/*
 * Converts value to "type" variable. The result is declared in converted_value
 */
#define PRINT_OGC_BINARY_TYPE(_type, _printfcode) \
	_type _converted_value; \
	memcpy(&_converted_value, value, sizeof(_type)); \
	dmsgn("value: "); \
	cdmsgn(GRN,_printfcode , _converted_value); \
	dmsg(" (" #_type ")");



/*
 * Prints the value according to its encoding
 */
int print_swe_value(void* value, swe_data_encoding e){

	// print ASCII encodings //
	if ( check_if_binary(e) == FALSE){
		dmsg("value %s (ascii)", (char*)value);
	}
	// Binary Encodings //
	else if ( e == signed_byte_data ) {
		PRINT_OGC_BINARY_TYPE(signed_byte, "%d");
	}
	else if ( e == unsigned_byte_data ) {
		PRINT_OGC_BINARY_TYPE(unsigned_byte, "%u");
	}
	else if ( e == signed_short_data ) {
		PRINT_OGC_BINARY_TYPE(signed_short, "%d");
	}
	else if ( e == unsigned_short_data ) {
		PRINT_OGC_BINARY_TYPE(unsigned_short, "%u");
	}
	else if ( e == signed_int_data ) {
		//CAST_TO_OGC_BINARY_TYPE(signed_int, "%d");
		PRINT_OGC_BINARY_TYPE(signed_int, "%d");
	}
	else if ( e == unsigned_int_data ) {
		PRINT_OGC_BINARY_TYPE(unsigned_int, "%u");
	}
	else if ( e == signed_long_data ) {
		PRINT_OGC_BINARY_TYPE(signed_long, "%ld");
	}
	else if ( e == unsigned_long_data ) {
		PRINT_OGC_BINARY_TYPE(unsigned_long, "%lu");
	}
	else if ( e == half_precision_float_data ) {
		errmsg("Half (float16) not implemented!");
		return swe_unimplemented;
	}
	else if ( e == float_data ) {
		PRINT_OGC_BINARY_TYPE(float32, "%f");
	}
	else if ( e == double_data ) {
		PRINT_OGC_BINARY_TYPE(float64, "%lf");
	}
	else if ( e == long_double_data ) {
		PRINT_OGC_BINARY_TYPE(float128, "%llf");
	}
	else if ( e == string_data ) {
		dmsg("type %s: %s", swe_data_encoding_text(e), (char*)value);
	}
	else {
		errmsg("Unimplemented data type %d", (int)e);
		return swe_unimplemented;
	}
	return swe_ok;
}





/*
 * Returns TRUE if the encoding is binary (in case of unknown TRUE is also returned)
 */
uchar check_if_binary(swe_data_encoding e){
	if ( e == ascii_quantity_data || e == ascii_count_data ||
		 e == ascii_boolean_data || e == ascii_text_data) {
		return FALSE;
	}
	return TRUE;
}



/*
 * Converts value to "type" variable. The result is declared in converted_value
 */
#define CONVERT_TO_OGC_BINARY_TYPE(_string_output, _binary_input, _type, _printfcode) \
	_type _converted_value; \
	memcpy(&_converted_value, _binary_input, sizeof(_type)); \
	sprintf(_string_output, _printfcode, _converted_value);




/*
 * Define the printf conversion codes as an static array of const strings
 */
static const char* _precision_printf_codes[UPPER_LIMIT_DECIMAL_PRECISION - LOWER_LIMIT_DECIMAL_PRECISION +1 ] =
		PRECISION_CONVERTION_CODES;

/*
 * This function encodes the data (with encoding e) into buffer. Buffer has to be previously
 * allocated. The decimals parameter is only used if the data is float. precision has to be
 * between 0 and 10 (number of decimals)
 */
int swe_encoding_to_string(char* buffer, void* data, swe_data_encoding e, int precision){
	if (check_if_binary(e) == FALSE ){
		errmsg("Data encoding not valid %s", swe_data_encoding_text(e));
		return swe_invalid_arguments;
	}

	if ( e == half_precision_float_data || e == float_data || e == double_data || e == long_double_data ){
		if( precision < LOWER_LIMIT_DECIMAL_PRECISION || precision > UPPER_LIMIT_DECIMAL_PRECISION ) {
			errmsg("precision should be between %d and %d, received %d",
					LOWER_LIMIT_DECIMAL_PRECISION, UPPER_LIMIT_DECIMAL_PRECISION,precision);
		}
	}

	if ( e == signed_byte_data ) {
		CONVERT_TO_OGC_BINARY_TYPE(buffer, data, signed_byte, "%d");
	}
	else if ( e == unsigned_byte_data ) {
		CONVERT_TO_OGC_BINARY_TYPE(buffer, data,unsigned_byte, "%u");
	}
	else if ( e == signed_short_data ) {
		CONVERT_TO_OGC_BINARY_TYPE(buffer, data,signed_short, "%d");
	}
	else if ( e == unsigned_short_data ) {
		CONVERT_TO_OGC_BINARY_TYPE(buffer, data,unsigned_short, "%u");
	}
	else if ( e == signed_int_data ) {
		CONVERT_TO_OGC_BINARY_TYPE(buffer, data,signed_int, "%d");
	}
	else if ( e == unsigned_int_data ) {
		CONVERT_TO_OGC_BINARY_TYPE(buffer, data,unsigned_int, "%u");
	}
	else if ( e == signed_long_data ) {
		CONVERT_TO_OGC_BINARY_TYPE(buffer, data,signed_long, "%ld");
	}
	else if ( e == unsigned_long_data ) {
		CONVERT_TO_OGC_BINARY_TYPE(buffer, data,unsigned_long, "%lu");
	}
	else if ( e == half_precision_float_data ) {
		errmsg("Half (float16) not implemented!");
		return swe_unimplemented;
	}
	else if ( e == float_data ) {
		CONVERT_TO_OGC_BINARY_TYPE(buffer, data, float32, _precision_printf_codes[precision]);
	}
	else if ( e == double_data ) {
		CONVERT_TO_OGC_BINARY_TYPE(buffer, data, float64, _precision_printf_codes[precision]);
	}
	else if ( e == long_double_data ) {
		CONVERT_TO_OGC_BINARY_TYPE(buffer, data, float128, _precision_printf_codes[precision]);
	}

	else if ( e == signed_int_24_data ) {
//		signed_int a;
//		signed_int_24* b = (signed_int_24*)data;
//
//		uchar *buff2 = (uchar*)&b;
//		a = int24to32(*b);
//		CONVERT_TO_OGC_BINARY_TYPE(buffer, data, signed_int, "%d");
		errmsg("Unimplemented!");
		exit(0);

	}
	return swe_ok;
}




#define CONVERT_SWE_DATA_TO_FLOAT64(_data_type, _value) ({ \
	_data_type _temp; \
	memcpy(&_temp, _value, sizeof(_data_type));\
	(float64)_temp; \
})


/*
 * Converts from SWE_Data to a float64 (double)
 */
float64 swe_data_to_double(SWE_Data* data){
	float64 val;
	swe_data_encoding e;

	if (data == NULL) {
		errmsg("NULL pointer received");
		return -1;
	} else if (data->value == NULL){
		errmsg("NULL pointer received");
		return -1;
	}

	 e = data->encoding;

	//---- ASCII ----//
	if (check_if_binary(e) == FALSE ){
		val = strtod((char*)data->value, NULL);
	}

	//---- Binary Encodings ----//
	else if ( e == signed_byte_data ) {
		val = CONVERT_SWE_DATA_TO_FLOAT64(signed_byte, data->value);
	}
	else if ( e == unsigned_byte_data ) {
		val = CONVERT_SWE_DATA_TO_FLOAT64(unsigned_byte,  data->value);
	}
	else if ( e == signed_short_data ) {
		val = CONVERT_SWE_DATA_TO_FLOAT64(signed_short, data->value);
	}
	else if ( e == unsigned_short_data ) {
		val = CONVERT_SWE_DATA_TO_FLOAT64(unsigned_short, data->value);
	}
	else if ( e == signed_int_data ) {
		val = CONVERT_SWE_DATA_TO_FLOAT64(signed_int, data->value);
	}
	else if ( e == unsigned_int_data ) {
		val = CONVERT_SWE_DATA_TO_FLOAT64(unsigned_int, data->value);
	}
	else if ( e == signed_long_data ) {
		val = CONVERT_SWE_DATA_TO_FLOAT64(signed_long, data->value);
	}
	else if ( e == unsigned_long_data ) {
		val = CONVERT_SWE_DATA_TO_FLOAT64(unsigned_long, data->value);
	}
	else if ( e == half_precision_float_data ) {
		errmsg("Half (float16) not implemented!");
		return swe_unimplemented;
	}
	else if ( e == float_data ) {
		val = CONVERT_SWE_DATA_TO_FLOAT64(float32, data->value);
	}
	else if ( e == double_data ) {
		val = CONVERT_SWE_DATA_TO_FLOAT64( float64, data->value);
	}
	else if ( e == long_double_data ) {
		val = CONVERT_SWE_DATA_TO_FLOAT64( float128, data->value);
	}

	return val;
}

/*
 * Converts from SWE_Data to a float64 (double)
 */
float64 convert_to_float64(uchar* value, swe_data_encoding e){
	float64 val;


	//---- ASCII ----//
	if (check_if_binary(e) == FALSE ){
		val = strtod((char*)value, NULL);
	}

	//---- Binary Encodings ----//
	else if ( e == signed_byte_data ) {
		val = CONVERT_SWE_DATA_TO_FLOAT64(signed_byte, value);
	}
	else if ( e == unsigned_byte_data ) {
		val = CONVERT_SWE_DATA_TO_FLOAT64(unsigned_byte,  value);
	}
	else if ( e == signed_short_data ) {
		val = CONVERT_SWE_DATA_TO_FLOAT64(signed_short, value);
	}
	else if ( e == unsigned_short_data ) {
		val = CONVERT_SWE_DATA_TO_FLOAT64(unsigned_short, value);
	}
	else if ( e == signed_int_data ) {
		val = CONVERT_SWE_DATA_TO_FLOAT64(signed_int, value);
	}
	else if ( e == unsigned_int_data ) {
		val = CONVERT_SWE_DATA_TO_FLOAT64(unsigned_int, value);
	}
	else if ( e == signed_long_data ) {
		val = CONVERT_SWE_DATA_TO_FLOAT64(signed_long, value);
	}
	else if ( e == unsigned_long_data ) {
		val = CONVERT_SWE_DATA_TO_FLOAT64(unsigned_long, value);
	}
	else if ( e == half_precision_float_data ) {
		errmsg("Half (float16) not implemented!");
		return swe_unimplemented;
	}
	else if ( e == float_data ) {
		val = CONVERT_SWE_DATA_TO_FLOAT64(float32, value);
	}
	else if ( e == double_data ) {
		val = CONVERT_SWE_DATA_TO_FLOAT64( float64, value);
	}
	else if ( e == long_double_data ) {
		val = CONVERT_SWE_DATA_TO_FLOAT64( float128, value);
	}
	else if ( e == signed_int_24_data ) {
		signed_int a;
		signed_int_24 *b = (signed_int_24 *)value;
		a = int24to32(*b);
		val = CONVERT_SWE_DATA_TO_FLOAT64(signed_int, &a);
	}
	return val;
}

#define CONVERT_FLOAT64_TO_SWE_DATA(_data_type, _newvalue, _mem_pointer) ({ \
		_data_type *_memory = (_data_type*)_mem_pointer; \
		*_memory = (_data_type)newvalue; \
})



signed_int_24 int32to24(signed_int i32){
	signed_int_24 i24;

	signed_int absi32 = abs(i32);
	uchar* buff32 = (uchar*)&absi32;
	uchar* buff24 = (uchar*)&i24;

	buff24[0] = buff32[0];
	buff24[1] = buff32[1];
	buff24[2] = buff32[2];

	printf("In:  %02x %02x %02x %02x \n", buff32[3], buff32[2], buff32[1], buff32[0]);

	if ( i32 < 0 ){ // Operate with negative numbers!
		buff24[0] = ~buff24[0];
		buff24[1] = ~buff24[1];
		buff24[2] = ~buff24[2];

		// here we have the one's complement
		buff24[0] += 1; // add 1 to get the 2's complement

		// propagate byte overflow
		if (buff24[0] == 0) {
			buff24[1] += 1;
			if (buff24[1] == 0) {
				buff24[2] += 1;
			}
		}
	}
	printf("out:   %02x %02x %02x \n", buff24[2], buff24[1], buff24[0]);
	return i24;
}


signed_int int24to32(signed_int_24 i24){
	signed_int i32;
	uchar* buff24 = (uchar*)&i24;
	uchar* buff32 = (uchar*)&i32;
	buff32[0] = buff24[0];
	buff32[1] = buff24[1];
	buff32[2] = buff24[2];
	buff32[3] = 0;

	if ( 0b10000000 & buff24[2]) { // it was in two's complement...
		buff32[3] = 0b11111111;

	}
	return i32;
}



/*
 * Converts from float64 (double) to SWE_Data
 */
int double_to_swe_data(float64 newvalue, SWE_Data* data){
	swe_data_encoding e = data->encoding;

	//---- ASCII ----//
	if (check_if_binary(e) == FALSE ){
		sprintf((char*)data->value, "%f", newvalue);
	}

	//---- Binary Encodings ----//
	else if ( e == signed_byte_data ) {
		CONVERT_FLOAT64_TO_SWE_DATA(signed_byte, newvalue, data->value);
	}
	else if ( e == unsigned_byte_data ) {
		CONVERT_FLOAT64_TO_SWE_DATA(unsigned_byte, newvalue, data->value);
	}
	else if ( e == signed_short_data ) {
		CONVERT_FLOAT64_TO_SWE_DATA(signed_short, newvalue, data->value);
	}
	else if ( e == unsigned_short_data ) {
		CONVERT_FLOAT64_TO_SWE_DATA(unsigned_short, newvalue, data->value);
	}
	else if ( e == signed_int_data ) {
		CONVERT_FLOAT64_TO_SWE_DATA(signed_int, newvalue, data->value);
	}
	else if ( e == unsigned_int_data ) {
		CONVERT_FLOAT64_TO_SWE_DATA(unsigned_int, newvalue, data->value);
	}
	else if ( e == signed_long_data ) {
		CONVERT_FLOAT64_TO_SWE_DATA(signed_long, newvalue, data->value);
	}
	else if ( e == unsigned_long_data ) {
		CONVERT_FLOAT64_TO_SWE_DATA(unsigned_long, newvalue, data->value);
	}
	else if ( e == half_precision_float_data ) {
		errmsg("Half (float16) not implemented!");
		return swe_unimplemented;
	}
	else if ( e == float_data ) {
		CONVERT_FLOAT64_TO_SWE_DATA( float32, newvalue, data->value);
	}
	else if ( e == double_data ) {
		CONVERT_FLOAT64_TO_SWE_DATA( float64, newvalue, data->value);

	}
	else if ( e == long_double_data ) {
		CONVERT_FLOAT64_TO_SWE_DATA( float128, newvalue, data->value);
	}

	return swe_ok;
}



//----------------------------------------------------------------------------//
//------------------------ SWE Data Model Texts ------------------------------//
//----------------------------------------------------------------------------//
const char* _swe_component_type_text[]={
		"unknown type",
		/* Basic Types */
		"Text",
		"Quantity",
		"Count",
		"Boolean",
		"Category",
		"Time",
		/* Complex Types */
		"DataRecord",
		"DataArray",
		/* Root type */
		"DataStream"
};

const char * _swe_data_encoding_text[] = {
		"unknown type",
		// ASCII //
		"ascii_quantity_data",
		"ascii_count_data",
		"ascii_boolean_data",
		"ascii_text_data",

		// Binary //
		"signedByte",
		"unsignedByte",
		"singedShort",
		"unsignedShort",
		"signedInt",
		"unsignedInt",
		"singedLong",
		"unsignedLong",
		"float16",
		"float32",
		"double", /* same as OGC Float 16 */
		"float128",
		"string UTF8"
		"signedInt24"
};


static const char* _endianness_text [] = {
		"unknown_encoding",
		"little endian",
		"big endian"
};

const char* swe_component_type_text(swe_component_type type){
	return _swe_component_type_text[(int)type];
}

const char* swe_data_encoding_text(swe_data_encoding type){
	return  _swe_data_encoding_text[(int)type];
}


const char*  endianness_text(endianness e){
	return _endianness_text[(int)e];
}


//---------------------------------------------------------------------------//
//--------------- Display SWE Common Data Model Functions--------------------//
//---------------------------------------------------------------------------//

/*
 * Macro that prints spaces according to the representation level
 */
#define PRINT_SPACES(level) ({ \
		int _index=0; \
		for (_index=0; _index<level; _index++) dmsgn("|---"); \
	})

/*
 * Shows an field's attribute
 */
#define PRINT_FIELD_ATTRIBUTE(f, name) do{ \
	if(f->name != NULL) { \
			dmsgn(#name"=\""); \
			cdmsgn(BLU, "%s", f->name); \
			dmsgn("\" "); \
		} \
}while(0)


#define PRINT_NILVALUES(_nilvalues) do { \
	if ( _nilvalues != NULL ) { \
		int _i; \
		level++; \
		for (_i=0;  _nilvalues[_i]!=NULL ; _i++ ) { \
			imsg(""); \
			PRINT_SPACES(level); \
			imsgn("NilValue: "); \
			cimsgn(CYN,"%s", _nilvalues[_i]); \
		} \
		level--; \
	} \
}while(0)



/*
 * Macro than shows a field attriute
 */
#define PRINT_FIELD_ELEMENT(f, name) do{ \
	if(f->name != NULL) { \
			level++; \
			dmsg(""); \
			PRINT_SPACES(level); \
			dmsgn(#name"=\""); \
			cdmsgn(BLU, "%s", f->name); \
			dmsgn("\""); \
			level--; \
		} \
}while(0)


/*
 * Shows a DataStructure
 */
int print_data_structure(Field* root, int level){
	print_swe_data_field(root, level);
	return swe_ok;
}


/*
 * Show Data Record
 */
int print_swe_data_record(DataRecord* dr, int level){
	int i;
	PRINT_FIELD_ATTRIBUTE(dr, id);
	PRINT_FIELD_ATTRIBUTE(dr, definition);
	if (dr->fields == NULL) return swe_ok;
	for (i=0; dr->fields[i]!=NULL; i++){
		dmsg("");
		print_swe_data_field(dr->fields[i], level);
	}

	return swe_ok;
}

/*
 * Shows a SWE Data Stream
 */
int print_swe_data_stream(DataStream* ds, int level){
	PRINT_FIELD_ATTRIBUTE(ds, id);
	dmsg("");
	print_swe_data_field(ds->field, level);
	print_swe_encoding(ds->encoding, level);

	if (ds->values != NULL ) {
		dmsg("");
		PRINT_SPACES(level);
		dmsgn("values= [");
		fmt_dmsg( BLU, "%s", ds->values);
		dmsg("]");

	}
	return swe_ok;
}


/*
 * Shows a SWE Data Array
 */
int print_swe_data_array(DataArray* da, int level){

	int i;
	if (da->fields == NULL) return swe_ok;
	for (i=0; da->fields[i]!=NULL; i++){
		dmsg("");
		print_swe_data_field(da->fields[i], level);
	}
	dmsg("");
	PRINT_SPACES(level);
	// Showing Data Count //
	if (da->arrayCount != NULL ){
		cdmsg(CYN,"elementCount");
		print_swe_data_field(da->arrayCount, level +1 );
	}
	print_swe_encoding(da->encoding, level);
	return swe_ok;
}


/*
 * Shows Data Encoding
 */
int print_swe_encoding(Encoding* encoding, int level){
	if (encoding == NULL || encoding->encoding == NULL){
		//PRINT_SPACES(level);
		//warnmsg("encoding is empty!");
		return swe_ok;
	}
	//---- Text Encoding ----//
	if (encoding->type == SWE_TextEncoding) {
		TextEncoding* te = (TextEncoding* ) encoding->encoding;
		dmsg("");
		PRINT_SPACES(level);
		cdmsgn(CYN,"TextEncoding ");
		dmsgn("tokenSeparator = \"");
		if (te->tokenSeparator != NULL) fmt_dmsg(BLU, "%s", te->tokenSeparator);
		dmsgn("\" ");
		dmsgn("blockSeparator = \"");
		if (te->blockSeparator != NULL) fmt_dmsg(BLU, "%s", te->blockSeparator);
		dmsgn("\" ");

		if (te->startToken!=NULL) {
			dmsgn("startToken = \"");
			fmt_dmsg(BLU, "%s", te->startToken);
			dmsgn("\" ");
		}

		dmsgn("collapseWhiteSpaces = \"");
		if (te->collapseWhiteSpaces == TRUE) fmt_dmsg(BLU, "true");
		else fmt_dmsg(BLU, "false");
		dmsgn("\" ");

	}
	//---- Binary Encoding ----//
	else if (encoding->type == SWE_BinaryEncoding) {
		int i;
		BinaryEncoding* be = (BinaryEncoding* ) encoding->encoding;
		dmsg("");
		PRINT_SPACES(level);
		cdmsgn(CYN,"BinaryEncoding ");

		// Byte Order//
		dmsgn("byteOrder=\"");
		if (be->byteOrder == little_endian) {
			cdmsgn(BLU, "%s", LITTLE_ENDIAN_ATTRIUTE);
		} else if (be->byteOrder == big_endian) {
			cdmsgn(BLU, "%s", BIG_ENDIAN_ATTRIUTE);
		} else {
			cdmsgn(RED, "unknown");
		}
		dmsgn("\" ");

		// Byte Encoding//
		dmsgn("byteEncoding=\"");
		if (be->byteOrder == little_endian) {
			cdmsgn(BLU, "%s", RAW_ATTRIBUTE);
		} else if (be->byteOrder == big_endian) {
			cdmsgn(BLU, "%s", BASE64_ATTRIBUTE);
		} else {
			cdmsgn(RED, "unknown");
		}
		dmsgn("\" ");


		PRINT_FIELD_ATTRIBUTE(be, id);
		level++;
		if (be->components == NULL){
			errmsg("Byte encoding should have at least one Component");
		} else {
			for (i=0; be->components[i] != NULL; i++ ){
				dmsg("");
				BinaryEncodingComponent* c = be->components[i];
				PRINT_SPACES(level);
				cdmsgn(MAG,"Component ");
				PRINT_FIELD_ATTRIBUTE(c, id);
				PRINT_FIELD_ATTRIBUTE(c, ref);
				dmsgn("dataType=\"");
				cdmsgn(CYN,"%s", _swe_component_type_text[(int)c->type]);
				dmsgn("\"");
				if (c->bitLength != 0) {
					dmsgn("byteOrder = \"");
					cdmsgn(GRN,"%d", c->bitLength);
					dmsgn("\"");
				}
				if (c->byteLength != 0) {
					dmsgn("byteLength = \"");
					cdmsgn(GRN,"%d", c->byteLength);
					dmsgn("\"");
				}
				dmsg("");
			}
		}
	}
	dmsg("");
	return swe_ok;
}



/*
 * Shows a SWE Field and all its subfields
 */
int print_swe_data_field(Field* field, int level){
	if (field == NULL)  return swe_ok; // if NULL avoid errors
	PRINT_SPACES(level);
	cdmsgn(CYN, "%s ", swe_component_type_text(field->type));
	PRINT_FIELD_ATTRIBUTE(field, name);
	PRINT_FIELD_ATTRIBUTE(field, title);
	PRINT_FIELD_ATTRIBUTE(field, href);

	if (field->type == SWE_DataRecord){
		level++;
		print_swe_data_record((DataRecord*)field->component, level);
	}
	else if (field->type == SWE_DataStream) {
		level++;
		print_swe_data_stream((DataStream*)field->component, level);
	}
	else if (field->type == SWE_DataArray) {
		level++;
		print_swe_data_array((DataArray*)field->component, level);
	}
	// Basic Types //
	else if (check_if_basic(field->type)) {


		if (field->linked == TRUE) {
			cimsgn(YEL, " linked ");
		}

		imsg("");
		level++;
		PRINT_SPACES(level);
		if (field->type == SWE_Count){
			Count* component = (Count*) field->component;
			PRINT_FIELD_ATTRIBUTE(component, definition);
			PRINT_FIELD_ATTRIBUTE(component, id);
			dmsg("");
			PRINT_SPACES(level);
			imsgn("value: ");
			cimsgn(CYN,"%d", component->value);
		}
		else if (field->type == SWE_Quantity){
			Quantity* component = (Quantity*) field->component;
			PRINT_FIELD_ATTRIBUTE(component, definition);
			PRINT_FIELD_ATTRIBUTE(component, id);
			PRINT_FIELD_ATTRIBUTE(component, uom);
			PRINT_NILVALUES(component->nilvalues);
			dmsg("");
			PRINT_SPACES(level);
			imsgn("value: ");
			cimsgn(CYN,"%f", component->value);
		}
		else if (field->type == SWE_Text){
			Text* component = (Text*) field->component;
			PRINT_FIELD_ATTRIBUTE(component, definition);
			PRINT_FIELD_ATTRIBUTE(component, id);
			PRINT_NILVALUES(component->nilvalues);
			dmsg("");
			PRINT_SPACES(level);
			imsgn("value: ");
			cimsgn(CYN,"%s", component->value);
		}
		else if (field->type == SWE_Text){
			Category* component = (Category*) field->component;
			PRINT_FIELD_ATTRIBUTE(component, definition);
			PRINT_FIELD_ATTRIBUTE(component, id);
			PRINT_NILVALUES(component->nilvalues);
			dmsg("");
			PRINT_SPACES(level);
			imsgn("value: ");
			cimsgn(CYN,"%s", component->value);
		}
		else if (field->type == SWE_Text){
			Boolean* component = (Boolean*) field->component;
			PRINT_FIELD_ATTRIBUTE(component, definition);
			PRINT_FIELD_ATTRIBUTE(component, id);
			PRINT_NILVALUES(component->nilvalues);
			dmsg("");
			PRINT_SPACES(level);
			imsgn("value: ");
			if ( component->value ) cimsgn(CYN,"true");
			else cimsgn(CYN,"false");
		}
		else if (field->type == SWE_Time){
			Time* component = (Time*) field->component;
			PRINT_FIELD_ATTRIBUTE(component, definition);
			PRINT_FIELD_ATTRIBUTE(component, id);
			PRINT_FIELD_ATTRIBUTE(component, uom);
			PRINT_NILVALUES(component->nilvalues);
			dmsg("");
			PRINT_SPACES(level);
			imsgn("value: ");
			cimsgn(CYN,"%f", component->value);
		}
	}

	else {
		imsg("");
	}
	return swe_ok;
}

/*
 * Prints the fields ina  list
 */
int print_field_list(Field** list){
	int i;
	if (list == NULL) {
		warnmsg("Field List is empty!");
		return swe_invalid_arguments;
	}
	for (i=0 ; list[i] != NULL; i++) {
		Field* field = list[i];
		imsgn("%d of %d- ", i+1, ARRAY_LENGTH(list));
		cdmsgn(CYN, "%s ", swe_data_encoding_text(field->type));
		PRINT_FIELD_ATTRIBUTE(field, name);
		PRINT_FIELD_ATTRIBUTE(field, title);
		PRINT_FIELD_ATTRIBUTE(field, href);
		imsg(" (%p)", field);
	}
	imsg("\n");
	return swe_ok;
}

/*
 * Wrapper for print swe value
 */
void print_swe_data(SWE_Data* d){
	print_swe_value(d->value, d->encoding);
}
