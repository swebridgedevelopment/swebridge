/*
 * This file implements a simple ring buffer to perform fast I/O operations.
 * The ring buffer only allows operations in a fixed data size called 'block size'.
 * Each data block in the buffer has its own timestamp.
 *
 * @author: Enoc Martínez
 * @institution: Universitat Politècnica de Catalunya (UPC)
 * @contact: enoc.martinez@upc.edu
 */

#include "swe_conf.h"
#if ENABLE_HIGH_FREQUENCY_MODULES

#include "common/swe_utils.h"
#include "common/ring_buffer.h"
#include "resources/mem_control.h"
#include "resources/resources.h"

#define DEBUG_RING_BUFFER OFF

#if DEBUG_RING_BUFFER
/*
 * Debug message macro, will be compiled only if DEBUG_RING_BUFFER is ON
 */
#define DBGM(...) dmsg(__VA_ARGS__) // DEBUG this module
#else
#define DBGM(...)
#endif



/*
 * Creates a BlockRingBuffer structure with 'nblocks' blocks, each one with
 * size 'blocksize' (bytes)
 */
BlockRingBuffer* ringbuff_constructor(uint nblocks, uint blocksize){
	BlockRingBuffer* rb = NULL;
	int i;
	if ( nblocks == 0 || blocksize == 0 ) {
		errmsg("Can't create a ring buffer with size 0!");
		return NULL;
	}
	rb = swe_malloc(sizeof(BlockRingBuffer));
	// Assign sizes
	rb->blocksize = blocksize;
	rb->nblocks = nblocks;
	rb->iread = 0;
	rb->iwrite = 0;
	rb->blocks = swe_malloc(nblocks*sizeof(BufferBlock));
	for ( i=0 ; i<nblocks ; i++) {
		rb->blocks[i].data = swe_malloc(sizeof(uchar)*blocksize);
		rb->blocks[i].timestamp = -1.0;
		rb->blocks[i].filled = FALSE;
	}
	return rb;
}


/*
 * Puts n bytes to a single ring buffer block. The data will be written regardless the
 * buffer state
 */
int ringbuff_put_block(BlockRingBuffer* rb, uchar* from, uint bytes){
	BufferBlock *block = &rb->blocks[rb->iwrite]; // Get current block
	float64 timestamp = swe_get_epoch_time();
	if( bytes != rb->blocksize ) {
		errmsg("Buffer block must have size %u", rb->blocksize);
		return swe_error;
	}

	// Check if we have an overflow //
	if ( rb->blocks[rb->iwrite].filled == TRUE ) {
		errmsg("Ring Buffer overflow!!");
	}

	memcpy(block->data, from, bytes); // Copy the content to the buffer
	block->timestamp = timestamp;
	block->filled = TRUE;

	rb->iwrite++;

	// Reset pointer if we are at the end of the buffer //
	if ( rb->iwrite >= rb->nblocks ) {
		rb->iwrite = 0;
	}
	return bytes;
}


/*
 * Reads "size" bytes from the next buffer block. If timestamp pointer is not NULL
 * the data timestamp is stored in it
 */
int ringbuff_get_block(BlockRingBuffer* rb, uchar* to, uint bytes, float64* timestamp) {
	BufferBlock* block = &rb->blocks[rb->iread];

	if ( block->filled == FALSE ){ // Check if buffer is empty
		return swe_empty_buffer;
	}

	if (bytes != rb->blocksize) { // If block has data get it
		errmsg("requested bytes does not match RingBuffer block size");
		return swe_invalid_arguments;
	}

	memcpy(to, block->data, bytes); // read data from buffer
	block->filled = FALSE; // data already used


	if ( timestamp != NULL ) { // Get timestamp
		*timestamp = block->timestamp;
	}

	// Increase the read pointer //
	rb->iread++;
	if ( rb->iread >= rb->nblocks) {
		rb->iread = 0;
	}

	return bytes;
}

/*
 * Resets the ringbuffer by setting all filled flags to FALSE and the read / write index to 0
 */
int ringbuff_reset(BlockRingBuffer* rb) {
	int i;
	rb->iread = 0;
	rb->iwrite = 0;
	for ( i=0 ; i<rb->nblocks ; i++ ) {
		rb->blocks[i].filled = FALSE;
	}
	return swe_ok;
}

/*
 * Frees allocated memory in ring buffer
 */
int ringbuff_destructor(BlockRingBuffer* rb) {
	int i;
	if (rb == NULL) {
		return swe_ok;
	}
	for ( i=0 ; i<rb->nblocks ; i++ ) {
		BufferBlock* block = &rb->blocks[i];
		swe_free(block->data);
		swe_free(block);
	}
	swe_free(rb);
	return swe_ok;
}

/*
 * Returns the number of blocks available to be read in the Ring Buffer
 */
int ringbuff_available_blocks(BlockRingBuffer* rb) {
	int nblocks = 0;
	// Calculate the number of bytes available //
	if (rb->iwrite >= rb->iread){
		nblocks = rb->iwrite - rb->iread;
	} else {
		nblocks= rb->nblocks - rb->iread + rb->iwrite;
	}
	return nblocks;
}

#endif // ENABLE_HIGH_FREQUENCY_STREAM //
