/*
 * This file implements lite, non-normative C structure to store SensorML
 * documents. It is not intended to be a fully-functional SensorML standard
 * implementation in C, but to provide the basics for the SWE_Bridge to be
 * able to interpret SensorML documents.
 *
 * @author: Enoc Martínez
 * @institution: Universitat Politècnica de Catalunya (UPC)
 * @contact: enoc.martinez@upc.edu
 */

#ifndef COMMON_INC_SENSORML_H_
#define COMMON_INC_SENSORML_H_

#include "common/swe_data_model.h"





//-----------------------------------------------------------------//
//---------------------- PhysicalSystem ---------------------------//
//-----------------------------------------------------------------//

/*
 * Implementation for each of the elements of an sml:IdentifierList.
 * sml:identification, sml:IdentiferList are ignored
 */
typedef struct {
	char* definition; // sml:Term's definition attribute
	char* label;      // sml:Term's sml:label children value
	char* value;      // sml:Term's sml:value children value
}Identifier;

// Classifier takes exact same values than Identifier
typedef Identifier Classifier;



/*
 * Input structure hosts a swe data field modelling a instrument input
 */
typedef struct {
	char* title;
	char* name;
	char* href;
	Field* field;
}Input;
// Output and Parameter have the same structure as Input
typedef Input Output;
typedef Input Parameter;
typedef Input Capability;

/*
 * Structure that contains a Setting with a reference and a value. The same structure
 * is used for setValue and setStatus
 */
typedef struct {
	char* ref;
	char* value;
}Setting;



/*
 * A SimpleProcess is simple software process modeled in a single component, such as
 * sending a command or storing data to a file
 */
typedef struct {
	char *typeOf;
	char *uid;
	char *name; // component's name

	Input **inputs;
	Output **outputs;
	Parameter **parameters;
	Setting **settings;


	Field** field_list; // List of all fields used by the SimpleProcess, including within inputs, outputs and parameters
	uchar linked_fields; // this flag indicates that the field's memory is not allocated in this SimpleProcess

	void *parent; // If the simpleProcess inherits from another simple process
	              // this variable will point to the parent

	void *physical_system; // Pointer to the physical system who owns the process

	void *interface; // Pointer to the instrument's interface

}SimpleProcess;

/*
 * Connections are used to link an output of a simple process to an input
 * of another process
 */
typedef struct{
	char* source;
	char* destination;
}Connection;

/*
 * DataInterface is used to store all the fields of a certain interface (i.e. RS232, TCP, etc)
 */
typedef struct{
	char* id;
	Field** fields;
}DataInterface;


/*
 * Position
 */
typedef struct {
	float latitude;
	float longitude;
	float depth;
}Position;

/*
 * attachedTo
 */

typedef struct {
	char* title;
	char* href;
}AttachedTo;

/*
 * PhysicalSystem is the
 */
typedef struct {
	char* id;

	Identifier** identifiers; // list of identifiers
	Classifier** classifiers; // list of classifiers
	Parameter** parameters;   // list of parameters
	Capability** capabilities;   // list of parameters
	DataInterface* interface_conf; // Array of fields containing the configuration info

	SimpleProcess** processes; // list of available processes

	void* interface; // pointer to the interface (not allocated here)

	// Parameters for SOS Transactions Generation //
	char* uid;

	char* feature_of_interest_href;
	char* feature_of_interest_title;

	uchar mobile; // flag to determine if the sensor is mobile

	Position* position;
	AttachedTo* attached_to;
}PhysicalSystem;


/*
 * SensorML AggregateProcess structure. This structure models a set of software
 * processes. Within the SWE Bridge the AggregateProcess is used to setup a
 * the mission
 */
typedef struct {
	char* uid;
	char* id;
	SimpleProcess** processes;
	Connection** connections;

	char* instrument_id; // uid of the instrument that uses this AggregateProcess

	//void * instrument;
	void * interface; // Pointer to the interface

	PhysicalSystem* instrument;

	// Parameter** parameters; UNIMPLEMENTED!!
}AggregateProcess;


SimpleProcess* simple_process_create(PhysicalSystem* ps);
Parameter* get_parameter_by_definition(Parameter** parameters, const char* definitions[]);
Capability* get_capability_by_definition(Capability** capabilties, const char* definitions[]);


int link_referenced_fields(PhysicalSystem* system);
int print_physical_system(PhysicalSystem* p);
int print_instrument_with_mission(PhysicalSystem* p, AggregateProcess* a);
int print_aggregate_process(AggregateProcess* a);

// Inherit
int inherit_inputs(SimpleProcess *process_to, SimpleProcess *process_from);
int inherit_outputs(SimpleProcess *process_to, SimpleProcess *process_from);
int inherit_parameters(SimpleProcess *process_to, SimpleProcess *process_from);


PhysicalSystem** remove_physical_system(PhysicalSystem** list, int index);
PhysicalSystem** clear_unused_physical_systems(PhysicalSystem** list);

// Free Functions //
int free_physical_system(PhysicalSystem* ps);
int free_aggregate_process(AggregateProcess* ap);


// Interfaces fixed terms //
#define SML_PORT_TYPE "portType"
#define SML_SERIAL_DEVICE "serialDevice"
#define SML_SOFTWARE_FLOW_CONTROL "softwareFlowControl"
#define SML_BAUDRATE "baudRate"
#define SML_IP_ADDRESS "IP"
#define SML_PORT "portNumber"

// Identifiers fixed Terms //

#define SML_SHORT_NAME "shortName"
#define SML_LONG_NAME  "longName"
#define SML_OFFERING_NAME "offeringName"
#define SML_RESPONSE_TEMPLATE "responseTemplate"


// List of valid values to determine the latitude within a axisID
#define SML_LATITUDE_AXIS_ID_LIST {"Latitude", "latitude","Lat", "lat", "y",  NULL}
// List of valid values to determine the longitude within a axisID
#define SML_LONGITUDE_AXIS_ID_LIST {"longitude", "Longitude", "Long", "long", "Lon", "lon", "x", NULL}
// List of valid values to determine the depth within a axisID
#define SML_DEPTH_AXIS_ID_LIST {"depth", "Depth", "z", NULL}



#endif /* COMMON_INC_SENSORML_H_ */
