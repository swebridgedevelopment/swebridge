/*
 * This file implements a simple ring buffer to perform fast I/O operations
 *
 * @author: Enoc Martínez
 * @institution: Universitat Politècnica de Catalunya (UPC)
 * @contact: enoc.martinez@upc.edu
 */

#ifndef RING_BUFFER_H_
#define RING_BUFFER_H_

#include "swe_conf.h"
#if ENABLE_HIGH_FREQUENCY_MODULES

/*
 * Structure to store the timestamp
 */
typedef struct{
	int nanoseconds;
	int seconds;
	int minutes;
	int hour;
	int day;
	int month;
	int year;
}Timestamp;


/*
 * BufferBlock is a the element where each stream should be stored
 */
typedef struct{
	uchar* data; // Buffer
	float64 timestamp; // Timestamp in epoch
	uchar filled; // if this flag is TRUE data in the buffer block has not yet been processed
}BufferBlock;


/*
 * Define RingBuffer as a set of BufferBlocks
 */
typedef struct {
	BufferBlock *blocks; // array of BufferBlocks
	uint iread; // read block index
	uint iwrite; // write block index
	uint nblocks; // Number of blocks
	uint blocksize; // Number of blocks

	uchar overflow; // Flag that determines if we have an overflow in the buffer
}BlockRingBuffer;



BlockRingBuffer* ringbuff_constructor(uint blocks, uint blocksize);
int ringbuff_destructor(BlockRingBuffer* rb);
int ringbuff_available_blocks(BlockRingBuffer* rb);
int ringbuff_get_block(BlockRingBuffer* rb, uchar* to, uint bytes, float64* timestamp);
int ringbuff_put_block(BlockRingBuffer* rb, uchar* from, uint bytes);
int ringbuff_reset(BlockRingBuffer* rb);

#endif // ENABLE HIGH FREQUENCY MODULES //
#endif // RING_BUFFER_H_


