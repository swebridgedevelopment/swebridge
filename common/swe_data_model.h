/*
 *  This file contains a minimal implementation of the SWE Data Common standard. It is not
 *  intended to be a full implementation, but a lightweight and fast version enough for the SWE
 *  Bridge project.
 *
 *  @author: Enoc Martínez
 *  @institution: Universitat Politècnica de Catalunya (UPC)
 *  @contact: enoc.martinez@upc.edu
 */



#ifndef _SWE_COMMON_DATA_MODEL_H_
#define _SWE_COMMON_DATA_MODEL_H_

#include "swe_conf.h"
#include "swe_utils.h"


//----------------------------------------------------------------------------//
//------------------- SWE Common Data Model Enumerations ---------------------//
//----------------------------------------------------------------------------//
/*
 * Possible Encodings for a DataStream
 */
typedef enum {
	SWE_UnknownEncoding=0, // encoding defined by a lower level field
	SWE_TextEncoding,
	SWE_BinaryEncoding,
	SWE_XMLEncoding
}data_stream_encoding;


/*
 * Generic Encoding Container
 */
typedef struct{
	data_stream_encoding type;
	void* encoding; // Pointer to the encoding structure (Text, XML or binary)
}Encoding;



//----------------------------------------------------------------------------//
//---------------------------- Endianness  -----------------------------------//
//----------------------------------------------------------------------------//

#define BIG_ENDIAN_ATTRIUTE "bigEndian"
#define LITTLE_ENDIAN_ATTRIUTE "littleEndian"

/*
 * Endianness
 */
typedef enum {
	unknown_order=0,
	little_endian,
	big_endian
}endianness;

// endiannes check //
endianness get_platform_endianness();

//----------------------------------------------------------------------------//
//-------OGC Data Types defined at the SWE Common Data Model Standard --------//
//----------------------------------------------------------------------------//

#define OGC_SIGNED_BYTE "http://www.opengis.net/def/dataType/OGC/0/signedByte"
// Description: 8-bits signed binary integer
// Range : -128 to 127
typedef int8_t signed_byte;

#define OGC_UNSIGNED_BYTE "http://www.opengis.net/def/dataType/OGC/0/unsignedByte"
// Description: 8-bits unsigned binary integer
// Range: 0 to 256
typedef uint8_t unsigned_byte;

#define OGC_SIGNED_SHORT  "http://www.opengis.net/def/dataType/OGC/0/signedShort"
// Description: 16-bits signed binary integer
// Range: −32,768 to +32,767
typedef int16_t signed_short;

#define OGC_UNSIGNED_SHORT "http://www.opengis.net/def/dataType/OGC/0/unsignedShort"
// Description: 16-bits unsigned binary integer
// Range: 0 to +65,535
typedef uint16_t unsigned_short;

#define OGC_SIGNED_INT "http://www.opengis.net/def/dataType/OGC/0/signedInt"
// Description: 32-bits signed binary integer.
// Range**: −2,147,483,648 to +2,147,483,647
typedef int32_t signed_int;

#define OGC_UNSIGNED_INT "http://www.opengis.net/def/dataType/OGC/0/unsignedInt"
// Description: 32-bits unsigned binary integer
// Range: 0 to +4,294,967,295
typedef uint32_t unsigned_int;

#define OGC_SIGNED_LONG "http://www.opengis.net/def/dataType/OGC/0/signedLong"
// Description: 64-bits signed binary integer.
// Range: −2^63 to +2^63 - 1
typedef int64_t signed_long;

#define OGC_UNSIGNED_LONG "http://www.opengis.net/def/dataType/OGC/0/unsignedLong"
// Description: 64-bits unsigned binary integer.
// Range: 0 to +2^64 - 1 */
typedef uint64_t unsigned_long;

/* WARNING: 16-bit float is not currently implemented because some architectures do not
 * implement it. Instead a 32 float should be used */
#define OGC_FLOAT_16 "http://www.opengis.net/ def/dataType/OGC/0/float16"
// Description: 16-bits single precision floating point number as defined in IEEE 754.
//UNIMPLEMENTED!

#define OGC_FLOAT_32 "http://www.opengis.net/def/dataType/OGC/0/float32"
// Description: 32-bits single precision floating point number as defined in IEEE 754.
typedef float float32;

#define OGC_DOUBLE "http://www.opengis.net/def/dataType/OGC/0/double"
#define OGC_FLOAT_64 "http://www.opengis.net/def/dataType/OGC/0/float64"
// Description: 64-bits double precision floating number as defined in IEEE 754.
//typedef double float64;

#define OGC_FLOAT_128 "http://www.opengis.net/def/dataType/OGC/0/float128"
// Description: 128-bits quadruple precision floating point number as defined in IEEE 754.
typedef long double float128;


#define OGC_STRING_UTF8 "http://www.opengis.net/def/dataType/OGC/0/string-utf-8"



// WARNING: the following ogc types are not part of the standard, but required in some real-world applications



#define OGC_SIGNED_INT_24 "http://www.opengis.net/def/dataType/OGC/0/signedInt24"
// Description: 24-bits signed binary integer.

// define a struct to host 24-bit integers. Mainly used for 24-bit hydrophones
typedef struct {
	unsigned char data[3];
}signed_int_24;



// custom 24-bit functions
signed_int_24 int32to24(signed_int i32);
signed_int int24to32(signed_int_24 i24);

//----------------------------------------------------------------------------//
//------------- SWE Common Data Model Inheritable Structures  ----------------//
//----------------------------------------------------------------------------//
/*
 * SWE Data Common Abstract Simple Component fields
 */
#define AbstractSimpleComponent \
		char* definition; /* link to an URL defining the measured property */ \
		char* label; /* human readable name */ \
		char* id;    /* unique identifier for this field within the document */ \
		char** nilvalues /* list of invalid values (e.g. -999) */ \



/*
 * Xlink attributes
 */
#define XlinkAttributes \
	char* title; \
	char* href



//----------------------------------------------------------------------------//
//------------------- SWE Common Data Model Structures -----------------------//
//----------------------------------------------------------------------------//

/*
 * SWE Data Common Data Types
 */
typedef enum {
	SWE_Unknown_Type=0, //default value

	/* Basic Types */
	SWE_Text,   // String
	SWE_Quantity, // float64
	SWE_Count,    // int32
	SWE_Boolean,  // Boolean (uchar)
	SWE_Category, // pre-defined set of options (equivalent ot text)
	SWE_Time, // epoch time (float64)
	/* Complex Types */
	SWE_DataRecord,
	SWE_DataArray,
	/* Root type */
	SWE_DataStream,
}swe_component_type;


/*
 * The Field is a generic container used to nest swe data compoennts in a
 * unique structure.
 */
typedef struct{
	XlinkAttributes;
	char* name;
	swe_component_type type; // Type of the current structure
	void* component; // Pointer to the Element Structure, can be any SWE Component Type
	void* parent;
	int level;

	uchar linked; /* this flags indicates that the contents of field are not
	                 allocated within this structure, but within other field
	                 and should not be freed here. */

	uchar status; /* status flag, if disabled (false) data from this field
	                 will not be stored in InsertResult files */

	uchar static_field; /* This flag is set by the parser and determines that the component's value
	                 is static and should not be changed. */
}Field;



//-------- Simple Component Data types --------//
typedef struct {
	AbstractSimpleComponent; // Inherit AbstractSimpleComponent
	int value;
}Count;


typedef struct {
	AbstractSimpleComponent; // Inherit AbstractSimpleComponent
	float64 value;
	char* uom;
}Quantity;

typedef Quantity Time; // Time is equivalent to quantity (epoch time)


typedef struct {
	AbstractSimpleComponent; // Inherit AbstractSimpleComponent
	char* value;
	int maxsize; // number of bytes allocated in value
}Text;

typedef Text Category; // Treat category as text (although they're different)

typedef struct {
	AbstractSimpleComponent; // Inherit AbstractSimpleComponent
	unsigned_byte value;
}Boolean;




//---- complex data types ----/
/*
 * A DataArray structure contains an arbitrary number of fields
 * which repeat a number of times defined by count. It has its
 * own encoding
 */
typedef struct{
	AbstractSimpleComponent; // Inherit AbstractSimpleComponent
	Field** fields;
	Field* arrayCount;
	Encoding* encoding;
}DataArray;


/*
 * A DataRecord structure contains an arbitrary number of fields. Each
 * DataRecord element is delimited by "tokenSeparators" and each
 * DataRecord ends with a "blockSeparator"
 */
typedef struct{
	Field** fields;
	char* id;
	char* definition;
}DataRecord;


/*
 * The response container. It has an arbitrary number of elements which can
 * be DataRecords or DataArrays.
 */
typedef struct{
	Field* field; // DataStream can only host one field
	Encoding* encoding;
	char* id;
	char* name;
	char* values; // Fixed values (only used in commands)
}DataStream;





//----------------------------------------------------------------------------//
//---------------------- Binaray data encodings  -----------------------------//
//----------------------------------------------------------------------------//

#define RAW_ATTRIBUTE "raw"
#define BASE64_ATTRIBUTE "base64"

/*
 * Possible encodings for binary data chunks
 */
typedef enum {
	unknown_encoding=0,
	raw,
	base64
}byte_encoding;

//----------------------------------------------------------------------------//
//------------------------  Data Stream Encodings ---- -----------------------//
//----------------------------------------------------------------------------//

/*
 * SWE Data Encoding defines all the possible encodings for a certain
 * field in a SWE Data structure.
 */
typedef enum {
	unknown_data=0,

	// ASCII //
	ascii_quantity_data,
	ascii_count_data,
	ascii_boolean_data,
	ascii_text_data,

	// Binary //
	signed_byte_data,
	unsigned_byte_data,
	signed_short_data,
	unsigned_short_data,
	signed_int_data,
	unsigned_int_data,
	signed_long_data,
	unsigned_long_data,
	half_precision_float_data,
	float_data,
	double_data,
	long_double_data,
	string_data, // ? equivalent to ascii_data
	signed_int_24_data // custom 24-bit integer
} swe_data_encoding;


/*
 * Text Encoding structure
 * The “collapseWhiteSpaces” attribute is a boolean flag used to specify if extra white
 * spaces (including line feeds, tabs, spaces and carriage returns) surrounding the token and
 * block separators should be ignored (skipped) when processing the stream. This is useful
 * for encoded blocks of data that are embedded in an XML document, since formatting
 * (indenting with spaces or tabs especially) is often done in XML content.
 */
typedef struct{
	// Mandatory Elements //
	char* blockSeparator; //block separator
	char* tokenSeparator; //token separator

	// Optional Elements //
	uchar collapseWhiteSpaces; // default: TRUE
	// char* decimalSeparator;    // default: '.' NOT USED

	// Custom Elements //
	char* startToken; // default: NULL
}TextEncoding;



/*
 * Binary Encoding Component
 */
typedef struct{
	swe_data_encoding type;
	char* ref;
	char* id;
	char** path; // ref field, divided  in individual names (i.e. "output/field" => {"output", "field"}
	int bitLength;
	int byteLength;
}BinaryEncodingComponent;


/*
 * Binary Encoding Structure
 */
typedef struct{
	char* id;
	endianness byteOrder;
	byte_encoding byteEncoding;
	BinaryEncodingComponent** components;
}BinaryEncoding;

/*
 * XML Encoding Component
 */
typedef struct {
	char* ref;
	char* dataType; // data type is the XML tag where data is stored
	char** path; // ref field, divided  in individual names (i.e. "output/field" => {"output", "field"}
}XMLEncodingComponent;


/*
 * XML Encoding Structure
 */
typedef struct{
	XMLEncodingComponent **components;
}XMLEncoding;




//----------------------------------------------------------------------------//
//---------------------------- miscellaneous ---------------------------------//
//----------------------------------------------------------------------------//

/*
 * Limits for binary to ASCII float conversion
 */
#define LOWER_LIMIT_DECIMAL_PRECISION 0
#define UPPER_LIMIT_DECIMAL_PRECISION 10
#define PRECISION_CONVERTION_CODES {"%.0f", "%.1f", "%.2f", "%.3f", "%.4f", "%.5f","%.6f","%.7f", "%.8f", "%.9f", "%.10f"}

/*
 * Debugging options
 */
#define SWE_DATA_MODEL_DEBUG_FREE ON

#define DUMMY_POINTER_ADDRESS (void*)0x1111 // this is a non-NULL address that marks a pointer as non-accessible
                                     // it is used in NULL-terminated arrays to mark positions that can't
                                     // be accessed without using a NULL pointer, which could cause conflicts

// Define ENABLED and DISABLED for more human-readable status  in SWE Data Field //
#ifndef ENABLED
#define ENABLED 1
#endif

#ifndef DISABLED
#define DISABLED 0
#endif


/*
 * Simplified version of data where
 */
typedef struct{
	uchar* value; // Pointer where the data is stored
	swe_data_encoding encoding; // encoding of the data stored in value
	uint total_size; // Total size of the data
	uint element_size; // Size of each element
	uint element_count; // If count greater than 1 this element is an array
	char* name; // name of the variable
}SWE_Data;


#define ASCII_QUANTITY_BYTES 20
#define ASCII_COUNT_BYTES 20
#define ASCII_BOOLEAN_BYTES 10
#define ASCII_TEXT_BYTES 256


//----------------------------------------------------------------------------//
//----------------------------- Functions  -----------------------------------//
//----------------------------------------------------------------------------//




/*
 * Generic functions to work with Fields
 */
int field_component_type_set(Field* field, swe_component_type type);
int field_component_label_set(Field* field, const char* label);
const char* field_component_label_get(Field* field);
int field_component_nilvalue_set(Field* field, char* nilvalue);
char** field_component_nilvalues_get(Field* field);
int field_component_value_set_static(Field* field, const char* value);
int field_component_value_get_ascii(Field* field, char* buffer, int bufflen);
const char* field_component_definition_get(Field* field);

/*
 * Field's component value getters
 */
int field_text_value_get(Field* field, char* buffer, int bufflen);
int field_category_value_get(Field* field, char* buffer, int bufflen);
int field_quantity_value_get(Field* field, float64 *buffer);
int field_time_value_get(Field* field, float64 *buffer);
int field_count_value_get(Field* field, int *buffer);
int field_boolean_value_get(Field* field, uchar *buffer);




// Get Functions //
Field* get_field_by_name(Field** list, const char* name);
int get_count_value_by_name(Field** list, const char* name, int* value);
int get_quantity_value_by_name(Field** list, const char* name, float64* value);
int get_time_value_by_name(Field** list, const char* name, float64* value);
int get_boolean_value_by_name(Field** list, const char* name, uchar* value);
int get_text_value_by_name(Field** list, const char* name, char* buffer, int bufflen);
int get_category_value_by_name(Field** list, const char* name, char* buffer, int bufflen);

Field* new_swe_data_field(int level, Field* parent);
uchar check_if_complex(swe_component_type type);
uchar check_if_basic(swe_component_type type);
Field* get_swe_data_parent(Field* current, int inputlevel);
Field* new_swe_data_field(int level, Field* parent);
int attach_swe_field_to_parent(Field* field, Field* parent);
Field* find_last_swe_field(Field* current, swe_component_type type);
const char* swe_component_type_text(swe_component_type type);
const char* swe_data_encoding_text(swe_data_encoding type);
char* get_field_id(Field* field);

// Get Binary Encodings //
swe_data_encoding get_data_type(const char* string);

// Print SWE Data model Components //

// Byte order //
endianness get_byte_order(char* string);

// Get binary encoding //
byte_encoding get_byte_encoding(char* string);

// Get Encodings //
BinaryEncodingComponent* get_binary_encoding_component_by_field(Field* field, BinaryEncoding* be);
XMLEncodingComponent* get_xml_encoding_component_by_field(Field* field, XMLEncoding* xe);
BinaryEncodingComponent* get_binary_encoding_component(BinaryEncoding* bec);

// Inherit functions //


/*
 * If to is empty and from is not, copy "from" to "to"
 */
#define INHERIT_STRING(_to, _from, _property) ({ \
		if (_to->_property == NULL && _from->_property != NULL) { \
			_to->_property = set_string(_from->_property); \
		} \
	})


int inherit_field(Field* to, Field* from);

int inherit_string(char* to, char* from);




// Conversion functions //
float64 swe_data_to_double(SWE_Data* data);
int double_to_swe_data(float64 value, SWE_Data* data);
float64 convert_to_float64(uchar* value, swe_data_encoding e);


// Free Functions //
int free_field(Field* field);

// Print Functions //
int print_swe_value(void* value, swe_data_encoding e);
void print_swe_data(SWE_Data* d);
int print_data_structure(Field* root, int level);
int print_swe_component_field(Field* field, int level);
int print_field_list(Field** list);
int print_swe_encoding(Encoding* encoding, int level);
const char* endianness_text(endianness e);


uchar check_if_binary(swe_data_encoding e);
int swe_encoding_to_string(char* buffer, void* data, swe_data_encoding e, int precision);
uint get_size_by_encoding(swe_data_encoding e);


#if SWE_DATA_MODEL_DEBUG_FREE
#define SDM_DEBUG(x) x
#else
#define SDM_DEBUG(x)
#endif

#endif

