/*
 * platform_config.h
 *
 *  Created on: Sep 17, 2018
 *      Author: enoc
 */


#ifndef RESOURCES_INC_PLATFORM_CONFIG_H_
#define RESOURCES_INC_PLATFORM_CONFIG_H_


/*
 * Choose one (and only one) of the following platforms
 */
#define GENERIC_LINUX
//#define PIROS
//#define COSTOF2
//#define SENSORBOX
//#define RASPBERRY_PI

#ifdef GENERIC_LINUX
#include "platforms/linux/linux_conf.h"
#elif defined COSTOF2
#include "costof2_conf.h"
#elif defined PIROS
#include "platforms/piros/piros_conf.h"
#elif defined SENSORBOX
#include "platforms/sensorbox/sensorbox_config.h"
#elif defined RASPBERRY_PI
#include "platforms/raspberry_pi/raspberry_pi_conf.h"
//--- Add new platform include here ---//
#elif defined EXAMPLE
#include "platforms/example/example_conf.h"

#endif



#endif /* RESOURCES_INC_PLATFORM_CONFIG_H_ */
