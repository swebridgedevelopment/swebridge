#ifndef RESOURCES_PLATFORMS_SENSORBOX_INC_SENSORBOX_H_
#define RESOURCES_PLATFORMS_SENSORBOX_INC_SENSORBOX_H_

#include "platform_config.h"
#ifdef SENSORBOX

//---- Include SENSORBOX resources ----//
#include "platforms/linux/linux_tcp_udp.h"
#include "platforms/linux/linux_uart.h"
#include "platforms/linux/linux_filesystem.h"
#include "platforms/linux/linux_timer.h"
#include "platforms/sensorbox/sensorbox.h"
#include "platforms/linux/linux.h"

//---------------------------------------------------------------------------//
//-------------------- DEFINE SENSORBOX FUCNTIONS -----------------------//
//---------------------------------------------------------------------------//
int init_sensorbox_platform(int argc, char** argv);
int sensorbox_get_coordinates(float* latitude, float* longitude);
int sensorbox_standby();
int exit_sensorbox_platform();
int sensorbox_fflush();


//---------------------------------------------------------------------------//
//-------------------- DEFINE SENSORBOX HANDLERS ------------------------//
//---------------------------------------------------------------------------//

/* UART Functions */
#define platform_open_uart linux_open_uart
#define platform_read_uart linux_read_uart
#define platform_write_uart linux_write_uart
#define platform_close_uart linux_close_uart
#define	platform_fflush_uart linux_fflush_uart
#define	platform_set_baudrate linux_set_baudrate
#define platform_set_uart_interrupt linux_set_uart_interrupt

/* TCP Functions */
#define platform_open_tcp linux_open_tcp
#define platform_read_tcp linux_read_tcp
#define platform_write_tcp linux_write_tcp
#define platform_close_tcp linux_close_tcp
#define	platform_fflush_tcp linux_fflush_tcp
#define platform_set_tcp_interrupt linux_set_tcp_interrupt

/* UDP Functions */
#define platform_open_udp linux_open_udp
#define platform_read_udp linux_read_udp
#define platform_write_udp linux_write_udp
#define platform_close_udp linux_close_udp
#define	platform_fflush_udp linux_fflush_udp
#define platform_set_udp_interrupt linux_set_udp_interrupt


/* FILESYSTEM Functions */
#define platform_fopen linux_fopen
#define platform_fclose linux_fclose
#define platform_fwrite linux_fwrite
#define platform_fread linux_fread
#define platform_create_dir linux_create_dir
#define platform_fremove linux_fremove
#define platform_file_size linux_file_size
#define platform_fmove linux_fmove
#define platform_getc linux_getc
#define platform_eof linux_eof
#define platform_set_pointer linux_set_pointer
#define platform_list_dir linux_list_dir


/* TIMING Functions */
#define platform_delayms linux_delayms
#define platform_get_time linux_get_time
#define platform_get_epoch_time linux_get_epoch_time
#define platform_set_timer linux_set_timer
#define platform_start_timer linux_start_timer
#define platform_stop_timer linux_stop_timer
#define platform_set_timeout linux_set_timeout
#define platform_disable_timeout linux_disable_timeout

/* Platform Functions */
#define platform_init init_sensorbox_platform
#define exit_sensorbox_platform exit_sensorbox_platform
#define platform_standby sensorbox_standby
#define platform_deep_sleep linux_deep_sleep
#define platform_vprintf vprintf
#define platform_vfprintf vfprintf
#define platform_fflush sensorbox_fflush
#if (( LINUX_UDP_NOTIFICATIONS == TRUE ))
#define platform_notify linux_udp_notify
#endif


/* Platform Functions */
#define platform_init init_sensorbox_platform
#define platform_exit exit_sensorbox_platform
#define platform_standby sensorbox_standby
#define platform_get_coordinates sensorbox_get_coordinates
#define platform_system_monitor NULL




#endif /* SENSORBOX */
#endif /* RESOURCES_PLATFORMS_SENSORBOX_INC_SENSORBOX_H_ */
