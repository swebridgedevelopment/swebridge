
 /*
 ============================================================================
								waveglider_plocan.c
 ============================================================================
 This file includes all the functions related to the implementation of this
 software in the waveglider_plocan platform.
 ============================================================================
 by SARTI
 Author: Enoc Martínez
 ============================================================================
 */

#include "swe_conf.h"

#ifdef SENSORBOX

#include "platforms/sensorbox/sensorbox.h"
#include "common/swe_utils.h"
#include "resources/resources.h"
#include "core/scheduler.h"
#include "swe_bridge.h"

#include <signal.h>
#include <unistd.h>
#include <sys/time.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <fcntl.h>


//---- Private functions ----//
void position_interrupt_handler();
int open_position_socket(); //function to open an UDP socket to receive the position
int parse_sensorbox_position(char* buffer);


int set_sigquit_handler();
void sigquit_handler();

typedef struct{
	float latitude;
	float longitude;
	uchar valid_data; //flag to used to verify if data is usable or not
}SensorBoxPosition;

SensorBoxPosition position; //structure to store the platform position


char position_buffer[SENSORBOX_POSITION_BUFFER]; //buffer where the last packet is stored
uchar new_packet=FALSE; //flag to determine if a new position packet has been received

int socket_fd=0;

/**************************************************************************
 	 	 	 	 	 	 	 init_waveglier_plocan_platform
 *************************************************************************
 * Initialize the necessary software components for the waveglier_plocan
 * platform:
 *
 * 	1. Open a UDP port to receive the position packets and configure a SIGIO signal
 * 	2. Configure the SIGQUIT handler
 *
 *************************************************************************/
int init_sensorbox_platform(int argc, char** argv){
	uint port;
	cimsg(BLU, "Initializing SensorBox Environment");
	if (argc != 1) {
		errmsg("init_sensorbox_platform expected 1 argument with the position UDP port");
		return swe_invalid_arguments;
	}
	dmsg( "Platform options \"%s\"", argv[0]);
	memset(&position, 0, sizeof(position)); //initialize the position structure

	// STEP 1: Open the position UDP port
	if(argv[0]==NULL){
		errmsg( "No options, expected UDP position port");
		return swe_invalid_arguments;
	}
	if((port=atoi(argv[0]))==0){
		errmsg( "Port number \"%s\" not valid", argv[0]);
	}

	if((socket_fd=open_position_socket(port))<0){
		errmsg( "Error opening UDP port for position");
		return socket_fd; //socket_fd contains the errorcode
	}
	//STEP 2: Configure the SIGQUIT signal handler
	set_sigquit_handler();

	return swe_ok;
}




/*
 * Exit function for generic linux platforms
 */
int exit_sensorbox_platform(){


	exit(0);
	return swe_error;
}

/*
 * Sleep platform until a timer expires or a interface interruption is received
 */
int sensorbox_standby(){
	sleep(3600); //sleep for 1 hour
	return swe_ok;
}


void sigquit_handler(){
	errmsg( "received SIGQUIT, dying");
	// iface_close();
	swe_bridge_exit();
	exit_platform();
}
/**************************************************************************
 	 	 	 	 	 	 	 set_sigquit_handler
 **************************************************************************
 * Configure a handler for the SIGQUIT signal
 *
 * *************************************************************************/
int set_sigquit_handler(){
	//serTermio
	dmsg( "settingset_siquit_handler");
	sigset_t mask;
	struct sigaction sa;
    sigprocmask(SIG_SETMASK, NULL, &mask); //get the current mask
	sigaddset(&mask,SIGQUIT); //add the new signal (user signal 1)
	sa.sa_sigaction = sigquit_handler; //define the handler function
	sa.sa_flags = 0;  //no flags needed
	sigemptyset(&sa.sa_mask); //Empty the signal mask (this mask blocks other signals during the execution of the signal handler)
 	if (sigaction(SIGQUIT, &sa, NULL) == -1) { //set the sigaction to the signal
    	return swe_error;
    }
 	sigaddset(&mask, SIGQUIT); //add the signal to the mask

	if (sigprocmask(SIGQUIT, &mask, NULL) == -1){
		return swe_error;
	}

	sigprocmask(SIG_UNBLOCK, &mask, NULL);
    return 0;
}



/**************************************************************************
 	 	 	 	 	 get the waveglider position
 **************************************************************************
 * Return the seaxplorer position:
 * 	1. If there's a new packet with the position pending, process it
 * 	2. if the position is valid return it, else return NULL
 *
 * *************************************************************************/

int sensorbox_get_coordinates(float*latitude, float*longitude) {
	sigset_t mask;

	if(new_packet==TRUE) {
		dmsg( "new position packet ready to be parsed");
		sigemptyset(&mask); //block the SIGIO signal
		sigaddset(&mask, POSITION_UDP_SIGNAL);
		sigprocmask(SIG_BLOCK,&mask,NULL);
		parse_sensorbox_position(position_buffer); //Fill the position structure
		sigprocmask(SIG_UNBLOCK,&mask,NULL); //unblock SIGIO
		new_packet=FALSE;
	}
	if(position.valid_data==TRUE){
		// If the data is valid return OK //
		*latitude = position.latitude;
		*longitude = position.longitude;
		return 0;
	}
	// If the data is not valid return error //
	return -1;
}



int parse_sensorbox_position(char* buffer){
	//read the last UDP port, update the value if necessary

	// the format is #<latitude>,<longitud>;

	SensorBoxPosition temp_position;
	char* latitude=NULL;
	char* longitude=NULL;
	int i=0;
	uchar latitude_found=FALSE, longitude_found=FALSE;
	int buff_len;
	fdmsg( "parsing new packet: [%s]", buffer);
	//check if the header matches
	buff_len=strlen(buffer);
	if(buff_len < 5) {
		errmsg( "packet is too small");
		return swe_wrong_response;
	}

	if(buffer[0] != '#') {
		//if the beginning does not match with the start token
		errmsg( "Start token not found");
		return swe_wrong_response;
	}

	latitude=buffer+1;


	for (i=1; i<buff_len; i++) {
		if(latitude_found==FALSE && buffer[i]==',') {
			buffer[i]=0;
			i++;
			longitude=buffer+i;
			latitude_found=TRUE;

		} else if (buffer[i]==';') {
			buffer[i]=0;
			longitude_found=TRUE;
			break;
		}
	}


	if(longitude_found == TRUE && latitude_found == TRUE){
		temp_position.latitude=atof(latitude);
		temp_position.longitude=atof(longitude);
	} else {
		warnmsg("couldn't parse the position data, keeping old position");
		return swe_wrong_response;
	}
	// Check that it is not 0
	if	((temp_position.latitude > -0.01) &&  (temp_position.latitude < 0.01) &&
		 (temp_position.longitude > -0.01) &&  (temp_position.longitude < 0.01)) {
			temp_position.valid_data = FALSE;
			warnmsg("Position is 0 0");
			return swe_wrong_response;
	}
	else if	((temp_position.latitude > -90) &&  (temp_position.latitude < 90) &&
			(temp_position.longitude > -90) &&  (temp_position.longitude < 90)) {
		temp_position.valid_data = TRUE;
	} else {
		warnmsg("couldn't parse the position data, keeping old position");
		return swe_wrong_response;
	}


	memcpy(&position, &temp_position, sizeof(SensorBoxPosition));

	//---- Uncomment to se the results of the UDP ----//

	cdmsg( MAG, "latitude %2.4f, longitude %2.4f", position.latitude, position.longitude);

	//copy the temporal position into the global platform position
	return swe_ok;
}


/**************************************************************************
 	 	 	 	 	 	 	 set_udp_interrupt
 **************************************************************************
 * Configures a software interrupt (signal) each time a packet is received
 * in the UDP port.
 *
 * *************************************************************************/
void position_interrupt_handler(){
	memset(position_buffer, 0, sizeof(position_buffer));
    int ret=read(socket_fd, position_buffer, SENSORBOX_POSITION_BUFFER);
    if(ret<1){
    	dmsg( "nothing to read in position_interrupt_handler");
    }
    else {
        cdmsg( YEL, "received position udp [%s]", position_buffer);
        new_packet=TRUE;
    }
}

/**************************************************************************
 	 	 	 	 	 	 	 open_position_socket
 *************************************************************************
 * Opens a UDP socket to receive the position and configures a SIGIO
 * signal each time something is received on the port
 * *************************************************************************/
int open_position_socket(uint port){
	uint length;
	struct sockaddr_in server;

	int sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock < 0) {
		errmsg( "error opening datagram socket");
		return swe_iface_error;
	}

	server.sin_family = AF_INET;
	server.sin_addr.s_addr = htonl(INADDR_ANY);
	server.sin_port = htons(port);

	if (bind(sock, (struct sockaddr *)&server, sizeof server) <0 ){
		errmsg( "error binding datagram socket");
		return swe_iface_error;
	}

	length = sizeof(server);
	if (getsockname(sock, (struct sockaddr *)&server, &length) < 0){
		errmsg( "error getting socket name");
		return swe_iface_error;
	}
#ifdef F_SETSIG
	fcntl(sock, F_SETSIG, POSITION_UDP_SIGNAL);
#else
	fcntl(sock, __F_SETSIG, POSITION_UDP_SIGNAL);
#endif
	signal(POSITION_UDP_SIGNAL,position_interrupt_handler); //assign a SIGIO handler

	// set the process id or process group id that is to receive
	// notification of pending input to its own process id or process
	// group id
	if (fcntl(sock,F_SETOWN, getpid()) < 0){
		errmsg( "error settings F_SETOWN");
		return swe_iface_error;
	}

	if (fcntl(sock,F_SETFL,FASYNC) <0 ){ //allow receipt of asynchronous I/O signals
		errmsg( "F_SETFL");
		return swe_iface_error;
	}
	return sock;
}

/*
 * fflush stdout
 */
int sensorbox_fflush() {
	fflush(stdout);
	return swe_ok;
}


#endif //waveglier_plocan_ENVIRONMENT
