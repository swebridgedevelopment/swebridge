/*
 * sensorbox_config.h
 *
 *  Created on: Sep 17, 2018
 *      Author: enoc
 */

#ifndef RESOURCES_PLATFORMS_SENSORBOX_INC_SENSORBOX_CONFIG_H_
#define RESOURCES_PLATFORMS_SENSORBOX_INC_SENSORBOX_CONFIG_H_

#ifdef SENSORBOX

// Include generic C Linux libraries //
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#define __USE_GNU

// Declare uchar, uint and ulong //
typedef uint8_t uchar;
typedef uint32_t uint;
//typedef uint64_t ulong;
typedef double float64;


// Declare file type //
typedef FILE swe_file;


// #define __USE_GNU


// Activate generic linux resources //
#define USE_GENERIC_LINUX_FILESYSTEM
#define USE_GENERIC_LINUX_UART
#define USE_GENERIC_LINUX_TCP_UDP
#define USE_GENERIC_LINUX_TIMER


// Other Configuration //
#define STARTING_ARGUMENT 1


#define PLATFORM_NAME "NeXOS SensorBox"

#define SENSORBOX_POSITION_BUFFER 512
#define POSITION_UDP_SIGNAL SIGUSR2

#define WAVEGLIDER_PLOCAN_POSITION_START_TOKEN "#"
#define WAVEGLIDER_PLOCAN_POSITION_END_TOKEN ";"
#endif


#endif /* RESOURCES_PLATFORMS_SENSORBOX_INC_SENSORBOX_CONFIG_H_ */
