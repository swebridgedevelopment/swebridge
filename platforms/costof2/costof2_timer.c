/*
 * costof2_timer.c
 *
 *  Created on: Apr 16, 2018
 *      Author: enoc
 */

#include "swe_conf.h"

#ifdef  USE_COSTOF2_TIMER
#include "common/swe_utils.h"
#include "swe_conf.h"

#include "platforms/costof2/costof2_timer.h"
#include "time.h"
#include "unistd.h"
#include "cmsis_os.h"



/*void osTimer_Callback(void const *arg);                    // prototype for timer callback function
osTimerDef (Timer, osTimer_Callback);                        // define timer
void oneShotTimer_Callback(void const *arg);
osTimerDef (oneShotTimer, oneShotTimer_Callback);
*/

void osTimer_Callback(void const* arg);
osThreadDef(osTimer_Callback, osPriorityAboveNormal, 0, 1024);


Costof2Timer *g_costof2Timer;



/*
 * Sleeps for n useconds
 */
int costof2_delayms(ulong msecs) {
	usleep(msecs*1000);
	return swe_ok;
}

/*
 * Writes the timestamp into buffer with the format 2018-12-31T23:59:59Z
 * Return value is the number of bytes written
 */
int costof2_get_timestamp(char* buffer){
	int nchars;
	time_t now;
	struct tm my_time;
	now=time(NULL);
	localtime_r((const time_t*)(&now), &my_time);
#if GMT_ZONE
	// If GMT ZONE is not 0
	nchars=sprintf(buffer, "%04d-%02d-%02dT%02d:%02d:%02d+%02d:00",
		my_time.tm_year + 1900, //tm_year is year - 100
		my_time.tm_mon +1,
		my_time.tm_mday,
		my_time.tm_hour,
		my_time.tm_min,
		my_time.tm_sec,
		GMT_ZONE);
#else
	// use UTC timestamping by default
		nchars=sprintf(buffer, "%04d-%02d-%02dT%02d:%02d:%02dZ",
			my_time.tm_year + 1900, //tm_year is year - 100
			my_time.tm_mon +1,
			my_time.tm_mday,
			my_time.tm_hour,
			my_time.tm_min,
			my_time.tm_sec);
#endif
	return nchars;
}

/*
 * Writes the date + time into the buffer with format YYYYMMDD_HHMMSS
 */
int costof2_get_date_time(char* buffer) {
	time_t now;
	struct tm* tmp;
	int chars;
	if(buffer == NULL) return swe_invalid_arguments;

	now=time(NULL);
	tmp = localtime(&now);
	chars=sprintf(buffer, "%02d%02d%02d_%02d%02d%02d",
			tmp->tm_year-100,
			tmp->tm_mon + 1,
			tmp->tm_mday,
			tmp->tm_hour,
			tmp->tm_min,
			tmp->tm_sec);
	return chars;
}

/*
 * Configures a periodic real-time signal
 */
int costof2_set_timer(ulong period_us, void(*handler)())
{
	int res=swe_ok;


	g_costof2Timer->sweTimerHandler=handler;
	g_costof2Timer->timerPeriodMs=period_us/1000;
	imsg("costof2_set_timer OK period=%d ms", g_costof2Timer->timerPeriodMs);



	return res;

}
/*
 * Starts a previously configured timer. Unblocks the timer signal from the program mask
 */
int costof2_start_timer(){

	int res=swe_ok;
	int status;
	if(g_costof2Timer->timerObj==NULL){

		//g_costof2Timer->timerObj=osTimerCreate (osTimer(Timer), osTimerPeriodic, NULL);
		g_costof2Timer->timerObj=osThreadCreate(osThread(osTimer_Callback),NULL);

		if(g_costof2Timer->timerObj==NULL){
			errmsg("Error creating COSTOF2 timer obj");
			res=swe_timer_error;
		}


	}else{
		errmsg(" COSTOF2 timer does not exist");
		res=swe_timer_error;
	}
	return res;
}

/*
 * Blocks the timer signal from the program mask
 */
int costof2_stop_timer(){

	int res=swe_ok;
	int status;
	if(g_costof2Timer->timerObj!=NULL){
		g_costof2Timer->exitTimer=true;
		sleep(1);
		status = osThreadTerminate (g_costof2Timer->timerObj);
		if (status != osOK)  {
			errmsg("Error Stopping COSTOF2 timer");
			res=  swe_timer_error;
		}
	}else {
		errmsg("%s: COSTOF2 timer does not exist",__FUNCTION__);
		res=swe_timer_error;
	}
	return res;
}


void osTimer_Callback(void const *arg)
{
	(void) (arg);
	while(g_costof2Timer->exitTimer==false){
		(*g_costof2Timer->sweTimerHandler)();
		usleep(g_costof2Timer->timerPeriodMs*1000);
	}
}


#endif //COSTOF2
