/*
 * costof2_tcp_udp.h
 *
 *  Created on: Apr 16, 2018
 *      Author: enoc
 */

#ifndef RESOURCES_PLATFORMS_COSTOF2_INC_COSTOF2_TCP_UDP_H_
#define RESOURCES_PLATFORMS_COSTOF2_INC_COSTOF2_TCP_UDP_H_

#include "swe_conf.h"
#ifdef USE_COSTOF2_TCP_UDP


#include "platforms/costof2/costof2.h"


typedef struct{
//	Linux variables
//	uint port;
//	char* IP;
//	uint max_size; //maximum packet size
//	int fd; //file descriptor
//	uint timeout;
//	struct sockaddr_in server;
} Costof2_TCP;

/*
 * TCP Protocol
 */
Interface_Descriptor* costof2_open_tcp(Interface_Options* opts);
int costof2_write_tcp(Interface_Descriptor* iface, void* buffer, uint size);
int costof2_read_tcp(Interface_Descriptor* iface,char* buffer, uint max_bytes,  char* end_token, ulong timeout_us);
int costof2_close_tcp(Interface_Descriptor* iface);
int costof2_fflush_tcp(Interface_Descriptor* iface);
int costof2_set_tcp_interrupt(Interface_Descriptor* iface, uchar* flag);

/*
 * UDP Protocol
 */

typedef Costof2_TCP Costof2_UDP; // use the same struct for TCP and UDP


Interface_Descriptor* costof2_open_udp(Interface_Options* opts);
int costof2_write_udp(Interface_Descriptor* iface, void* buffer, uint size);
int costof2_read_udp(Interface_Descriptor* iface, char* buffer, uint max_bytes,  char* end_token, ulong timeout_us);
int costof2_close_udp (Interface_Descriptor* iface);
int costof2_fflush_udp (Interface_Descriptor* iface);
int costof2_set_udp_interrupt(Interface_Descriptor* iface, uchar* flag);

#endif // costof2

#endif /* RESOURCES_PLATFORMS_COSTOF2_INC_COSTOF2_TCP_UDP_H_ */
