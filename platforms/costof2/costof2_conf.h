/*
 ============================================================================
				   COSTOF2 Configuration File
 ============================================================================
 This file configures the project for a generic Linux platform. The following


  I.  Define basic types (uchar, uint and ulon)

  II.  Define SWE file type

  III. Activate the proper handlers

 ============================================================================
 Author: Enoc Martínez
 Contact: enoc.martinez@upc.edu
 ============================================================================
 */

#ifndef RESOURCES_PLATFORMS_COSTOF2_INC_COSTOF2_CONF_H_
#define RESOURCES_PLATFORMS_COSTOF2_INC_COSTOF2_CONF_H_

#include "swe_conf.h"
#ifdef COSTOF2
// Include generic C Linux libraries //
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>


#include "libeasyfile.h"

// Declare uchar, uint and ulong //
typedef uint8_t uchar;
// typedef uint32_t uint;
// typedef uint64_t ulong;


typedef struct {
	int fd;
}Costof2_File;

typedef Costof2_File swe_file;

// Activate Costof2 resources //
#define USE_COSTOF2_FILESYSTEM
#define USE_COSTOF2_TCP_UDP
#define USE_COSTOF2_TIMER
#define USE_COSTOF2_UART


#define FORCE_CONFIG_FILE ON
#define STATIC_CONFIG_FILE "/SWE_Bridge/swe_bridge_setup.conf"
#define STARTING_ARGUMENT 0 // if we are using a file, first argument is also important



// Other Costof2 Configuration //
#define SWE_BRIDGE_STANDALONE OFF
#define ENVIRONMENT_32_BIT

// Do not use a timer //
#define SIMULATE_SCHEDULER_TIMER ON

// Do not compile EXIP library //
#define USE_EXIP_LIBRARY OFF

// Output files path //
#define TEMP_FOLDER_PATH "/SWE_Bridge/temp"
#define OUTPUT_FOLDER_PATH "/SWE_Bridge/files"



#define PLATFORM_NAME "COSTOF2"

#define SWE_BRIDGE_TEST_FOLDER "/SWE_Bridge/Test" // Only used by ssystem check



#define COSTOF2_STANDBY_SLEEP_TIME_US 200000 // Costof2 sleep time un usecs



#endif /* COSTOF2 */

#endif /* RESOURCES_PLATFORMS_COSTOF2_INC_COSTOF2_CONF_H_ */
