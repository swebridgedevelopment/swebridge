/*
 * costof2_filesystem.h
 *
 *  Created on: Apr 10, 2018
 *      Author: enoc
 */

#ifndef RESOURCES_LINUX_INC_costof2_FILESYSTEM_H_
#define RESOURCES_LINUX_INC_costof2_FILESYSTEM_H_

#include "swe_conf.h"

#ifdef USE_COSTOF2_FILESYSTEM

/*
 * SWE Bridge wrapper for open file
 */
swe_file* costof2_fopen(const char* filename, swe_file_flags flags);
int costof2_fclose(swe_file* file);
int costof2_fwrite(const void* data, int bytes, swe_file* file);
int costof2_fread(void* readData,int bytes, swe_file* file);
int costof2_create_dir(const char *dirname);
int costof2_fremove(const char *filename);
int costof2_file_size(swe_file* filename);
int costof2_fmove(const char* oldname, const char* newname);

int costof2_getc(swe_file* filename);
int costof2_eof(swe_file* filename);

#endif
#endif /* RESOURCES_LINUX_INC_costof2_FILESYSTEM_H_ */
