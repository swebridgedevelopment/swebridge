/*
 * costof2_uart.c
 *
 *  Created on: Apr 16, 2018
 *      Author: enoc
 */

#ifndef RESOURCES_PLATFORMS_COSTOF2_INC_COSTOF2_UART_H_
#define RESOURCES_PLATFORMS_COSTOF2_INC_COSTOF2_UART_H_

#include "swe_conf.h"
#ifdef USE_COSTOF2_UART


/*
 * Structure that holds the information for LINUX UART devices
 */
typedef struct{
	int fd;

} Costof2_UART;


#define SENSOR1_UART_ID "/dev/ttyS0"
#define SENSOR2_UART_ID "/dev/ttyS1"
#define SENSOR3_UART_ID "/dev/ttyLE0"

Interface_Descriptor* costof2_open_uart(Interface_Options* conf);
int costof2_write_uart(Interface_Descriptor* iface, void* buffer, uint size);
int costof2_read_uart(Interface_Descriptor* iface, uchar* buffer, uint max_bytes, ulong timeout_us);
int costof2_close_uart(Interface_Descriptor* iface);
int costof2_fflush_uart(Interface_Descriptor* iface);
int costof2_set_baudrate(Interface_Descriptor* iface, ulong baudrate);
int costof2_set_uart_interrupt(Interface_Descriptor* iface, uchar* flag);


#endif // COSTOF2

#endif /* RESOURCES_PLATFORMS_COSTOF2_INC_COSTOF2_UART_C_ */
