/********************************************************************
 *                        COSTOF2 Resources
 ********************************************************************
 *
 * SWE Bridge configuration for for Costof2 platform. Within this
 * file the following parameters need to be defined:
 *   I.  Costof2 resources (tcp, udp, uart , timer and filesytem)
 *
 *  II.  Define the handlers for the resources (platform_open_uart,
 *       platform_fopen, etc.)
 *
 * III. Define the handlers init and exit
 *
 *
 ********************************************************************/
#ifndef RESOURCES_PLATFORMS_COSTOF2_INC_COSTOF2_H_
#define RESOURCES_PLATFORMS_COSTOF2_INC_COSTOF2_H_

#include "swe_conf.h"
#ifdef COSTOF2

#include "ff.h"

//---------------------------------------//
//----  I. Include COSTOF2 resources ----//
//---------------------------------------//
#include "platforms/costof2/inc/costof2_tcp_udp.h"
#include "platforms/costof2/inc/costof2_uart.h"
#include "platforms/costof2/inc/costof2_timer.h"
#include "platforms/costof2/inc/costof2_filesystem.h"


//--------------------------------------//
//------ II. Platform functions  -------//
//--------------------------------------//
int init_costof2_platform(int argc, char** argv);
int exit_costof2_platform();
int standby_costof2();
int costof2_get_coordinates();
int costof2_system_monitoring();

//-------------------------------------//
//---- III. Handlers for resources ----//
//-------------------------------------//
/* UART Functions */
#define platform_open_uart costof2_open_uart
#define platform_read_uart costof2_read_uart
#define platform_write_uart costof2_write_uart
#define platform_close_uart costof2_close_uart
#define	platform_fflush_uart costof2_fflush_uart
#define	platform_set_baudrate costof2_set_baudrate
#define platform_set_uart_interrupt costof2_set_uart_interrupt

/* TCP Functions */
#define platform_open_tcp costof2_open_tcp
#define platform_read_tcp costof2_read_tcp
#define platform_write_tcp costof2_write_tcp
#define platform_close_tcp costof2_close_tcp
#define	platform_fflush_tcp costof2_fflush_tcp
#define platform_set_tcp_interrupt costof2_set_tcp_interrupt

/* UDP Functions */
#define platform_open_udp costof2_open_udp
#define platform_read_udp costof2_read_udp
#define platform_write_udp costof2_write_udp
#define platform_close_udp costof2_close_udp
#define	platform_fflush_udp costof2_fflush_udp
#define platform_set_udp_interrupt costof2_set_udp_interrupt

/* FILESYSTEM Functions */
#define platform_fopen costof2_fopen
#define platform_fclose costof2_fclose
#define platform_fwrite costof2_fwrite
#define platform_fread costof2_fread
#define platform_create_dir costof2_create_dir
#define platform_fremove costof2_fremove
#define platform_file_size costof2_file_size
#define platform_fmove costof2_fmove
#define platform_getc costof2_getc
#define platform_eof costof2_eof

/* TIMING Functions */
#define platform_delayms costof2_delayms
#define platform_get_date_time costof2_get_date_time
#define platform_get_timestamp costof2_get_timestamp
#define platform_set_timer costof2_set_timer
#define platform_start_timer costof2_start_timer
#define platform_stop_timer costof2_stop_timer
#define platform_set_timeout NULL
#define platform_disable_timeout NULL

/* Platform Functions */
#define platform_init init_costof2_platform
#define platform_exit exit_costof2_platform
#define platform_standby standby_costof2
#define platform_get_coordinates costof2_get_coordinates
#define platform_system_monitor costof2_system_monitoring



#endif /* COSTOF2 */
#endif /* RESOURCES_PLATFORMS_COSTOF2_INC_COSTOF2_H_ */
