/*
 * costof2_uart.c
 *
 *  Created on: Apr 16, 2018
 *      Author: enoc
 */

#include "swe_conf.h"

#ifdef USE_COSTOF2_UART

#include "common/swe_utils.h"
#include "resources/mem_control.h"
#include "platforms/costof2/costof2_uart.h"
#include "specs.h"
#include "specs_target.h"
#include "linux/apm.h"
#include "libeasyserial.h"
#include "libeasyapm.h"

Interface_Descriptor* costof2_open_uart(Interface_Options* opts){
	Costof2_UART* UART = NULL;
	int costof_sensor_num;
	// Checking input
	if(opts == NULL || opts->serial_device==NULL){
		errmsg( "NULL pointer received in linux_open_uart");
		return NULL;
	}

	imsg( "Opening Costof2 UART...");
	UART = (Costof2_UART*)swe_malloc(sizeof(Costof2_UART));
	if(UART==NULL){
		errmsg("swe_malloc error UART ");
		return NULL;
	}

	// Check if Serial Device is valid and get sensor number

	if (!strcmp(opts->serial_device, SENSOR1_UART_ID)) {
		costof_sensor_num = 0;
	}
	else if (!strcmp(opts->serial_device, SENSOR2_UART_ID)) {
		costof_sensor_num = 1;
	}

	else if (!strcmp(opts->serial_device, SENSOR3_UART_ID)) {
		costof_sensor_num = 2;
	}
	else {
		errmsg("Serial device \"%s\" does not exist!", opts->serial_device);
		return NULL;
	}


	dmsg("Opening port \"%s\"", opts->serial_device);
	//open serial port and assign a file descriptor
	UART->fd =les_open(opts->serial_device);
	if(UART->fd < 0)   {
		errmsg("unable to open comport %s", opts->serial_device);
		return NULL;
	}

	//set baudrate
	dmsg("Setting baudrate %d", opts->baudrate);
	if (les_set_baudrate(UART->fd, opts->baudrate) < 0 ) {
		errmsg("Configuring baudrate %u for Costof2 serial port ", opts->baudrate, opts->serial_device);
	}
	dmsg("Costof2 UART fd is %d", UART->fd);

	if (opts->sw_flot_control == TRUE ) {
		dmsg("activating software flow control");
		les_enable_rx_sw_flow_control(UART->fd, TRUE);
	} else {
		dmsg("Software flow control is disabled");
	}

	// Power UART on
	int apm_list[] = {
			apmIOCTL_SENSOR_PWR(costof_sensor_num),
			apmIOCTL_SENSOR_RS232_TX(costof_sensor_num)
		};
	lea_apm_set(apm_list, LEN(apm_list), 1);

	// fflush uart just in case
	les_resetRxFifo(UART->fd);

	cdmsg(CYN,"File descriptor is %d!", UART->fd);
	return (Interface_Descriptor*)UART;
}


int costof2_set_baudrate(Interface_Descriptor* iface, ulong baudrate_in){
	Costof2_UART *UART =  (Costof2_UART *)iface;

	return les_set_baudrate(UART->fd, baudrate_in);
}



int costof2_write_uart(Interface_Descriptor* iface, void* buffer, uint size){
	Costof2_UART *UART =  (Costof2_UART *)iface;

	return les_write(UART->fd, 5000, (char*) buffer, size);
}

int costof2_fflush_uart(Interface_Descriptor* iface)
{
	Costof2_UART *UART =  (Costof2_UART *)iface;
	return les_resetRxFifo(UART->fd);
}

int costof2_read_uart(Interface_Descriptor* iface, uchar* buffer, uint max_bytes, ulong timeout_us)
{
	int bytes_to_read;
	int nbytes = 0;
	Costof2_UART *uart =  (Costof2_UART *)iface;
	dmsg("reading from UART fd %d", uart->fd);
	if (uart->fd <= 0) {
		return(swe_iface_error);
	}


	// If timetout is 0 ask read and exit
	if (timeout_us == 0){
		bytes_to_read = les_getRxNbBytesAvailable(uart->fd);
		bytes_to_read = MIN(bytes_to_read, max_bytes);
		return les_read(uart->fd, 0, buffer , bytes_to_read);
	}

	// Otherwise read with timeout

	int end_token_length;
	uchar tmout_flag=FALSE;
	uint char_tmoutMs=timeout_us/1000;
	dmsg("function %s, timeout ms is %u", __func__, char_tmoutMs);
	struct timeval t;
	gettimeofday(&t, NULL);

	ulong start=t.tv_sec*1000000+t.tv_usec;
	ulong now;

	tmout_flag=FALSE;
	while((!tmout_flag) && (nbytes<(int)max_bytes)) {
		gettimeofday(&t, NULL);
		now=t.tv_sec*1000000+t.tv_usec;
		if(now > (start + timeout_us)){
			tmout_flag=TRUE;
		}
		int tmp_bytes=les_read(uart->fd, 0, buffer+nbytes , (max_bytes-nbytes));

		if(tmp_bytes<0){
			errmsg( "Uart error, %d", tmp_bytes);
			if (nbytes > 0) { // If we already have some data ignore the error
				return nbytes;
			}
			else { //Otherwise return the errror
				return tmp_bytes;
			}
		}
		nbytes+=tmp_bytes;
	}

	if(tmout_flag==TRUE) {
		warnmsg( "linux_read_uart timeout (%u usecs)", timeout_us);
	}

	return nbytes;

}

int costof2_close_uart(Interface_Descriptor* iface)
{
	Costof2_UART *UART =  (Costof2_UART *)iface;
	return les_close(UART->fd);
}

extern void rxCallBackUart(void *arg)
{

	uchar* flag=(uchar*)arg;
	*flag=TRUE;
}

int costof2_set_uart_interrupt(void *iface, uchar* flag){
	Costof2_UART *UART =  (Costof2_UART *)iface;
	cdmsg(BLU, "%s", __func__);
	dmsg("function soriginal direction is %p, flag pointer %p", rxCallBackUart);

	return les_addRxDirectCallBack(UART->fd , rxCallBackUart, (void*)flag);
}

#endif
