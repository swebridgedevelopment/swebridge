/*
 * costof2.c
 *
 *  Created on: Apr 17, 2018
 *      Author: enoc and Bertrand MOREAU
 */

#include "swe_conf.h"
#ifdef COSTOF2
#include "platforms/costof2/inc/costof2.h"
#include "platforms/costof2/inc/costof2_timer.h"

/*
 *  To monitor system status use 	monitoringDisplaySystemInfo(0);
 */


/*
 * Initialization function for costof2
 */
int init_costof2_platform(int argc, char** argv){


	//init timer obj structure
	int res=swe_ok;
	g_costof2Timer=(Costof2Timer*)swe_malloc(sizeof(Costof2Timer));

	if(g_costof2Timer==NULL){
		//LOG malloc error
		res=-1;
	}

	g_costof2Timer->exitTimer=false;
	g_costof2Timer->timerObj=NULL;


	return res;
}

/*
 * Exit function for costof2
 */
int exit_costof2_platform(){



	swe_free(g_costof2Timer);


	// TODO close any open files

	// TODO deinit filesystem

	// TODO close any open communication's interface

	// TODO exit

	return swe_error;
}


int standby_costof2() {
	usleep(COSTOF2_STANDBY_SLEEP_TIME_US);
	return swe_ok;
}


/*
 * Get the position, unimplemented by default
 */
int costof2_get_coordinates(Coordinates* coordinates) {
	warnmsg("Costof2 is a fixed-point platform!!!");
	coordinates->latitue = 0.000;
	coordinates->longitude = 0.000;
	coordinates->depth= 0.000;
	return swe_ok;
}


int costof2_system_monitoring() {
	monitoringDisplaySystemInfo(0);
	return swe_ok;
}
#endif /* COSTOF2 */
