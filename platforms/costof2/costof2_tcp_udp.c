/*
 * costof2_tcp_udp.c
 *
 *  Created on: Apr 16, 2018
 *      Author: enoc
 */


#include "swe_conf.h"

#ifdef USE_COSTOF2_TCP_UDP

#include "common/swe_utils.h"
#include "platforms/costof2/inc/costof2_tcp_udp.h"


/********************************************************************
 *                     Costof2 TCP Protocol
 *******************************************************************/

/*
 * Open TCP socket
 */
Interface_Descriptor* costof2_open_tcp(Interface_Options* opts){
	Costof2_TCP* costof2_tcp = NULL;
	return NULL;
	return costof2_tcp;
}

int costof2_write_tcp(Interface_Descriptor* iface, void* buffer, uint size){
	return swe_unimplemented;
}

int costof2_read_tcp(Interface_Descriptor* iface,char* buffer, uint max_bytes,  char* end_token, ulong timeout_us){
	int bytes = 0;
	return swe_unimplemented;
	return bytes;
}

int costof2_close_tcp(Interface_Descriptor* iface){
	return swe_unimplemented;
}

int costof2_fflush_tcp(Interface_Descriptor* iface){
	return swe_unimplemented;
}

int costof2_set_tcp_interrupt(Interface_Descriptor* iface,uchar* flag){
	return swe_unimplemented;
}


/********************************************************************
 *                    Costof2 UDP Protocol
 *******************************************************************/
Interface_Descriptor* costof2_open_udp(Interface_Options* opts){
	Costof2_UDP* costof2_udp_iface=NULL;
	return NULL;
	return (Interface_Descriptor*)costof2_udp_iface;
}

int costof2_close_udp (Interface_Descriptor* iface){
	return swe_unimplemented;
}


int costof2_write_udp(Interface_Descriptor* iface, void* buffer, uint size){
	return swe_unimplemented;
}

int costof2_read_udp(Interface_Descriptor* iface, char* buffer, uint max_bytes,  char* end_token, ulong timeout_us){
	int bytes = 0;
	return swe_unimplemented;
	return bytes;
}

int costof2_set_udp_interrupt(Interface_Descriptor* iface, uchar* flag){
	return swe_unimplemented;
}

int costof2_fflush_udp(Interface_Descriptor* iface){
	return swe_unimplemented;
}




#endif
