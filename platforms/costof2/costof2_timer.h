/*
 * _timer.h
 *
 *  Created on: Apr 16, 2018
 *      Author: enoc
 */

#ifndef RESOURCES_PLATFORMS_COSTOF2_INC_COSTOF2_TIMER_H_
#define RESOURCES_PLATFORMS_COSTOF2_INC_COSTOF2_TIMER_H_

#include "swe_conf.h"
#ifdef USE_COSTOF2_TIMER
#include <cmsis_os.h>


int costof2_delayms(ulong msecs);
int costof2_get_date_time(char* buffer);
int costof2_get_timestamp(char* buffer);
int costof2_set_timer(ulong tmout_interval_ms, void(*handler)());
int costof2_start_timer();
int costof2_stop_timer();






typedef struct{

	void(*sweTimerHandler)();
	osTimerId timerObj;
	uint32_t  timerPeriodMs;
	bool exitTimer;





}Costof2Timer;

extern Costof2Timer *g_costof2Timer;


#endif

#endif /* RESOURCES_PLATFORMS_COSTOF2_INC_COSTOF2_TIMER_H_ */
