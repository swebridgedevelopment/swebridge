#!/bin/bash
#---------------------------------------------------------------------#
#            COSTOF2 SWE Bridge Makefile Generator                    #
#---------------------------------------------------------------------#
# This script generates automatically a makefile for the SWE Bridge   #
# to include it in the COSTOF2 project                                #
#                                                                     #
# Please modify the "path" and "output" variables before running the  #
# script!                                                             #
#---------------------------------------------------------------------#

# project path
path="/home/enoc/workspace/costof2/firmware2/ROOTFS/src/SWE_Bridge/swe_bridge/"

output="/home/enoc/workspace/costof2/firmware2/ROOTFS/src/SWE_Bridge/swe_bridge/Makefile.mk"

print() {
	echo "$1"
	echo "$1" >> $output
}


#============= SCRIPT STARTS HERE==================#

set -o errexit  # Exits script on error          
set -o nounset  # Detects uninitialized variables on the script and exits with error

rm $output
print "#---------------------------------------------------------#"
print "#                  SWE Bridge Makefile                    #"
print "#---------------------------------------------------------#"
print "# Automatically generated makefile for SWE Bridge project #"
print "# To update the Makefile run the script:                  #"
print "#    ./generate_swebridge_makefile.sh                     #"
print "#                                                         #"
print "# DO NOT EDIT MANUALLY!                                   #"
print "#---------------------------------------------------------#"

print ""
print ""
print "SWE_BRIDGE_SRC_DIR:= \$(dir \$(lastword \$(MAKEFILE_LIST)))"
print ""
print ""
print "# Include Paths #"

print "INCLUDEPATHS += \\" # include paths array

# search for folder named "inc"
pathArray=$(find $path  -name "inc")
for p in $pathArray ; do
	print "        \$(SWE_BRIDGE_SRC_DIR)${p//$path//} \\" # add the file replacing the path for nothing
done

print ""
print ""

print "# Source Files #"
print "C_SRC += \\" # source files array
# Search for .c files
pathArray=$(find $path  -name "*.c") # Array of source files
count=0
for p in $pathArray ; do
	print "        \$(SWE_BRIDGE_SRC_DIR)${p//$path//} \\" # add the file replacing the path for nothing

done





