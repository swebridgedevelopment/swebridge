#include "resources/mem_control.h"
#include "swe_conf.h"
/*
 * Static Filesystem structure
 */

#ifdef USE_COSTOF2_FILESYSTEM
#include "common/swe_utils.h"
#include "libfilename.h"

/*
 * Costof2 Open Function
 */
swe_file* costof2_fopen(const char* const_filename, swe_file_flags flags){
	(void)flags; // avoid warning

	// Adding the /mnt automatically
	int temp_len = strlen(const_filename) + 4; // space for /mnt + the rest of the const_filename
	char filename[temp_len + 1]; // Declare a tmp variable to host the real filename
	memcpy(filename, "/mnt", 4); // Add the mountpoint
	memcpy(filename + 4, const_filename, temp_len); // copy the filename
	filename[temp_len] = 0; // finish the string

	swe_file * file = fastMalloc(sizeof(swe_file));

	int fd;
	//Try to open the file but do not create it if it does not exist.
	dmsg("opening file %s", filename);
	int open_flags;
	if (flags == swe_read_file) {
		open_flags = O_RDWR | S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
	} else if (flags == swe_append_file){
		open_flags = O_RDWR | O_APPEND | S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;;
	} else if (flags == swe_write_file) {
		open_flags = O_RDWR | O_CREAT | S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;;
	} else {
		errmsg("unknown flags in open file");
		return NULL;
	}
	fd = open(filename,open_flags);
	if (fd < 0) {
		warnmsg("open returned %d, trying to create dir", fd);
		if (lfn_mkpath(filename, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) < 0) {
			warnmsg("Can't create path %s", filename);
		}
		fd  = open(filename,open_flags);
	} else {
		dmsg("File descriptor %d", fd);
	}
	if (fd < 0) {
		errmsg("Second open ret %d", fd);
		return NULL;
	}
	file->fd = fd;
	return file;
}

/*
 * Costof2 Close function
 */
int costof2_fclose(swe_file* file){
	int ret = close(file->fd);
	fastFree(file);
	return ret;
}

/*
 * Costof2 fwrite
 */
int costof2_fwrite(const void* data, int bytes, swe_file* file){
	/*int bytes_written = lef_write(file, data, bytes);
	if (bytes_written < 0 ) {
		errmsg("lef_write returned %d", bytes_written);
		return swe_filesystem_error;
	}*/
	int bytes_written = write(file->fd, data, bytes);
	return bytes_written;
}


int costof2_fread(void* readData,int bytes, swe_file* file){
/*	int bytes_read = lef_read(file, readData, bytes);
	if (bytes_read < 0 ) {
		return swe_filesystem_error;
	} */

	return read(file->fd, readData, bytes);

}

/*
 * Create directory
 */
int costof2_create_dir(const char *dirname){
	/*int ret = mkdir(dirname,(
							 S_IRWXU  //read and write owner permissions
						   | S_IRWXG  //read and write user group permissions
						   | S_IRWXO )); //read and write other user permissions */

	dmsg("dirname %s", dirname);
	int ret = mkdir(dirname, 664);
	if(ret<0){
		return swe_filesystem_error;
	}
	return swe_ok;
}

/*
 * Remove File
 */
int costof2_fremove(const char *filename){
	if(remove(filename) < 0){
		return swe_filesystem_error;
	}
	return swe_ok;
}


int costof2_file_size(swe_file* file){
	uint size;
	size = lseek(file->fd, 0, SEEK_END);
	lseek(file->fd, 0, SEEK_SET);
	return size; // return size
}


int costof2_fmove(const char* oldname, const char* newname){
	if (rename (oldname, newname) < 0 ){
		return swe_filesystem_error;
	}
	return swe_ok;
}


int costof2_getc(swe_file* file){
	char buff[1];
	if (read(file->fd, &buff, 1) < 0) {
		return swe_filesystem_error;
	}
	return (int)buff[0];

}

int costof2_eof(swe_file* file){
	return eof(file->fd);
}


#endif /*USE_COSTOF2_FILESYSTEM*/
