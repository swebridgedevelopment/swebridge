/*
 ============================================================================
				   PIROS Platform Power Management
 ============================================================================
 Implementation of platform handlers for piros platform
 ============================================================================
 Author: Enoc Martínez
 Contact: enoc.martinez@upc.edu
 ============================================================================
 */
#include "swe_conf.h"

#if defined USE_PIROS_POWER
#include "common/swe_utils.h"
#include "platforms/piros/inc/piros_power.h"


int piros_power_on_sensor(int sensorid) {
	return swe_unimplemented;
	return swe_ok;
}


int piros_power_off_sensor(int sensorid) {
	return swe_unimplemented;
	return swe_ok;
}

#endif
