/*
============================================================================
			   PIROS Platform Power Management
============================================================================
Declaration of platform handlers for piros platform
============================================================================
Author: Enoc Martínez
Contact: enoc.martinez@upc.edu
============================================================================
*/

#ifndef RESOURCES_PLATFORMS_PIROS_INC_PIROS_POWER_H_
#define RESOURCES_PLATFORMS_PIROS_INC_PIROS_POWER_H_

#include "swe_conf.h"

#if defined USE_PIROS_POWER
#include "common/swe_utils.h"
#include <piros/inc/piros_power.h>


int piros_power_on_sensor(int sensorid);
int piros_power_off_sensor(int sensorid);

#endif /* PIROS_POWER */

#endif /* RESOURCES_PLATFORMS_PIROS_INC_PIROS_POWER_H_ */
