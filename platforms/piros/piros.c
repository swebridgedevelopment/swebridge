/*
 ============================================================================
				   PIROS Platform system
 ============================================================================
 Implementation of platform handlers for piros platform
 ============================================================================
 Author: Enoc Martínez
 Contact: enoc.martinez@upc.edu
 ============================================================================
 */
#include "swe_conf.h"

#if defined USE_PIROS_PLATFORM

#include "platforms/piros/inc/piros.h"

/*
 * Initialization function for generic piros platforms
 */
int piros_platform_init(int argc, char** argv){
	imsg("Initializing PIROS Platform");
	// Initialize PIROS platform //
	return swe_unimplemented;
}

/*
 * Exit function for generic piros platforms
 */
int piros_platform_exit(){
	// Add PIROS exit functions here //
	return swe_unimplemented;
}

/*
 * Sleep platform until a timer expires or a interface interruption is received
 */
int piros_platform_standby(){

	// Define PIROS sleep //

	return swe_unimplemented;
}

/*
 * Get the position, unimplemented by default
 */
int piros_platform_get_coordinates(float* latitude, float* longitude) {

	// Fill the latitude and longitude fields //
	// return 0 (swe_ok) on success

	// if error or not available return errorcode
	return swe_unimplemented;
}

/*
 * Return the altitude respect the sea level (in underwater platforms depth = -altitude)
 */
int piros_platform_get_altitude(float* altitude) {

	// Fill the latitude field  //
	// return 0 (swe_ok) on success

	// if error or not available return errorcode

	return swe_unimplemented;
}

#endif

