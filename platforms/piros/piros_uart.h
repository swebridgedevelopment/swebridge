/*
 ============================================================================
						PIROS Platform UART
 ============================================================================
 Implementation of a PIROS Platform UART
 ============================================================================
 Author: Enoc Martínez
 Contact: enoc.martinez@upc.edu
 ============================================================================
 */
#ifndef PIROS_UART_H_
#define PIROS_UART_H_


#include "swe_conf.h"

#ifdef USE_PIROS_UART
#include "resources/resources.h"
#include <termios.h>


/*
 * Structure that holds the information for PIROS UART devices
 */
typedef struct{
	int fd;
	char* serial_device;
	struct termios current_settings;
	struct termios original_settings;
} Example_UART;




Interface_Descriptor* piros_open_uart(Interface_Options* conf);
int piros_write_uart(Interface_Descriptor* iface, void* buffer, uint size);
int piros_read_uart(Interface_Descriptor* iface, uchar* buffer, uint max_bytes, ulong timeout_us);
int piros_close_uart(Interface_Descriptor* iface);
int piros_fflush_uart(Interface_Descriptor* iface);
int piros_set_baudrate(Interface_Descriptor* iface, ulong baudrate);
int piros_set_uart_interrupt(Interface_Descriptor* iface, uchar* flag);

#endif /* USE_PIROS_UART */

#endif //PIROS_UART_H_
