/*
 ============================================================================
						PIROS Platform Timer
 ============================================================================
 Implementation of a PIROS Platform Timer
 ============================================================================
 Author: Enoc Martínez
 Contact: enoc.martinez@upc.edu
 ============================================================================
 */

#ifndef PIROS_TIMER_H_
#define PIROS_TIMER_H_

#include "swe_conf.h"

#if defined USE_PIROS_TIMER



int piros_delayms(ulong msecs);
int piros_get_time(Time_Structure* mytime);
ulong piros_get_epoch_time();
int piros_set_timer(ulong tmout_interval_ms, void(*handler)());
int piros_start_timer();
int piros_stop_timer();



#endif
#endif
