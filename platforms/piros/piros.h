/*
 ============================================================================
				        PIROS Platform
 ============================================================================

  SWE Bridge configuration for for piros platform. Within
  this file the following parameters need to be defined:

  	- Include header files for platform resources (uart, tcp, udp
     and filesystem)

  	- Declare the platform handlers (init, exit, etc.)

  	- Assign the handlers

 ============================================================================
 Author: Enoc Martínez
 Contact: enoc.martinez@upc.edu
 ============================================================================
 */

#ifndef RESOURCES_PLATFORMS_PIROS_INC_PIROS_H_
#define RESOURCES_PLATFORMS_PIROS_INC_PIROS_H_

#include "swe_conf.h"
#ifdef PIROS

//---- Include generic piros resources ----//
#include "platforms/piros/inc/piros_tcp_udp.h"
#include "platforms/piros/inc/piros_uart.h"
#include "platforms/piros/inc/piros_filesystem.h"
#include "platforms/piros/inc/piros_timer.h"
#include "platforms/piros/inc/piros_power.h"

//---------------------------------------------------------------------------//
//-------------------- DEFINE GENERIC PIROS FUCNTIONS -----------------------//
//---------------------------------------------------------------------------//
int piros_platform_init(int argc, char** argv);
int piros_platform_exit();
int piros_platform_standby();
int piros_platform_get_coordinates(float* latitude, float* longitude);
int piros_platform_get_altitude(float* altitude);


//---------------------------------------------------------------------------//
//-------------------- DEFINE GENERIC PIROS HANDLERS ------------------------//
//---------------------------------------------------------------------------//

/* UART Functions */
#define platform_open_uart piros_open_uart
#define platform_read_uart piros_read_uart
#define platform_write_uart piros_write_uart
#define platform_close_uart piros_close_uart
#define	platform_fflush_uart piros_fflush_uart
#define	platform_set_baudrate piros_set_baudrate
#define platform_set_uart_interrupt piros_set_uart_interrupt

/* TCP Functions */
#define platform_open_tcp piros_open_tcp
#define platform_read_tcp piros_read_tcp
#define platform_write_tcp piros_write_tcp
#define platform_close_tcp piros_close_tcp
#define	platform_fflush_tcp piros_fflush_tcp
#define platform_set_tcp_interrupt piros_set_tcp_interrupt

/* UDP Functions */
#define platform_open_udp piros_open_udp
#define platform_read_udp piros_read_udp
#define platform_write_udp piros_write_udp
#define platform_close_udp piros_close_udp
#define	platform_fflush_udp piros_fflush_udp
#define platform_set_udp_interrupt piros_set_udp_interrupt


/* FILESYSTEM Functions */
#define platform_fopen piros_fopen
#define platform_fclose piros_fclose
#define platform_fwrite piros_fwrite
#define platform_fread piros_fread
#define platform_create_dir piros_create_dir
#define platform_fremove piros_fremove
#define platform_file_size piros_file_size
#define platform_fmove piros_fmove
#define platform_getc piros_getc
#define platform_eof piros_eof

/* TIMING Functions */
#define platform_delayms piros_delayms
#define platform_get_time piros_get_time
#define platform_get_epoch_time piros_get_epoch_time
#define platform_set_timer piros_set_timer
#define platform_start_timer piros_start_timer
#define platform_stop_timer piros_stop_timer
#define platform_set_timeout piros_set_timeout
#define platform_disable_timeout piros_disable_timeout
#define platform_mktime mktime
#define platform_localtime_r localtime_r

/* Platform Functions */
#define platform_init piros_platform_init
#define platform_exit piros_platform_exit
#define platform_standby piros_platform_standby
#define platform_get_coordinates piros_platform_get_coordinates
#define platform_get_altitude piros_platform_get_altitude
#define platform_system_monitor NULL

/* Power Management Functions */
#define platform_power_on_sensor piros_power_on_sensor
#define platform_power_off_sensor piros_power_off_sensor

#endif /* PIROS */

#endif /* RESOURCES_PLATFORMS_PIROS_INC_PIROS_H_ */
