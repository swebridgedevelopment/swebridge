/*
 ============================================================================
				       PIROS Platform  File system
 ============================================================================
 Implementation of File System handlers for piros platform
 ============================================================================
 Author: Enoc Martínez
 Contact: enoc.martinez@upc.edu
 ============================================================================
 */



#include "swe_conf.h"

#ifdef USE_PIROS_FILESYSTEM // Compile only if we are using PIROS_FILESYSTEM

//---------------------------------------------------------------------------//
//--------------------- FILE SYSTEM PLATFORM HANDLERS -----------------------//
//---------------------------------------------------------------------------//
/*
 * In this file the filesystem handlers for the piros are implemented
 */


/*
 * RETURN CODES: Functions should return 0 (swe_unimplemented) in success except when
 * otherwise is specified (i.e. the number of bytes written,  * a pointer to a structure,
 *  etc.) In case of failure an errorcode should be set (a negative value, defined in
 *  swe_conf.h). In case of a function that returns a pointer, in case of failure a NULL
 *  pointer should be returned. By default, functions in this file return unimplemented
 *  errorcode (swe_unimplemented) or NULL.
 */


/*
 * Open function should open a file based on a char* filename with the options of read, write or append (defined in flags).
 * A pointer to the opened file should be returned
 */
swe_file* piros_fopen(const char* filename, swe_file_flags flags){

	if (flags == swe_read_file) {
		// Open file in read mode
	}

	else if (flags == swe_write_file) {
		// Open file in write mode (from the beginning)
	}

	else if (flags == swe_append_file) {
		// Open a file in write mode to append new data at the end
	}

	return NULL;
}

/*
 * Close previously opened file
 */
int piros_fclose(swe_file* file){
	// close file
	return swe_unimplemented;
}

/*
 * Write n 'bytes' from 'data' to 'file'. Return the number of bytes written
 */
int piros_fwrite(const void* data, int bytes, swe_file* file){
	// write to file
	return swe_unimplemented;
}


/*
 * Read n bytes from file and store them in read_data
 */
int piros_fread(void* read_data,int bytes, swe_file* file){
	int read_bytes=0;
	// read bytes
	return read_bytes;

}

/*
 * Create directory 'dirname'.
 * WARNING: This function should not return error fif the directory already exists
 */
int piros_create_dir(const char *dirname){
	return swe_unimplemented;
}


/*
 * Remove an existing file
 */
int piros_fremove(const char *filename){
	return swe_unimplemented;
}


/*
 * Return the size of a file that has already been opened.
 * NOTE: If the size is obtained by moving its read pointer (i.e. lseek), before returning
 * the pointer must be returned to its original value
 */
int piros_file_size(swe_file* file){
	int size = swe_unimplemented;

	return size; // return size
}

/*
 * Remove an existing file
 */
int piros_fmove(const char* oldname, const char* newname){
	if (rename (oldname, newname) < 0 ){
		return swe_filesystem_error;
	}
	return swe_unimplemented;
}

/*
 * Get one char from a file. The return value should be the char casted to an integer value (success)
 * or an errorcode in failure
 */
int piros_getc(swe_file* file){
	// This is a non-efficient implementation of get char based on piros_fread
	int errorcode;
	char c;
	if ( (errorcode = piros_fread(&c, 1, file)) != 1 ){
		return errorcode;
	}

	return (int)c;
}


/*
 * checks if the end of file has been reached.
 * NOTE: If the read pointer is modified (i.e. reading one char and comparing it to EOF), before returning the pointer
 * should be set to its original value!!ç
 */
int piros_eof(swe_file* filename){
	return feof(filename);
}

#endif
