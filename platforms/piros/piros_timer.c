/*
 ============================================================================
						PIROS Platform Timer
 ============================================================================
 Implementation of a PIROS Platform Timer
 ============================================================================
 Author: Enoc Martínez
 Contact: enoc.martinez@upc.edu
 ============================================================================
 */


#include "swe_conf.h"

#if defined USE_PIROS_TIMER

/*
 * Sleeps for n useconds
 */
int piros_delayms(ulong msecs) {
	return swe_unimplemented;
}


/*
 * Returns the epoch time
 */
ulong piros_get_epoch_time(){
	return swe_unimplemented;
}

/*
 * Fills the Time_Structure
 */
int piros_get_time(Time_Structure* mytime) {
	return swe_unimplemented;

}

/*
 * Configures a periodic real-time signal
 */
int piros_set_timer(ulong period_us, void(*handler)()){
	return swe_unimplemented;
}

/*
 * Starts a previously configured timer. Unblocks the timer signal from the program mask
 */
int piros_start_timer(){
	return swe_unimplemented;
}

/*
 * Blocks the timer signal from the program mask
 */
int piros_stop_timer(){
	return swe_unimplemented;

}



#endif //PIROS
