/*
 ============================================================================
				   PIROS Platform TCP UDP
 ============================================================================
 Implementation of piros platform TCP / UDP handlers
 ============================================================================
 Author: Enoc Martínez
 Contact: enoc.martinez@upc.edu
 ============================================================================
 */
#ifndef PIROS_INC_PIROS_TCP_UDP_H_
#define PIROS_INC_PIROS_TCP_UDP_H_


#include "swe_conf.h"

#if defined USE_PIROS_TCP_UDP

//-----------------------------------------------------------------//
//------------------------ TCP Interface --------------------------//
//-----------------------------------------------------------------//
typedef struct{
	/*
	 * Add here the required fields for TCP
	 */
} Example_TCP;

Interface_Descriptor* piros_open_tcp(Interface_Options* opts);
int piros_write_tcp(Interface_Descriptor* iface, void* buffer, uint size);
int piros_read_tcp(Interface_Descriptor* iface, uchar* buffer, uint max_bytes,  ulong timeout_us);
int piros_close_tcp(Interface_Descriptor* iface);
int piros_fflush_tcp(Interface_Descriptor* iface);
int piros_set_tcp_interrupt(Interface_Descriptor* iface, uchar* flag);


//-----------------------------------------------------------------//
//------------------------ UDP Interface --------------------------//
//-----------------------------------------------------------------//
typedef Example_TCP Example_UDP; // use the same structure for TCP and UDP or declare a different structure for UDP

Interface_Descriptor* piros_open_udp(Interface_Options* opts);
int piros_write_udp(Interface_Descriptor* iface, void* buffer, uint size);
int piros_read_udp(Interface_Descriptor* iface, uchar* buffer, uint max_bytes,  ulong timeout_us);
int piros_close_udp (Interface_Descriptor* iface);
int piros_fflush_udp (Interface_Descriptor* iface);
int piros_set_udp_interrupt(Interface_Descriptor* iface, uchar* flag);



#endif /* Example */

#endif /* PIROS_INC_PIROS_TCP_UDP_H_ */
