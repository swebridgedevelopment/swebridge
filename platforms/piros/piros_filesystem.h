/*
 ============================================================================
				       PIROS Platform  File system
 ============================================================================
 Implementation of File System handlers for piros platform
 ============================================================================
 Author: Enoc Martínez
 Contact: enoc.martinez@upc.edu
 ============================================================================
 */



#ifndef RESOURCES_PIROS_INC_PIROS_FILESYSTEM_H_
#define RESOURCES_PIROS_INC_PIROS_FILESYSTEM_H_

#include "swe_conf.h"
#ifdef USE_PIROS_FILESYSTEM

/*
 * SWE Bridge wrapper for open file
 */
swe_file* piros_fopen(const char* filename, swe_file_flags flags);
int piros_fclose(swe_file* file);
int piros_fwrite(const void* data, int bytes, swe_file* file);
int piros_fread(void* readData,int bytes, swe_file* file);
int piros_create_dir(const char *dirname);
int piros_fremove(const char *filename);
int piros_file_size(swe_file* filename);
int piros_fmove(const char* oldname, const char* newname);
int piros_getc(swe_file* filename);
int piros_eof(swe_file* filename);

#endif /* USE_PIROS_FILESYSTEM */

#endif /* RESOURCES_PIROS_INC_PIROS_FILESYSTEM_H_ */
