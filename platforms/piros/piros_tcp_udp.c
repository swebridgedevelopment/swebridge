/*
 ============================================================================
				   PIROS Platform TCP UDP
 ============================================================================
 Implementation of piros platform TCP / UDP handlers
 ============================================================================
 Author: Enoc Martínez
 Contact: enoc.martinez@upc.edu
 ============================================================================
 */
#include "swe_conf.h"

#if defined USE_PIROS_TCP_UDP


/*
 * RETURN CODES: Functions should return 0 (swe_unimplemented) in success except when
 * otherwise is specified (i.e. the number of bytes written,  * a pointer to a structure,
 *  etc.) In case of failure an errorcode should be set (a negative value, defined in
 *  swe_conf.h). In case of a function that returns a pointer, in case of failure a NULL
 *  pointer should be returned. By default, functions in this file return unimplemented
 *  errorcode (swe_unimplemented) or NULL.
 */



//-----------------------------------------------------------------//
//------------------------ TCP Interface --------------------------//
//-----------------------------------------------------------------//

/*
 * Open TCP socket
 */
Interface_Descriptor* piros_open_tcp(Interface_Options* opts){
	return NULL;
}

int piros_write_tcp(Interface_Descriptor* iface, void* buffer, uint size){
	return swe_unimplemented;
}

int piros_read_tcp(Interface_Descriptor* iface, uchar* buffer, uint max_bytes, ulong timeout_us){
	return swe_unimplemented;
}

int piros_close_tcp(Interface_Descriptor* iface){
	return swe_unimplemented;
}

int piros_fflush_tcp(Interface_Descriptor* iface){
	return swe_unimplemented;
}

int piros_set_tcp_interrupt(Interface_Descriptor* iface,uchar* flag){
	return swe_unimplemented;
}

//-----------------------------------------------------------------//
//------------------------ UDP Interface --------------------------//
//-----------------------------------------------------------------//


Interface_Descriptor* piros_open_udp(Interface_Options* opts){
	return NULL;

}

int piros_close_udp (Interface_Descriptor* iface){
return swe_unimplemented;
}


int piros_write_udp(Interface_Descriptor* iface, void* buffer, uint size){
	return swe_unimplemented;

}

int piros_read_udp(Interface_Descriptor* iface, uchar* buffer, uint max_bytes,  ulong timeout_us){
    return swe_unimplemented;
}

int piros_set_udp_interrupt(Interface_Descriptor* iface, uchar* flag){
	return swe_unimplemented;
}

int piros_fflush_udp(Interface_Descriptor* iface){
	return swe_unimplemented;
}

#endif
