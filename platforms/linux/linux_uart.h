#ifndef LINUX_UART_H_
#define LINUX_UART_H_

#include <termios.h>
#include "resources/resources.h"
#include "swe_conf.h"


/*
 * Structure that holds the information for LINUX UART devices
 */
typedef struct{
	int fd;
	char* serial_device;
	struct termios current_settings;
	struct termios original_settings;
} Linux_UART;




Interface_Descriptor* linux_open_uart(Interface_Options* conf);
int linux_write_uart(Interface_Descriptor* iface, void* buffer, uint size);
int linux_read_uart(Interface_Descriptor* iface, uchar* buffer, uint max_bytes, ulong timeout_us);
int linux_close_uart(Interface_Descriptor* iface);
int linux_fflush_uart(Interface_Descriptor* iface);
int linux_set_baudrate(Interface_Descriptor* iface, ulong baudrate);
int linux_set_uart_interrupt(Interface_Descriptor* iface, uchar* flag);


#endif //LINUX_UART_H_
