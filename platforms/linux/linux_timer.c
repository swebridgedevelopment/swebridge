/*
 ============================================================================
									linuxTimer.c
 ============================================================================
 Implementation of a Linux Timer using real-time signals.
 ============================================================================
 by SARTI
 Author: Enoc Martínez
 ============================================================================
 */


#include "swe_conf.h"

#ifdef USE_GENERIC_LINUX_TIMER

#include "common/swe_utils.h"
#include "platforms/linux/linux_timer.h"
#include "resources/mem_control.h"

#include <unistd.h>
#include <poll.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>
#include <time.h>
#include <math.h>
#include <sys/time.h>

int config_linux_timer(ulong initial_usec_value, ulong interval_usec_value, uint signal_num, void (*handler)());



/*
 * Sleeps for n useconds
 */
int linux_delayms(ulong msecs) {
	usleep(msecs*1000);
	return swe_ok;
}


/*
 * Returns the epoch time
 */
float64 linux_get_epoch_time(){
	struct timespec ts;
	double epoch;
	// Get time from system-wide clock
	if (clock_gettime(CLOCK_REALTIME, &ts) < 0) {
		return -1.0;
	}
	epoch = (double)ts.tv_sec;
	epoch += ((double)ts.tv_nsec) / 1e9;
	return (float64)epoch;
}


/*
 * Configures a periodic real-time signal
 */
int linux_set_timer(ulong period_us, void(*handler)()){
	return config_linux_timer(period_us, period_us, TIMER_SIGNAL_NUM, handler);
}

/*
 * Starts a previously configured timer. Unblocks the timer signal from the program mask
 */
int linux_start_timer(){
	sigset_t mask;
	sigemptyset(&mask);
	sigaddset(&mask, TIMER_SIGNAL_NUM);
	if(sigprocmask(SIG_UNBLOCK, &mask, NULL)!=0) {
		return swe_timer_error;
	}
	return swe_ok;
}

static timer_t timerid; // Scheduler Timer

/*
 * Blocks the timer signal from the program mask
 */
int linux_stop_timer(){
	if (timer_delete(timerid) < 0) {
		errmsg("Couldn't delete timer");
		return swe_error;
	}
	return swe_ok;
}

/*
 * Timeout handler, sets the flag to true when executed
 */
uchar *global_flag;
uchar timeout_enable=FALSE;
void timeout_handler(){
	*global_flag=TRUE;
}


/****************************************************************************
 *								Config Linux Interval Timer
 ****************************************************************************
 * This function configures a real time linux signal that periodically calls
 * the handler passed as argument. The initial_usec_value is the period until
 * the signal is sent for the first time (e.g. if initial_usec_value 10000000
 * the first signal will be send after 1 second). The interval_usec_value
 * periodicity of the signal after the first one is sent. If interval_usec_value
 * is 0 the signal is only sent once and then disarmed.
 *
 * This function sets up the linux real-time signal, but keeps the signal blocked
 * in the program mask. The signal should be unblocked manually.
 ****************************************************************************/

int config_linux_timer(ulong initial_usec_value, ulong interval_usec_value, uint signal_num, void (*handler)()){

	struct sigevent sev;
	struct itimerspec its;
	sigset_t mask;
	struct sigaction sa;

	sigprocmask(SIG_SETMASK, NULL, &mask); //get the current mask
	sigaddset(&mask,signal_num); //add the new signal
	//Disable signals
	sigprocmask(SIG_BLOCK, NULL, &mask); //block the new signal

	sa.sa_sigaction = handler; //define the handler function
	sa.sa_flags = 0;  //no flags needed
	sigemptyset(&sa.sa_mask); //Empty the signal mask (this mask blocks other signals during the execution of the signal handler)
	if (sigaction(signal_num, &sa, NULL) == -1) { //set the sigaction to the signal
		return -1;
	}

	sigaddset(&mask, signal_num); //add the signal to the mask
	if (sigprocmask(SIG_SETMASK, &mask, NULL) == -1){
		return -1;
	}

	//configure the event
	sev.sigev_notify = SIGEV_SIGNAL; //Define the event as signal
	sev.sigev_signo = signal_num; //define the signal number
	sev.sigev_value.sival_ptr = &timerid; //set the pointer to the timer struct
	if (timer_create(TIMER_CLOCK, &sev, &timerid) == -1)  { //create the timer
		return -1;
	}

	its.it_value.tv_sec = initial_usec_value/1000000; //Set the first sec value
	its.it_value.tv_nsec = (initial_usec_value%1000000)*1000; //set the first nsec value
	//define the the following interval
	its.it_interval.tv_sec = interval_usec_value/1000000;
	its.it_interval.tv_nsec = (interval_usec_value%1000000)*1000;

	if (timer_settime(timerid, 0, &its, NULL) == -1){ 	//Unlock the timer signal, so that timer notification
		return -1;										//can be delivered
	}
	return 0;
}


/*
 * The pollfd contains a structure with the following elements:
 *     int fd      -> file descriptor to monitor
 *     int events  -> events that we want to monitor (i.e. if we set POLLIN  detect available
 *                    we are telling to poll that we want to know if there's data to be read)
 *     int revents -> events returned by the poll functions (if POLLIN is set there's data
 *                    data to be read)
 */
static struct pollfd *poll_list; // List of all the poll elements (one element per file descriptor)
static int interrupt_count = 0; // Number of interrupts configured

struct InterruptData{
	int fd; // file descriptor associated with an interrupt
	uchar* flag; // flag associated with an interrupt
};

static struct InterruptData *interrupt_data = NULL;
static int count=0;
/*
 * Interrupt handler based on poll
 */
static void linux_interrupt_handler(){
	int i;
	count++;
	// Check if there has been activity in any fd on the poll list. Timeout 0 means that
	// the function will return immediately
	poll(poll_list, interrupt_count, 0);
	for (i=0; i < interrupt_count; i++) {
		if (poll_list[i].revents  > 0) { // Check if there's data to be read
			*interrupt_data[i].flag = TRUE; // Set the adequate flag to true
		}
	}
}


/*
 * Sets an interrupt when an input / output event occurs at a certain file descriptor
 */
int set_linux_interrupt(int fd, uchar* flag){
	sigset_t mask;
	struct sigaction sa;
	int signal = SIGPOLL;

	cimsg(BLU, "Setting linux interrupt on fd %d (flag pointer %p)", fd, flag);

	// Creating a new InterruptProperties structure
	if (interrupt_count == 0) {

		interrupt_data = swe_malloc(sizeof(struct InterruptData));
		poll_list = swe_malloc(sizeof(struct pollfd));

	} else {
		interrupt_data = swe_realloc(interrupt_data, sizeof(struct InterruptData)*(interrupt_count+1));
		poll_list = swe_realloc(poll_list, sizeof(struct pollfd)*(interrupt_count+1));
	}

	// Fill the new interrupt data //
	interrupt_data[interrupt_count].fd = fd; // assign the filedescriptor
	interrupt_data[interrupt_count].flag = flag; // assign the flag

	poll_list[interrupt_count].fd = fd;
	poll_list[interrupt_count].events = POLLIN; // Detect incoming data


	sigprocmask(SIG_SETMASK, NULL, &mask); //get the current mask
	sigaddset(&mask,signal); //add the new signal
	sa.sa_sigaction = linux_interrupt_handler; //define the handler function
	sa.sa_flags = 0;  //no flags needed
	sigemptyset(&sa.sa_mask); //Empty the signal mask (this mask blocks other signals
	                          //during the execution of the signal handler)

 	if (sigaction(signal, &sa, NULL) == -1) { //set the sigaction to the signal
    	return -1;
    }
 	sigaddset(&mask, signal); //add the signal to the mask
	if (sigprocmask(SIG_SETMASK, &mask, NULL) == -1){
		return -1;
	}
    if((fcntl(fd, F_SETFL, FNDELAY))<0){
    	errmsg("fcntl failed, errno %d", errno);
    	return swe_iface_error;
    }
    fcntl(fd, F_SETOWN, getpid()); //send signal to PID when event happens on fd

#if defined __CYGWIN__
    //windows (Cygwin) works without the O_ASYNC flag
#else
	fcntl(fd, F_SETFL,  O_ASYNC );
#endif

    sigprocmask(SIG_UNBLOCK, &mask, NULL); // Set the mask
	interrupt_count++;
	dmsg("Interrupt n %d configured properly", interrupt_count);
    return 0;
}


/*
 * Priority interrupt is a kind of interrupt that instead of setting a flag
 * to TRUE automatically executes it's handler. It's designed for fast I/O
 * operations. Only one priority interrupt can be set
 */
int set_linux_priority_interrupt(int fd, void (*handler)(void)){
	sigset_t mask;
	struct sigaction sa;
	int signal = SIGIO;

	sigprocmask(SIG_SETMASK, NULL, &mask); //get the current mask
	sigaddset(&mask,signal); //add the new signal
	sa.sa_handler = (void(*)(int)) handler; // assign the handler (and cast it to void func(int) )
	sa.sa_flags = 0;  //no flags needed
	sigemptyset(&sa.sa_mask); //Empty the signal mask (this mask blocks other signals
	                          //during the execution of the signal handler)

 	if (sigaction(signal, &sa, NULL) == -1) { //set the sigaction to the signal
    	return -1;
    }
 	sigaddset(&mask, signal); //add the signal to the mask
	if (sigprocmask(SIG_SETMASK, &mask, NULL) == -1){
		return -1;
	}
    if((fcntl(fd, F_SETFL, FNDELAY))<0){
    	errmsg("fcntl failed, errno %d", errno);
    	return swe_iface_error;
    }
    fcntl(fd, F_SETOWN, getpid()); //send signal to PID when event happens on fd

#if defined WINDOWS
    //windows (Cygwin) works without the O_ASYNC flag
#else
	fcntl(fd, F_SETFL,  O_ASYNC );
#endif

    sigprocmask(SIG_UNBLOCK, &mask, NULL); // Set the mask
    return 0;
}


/*
 * This function blocks the timer and IO signal and sleeps. When it wakes up unblocks the signal
 */
int linux_deep_sleep(float seconds){
	sigset_t mask;
	if (sigprocmask(SIG_SETMASK, NULL, &mask) < 0 ){ // get the current mask
		return -1;
	}
	sigaddset(&mask,TIMER_SIGNAL_NUM); //add timer signal
	sigaddset(&mask, SIGIO); //add the interface interrupt signals
	if (sigprocmask(SIG_SETMASK, &mask, NULL) == -1){ // Set the mask
		return -1;
	}
	sleep(seconds); // sleep
	sigdelset(&mask,TIMER_SIGNAL_NUM); //delete the timer signal
	sigdelset(&mask, SIGIO); //delete the interface interrupt isngl
	if (sigprocmask(SIG_SETMASK, &mask, NULL) == -1){ //
		return -1;
	}
	return swe_ok;
}


/*
 * returns the timezone as seconds east of UTC
 */
long int linux_get_timezone(){
	// Timezone not yet set, using localtime_r to get timezone
	struct timeval stdtime;
	struct tm *isoctime;
	stdtime.tv_sec = (uint)linux_get_epoch_time();
	stdtime.tv_usec = 0;
	isoctime = localtime((const time_t*)&stdtime.tv_sec);
	return isoctime->tm_gmtoff;
}


/*
 * Just calls localtime
 */
struct tm* linux_localtime(const time_t* time){
	return localtime(time);
}


/*
 * Wrapper for mktime to convert the return value to float64
 */
float64 linux_mktime(struct tm* isoctime){
	time_t t = mktime(isoctime);
	return (float64)t;
}


#endif //LINUX
