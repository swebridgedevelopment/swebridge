/********************************************************************
 *                        Linux Resources
 ********************************************************************
 *
 * SWE Bridge configuration for for generic linux platforms. Within
 * this file the following parameters need to be defined:
 *
 * I.  Include header files for platform resources (uart, tcp, udp
 *     and filesystem)
 *
 * II. File structure used by the platform (FILE* in linux, FIL*
 *     in FatFs, etc.) and other special platform types
 *
 * III. Handlers for the resources (i.e. platform_fopen)
 *
 * IV. init and exit functions for the platform
 *
 ********************************************************************/

#ifndef RESOURCES_PLATFORMS_LINUX_INC_LINUX_H_
#define RESOURCES_PLATFORMS_LINUX_INC_LINUX_H_

#include "swe_conf.h"
#ifdef GENERIC_LINUX

//---- Include generic linux resources ----//
#include "platforms/linux/linux_tcp_udp.h"
#include "platforms/linux/linux_uart.h"
#include "platforms/linux/linux_filesystem.h"
#include "platforms/linux/linux_timer.h"
#include "platforms/linux/linux_notifications.h"


//---------------------------------------------------------------------------//
//-------------------- DEFINE GENERIC LINUX FUCNTIONS -----------------------//
//---------------------------------------------------------------------------//
int generic_linux_init(int argc, char** argv);
int generic_linux_exit();
int generic_linux_standby();
int generic_linux_fflush();


//---------------------------------------------------------------------------//
//-------------------- DEFINE GENERIC LINUX HANDLERS ------------------------//
//---------------------------------------------------------------------------//

/* UART Functions */
#define platform_open_uart linux_open_uart
#define platform_read_uart linux_read_uart
#define platform_write_uart linux_write_uart
#define platform_close_uart linux_close_uart
#define	platform_fflush_uart linux_fflush_uart
#define	platform_set_baudrate linux_set_baudrate
#define platform_set_uart_interrupt linux_set_uart_interrupt


/* TCP Functions */
#define platform_open_tcp linux_open_tcp
#define platform_read_tcp linux_read_tcp
#define platform_write_tcp linux_write_tcp
#define platform_close_tcp linux_close_tcp
#define	platform_fflush_tcp linux_fflush_tcp
#define platform_set_tcp_interrupt linux_set_tcp_interrupt
#define platform_set_tcp_priority_interrupt linux_set_tcp_priority_interrupt

/* UDP Functions */
#define platform_open_udp linux_open_udp
#define platform_read_udp linux_read_udp
#define platform_send_to_udp linux_send_to_udp
#define platform_close_udp linux_close_udp
#define	platform_fflush_udp linux_fflush_udp
#define platform_set_udp_interrupt linux_set_udp_interrupt
#define platform_set_udp_priority_interrupt linux_set_udp_priority_interrupt


/* FILESYSTEM Functions */
#define platform_fopen linux_fopen
#define platform_fclose linux_fclose
#define platform_fwrite linux_fwrite
#define platform_fread linux_fread
#define platform_create_dir linux_create_dir
#define platform_fremove linux_fremove
#define platform_file_size linux_file_size
#define platform_fmove linux_fmove
#define platform_getc linux_getc
#define platform_eof linux_eof
#define platform_set_pointer linux_set_pointer
#define platform_list_dir linux_list_dir
#define platform_file_exists linux_file_exists

/* TIMING Functions */
#define platform_delayms linux_delayms
#define platform_get_epoch_time linux_get_epoch_time
#define platform_set_timer linux_set_timer
#define platform_start_timer linux_start_timer
#define platform_stop_timer linux_stop_timer
#define platform_set_timeout linux_set_timeout
#define platform_disable_timeout linux_disable_timeout
#define platform_mktime linux_mktime
#define platform_localtime linux_localtime
#define platform_get_timezone linux_get_timezone

/* Platform Functions */
#define platform_init generic_linux_init
#define platform_exit generic_linux_exit
#define platform_standby generic_linux_standby
#define platform_deep_sleep linux_deep_sleep
#define platform_vprintf vprintf
#define platform_vfprintf vfprintf
#define platform_fflush generic_linux_fflush
#if (( LINUX_UDP_NOTIFICATIONS == TRUE ))
#define platform_notify linux_udp_notify
#endif

// Simulate position //
#ifdef SIMULATE_COORDINATES
#include "platforms/common/simulate_coordinates.h"
#define platform_get_coordinates simulate_coordinates
#define platform_get_altitude simulate_altitude
#endif


#ifdef SIMULATE_INTERNAL_SENSORS
#include "platforms/common/simulate_internal_sensors.h"
// Internal Sensors //
#define platform_internal_sensors_initialize simulate_internal_sensors_initialize
#define platform_internal_sensors_read simulate_internal_sensors_read
#endif

// ADCs //
#ifdef SIMULATE_ADC
#include "platforms/common/simulate_adc.h"
// Simulate ADC Resources //
#define platform_adc_initialize simulate_adc_initialize
#define platform_adc_read simulate_adc_read
#endif

// Power Management //
#ifdef SIMULATE_POWER_MANAGEMENT
#include "platforms/common/simulate_power.h"
// Simulate ADC Resources //
#define platform_power_initialize simulate_power_initialize
#define platform_power_on_sensor simulate_power_on
#define platform_power_off_sensor simulate_power_off

#endif



#endif /* LINUX */
#endif /* RESOURCES_PLATFORMS_LINUX_INC_LINUX_H_ */
