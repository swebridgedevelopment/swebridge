/*
 ============================================================================
									linuxTimer.h
 ============================================================================
 Implementation of a Linux Timer.
 ============================================================================
 by SARTI
 Author: Enoc Martínez
 ============================================================================
 */

#ifndef LINUXTIMER_H_
#define LINUXTIMER_H_

#include <signal.h>
#include <time.h>
#include "swe_conf.h"

#ifdef  USE_GENERIC_LINUX_TIMER

#define TIMER_SIGNAL_NUM SIGRTMIN
#define TIMEOUT_SIGNAL_NUM SIGRTMIN+1
#define TIMER_CLOCK CLOCK_REALTIME // A settable system-wide real-time clock.


int linux_delayms(ulong msecs);
int linux_deep_sleep(float seconds);
float64 linux_get_epoch_time();

int linux_set_timer(ulong tmout_interval_ms, void(*handler)());
int linux_start_timer();
int linux_stop_timer();


int set_linux_interrupt(int fd, uchar* flag);
int set_linux_priority_interrupt(int fd, void (*handler)(void));


long int linux_get_timezone();
struct tm* linux_localtime(const time_t* time);
float64 linux_mktime(struct tm* isoctime);


#endif /*USE_GENERIC_LINUX_TIMER*/
#endif
