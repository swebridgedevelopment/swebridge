/*
 * Linux_TCP.h
 *
 *  Created on: Sep 23, 2016
 *      Author: enoc
 */

#ifndef LINUX_INC_LINUX_TCP_UDP_H_
#define LINUX_INC_LINUX_TCP_UDP_H_


#include "swe_conf.h"
#if defined USE_GENERIC_LINUX_TCP_UDP

//#include "resources/resources.h"
#include <arpa/inet.h>


//-----------------------------------------------------------------//
//------------------------ TCP Interface --------------------------//
//-----------------------------------------------------------------//
typedef struct{
	uint port;
	char* IP;
	uint max_size; //maximum packet size
	int fd; //file descriptor
	uint timeout;
	struct sockaddr_in server;
} Linux_TCP;

Interface_Descriptor* linux_open_tcp(Interface_Options* opts);
int linux_write_tcp(Interface_Descriptor* iface, void* buffer, uint size);
int linux_read_tcp(Interface_Descriptor* iface, uchar* buffer, uint max_bytes,  ulong timeout_us);
int linux_close_tcp(Interface_Descriptor* iface);
int linux_fflush_tcp(Interface_Descriptor* iface);
int linux_set_tcp_interrupt(Interface_Descriptor* iface, uchar* flag);
int linux_set_tcp_priority_interrupt(Interface_Descriptor* iface, void (*handler)(void));

//-----------------------------------------------------------------//
//------------------------ UDP Interface --------------------------//
//-----------------------------------------------------------------//

typedef Linux_TCP Linux_UDP; // use the same structure for TCP and UDP

Interface_Descriptor* linux_open_udp(Interface_Options* opts);
int linux_write_udp(Interface_Descriptor* iface, void* buffer, uint size);
int linux_read_udp(Interface_Descriptor* iface, uchar* buffer, uint max_bytes, ulong timeout_us);
int linux_close_udp (Interface_Descriptor* iface);
int linux_send_to_udp(Interface_Descriptor* iface, void* buffer, uint size, char* address);
int linux_fflush_udp (Interface_Descriptor* iface);
int linux_set_udp_interrupt(Interface_Descriptor* iface, uchar* flag);
int linux_set_udp_destination(Interface_Descriptor* iface, char* dest_address, void* destination);
int linux_set_udp_priority_interrupt(Interface_Descriptor* iface, void (*handler)(void));



#endif /* USE_GENERIC_LINUX_TCP_UDP */

#endif /* LINUX_INC_LINUX_TCP_UDP_H_ */
