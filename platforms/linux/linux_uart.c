#include "swe_conf.h"


#if defined USE_GENERIC_LINUX_UART
#include "common/swe_utils.h"

#include "platforms/linux/linux_uart.h"
#include "platforms/linux/linux_timer.h"
#include "resources/resources.h"

#include <termios.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <sys/file.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/socket.h>

/*
 * Opens a Linux UART and returns a pointer to the Linux_UART structure containing
 * its settings and file descriptor
 */
Interface_Descriptor* linux_open_uart(Interface_Options* opts){
	int status;
	int error;
	Linux_UART *UART;

	int cbits=CS8,
	      cpar=0,
	      ipar=IGNPAR,
	      bstop=0;

	// Checking input
	if(opts == NULL || opts->serial_device==NULL){
		errmsg( "NULL pointer received in linux_open_uart");
		return NULL;
	}
	imsg( "Opening Linux UART...");
	UART=swe_malloc(sizeof(Linux_UART));

	dmsg("Opening port \"%s\"", opts->serial_device);
	//open serial port and assign a file descriptor
	UART->fd = open(opts->serial_device, O_RDWR | O_NOCTTY | O_NDELAY);
	if(UART->fd==-1)   {
		errmsg("unable to open comport ");
		return NULL;
	}

	  /* lock access so that another process can't also use the port */
	if(flock(UART->fd, LOCK_EX | LOCK_NB) != 0) {
		close(UART->fd);
		errmsg( "Another process has locked the comport.");
		return NULL;
	}

	error = tcgetattr(UART->fd, &UART->original_settings);
	if(error==-1)  {
		close(UART->fd);
	    flock(UART->fd, LOCK_UN);  /* free the port so that others can use it. */
	    errmsg("unable to read portsettings ");
	    return NULL;
	 }
	memset(&UART->current_settings, 0, sizeof(UART->current_settings));  /* clear the new struct */

	UART->current_settings.c_cflag = cbits | cpar | bstop | CLOCAL | CREAD;
	UART->current_settings.c_iflag = ipar;
	UART->current_settings.c_oflag = 0;
	UART->current_settings.c_lflag = 0;
	UART->current_settings.c_cc[VMIN] = 0;      /* block until n bytes are received */
	UART->current_settings.c_cc[VTIME] = 0;     /* block until a timer expires (n * 100 mSec.) */

	//set baudrate

	 linux_set_baudrate(UART, opts->baudrate);

	if(ioctl(UART->fd, TIOCMGET, &status) == -1) {
		tcsetattr(UART->fd, TCSANOW, &UART->original_settings );
		flock(UART->fd, LOCK_UN);  /* free the port so that others can use it. */
		errmsg("unable to get portstatus");
		return NULL;
	}

	status |= TIOCM_DTR;    /* turn on DTR */
	status |= TIOCM_RTS;    /* turn on RTS */

	if(ioctl(UART->fd, TIOCMSET, &status) == -1) {
		tcsetattr(UART->fd, TCSANOW, &UART->original_settings);
		flock(UART->fd, LOCK_UN);  /* free the port so that others can use it. */
		errmsg("unable to set port status");
		return NULL;
	}
	cimsg(GRN, "Port \"%s\" opened successfully (fd %d)", opts->serial_device, UART->fd);


	// Cast Linux_UART to Platform_Interface (void*)
	return (Interface_Descriptor*)UART;
}


int linux_set_baudrate(Interface_Descriptor* iface, ulong baudrate_in){
	Linux_UART *uart =  (Linux_UART*)iface;
	ulong baudr;

	if (baudrate_in == 0){
		warnmsg("Baudrate not specific, falling back to default %U", DEFAULT_BAUDRATE);
	}

	switch(baudrate_in)
	  {
		case      50 : baudr = B50; break;
		case      75 : baudr = B75; break;
		case     110 : baudr = B110; break;
		case     134 : baudr = B134; break;
		case     150 : baudr = B150; break;
		case     200 : baudr = B200; break;
		case     300 : baudr = B300; break;
		case     600 : baudr = B600; break;
		case    1200 : baudr = B1200; break;
		case    1800 : baudr = B1800; break;
		case    2400 : baudr = B2400; break;
		case    4800 : baudr = B4800; break;
		case    9600 : baudr = B9600; break;
		case   19200 : baudr = B19200; break;
		case   38400 : baudr = B38400; break;
		case   57600 : baudr = B57600; break;
		case  115200 : baudr = B115200; break;
		case  230400 : baudr = B230400; break;
		case  460800 : baudr = B460800; break;
		case  500000 : baudr = B500000; break;
		case  576000 : baudr = B576000; break;
		case  921600 : baudr = B921600; break;
		case 1000000 : baudr = B1000000; break;
		case 1152000 : baudr = B1152000; break;
		case 1500000 : baudr = B1500000; break;
		case 2000000 : baudr = B2000000; break;
		case 2500000 : baudr = B2500000; break;
		case 3000000 : baudr = B3000000; break;
#ifndef __CYGWIN__ // Use only if CYGWIN is not defined (i.e. "pure" Linux)
		case 3500000 : baudr = B3500000; break;
		case 4000000 : baudr = B4000000; break;
#endif
		default      : errmsg( "invalid baudrate %u",baudrate_in);
		   return(swe_error);
		   break;
	  }
	  cfsetispeed(&uart->current_settings, baudr);
	  cfsetospeed(&uart->current_settings, baudr);

	  if((tcsetattr(uart->fd, TCSANOW, &uart->current_settings))==-1) {
		tcsetattr(uart->fd, TCSANOW, &uart->original_settings);
		close(uart->fd);
		flock(uart->fd, LOCK_UN);  /* free the port so that others can use it. */
		errmsg("unable to adjust port settings ");
		return(1);
	  }

	  return swe_ok;
}



int linux_write_uart(Interface_Descriptor* iface, void* buffer, uint size){
	Linux_UART *uart =  (Linux_UART*)iface;
	return write(uart->fd, buffer, size);
}

int linux_fflush_uart(Interface_Descriptor* iface){
	Linux_UART *uart =  (Linux_UART*)iface;
	int nbytes=0;
	tcflush(uart->fd, TCIOFLUSH);
	//get the number of available bytes
	ioctl(uart->fd, FIONREAD, &nbytes);
	if(nbytes>0) {
		char buffer[nbytes+1];
		return read(uart->fd, buffer, nbytes);
	}
	return swe_ok;
}


int char_ready(Interface_Descriptor* iface, uint tmout){
	Linux_UART *uart =  (Linux_UART*)iface;
	fd_set fds;
	struct timeval tv;
	tv.tv_sec=0;
	tv.tv_usec=tmout;
	FD_ZERO(&fds);
	FD_SET(uart->fd, &fds);
	if(select(uart->fd+1, &fds, NULL, NULL, &tv)<1) return swe_error;
	int ret=FD_ISSET(uart->fd, &fds);
	return ret;
}

int linux_read_uart(Interface_Descriptor* iface, uchar* buffer, uint max_bytes, ulong timeout_us){
	Linux_UART *uart =  (Linux_UART*)iface;
	if (uart->fd <= 0) {
		return(swe_iface_error);
	}
	int nbytes=0;


	uchar end_tx_token=FALSE;
	uchar tmout_flag=FALSE;
	uint char_tmout=timeout_us;

	struct timeval t;
	gettimeofday(&t, NULL);
	//double start_time= (double)t.tv_sec + ((double)t.tv_usec)/1000000;
	ulong start=t.tv_sec*1000000+t.tv_usec;
	ulong now;

	tmout_flag=FALSE;
	while((!tmout_flag) && (!end_tx_token) && (nbytes<max_bytes)) {
		gettimeofday(&t, NULL);
		now=t.tv_sec*1000000+t.tv_usec;
		if(now > (start + timeout_us)){
			tmout_flag=TRUE;
		}
		if(char_ready(uart, char_tmout)>0){
			int tmp_bytes=read(uart->fd, buffer+nbytes , (max_bytes-nbytes));
			char_tmout=10*timeout_us/max_bytes;
			if(tmp_bytes<0){
				errmsg( "Uart error, %d", tmp_bytes);
				return nbytes;
			}
			nbytes += tmp_bytes;
		}
	}

	return nbytes;
}

int linux_close_uart(Interface_Descriptor* iface){
	Linux_UART *uart =  (Linux_UART*)iface;
	return close(uart->fd);
}

int linux_set_uart_interrupt(void *iface, uchar* flag){
	Linux_UART *uart =  (Linux_UART*)iface;
	dmsg( "setting iface UART interrupt");
	return set_linux_interrupt(uart->fd, flag);

}
#endif
