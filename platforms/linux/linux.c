/*
 * linux.c
 *
 *  Created on: Apr 17, 2018
 *      Author: enoc
 */
#include "swe_conf.h"

#if defined GENERIC_LINUX
#include "common/swe_utils.h"
#include "resources/resources.h"
#include <unistd.h>


int generic_linux_set_signal_handler(int signal_num, void(*handler)(int signal));
void signal_exit_handler(int signal);


/*
 * Initialization function for generic linux platforms
 */
int generic_linux_init(int argc, char** argv){
	// Setup signal to handle SIGTERM

	imsg("Initializing generic Linux Platform");

	imsg("setting up SIGQUIT signal");
	generic_linux_set_signal_handler(SIGQUIT, signal_exit_handler);
	imsg("setting up SIGTERM signal");
	generic_linux_set_signal_handler(SIGTERM, signal_exit_handler);
	imsg("setting up SIGINT signal");
	generic_linux_set_signal_handler(SIGINT, signal_exit_handler);
	return swe_ok;
}

/*
 * Exit function for generic linux platforms
 */
int generic_linux_exit(){
	imsg("Closing all files");
	close_all_files();
	imsg("Closing all interfaces");
	close_all_interfaces();
	exit(0);
	return swe_error;
}

/*
 * Sleep platform until a timer expires or a interface interruption is received
 */
int generic_linux_standby(){
	sleep(3600); //sleep for 1 hour
	return swe_ok;
}


/*
 * wrapper for linux exit
 */
void signal_exit_handler(int signal) {
	generic_linux_exit();
}

/*
 * Configures a signal
 */
int generic_linux_set_signal_handler(int signal_num, void(*handler)(int signal)) {
	sigset_t mask;
	struct sigaction sa;


	sigprocmask(SIG_SETMASK, NULL, &mask); //get the current mask
	sigaddset(&mask, signal_num); //add the new signal
	//Disable signals
	sigprocmask(SIG_BLOCK, NULL, &mask); //block the new signal

	sa.sa_handler = handler; //define the handler function
	sa.sa_flags = 0;  //no flags needed
	sigemptyset(&sa.sa_mask); //Empty the signal mask (this mask blocks other signals during the execution of the signal handler)

	if (sigaction(signal_num, &sa, NULL) == -1) { //set the sigaction to the signal
		return -1;
	}

	sigaddset(&mask, signal_num); //add the signal to the mask
	if (sigprocmask(SIG_SETMASK, &mask, NULL) == -1){
		return -1;
	}

	sigprocmask(SIG_UNBLOCK, &mask, NULL); // Set the mask
	return swe_ok;
}


/*
 * fflush stdout
 */
int generic_linux_fflush() {
	fflush(stdout);
	return swe_ok;
}


#endif /* GENERIC_LINUX_PLATFORM */

