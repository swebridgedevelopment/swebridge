/*
 * linux_filesystem.h
 *
 *  Created on: Apr 10, 2018
 *      Author: enoc
 */

#ifndef RESOURCES_LINUX_INC_LINUX_FILESYSTEM_H_
#define RESOURCES_LINUX_INC_LINUX_FILESYSTEM_H_

#include "swe_conf.h"

#ifdef USE_GENERIC_LINUX_FILESYSTEM
/*
 * SWE Bridge wrapper for open file
 */
swe_file* linux_fopen(const char* filename, swe_file_flags flags);

int linux_fclose(swe_file* file);
int linux_fwrite(const void* data, int bytes, swe_file* file);
int linux_fread(void* readData,int bytes, swe_file* file);
int linux_create_dir(const char *dirname);
int linux_fremove(const char *filename);
int linux_file_size(swe_file* filename);
int linux_fmove(const char* oldname, const char* newname);

int linux_getc(swe_file* filename);
int linux_eof(swe_file* filename);
int linux_set_pointer(swe_file* file, int pointer);
char** linux_list_dir(const char* dirpath, int* n);
uchar linux_file_exists(const char* filename);



#endif /* USE_GENERIC_LINUX_FILESYSTEM */
#endif /* RESOURCES_LINUX_INC_LINUX_FILESYSTEM_H_ */
