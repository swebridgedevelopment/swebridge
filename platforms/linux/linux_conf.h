/*
 ============================================================================
				   Generic Linux Configuration File
 ============================================================================
 This file configures the project for a generic Linux platform. The following


  I.  Define basic types (uchar, uint and ulon)

  II.  Define SWE file type

  III. Activate the linux generic handlers

 ============================================================================
 Author: Enoc Martínez
 Contact: enoc.martinez@upc.edu
 ============================================================================
 */

#ifndef RESOURCES_PLATFORMS_LINUX_INC_LINUX_CONF_H_
#define RESOURCES_PLATFORMS_LINUX_INC_LINUX_CONF_H_

#ifdef GENERIC_LINUX

// Include generic C Linux libraries //
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>


// Declare uchar, uint and ulong //
typedef uint8_t uchar;
typedef uint32_t uint;
//typedef uint64_t ulong;
typedef double float64;


// Declare file type //
typedef FILE swe_file;



// Activate generic linux resources //
#define USE_GENERIC_LINUX_FILESYSTEM
#define USE_GENERIC_LINUX_UART
#define USE_GENERIC_LINUX_TCP_UDP
#define USE_GENERIC_LINUX_TIMER


#define SIMULATE_POWER_MANAGEMENT
#define SIMULATE_INTERNAL_SENSORS
#define SIMULATE_ADC
#define SIMULATE_COORDINATES


// Other Linux Configuration //
#define STARTING_ARGUMENT 1


#define PLATFORM_NAME "Generic Linux"

#define LINUX



// If set to true, udp notifications about the SWE Bridge status
// will be sent.
#define LINUX_UDP_NOTIFICATIONS FALSE
#if (( LINUX_UDP_NOTIFICATIONS == TRUE ))
#define LINUX_UDP_NOTIFICATIONS_HOST "127.0.0.1"
#define LINUX_UDP_NOTIFICATIONS_PORT 37541
#endif


#endif /* LINUX */

#endif /* RESOURCES_PLATFORMS_LINUX_INC_LINUX_CONF_H_ */
