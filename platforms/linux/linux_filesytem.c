#include "swe_conf.h"

/*
 * Static Filesystem structure
 */

#ifdef USE_GENERIC_LINUX_FILESYSTEM
#include "common/swe_utils.h"
#include "resources/mem_control.h"
#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <dirent.h>

/*
 * Linux Open Function
 */
swe_file* linux_fopen(const char* filename, swe_file_flags flags){
	swe_file * file = NULL;
	if(flags==swe_write_file){
		file= (swe_file*)fopen(filename, "wb");
	}
	else if(flags==swe_read_file){
		file=(swe_file*)fopen(filename, "rb");
	}
	else if(flags==swe_append_file){
		file=(swe_file*)fopen(filename, "a");
	} else {
		errmsg("Unknown flags at function %s", __func__);
		return NULL;
	}
	return file;
}

/*
 * Linux Close function
 */
int linux_fclose(swe_file* file){
	if(fclose((FILE*)file)!=0){
		return swe_filesystem_error;
	}
	return swe_ok;
}

/*
 * Linux fwrite
 */
int linux_fwrite(const void* data, int bytes, swe_file* file){
	int bytes_written=0;
	if(bytes < 1 ){
		return  swe_invalid_arguments;
	} else if (file == NULL) {
		errmsg("linux_fwrite NULL pointer received!!");
		return swe_invalid_arguments;
	}

	if ((bytes_written=fwrite(data, 1 ,bytes, (FILE*)file)) != bytes ) {
		warnmsg("bytes written %d of %d\n", bytes_written, bytes);
		return swe_filesystem_error;
	}

	return bytes_written;
}


int linux_fread(void* readData,int bytes, swe_file* file){
	int read_bytes=0;
	if (bytes<1){
		return swe_invalid_arguments;
	}
	read_bytes = fread(readData, 1 , bytes, (FILE*)file);
	if (read_bytes<0){
		errmsg( "fread returned %d", read_bytes);
		return swe_filesystem_error;
	}

	return read_bytes;

}

/*
 * Create directory
 */
int linux_create_dir(const char *dirname){
	// check if the directory exists
	struct stat st = {0};

	if (stat(dirname, &st) == 0) {
	    return swe_ok; // directory exists
	}


	int ret = mkdir(dirname,(
							 S_IRWXU  //read and write owner permissions
						   | S_IRWXG  //read and write user group permissions
						   | S_IRWXO
							)); //read and write other user permissions

	if(ret<0){
		return swe_filesystem_error;
	}
	return swe_ok;
}

/*
 * Remove File
 */
int linux_fremove(const char *filename){
	if(remove(filename) < 0){
		return swe_filesystem_error;
	}
	return swe_ok;
}


int linux_file_size(swe_file* file){
	uint size;
	fseek((FILE*)file, 0, SEEK_END); // seek to end of file
	size = ftell(file); // get current file pointer
	fseek(file, 0, SEEK_SET); // seek back to beginning of file

	return size; // return size
}


int linux_fmove(const char* oldname, const char* newname){
	if (rename (oldname, newname) < 0 ){
		return swe_filesystem_error;
	}
	return swe_ok;
}

int linux_getc(swe_file* filename){
	return getc(filename);
}
int linux_eof(swe_file* filename){
	return feof(filename);
}

/*
 * Set the pointer
 */
int linux_set_pointer(swe_file* file, int pointer) {
	return fseek(file, pointer, SEEK_SET);
}


/*
 * Returns the number of elements in a NULL-terminated array
 */
#define ARRAY_LENGTH(pointer) ({int __index; \
	if(pointer!=NULL) for (__index=0; pointer[__index]!=NULL; __index++);\
	else __index=0; \
	__index;})


/*
 * Lists all files in a directory
 */
char** linux_list_dir(const char* dirpath, int* n){
	DIR *d;
	struct dirent* dir;
	int count = 0;
	char** files = NULL;
	if (( d = opendir(dirpath)) == NULL ){
		errmsg("Can't open directory");
		return NULL;
	}
    while ((dir = readdir(d)) != NULL) {
    	 if (dir->d_type == DT_REG) {
    		 char* filename = set_string(dir->d_name);
    		 ATTACH_TO_ARRAY(files, filename);
    		 count++;
    	 }
    }
    closedir(d);
    if (n != NULL) {
    	*n = count;
    }
	return files;
}

/*
 * Checks if a file exsists and returns TRUE (if it does) or FALSE (if it doesn't)
 */
uchar linux_file_exists(const char* filename){
	int fd;
	if( access( filename, F_OK ) == 0 ) {
		return TRUE;
	}
	return FALSE;
}


#endif
