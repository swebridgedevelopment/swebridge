/*
 * 
 *
 *  @author: Enoc Martínez
 *  @institution: Universitat Politècnica de Catalunya (UPC)
 *  @contact: enoc.martinez@upc.edu
 */
#ifndef RESOURCES_PLATFORMS_LINUX_INC_LINUX_NOTIFICATIONS_H_
#define RESOURCES_PLATFORMS_LINUX_INC_LINUX_NOTIFICATIONS_H_

#include "swe_conf.h"
#if LINUX_UDP_NOTIFICATIONS

int linux_udp_notify(const int code, void* arg); // Notify an event to the host platform

#endif

#endif /* RESOURCES_PLATFORMS_LINUX_INC_LINUX_NOTIFICATIONS_H_ */
