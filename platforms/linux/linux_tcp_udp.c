/*
 * Linux_TCP.c
 *
 *  Created on: Sep 23, 2016
 *      Author: enoc
 */


#include "swe_conf.h"

#ifdef USE_GENERIC_LINUX_TCP_UDP
#include "common/swe_utils.h"
#include "resources/resources.h"
#include "platforms/linux/linux_tcp_udp.h"
#include "platforms/linux/linux_timer.h"

#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <sys/socket.h>

//-----------------------------------------------------------------//
//------------------------ TCP Interface --------------------------//
//-----------------------------------------------------------------//

/*
 * Open TCP socket
 */
Interface_Descriptor* linux_open_tcp(Interface_Options* opts){
	Linux_TCP* linux_tcp_iface=NULL;

	// Check arguments //
	if (opts == NULL) {
		errmsg("linux_open_tcp NULL pointer received");
		return NULL;
	} else if (opts->IP == NULL){
		errmsg("linux_open_tcp missing IP address");
	} else if (opts->port == 0 || opts->port > 65335){
		errmsg("invalid port %u", opts->port);
	}

	imsg( "Opening Linux TCP socket...");
	dmsg( "IP: %s", opts->IP);
	dmsg( "port %d", opts->port);

	linux_tcp_iface=swe_malloc(sizeof(Linux_TCP));

	int ret;
	if((linux_tcp_iface->fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		errmsg("linux_open_tcp : Could not create socket");
		return NULL;
	}

	struct sockaddr_in server;

	server.sin_family= AF_INET;
	server.sin_port =  htons(opts->port);
	server.sin_addr.s_addr = inet_addr(opts->IP);

	if ((ret=connect(linux_tcp_iface->fd, (const struct sockaddr*)&server,sizeof(server))) < 0){
	   errmsg( "Connect Failed, returned %d \n", ret);
	   return NULL;
	}

	int flags;
	// get current flags
	flags = fcntl(linux_tcp_iface->fd, F_GETFL, 0);
	// adding nonblock
	if (fcntl(linux_tcp_iface->fd, F_SETFL, flags | O_NONBLOCK) < 0) {
		errmsg("Can't set iface to NONBLOCK!");
		return NULL;
	}

	return (Interface_Descriptor*)linux_tcp_iface;
}

int linux_write_tcp(Interface_Descriptor* iface, void* buffer, uint size){
	Linux_TCP* linux_tcp_iface = (Linux_TCP*)iface;
	return write(linux_tcp_iface->fd, buffer, size);
}


/*
 * Tries to read max_bytes from a TCP socket (timeout is ignored)
 */
int linux_read_tcp(Interface_Descriptor* iface, uchar* buffer, uint max_bytes,  ulong timeout_us){
	Linux_TCP* linux_tcp_iface = (Linux_TCP*)iface;
	int bytes_in_buffer, bytes_to_read;
	int fd = linux_tcp_iface->fd;
	if (ioctl(fd, FIONREAD, &bytes_in_buffer) < 0){
		errmsg("Can't get number of bytes in buffer");
		return swe_iface_error;
	}
	bytes_to_read = bytes_in_buffer;
	if (max_bytes < bytes_in_buffer) {
		warnmsg("Can't read all bytes in buffer! (reading %d of %d)",max_bytes, bytes_in_buffer);
		bytes_to_read = max_bytes;
	}
	int ret = read(fd, buffer, bytes_to_read);
	if (ret < 0) {
		errmsg("linux read returned %d", ret);
		return swe_iface_error;
	} else if (ret == 0) {
		// Connection closed?

	}
	else if (ret < bytes_to_read) {
		warnmsg("read only %d of %d", ret, bytes_to_read);
	}
	return ret;
}

/*
 * Closes TCP Interface
 */
int linux_close_tcp(Interface_Descriptor* iface){
	Linux_TCP* linux_tcp_iface = (Linux_TCP*)iface;
	close(linux_tcp_iface->fd);
	return swe_ok;
}

int linux_fflush_tcp(Interface_Descriptor* iface){
	return swe_ok;
}


/*
 * Sets an Interruption on a tcp socket
 */
int linux_set_tcp_interrupt(Interface_Descriptor* iface,uchar* flag){
	Linux_TCP* linux_tcp_iface = (Linux_TCP*)iface;
	return set_linux_interrupt(linux_tcp_iface->fd, flag);
}


int linux_set_tcp_priority_interrupt(Interface_Descriptor* iface, void (*handler)(void)){
	Linux_TCP* linux_tcp_iface=(Linux_TCP*)iface; // Cast from generic interface to linux udp
	dmsg( "setting iface tcp interrupt at fd %d (linux_udp_iface %p)", linux_tcp_iface->fd, linux_tcp_iface);
	return set_linux_priority_interrupt(linux_tcp_iface->fd, handler);
}


//-----------------------------------------------------------------//
//------------------------ UDP Interface --------------------------//
//-----------------------------------------------------------------//


Interface_Descriptor* linux_open_udp(Interface_Options* opts){
	Linux_UDP* linux_udp_iface=NULL;

	dmsg( "Opening UDP iface with port %d", opts->port);
	if (opts->port < 1 ) {
		errmsg("trying to configure linux udp with port %d", opts->port);
		return NULL;
	}
	linux_udp_iface=swe_malloc(sizeof(Linux_UDP));

	if((linux_udp_iface->fd = socket(PF_INET, SOCK_DGRAM, 0)) < 0) {
		errmsg( "Could not create UDP socket");
		return NULL;
	}

	memset(&linux_udp_iface->server, 0, sizeof(linux_udp_iface->server));
	linux_udp_iface->server.sin_family = AF_INET;

	linux_udp_iface->server.sin_port = htons(opts->port);

	//linux_udp_iface->server.sin_addr.s_addr = inet_addr(linux_udp_iface->IP);
	linux_udp_iface->server.sin_addr.s_addr = htonl(INADDR_ANY);

	if (bind(linux_udp_iface->fd, (struct sockaddr *) &linux_udp_iface->server,sizeof(linux_udp_iface->server))==1) {
		errmsg( "Could not bind UDP socket");
		return NULL;
	}

	int flags;
	// get current flags
	flags = fcntl(linux_udp_iface->fd, F_GETFL, 0);
	// adding nonblock
	if (fcntl(linux_udp_iface->fd, F_SETFL, flags | O_NONBLOCK) < 0) {
		errmsg("Can't set iface to NONBLOCK!");
		return NULL;
	}
	return (Interface_Descriptor*)linux_udp_iface;

}

int linux_close_udp (Interface_Descriptor* iface){
	Linux_UDP* linux_udp_iface=(Linux_UDP*)iface; // Cast from generic interface to linux udp
	close(linux_udp_iface->fd);
	return swe_ok;
}



/*
 * Sends data in buffer to address. Address should be <host>:<port> such as my.destination.org:9987 or 192.168.1.179:9987
 */
int linux_send_to_udp(Interface_Descriptor* iface, void* buffer, uint size, char* address){
	Linux_UDP* linux_udp_iface=(Linux_UDP*)iface; // Cast from generic interface to linux udp
	char temp_address[512];
	uint fields;
	char ** substrings;
	struct sockaddr_in addr;

	strcpy(temp_address, address);

	substrings =  get_substrings(temp_address, ":", &fields); // split addres into host and port
	if ( fields != 2 ){
		errmsg("input address does not match pattern <host>:<dest>");
		return swe_invalid_arguments;
	}

    memset(&addr, 0, sizeof(addr));

    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(substrings[0]);
    addr.sin_port = htons(atoi(substrings[1]));

    if (addr.sin_addr.s_addr == INADDR_NONE) {
    	errmsg("Error setting IP \"%s\"", substrings[0]);
    	return swe_error;
    }

    int sent_bytes = sendto(linux_udp_iface->fd	, buffer, size, 0, (const struct sockaddr*)&addr, sizeof(struct sockaddr_in));
	return sent_bytes;
}


/*
 * Linux read UDP
 */
int linux_read_udp(Interface_Descriptor* iface, uchar* buffer, uint max_bytes, ulong timeout_us){
	Linux_UDP* linux_udp_iface = (Linux_UDP*)iface;
	int bytes_in_buffer, bytes_to_read;
	int fd = linux_udp_iface->fd;
	if (ioctl(fd, FIONREAD, &bytes_in_buffer) < 0){
		errmsg("Can't get number of bytes in buffer");
		return swe_iface_error;
	}
	bytes_to_read = bytes_in_buffer;
	if (max_bytes < bytes_in_buffer) {
		warnmsg("Can't read all bytes in buffer! (reading %d of %d)",max_bytes, bytes_in_buffer);
		bytes_to_read = max_bytes;
	}
	int ret = read(fd, buffer, bytes_to_read);
	if (ret < 0) {
		errmsg("linux read returned %d", ret);
		return swe_iface_error;
	} else if (ret < bytes_to_read) {
		warnmsg("read only %d of %d", ret, bytes_to_read);
	}
	return ret;
}


int linux_set_udp_interrupt(Interface_Descriptor* iface, uchar* flag){
	Linux_UDP* linux_udp_iface=(Linux_UDP*)iface; // Cast from generic interface to linux udp
	dmsg( "setting iface udp interrupt at fd %d (linux_udp_iface %p)", linux_udp_iface->fd, linux_udp_iface);
	return set_linux_interrupt(linux_udp_iface->fd, flag);
}


int linux_set_udp_priority_interrupt(Interface_Descriptor* iface, void (*handler)(void)){
	Linux_UDP* linux_udp_iface=(Linux_UDP*)iface; // Cast from generic interface to linux udp
	dmsg( "setting priority iface udp interrupt at fd %d (linux_udp_iface %p)", linux_udp_iface->fd, linux_udp_iface);
	return set_linux_priority_interrupt(linux_udp_iface->fd, handler);
}


int linux_fflush_udp(Interface_Descriptor* iface){
	warnmsg("Unimplemented!");
	return swe_ok;
}






#endif
