/*
 * This file contains an implementation example of the notify for Linux systems.
 * This is intended to be used only to debug the notifications
 *
 *  @author: Enoc Martínez
 *  @institution: Universitat Politècnica de Catalunya (UPC)
 *  @contact: enoc.martinez@upc.edu
 */
#include "swe_conf.h"
#if LINUX_UDP_NOTIFICATIONS
#include "common/swe_utils.h"
#include "resources/notification_codes.h"
#include <arpa/inet.h>
#include <unistd.h>




static const char *notification_strings[] = {
	"Initialization",
	"System Check",
	"PUCK Protocol extract file",
	"PUCK Protocol write file",
	"Decoding SensorML",
	"Setup Scheduler",
	"Init Scheduler",
	"Start Scheduler",
	"Pause Scheduler",
	"Stop Scheduler",
	"Exit",
	"New Data File",
	"Enter Deep Sleep",
	"Exit Deep Sleep",
	"Open Interface",
	"Close Interface",
	"Temp Folder",
	"Output Folder",
	NULL
	};

int send_udp_frame(void* notification, int len );


/*
 * Notify an event by sending a UDP stream
 */
int linux_udp_notify(const int code, void* arg){ // Notify an event to the host platform
	if (code > ARRAY_LENGTH(notification_strings)) {
		errmsg("Unrecognized notification");
		return swe_error;
	}
	const char *notification = notification_strings[code];
	char buffer[1024];

	sprintf(buffer,"%d - Notification %d: \"%s\"", (int)getpid(), code, notification);


	if (arg != NULL ){
		if (code == SWEB_NOTIFY_ENTERING_DEEP_SLEEP) {
			// If deep sleep the argument is an unsigned integer
			float *seconds = (float*)arg;
			sprintf(buffer,"%s %f", buffer, *seconds);
		}
		else if ( arg != NULL ) {
			sprintf(buffer,"%s %s", buffer, (const char*)arg);
		}
	}
	cdmsg(CYN, "                   %s", buffer);
	send_udp_frame(buffer, strlen(buffer));
	return 0;
}

/*
 * Send UDP frame to a predefined address
 */
int send_udp_frame(void* notification, int len ){
	struct sockaddr_in sender, recipient;
	int sock;

	//create a UDP socket
	if ((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
		return -1;
	}

	// zero out the structure
	sender.sin_family = AF_INET;
	sender.sin_port = htons(0);
	sender.sin_addr.s_addr = htonl(INADDR_ANY);
	recipient.sin_family = AF_INET;
	recipient.sin_port = htons(LINUX_UDP_NOTIFICATIONS_PORT);
	recipient.sin_addr.s_addr = inet_addr(LINUX_UDP_NOTIFICATIONS_HOST);


	//bind socket to port
	if( bind(sock , (struct sockaddr*)&sender, sizeof(sender) ) == -1) 	{
		return -1;
	}

	if (sendto(sock , notification, len, 0, (struct sockaddr*) &recipient, sizeof(recipient)) == -1) {
		errmsg("Couldn't sent notification");
	}

	close(sock);
	return 0;
}

#endif /* LINUX_UDP_NOTIFICATIONS */
