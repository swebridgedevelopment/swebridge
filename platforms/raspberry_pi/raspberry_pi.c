/*
 ============================================================================
				   RASPBERRY_PI Platform system
 ============================================================================
 Implementation of platform handlers for raspberry_pi platform
 ============================================================================
 Author: Enoc Martínez
 Contact: enoc.martinez@upc.edu
 ============================================================================
 */
#include "swe_conf.h"

#if defined RASPBERRY_PI
#include "common/swe_utils.h"
#include "resources/resources.h"
#include "platforms/raspberry_pi/raspberry_pi.h"


#include <signal.h>
#include <unistd.h>
#include <sys/time.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <fcntl.h>

int rpi_set_signal_handler(int signal_num, void(*handler)(int signal));
void signal_exit_handler(int signal);


/*
 * Initialization function for generic raspberry_pi platforms
 */
int raspberry_pi_platform_init(int argc, char** argv){


	imsg("Initializing Raspberry Pi Platform");
	imsg("setting up SIGQUIT signal");
	rpi_set_signal_handler(SIGQUIT, signal_exit_handler);
	imsg("setting up SIGTERM signal");
	rpi_set_signal_handler(SIGTERM, signal_exit_handler);
	imsg("setting up SIGINT signal");
	rpi_set_signal_handler(SIGINT, signal_exit_handler);
	return swe_ok;
}

/*
 * Exit function for generic raspberry_pi platforms
 */
int raspberry_pi_platform_exit(){
	imsg("Closing all files");
	close_all_files();
	imsg("Closing all interfaces");
	close_all_interfaces();
	exit(0);
	// we should never be here //
	return swe_error;
}




/*
 * wrapper for linux exit
 */
void signal_exit_handler(int signal) {
	raspberry_pi_platform_exit();
}

/*
 * Configures a signal
 */
int rpi_set_signal_handler(int signal_num, void(*handler)(int signal)) {
	sigset_t mask;
	struct sigaction sa;


	sigprocmask(SIG_SETMASK, NULL, &mask); //get the current mask
	sigaddset(&mask, signal_num); //add the new signal
	//Disable signals
	sigprocmask(SIG_BLOCK, NULL, &mask); //block the new signal

	sa.sa_handler = handler; //define the handler function
	sa.sa_flags = 0;  //no flags needed
	sigemptyset(&sa.sa_mask); //Empty the signal mask (this mask blocks other signals during the execution of the signal handler)

	if (sigaction(signal_num, &sa, NULL) == -1) { //set the sigaction to the signal
		return -1;
	}

	sigaddset(&mask, signal_num); //add the signal to the mask
	if (sigprocmask(SIG_SETMASK, &mask, NULL) == -1){
		return -1;
	}

	sigprocmask(SIG_UNBLOCK, &mask, NULL); // Set the mask
	return swe_ok;
}

/*
 * Sleep platform until a timer expires or a interface interruption is received
 */
int raspberry_pi_platform_standby(){
	sleep(3600); //sleep for 1 hour or until next timer interrupt
	return swe_ok;
}


/*
 * fflush stdout
 */
int raspberry_pi_linux_fflush() {
	fflush(stdout);
	return swe_ok;
}



#endif

