/*
 ============================================================================
				   RASPBERRY Platform Power Management
 ============================================================================
 Implementation of platform handlers for raspberry platform
 ============================================================================
 Author: Enoc Martínez
 Contact: enoc.martinez@upc.edu
 ============================================================================
 */
#include "swe_conf.h"

#if defined USE_RASPBERRY_PI_POWER
#include "common/swe_utils.h"
#include "platforms/raspberry_pi/raspberry_pi_gpio.h"
#include <unistd.h>


#define RASBPERRY_PI_GPIO_EXPORT "/sys/class/gpio/export"
#define RASPBERRY_PI_GPIO_DIRECTION_FORMAT "/sys/class/gpio/gpio%d/direction"
#define RASPBERRY_PI_GPIO_VALUE "/sys/class/gpio/gpio%d/direction"

/*
 * To initialize a raspberry pi GPIO pin:
 * Step   I. Export pin number
 * Step  II. Declare it as outpuT
 */
int raspberry_pi_power_initialize(int sensorid) {
	dmsg("initializing GPIO %d...", sensorid);
	TRY_RET(GPIOExport(sensorid));
	usleep(100);
	dmsg("setting GPIO %d as output...", sensorid);
	return GPIODirection(sensorid, 1);
}

int raspberry_pi_power_on_sensor(int sensorid) {
	return GPIOWrite(sensorid, HIGH);
}

int raspberry_pi_power_off_sensor(int sensorid) {
	return GPIOWrite(sensorid, LOW);
}

#endif
