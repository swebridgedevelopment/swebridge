/*
============================================================================
			   RASPBERRY_PI Platform Power Management
============================================================================
Declaration of platform handlers for raspberry_pi platform
============================================================================
Author: Enoc Martínez
Contact: enoc.martinez@upc.edu
============================================================================
*/

#ifndef RESOURCES_PLATFORMS_RASPBERRY_PI_INC_RASPBERRY_PI_POWER_H_
#define RESOURCES_PLATFORMS_RASPBERRY_PI_INC_RASPBERRY_PI_POWER_H_

#include "swe_conf.h"

#if defined USE_RASPBERRY_PI_POWER

int raspberry_pi_power_initialize(int sensorid);
int raspberry_pi_power_on_sensor(int sensorid);
int raspberry_pi_power_off_sensor(int sensorid);

#endif /* RASPBERRY_PI_POWER */

#endif /* RESOURCES_PLATFORMS_RASPBERRY_PI_INC_RASPBERRY_PI_POWER_H_ */
