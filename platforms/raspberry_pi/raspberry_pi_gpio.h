/*
 * raspberry.h
 *
 *  Created on: 21 feb. 2019
 *      Author: carla
 */

#ifndef RESOURCES_PLATFORMS_RASPBERRY_PI_INC_RASPBERRY_PI_GPIO_H_
#define RESOURCES_PLATFORMS_RASPBERRY_PI_INC_RASPBERRY_PI_GPIO_H_

#define IN  0
#define OUT 1

#define LOW  0
#define HIGH 1

#define PIN  21 /* P1-18 */

int GPIOExport(int pin);
int GPIOUnexport(int pin);
int GPIODirection(int pin, int dir);
int GPIORead(int pin);
int GPIOWrite(int pin, int value);



#endif /* RESOURCES_PLATFORMS_RASPBERRY_PI_INC_RASPBERRY_PI_GPIO_H_ */
