/*
 ============================================================================
				        RASPBERRY_PI Platform
 ============================================================================

  SWE Bridge configuration for for raspberry_pi platform. Within
  this file the following parameters need to be defined:

  	- Include header files for platform resources (uart, tcp, udp
     and filesystem)

  	- Declare the platform handlers (init, exit, etc.)

  	- Assign the handlers

 ============================================================================
 Author: Enoc Martínez
 Contact: enoc.martinez@upc.edu
 ============================================================================
 */

#ifndef RESOURCES_PLATFORMS_RASPBERRY_PI_INC_RASPBERRY_PI_H_
#define RESOURCES_PLATFORMS_RASPBERRY_PI_INC_RASPBERRY_PI_H_

#include "swe_conf.h"
#ifdef RASPBERRY_PI

//---- Include raspberry_pi resources ----//
#include "platforms/raspberry_pi/raspberry_pi_power.h"

//---- Include generic linux resources ----//
//---- Include generic linux resources ----//
#include "platforms/linux/linux_tcp_udp.h"
#include "platforms/linux/linux_uart.h"
#include "platforms/linux/linux_filesystem.h"
#include "platforms/linux/linux_timer.h"
//---------------------------------------------------------------------------//
//-------------------- DEFINE GENERIC RASPBERRY_PI FUCNTIONS -----------------------//
//---------------------------------------------------------------------------//
int raspberry_pi_platform_init(int argc, char** argv);
int raspberry_pi_platform_exit();
int raspberry_pi_platform_standby();
int raspberry_pi_platform_get_coordinates(float* latitude, float* longitude);
int raspberry_pi_linux_fflush();

//---------------------------------------------------------------------------//
//-------------------- DEFINE GENERIC RASPBERRY_PI HANDLERS ------------------------//
//---------------------------------------------------------------------------//

/* UART Functions */
#define platform_open_uart linux_open_uart
#define platform_read_uart linux_read_uart
#define platform_write_uart linux_write_uart
#define platform_close_uart linux_close_uart
#define	platform_fflush_uart linux_fflush_uart
#define	platform_set_baudrate linux_set_baudrate
#define platform_set_uart_interrupt linux_set_uart_interrupt

/* TCP Functions */
#define platform_open_tcp linux_open_tcp
#define platform_read_tcp linux_read_tcp
#define platform_write_tcp linux_write_tcp
#define platform_close_tcp linux_close_tcp
#define	platform_fflush_tcp linux_fflush_tcp
#define platform_set_tcp_interrupt linux_set_tcp_interrupt
#define platform_set_tcp_priority_interrupt linux_set_tcp_priority_interrupt


/* UDP Functions */
#define platform_open_udp linux_open_udp
#define platform_read_udp linux_read_udp
#define platform_write_udp linux_write_udp
#define platform_close_udp linux_close_udp
#define	platform_fflush_udp linux_fflush_udp
#define platform_set_udp_interrupt linux_set_udp_interrupt
#define platform_set_udp_priority_interrupt linux_set_udp_priority_interrupt


/* FILESYSTEM Functions */
#define platform_fopen linux_fopen
#define platform_fclose linux_fclose
#define platform_fwrite linux_fwrite
#define platform_fread linux_fread
#define platform_create_dir linux_create_dir
#define platform_fremove linux_fremove
#define platform_file_size linux_file_size
#define platform_fmove linux_fmove
#define platform_getc linux_getc
#define platform_eof linux_eof
#define platform_set_pointer linux_set_pointer
#define platform_list_dir linux_list_dir

/* TIMING Functions */
#define platform_delayms linux_delayms
#define platform_get_epoch_time linux_get_epoch_time
#define platform_set_timer linux_set_timer
#define platform_start_timer linux_start_timer
#define platform_stop_timer linux_stop_timer
#define platform_set_timeout linux_set_timeout
#define platform_disable_timeout linux_disable_timeout

/* Platform Functions */
#define platform_init raspberry_pi_platform_init
#define platform_exit raspberry_pi_platform_exit
#define platform_standby raspberry_pi_platform_standby
#define platform_deep_sleep linux_deep_sleep
#define platform_vprintf vprintf
#define platform_vfprintf vfprintf
#define platform_fflush raspberry_pi_linux_fflush
#if (( LINUX_UDP_NOTIFICATIONS == TRUE ))
#define platform_notify linux_udp_notify
#endif

// Simulate position //
#ifdef SIMULATE_COORDINATES
#include "platforms/common/simulate_coordinates.h"
#define platform_get_coordinates simulate_coordinates
#define platform_get_altitude simulate_altitude
#endif


#ifdef SIMULATE_INTERNAL_SENSORS
#include "platforms/common/simulate_internal_sensors.h"
// Internal Sensors //
#define platform_internal_sensors_initialize simulate_internal_sensors_initialize
#define platform_internal_sensors_read simulate_internal_sensors_read
#endif


// ADCs //
#ifdef SIMULATE_ADC
#include "platforms/common/simulate_adc.h"
// Simulate ADC Resources //
#define platform_adc_initialize simulate_adc_initialize
#define platform_adc_read simulate_adc_read
#endif


// Power Management Functions //
// NOTE: In Generic Example power management is disabled!
#ifdef USE_RASPBERRY_PI_POWER
#define platform_power_initialize raspberry_pi_power_initialize
#define platform_power_on_sensor raspberry_pi_power_on_sensor
#define platform_power_off_sensor raspberry_pi_power_off_sensor
#endif // USE_RASPBERRY_PI_POWER /

#endif /* RASPBERRY_PI */

#endif /* RESOURCES_PLATFORMS_RASPBERRY_PI_INC_RASPBERRY_PI_H_ */
