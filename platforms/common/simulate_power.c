#include "common/swe_utils.h"
#include "swe_conf.h"

#ifdef SIMULATE_POWER_MANAGEMENT

/*
 * Initialize power
 */
int simulate_power_initialize(int index) {

	cimsg(GRN, "Initializing Simulated power (sensor %d)", index);

	return swe_ok;
}


/*
 * Power ON sensor
 */
int simulate_power_on(int index) {

	cimsg(GRN, "Simulate Power ON (sensor %d)", index);

	return swe_ok;
}

/*
 * Power OFF sensor
 */
int simulate_power_off(int index) {
	cimsg(MAG, "Simulate Power OFF (sensor %d)", index);
	return swe_ok;
}
#endif
