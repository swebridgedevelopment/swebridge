/*
 * 
 *
 *  @author: Enoc Martínez
 *  @institution: Universitat Politècnica de Catalunya (UPC)
 *  @contact: enoc.martinez@upc.edu
 */
#ifndef PLATFORMS_COMMON_SIMULATE_COORDINATES_H_
#define PLATFORMS_COMMON_SIMULATE_COORDINATES_H_

#include "swe_conf.h"
#ifdef SIMULATE_COORDINATES
int simulate_coordinates(float* lat, float* lon);
int simulate_altitude(float* a);
#endif


#endif /* PLATFORMS_COMMON_SIMULATE_COORDINATES_H_ */
