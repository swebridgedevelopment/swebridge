
#include "swe_conf.h"

#ifdef SIMULATE_ADC
#include "common/swe_utils.h"

int simulate_adc_initialize(int channel){
	dmsg("Simulating ADC channel %d", channel);
	return swe_ok;
}

int simulate_adc_read(int channel, float* meas){

	if (meas == NULL){
		return swe_invalid_arguments;
	}

	int r =  rand();

	*meas = (float)r/1000000;

	return swe_ok;
}

#endif // SIMULATE_POWER_MANAGEMENT
