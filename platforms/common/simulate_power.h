


#ifndef RESOURCES_PLATFORMS_COMMON_INC_SIMULATE_POWER_H_
#define RESOURCES_PLATFORMS_COMMON_INC_SIMULATE_POWER_H_

#include "swe_conf.h"

#ifdef SIMULATE_POWER_MANAGEMENT
int simulate_power_initialize(int index);
int simulate_power_on(int index);
int simulate_power_off(int index);
#endif // SIMULATE_POWER_MANAGEMENT //
#endif // RESOURCES_PLATFORMS_COMMON_INC_SIMULATE_POWER_H_ //
