/*
 * fatfs_filesystem.h
 *
 *  Created on: Apr 16, 2018
 *      Author: enoc
 */

#ifndef RESOURCES_PLATFORMS_COMMON_INC_FATFS_FILESYSTEM_H_
#define RESOURCES_PLATFORMS_COMMON_INC_FATFS_FILESYSTEM_H_


#if defined USE_FATFS

#include "swe_conf.h"

swe_file* fatfs_fopen(const char* filename, swe_file_flags flags);
int fatfs_fclose(swe_file* file);
int fatfs_fwrite(const void* data, int bytes, swe_file* file);
int fatfs_fread(void* readData,int bytes, swe_file* file);
int fatfs_create_dir(const char *dirname);
int fatfs_fremove(const char *filename);
int fatfs_file_size(swe_file* filename);
int fatfs_fmove(const char* oldname, const char* newname);
int fatfs_getc(swe_file *file);
int fatfs_eof(swe_file* file);
#endif // FATFS

#endif /* RESOURCES_PLATFORMS_FATFS_INC_FATFS_FILESYSTEM_H_ */
