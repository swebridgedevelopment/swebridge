/*
 * 
 *
 *  @author: Enoc Martínez
 *  @institution: Universitat Politècnica de Catalunya (UPC)
 *  @contact: enoc.martinez@upc.edu
 */

#include "swe_conf.h"


#ifdef SIMULATE_COORDINATES

static float latitude = 27.992014;
static float longitude = -15.3707307;
static float altitude = 0.00;
static float increment = 0.05;

/*
 * This function simulates a
 */
int simulate_coordinates(float* lat, float* lon){

	latitude += increment;
	if (latitude > 90) latitude -= 90;
	else if (latitude < -90) latitude += 90;

	longitude += increment;
	if (longitude > 90) latitude -= 90;
	else if (longitude < -90) latitude += 90;

	*lat = latitude;
	*lon = longitude;

	return swe_ok;
}



/*
 * This function simulates the altitude
 */
int simulate_altitude(float* a){

	altitude -= increment;
	*a = altitude;
	return swe_ok;
}

#endif /* SIMULATE_COORDINATES */
