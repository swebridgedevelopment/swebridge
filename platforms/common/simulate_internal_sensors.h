/*
 * test_internal_sensors.h
 *
 *  Created on: Oct 18, 2018
 *      Author: enoc
 */

#ifndef RESOURCES_PLATFORMS_COMMON_INC_SIMULATE_INTERNAL_SENSORS_H_
#define RESOURCES_PLATFORMS_COMMON_INC_SIMULATE_INTERNAL_SENSORS_H_

#include "swe_conf.h"
#ifdef SIMULATE_INTERNAL_SENSORS
int simulate_internal_sensors_initialize(int sensor_index);
int simulate_internal_sensors_read(int sensor_index, void* buffer, int bufflen);
#endif

#endif /* RESOURCES_PLATFORMS_COMMON_INC_SIMULATE_INTERNAL_SENSORS_H_ */
