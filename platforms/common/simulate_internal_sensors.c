/*
 * test_internal_sensors.h
 *
 *  Created on: Oct 18, 2018
 *      Author: enoc
 */


#include "swe_conf.h"

#ifdef SIMULATE_INTERNAL_SENSORS

#include "common/swe_utils.h"

/*
 * Initialize internal sensors for
 */
int simulate_internal_sensors_initialize(int sensor_index){
	dmsg("Simulating Internal Sensor %d", sensor_index);
	return swe_ok;
}

/*
 * Dummy read internal sensors, depending on sensor index different values are replied.
 * Returns the number of bytes in buffer
 */
int simulate_internal_sensors_read(int sensor_index, void* buffer, int bufflen){
	int reply_size = 0;
	if (sensor_index == 0) {
		// Index 1 returns 3 floats sepearated by comma //
		// i.e. "123.456,789.456,987.456\r\n"
		char reply[200];

		sprintf(reply, "%f,%f,%f\r\n", 0.001*(float)rand(), 0.001*(float)rand(), 0.001*(float)rand());
		reply_size = strlen(reply);
		if (bufflen < reply_size) {
			errmsg("BUffer too small");
			return swe_error;
		}
		memcpy(buffer, reply, reply_size + 1);
	}
	else {
		errmsg("Sensor inex out of bounds");
		return swe_error;
	}
	return reply_size;
}

#endif
