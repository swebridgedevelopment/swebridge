#include "swe_conf.h"

#if defined USE_FATFS

#include <stdlib.h>
#include "common/swe_utils.h"
#include "resources/resources.h"
#include "platforms/commonfatfs_filesystem.h"



static const char* fat_fs_errocode_strings[]  = {
		"Succeeded",
		"FR_DISK_ERR: (1) A hard error occurred in the low level disk I/O layer",
		"FR_INT_ERR, (2) Assertion failed",
		"FR_NOT_READY: (3) The physical drive cannot work",
		"FR_NO_FILE: (4) Could not find the file",
		"FR_NO_PATH: (5) Could not find the path",
		"FR_INVALID_NAME: (6) The path name format is invalid",
		"FR_DENIED: (7) Access denied due to prohibited access or directory full",
		"FR_EXIST: (8) Access denied due to prohibited access",
		"FR_INVALID_OBJECT: (9) The file/directory object is invalid",
		"FR_WRITE_PROTECTED: (10) The physical drive is write protected",
		"FR_INVALID_DRIVE: (11) The logical drive number is invalid",
		"FR_NOT_ENABLED: (12) The volume has no work area",
		"FR_NO_FILESYSTEM: (13) There is no valid FAT volume",
		"FR_MKFS_ABORTED: (14) The f_mkfs() aborted due to any parameter error",
		"FR_TIMEOUT: (15) Could not get a grant to access the volume within defined period",
		"FR_LOCKED: (16) The operation is rejected according to the file sharing policy",
		"FR_NOT_ENOUGH_CORE: (17) LFN working buffer could not be allocated",
		"FR_TOO_MANY_OPEN_FILES: (18) Number of open files > _FS_LOCK",
		"FR_INVALID_PARAMETER /* (19) Given parameter is invalid"
};

/*
 * Macro that prints a FAT FS errorcode
 */
#define FAT_FS_ERROR(_code) \
	errmsg("Error %d at function %s (line %d)",(int)_code, __func__, __LINE__); \
	errmsg("FatFs errorcode: \"%s\"", fat_fs_errocode_strings[(int)_code]); \
	errmsg("trapping execution into while(1) loop"); \
	while(1) {}




/*
 * Costof2 Open Function
 */
swe_file* fatfs_fopen(const char* filename, swe_file_flags flags){


	FIL* file = swe_malloc(sizeof(FIL));

	BYTE mode;
	FRESULT result;

	if ( flags == swe_read_file ){
		mode = FA_READ;
	}
	else if ( flags == swe_write_file) {
		mode = FA_WRITE;
	}
	else if ( flags == swe_append_file ) {
		errmsg("Trying to open in mode \"append\", unimplemented in FATFS!");
		return NULL;
	}
	else {
		errmsg("Unknown flags at function %s", __func__);
		return NULL;
	}

	// Open file

	result = f_open(file, filename, mode); /* Open or create a file */
	if ( result != FR_OK ) { // Print error and exit
		FAT_FS_ERROR(result);
		return NULL;
	}
	return file;
}

/*
 * Costof2 Close function
 */
int fatfs_fclose(swe_file* file){
	FRESULT result = f_close(file);
	if (result != FR_OK) {
		FAT_FS_ERROR(result);
		return swe_filesystem_error;
	}
	return swe_ok;
}

/*
 * Costof2 fwrite
 */
int fatfs_fwrite(const void* data, int bytes, swe_file* file){
	UINT bytes_written=0;
	FRESULT result = f_write(file, data, bytes, &bytes_written);
	if (result != FR_OK) {
		FAT_FS_ERROR(result);
		return swe_filesystem_error;
	}
	return (int)bytes_written;
}


/*
 * read file
 */
int fatfs_fread(void* readData,int bytes, swe_file* file){
	UINT read_bytes=0;
	delayms(1);
	FRESULT result = f_read(file, readData, bytes, &read_bytes);
	if (result != FR_OK) {
		FAT_FS_ERROR(result);
		return swe_filesystem_error;
	}
	return (int)read_bytes;
}

/*
 * Create directory
 */
int fatfs_create_dir(const char *dirname){
	FRESULT result = f_mkdir(dirname);
	if (result != FR_OK) {
		FAT_FS_ERROR(result);
		return swe_filesystem_error;
	}
	return swe_ok;
}

/*
 * Remove File
 */
int fatfs_fremove(const char *filename){
	FRESULT result = f_unlink(filename);
	if (result != FR_OK) {
		FAT_FS_ERROR(result);
		return swe_filesystem_error;
	}
	return swe_ok;
}

/*
 * Get the file size from an open file
 */
// TODO implement FATFS file size
int fatfs_file_size(swe_file* file){

	return (int) f_size(file);
}

/*
 * Move / rename a file
 */
int fatfs_fmove(const char* oldname, const char* newname){
	FRESULT result = f_rename(oldname, newname);
	if (result != FR_OK) {
		FAT_FS_ERROR(result);
		return swe_filesystem_error;
	}
	return swe_ok;
}

/*
 * Reads a char from a file
 */
int fatfs_getc(swe_file *file){
	// TODO implemente getchar for fatfs
	char result;
	int ret;
	if ( (ret=fatfs_fread(&result, sizeof(char), file)) < 0) {
		return ret;
	}
	return (int)result;
}

/*
 * returns true if EOF has been reached within file
 */
int fatfs_eof(swe_file* file) {
	return f_eof(file);
}





#endif // FATFS
