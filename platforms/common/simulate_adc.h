


#ifndef RESOURCES_PLATFORMS_COMMON_INC_SIMULATE_ADC_H_
#define RESOURCES_PLATFORMS_COMMON_INC_SIMULATE_ADC_H_

#include "swe_conf.h"
#ifdef SIMULATE_ADC
int simulate_adc_initialize(int channel);

int simulate_adc_read(int channel, float* meas);
#endif // SIMULATE_ADC //
#endif // RESOURCES_PLATFORMS_COMMON_INC_SIMULATE_ADC_H_ //
