/*
 ============================================================================
				        EXAMPLE Platform
 ============================================================================

  SWE Bridge configuration for for example platform. Within
  this file the following parameters need to be defined:

  	- Include header files for platform resources (uart, tcp, udp
     and filesystem)

  	- Declare the platform handlers (init, exit, etc.)

  	- Assign the handlers

 ============================================================================
 Author: Enoc Martínez
 Contact: enoc.martinez@upc.edu
 ============================================================================
 */

#ifndef RESOURCES_PLATFORMS_EXAMPLE_INC_EXAMPLE_H_
#define RESOURCES_PLATFORMS_EXAMPLE_INC_EXAMPLE_H_

#include "swe_conf.h"
#ifdef EXAMPLE

//---- Include generic example resources ----//
#include "platforms/example/inc/example_tcp_udp.h"
#include "platforms/example/inc/example_uart.h"
#include "platforms/example/inc/example_filesystem.h"
#include "platforms/example/inc/example_timer.h"
#include "platforms/example/inc/example_power.h"

//---------------------------------------------------------------------------//
//-------------------- DEFINE GENERIC EXAMPLE FUCNTIONS -----------------------//
//---------------------------------------------------------------------------//
int example_platform_init(int argc, char** argv);
int example_platform_exit();
int example_platform_standby();
int example_platform_get_coordinates(float* latitude, float* longitude);

//---------------------------------------------------------------------------//
//-------------------- DEFINE GENERIC EXAMPLE HANDLERS ------------------------//
//---------------------------------------------------------------------------//

/* UART Functions */
#define platform_open_uart example_open_uart
#define platform_read_uart example_read_uart
#define platform_write_uart example_write_uart
#define platform_close_uart example_close_uart
#define	platform_fflush_uart example_fflush_uart
#define	platform_set_baudrate example_set_baudrate
#define platform_set_uart_interrupt example_set_uart_interrupt

/* TCP Functions */
#define platform_open_tcp example_open_tcp
#define platform_read_tcp example_read_tcp
#define platform_write_tcp example_write_tcp
#define platform_close_tcp example_close_tcp
#define	platform_fflush_tcp example_fflush_tcp
#define platform_set_tcp_interrupt example_set_tcp_interrupt

/* UDP Functions */
#define platform_open_udp example_open_udp
#define platform_read_udp example_read_udp
#define platform_write_udp example_write_udp
#define platform_close_udp example_close_udp
#define	platform_fflush_udp example_fflush_udp
#define platform_set_udp_interrupt example_set_udp_interrupt


/* FILESYSTEM Functions */
#define platform_fopen example_fopen
#define platform_fclose example_fclose
#define platform_fwrite example_fwrite
#define platform_fread example_fread
#define platform_create_dir example_create_dir
#define platform_fremove example_fremove
#define platform_file_size example_file_size
#define platform_fmove example_fmove
#define platform_getc example_getc
#define platform_eof example_eof

/* TIMING Functions */
#define platform_delayms example_delayms
#define platform_get_time example_get_time
#define platform_get_epoch_time example_get_epoch_time
#define platform_set_timer example_set_timer
#define platform_start_timer example_start_timer
#define platform_stop_timer example_stop_timer
#define platform_set_timeout example_set_timeout
#define platform_disable_timeout example_disable_timeout

/* Platform Functions */
#define platform_init example_platform_init
#define platform_exit example_platform_exit
#define platform_standby example_platform_standby
#define platform_get_coordinates example_platform_get_coordinates
#define platform_system_monitor NULL

/* Power Management Functions */
// NOTE: In Generic Example power management is disabled!
#define platform_power_on_sensor example_power_on_sensor
#define platform_power_off_sensor example_power_off_sensor

#endif /* EXAMPLE */

#endif /* RESOURCES_PLATFORMS_EXAMPLE_INC_EXAMPLE_H_ */
