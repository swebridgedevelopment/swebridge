/*
 ============================================================================
						EXAMPLE Platform Timer
 ============================================================================
 Implementation of a EXAMPLE Platform Timer
 ============================================================================
 Author: Enoc Martínez
 Contact: enoc.martinez@upc.edu
 ============================================================================
 */

#ifndef EXAMPLE_TIMER_H_
#define EXAMPLE_TIMER_H_

#include "swe_conf.h"

#if defined USE_EXAMPLE_TIMER



int example_delayms(ulong msecs);
int example_get_time(Time_Structure* mytime);
ulong example_get_epoch_time();
int example_set_timer(ulong tmout_interval_ms, void(*handler)());
int example_start_timer();
int example_stop_timer();



#endif
#endif
