/*
 ============================================================================
				   EXAMPLE Platform system
 ============================================================================
 Implementation of platform handlers for example platform
 ============================================================================
 Author: Enoc Martínez
 Contact: enoc.martinez@upc.edu
 ============================================================================
 */
#include "swe_conf.h"

#if defined EXAMPLE

#include "platforms/example/inc/example.h"

/*
 * Initialization function for generic example platforms
 */
int example_platform_init(int argc, char** argv){
	// Setup signal to handle SIGTERM

	imsg("Initializing generic Example Platform");
	return swe_ok;
}

/*
 * Exit function for generic example platforms
 */
int example_platform_exit(){
	// TODO close any open files

	// TODO close any open communication's interface

	// TODO exit

	return swe_error;
}

/*
 * Sleep platform until a timer expires or a interface interruption is received
 */
int example_platform_standby(){
	return swe_ok;
}

/*
 * Get the position, unimplemented by default
 */
int example_platform_get_coordinates(float* lat, float* lon)) {

	return swe_unimplemented;

	return swe_ok;
}


#endif

