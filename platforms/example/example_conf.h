/*
 ============================================================================
				   Example Configuration File
 ============================================================================
 This file configures the project for a generic Example platform. The
 following actions are required:

	- Include Generic C Libraries
	- Defines types uchar, uint, ulong and swe_file (if not defined)
	- Define the activation flags for the platform handlers or generic handlers
	- Define the starting argument
	- Define the Platform Name

 ============================================================================
 Author: Enoc Martínez
 Contact: enoc.martinez@upc.edu
 ============================================================================
 */

#ifndef RESOURCES_PLATFORMS_EXAMPLE_INC_EXAMPLE_CONF_H_
#define RESOURCES_PLATFORMS_EXAMPLE_INC_EXAMPLE_CONF_H_

#include "swe_conf.h" // include swe_conf to import the definition of the current platform and common types

#ifdef EXAMPLE // compile this file only If the example platform is defined

// Include generic C Example libraries for the current platform //
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

// Declare uchar, uint and ulong (if they are not previously declared in the platform//
typedef uint8_t uchar;
typedef uint32_t uint;
typedef uint64_t ulong;

// Declare file type used by the platform (i.e. FILE in UNIX, FIL in FatFs, etc.) //
typedef FILE swe_file;


// These flags are used to activate the platform-specific resources. They allow to use some handlers from one platform
// I.e. if USE_FATFS is defined, the handlers for FatFs filesystem will be compiled.
#define USE_EXAMPLE_FILESYSTEM
#define USE_EXAMPLE_UART
#define USE_EXAMPLE_TCP_UDP
#define USE_EXAMPLE_TIMER
#define USE_EXAMPLE_POWER

/*
 * This is the first relevant argument which is passed to the code. In UNIX systems it is usually 1 (index 0 corresponds
 * to the program name) while in embedded systems usually is 0
 */
#define STARTING_ARGUMENT 1


#define PLATFORM_NAME "Example Platform" // String identifying the platform


#endif /* EXAMPLE */

#endif /* RESOURCES_PLATFORMS_EXAMPLE_INC_EXAMPLE_CONF_H_ */
