/*
 ============================================================================
						EXAMPLE Platform UART
 ============================================================================
 Implementation of a EXAMPLE Platform UART
 ============================================================================
 Author: Enoc Martínez
 Contact: enoc.martinez@upc.edu
 ============================================================================
 */

#include "swe_conf.h"

#if defined USE_EXAMPLE_UART

#include "platforms/example/inc/example_uart.h"


/*
 * RETURN CODES: Functions should return 0 (swe_unimplemented) in success except when
 * otherwise is specified (i.e. the number of bytes written,  * a pointer to a structure,
 *  etc.) In case of failure an errorcode should be set (a negative value, defined in
 *  swe_conf.h). In case of a function that returns a pointer, in case of failure a NULL
 *  pointer should be returned. By default, functions in this file return unimplemented
 *  errorcode (swe_unimplemented) or NULL.
 */


/*
 * Opens a Example UART and returns a pointer to the Example_UART structure containing
 * its settings and file descriptor
 */
Interface_Descriptor* example_open_uart(Interface_Options* opts){
	return NULL;
}


int example_set_baudrate(Interface_Descriptor* iface, ulong baudrate_in){
	return swe_unimplemented;
}



int example_write_uart(Interface_Descriptor* iface, void* buffer, uint size){
	return swe_unimplemented;

}

int example_fflush_uart(Interface_Descriptor* iface){
	return swe_unimplemented;
}


int char_ready(Interface_Descriptor* iface, uint tmout){
	return swe_unimplemented;
}

int example_read_uart(Interface_Descriptor* iface, uchar* buffer, uint max_bytes, ulong timeout_us){
	return swe_unimplemented;

}

int example_close_uart(Interface_Descriptor* iface){
	return swe_unimplemented;
}

int example_set_uart_interrupt(void *iface, uchar* flag){
	return swe_unimplemented;
}

#endif /* USE_EAMPLE_UART */
