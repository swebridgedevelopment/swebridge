/*
============================================================================
			   EXAMPLE Platform Power Management
============================================================================
Declaration of platform handlers for example platform
============================================================================
Author: Enoc Martínez
Contact: enoc.martinez@upc.edu
============================================================================
*/

#ifndef RESOURCES_PLATFORMS_EXAMPLE_INC_EXAMPLE_POWER_H_
#define RESOURCES_PLATFORMS_EXAMPLE_INC_EXAMPLE_POWER_H_

#include "swe_conf.h"

#if defined USE_EXAMPLE_POWER
#include "common/swe_utils.h"
#include <example/inc/example_power.h>


int example_power_on_sensor(int sensorid);
int example_power_off_sensor(int sensorid);

#endif /* EXAMPLE_POWER */

#endif /* RESOURCES_PLATFORMS_EXAMPLE_INC_EXAMPLE_POWER_H_ */
