/*
 ============================================================================
				       EXAMPLE Platform  File system
 ============================================================================
 Implementation of File System handlers for example platform
 ============================================================================
 Author: Enoc Martínez
 Contact: enoc.martinez@upc.edu
 ============================================================================
 */



#ifndef RESOURCES_EXAMPLE_INC_EXAMPLE_FILESYSTEM_H_
#define RESOURCES_EXAMPLE_INC_EXAMPLE_FILESYSTEM_H_

#include "swe_conf.h"
#ifdef USE_EXAMPLE_FILESYSTEM

/*
 * SWE Bridge wrapper for open file
 */
swe_file* example_fopen(const char* filename, swe_file_flags flags);
int example_fclose(swe_file* file);
int example_fwrite(const void* data, int bytes, swe_file* file);
int example_fread(void* readData,int bytes, swe_file* file);
int example_create_dir(const char *dirname);
int example_fremove(const char *filename);
int example_file_size(swe_file* filename);
int example_fmove(const char* oldname, const char* newname);
int example_getc(swe_file* filename);
int example_eof(swe_file* filename);

#endif /* USE_EXAMPLE_FILESYSTEM */

#endif /* RESOURCES_EXAMPLE_INC_EXAMPLE_FILESYSTEM_H_ */
