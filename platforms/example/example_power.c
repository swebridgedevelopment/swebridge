/*
 ============================================================================
				   EXAMPLE Platform Power Management
 ============================================================================
 Implementation of platform handlers for example platform
 ============================================================================
 Author: Enoc Martínez
 Contact: enoc.martinez@upc.edu
 ============================================================================
 */
#include "swe_conf.h"

#if defined USE_EXAMPLE_POWER
#include "common/swe_utils.h"
#include "platforms/example/inc/example_power.h"


int example_power_on_sensor(int sensorid) {
	return swe_unimplemented;
	return swe_ok;
}


int example_power_off_sensor(int sensorid) {
	return swe_unimplemented;
	return swe_ok;
}

#endif
