/*
 ============================================================================
						EXAMPLE Platform Timer
 ============================================================================
 Implementation of a EXAMPLE Platform Timer
 ============================================================================
 Author: Enoc Martínez
 Contact: enoc.martinez@upc.edu
 ============================================================================
 */


#include "swe_conf.h"

#if defined USE_EXAMPLE_TIMER

/*
 * Sleeps for n useconds
 */
int example_delayms(ulong msecs) {
	return swe_unimplemented;
}


/*
 * Returns the epoch time
 */
ulong example_get_epoch_time(){
	return swe_unimplemented;
}

/*
 * Fills the Time_Structure
 */
int example_get_time(Time_Structure* mytime) {
	return swe_unimplemented;

}

/*
 * Configures a periodic real-time signal
 */
int example_set_timer(ulong period_us, void(*handler)()){
	return swe_unimplemented;
}

/*
 * Starts a previously configured timer. Unblocks the timer signal from the program mask
 */
int example_start_timer(){
	return swe_unimplemented;
}

/*
 * Blocks the timer signal from the program mask
 */
int example_stop_timer(){
	return swe_unimplemented;

}



#endif //EXAMPLE
