/*
 ============================================================================
				   EXAMPLE Platform TCP UDP
 ============================================================================
 Implementation of example platform TCP / UDP handlers
 ============================================================================
 Author: Enoc Martínez
 Contact: enoc.martinez@upc.edu
 ============================================================================
 */
#ifndef EXAMPLE_INC_EXAMPLE_TCP_UDP_H_
#define EXAMPLE_INC_EXAMPLE_TCP_UDP_H_


#include "swe_conf.h"

#if defined USE_EXAMPLE_TCP_UDP

//-----------------------------------------------------------------//
//------------------------ TCP Interface --------------------------//
//-----------------------------------------------------------------//
typedef struct{
	/*
	 * Add here the required fields for TCP
	 */
} Example_TCP;

Interface_Descriptor* example_open_tcp(Interface_Options* opts);
int example_write_tcp(Interface_Descriptor* iface, void* buffer, uint size);
int example_read_tcp(Interface_Descriptor* iface, uchar* buffer, uint max_bytes,  ulong timeout_us);
int example_close_tcp(Interface_Descriptor* iface);
int example_fflush_tcp(Interface_Descriptor* iface);
int example_set_tcp_interrupt(Interface_Descriptor* iface, uchar* flag);


//-----------------------------------------------------------------//
//------------------------ UDP Interface --------------------------//
//-----------------------------------------------------------------//
typedef Example_TCP Example_UDP; // use the same structure for TCP and UDP or declare a different structure for UDP

Interface_Descriptor* example_open_udp(Interface_Options* opts);
int example_write_udp(Interface_Descriptor* iface, void* buffer, uint size);
int example_read_udp(Interface_Descriptor* iface, uchar* buffer, uint max_bytes,  ulong timeout_us);
int example_close_udp (Interface_Descriptor* iface);
int example_fflush_udp (Interface_Descriptor* iface);
int example_set_udp_interrupt(Interface_Descriptor* iface, uchar* flag);



#endif /* Example */

#endif /* EXAMPLE_INC_EXAMPLE_TCP_UDP_H_ */
