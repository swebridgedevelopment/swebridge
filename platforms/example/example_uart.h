/*
 ============================================================================
						EXAMPLE Platform UART
 ============================================================================
 Implementation of a EXAMPLE Platform UART
 ============================================================================
 Author: Enoc Martínez
 Contact: enoc.martinez@upc.edu
 ============================================================================
 */
#ifndef EXAMPLE_UART_H_
#define EXAMPLE_UART_H_


#include "swe_conf.h"

#ifdef USE_EXAMPLE_UART
#include "resources/resources.h"
#include <termios.h>


/*
 * Structure that holds the information for EXAMPLE UART devices
 */
typedef struct{
	int fd;
	char* serial_device;
	struct termios current_settings;
	struct termios original_settings;
} Example_UART;




Interface_Descriptor* example_open_uart(Interface_Options* conf);
int example_write_uart(Interface_Descriptor* iface, void* buffer, uint size);
int example_read_uart(Interface_Descriptor* iface, uchar* buffer, uint max_bytes, ulong timeout_us);
int example_close_uart(Interface_Descriptor* iface);
int example_fflush_uart(Interface_Descriptor* iface);
int example_set_baudrate(Interface_Descriptor* iface, ulong baudrate);
int example_set_uart_interrupt(Interface_Descriptor* iface, uchar* flag);

#endif /* USE_EXAMPLE_UART */

#endif //EXAMPLE_UART_H_
