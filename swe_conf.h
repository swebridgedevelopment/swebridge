/*
 ============================================================================
								SWE conf
 ============================================================================
 SWE Bridge configuration file. This file contains the project configuration
 variables to enable or disable functionalities, platform options and debugging
 info comment or set the definitions as explained below.
 ============================================================================
 by SARTI
 Author: Enoc Martinez
 ============================================================================
 */
#ifndef SWE_CONF_H_
#define SWE_CONF_H_



//-----------------------------------------------------------------//
//------------------------ GENERAL INFO ---------------------------//
//-----------------------------------------------------------------//

#define SOFT_VERSION "0.9.5"
#define COMPILE_DATE __DATE__" "__TIME__

#ifndef ON
#define ON 1
#endif

#ifndef OFF
#define OFF 0
#endif

//-----------------------------------------------------------------//
//--------------------- GENERAL OPTIONS ---------------------------//
//-----------------------------------------------------------------//

#include "platform_config.h"

/*-------------------------------------------------------------------*
 * DEFAULT OPTIONS: From this point all the options are encoded      *
 * within #ifdef tags, giving priority to the options declared       *
 * within the platform's configuration                              *
 *-------------------------------------------------------------------*/

/*
 * If SWE Bridge is not in standalone mode, the function swe_bridge_main
 * needs to be called to start the swe bridge
 */
#ifndef SWE_BRIDGE_STANDALONE
#define SWE_BRIDGE_STANDALONE ON
#endif

/*
 *  When turned on it forces to use the file pointed by STATIC_CONFIG_FILE
 *  only turn on when executing in embedded systems without command-line
 *  interface
 */

#ifndef FORCE_CONFIG_FILE
#define FORCE_CONFIG_FILE OFF
#if FORCE_CONFIG_FILE
#define STATIC_CONFIG_FILE "/swe_bridge_setup.conf"
#endif
#endif


/*
 * First relevant argument when launching the project from a command-line
 * */
#ifndef STARTING_ARGUMENT
#define STARTING_ARGUMENT 1
#endif


/*
 * If this flag is set to on, the high frequency components will be compiled. These components include
 * Common: High frequency buffer, block ring buffer
 * Modules: sound pressure level, hydrophone stream, wav generator
 * Resources: FFT
 */
#ifndef ENABLE_HIGH_FREQUENCY_MODULES
#define ENABLE_HIGH_FREQUENCY_MODULES ON
#endif


/*
 * If USE_EXIP_LIBRARY is set to OFF the EXIP library will not be compiled. This
 * will save some RAM memory
 */
#ifndef USE_EXIP_LIBRARY
#define USE_EXIP_LIBRARY ON
#endif

/*
 * Declare a GMT zone to 0 (UTC timestamping)
 */
#ifndef GMT_ZONE
#define GMT_ZONE 0
#endif

/*
 * Output files paths used in InsertResult module
 */
#ifndef TEMP_FOLDER_PATH
#define TEMP_FOLDER_PATH "temp" // define relative path
#endif

#ifndef OUTPUT_FOLDER_PATH
#define OUTPUT_FOLDER_PATH "files" // define relative path
#endif


#ifndef SWE_BRIDGE_TEST_FOLDER
#define SWE_BRIDGE_TEST_FOLDER "SWE_Bridge_test"
#endif

#ifndef PLATFORM_MIN_TICK_RATE
#define PLATFORM_MIN_TICK_RATE 10000 // minimum tick rate 10ms
#endif

#ifndef PLATFORM_MAX_TICK_RATE
#define PLATFORM_MAX_TICK_RATE 500000 // minimum tick rate 500ms
#endif


#define TIMESTAMP_LENGTH 40



//-----------------------------------------------------------------//
//----------------------- DEBUG OPTIONS ---------------------------//
//-----------------------------------------------------------------//

/*
 * If toggled ON a timer drift  in the scheduler timer will be simulated.
 * WARNING: use only for debugging!
 */
#ifndef SIMULATE_SCHEDULER_DRIFT
#define SIMULATE_SCHEDULER_DRIFT OFF
#endif



/*
 * The Simulate scheduler timer instead of creating a timer to schedule the measurements
 * setups a delay. This option is useful for debugging, but it's not recommended for operation!
 */
#ifndef SIMULATE_SCHEDULER_TIMER
#define SIMULATE_SCHEDULER_TIMER OFF
#endif


/*
 * Activate the memory control option. This feature will keep track
 * of every malloc, realloc and free performed by the SWE Bridge,
 * however it will increase the use of static memory significantly
 */
#ifndef MEMORY_CONTROL
#define MEMORY_CONTROL OFF
#endif


/*
 * Enable colored messages into terminal
 */
#ifndef PRINT_WITH_COLOUR
#define PRINT_WITH_COLOUR ON
#endif


/*
 * Additional debugging info. When activated a lot debugging info
 * will be displayed.
 */
#define PRINT_SENSORML OFF
#define PRINT_DECODER_PATH OFF
#define DEBUG_PARSER OFF
#define DEBUG_PARSER_HANDLERS OFF


//-----------------------------------------------------------------//
//------------------- Declare Generic Types -----------------------//
//-----------------------------------------------------------------//

// Define true false //
#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define	TRUE 1
#endif

#ifndef UNSET_BOOLEAN
#define UNSET_BOOLEAN 128
#endif

//-------------Error Code-----------//
enum swe_error_codes {
	OK=0,
	swe_ok=0,
	swe_error=-1,
	swe_filesystem_error=-2,
	swe_iface_error=-3,
	swe_timer_error=-4,
	swe_unimplemented=-5,
	swe_invalid_arguments=-6,
	swe_puck_error=-7,
	swe_signal_error=-8,
	swe_element_not_found=-9,
	swe_scheduler_error=-10,
	swe_wrong_response=-11,
	swe_memory_error=-12,
	swe_warn=-13,
	swe_timeout_error=-14,
	swe_unknown_data_type=-15,
	swe_buffer_overflow=-16,
	swe_empty_buffer=-17
};

#define SWE_ERROR_STRINGS { \
	"swe_ok", \
	"Unknown error", \
	"Filesystem error", \
	"Communications interface error", \
	"Timer error", \
	"Unimplemented", \
	"Invalid arguments", \
	"PUCK protocol error", \
	"UNIX signal error", \
	"Element not found", \
	"Scheduler error", 	\
	"Wrong response", 	\
	"Memory error",		\
	"Warning", 			\
	"Timeout error", 	\
	"Unknown data type",\
	"Buffer overflow",	\
	NULL }

// define swe_boolean //
enum bool_enum {
	e_FALSE = 0,
	e_TRUE  = 1
};

#ifndef boolean
typedef enum bool_enum swe_boolean;
#endif

// Path Separator //
#define PATH_SEPARATOR "/"
#define PATH_SEPARATOR_LENGTH 1


// SWE File open flags //
typedef enum {
	swe_write_file=0,
	swe_read_file,
	swe_append_file
}swe_file_flags;



typedef struct{
	float latitude;
	float longitude;
	float altitude;
	ulong epoch_time; //time where the measure was performed
	uchar valid_data; //flag to used to verify if data is usable or not
}Platform_position;


//-----------------------------------------------------------------//
//------------------GENERIC INTERFACE INFO ------------------------//
//-----------------------------------------------------------------//
/*
 * Structure containing all information related to the configuration
 * of an interface. It contains all possible fields for each structure
 */
typedef struct {
	// UART - RS232 options //
	char *serial_device;
	uint baudrate;
	uchar sw_flot_control;

	// TCP - UDP options //
	uint port;
	char *IP;

	// Generic options
	ulong timeout; // timeout in us
}Interface_Options;


/*
 * Internal Interface Descriptor. Depends on the OS and the Interface type (UDP, TCP or UART)
 */
typedef void Interface_Descriptor;


/*
 * Structure to store date and time
 */
typedef struct {
	int year; // 4-digit
	int month;
	int day;
	int hour;
	int minute;
	int second;
	int useconds; // Microseconds
	int timezone; // GMT zone in hours
}DateTime;

#endif /* SWE_CONF_H_ */
