#!/usr/bin/env python3
# This script shows in stdout the UDP frames received, useful to visualize 
# SWE Bridge notifications

import socket
import sys

port = 37541

if len(sys.argv) > 1 :
    port = int(sys.argv[1])

print("Listening to port", port)
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind(('', port))
while True:
    data = sock.recv(1024)
    print(data.decode())
