#!/usr/bin/env python3
#---------------------------------------------------#
#                  Zabbix Sender                    #  
#---------------------------------------------------#
# This pyhton3 script sends data to a Zabbix sender #
#---------------------------------------------------#

import socket
import json
import socket
from time import sleep, time
import struct
import sys

# Define the ZabbixSender Class #
class ZabbixSender:
    def __init__(self, host='127.0.0.1', port=10051):
        self.address = (host, port)
        self.data    = []

    def __connect(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.sock.connect(self.address)
        except:
            raise Exception("Can't connect server.")
        
    def __close(self):
        self.sock.close()
        
    def __pack(self, request):
        string = json.dumps(request)
        header = struct.pack(b'<4sBQ', b'ZBXD', 1, len(string))
        return header + string.encode()
   
    def __unpack(self, response):
        if len(response) < 13 :
            print("response", response)
            raise Exception("response too small")
    
            
        header, version, length = struct.unpack('<4sBQ', response[:13])
        (data, ) = struct.unpack('<%ds'%length, response[13:13+length])
        return json.loads(data)
   
    def __request(self, request):
        self.__connect()
        try:
            self.sock.sendall(self.__pack(request))
        except Exception as e:
            raise Exception("Failed sending data.\nERROR: %s" % e)
        response = b''
        while True:
            data = self.sock.recv(4096)
            if not data:
                break
            response += data
        self.__close()
        return self.__unpack(response)
    
    def __active_checks(self):
        hosts = set()
        for d in self.data:
            hosts.add(d['host'])
        for h in hosts:
            request = {"request":"active checks", "host":h}
            response = self.__request(request)
    
    def add(self, host, key, value, clock=None):
        if clock is None: clock = int(time())
        self.data.append({"host":host, "key":key, "value":value, "clock":clock})
    
    def send(self):
        if not self.data:
            self.__log("Not found sender data, end without sending.")
            return False
        self.__active_checks()
        request  = {"request":"sender data", "data":self.data}
        response = self.__request(request)
        result   = True if response['response'] == 'success' else False
        if not result:                
            raise Exception("Failed send data.")
        else :
            print("Data sent!")
        return result
    
  
    


# -z IP
# -p port
# -s host
# -k item
# -o value

# Get an option #
def getOption(string, options):
    for i in range(len(options)) :
        if string == options[i] :
            # check that it's not the last element
            if i  == len(options) :             
                raise Exception("Too few elements")
            # check that the next element does not start with '-'
            return options[i+1]
    raise Exception ("element not found")

# Main program #
if __name__ == '__main__':
    args = sys.argv
    
    if len(sys.argv) != 11 :
        print("Expected 11 elements, got ", len(sys.argv))
        print("usage: zabbix_sender -z <IP> -p <port> -s <hostname> -k <field_name> -o <value>")
        exit(0)
    
    IP = getOption('-z', args)
    port = getOption('-p', args)
    hostname = getOption('-s', args)
    item = getOption('-k', args)
    value = getOption('-o', args)
    
    sender = ZabbixSender(IP, int(port))
    sender.add(hostname, item, value)
    sender.send()

    
    
    
    
    
    
