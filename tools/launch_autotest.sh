#!/bin/bash

set -o errexit  # Exits script on error
set -o nounset  # Detects uninitialized variables on the script and exits with error


#-------------------------------------------#
#-------------- Colour codes ---------------#
#-------------------------------------------#
# Colour codes, for internal use only, do not modify
red=$'\e[1;31m'
grn=$'\e[1;32m'
yel=$'\e[1;33m'
blu=$'\e[1;34m'
mag=$'\e[1;35m'
cyn=$'\e[1;36m'
end=$'\e[0m'        


outputFolder="./Debug/files"
execPath="./myTest"

#--------------------------------------#
# Is alive
#--------------------------------------#
isAlive(){
	# argument1: PID
	# argument2: return
	# return value	1 - process is running
	#				0 - proccess is stopped

	local myPID=$1
    local _return=$2
	
	if [ $# != 2 ]; then
	   error "check_status, expected 2 arguments, got [$@]"	
	fi

	if ps -p $myPID > /dev/null  ; then
	    eval ${_return}=1
	else 
	    eval ${_return}=0
	fi
	return 0
}


unexpected_exit(){
    # cleaning up...
    echo "${red}unexpected_exit${end}"
    trap -  TERM EXIT INT
	for p in ${pythons_pids[@]} ; do
		alive=0
		isAlive $p alive
		if [ $alive -gt 0 ]; then 
			echo "killing python $p"
			kill $p	
		else
			echo "python $p not running"
		fi
	done

	for p in ${swe_bridge_pids[@]} ; do
		alive=0
		isAlive $p alive
		if [ $alive -gt 0 ]; then 
			echo "killing SWE Bridge $p"
			kill $p	
		else
			echo "SWE Bridge $p not running"
		fi
	done
	clear_folders

    exit 
}

die(){
    # cleaning up...
    
    # disabling traps
    trap -  TERM EXIT INT
	set +o errexit  # Exits script on error
	set +o nounset  # Detects uninitialized variables on the script and exits with error

	clear_folders
    exit              
}

clear_folders(){


	rm -R $execPath/files
	rm -R $execPath/temp
	rm -R $execPath
}

get_index(){
	# arg1 = array
	# arg2 = element
	# arg3 = response
	local my_array=$1
	local value=$2
	local retval=$3

	for i in "${!my_array[@]}"; do
	   if [[ "${my_array[$i]}" = "${value}" ]]; then
			eval ${retval}=${i}
			return 0
		else 
		echo "nope"
	   fi
	done
	return -1
}

#---------------------------------------------------------------------------#
#------------------------------ PROGRAM START ------------------------------#
#---------------------------------------------------------------------------#
   
set -o errexit  # Exits script on error
set -o nounset  # Detects uninitialized variables on the script and exits with error

execPath=$(pwd)
testPath="${execPath}/AutoTest"

if [ $# != 1 ]; then
	echo "${cyn}== SWE Bridge Autotest =="${end}
	echo "This script launches all tests to ensure that the SWE Bridge functions as expected."
	echo "Please, ensure that AutoTest folder is located at: $testPath"
	echo ""	 
	echo "usage $0 <swe bridge binary>"
	exit 0
fi

swebridge=$1

if [ ! -f $swebridge ]; then
	echo ${red}"ERROR: file $swebridge does not exist!${end}"
	exit 0
fi 


# check if swe bridge path is absolute of relative

if [ ${swebridge:0:1} != "/" ]; then
	swebridge=${PWD}/${swebridge}
fi

trap 'unexpected_exit' TERM EXIT INT # Trap signals to cleanup before exiting

echo "${cyn}#### Launching SWE Bridge AutoTest ####${end}"


cd ..


# Create myTest
execPath=${execPath}"/myTest"
mkdir -p $execPath
cd $execPath
echo "Changing execution path to $(pwd)"




if [ -e $execPath/files ]; then
	echo "Removing old test files"
	rm -f $execPath/files/*.xml
fi

if [ -e $execPath/../temp ]; then
	echo "Removing old test files"
	rm -f $execPath/temp/*.xml
fi


pythons_pids=()
swe_bridge_pids=()
swe_bridge_names=()

#---------- Launch Pythons ----------#
echo "Launching all Pythons..."

for f in ${testPath}/test*.py ; do
	if [ -e $f ] ; then 

		nohup python3 $f > /dev/null 2>&1 &
		newPID=$!
		printf "Launching $(basename $f)  with PID $newPID..."
		alive=0
		isAlive $newPID alive
		pythons_pids+=($newPID)
		echo "${grn}ok${end}"
	fi
done


dateinit=$(date)

#---------- SWE Bridge ----------#
echo "Launching all SWE Bridge..."
for f in ${testPath}/test*.xml ; do
	if [ -e $f ] ; then 
		filename=$(basename $f)
		if [ $filename == "test8.xml" ] ; then
			# use Deep Sleep for test 8
			nohup $swebridge -file $f --deep-sleep 20 > /dev/null 2>&1 &			
		else # Regular launch
			nohup $swebridge -file $f > /dev/null 2>&1 &
			echo "nohup $swebridge -file $f > /dev/null 2>&1 &"
		fi
		newPID=$!
		printf "Launching SWE Bridge with SensorML $(basename $f)  with PID $newPID..."
		alive=0
		isAlive $newPID alive
		swe_bridge_pids+=($newPID)
		swe_bridge_names+=($(basename $f))
		echo "${grn}ok${end}"
	fi
done

k=0
for a in ${swe_brdige_names[@]} ; do
	let "k+=1"
	echo "$k - $a"
done


#----------- Loop -----------#
sleep 2
while : ; do 
	printf "\033c"
	pwd
	echo "Test started at $dateinit"
	echo "         Now is $(date)"
	for p in ${pythons_pids[@]} ; do
		alive=0
		isAlive $p alive
		if [ $alive -gt 0 ]; then 
			echo "python $p is ${grn}alive${end}"
		else
			echo "python $p ${red}not running${end}"
		fi
	done
	i=0

	for p in ${swe_bridge_pids[@]} ; do
		alive=0
		isAlive $p alive
		myName=${swe_bridge_names[$i]}
		printf "SWE Bridge ${swe_bridge_names[$i]} ($p) is..."
		if [ $alive -gt 0 ]; then 
			printf "${blu}running${end}..."
			# Get test num
			testnum=$(basename $myName | cut -d. -f1)"-"
			# echo $testnum
			num_files=$(ls -l "${execPath}/files" 2>/dev/null | grep $testnum | wc -l)
			num_csv=$(ls -l "${execPath}/csv" 2>/dev/null | grep $testnum | wc -l)
			num_wavs=$(ls -l "${execPath}/wavs" 2>/dev/null | grep $testnum | wc -l)
			num=$(( num_files + num_csv + num_wavs))
			
			if [ $num -gt 0 ]; then 
				echo "${grn}$num files$end"
			else 
				echo "${yel}$num files$end"
			fi
	
		else			
			echo "${red}terminated${end}..."
		fi
		

		let "i+=1"
	done

	sleep 2
done	






