#!/bin/bash
#=============================================================================#
#                              Add Platform                                   #
#-----------------------------------------------------------------------------#
# This scripts copies the "example" platform handlers and renames it for      #
# another platform.
#
# Step 1: Copy the "example" platform to another folder with the platform name
#         name.
#
# Step 2: Rename all files from "example_..." to "myplatform_..."
#
# Step 3: Search and replace within all platform files : 
#         "example" -> "myplatform"   "EXAMPLE" -> "MYPLATFORM"
#
# Step 4: Add a define for the platform in swe_conf.h
#
# Step 5: Include platform types (myplatform_conf.h) in swe_conf.h
#
# Step 6: Include platform in resources.c (myplatform.h)
#
#=============================================================================#


swe_bridge_path="/home/enoc/workspace/swe_bridge"
new_path="$swe_bridge_path/resources/platforms/$name"
example_path="$swe_bridge_path/resources/platforms/example_platform"



set -o errexit  # Exits script on error          
set -o nounset  # Detects uninitialized variables on the script and exits with error
   

if [ $# != 1 ]; then
	echo "Usage $0 <new platform>"
	exit 0
fi
name=$1

lc_name=$( echo "$name" | tr '[:upper:]' '[:lower:]' )
uc_name=$( echo "$name" | tr '[:lower:]' '[:upper:]' )


echo "New platform name \"$lc_name\""

new_path="$swe_bridge_path/resources/platforms/$lc_name"

example_path="$swe_bridge_path/resources/platforms/example"

if [ -d $new_path ]; then
	echo "ERROR: path already exists \"$example_path\""
	exit	
fi

# Step 1: Copy the "example" platform to another folder with the platform name
cp -R $example_path $new_path



# Step 2: Rename all files from "example_..." to "myplatform_..."
# renaming files
hfiles=$(ls $new_path/inc)
cfiles=$(ls $new_path/src)

echo "renaming files..."
cd $new_path/inc
for h in $hfiles ; do
	mv $h ${h/example/$lc_name}
done

cd $new_path/src
for c in  $cfiles ; do
	mv $c ${c/example/$lc_name}
done




# Step 3: Search and replace within all platform files : 
#         "example" -> "myplatform"   "EXAMPLE" -> "MYPLATFORM"

echo "seeding files..."
find $new_path -name "*.h" -exec sed -i "s/example/$lc_name/g" {} +
find $new_path -name "*.h" -exec sed -i "s/EXAMPLE/$uc_name/g" {} +
find $new_path -name "*.c" -exec sed -i "s/example/$lc_name/g" {} +
find $new_path -name "*.c" -exec sed -i "s/EXAMPLE/$uc_name/g" {} +



# Step 4: Add a define for the platform in swe_conf.h
echo "adding platform define..."
tag="//--- Define new platform here ---//"
replace_with="//#define $uc_name \r\n$tag"
sed -i "s|${tag}|$replace_with|g" ${swe_bridge_path}/common/inc/swe_conf.h



# Step 5: Include platform types (myplatform_conf.h) in swe_conf.h
tag='//--- Add new platform include here ---//'
replace_with="#elif defined $uc_name \r\n#include \"../platforms/${lc_name}/inc/${lc_name}_conf.h\"\r\n$tag"
sed -i "s|${tag}|$replace_with|g" ${swe_bridge_path}/common/inc/swe_conf.h



# Step 6: Include platform in resources.c (myplatform.h)
tag="//---- Include new platform resources here ----//"
replace_with="#elif defined $uc_name \r\n#include \"../platforms/${lc_name}/inc/${lc_name}.h\"\r\n$tag"
sed -i "s|${tag}|$replace_with|g" ${swe_bridge_path}/resources/src/resources.c
exit

