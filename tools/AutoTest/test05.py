#!/usr/bin/env python3
print("""
-----------------------------------------------
--------------- SWE Bridge Test 5--------------
-----------------------------------------------
"The following feature will be tested:")
    Instrument Command Module
        - Binary encoding
        - Stream
        - DataRecord
    Insert Result
        - XML Encoding
        - redordingTime
        - georeference
        - altitude
        - decimal precision        
    Interface: TCP
-----------------------------------------------""")

import socket
from time import sleep, time
from struct import pack
    
    
IP = '127.0.0.1'
port = 54325



#littleEndian = True
littleEndian = True
arrayCount = 3

a = 0 # uint32
b = 0.0 # float64
c = 0 # byte


# This function generates an string with n floats with a certain precision separated
# tokenSeparator
def generateResponse(iteration):
    
    global a
    global b
    global c
    a  += 1
    b -= 0.01
    c -= 1

	# Avoid overflow
    ctmp = -(-c % 128)
       
    if littleEndian == True :
        endianness = '<'
    else :
        endianness = '>'
    
    print("iteration", iteration)
    binaryResponse = pack(endianness+'I', iteration) #unsigned int
    
    for n in range(0,arrayCount) :
        print(a, b, ctmp)
        binaryResponse += pack(endianness+'I', a) #unsigned int
        binaryResponse += pack(endianness+'d', b) #double
        binaryResponse += pack(endianness+'b', ctmp ) #signed byte
        
    return binaryResponse 
      
        




while True:
# wait for connections
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) # force close when ctrl + C
    server_address = (IP, port)
    print ('starting Test 5 on IP', IP, 'port', port)
    sock.bind(server_address)
    sock.listen(1)
    print("awaiting connection...")                    
    connection, unused = sock.accept()
    print("\nConnection accepted!")
    sock.settimeout(0.1) # set a timeout of 100 ms to check if it is still alive
    totalbytes = 0        
    inittime = int(time()) - 1
    byterate = 0
    sleep(2)
    alive = True    

    socketTimeout = 60
    print("Setting timeout to", socketTimeout, "secs")
    connection.settimeout(socketTimeout)                                            

    iteration = 0    
    while alive == True :
        try :
            data = connection.recv(100)
            if len(data) > 0 :
                print("received", len(data), "bytes:", data)
                if data == b'\x00\x01\x02\x03' :
                    iteration += 1
                    print("Processing command...")
                    reply = generateResponse(iteration)                    
                    

                else :
                    print("ERROR: unknown command", data)
                    reply = "ERROR: Unknown command\r\n".encode()
                
                print("Sending command with size", len(reply))
                connection.sendall(reply)
            else :
                sleep(0.1)
            
            nbytes = len(reply)            
                                                    
        except Exception as e :
            print("Exception", e)
            print("Connection died, cleaning up...")                    
            connection.close() 
            alive = False            
