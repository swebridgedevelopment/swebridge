#!/usr/bin/env python3
print("""
-----------------------------------------------
--------------- SWE Bridge Test 4--------------
-----------------------------------------------
"The following feature will be tested:")
    Instrument Command Module
        - Binary encoding
        - Stream
        - DataRecord
    Insert Result
        - XML Encoding
        - redordingTime
        - georeference
        - altitude
        - decimal precision        
    Interface: TCP
-----------------------------------------------""")

import random
import socket
from time import sleep
from struct import pack
    
    
IP = '127.0.01'
port = 54324
delay = 3 # delay between streams

#littleEndian = True
littleEndian = True

# This function generates an string with n floats with a certain precision separated
# tokenSeparator
def genericResponse(nfields, tokenSeparator, precision):   
    response =  str(float(round(random.uniform(10,100),precision)))
    i = 0
    while i < ( nfields - 1 ) :
        i += 1
        response +=  tokenSeparator +  str(float(round(random.uniform(10,100),precision)))
    return response 



# wait for connections
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

u = 0 # Unsigned Integer 32
li = 0 # long integer (64) 
f = 0.0 # float 32

while True : 
    u += 1
    li -=1
    f += 0.01
    
    if littleEndian == True:
        print("\nEncoding data in little endian")
        # '<' is little-endian 
        binaryResponse = pack('<I', u) #encoding unsigned int
        binaryResponse +=   pack('<q', li) #encoding unsigned int
        binaryResponse += pack('<f', f) #encoding unsigned int
        
    else : 
        print("\nEncoding data in big endian")
        binaryResponse = pack('>I', u) #encoding unsigned int
        binaryResponse +=   pack('>q', li) #encoding unsigned int
        binaryResponse += pack('>f', f) #encoding unsigned int
        
    print("Sending:", u, li, f)
    print("Binary:", binaryResponse)
    
    sock.sendto(binaryResponse, (IP, port))
    sleep(delay)
            
