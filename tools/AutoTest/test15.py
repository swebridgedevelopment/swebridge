#!/usr/bin/env python3
print("""
-----------------------------------------------
--------------- SWE Bridge Test 15-------------
-----------------------------------------------
"The following feature will be tested:")
    Instrument Command Module
        - ASCII encoding
        - Command + reply
        - DataRecord 
    CSV Generator
        - msec Timestamp

    Interface: TCP
-----------------------------------------------""")

import random
import socket
from time import sleep, time
    
    
IP = '127.0.01'
port = 54335

# This function generates an string with n floats with a certain precision separated
# tokenSeparator
def genericResponse(nfields, tokenSeparator, precision):   
    response =  str(float(round(random.uniform(10,100),precision)))
    i = 0
    while i < ( nfields - 1 ) :
        i += 1
        response +=  tokenSeparator +  str(float(round(random.uniform(10,100),precision)))
    return response 



while True:
# wait for connections
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) # force close when ctrl + C
    server_address = (IP, port)
    print ('starting Test 15 on IP', IP, 'port', port)
    sock.bind(server_address)
    sock.listen(1)
    print("awaiting connection...")                    
    connection, unused = sock.accept()
    print("\nConnection accepted!")
    sock.settimeout(0.1) # set a timeout of 100 ms to check if it is still alive
    totalbytes = 0        
    inittime = int(time()) - 1
    byterate = 0    
    alive = True    

    socketTimeout = 120
    print("Setting timeout to", socketTimeout, "secs")
    connection.settimeout(socketTimeout)                                            

    iteration = 0    
    while alive == True :
        try :
            data = connection.recv(100)
            if len(data) > 0 :
                print("received", len(data), "bytes:", data)
                if data == b'TakeSample?\r\n' :
                    iteration += 1
                    print("Processing command...")                    
                    reply = ''
                    reply += genericResponse(3,',', 4) + '\r\n'
                    print("--------------------------------------")                    
                    print(reply)
                    print("--------------------------------------")

                else :
                    print("ERROR: unknown command", data)
                    reply = "ERROR: Unknown command\r\n"
                
                connection.sendall(reply.encode())
            else :
                sleep(0.1)
            
            nbytes = len(reply.encode())            
                                                    
        except Exception as e :
            print("Exception", e)
            print("Connection died, cleaning up...")                    
            connection.close() 
            alive = False
            
