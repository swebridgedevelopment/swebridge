#!/usr/bin/env python3
print("""
-----------------------------------------------
-------------- SWE Bridge Test 13--------------
-----------------------------------------------
"The following features will be tested:")
    - Hydrophone Stream

    Interface: UDP
-----------------------------------------------""")

import random
import socket
from time import sleep, time
import datetime, threading
from numpy import arange, random, int16, zeros, sin, pi, uint8
from random import randint    
    
IP = '127.0.0.1'
port = 54336
delay = 3 # delay between streams

fs = 32000 # Hz
samples_per_packet = 512
tstep = 1 / fs
packetperiod = tstep * samples_per_packet


def simulate_signal(T, fs, frequencies, amplitudes, noise_amplitude=0):
    if len(frequencies) != len(amplitudes) :
        raise ValueError("length of frequencies and ampolitudes should match")

    N = int(T*fs)    
    x = zeros(N)        
    t = arange(0,T, 1/fs) # time array
    psignal = 0
    for i in range(0, len(frequencies)) :        
        x +=  amplitudes[i]*sin(t*frequencies[i]*2*pi)
    
    pnoise = 0
    if noise_amplitude > 0 :
        noise = noise_amplitude * random.normal(0,1,N) 
        x += noise
        pnoise = sum(noise**2) / len(noise)
        
    #print("Signal power", round(psignal, 2), "Noise power", round(pnoise,2), "SNR", round(10*log10(psignal/pnoise),2))
        
    return x


def sendpacket():
  global next_call
  print(datetime.datetime.now())
  next_call = next_call + packetperiod
  threading.Timer( next_call - time.time(), sendpacket ).start()




last_t = 0
packetcount = randint(0, 255) 
def generatepacket(frequencies, amplitudes, noise_amplitude=0.000):
    '''
    This function generates a hydrophone-like packet, ready to be sent. The signal contained
    in the packet is a combination of sine waves and white noise 
    @param frequencies: list of sine frequencies
    @param amplitudes: list of the amplitudes of each frequency
    @param noise_amplitude: noise amplitude to be added
    @returns a hydrophone-like packet
    '''
    global last_t
    global packetcount
    tinit = last_t + tstep
    tend = tinit + tstep * samples_per_packet
    last_t = tend
        
   
    signal = zeros(samples_per_packet)
    timevector = arange(tinit, tend - 1e-9, tstep)

#     if len(timevector) != 512 :
#         print("ERROR: packet has length %d, expected 512" % len(timevector))
#         exit()
    for i in range(0, len(frequencies)) :        
        signal +=  amplitudes[i]*sin(timevector*frequencies[i]*2*pi)
        
    if noise_amplitude > 0 :
        noise = noise_amplitude * random.normal(0,1,samples_per_packet) 
        signal += noise
    
    
    header = bytes(1)
    packetcount = (packetcount + 1) % 256
    packetcountbin = uint8(packetcount)
    
    

    signalbytes = signal.astype('<i2').tobytes() # little endian
    #signalbytes = signal.astype('>i2') # big endian
    
    packet = b''
    packet = packetcount.to_bytes(1, byteorder='big') + header +  signalbytes

    return packet
    
    

print("Sending a packet every %f seconds" % packetperiod)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

totalcount = 0
while True :
    tinit = time()
    tnext = tinit + packetperiod # calculate next packet
    mypacket = generatepacket([440], [100], noise_amplitude=0)
    

    print("%s - send packet %d" % (datetime.datetime.now().strftime('%H:%M:%S.%f') , packetcount))    
    sock.sendto(mypacket, (IP, port))
    totalcount += 512
    
      
    while time() < tnext :
        sleep(1e-6)
    
    


exit()


