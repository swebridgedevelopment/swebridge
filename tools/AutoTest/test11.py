#!/usr/bin/env python3
print("""
-----------------------------------------------
--------------- SWE Bridge Test 1--------------
-----------------------------------------------
"The following feature will be tested:")
    Instrument Command Module
        - ASCII encoding
        - NilValues
        - Stream (DataRecord)
    Insert Result
        - XML Encoding
        - redordingTime
    Interface: UDP
-----------------------------------------------""")

import random
import socket
from time import sleep
    
    
IP = '127.0.01'
port = 54331
delay = 3 # delay between streams

# This function generates an string with n floats with a certain precision separated
# tokenSeparator
def genericResponse(nfields, tokenSeparator, precision):   
    response =  str(float(round(random.uniform(10,100),precision)))
    i = 0
    while i < ( nfields - 1 ) :
        i += 1
        response +=  tokenSeparator +  str(float(round(random.uniform(10,100),precision)))
    return response 



# wait for connections
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
i = 0
while True :
    i += 1
    if ( i % 3) == 0 :
        # Force invalid parameter in field 1
        resp = '-9999,' + genericResponse(4,',', 5) + '\r\n'
    elif ( i % 5 ) == 0 :
        resp = genericResponse(2,',', 2) + ',' + '65355.000,' + genericResponse(2,',', 2) + '\r\n'
    elif ( i % 11 ) == 0 :
        resp = 'n/a,' + genericResponse(4,',', 5) + '\r\n'        
    else :
        resp = genericResponse(5,',', 5) + '\r\n'
    print("sending stream", resp)
    sock.sendto(resp.encode(), (IP, port))
    sleep(delay)
