#!/usr/bin/env python3
print("""
-----------------------------------------------
--------------- SWE Bridge Test 9 -------------
-----------------------------------------------
"The following feature will be tested:")
    Instrument Command Module
        - XML encoding
        - Stream (DataRecord)
    Insert Result
        - XML Encoding
        - redordingTime
    Interface: TCP
-----------------------------------------------""")

import random
import socket
from time import sleep
import sys
    
    
IP = '127.0.0.1'
port = 54329
delay = 3 # delay between streams

def ran(precision):
    return str(float(round(random.uniform(10,100),precision)))

def generateResponse(nfields, tokenSeparator, precision):
    # $WIMDA,30.0261,I,1.0168,B,,,,,,,,,111.7,T,111.5,M,19.8,N,10.2,M*7E   
    response =  "$WIMDA," + ran(4) + ',I,' + ran(4) + ',B,,,,,,,,,'
    i = 0
    while i < ( nfields - 1 ) :
        i += 1
        response +=  tokenSeparator +  str(float(round(random.uniform(10,100),precision)))
    return response 


while True:
# wait for connections
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) # force close when ctrl + C
    server_address = (IP, port)
    print ('starting Test 9 on IP', IP, 'port', port)
    sock.bind(server_address)
    sock.listen(1)
    print("awaiting connection...")                    
    connection, unused = sock.accept()
    print("\nConnection accepted!")
    sock.settimeout(0.1) # set a timeout of 100 ms to check if it is still alive
    totalbytes = 0        
    alive = True    

    socketTimeout = 60
    print("Setting timeout to", socketTimeout, "secs")
    connection.settimeout(socketTimeout)                                            

    iteration = 0    
    while alive == True :
        try :
            SBE54resp='''<Sample Num='3517098' Type='Pressure'>\r\n<Time>2019-02-26T05:02:08</Time>\r\n'''
            SBE54resp += '<PressurePSI>' + str(float(round(random.uniform(10,100),4))) + '</PressurePSI>\r\n'
            SBE54resp += '<PTemp>' +       str(float(round(random.uniform(10,100),4))) + '</PTemp>\r\n'
            SBE54resp += '''<ALERT type='LowBattery' msg='Voltage=11.8'/>\r\n\r\n'''       

            l=len(SBE54resp)    
            bytecount=0
            while bytecount < l :
                sendbytes = int(random.uniform(2,5))
                sendbytes = min(sendbytes, (l-bytecount))
                chunk = SBE54resp[bytecount:bytecount+sendbytes]
                sys.stdout.write(chunk)
                connection.sendall(chunk.encode())
                bytecount += sendbytes
                randdelay = random.uniform(0.001, 0.004)                
                sleep(randdelay)
            sleep(1)
                            
        except Exception as e :
            print("Exception", e)
            print("Connection died, cleaning up...")                    
            connection.close() 
            alive = False
