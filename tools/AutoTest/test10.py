#!/usr/bin/env python3
print("""
-----------------------------------------------
--------------- SWE Bridge Test 10--------------
-----------------------------------------------
"The following feature will be tested:")
    Instrument Command Module
        - ASCII encoding
        - Command + reply
        - DataRecord (Data Record as Data Block)
    Insert Result
        - Generate file each 4 measures
    Interface: TCP
-----------------------------------------------""")

import random
import socket
from time import sleep, time
    
    
IP = '127.0.01'
port = 54330

# This function generates an string with n floats with a certain precision separated
# tokenSeparator
def genericResponse(nfields, tokenSeparator, precision):   
    response =  str(float(round(random.uniform(10,100),precision)))
    i = 0
    while i < ( nfields - 1 ) :
        i += 1
        response +=  tokenSeparator +  str(float(round(random.uniform(10,100),precision)))
    return response 



while True:
# wait for connections
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) # force close when ctrl + C
    server_address = (IP, port)
    print ('starting Test 10 on IP', IP, 'port', port)
    sock.bind(server_address)
    sock.listen(1)
    print("awaiting connection...")                    
    connection, unused = sock.accept()
    print("\nConnection accepted!")
    sock.settimeout(0.1) # set a timeout of 100 ms to check if it is still alive
    totalbytes = 0        
    inittime = int(time()) - 1
    byterate = 0    
    alive = True    

    socketTimeout = 60
    print("Setting timeout to", socketTimeout, "secs")
    connection.settimeout(socketTimeout)                                            

    iteration = 0   
    respCount = 0
    while alive == True :
        try :
            data = connection.recv(100)
            if len(data) > 0 :
                print("received", len(data), "bytes:", data)
                if data == b'TakeSample?\r\n' :
                    iteration += 1
                    respCount += 1
                    print("Processing command...")                    
                    reply = ''
                    reply += genericResponse(2,',', 4) + '\r\n'
                    reply += genericResponse(3,',', 4) + '\r\n'
                    

                else :
                    print("ERROR: unknown command", data)
                    reply = "ERROR: Unknown command\r\n"
                
                if respCount < 7:
                    print("--------------------------------------")                    
                    print(reply)
                    print("--------------------------------------")
                    connection.sendall(reply.encode()) # send first 6 replies
                elif respCount >= 8 :
                    print("Ignoring command to force timeout...")	
                    respCount = 0 # do not send replies 7 and 8
                else : 
                    print("Ignoring command to force timeout...")
            else :
                sleep(0.1)
            
            nbytes = len(reply.encode())            
                                                    
        except Exception as e :
            print("Exception", e)
            print("Connection died, cleaning up...")                    
            connection.close() 
            alive = False
            
