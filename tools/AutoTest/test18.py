#!/usr/bin/env python3
print("""
-----------------------------------------------
--------------- SWE Bridge Test 18--------------
-----------------------------------------------
"The following feature will be tested:")
    Instrument Command Module
        - ASCII encoding
        - Stream (DataRecord)
    JsonUdpSender
    Interface: UDP
-----------------------------------------------""")

import random
import socket
from time import sleep
    
    
IP = '127.0.0.1'
port = 54338
delay = 3 # delay between streams

# This function generates an string with n floats with a certain precision separated
# tokenSeparator
def genericResponse(nfields, tokenSeparator, precision):   
    response =  str(float(round(random.uniform(10,100),precision)))
    i = 0
    while i < ( nfields - 1 ) :
        i += 1
        response +=  tokenSeparator +  str(float(round(random.uniform(10,100),precision)))
    return response 



# wait for connections
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

while True : 
    resp = genericResponse(5,',', 5) + '\r\n'
    print("sending stream", resp)
    sock.sendto(resp.encode(), (IP, port))
    sleep(delay)
