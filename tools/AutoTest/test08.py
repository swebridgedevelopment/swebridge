#!/usr/bin/env python3
print("""
-----------------------------------------------
--------------- SWE Bridge Test 8--------------
-----------------------------------------------
"The following feature will be tested:")
    Instrument Command Module
        - ASCII encoding
        - Stream (DataRecord) with startToken
    Insert Result
        - XML Encoding
        - redordingTime
    Interface: UDP

Simulating NMEA stream:
$WIMDA,30.0261,I,1.0168,B,,,,,,,,,111.7,T,111.5,M,19.8,N,10.2,M*7E   
-----------------------------------------------""")

import random
import socket
from time import sleep
    
    
IP = '127.0.0.1'
port = 54328
delay = 3 # delay between streams

def ran(precision):
    return str(float(round(random.uniform(10,100),precision)))

def generateResponse():
    # $WIMDA,30.0261,I,1.0168,B,,,,,,,,,111.7,T,111.5,M,19.8,N,10.2,M*7E   
    response =  "$WIMDA," + ran(4) + ',I,' + ran(4) + ',B,,,,,,,,,'      
    response+= ran(1) + ',T,' + ran(1) + ',M,'  + ran(1) + ',N,' + ran(1) + ',M*7E\r\n' 
    return response 


# wait for connections
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

while True : 
    resp = generateResponse()
    print("sending stream", resp)
    sock.sendto(resp.encode(), (IP, port))
    sleep(delay)
