# README #

## Sensor Web Enablement Bridge ##

The **SWE Bridge** is a cross-platform universal instrument driver designed to be deployed in any acquisition system. It aims to provide plug and play instrument integration into the [Sensor Web Enablement](http://www.opengeospatial.org/ogc/markets-technologies/swe) framework. Its main features are:
 
* Automatic instrument detection  
* Instrument identification  
* Automatic instrument configuration   
* Measurements operations.  

These operations are performed by automatically retrieving a [SensorML](http://www.opengeospatial.org/standards/sensorml) description file, decoding it, auto-configuring itself and performing measures.

There are a number of different functionalities implemented in the SWE Bridge called modules. Each module implements an independent process that can be executed alone or in a chain. An example of a process chain could be a "instrument command" process that gets some data from the instrument, then it passes its data to the the next process in the chain: "insertResult process", which stores the data into a SOS-compliant InsertResult file. 

## Building as Standalone Application ##
Currently this software can run on Linux platforms and Windows platforms using Cygwin, but other platforms may be used if proper wrappers are provided. This project has been developed using [Eclipse C/C++ IDE](https://www.eclipse.org/downloads/packages/). To build this project: 

1. **New C project** (empty project) in Eclipse C/C++ IDE  

2. **Clone the repository**: go to project directory download/clone the directory:
> $ git init .  
> $ git remote add origin https://bitbucket.org/swebridgedevelopment/swebridge  
> $ git fetch -p  
> $ git checkout master  

3. **Add library paths** in eclipse project: properties -> c/c++ general -> paths and symbols -> includes:  
> /${ProjName}
4. **Link libraries ** Open the Libraries tab in properties -> c/c++ general -> paths and symbols -> libraries and add the follwing libraries to the linker:
> rt  
> m
5. **Select your platform** in "platform_config.h". Additional configuration can also be selected at "swe_conf.h"

6. **Build project**


## Building as a Submodule ##
This project can also be used as part of a larger project, i.e. using it as a git submodule. An example project using the SWE Bridge as a submodule can be found [here](https://bitbucket.org/swebridgedevelopment/swe-bridge-example)

1. **Go to your existing git project**

2. **Add submodule**: go to project a
> $ git add submodule http://bitbucket.org/swebridgedevelopment/swebridge  

3. **Add library paths** in eclipse project: properties -> c/c++ general -> paths and symbols -> includes:  
> /${ProjName}/swebridge
4. **Link libraries ** Open the Libraries tab in properties -> c/c++ general -> paths and symbols -> libraries and add the follwing libraries to the linker:
> rt  
> m
5. **Disable SWE Bridge Standalone mode**: open "swebridge/platform_config.h" an change to OFF the option SWE_BRIDGE_STANDALONE 
6. **Select your platform** in "swebridge/platform_config.h". Additional configuration can also be selected at "swebridge/swe_conf.h"

7. **Build project**


## AutoTest ##
The repository also includes a set of example SensorMLs and sensor simulators to test the functionality of the SWE Bridge. Some SensorMLs are accompanied by a python script that simulates a sensor. Tests that do not have a python script do not require any additional program (i.e. simulating ADC measurments). AutoTests do not check OGC PUCK protocol nor RS232 communications. For Linux systems a bash script to automatically launch all tests simultaneously is also provided at "tools/launch_autotest.sh". An overview of all the tests can be found [here](https://docs.google.com/spreadsheets/d/1-aDeoc9WAT5WiqIlwiEmcHkmawxJhLwWdaigX0E2p1A/edit#gid=1844657313).

![picture](https://bitbucket.org/swebridgedevelopment/swebridge/downloads/AutoTests.png)






