#include "decoder/parser.h"

#include "common/swe_utils.h"
#include "decoder/parser_handlers.h"
#include "resources/mem_control.h"

/****************************************************************************
              STEPS TO INCLUDE MORE HANDLERS TO THE PARSER
 ****************************************************************************
 *
 *   I. Declare a new "block" with a master_path and a block_handler (in order
 *      to keep the code clean and legible is recommended to do it with a
 *      macro at parser.h as in the examples)
 *
 *  II. Declare a new "element list" with the path (in order to keep the code
 *      clean and legible it is recommended to do it with a macro at parser.h
 *      as in the examples)
 *
 * III. Add the new block in the block_array global structure
 *
 *  IV. Initialize the Comparison block in the function init_element_array()
 *
 ****************************************************************************/

uchar compare_partial_paths(Parser_Path_Compare* target, Parser_Path_Compare* inpath);
uchar compare_paths_strict(Parser_Path_Compare* target, Parser_Path_Compare* inpath);
int init_parser_path_compare(Parser_Path_Compare* path, char* inpath);
int get_next_path_chunk_element(Parser_Path_Compare* path);
int restore_inpath(Parser_Path_Compare* inpath);
Parser_element** init_element_array(const Parser_element init_structure[]);
int init_parser(DecoderData* data);
int deinit_parser(DecoderData* data);

int parser_check_elements(DecoderData* data, Comparison_block* block, Parser_Path_Compare* inpath);
int parser_check_element(DecoderData* data,Parser_element* element, Parser_Path_Compare* inpath);
int parser_check_block(DecoderData* data, Comparison_block* block);
int print_element(DecoderData* data);

typedef enum {
	NO_MATCH=0,
	PARTIAL_MATCH,  // The path partially matched (only used in VARIABLE types)
	BLOCK_MATCH,    // The target path (block) ended, but there's still path to compare with elements
	TOTAL_MATCH     // The paths match completely
}compare_return;



compare_return compare_paths(Parser_Path_Compare* target, Parser_Path_Compare* inpath);



Comparison_block instrument_identification_block = INSTRUMENT_IDENTIFICATION_BLOCK;
const Parser_element instrument_identification_elements[]={INSTRUMENT_IDENTIFICATION_ELEMENTS};

//instrument parameters block
Comparison_block instrument_parameters_block=INSTRUMENT_PARAMETERS_BLOCK;
const Parser_element instrument_parameters_elements[]={INSTRUMENT_PARAMETERS_ELEMENTS};

// instrument capability block
Comparison_block instrument_capabilities_block=INSTRUMENT_CAPABILITIES_BLOCK;
const Parser_element instrument_capabilities_elements[]={INSTRUMENT_CAPABILITIES_ELEMENTS};

//instrument simple process description block
Comparison_block instrument_simple_process_block=INSTRUMENT_SIMPLE_PROCESS_BLOCK;
// no elements

Comparison_block mission_simple_process_block=MISSION_SIMPLE_PROCESS_BLOCK;
// no elements

Comparison_block generic_simple_process_block=GENERIC_SIMPLE_PROCESS_BLOCK;
const Parser_element generic_simple_process_elements[]={GENERIC_SIMPLE_PROCESS_ELEMENTS};


Comparison_block mission_block=MISSION_BLOCK;
const Parser_element mission_elements[]={MISSION_IDENTIFIERS_ELEMENTS};


//SWE Bridge configuration blocks
/*Comparison_block bridge_identification_block=BRIDGE_IDENTIFICATION_BLOCK;
const Parser_element bridge_identification_elements[]={BRIDGE_IDENTIFICATION_ELEMENTS};*/

/*Comparison_block bridge_processes_block=BRIDGE_PROCESSES_BLOCK;
const Parser_element bridge_processes_elements[]={BRIDGE_PROCESSES_ELEMENTS};*/

Comparison_block data_processes_block= DATA_STRUCTURE_BLOCK ;
const Parser_element data_processes_elements[]={DATA_STRUCTURE_ELEMENTS};


/**** block comparison structure******/
Comparison_block* block_array[]={
		&instrument_identification_block,
		&instrument_simple_process_block,
		&mission_block,
		&mission_simple_process_block,
		&generic_simple_process_block,
		&data_processes_block,
		&instrument_parameters_block,
		&instrument_capabilities_block,
		/*&bridge_identification_block,
		&bridge_processes_block,*/
		NULL
};



/**************************************************************
 * 		Comparison structures declaration *************************************************************/
int process_element(DecoderData* data){
	int i;
#if PRINT_SENSORML
	print_element(data);
#endif
	for (i=0; i<ARRAY_LENGTH(block_array); i++){
		PDEBUG_INSTRUCTION(dmsgn("target (b): ");cdmsg(CYN, block_array[i]->path);
		dmsgn("inpath    : ");cdmsg(BLU, data->path); );


		TRY_RET(parser_check_block (data, block_array[i]));
	}
	return swe_ok;
}

/**************************************************************
 * 		     Initializes the Parser_Path_compare struct
 *************************************************************/
int init_parser_path_compare(Parser_Path_Compare* path, char* inpath){
	if (path == NULL || inpath == NULL) {
		return swe_invalid_arguments;
	}
	path->fullpath=inpath;
	path->cpath=NULL;
	path->type=0;
	path->clength=0;
	path->end=FALSE;

	return swe_ok;
}

/**********************************************************
 * Takes a parser path strcut as argument and searches for
 * the next element. It also sets the end and type flags
 *
 *********************************************************/
int get_next_path_chunk_element(Parser_Path_Compare* path){
	char* p;
	if (path == NULL) {
		errmsg("path structure is empty");
		return swe_invalid_arguments;
	}

	if (path->cpath == NULL){
		// Initialize
		path->cpath = path->fullpath;

	} else {
		path->cpath += path->clength; // Increase the cpath pointer to the end of previous string
	}

	if(path->cpath[0]==0){
		errmsg("Unexpected path end");
		return swe_invalid_arguments;
	}
	path->clength=0;

	// Check if the next element is variable
	if (path->cpath[0]=='.' || (path->cpath[1]=='.' && path->cpath[0]=='/')){
		path->type=VARIABLE_PATH_ELEMENT;
		if ((path->cpath[1]=='.' && path->cpath[0]=='/')){
			path->cpath++;
		}
		while (path->cpath[0]=='.') { // skip the /../ element
			path->cpath++;
		}
	}else {
		path->type=STATIC_PATH_ELEMENT;
	}

	// Search for the next /.../ tag or the end of the string
	p=path->cpath;
	while ( p[path->clength]!=0 && p[path->clength]!='.'){
		path->clength++;
	}

	// Check if it is the last string
	if (p[path->clength ] == 0){
		path->end = TRUE;
	}

	return swe_ok;
}

/**********************************************************
 *        Shows the target path and the inpath
 *********************************************************/
#if DEBUG_PARSER
int show_paths(Parser_Path_Compare* target, Parser_Path_Compare* inpath){
	const char* truefalse[] = {"FALSE", "TRUE "};
	const char* statvar[] = {"VARIAB", "STATIC"};
	char targetpath[target->clength+1];
	char inpathpath[inpath->clength+1];

	memcpy(targetpath, target->cpath, target->clength);
	targetpath[target->clength]=0;
	memcpy(inpathpath, inpath->cpath, MIN(inpath->clength,target->clength ));
	inpathpath[MIN(inpath->clength,target->clength )]=0;
	dmsgn("target (%s, end=%s) : ", statvar[target->type], truefalse[target->end]);
	cdmsg(CYN, "%s", targetpath);
	dmsgn("inpath (%s, end=%s) : ", statvar[inpath->type], truefalse[inpath->end]);
	cdmsgn(WHT, "%s", inpathpath);
	if(inpath->clength > target->clength){
		int len = inpath->clength - target->clength +1;
		char restinpath[len];
		memset(restinpath, 0, len);
		memcpy(restinpath, &inpath->cpath[target->clength], (inpath->clength - target->clength));
		cdmsg(BLU, "%s", restinpath);

	}else{
		dmsg("");
	}
	return swe_ok;
}
#endif

int skip_path_element(Parser_Path_Compare* inpath, int length){
	char* p;
	inpath->cpath+=length;

	//As this function is only called by inpath it is always static, so end is with 0
	inpath->clength=strlen(inpath->cpath);
	p=inpath->cpath;
	// Check if it's the last element
	p++; //skip first /
	while(!(p[0]=='/' || p[0]==0 )) p++;
	if(p[0]==0){
		inpath->end= TRUE;
	}

	return swe_ok;
}

int go_to_next_element(Parser_Path_Compare* inpath){
	if (inpath->cpath == NULL){
		// Initialize
		inpath->cpath = inpath->fullpath;
		inpath->clength = strlen(inpath->cpath);
		inpath->type=STATIC_PATH_ELEMENT;


		return swe_ok;
	}
	inpath->cpath++; // skip first /
	inpath->clength--;
	while (inpath->cpath[0]!='/' && inpath->cpath[0]!=0){
		inpath->clength--;
		inpath->cpath++;
	}


	return swe_ok;
}

/********************************************************************
 *                       parser_check_block
 *********************************************************************
 *
 * This function checks if the input paths matches with any of the
 * block paths defined by the parser.
 *
 * target -> element that we are looking for (dynamic or static)
 * inpath -> input from the decoder (always static)
 *
 * Steps:
 * I  . Get the next strings and its type (dynamic or static)
 *
 * II . Compare the paths
 *
 * III. Check for match:
 *
 * 	 a) If TOTAL MATCH execute the block handler and exit
 *
 * 	 b) If PARTIAL MATCH and target ended (but not inpath)
 * 	    check its elements and exit
 *
 * IV. If NOT MATCH
 *	 a) If type is STATIC exit current block
 *
 * 	 b) If type is VARIABLE and inpath hasn't ended yet: skip the
 * 	    current element in inpath and go keep comparing (STEP II)
 *
 * 	 c) If target type is VARIABLE and inpath ended, exit block
 *
 *
 *********************************************************************/
#if DEBUG_PARSER
	int blockcount=0;
#endif



int parser_check_block(DecoderData* data, Comparison_block* block) {
	Parser_Path_Compare target;
	Parser_Path_Compare inpath;

	PDEBUG_INSTRUCTION(dmsg("block count %d", blockcount++));

	// Step I  . Preparing paths to compare //
	TRY_RET(init_parser_path_compare(&target, block->path)); // Initialize the parser path objects
	TRY_RET(init_parser_path_compare(&inpath, data->path));
	TRY_RET(get_next_path_chunk_element(&target));
	TRY_RET(go_to_next_element(&inpath));
	while(1) {
		PDEBUG_INSTRUCTION(show_paths(&target, &inpath));

		// Step II . Compare the paths //
		compare_return ret = compare_paths(&target, &inpath);

		// Step III.a If TOTAL MATCH execute the block handler //
		if (ret==TOTAL_MATCH) {
			PDEBUG_INSTRUCTION(cdmsg(GRN, "TOTAL Match, executing handler"));
			if (block->handler!=NULL){

				set_memory_table(MEM_SML); // switch to SML memory slot (only compiled if MEMORY_CONTROL is ON)
				TRY_RET(block->handler(data));
				set_memory_table(MEM_DECODER); // switch back to decoder (only compiled if MEMORY_CONTROL is ON)
				mem_status();

			}
			break;
		}
		// 	 STEP III.b) Check the elements within block //
		else if (ret==PARTIAL_MATCH && target.end == TRUE){
			PDEBUG_INSTRUCTION(cdmsg(GRN, "Block path Match (partial match) , inspecting elements"));
			//skip current element
			skip_path_element(&inpath, target.clength);
			// Check elements
			TRY_RET(parser_check_elements(data, block, &inpath));
			break;
		}
		else if (ret == PARTIAL_MATCH && target.end == FALSE){
			if (inpath.end == FALSE) {
				PDEBUG_INSTRUCTION(cdmsg(GRN,"Partial match, keep looping"));
				//skip current element

				TRY_RET(get_next_path_chunk_element(&target));
				TRY_RET(go_to_next_element(&inpath));
			} else {
				// End of inpath reached
				break;
			}
		}
		//  IV. If NOT MATCH  //
		else {
			// STEP IV.a) If type is STATIC exit current block
			if (target.type == STATIC_PATH_ELEMENT){
				break;
			}
			// STEP IV.b) If type is VARIABLE and inpath hasn't ended yet: skip the current element
			//            in inpath and go back to looping (step II)
			else if (target.type == VARIABLE_PATH_ELEMENT && inpath.end == FALSE){
				go_to_next_element(&inpath);
			}
			// STEP IV.c) If  type is VARIABLE and inpath ended exit block
			else {
				break;

			}
		}
	}
	//getchar();

	return swe_ok;
}

compare_return compare_paths(Parser_Path_Compare* target, Parser_Path_Compare* inpath){
	char *p;
	// Check if the current element is the last in inpath
	p=inpath->cpath;
	// Check if it's the last element
	p++; //skip first /
	while(!(p[0]=='/' || p[0]==0 )) p++;
	if(p[0]==0){
		inpath->end= TRUE;
	}

	// inpath element should at least have the same size taht target element
	if (inpath->clength < target->clength) return NO_MATCH;

	// To have a complete match, the target and inpath must be identical
	// and both must end in the current element
	if (target->clength == inpath->clength &&      // Same length
		!memcmp(target->cpath, inpath->cpath, target->clength) &&  // String are equal
		target->end == TRUE){
		return TOTAL_MATCH;
	}


	// To have a partial match the target current element needs to match with the
	// inpath current element. However, the inpath element must end at the same
	// point as the target, i.e.
	// inpath     "../parameter"
	// target    "../parameters/ParameterList/..." <- Do not match!
	else if(!memcmp(target->cpath, inpath->cpath, target->clength)){
		// Check that the input element ends as expected
		// i.e. "../parameter"
		// +    "../parameters/ParameterList/..." <- Do not match!
		if (target->end == TRUE) {
			char c=inpath->cpath[target->clength];
			// If target ended here, the next char in cpath should be the / preceding the next element
			if (c == '/') return PARTIAL_MATCH;
		}
		else {
			char c=inpath->cpath[target->clength-1];
			// If target ended here, the next char in cpath should be the / preceding the next element
			if (c == '/') return PARTIAL_MATCH;
		}
		return NO_MATCH; // Avoid misaligned elements
	}

	// Otherwise, no match
	return NO_MATCH;
}


int parser_check_elements(DecoderData* data, Comparison_block* block, Parser_Path_Compare* remaining_input_path) {
	int i;
	Parser_element** elements =	block->element;

	// Store current position
	remaining_input_path->save_position=remaining_input_path->cpath;


	for (i=0; i<ARRAY_LENGTH(elements); i++){

		TRY_RET(parser_check_element(data, elements[i], remaining_input_path));
		restore_inpath(remaining_input_path); // restore it to previous point

	}
	return swe_ok;
}




/********************************************************************
 *                       parser_check_block
 *********************************************************************
 *
 * This function checks if the input paths matches with any of the
 * block paths defined by the parser.
 *
 * target -> element that we are looking for (dynamic or static)
 * inpath -> input from the decoder (always static)
 *
 * Steps:
 * I  . Get the next strings and its type (dynamic or static)
 *
 * II . Compare the paths
 *
 * III. Check for match:
 *
 * 	 a) If TOTAL MATCH execute the element handler and exit
 *
 * 	 b) If PARTIAL MATCH and target is VARIABLE look for next
 *
 * 	 c) I
 *
 * IV. If NOT MATCH
 *	 a) If type is STATIC exit current block
 *
 * 	 b) If type is VARIABLE and inpath hasn't ended yet: skip the
 * 	    current element in inpath and go keep comparing (STEP II)
 *
 * 	 c) If target type is VARIABLE and inpath ended, exit block
 *
 *
 *********************************************************************/
#if DEBUG_PARSER
	int elementcount=0;
#endif

int parser_check_element(DecoderData* data,Parser_element* element, Parser_Path_Compare* inpath){
	Parser_Path_Compare target;
	PDEBUG_INSTRUCTION(dmsg("element count %d", elementcount++));

	// Step I  . Preparing paths to compare //
	TRY_RET(init_parser_path_compare(&target, element->path));
	// Initialize the parser path objects
	TRY_RET(get_next_path_chunk_element(&target));
	while(1) {
		PDEBUG_INSTRUCTION(show_paths(&target, inpath));

		// Step II . Compare the paths //
		compare_return ret = compare_paths(&target, inpath);

		// Step III.a If TOTAL MATCH execute the block handler //
		if (ret==TOTAL_MATCH) {
			PDEBUG_INSTRUCTION(cdmsg(GRN, "TOTAL Match, executing handler"));
			if (element->handler!=NULL){
				set_memory_table(MEM_SML); // switch to SML memory slot (only compiled if MEMORY_CONTROL is ON)
				TRY_RET(element->handler(data));
				set_memory_table(MEM_DECODER); // switch back to decoder (only compiled if MEMORY_CONTROL is ON)

			}
			break;
		}
		// 	 STEP III.b) Check the elements within block //
		else if (ret==PARTIAL_MATCH && target.end == FALSE) {
			// keep comparing
			TRY_RET(get_next_path_chunk_element(&target));
			TRY_RET(go_to_next_element(inpath));
		}

		else {
			if(target.type == VARIABLE_PATH_ELEMENT){
				// go to next element and keep comparing until end
				go_to_next_element(inpath);
				if (inpath->clength < target.clength){
					PDEBUG_INSTRUCTION(cdmsg(YEL,"Looping finished, no match"));
					break;
				}
				PDEBUG_INSTRUCTION(dmsg("continue checking..."));

			} else {
				PDEBUG_INSTRUCTION(cdmsg(YEL,"no match"));
				break;
			}
		}
	}
	return swe_ok;
}

/*
 * Restores the path to a certain point to allow to compare it with
 * another element
 */
int restore_inpath(Parser_Path_Compare* inpath){
	char* p;
	inpath->cpath=inpath->save_position;

	//As this function is only called by inpath it is always static, so end is with 0
	inpath->clength=strlen(inpath->cpath);
	p=inpath->cpath;
	// Check if it's the last element
	p++; //skip first /
	while(!(p[0]=='/' || p[0]==0 )) p++;
	if(p[0]==0){
		inpath->end= TRUE;
	}


	return swe_ok;
}



uchar compare_partial_paths(Parser_Path_Compare* target, Parser_Path_Compare* inpath){
	// check if the function is going to compare until the end of the input
	if (target->clength >= inpath->clength){
		inpath->end=TRUE;
	}

	if (!memcmp(target->cpath, inpath->cpath, target->clength)){
		// Check that the input element ends as expected
		// i.e. "../parameter"
		// +    "../parameters/ParameterList/..." <- Do not match!
		char end1=inpath->cpath[target->clength-1];
		char end2=inpath->cpath[target->clength];
		if (end1=='/' || end2=='/' || end2==0 ){
			return TRUE;
		}
		PDEBUG_INSTRUCTION(dmsg("Warning, target and inpath do not have the same ending!"));
	}
	return FALSE;
}

uchar compare_paths_strict(Parser_Path_Compare* target, Parser_Path_Compare* inpath){
	// check if the function is going to compare until the end of the input
	if (target->clength >= inpath->clength){
		inpath->end=TRUE;
	}
	if (target->type==VARIABLE_PATH_ELEMENT) {
		if (!memcmp(target->cpath, inpath->cpath, target->clength)){
			// The get a complete match, if the target ends here, the inpath length should match
			// the target's length
			if (inpath->end == TRUE) {
				if (inpath->clength != target->clength) {
					return FALSE;
				}

			}
			// Check that the input element ends as expected
			// i.e. "../parameter"
			// +    "../parameters/ParameterList/..." <- Do not match!
			char end1=inpath->cpath[target->clength-1];
			char end2=inpath->cpath[target->clength];
			if (end1=='/' || end2=='/' || end2==0 ){
				return TRUE;
			}
			PDEBUG_INSTRUCTION(dmsg("Warning, target and inpath do not have the same ending!"));
		}
	} else { //STATIC_PATH_ELEMENT
		if (target->clength != inpath->clength) {
			return FALSE;
		}
		if (!memcmp(target->cpath, inpath->cpath, target->clength)){
				return TRUE;
		}
	}
	return FALSE;
}


Decoder_Element* element_constructor(){

	Decoder_Element* e=swe_malloc(sizeof(Decoder_Element));

	return e;
}


int element_destructor(Decoder_Element* e){
	int i;

	Decoder_Attribute* attr;
	//erase all attributes
	for(i=0; i < e->nattr; i++){
		attr=e->attr[i];

		swe_free(attr->value);
		swe_free(attr->name);
		swe_free(attr);
	}
	if(e->attr) swe_free(e->attr);

	swe_free(e->name);

	if(e->value!=NULL)swe_free(e->value);
	swe_free(e);

/*	mem_status();

	if (destructor_count == 3) {
		show_memory_table_contents(MEM_DECODER);
		exit(0);
	}*/

	return OK;
}

#define MAX_DOC_DEPTH 25

int print_element(DecoderData* data){
	int i=0;
	char spaces[MAX_DOC_DEPTH];
	memset(spaces, 0 , MAX_DOC_DEPTH);
#if PRINT_DECODER_PATH
	cdmsgn( MAG, "%s (%d)\n", data->path, strlen(data->path));
#endif
	if(data->current_element==NULL){
		memset(spaces, ' ', data->level);
		dmsg("%s (%d)\n", spaces,data->level);
		return 0;
	}
	memset(spaces, 0 , MAX_DOC_DEPTH);
	memset(spaces, ' ', data->level);
	//Print spaces
	if(data->level<10) dmsg("%s (%d) %s", spaces, data->level, data->current_element->name); //print name
	else dmsg("%s(%d) %s", spaces, data->level, data->current_element->name); //print name
	for(i=0; i<data->current_element->nattr; i++){
		Decoder_Attribute* attr=data->current_element->attr[i];
		cdmsgn(BLU, "    %s %s",spaces, attr->name);
		dmsgn( "=\"");
		cdmsgn( GRN, "%s", attr->value);
		dmsgn( "\"\n");
	}
	if(data->current_element->value!=NULL) {
		cdmsgn( MAG, "    %s value", spaces );
		dmsgn( "=\"", spaces);
		cdmsgn( CYN,"%s", data->current_element->value);
		dmsgn( "\"\n");
	}
	return OK;
}



Parser_element** init_element_array(const Parser_element init_structure[]){
	Parser_element** element_array=NULL;
	int n, i;

	for(n=0; init_structure[n].path!=NULL; n++); // Get the array size

	element_array=swe_malloc(sizeof(Parser_element*)*(n+1));;

	//assign the pointers
	for(i=0; i<n; i++) {
		element_array[i]=(Parser_element*)swe_malloc(sizeof(Parser_element));
		element_array[i]->path=init_structure[i].path;
		element_array[i]->handler=init_structure[i].handler;
	}
	return element_array;
}

int init_parser(DecoderData* data){
	//Assign each element array with its block
	mem_status();
	instrument_identification_block.element=init_element_array(instrument_identification_elements);
	generic_simple_process_block.element=init_element_array(generic_simple_process_elements);
	data_processes_block.element=init_element_array(data_processes_elements);
	instrument_parameters_block.element=init_element_array(instrument_parameters_elements);
	instrument_capabilities_block.element=init_element_array(instrument_capabilities_elements);
	mission_block.element=init_element_array(mission_elements);

	//bridge_identification_block.element=init_element_array(bridge_identification_elements);
	//bridge_processes_block.element=init_element_array(bridge_processes_elements);


	if (data->path == NULL) { // allocate space for the path
		data->path=swe_malloc(sizeof(char)*DECODER_MAX_PATH_SIZE); //Initialize the path memory
		memset(data->path, 0, sizeof(char)*DECODER_MAX_DEPTH);
	}
	data->level = 0;

	return swe_ok;
}


int deinit_parser(DecoderData* data) {
	int i, j;

	mem_status();

	swe_free(data->path);
	Comparison_block* blocks[] = {&instrument_identification_block, &generic_simple_process_block,
			&data_processes_block, &instrument_parameters_block, &instrument_capabilities_block, &mission_block, NULL};

	for (i=0 ; blocks[i]!=NULL ; i++ ) {
		if (blocks[i]->element!= NULL) {
			for (j=0 ; blocks[i]->element[j] != NULL ; j++) {
				swe_free(blocks[i]->element[j]);
			}
		}
		swe_free(blocks[i]->element);
	}
	return swe_ok;
}
