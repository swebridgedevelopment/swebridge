/*
 ============================================================================
									decode.c
 ============================================================================
 This program is a modification of the EXIP project which starts the parsing
 and the decoding of an EXI file. The EXI options had been set in the const
 string EXImask. All the information retrieved from the exi file is stored in
 the returned SensorML file.
 ============================================================================
 by SARTI
 Author: Enoc Martínez
 ============================================================================
 */

#include "decoder/decoder.h"
#include "resources/resources.h"
#include "common/swe_utils.h"
#include "decoder/exip/inc/exip_decoder.h"
#include "decoder/sxmlc/sxmlc_decoder.h"
#include "swe_conf.h"



int decode_files(char* file, DecoderData* decoder_data){
	char* extension;
	int i, len;
	system_notify(SWEB_NOTIFY_DECODING_SENSORML, (void*)file);
	len = strlen(file);
	if (len < 5 ) {
		errmsg( "File %s not valid (should contain extension)");
	}

	// search the extension
	for ( i = len-1; file[i] != '.' ; i--) {
		if (i == 0) {
			errmsg( "Extension not found in file %s", file);
			return swe_invalid_arguments;
		}
	}
	extension = file + i;
	if (!compare_strings(".exi", extension)){
#if USE_EXIP_LIBRARY
		set_memory_table(MEM_EXIP);
		TRY_RET(exip_decode_file(file, decoder_data));
		set_memory_table(MEM_MAIN);
#else
		errmsg("EXIP library not compiled! change compilations flags to decode exi files");
		return swe_invalid_arguments;
#endif

	}
	else if (!compare_strings(".xml", extension)){
		set_memory_table(MEM_SXMLC);
		TRY_RET(decode_xml_file(file, decoder_data));
		set_memory_table(MEM_MAIN);
	}
	else{
		errmsg( "Extension not valid");
		return swe_error;
	}

	return swe_ok;
}

