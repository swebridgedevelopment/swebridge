/*
 ============================================================================
									parser.c
 ============================================================================
 This program is a modification of the EXIP project which starts the parsing
 and the decoding of an EXI file. The EXI options had been set in the const
 string EXImask. All the information retrieved from the exi file is stored in
 the returned SensorML file.
 ============================================================================
 by SARTI
 Author: Enoc Martínez
 ============================================================================
 */
#ifndef PARSER_H_
#define PARSER_H_


#include "common/sensorml.h"
#include "swe_conf.h"


typedef struct{
	char*name;
	char*value;
} Decoder_Attribute;

typedef struct {
	char* name; //element name
	char* value; //value, if any
	Decoder_Attribute** attr; //attribute array
	uint nattr; //number of attributes
	uchar attr_flag; //expecting attribute data flag
} Decoder_Element;


typedef struct{
	Decoder_Element* current_element;
	int level;  // depth level of the XML tree
	char* path; // current path
	uchar expecting_field; // flag to determine if new data for a field is expected (i.e. a new Quantity)

	//Bridge_conf* bridge_conf;
	AggregateProcess* mission;
	//Instrument_conf* instrument_conf;
	PhysicalSystem* instrument;
} DecoderData;



//Print
#define OUT_EXI 0
#define OUT_XML 1

#define  PATH_LENGTH_MAX 200

int decode_files(char* file, DecoderData* decoder_data);


#endif


