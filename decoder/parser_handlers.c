/*
 * parser_handlers.c
 *
 *  Created on: Jul 25, 2016
 *      Author: enoc
 */

#include "decoder/parser_handlers.h"

#include "common/sensorml.h"
#include "common/swe_data_model.h"
#include "common/swe_utils.h"
#include "core/simple_process.h"
#include "decoder/decoder.h"



/*
 * Private functions declaration
 */
char* get_attribute(char* attribute_name, Decoder_Element* e);
char* get_value(Decoder_Element* e);
char* get_uid(Decoder_Element* e);
Field* new_swe_data_field_handler(DecoderData* data);


/*
 * Global variables
 */

Field* cdata; //current data field
Field* croot;
SimpleProcess *cprocess; // current process

enum {input_type, output_type, parameter_type, capability_type} input_output_flag;
void *current_input_output; // pointer to the current input, output or parameter

/*
 * Fags that determines if the current element is the Instrument (PhysicalSystem)
 * or the Mission (AggregateProcess)
 */

/*************************************************************************
 * 	 	 	 	 	 	Identification Handlers
 *------------------------------------------------------------------------
 * Parser handlers for the identification detection
 *************************************************************************/


int new_instrument(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	data->instrument=swe_malloc(sizeof(PhysicalSystem));//Initialize the simple process structure
	GET_ATTRIBUTE(data->instrument, id);
	if(data->instrument==NULL) return swe_error;
	return swe_ok;
}


int instrument_uid(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	Decoder_Element* e = data->current_element;
	char* tmp = get_attribute("codeSpace", e);
	if(!strcmp("uniqueID", tmp)){
		data->instrument->uid=get_value(e);
	}
	swe_free(tmp);
	return swe_ok;
}

int instrument_attached_to(DecoderData* data)  {
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	data->instrument->attached_to = swe_malloc(sizeof(AttachedTo));
	GET_ATTRIBUTE(data->instrument->attached_to, href);
	GET_ATTRIBUTE(data->instrument->attached_to, title);
	return swe_ok;
}



/*************************************************************************
 * 	 	 	 	 	 	Physical system - Simple process
 *------------------------------------------------------------------------
 * Parser handlers for the simple process found inside a physical system.
 *
 *************************************************************************/


int new_instrument_process(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	SimpleProcess *newprocess = simple_process_create(data->instrument);



	data->instrument->processes = ATTACH_TO_ARRAY(data->instrument->processes, newprocess);
	GET_ATTRIBUTE(newprocess, name );
	cprocess = newprocess;

	return swe_ok;
}

/*************************************************************************
 * 	 	 	 	 	 	Aggregate Process - Simple process
 *------------------------------------------------------------------------
 * Parser handlers for the simple process found inside an AggregateProcess
 *
 *************************************************************************/

int new_mission(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	data->mission=swe_malloc(sizeof(AggregateProcess));//Initialize the simple process structure
	if(data->mission==NULL) return swe_error;

	GET_ATTRIBUTE(data->mission, id);

	return swe_ok;
}

int mission_uid(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	Decoder_Element* e = data->current_element;
	char* tmp = get_attribute("codeSpace", e);
	if(!strcmp("uid", tmp)){
		data->mission->uid=get_value(e);

	}
	swe_free(tmp);
	return swe_ok;
}

int new_mission_process(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	SimpleProcess *newprocess = swe_malloc(sizeof(SimpleProcess));
	data->mission->processes = ATTACH_TO_ARRAY(data->mission->processes, newprocess);
	GET_ATTRIBUTE(newprocess, name );
	cprocess = newprocess;
	return swe_ok;
}

int mission_connection_source(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	Connection* c = swe_malloc(sizeof(Connection));
	c->source = get_attribute("ref",data->current_element);
	data->mission->connections = ATTACH_TO_ARRAY(data->mission->connections, c);
	return swe_ok;
}

int mission_connection_destination(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	Connection* c = CURRENT_ELEMENT(data->mission->connections);
	c->destination = get_attribute("ref",data->current_element);
	return swe_ok;
}

//------------------------------------------------------------------------//
//----------------------- PhysicalSystem Identifier ----------------------//
//------------------------------------------------------------------------//

/*
 * Creates a new identifier and get the identifier's term definition
 */
int instrument_identifier_term(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	Identifier* identifier = swe_malloc(sizeof(Identifier));
	data->instrument->identifiers = ATTACH_TO_ARRAY(data->instrument->identifiers, identifier);
	identifier->definition = get_attribute("definition", data->current_element);
	return swe_ok;
}

/*
 * Get the identifier label
 */
int instrument_identifier_term_label(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	Identifier* identifier = CURRENT_ELEMENT(data->instrument->identifiers);
	identifier->label = get_value(data->current_element);
	return swe_ok;
}

/*
 * Get the identifier value
 */
int instrument_identifier_term_value(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	Identifier* identifier = CURRENT_ELEMENT(data->instrument->identifiers);
	identifier->value = get_value(data->current_element);
	return swe_ok;
}



//------------------------------------------------------------------------//
//----------------------- PhysicalSystem Classifier ----------------------//
//------------------------------------------------------------------------//

/*
 * Creates a new identifier and get the identifier's term definition
 */
int instrument_classifier_term(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	Classifier* classifier = swe_malloc(sizeof(Classifier));
	data->instrument->classifiers = ATTACH_TO_ARRAY(data->instrument->classifiers, classifier);
	classifier->definition = get_attribute("definition", data->current_element);
	return swe_ok;
}

/*
 * Get the classifier label
 */
int instrument_classifier_term_label(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	Classifier* classifier = CURRENT_ELEMENT(data->instrument->classifiers);
	classifier->label = get_value(data->current_element);
	return swe_ok;
}

/*
 * Get the classifier value
 */
int instrument_classifier_term_value(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	Classifier* classifier = CURRENT_ELEMENT(data->instrument->classifiers);
	classifier->value = get_value(data->current_element);
	return swe_ok;
}


//---------------------------------------------------------------------------//
//-------------------------- Instrument Position ----------------------------//
//---------------------------------------------------------------------------//

/*
 * Flag used to determine th identifier value:
 */
enum position_type   {
	position_none = 0,
	position_latitude,
	position_longitude,
	position_depth
};

static uchar expecting_position = position_none;
static const char* latitude_strings[] = SML_LATITUDE_AXIS_ID_LIST;
static const char* longitude_strings[] = SML_LONGITUDE_AXIS_ID_LIST;
static const char* depths_strings[] = SML_DEPTH_AXIS_ID_LIST;

/*
 * Allocate space for position
 */
int instrument_position(DecoderData* data){
	data->instrument->position = swe_malloc(sizeof(Position));
	return swe_ok;
}

/*
 * Get the axis currently processed and set the expecting_position flag
 */
int instrument_position_axis(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	char* axisID = get_attribute("axisID", data->current_element);
	if (string_in_list(axisID, latitude_strings) == TRUE) {
		expecting_position = position_latitude;
	}
	else if (string_in_list(axisID, longitude_strings) == TRUE) {
		expecting_position = position_longitude;
	}
	else if (string_in_list(axisID, depths_strings)) {
		expecting_position = position_depth;
	}
	swe_free(axisID);
	return swe_ok;
}


/*
 * Get the Position Value
 */
int instrument_position_value(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	float value = strtof(data->current_element->value, NULL);
	dmsg("Value %f", value);
	if (expecting_position == position_latitude) {
		data->instrument->position->latitude = value;
	}
	else if (expecting_position == position_longitude) {
		data->instrument->position->longitude = value;
	}
	else if (expecting_position == position_depth) {
		data->instrument->position->depth = value;
	}
	expecting_position = position_none;
	return swe_ok;
}

//---------------------------------------------------------------------------//
//------------------------ Instrument Parameters ----------------------------//
//---------------------------------------------------------------------------//
/*
 * Create a new parameter field
 */
int instrument_parameter(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	Parameter* parameter;
	Field* field;

	parameter = swe_malloc(sizeof(Parameter));
	GET_ATTRIBUTE(parameter, name);
	GET_ATTRIBUTE(parameter, title);
	GET_ATTRIBUTE(parameter, href);

	// Generating new field //
	field = new_swe_data_field(data->level, NULL);
	data->expecting_field = TRUE;
	cdata = field;

	// Assign the field to the parameter //
	parameter->field = field;
	data->instrument->parameters = ATTACH_TO_ARRAY(data->instrument->parameters, parameter);
	current_input_output = parameter;

	input_output_flag = parameter_type;
	return swe_ok;
}

/*
 * Create a new data interface
 */
int instrument_parameter_data_interface(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	data->instrument->interface_conf = swe_malloc(sizeof(DataInterface));
	GET_ATTRIBUTE(data->instrument->interface_conf, id);
	return swe_ok;
}


/*
 * Create a new field within the current data interface
 */
int instrument_parameter_data_interface_field(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	DataInterface* iface = data->instrument->interface_conf;
	Field* newfield;
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	newfield = new_swe_data_field(data->level, NULL);
	GET_ATTRIBUTE(newfield, name);
	GET_ATTRIBUTE(newfield, title);
	GET_ATTRIBUTE(newfield, href);
	dmsg("Generating new iface field %s ", newfield->name);
	iface->fields=ATTACH_TO_ARRAY(iface->fields, newfield);
	dmsg("iface->fields pointer now has %d elements", ARRAY_LENGTH(iface->fields));
	cdata = newfield;
	data->expecting_field = TRUE;
	return swe_ok;
}


//---------------------------------------------------------------------------//
//------------------------ Instrument Capability ----------------------------//
//---------------------------------------------------------------------------//
/*
 * Create a new parameter field
 */
int instrument_capability(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	Capability* capability;
	Field* field;

	capability = swe_malloc(sizeof(Capability));
	GET_ATTRIBUTE(capability, name);
	GET_ATTRIBUTE(capability, title);
	GET_ATTRIBUTE(capability, href);

	// Generating new field //
	field = new_swe_data_field(data->level, NULL);
	data->expecting_field = TRUE;
	cdata = field;

	// Assign the field to the parameter //
	capability->field = field;
	data->instrument->capabilities = ATTACH_TO_ARRAY(data->instrument->capabilities, capability);
	current_input_output = capability;

	input_output_flag = capability_type;
	return swe_ok;
}



//---------------------------------------------------------------------------//
//---------------------- SimpleProcess Parameters ---------------------------//
//---------------------------------------------------------------------------//
/*
 * Create a new parameter field
 */
int instrument_process_parameter(DecoderData* data){
	Parameter* parameter;
	Field* field;

	parameter = swe_malloc(sizeof(Parameter));
	GET_ATTRIBUTE(parameter, name);
	GET_ATTRIBUTE(parameter, title);
	GET_ATTRIBUTE(parameter, href);

	// Generating new field //
	field = new_swe_data_field(data->level, NULL);
	data->expecting_field = TRUE;
	cdata = field;

	// Assign the field to the parameter //
	parameter->field = field;
	cprocess->parameters = ATTACH_TO_ARRAY(cprocess->parameters, parameter);
	current_input_output = parameter;

	// attach the new field to the field_list array //
	cprocess->field_list = ATTACH_TO_ARRAY(cprocess->field_list, field);
	input_output_flag = parameter_type;
	return swe_ok;
}


//---------------------------------------------------------------------------//
//-------------------------- Feature Of Interest ----------------------------//
//---------------------------------------------------------------------------//
int instrument_feature_of_interest(DecoderData* data){
	data->instrument->feature_of_interest_href = get_attribute("href", data->current_element);
	data->instrument->feature_of_interest_title = get_attribute("title", data->current_element);
	return swe_ok;
}

/*************************************************************************
 * 	 	 	 	 	 Generic Process Handlers
 *------------------------------------------------------------------------
 * Parser handlers for the si,ple process
 *
 *************************************************************************/

int instrument_process_identifier(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	Decoder_Element* e = data->current_element;
	char* codespace = get_attribute("codeSpace", e);

	if(!compare_strings("uid", codespace )){
		cprocess->uid=set_string(e->value);
	}
	swe_free(codespace);
	return swe_ok;
}


int instrument_process_typeof(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	Decoder_Element* e = data->current_element;
	cprocess->typeOf=get_attribute("title", e);
	return swe_ok;
}


/*
 * Create a new input in current process
 */
int instrument_process_input(DecoderData* data){
	Input* input;

	input = swe_malloc(sizeof(Input));
	GET_ATTRIBUTE(input, name);
	GET_ATTRIBUTE(input, title);
	GET_ATTRIBUTE(input, href);


	// assign new input to current process
	cprocess->inputs = ATTACH_TO_ARRAY(cprocess->inputs, input);
	current_input_output = input;
	input_output_flag = input_type;
	return swe_ok;
}

/*
 * Create a new output in current process
 */
int instrument_process_output(DecoderData* data){
	Output* output;

	output = swe_malloc(sizeof(Output));
	GET_ATTRIBUTE(output, name);
	GET_ATTRIBUTE(output, title);
	GET_ATTRIBUTE(output, href);


	// assign new input to current process
	cprocess->outputs = ATTACH_TO_ARRAY(cprocess->outputs, output);
	current_input_output = output;
	input_output_flag = output_type;
	return swe_ok;
}


int simple_process_settings_setvalue (DecoderData* data){
	Setting* setting = swe_malloc(sizeof(Setting));

	GET_ATTRIBUTE(setting, ref);
	setting->value = get_value(data->current_element);
	cprocess->settings = ATTACH_TO_ARRAY(cprocess->settings, setting);
	return swe_ok;
}



/****************************************************************************************
 *                           Instrument Interface Parameters
 ***************************************************************************************/
int instrument_new_interface(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	data->instrument->interface_conf = swe_malloc(sizeof(DataInterface));
	GET_ATTRIBUTE(data->instrument->interface_conf, id);


	return swe_ok;
}

int instrument_new_interface_field(DecoderData* data){
	DataInterface* iface = data->instrument->interface_conf;
	Field* newfield;
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));


	newfield = new_swe_data_field(data->level, NULL);

	GET_ATTRIBUTE(newfield, name);
	GET_ATTRIBUTE(newfield, title);
	GET_ATTRIBUTE(newfield, href);
	dmsg("Generating new iface field %s ", newfield->name);
	iface->fields=ATTACH_TO_ARRAY(iface->fields, newfield);
	dmsg("iface->fields pointer now has %d elements", ARRAY_LENGTH(iface->fields));

	cdata = newfield;

	data->expecting_field = TRUE;

	return swe_ok;
}





//---------------------------------------------------------------------------//
//--------------------- SWE Data Common Data Structures ---------------------//
//---------------------------------------------------------------------------//

/*
 * Generates a new DataStream structure to store more fields.
 * Triggered when a new DataStream is detected.
 *
 * Attributes: "id"
 *
 */
int new_data_stream (DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	Field* newfield; // Root field used to host all the others
	DataStream* ds;
	SimpleProcess* sp;

	newfield = new_swe_data_field(data->level, NULL);
	data->expecting_field = TRUE;
	cdata = newfield;
	croot = newfield;
	field_component_type_set(newfield, SWE_DataStream);
	ds=(DataStream*)newfield->component;

	// Get DataStream Attributes //
	GET_ATTRIBUTE(ds, id);

	// Attach new field to the element list
	sp = CURRENT_ELEMENT(data->instrument->processes);
	sp->field_list = ATTACH_TO_ARRAY(sp->field_list, newfield);

	// Assign the new DataStream to the last input / output
	if (input_output_flag == input_type){ // If the current element is input,
		Input* input = (Input*) current_input_output;
		input->field = newfield;
		// input->data = root; // assign the root field
	} else if( input_output_flag == output_type) { // otherwise if it is output
		Output* output = (Output*) current_input_output;
		output->field = newfield; // assign the root field
	} else  {
		errmsg("DataStream in a parameter not allowed!");
	}

	PHDEBUG_INSTRUCTION(dmsg("Assigning new DataStream to pointer %p", current_input_output));
	return swe_ok;
}


/*
 * Gets Info associated with a DataStream elementType and stores it in the DataStream root field.
 * It generates a new field to store the next element (Basic or complex)
 *
 * Attributes: "name" "title" "href"
 */
int data_stream_element_type (DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	Field *newfield;
	SimpleProcess* sp;

	GET_ATTRIBUTE(cdata, name);
	GET_ATTRIBUTE(cdata, title);
	GET_ATTRIBUTE(cdata, href);

	// prepare the new field //
	newfield=new_swe_data_field(data->level, croot);
	data->expecting_field = TRUE;
	// TODO memory leak here
	attach_swe_field_to_parent(newfield, croot);

	// Attach new element to the element list
	sp = CURRENT_ELEMENT(data->instrument->processes);
	sp->field_list = ATTACH_TO_ARRAY(sp->field_list, newfield);

	cdata = newfield; // update current data pointer
	return swe_ok;
}


/*
 * A new data structure is found. Initialize the Data Structures:
 * (data, datastructures and dataconfig)
 */
int data_stream_field (DecoderData* data){
	SimpleProcess *sp;
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	// Get the appropriate SWE Data Field to contain new data (its parent) //
	Field* parent = get_swe_data_parent(cdata, data->level);
	// Allocate space for a new SWE Data Field //
	Field* newfield=new_swe_data_field(data->level, parent);
	data->expecting_field = TRUE;
	// TODO memory leak here
	attach_swe_field_to_parent(newfield, parent);

	// Attach new field to the element list
	sp = CURRENT_ELEMENT(data->instrument->processes);
	sp->field_list = ATTACH_TO_ARRAY(sp->field_list, newfield);


	// Set Options to the new field //
	GET_ATTRIBUTE(newfield, name);
	GET_ATTRIBUTE(newfield, title);
	GET_ATTRIBUTE(newfield, href);
	// updating cdata
	cdata = newfield;
	return swe_ok;
}


/*
 * Generates a new DataRecord
 *
 */
int data_stream_new_data_record(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	// We assume that the correct field is in cdata //
	field_component_type_set(cdata, SWE_DataRecord);
	DataRecord *dr = (DataRecord*)cdata->component;

	// Get the attributes //
	GET_ATTRIBUTE(dr, id);
	GET_ATTRIBUTE(dr, definition);
	return swe_ok;
}



/*****************************************************************
 *                  Data Array Elements
 ****************************************************************/

/*
 * Generates a new DataArray
 */
int data_stream_new_data_array(DecoderData* data){
	Field *newfield, *parent;
	DataArray *da;
	SimpleProcess* sp;

	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));

	// Get the attributes //
	field_component_type_set(cdata, SWE_DataArray);
	da = (DataArray*)cdata->component;
	// Get attributes //
	GET_ATTRIBUTE(da, id);
	GET_ATTRIBUTE(da, definition);
	parent = cdata; // the current field is the parent
	newfield=new_swe_data_field(data->level, parent);
	data->expecting_field = TRUE;

	// Attach new field to the element list
	sp = CURRENT_ELEMENT(data->instrument->processes);
	sp->field_list = ATTACH_TO_ARRAY(sp->field_list, newfield);

	// Attaching new field to parent //
	// TODO memory leak here
	TRY_RET(attach_swe_field_to_parent(newfield, parent));
	cdata = newfield; // updadate current data pointer
	return swe_ok;
}

int data_stream_data_array_element_type(DecoderData* data){

	// Get attributes //
	GET_ATTRIBUTE(cdata, name);
	GET_ATTRIBUTE(cdata, title);
	GET_ATTRIBUTE(cdata, href);

	return swe_ok;
}

/*
 * Data Array Element Count
 */
int data_stream_data_array_element_count(DecoderData* data){
	Field* arrayfield;
	DataArray *da;
	SimpleProcess *sp;

	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	arrayfield = find_last_swe_field(cdata, SWE_DataArray);
	da  = (DataArray*) arrayfield->component;

	da->arrayCount = new_swe_data_field(data->level, arrayfield);
	data->expecting_field = TRUE;
	field_component_type_set(da->arrayCount, SWE_Count);

	// Attach new field to the element list
	sp = CURRENT_ELEMENT(data->instrument->processes);
	sp->field_list = ATTACH_TO_ARRAY(sp->field_list, da->arrayCount);

	GET_ATTRIBUTE(da->arrayCount, name);
	GET_ATTRIBUTE(da->arrayCount, title);
	GET_ATTRIBUTE(da->arrayCount, href);
	return swe_ok;
}


/*
 * Data Array Element Count Value
 */
int data_stream_data_array_element_count_value(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	Field* arrayfield;
	DataArray *da;
	Count* count;
	// get data array //
	arrayfield = find_last_swe_field(cdata, SWE_DataArray);
	da  = (DataArray*) arrayfield->component;
	count = da->arrayCount->component;
	count->value = atoi(get_value(data->current_element));;
	return swe_ok;
}

/*
 * Data Array Encoding
 */
int data_stream_array_encoding(DecoderData* data){
	// Navigate backwards until the data array is found
	Field* arrayfield;
	DataArray* da;
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	// Check for the last element that matches data level
	cdata = find_last_swe_field(cdata, SWE_DataArray);
	arrayfield = get_swe_data_parent(cdata, data->level);

	if (arrayfield->type != SWE_DataArray) {
		errmsg("Expected DataArray at level %d", data->level);
		return swe_error;
	}
	da = (DataArray*)arrayfield->component;
	da->encoding = swe_malloc(sizeof(Encoding));
	cdata = arrayfield;
	return swe_ok;
}



//----- Quantity ----//
int generic_field_quantity (DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));

	// If we are not expecting a field value, exit
	if (data->expecting_field == FALSE ){
		return swe_ok;
	}
	// We assume that the correct field is in cdata //
	field_component_type_set(cdata, SWE_Quantity);
	Quantity* component= (Quantity*)cdata->component;
	// Get the attributes //
	GET_ATTRIBUTE(component, definition);
	GET_ATTRIBUTE(component, id);
	data->expecting_field = FALSE;
	return swe_ok;
}

//----- Count ----//
int generic_field_count (DecoderData* data){
	if (data->expecting_field == FALSE ) return swe_ok;
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	field_component_type_set(cdata, SWE_Count);
	Count* component=(Count*)cdata->component;
	GET_ATTRIBUTE(component, definition);
	GET_ATTRIBUTE(component, id);
	data->expecting_field = FALSE;
	return swe_ok;
}

//----- Text ----//
int generic_field_text (DecoderData* data){
	if (data->expecting_field == FALSE ) return swe_ok;
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	field_component_type_set(cdata, SWE_Text);
	Text* component=(Text*)cdata->component;
	GET_ATTRIBUTE(component, definition);
	GET_ATTRIBUTE(component, id);
	data->expecting_field = FALSE;
	return swe_ok;
}

//---- Boolean ----//
int generic_field_boolean (DecoderData* data){
	if (data->expecting_field == FALSE ) return swe_ok;
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	field_component_type_set(cdata, SWE_Boolean);
	Boolean* component=(Boolean*)cdata->component;
	GET_ATTRIBUTE(component, definition);
	GET_ATTRIBUTE(component, id);
	data->expecting_field = FALSE;
	return swe_ok;
}

//---- Time ----//
int generic_field_time (DecoderData* data){
	if (data->expecting_field == FALSE ) return swe_ok;
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	field_component_type_set(cdata, SWE_Time);
	Time* component=(Time*)cdata->component;
	GET_ATTRIBUTE(component, definition);
	GET_ATTRIBUTE(component, id);
	data->expecting_field = FALSE;
	return swe_ok;
}


/*
 * The Category is used to choose among a pre-defined set of options.
 * As the parser, does not know beforehand what are those options,
 * it is treated simply as a Text field
 */
int generic_field_category (DecoderData* data){
	if (data->expecting_field == FALSE ) return swe_ok;
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	field_component_type_set(cdata, SWE_Category);
	Category* component=(Category*)cdata->component;
	GET_ATTRIBUTE(component, definition);
	GET_ATTRIBUTE(component, id);
	data->expecting_field = FALSE;
	return swe_ok;
}

/*
 * Get the value of a SWE Component Type
 */
int generic_field_value (DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	swe_component_type type = cdata->type;


	if (data->current_element->value == NULL ) {
		warnmsg("Expected some value, but received NULL");
		return swe_ok;
	}
	if (check_if_complex(type)) {
		errmsg("Unexpected complex field");
		return swe_error;
	}
	TRY_RET(field_component_value_set_static(cdata, data->current_element->value));
	return swe_ok;
}

/*
 * Get label
 */
int generic_field_label (DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	swe_component_type type = cdata->type;
	if (!check_if_basic(type)) {
		return swe_ok;
	}
	field_component_label_set(cdata, data->current_element->value);
	return swe_ok;
}

/*
 * get units of measurement
 */
int generic_field_uom (DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	if (cdata->type != SWE_Quantity || cdata->type != SWE_Time ) {
		warnmsg("uom should only be present in SWE_Quantity and SWE_Time!, ignoring uom");
		return swe_ok;
	}
	Quantity *q = (Quantity*)cdata->component; // current type should be a quantity
	q->uom = get_attribute("code", data->current_element);
	return swe_ok;
}

//---- Label ----//
int data_stream_field_label (DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	TRY_RET(field_component_label_set(cdata, data->current_element->value));
	return swe_ok;
}

//---- NilValue ----//
int data_stream_field_nilvalue (DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	TRY_RET(field_component_nilvalue_set(cdata, data->current_element->value));
	return swe_ok;
}



/*****************************************************************
 *                  Data Stream Elements
 ****************************************************************/
/*
 * Encoding of the DataStream Root element. Moves the cdata pointer
 * back to croot
 */
int data_stream_encoding_new(DecoderData* data){
	DataStream *ds;
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));


	if (croot == NULL) {
		return swe_error;
	}
	ds = (DataStream*)croot->component;
	ds->encoding = swe_malloc(sizeof(Encoding));
	ds->encoding->type = SWE_UnknownEncoding; // By default assign default encoding
	cdata = croot;
	return swe_ok;
}


/*****************************************************************
 *                      Encodings
 ****************************************************************/
int data_stream_text_encoding(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	TextEncoding *te;
	char* collapse;
	// Allocate Text Encoding
	te = swe_malloc(sizeof(TextEncoding));
	GET_ATTRIBUTE(te, blockSeparator);
	GET_ATTRIBUTE(te, tokenSeparator);
	collapse = get_attribute("collapseWhiteSpaces", data->current_element);
	if (collapse != NULL ) {
		te->collapseWhiteSpaces = get_true_false(collapse, NULL);
	} else {
		te->collapseWhiteSpaces = TRUE; // set to true by default
	}

	swe_free(collapse);

	// check collapse white spaces

	if ( te->blockSeparator != NULL ) {
		te->blockSeparator=convert_xml_special_chars(te->blockSeparator);
	}
	if ( te->blockSeparator != NULL ) {
		te->tokenSeparator=convert_xml_special_chars(te->tokenSeparator);
	}






	// If current data is DataArray //
	if (cdata->type == SWE_DataArray ) {
		DataArray *da = (DataArray*)cdata->component;
		da->encoding->type = SWE_TextEncoding; // By default assign default encoding
		da->encoding->encoding = te;
	}
	// If current data is DataStream //
	else if (cdata->type == SWE_DataStream) {

		DataStream *ds = (DataStream*)cdata->component;
		ds->encoding->type = SWE_TextEncoding; // By default assign default encoding
		ds->encoding->encoding = te;
	}
	return swe_ok;
}

int data_stream_text_encoding_start_token(DecoderData* data ){
	TextEncoding *te;

	// If current data is DataArray //
	if (cdata->type == SWE_DataArray ) {
		DataArray *da = (DataArray*)cdata->component;
		te = (TextEncoding*)da->encoding->encoding;

	}
	// If current data is DataStream //
	else if (cdata->type == SWE_DataStream) {

		DataStream *ds = (DataStream*)cdata->component;
		te = (TextEncoding*)ds->encoding->encoding;
	}
	te->startToken = get_value(data->current_element);
	return swe_ok;
}


/*
 * Binary Encoding
 */

int data_stream_binary_encoding(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));
	char* byteOrder, *byteEncoding;

	BinaryEncoding *be = swe_malloc(sizeof(BinaryEncoding));

	// Getting Byte Order //
	byteOrder = get_attribute("byteOrder", data->current_element);
	be->byteOrder = get_byte_order(byteOrder);
	dmsg("Byte Order %d", be->byteOrder);

	swe_free(byteOrder);

	byteEncoding = get_attribute("byteEncoding", data->current_element);
	be->byteEncoding = get_byte_encoding(byteEncoding);
	swe_free(byteOrder);

	GET_ATTRIBUTE(be, id);

	// If current data is DataArray //
	if (cdata->type == SWE_DataArray ) {
		DataArray *da = (DataArray*)cdata->component;
		da->encoding->type = SWE_BinaryEncoding; // By default assign default encoding
		da->encoding->encoding = be;
	}
	// If current data is DataStream //
	else if (cdata->type == SWE_DataStream) {
		DataStream *ds = (DataStream*)cdata->component;
		ds->encoding->type = SWE_BinaryEncoding; // By default assign default encoding
		ds->encoding->encoding = be;
	}
	return swe_ok;
}

int data_stream_binary_encoding_component(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));

	BinaryEncoding *be;
	BinaryEncodingComponent* c=swe_malloc(sizeof(BinaryEncodingComponent));
	char *dataType, *byteLength, *bitLength;
	// Get char attributes //
	GET_ATTRIBUTE(c, ref);
	GET_ATTRIBUTE(c, id);

	// Process the ref and split it in path //

	c->path = set_substrings(c->ref, "/");

		// get dataType //
	dataType = get_attribute("dataType", data->current_element);
	c->type = get_data_type(dataType);
	if (c->type == unknown_data){
		errmsg("Unknown data type %s", dataType);
		return swe_invalid_arguments;
	}

	swe_free(dataType);

	// get byteLength //
	byteLength = get_attribute("byteLength", data->current_element);
	if (byteLength != NULL) {
		c->byteLength = atoi(byteLength);
	} else {
		c->byteLength = 0;
	}
	swe_free(byteLength);

	// get bitLength //
	if ((bitLength = get_attribute("bitLength", data->current_element)) != NULL) {
		c->bitLength = atoi(bitLength);
	} else {
		c->bitLength = 0;
	}
	swe_free(bitLength);

	// If current data is DataArray //
	if (cdata->type == SWE_DataArray ) {
		DataArray *da = (DataArray*)cdata->component;
		be = da->encoding->encoding;
		da->encoding->type = SWE_BinaryEncoding; // By default assign default encoding
	}

	// If current data is DataStream //
	else if (cdata->type == SWE_DataStream) {
		DataStream *ds = (DataStream*)cdata->component;
		be = ds->encoding->encoding;
		ds->encoding->type = SWE_BinaryEncoding; // By default assign default encoding
		ds->encoding->encoding = be;
	}
	ATTACH_TO_ARRAY(be->components, c);

	return swe_ok;
}

/*
 * XML encoding
 */
int data_stream_xml_encoding(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));

	XMLEncoding *xe = swe_malloc(sizeof(XMLEncoding));

	// If current data is DataArray //
	if (cdata->type == SWE_DataArray ) {
		DataArray *da = (DataArray*)cdata->component;
		da->encoding->type = SWE_XMLEncoding; // By default assign default encoding
		da->encoding->encoding = xe;
	}
	// If current data is DataStream //
	else if (cdata->type == SWE_DataStream) {
		DataStream *ds = (DataStream*)cdata->component;
		ds->encoding->type = SWE_BinaryEncoding; // By default assign default encoding
		ds->encoding->encoding = xe;
	}
	return swe_ok;
}

/*
 *
 */
int data_stream_xml_encoding_component(DecoderData* data){
	PHDEBUG_INSTRUCTION(cdmsg(BLU,"%s", __func__));

	XMLEncoding *xe;
	XMLEncodingComponent* c=swe_malloc(sizeof(BinaryEncodingComponent));
	// Get char attributes //
	GET_ATTRIBUTE(c, ref);
	GET_ATTRIBUTE(c, dataType);


	// Process the ref and split it in path //

	c->path = set_substrings(c->ref, "/");

	// If current data is DataArray //
	if (cdata->type == SWE_DataArray ) {
		DataArray *da = (DataArray*)cdata->component;
		xe = da->encoding->encoding;
		da->encoding->type = SWE_XMLEncoding; // By default assign default encoding
	}

	// If current data is DataStream //
	else if (cdata->type == SWE_DataStream) {
		DataStream *ds = (DataStream*)cdata->component;
		xe = ds->encoding->encoding;
		ds->encoding->type = SWE_XMLEncoding; // By default assign default encoding
		ds->encoding->encoding = xe;
	}
	ATTACH_TO_ARRAY(xe->components, c);

	return swe_ok;
}

int data_stream_values(DecoderData* data){
	// DataStream is the root element
	DataStream* ds;
	if (croot->type != SWE_DataStream) {
		errmsg("expected DataStream! file %s line %d", __FILE__, __LINE__);
	}
	ds = (DataStream*)croot->component;
	ds->values = get_value(data->current_element);
	// Check if there are special chars
	ds->values = convert_xml_special_chars(ds->values);
	return swe_ok;
}

/*************************************************************************
 * 	 	 	 	 	 	Private functions
 *------------------------------------------------------------------------
 * Parser handlers for the identification detection
 *************************************************************************/
char* get_attribute(char* attribute_name, Decoder_Element* e){
	int i;
	if(attribute_name==NULL) return NULL;
	for(i=0; i<e->nattr; i++){
		if(!strcmp(e->attr[i]->name, attribute_name)){
			return set_string(e->attr[i]->value);
			//return e->attr[i]->valucdmsg(YEL,e;
		}
	}
	return NULL;
}

char* get_value(Decoder_Element* e){
	if(e->value!=NULL) return set_string(e->value);
	else return NULL;
}

char* get_uid(Decoder_Element* e){
	char* attr=get_attribute("codeSpace", e);
	if(attr==NULL) return NULL;
	if(!strcmp("uid", attr)){
		if(e->value!=NULL){
			return set_string(e->value);
		}
	}
	return NULL;
}

/*
 * Creates a new swe data field and sets its common attributes
 */
Field* new_swe_data_field_handler(DecoderData* data) {
	Field* newfield=new_swe_data_field(data->level, NULL);
	GET_ATTRIBUTE(newfield, name);
	GET_ATTRIBUTE(newfield, title);
	GET_ATTRIBUTE(newfield, href);
	return newfield;
}

