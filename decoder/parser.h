/*
 * parser.h
 *
 *  Created on: Jul 21, 2016
 *      Author: enoc
 */

#ifndef INC_PARSER_H_
#define INC_PARSER_H_

#include "decoder/decoder.h"


int process_element(DecoderData* data);
int init_compare_paths();
int init_parser();

/**************************************************************************
 	 	 	 	 	 	 Comparison structures definition
 *************************************************************************/

typedef struct {
	char* path;
	int (*handler)(DecoderData* e);
}Parser_element;


typedef struct {
	char* path;
	int (*handler)(DecoderData* e);
	Parser_element ** element;
}Comparison_block;


typedef enum {
	VARIABLE_PATH_ELEMENT=0,
	STATIC_PATH_ELEMENT
}Parser_Element_Type;

// Structure which contains useful info when parsing a path
typedef struct{
	char* fullpath;
	char* cpath; // current path start
	int clength; // current path length
	Parser_Element_Type type;
	uchar end; // TRUE if the path has ended

	char* save_position; // store the position for multiple checks in

}Parser_Path_Compare;



#define DECODER_MAX_PATH_SIZE 500 //max length in the path string
#define DECODER_MAX_DEPTH 50 //max number spaces

Decoder_Element* element_constructor();
int element_destructor(Decoder_Element*);
int deinit_parser(DecoderData* data);


/*************************************************************************
 * 	 	 	 	 	 	 	ADD_TO_PATH MACRO
 *------------------------------------------------------------------------
 * Macro function that takes a new name and adds it to the global path
 *************************************************************************/

#define ADD_TO_PATH(data) ({ \
	char* point=data->path+strlen(data->path); \
	memcpy(point, PATH_SEPARATOR, PATH_SEPARATOR_LENGTH); \
	point+=PATH_SEPARATOR_LENGTH; \
	memcpy(point, data->current_element->name, strlen(data->current_element->name)); \
	data->level++; \
})

/*************************************************************************
 * 	 	 	 	 	 	 	REMOVE_FROM_PATH MACRO
 *------------------------------------------------------------------------
 * Macro function that erases the last element in the global path
 *************************************************************************/

#define  REMOVE_FROM_PATH(data) ({ \
	char* point; \
	for(point=data->path+strlen(data->path); memcmp(point, PATH_SEPARATOR, PATH_SEPARATOR_LENGTH)!=0; point--){ \
		*point=0; \
	} \
	*point=0; \
	data->level--;})




/*************************************************************************
 * 	 	 	 	 	 	 	PARSER DEBUGGING MACRO
 *------------------------------------------------------------------------
 * In order to debug any possible bugs in the parser there are lots of
 * console messages wrapped with the PDEBUG_INSTRUCTION macro. If
 * DEBUG_PARSER is ON, they are displayed. Otherwise they are discarded by
 * the compiler
 *************************************************************************/

#if DEBUG_PARSER
// If debugging enabled, execute the instruction
#define PDEBUG_INSTRUCTION(x)({x;})
#else
// If debugging is not enabled, define an empty macro
// the compiler will discard this instructions
#define PDEBUG_INSTRUCTION(x)
#endif




//---------------------------------------------------------------------------//
//-------------------------- Instrument Identifiers -------------------------//
//---------------------------------------------------------------------------//
#define INSTRUMENT_IDENTIFICATION_BLOCK \
	{.path = "/../PhysicalSystem", .handler=new_instrument}

#define INSTRUMENT_IDENTIFICATION_ELEMENTS 		\
	/* Identifiers */ \
	{.path = "/identification/IdentifierList/identifier/Term", .handler=instrument_identifier_term}, \
	{.path = "/identification/IdentifierList/identifier/Term/label", .handler=instrument_identifier_term_label}, \
	{.path = "/identification/IdentifierList/identifier/Term/value", .handler=instrument_identifier_term_value}, \
	/* Classifiers */ \
	{.path = "/classification/ClassifierList/classifier/Term", .handler=instrument_classifier_term}, \
	{.path = "/classification/ClassifierList/classifier/Term/label", .handler=instrument_classifier_term_label}, \
	{.path = "/classification/ClassifierList/classifier/Term/value", .handler=instrument_classifier_term_value}, \
	/* Feature Of Interest */ \
	{.path = "/featuresOfInterest/FeatureList/feature", .handler=instrument_feature_of_interest}, \
	/* Position */ \
	{.path  = "/position", .handler = instrument_position }, \
	{.path  = "/position/Vector/coordinate/Quantity", .handler = instrument_position_axis }, \
	{ .path = "/position/Vector/coordinate/Quantity/value", .handler = instrument_position_value }, \
	/* Instrument UID */ \
	{.path = "/identifier", .handler=instrument_uid}, \
	/* AttachedTo section */\
	{.path = "/attachedTo", .handler=instrument_attached_to}, \
	{.path=NULL, .handler=NULL}	 //The last element MUST be NULL



//---------------------------------------------------------------------------//
//-------------------------- Instrument Parameters -------------------------//
//---------------------------------------------------------------------------//

#define INSTRUMENT_PARAMETERS_BLOCK \
	{.path = "/PhysicalSystem/parameters/ParameterList/parameter", .handler=instrument_parameter}

#define INSTRUMENT_PARAMETERS_ELEMENTS 		\
	{.path = "/DataInterface/interfaceParameters", .handler=instrument_parameter_data_interface}, \
	{.path = "/DataInterface/interfaceParameters/../field", .handler=instrument_parameter_data_interface_field}, \
	/* Generic handlers for Basic types */ \
	{.path = "/../Quantity", .handler=generic_field_quantity}, \
	{.path = "/../Count",    .handler=generic_field_count}, \
	{.path = "/../Text",     .handler=generic_field_text}, \
	{.path = "/../Boolean",  .handler=generic_field_boolean}, \
	{.path = "/../Time",  .handler=generic_field_time}, \
	{.path = "/../Category",  .handler=generic_field_category}, \
	{.path = "/../value",  .handler=generic_field_value}, \
	{.path = "/../label",  .handler=generic_field_label}, \
	{.path = "/../uom",  .handler=generic_field_uom}, \
	{.path=NULL, .handler=NULL}	 //The last element MUST be NULL */


//---------------------------------------------------------------------------//
//-------------------------- Instrument capabilities -------------------------//
//---------------------------------------------------------------------------//

#define INSTRUMENT_CAPABILITIES_BLOCK \
	{.path = "/PhysicalSystem/../CapabilityList/capability", .handler=instrument_capability}

#define INSTRUMENT_CAPABILITIES_ELEMENTS \
	/* Generic handlers for Basic types */ \
	{.path = "/../Quantity", .handler=generic_field_quantity}, \
	{.path = "/../Count",    .handler=generic_field_count}, \
	{.path = "/../Text",     .handler=generic_field_text}, \
	{.path = "/../Boolean",  .handler=generic_field_boolean}, \
	{.path = "/../Time",  .handler=generic_field_time}, \
	{.path = "/../Category",  .handler=generic_field_category}, \
	{.path = "/../value",  .handler=generic_field_value}, \
	{.path = "/../label",  .handler=generic_field_label}, \
	{.path = "/../uom",  .handler=generic_field_uom}, \
	{.path=NULL, .handler=NULL}	 //The last element MUST be NULL */


//---------------------------------------------------------------------------//
//----------------------------  SimpleProcess -------------------------------//
//---------------------------------------------------------------------------//

#define MISSION_SIMPLE_PROCESS_BLOCK \
	{.path ="/../AggregateProcess/components/ComponentList/component", .handler=new_mission_process}

// No Elements //

//------------ Mission Simple Processes ------------//
#define INSTRUMENT_SIMPLE_PROCESS_BLOCK \
	{.path ="/../PhysicalSystem/components/ComponentList/component", .handler=new_instrument_process}

// No Elements //

//------------ Generic Simple Processes ------------//
#define GENERIC_SIMPLE_PROCESS_BLOCK \
	{.path ="/PhysicalSystem/../SimpleProcess", .handler=NULL}

#define GENERIC_SIMPLE_PROCESS_ELEMENTS 		\
	{.path = "/identifier", .handler=instrument_process_identifier}, \
	{.path = "/typeOf", .handler=instrument_process_typeof}, \
	{.path = "/inputs/InputList/input", .handler=instrument_process_input}, \
	{.path = "/outputs/OutputList/output", .handler=instrument_process_output}, \
	{.path = "/parameters/ParameterList/parameter", .handler=instrument_process_parameter}, \
	/*----simple Process Parameters----*/ \
	{.path = "/../parameter/../Quantity", .handler=generic_field_quantity}, \
	{.path = "/../parameter/../Count",    .handler=generic_field_count}, \
	{.path = "/../parameter/../Text",     .handler=generic_field_text}, \
	{.path = "/../parameter/../Boolean",  .handler=generic_field_boolean}, \
	{.path = "/../parameter/../Time",  .handler=generic_field_time}, \
	{.path = "/../parameter/../Category",  .handler=generic_field_category}, \
	{.path = "/../parameter/../value",  .handler=generic_field_value}, \
	/* ------------SimpleProcess configuration --------------*/ \
	{.path = "/configuration/Settings/setValue", .handler=simple_process_settings_setvalue },\
	{.path = "/configuration/Settings/setStatus", .handler=simple_process_settings_setvalue },\
	/* Old Style Settings*/\
	{.path = "/configuration/AbstractSettings/extension/setValue", .handler=simple_process_settings_setvalue },\
	{.path = NULL, .handler=NULL} //The last element MUST be NULL


#define INSTRUMENT_INTERFACE_BLOCK \
		{.path ="/PhysicalSystem/../parameter/DataInterface", .handler=new_interface}

// No Elements //

#define MISSION_BLOCK \
		{.path ="/PhysicalSystem/../AggregateProcess", .handler=new_mission}

#define MISSION_IDENTIFIERS_ELEMENTS 		\
		{.path = "/identifier", .handler=mission_uid}, \
		{.path = "/../connection/Link/source", .handler=mission_connection_source}, \
		{.path = "/../connection/Link/destination", .handler=mission_connection_destination}, \
		{.path = NULL, .handler=NULL} //The last element MUST be NULL


//---------------------------------------------------------------------------//
//---------------------- SWE Bridge  Comparison Structures ------------------//
//---------------------------------------------------------------------------//


#define BRIDGE_IDENTIFICATION_BLOCK \
	{.path = "/PhysicalSystem/components/ComponentList/component/AggregateProcess", .handler=new_bridge}

#define BRIDGE_IDENTIFICATION_ELEMENTS 		\
	{.path = "/identifier", .handler=bridge_identifer}, \
	{.path = "/connections/ConnectionList/connection/Link/source", .handler=bridge_connection_source}, \
	{.path = "/connections/ConnectionList/connection/Link/destination", .handler=bridge_connection_destination}, \
	{.path=NULL, .handler=NULL}	 //The last element MUST be NULL



//------------ Data Model Structure ------------//
#define DATA_STRUCTURE_BLOCK \
	{.path = "/PhysicalSystem/../SimpleProcess/../DataInterface/../DataStream", .handler=new_data_stream}

#define DATA_STRUCTURE_ELEMENTS 		\
	{.path = "/elementType", .handler=data_stream_element_type}, \
	/* Field container */ \
	{.path = "/../field", .handler=data_stream_field}, \
	/* Basic data types */ \
	{.path = "/../Quantity", .handler=generic_field_quantity}, \
	{.path = "/../Count",    .handler=generic_field_count}, \
	{.path = "/../Text",     .handler=generic_field_text}, \
	{.path = "/../Boolean",  .handler=generic_field_boolean}, \
	{.path = "/../Time",  .handler=generic_field_time}, \
	{.path = "/../Category",  .handler=generic_field_category}, \
	/* Basic types elements */ \
	{.path = "/../uom",  .handler=generic_field_uom}, \
	{.path = "/../label", .handler=generic_field_label}, \
	{.path = "/../nilValue", .handler=data_stream_field_nilvalue}, \
	/* Complex Data Types */ \
	{.path = "/../DataRecord", .handler=data_stream_new_data_record}, \
	{.path = "/../DataArray", .handler=data_stream_new_data_array}, \
	/* Data Array Elements */  \
	{.path = "/../DataArray/elementCount", .handler=data_stream_data_array_element_count}, \
	{.path = "/../DataArray/elementCount/Count/value", .handler=data_stream_data_array_element_count_value}, \
	{.path = "/../DataArray/elementType", .handler=data_stream_data_array_element_type}, \
	{.path = "/../DataArray/encoding",  .handler=data_stream_array_encoding},   \
	/* Encoding */ \
	{.path = "/encoding",          .handler=data_stream_encoding_new}, \
	{.path = "/../TextEncoding/extension/Text/value",  .handler=data_stream_text_encoding_start_token},   \
	{.path = "/../TextEncoding",  .handler=data_stream_text_encoding},   \
	{.path = "/../BinaryEncoding",.handler=data_stream_binary_encoding}, \
	{.path = "/../BinaryEncoding/member/Component",.handler=data_stream_binary_encoding_component}, \
	{.path = "/../XMLEncoding",.handler=data_stream_xml_encoding}, \
	{.path = "/../XMLEncoding/extension/Component",.handler=data_stream_xml_encoding_component}, \
	/* DataStream values */ \
	{.path = "/values", .handler=data_stream_values}, \
	{.path=NULL, .handler=NULL}	 //The last element MUST be NULL

#endif /* INC_PARSER_H_ */

