// Standard Libraries //
#include "decoder/sxmlc/sxmlc_decoder.h"
#include "decoder/sxmlc/sxmlc_decoder.h"

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

// Local libraries //
#include "common/swe_utils.h"
#include "decoder/decoder.h"
#include "decoder/parser.h"
#include "decoder/sxmlc/sxmlc.h"
#include "decoder/sxmlc/sxmlsearch.h"
#include "resources/mem_control.h"
#include "swe_conf.h"


// Execs function and stops the SXMLC parser if error (can't use TRY_RET because of the return
// values in the SXMLC library)
#define SXMLC_TRY(func) ({\
	int __temp_error = func; \
	if (__temp_error != true ) { \
		errmsg("Error in SXMLC decoder: \"%s\" ",  swe_error_strings[abs(__temp_error)]);\
		return FALSE; \
	} \
})

int decode_xml_file(char* filename, DecoderData* decoder_data){
	msg_title(BLU, "XML Decoder");
	mem_status();
	set_memory_table(MEM_DECODER);
	XMLDoc doc;
	XMLDoc_init(&doc);
	SAX_Callbacks callbacks;
	dmsg("initializing sax callbacks...");
	init_sax_callbacks(&callbacks);
	dmsg("starting SAX decoder...");
	int ret =XMLDoc_parse_file_SAX(filename, &callbacks, decoder_data);
	if ( ret != true ) {
		errmsg( "error in SAX cxml parser");
		return swe_error;
	}

	return swe_ok;
}



/*************************************************************************
 	 	 	 	 	 	 	 init_sax_callbacks
 *************************************************************************
 * Sets the handlers for each callback in the SAX_Callback structure.
 *************************************************************************/
int init_sax_callbacks(SAX_Callbacks* sax){
	sax->start_doc = NULL; //cxml_start_doc;
	sax->start_node = NULL; //cxml_start_node;
	sax->end_node = NULL; //cxml_end_node;
	sax->new_text = NULL; //cxml_new_text;
	sax->end_doc = NULL; //xml_end_doc;
	sax->on_error = NULL;
	sax->all_event = cxml_all_events;
	return 0;
}

// Callback called when parsing starts, before parsing the first node.
int cxml_start_doc(SAX_Data* sd){
	DecoderData* data = (DecoderData*)sd->user;
	dmsg("start document...");
	TRY_RET(init_parser(data));
	return true;
}


static const char _non_relevant_char_list[] = {
	' ',
	'\n',
	'\r',
	'\t',
	0
};

// Checks if a string is entirely composed of ' ', '\n', '\r' and '\t'
// retunrs 0 on a relevant string and 1 in a non-relevant string
int check_nonrelevant_chars(char* text){
	int i, j;


	if (text==NULL) {
		return 1;
	}
	for (i=0; text[i] != 0; i++ ) { // loop through the input text
		uchar relevant_char = TRUE;
		for (j = 0; _non_relevant_char_list[j] != 0; j++) { //loop through the char list
			if (text[i] == _non_relevant_char_list[j]){
				relevant_char = FALSE;
			}
		}
		if (relevant_char) {
			return 0;
		}
	}

	return 1;
}


//Callback called when a new node starts (e.g. '<tag>' or '<tag/>').
//If any, attributes can be read from 'node->attributes'.
//N.B. '<tag/>' will trigger an immediate call to the 'end_node' callback
//after the 'start_node' callback.

// TODO remove count
int cxml_start_node(const XMLNode* node, SAX_Data* sd){
	int i;
	char* name;

	DecoderData* data = (DecoderData*)sd->user;


	// If there's any previous element, process it
	if (data->current_element != NULL) {
		TRY_RET(process_element(data));
		TRY_RET(element_destructor(data->current_element));
	}
	data->current_element = element_constructor();

	// Step I: Locate the namespace and the tagname
	// data->current_element->name = node->tag; // if no namespace is found, the tag is the whole string

	// ignore the namespace
	if (( name = sx_strchr(node->tag, ':')) == NULL ){
		name = node->tag; // if namespace not found, name is  the whole node tag
	} else{
		name++; // ignore ':'
	}
	data->current_element->name = set_string(name); // the second string is the tagname

	// Step II: Check attributes
	if (node->n_attributes > 0){
		// Allocate space for the attributes
		data->current_element->attr = swe_malloc((node->n_attributes+1)*sizeof(Decoder_Attribute*));
		data->current_element->nattr = node->n_attributes;
		for (i=0; i < node->n_attributes ; i++) {
			XMLAttribute* attr = &node->attributes[i];
			char* attr_name;
			data->current_element->attr[i] = swe_malloc(sizeof(Decoder_Attribute));

			// ignore namespace in the attributre (if present)
			if (( attr_name = sx_strchr(attr->name, ':')) == NULL ){
				attr_name = attr->name; // if namespace not found, name is  the whole node tag
			} else {
				attr_name++; // ignore ':'
			}

			data->current_element->attr[i]->name = set_string(attr_name);
			data->current_element->attr[i]->value = set_string(attr->value);


		}
	}
	ADD_TO_PATH(data); // add the new element to the data path
	return true;
}

int cxml_end_node(const XMLNode* node, SAX_Data* sd){
	DecoderData* data = (DecoderData*)sd->user;
	//mem_status();
	if(data->current_element != NULL) {
		process_element(data);
		element_destructor(data->current_element);
		data->current_element=NULL;
	}
	REMOVE_FROM_PATH(data);
	return true;
}

// Callback called when text has been found in the last node.

int cxml_new_text(SXML_CHAR* text, SAX_Data* sd){
	DecoderData* data = (DecoderData*)sd->user;


	// check if the input text is only whitespaces, newlines and tabs
	// This is a workaround to fix a bad xml parsing from the library...
	if (data->current_element == NULL) {
		errmsg( "No value (text) expected at this point!");
		return false;
	}

	// TODO check if set string is the guilty!!!
	data->current_element->value = set_string(text);

	return true;
}

// Callback called when parsing is finished. No other callbacks will be called after it.
int cxml_end_doc(SAX_Data* sd){



	return true;
}

// Callback called when an error occurs during parsing.
// 'error_num' is the error number and 'line_number' is the line number in the stream
// being read (file or buffer).
int cxml_on_error(ParseError error_num, int line_number, SAX_Data* sd){
	return true;
	errmsg( "CXML error in line %d (error_num %d)", line_number, error_num);
	return true;
}


/*static const char* tag_type_names[] = {
	"TAG_NONE",
	"TAG_PARTIAL",
	"TAG_FATHER",
	"TAG_SELF",
	"TAG_INSTR",
	"TAG_COMMENT",
	"TAG_CDATA",
	"TAG_DOCTYPE",
	"TAG_END",
	"TAG_TEXT",
};*/

/* static const char* xml_event_names[] = {
			"XML_EVENT_START_DOC",
			"XML_EVENT_START_NODE",
			"XML_EVENT_END_NODE",
			"XML_EVENT_TEXT",
			"XML_EVENT_ERROR",
			"XML_EVENT_END_DOC",
}; **/

int cxml_all_events(XMLEvent event, const XMLNode* node, SXML_CHAR* text, const int n, SAX_Data* sd){

	// Ignore CDATA, Comment DOCTYPE, INSTR tags

	if ( node != NULL ) {
		TagType tag = node->tag_type;
		if (tag == TAG_CDATA || tag == TAG_COMMENT || tag == TAG_DOCTYPE || tag == TAG_INSTR ){
			return true;
		}
	}
	DecoderData* data = (DecoderData*) sd->user;
	//dmsg("Event %s", xml_event_names[event]);
	if (event == XML_EVENT_START_DOC){
		//cdmsgn( MAG, "                                                       START DOC\n");
		TRY_RET(init_parser(data));

	} else if (event == XML_EVENT_START_NODE){
		// Check tag type to ignore comments, instructions and CDATA
		if (node->tag_type == TAG_FATHER || node->tag_type == TAG_SELF ) {
			//cdmsgn( BLU, "                                                       START NODE (%s) (%s)\n", node->tag, tag_type_names[node->tag_type]);
			SXMLC_TRY(cxml_start_node(node, sd));
		}

		if (node->tag_type == TAG_SELF){
			// If the tag was self-containd, trigger end node (i.e. <mytag is="cool"/>)
			SXMLC_TRY(cxml_end_node(node, sd));
		}

	} else if (event == XML_EVENT_END_NODE){

		if (node->tag_type == TAG_END ) {
			//cdmsgn( CYN, "                                                       END NODE (%s)\n",tag_type_names[node->tag_type]);
			SXMLC_TRY(cxml_end_node(node, sd));
		}

	} else if ((event == XML_EVENT_TEXT) && (!check_nonrelevant_chars(text))){
		//cdmsgn( YEL, "                                                       NEW TEXT (%s)\n", text);
		SXMLC_TRY(cxml_new_text(text, sd));

	} else if (event == XML_EVENT_END_DOC){
		deinit_parser(data);


	} else if (event == XML_EVENT_ERROR){
		errmsg("SXMLC ERROR!!");
		return false;


	} else {
		// Unhandled events, do nothing
	}

	return true;
}

