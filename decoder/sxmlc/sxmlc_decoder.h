/*
 * callbacks.h
 *
 *  Created on: Jan 10, 2018
 *      Author: enoc
 */

#ifndef CXML_CALLBACKS_H_
#define CXML_CALLBACKS_H_

#include "decoder/decoder.h"
#include "decoder/sxmlc/sxmlc.h"

int init_sax_callbacks(SAX_Callbacks* sax);

// Callback called when parsing starts, before parsing the first node.
int cxml_start_doc(SAX_Data* sd);

//Callback called when a new node starts (e.g. '<tag>' or '<tag/>').
//If any, attributes can be read from 'node->attributes'.
//N.B. '<tag/>' will trigger an immediate call to the 'end_node' callback
//after the 'start_node' callback.
int cxml_start_node(const XMLNode* node, SAX_Data* sd);

// Callback called when a node ends (e.g. '</tag>' or '<tag/>').
int cxml_end_node(const XMLNode* node, SAX_Data* sd);

// Callback called when text has been found in the last node.

int cxml_new_text(SXML_CHAR* text, SAX_Data* sd);

// Callback called when parsing is finished. No other callbacks will be called after it.
int cxml_end_doc(SAX_Data* sd);

// Callback called when an error occurs during parsing.
// 'error_num' is the error number and 'line_number' is the line number in the stream
// being read (file or buffer).
int cxml_on_error(ParseError error_num, int line_number, SAX_Data* sd);

/*
 Callback called when text has been found in the last node.
 'event' is the type of event for which the callback was called:
	 XML_EVENT_START_DOC:
		 'node' is NULL.
		 'text' is the file name if a file is being parsed, NULL if a buffer is being parsed.
		 'n' is 0.
	 XML_EVENT_START_NODE:
		 'node' is the node starting, with tag and all attributes initialized.
		 'text' is NULL.
		 'n' is the number of lines parsed.
	 XML_EVENT_END_NODE:
		 'node' is the node ending, with tag, attributes and text initialized.
		 'text' is NULL.
		 'n' is the number of lines parsed.
	 XML_EVENT_TEXT:
		 'node' is NULL.
		 'text' is the text to be added to last node started and not finished.
		 'n' is the number of lines parsed.
	 XML_EVENT_ERROR:
		 Everything is NULL.
		 'n' is one of the 'PARSE_ERR_*'.
	 XML_EVENT_END_DOC:
		 'node' is NULL.
		 'text' is the file name if a file is being parsed, NULL if a buffer is being parsed.
		 'n' is the number of lines parsed.
 */
int cxml_all_events(XMLEvent event, const XMLNode* node, SXML_CHAR* text, const int n, SAX_Data* sd);

//---- TAG TYPES ----//
/*
	TAG_FATHER (<mytag>) is a node that will have children. Nodes read after it will be added as children of this tag.

	TAG_SELF (<mytag/>) is a "self-contained" node that has no children. Nodes read after it are siblings of this tag.

	TAG_END (</mytag>) is an ending node that closes its father (if tag names matches). Nodes read after it are siblings of father tag.
	There is no XMLNode associated with this as it is only used internally by the parser.

	TAG_INSTR (<?text?>) is a node used for prolog and processing instructions.

	TAG_COMMENT (<!--text-->) is a comment node.

	TAG_CDATA (<![CDATA[text]]/>) is a CDATA escaped node.

	TAG_DOCTYPE (<!DOCTYPE text]/>) is a DOCTYPE node.

	TAG_TEXT (since v4.1.0) is a special node which text field contains the text of its father node. It is particularly useful when you
	want to DOM-parse a document and want to keep the text position in the childrend, instead of merging all text pieces (default behavior
	before v4.1.0).

*/

int decode_xml_file(char* filename, DecoderData* decoder);



#endif /* CXML_CALLBACKS_H_ */
