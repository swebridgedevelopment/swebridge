#include "swe_conf.h"
#if USE_EXIP_LIBRARY

/*
 * sml_parser.c
 *
 *  Created on: Jul 13, 2016
 *      Author: enoc
 */

#include "decoder/parser.h"
#include "decoder/decoder.h"
#include "decoder/exip/inc/exip_decoder_handlers.h"
#include "resources/mem_control.h"
#include "decoder/exip/inc/common/procTypes.h"
#include "swe_conf.h"





//Private functions declaration
char* convertToString(const String* in);





/**************************************************************************
 	 	 	 	 	 	 	 PUBLIC FUNCTION
 *-------------------------------------------------------------------------
 * Functions called by the exip processor as handlers to decode the file
 *************************************************************************/


errorCode sml_start_document(void* app_data){
	DecoderData* data = (DecoderData*)app_data;
	TRY_RET(init_parser(data));
	return OK;
}

errorCode sml_start_element(QName qname, void* app_data){
	DecoderData* data = (DecoderData*)app_data;

	//Process previous element
	if(data->current_element!=NULL) { //If there was an unprocessed element before this one
		process_element(data);
		element_destructor(data->current_element);
		data->current_element=NULL;
	}

	data->current_element=element_constructor(); 	//construct new element
	data->current_element->name=convertToString((String*)qname.localName); 	//set the name for the new element
	ADD_TO_PATH(data);
	return OK;
}


errorCode sml_end_element(void* app_data){
	DecoderData* data = (DecoderData*)app_data;
	//mem_status();
	if(data->current_element!=NULL) {
		process_element(data);
		element_destructor(data->current_element);
		data->current_element=NULL;
	}
	REMOVE_FROM_PATH(data);
	return OK;
}


errorCode sml_end_document(void* app_data){
	DecoderData* data = (DecoderData*)app_data;
	//free current element
	if(data->current_element!=NULL) swe_free(data->current_element);
	swe_free(data->path);
	deinit_parser(data);

	return OK;
}

errorCode sml_attribute(QName qname, void* app_data) {
	DecoderData* data = (DecoderData*)app_data;

	char* name=convertToString((String*)qname.localName);

	Decoder_Attribute* attr;
	data->current_element->attr_flag=1;
	//Allocate first attribute
	if(data->current_element->nattr==0){
		data->current_element->attr=swe_malloc(sizeof(Decoder_Attribute*));
	} else{//reallocate for more attributes
		int n=data->current_element->nattr;
		data->current_element->attr=(void*)swe_realloc(data->current_element->attr, sizeof(Decoder_Attribute*)*(n+1) );
	}
	data->current_element->attr[data->current_element->nattr]=swe_malloc(sizeof(Decoder_Attribute));

	attr=data->current_element->attr[data->current_element->nattr];
	attr->name=name;

	data->current_element->nattr++;
	return OK;
}

errorCode sml_string_data(String instr, void* app_data){
	DecoderData* data = (DecoderData*)app_data;
	char* value=convertToString(&instr);

	if(data->current_element==NULL){
		return OK;
	}
	//Check if the data is from a flag
	if(data->current_element->attr_flag){
		data->current_element->attr_flag=0;
		Decoder_Attribute* attr=data->current_element->attr[data->current_element->nattr-1];
		attr->value=value;
	} else{
		data->current_element->value=value;
	}
	return OK;
}

errorCode sml_fatal_error(const errorCode code, const char* str, void* app_data){
	errmsg( "\n\r%d : FATAL ERROR: %s", code, str);
	return EXIP_HANDLER_STOP;
}





/**************************************************************************
 	 	 	 	 	 	 	 Private functions
 *************************************************************************/

char* convertToString(const String* in){
	char* str;
	int len=in->length;
	str=swe_malloc(sizeof(char)*(len+1));
	memcpy(str, in->str, len);
	str[len]=0;
	return str;
}







#endif

