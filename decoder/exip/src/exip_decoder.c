#include "swe_conf.h"
#if USE_EXIP_LIBRARY

#include "swe_conf.h"
#include "resources/resources.h"

#include "decoder/decoder.h"

//EXIP libraries
#include "decoder/exip/inc/exip_decoder.h"
#include "decoder/exip/inc/exip_decoder_handlers.h"
#include "decoder/exip/inc/common/procTypes.h"
#include "decoder/exip/inc/common/stringManipulate.h"
#include "decoder/exip/inc/contentIO/EXIParser.h"
#include "decoder/exip/inc/grammarGen/grammarGenerator.h"

#define INPUT_BUFFER_SIZE 200
#define MAX_PREFIXES 10

// Content Handler API

static void parseOpsMask(char* mask, EXIOptions* ops);

int globalError=swe_ok;

#define MAX_XSD_FILES_COUNT 10 // up to 10 XSD files
#define OUT_EXI 0
#define OUT_XML 1
const char* EXImask = "=-%%v%%-%%-%%-"; //Define the mask

/****************************************************************************
 *								START EXI DECODER
 ****************************************************************************
 * Start the decode process by opening the input file and configuring the
 * parser. A SensorML structure is filled with all the relevant information
 * obtained from the decoded EXI file.
 ****************************************************************************/

int exip_decode_file(char* filename, DecoderData* decoder_data){
	//sweDriver Variables
	swe_file* infile;
	char* mask;
	int err;

	//EXIP variable declarations
	EXIPSchema* schemaPtr = NULL;
	EXIOptions ops;
	EXIOptions* opsPtr = NULL;
	swe_boolean outOfBandOpts = e_FALSE;
	unsigned char outFlag = OUT_EXI; // Default output option
	errorCode tmp_err_code = EXIP_UNEXPECTED_ERROR;
	set_memory_table(MEM_DECODER);
	msg_sub_title( CYN, "Decode SensorML File");
	makeDefaultOpts(&ops);
	outOfBandOpts = e_TRUE;

	mask=set_string(EXImask);
	parseOpsMask(mask, &ops);
	swe_free(mask);
	opsPtr = &ops;

	infile=swe_fopen(filename, swe_read_file);
	if(!infile) {
		errmsg( "Unable to open file %s\n", filename);
		return swe_error;
	}

	dmsg( "Start decoding the source file [%s]", filename);

	//Start the decoding process
	tmp_err_code = exip_decode(schemaPtr, outFlag, infile, outOfBandOpts, opsPtr, readFileInputStream,  decoder_data);

	if(schemaPtr != NULL)
		destroySchema(schemaPtr);
	err=swe_fclose(infile);
	if(err!=swe_ok){
		errmsg( "Error %d while closing file '%s'", err, filename);
	}

	if(tmp_err_code != EXIP_OK)
	{
		errmsg( "Error (code: %d) during parsing of the EXI stream: %s\n", tmp_err_code, filename);
		return swe_error;
	}

	cimsg( GRN, "Successful parsing of the EXI stream: %s\n", filename);
	return swe_ok;
}

/****************************************************************************
 *							READ FILE INPUT STREAM
 ****************************************************************************
 * Generic function to read from a file.
 ****************************************************************************/
size_t readFileInputStream(void* buf, size_t readSize, void* stream)
{
	swe_file *infile = (swe_file*) stream;
	int error;
	error=swe_fread(buf, readSize, infile);
	if(error<0){

		errmsg( "error %d in readFileInputStream", error);
		return 0;
	}
	return (size_t)error;
}

/****************************************************************************
 *							Parse Ops Mask
 ****************************************************************************
 ****************************************************************************/
static void parseOpsMask( char* mask, EXIOptions* ops)
{
	unsigned int i;
	char *token;

	for (token = strtok(mask, "=%"), i = 0; token != NULL; token = strtok(NULL, "=%"), i++)
	{
		switch(i)
		{
			case 0:
			if(strcmp(token, "-"))
			{
				// Preservation Options: c - comments, d - dtds, l - lexicalvalues, p - pis, x- prefixes
				if(strstr(token, "c") != NULL)
					SET_PRESERVED(ops->preserve, PRESERVE_COMMENTS);
				if(strstr(token, "d") != NULL)
					SET_PRESERVED(ops->preserve, PRESERVE_DTD);
				if(strstr(token, "l") != NULL)
					SET_PRESERVED(ops->preserve, PRESERVE_LEXVALUES);
				if(strstr(token, "p") != NULL)
					SET_PRESERVED(ops->preserve, PRESERVE_PIS);
				if(strstr(token, "x") != NULL)
					SET_PRESERVED(ops->preserve, PRESERVE_PREFIXES);
			}
			break;
			case 1:
			if(strcmp(token, "-"))
			{
				// Other options: v - strict interpretation of schema, f - fragments
			    // r - selfContained, c - compression, p - pre-compression, a - aligned to bytes\n");
				if(strstr(token, "v") != NULL)
					SET_STRICT(ops->enumOpt);
				if(strstr(token, "f") != NULL)
					SET_FRAGMENT(ops->enumOpt);
				if(strstr(token, "r") != NULL)
					SET_SELF_CONTAINED(ops->enumOpt);
				if(strstr(token, "c") != NULL)
					SET_COMPRESSION(ops->enumOpt);
				if(strstr(token, "p") != NULL)
					SET_ALIGNMENT(ops->enumOpt, PRE_COMPRESSION);
				else if(strstr(token, "a") != NULL)
					SET_ALIGNMENT(ops->enumOpt, BYTE_ALIGNMENT);
			}
			break;
			case 2:
			if(strcmp(token, "-"))
			{
				// valuePartitionCapacity
				ops->valuePartitionCapacity = (Index) strtol(token, NULL, 10);
			}
			break;
			case 3:
			if(strcmp(token, "-"))
			{
				// valueMaxLength
				ops->valueMaxLength = (Index) strtol(token, NULL, 10);
			}
			break;
			case 4:
			if(strcmp(token, "-"))
			{
				// blockSize
				ops->blockSize = (uint32_t) strtol(token, NULL, 10);
			}
			break;
			default:
			{
				errmsg( "Wrong options mask: %s", mask);

				exit(1);
			}
		}
	}
}


/****************************************************************************
 *								DECODE
 ****************************************************************************
 * Start the decode process. The function parses the strings found in
 * the EXI file one by one calling the appropriate handler function. After the
 * handler the function parserInputElement is called,
 ****************************************************************************/
errorCode exip_decode(EXIPSchema* schemaPtr, uchar outFlag, swe_file *infile,
		swe_boolean outOfBandOpts, EXIOptions* opts,
		size_t (*inputStream)(void* buf, size_t size, void* stream), DecoderData* decoder_data){

	Parser testParser;
	char buf[INPUT_BUFFER_SIZE];
	BinaryBuffer buffer;
	errorCode tmp_err_code = EXIP_UNEXPECTED_ERROR;
	mem_status();

	buffer.buf = buf;
	buffer.bufLen = INPUT_BUFFER_SIZE;
	buffer.bufContent = 0;
	// Parsing steps:
	// I: First, define an external stream for the input to the parser if any, otherwise set to NULL
	buffer.ioStrm.readWriteToStream = inputStream;
	buffer.ioStrm.stream = infile;
	// II: Second, initialize the parser object

	TRY(parse.initParser(&testParser, buffer, NULL));
	testParser.app_data = (void*)decoder_data;
	// III: Initialize the parsing data and hook the callback handlers to the parser object.
	//      If out-of-band options are defined use testParser.strm.header.opts to set them

	if(outOfBandOpts && opts != NULL)
		testParser.strm.header.opts = *opts;

	testParser.handler.fatalError = sml_fatal_error;
	testParser.handler.error = sml_fatal_error;
	testParser.handler.startDocument = sml_start_document;
	testParser.handler.endDocument = sml_end_document;
	testParser.handler.startElement = sml_start_element;
	testParser.handler.attribute = sml_attribute;
	testParser.handler.stringData = sml_string_data;
	testParser.handler.endElement = sml_end_element;


	// IV: Parse the header of the stream
	TRY(parse.parseHeader(&testParser, outOfBandOpts));
	// IV.1: Set the schema to be used for parsing.
	// The schemaID mode and schemaID field can be read at
	// parser.strm.header.opts.schemaIDMode and
	// parser.strm.header.opts.schemaID respectively
	// If schemaless mode, use setSchema(&parser, NULL);

	TRY(parse.setSchema(&testParser, schemaPtr));
	// V: Parse the body of the EXI stream

	while(tmp_err_code == EXIP_OK && globalError ==swe_ok)
	{
		tmp_err_code = parse.parseNext(&testParser);
	}
	// VI: Free the memory allocated by the parser
	parse.destroyParser(&testParser);

	if(tmp_err_code == EXIP_PARSING_COMPLETE)
		return EXIP_OK;
	else
		return tmp_err_code;
}






#endif

