#include "swe_conf.h"
#if USE_EXIP_LIBRARY


#ifndef INC_DECODER_HANDLERS_H_
#define INC_DECODER_HANDLERS_H_

#include "decoder/exip/inc/common/procTypes.h"
#include "swe_conf.h"



errorCode sml_fatal_error(const errorCode code, const char* str, void* app_data);
errorCode sml_start_document(void* app_data);
errorCode sml_start_element(QName qname, void* app_data);

errorCode sml_end_document(void* app_data);
errorCode sml_attribute(QName qname, void* app_data);
errorCode sml_string_data(String value, void* a);
errorCode sml_end_element(void* app_data);


#endif /* INC_DECODER_HANDLERS_H_ */


#endif

