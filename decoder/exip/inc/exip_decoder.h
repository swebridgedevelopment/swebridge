#include "swe_conf.h"
#if USE_EXIP_LIBRARY

/*
 * exip_decoder.h
 *
 *  Created on: Jan 10, 2018
 *      Author: enoc
 */

#ifndef DECODER_INC_EXIP_DECODER_H_
#define DECODER_INC_EXIP_DECODER_H_

#include "decoder/exip/inc/common/procTypes.h"
#include "swe_conf.h"


int exip_decode_file(char* filename, DecoderData* decoder_data);

size_t readFileInputStream(void* buf, size_t readSize, void* stream);

errorCode exip_decode(EXIPSchema* schemaPtr, uchar outFlag, swe_file *infile,
		swe_boolean outOfBandOpts, EXIOptions* opts,
		size_t (*inputStream)(void* buf, size_t size, void* stream), DecoderData* decoder_data);


#endif /* DECODER_INC_EXIP_DECODER_H_ */


#endif

