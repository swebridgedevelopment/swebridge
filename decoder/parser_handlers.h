/*
 * parser_handler.h
 *
 *  Created on: Jul 25, 2016
 *      Author: enoc
 */

#ifndef INC_PARSER_HANDLERS_H_
#define INC_PARSER_HANDLERS_H_

#include "decoder/decoder.h"


#if DEBUG_PARSER_HANDLERS
// If debugging enabled, execute the instruction
#define PHDEBUG_INSTRUCTION(x)({x;})
#else
// If debugging is not enabled, define an empty macro
// the compiler will discard this instructions
#define PHDEBUG_INSTRUCTION(x)
#endif



//Identification handlers
int new_instrument(DecoderData* data);
int instrument_uid(DecoderData* data);
int instrument_attached_to(DecoderData* data);


//Parameters handlers
int parameter_list(DecoderData* data);
int process_parameter_interface_field(DecoderData* data);

int instrument_parameter_category_label(DecoderData* data);
int instrument_parameter_category_value(DecoderData* data);
int instrument_parameter_count_label(DecoderData* data);
int instrument_parameter_count_value(DecoderData* data);

//------------- New Mission-------------/
int new_mission(DecoderData* data);
int mission_uid(DecoderData* data);
int new_mission_process(DecoderData* data);
int mission_connection_source(DecoderData* data);
int mission_connection_destination(DecoderData* data);

//------------ Instrument Process Identifiers  ------------//
int new_instrument_process(DecoderData* data);
int new_mission_process(DecoderData* data);

int instrument_process_typeof(DecoderData* data);
int instrument_process_identifier(DecoderData* data);

// PhysicalSystem Identifiers //
int instrument_identifier_term(DecoderData* data);
int instrument_identifier_term_label(DecoderData* data);
int instrument_identifier_term_value(DecoderData* data);

// PhysicalSystem Classifiers //
int instrument_classifier_term(DecoderData* data);
int instrument_classifier_term_label(DecoderData* data);
int instrument_classifier_term_value(DecoderData* data);

// PhysicalSystem Parameters //
int instrument_parameter(DecoderData* data);
int instrument_capability(DecoderData* data);
int instrument_classifier_term_label(DecoderData* data);
int instrument_classifier_term_value(DecoderData* data);


//------------ Instrument Process Inputs  ------------//
int instrument_process_input(DecoderData* data);
int instrument_process_output(DecoderData* data);
int instrument_process_parameter(DecoderData* data);
int simple_process_settings_setvalue (DecoderData* data);


// Instrument parameters //
int instrument_parameter(DecoderData* data);
int instrument_parameter_data_interface(DecoderData* data);
int instrument_parameter_data_interface_field(DecoderData* data);


//--------  Identifiers --------//
int instrument_identifier_label(DecoderData* data);
int instrument_identifier_value(DecoderData* data);

//---- Position ----//
int instrument_position(DecoderData* data);
int instrument_position_axis(DecoderData* data);
int instrument_position_value(DecoderData* data);
int instrument_feature_of_interest(DecoderData* data);



//---------------- Instrument Parameters ----------------------//

int instrument_new_interface(DecoderData* data);
int instrument_new_interface_field(DecoderData* data);

//------------ Bridge Configuration ------------//
int new_bridge(DecoderData* data);
int bridge_identifer (DecoderData* data);
int bridge_connection_source (DecoderData* data);
int bridge_connection_destination (DecoderData* data);

//------------ Bridge Processes ------------//
int new_bridge_process (DecoderData* data);
int bridge_process_identifier (DecoderData* data);
int bridge_process_name (DecoderData* data);
int bridge_process_process_typeOf (DecoderData* data);
int bridge_process_configuration_setValue (DecoderData* data);

int new_data_stream (DecoderData* data);
int data_stream_field (DecoderData* data);

/* Basic Data Types */
int data_stream_element_type (DecoderData* data);

int generic_field_quantity (DecoderData* data);
int generic_field_count (DecoderData* data);
int generic_field_text (DecoderData* data);
int generic_field_boolean (DecoderData* data);
int generic_field_category (DecoderData* data);
int generic_field_time (DecoderData* data);

int generic_field_value (DecoderData* data);
int generic_field_label (DecoderData* data);
int generic_field_uom (DecoderData* data);



/* Complex Data Types */
int data_stream_new_data_record (DecoderData* data);
int data_stream_new_data_array(DecoderData* data);

/* Data Stream Fucntions */
int data_stream_encoding_new(DecoderData* data);
int data_stream_text_encoding(DecoderData* data);
int data_stream_text_encoding_start_token(DecoderData* data);
int data_stream_binary_encoding(DecoderData* data);
int data_stream_binary_encoding_component(DecoderData* data);
int data_stream_xml_encoding(DecoderData* data);
int data_stream_xml_encoding_component(DecoderData* data);


int data_stream_values(DecoderData* data);


/* Data Array Functions */
int data_stream_data_array_element_type(DecoderData* data);
int data_stream_data_array_element_count(DecoderData* data);
int data_stream_data_array_element_count_value(DecoderData* data);
int data_stream_array_encoding(DecoderData* data);






int data_stream_field_nilvalue (DecoderData* data);


/*
 * This Macro assigns the attribute "name" to the "name" of the "field" struct. The data SWE_Decoder_Data is used
 */
#define GET_ATTRIBUTE(field, name) do { \
		field->name = get_attribute(#name, data->current_element); \
	}while(0)


#endif /* INC_PARSER_HANDLERS_H_ */
