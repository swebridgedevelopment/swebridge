/*
 * puck_commands.c
 *
 *  Created on: Oct 6, 2016
 *      Author: enoc
 */

#include "puck/puck_commands.h"

#include "common/swe_utils.h"
#include "resources/resources.h"
#include "swe_conf.h"


#define DEBUG_PUCK_PROTOCOL OFF

#if DEBUG_PUCK_PROTOCOL
#define DBGM(x) x
#else
#define DBGM(x)
#endif


int puck_get_descriptor(Interface* iface, Puck_descriptor* descriptor, ulong payloadSize);
int set_descriptor(Puck_descriptor* descriptor, char* tagBuf) ;
int puck_send_bin_command(Interface* iface, uchar* command,  int commandlen,
		uchar* buffer, int lenbuffer, int expectedbytes, float timeout);

int puck_send_ascii_command(Interface* iface, char* command,  char* buffer, int lenbuffer, float timeout);

/*
 * Get the payload size
 */
int puck_get_payload_size(Interface* iface){
	char response[32];
	PUCK_TRY(puck_send_ascii_command(iface, PUCK_PAYLOAD_SIZE, response, sizeof(response), PUCK_TIMEOUT));
	int size=atoi(response);
	return size;
}

/*
 * Tries to get the PUCK prompt
 */
int puck_get_prompt(Interface* iface){
	char response[32];
	return puck_send_ascii_command(iface, PUCK_NULL_CMD, response, sizeof(response), PUCK_PROMPT_TIMEOUT);
}

/*
 * Erase PUCK memory
 */
int puck_erase_memory(Interface* iface){
	char response[32];
	dmsg("Erasing PUCK memory...");
	PUCK_TRY(puck_send_ascii_command(iface, PUCK_ERASE_MEMORY, response, sizeof(response), PUCK_EM_TIMEOUT));
	return swe_ok;
}


/*
 * Returns the PUCK type
 * 0 : Embedded PUCK, read-write datasheet
 * 1 : Embedded PUCK, read-only datasheet memory
 * 2 : PUCK hardware is external to the instrument
 */
int puck_get_type(Interface* iface, puck_type* type){
	char response[32];
	*type = puck_unknown_type;
	PUCK_TRY(puck_send_ascii_command(iface, PUCK_GET_TYPE,   response, sizeof(response),  PUCK_EM_TIMEOUT));
	if ( (!strcmp(response, "0000")) || (!strcmp(response, "000"))) {
		*type =  puck_read_write_datasheet;
	} else if(!strcmp(response, "0001")){
		*type = puck_read_only_datasheet;
	} else if(!strcmp(response, "0001")){
		*type =  puck_external_memory;
	}
	return swe_ok;
}


/*
 * Sends a softbreak to enter in PUCK mode
 */
int puck_send_softbreak(Interface* iface){
	iface_puts(iface,"@@@@@@");
	delayms(750);
	iface_puts(iface,"!!!!!!");
	delayms(500);
	return swe_ok;
}


/*
 * Sets Instrument mode
 */
int set_instrument_mode(Interface* iface){
	iface_fflush(iface);
	delayms(750);
	iface_puts(iface,PUCK_SET_INSTRUMENT_MODE);
	delayms(750);
	iface_fflush(iface);
	return swe_ok;
}


/*
 * Tries to enter in PUCK mode by sending softbreaks at different baudrates
 */
int set_puck_mode(Interface* iface, int tries){
	int i, j;
	static long baud[] = {9600,115200, 19200, 38400, 57600, 232400, 1200, 2400, 4800, -1};

	for(i=0; baud[i]!=-1; i++){
	  for(j=0; j<tries; j++) {
		  puck_send_softbreak(iface);
		  if(puck_get_prompt(iface) == swe_ok){
			  dmsg( "Entered in PUCK mode with baudrate %u", baud[i]);
			  return swe_ok;
		  }
		  dmsg( "couldn't get prompt...");
	  }
	  imsg( "Trying baudrate %d...", baud[i]);
	  iface_set_baudrate(iface, baud[i]);
	}
	errmsg( "couldn't set the puck mode...");
	return swe_puck_error;
}


/*
 * Sets the PUCK address to specific point
 */
int puck_set_address(Interface* iface, ulong address){
	  char command[32];
	  char response[32];

	  dmsg( "setting puck address to %u...", address);
	  sprintf(command, "%s %lu\r",PUCK_SET_ADDRESS, address);
	  dmsg( "sending cmd %s", command);

	  PUCK_TRY(puck_send_ascii_command(iface, command,  response, sizeof(response),  PUCK_TIMEOUT));
	  cimsg(GRN, "address set correctly");
	  return swe_ok;
}


/*
 * Reads the PUCK datasheet
 */
int puck_read_datasheet(Interface* iface, Puck_datasheet* datasheet){
	if(puck_set_address(iface,0)!=swe_ok){
		return swe_puck_error;
	}
	uchar buf[DATASHEET_SIZE +PUCKRM_CHARS_EXTRA + 1];
	int ret=puck_read_memory(iface,buf,DATASHEET_SIZE +PUCKRM_CHARS_EXTRA , DATASHEET_SIZE);
	if(ret!=swe_ok){
		errmsg( "error in puck_read_memory ");
	}
	memcpy(datasheet->dataSheetBytes, &buf[0], UUID_LEN);
	memcpy(datasheet->uuidMnem, &buf[UUID_OFFSET], UUID_LEN);
	memcpy(datasheet->versionMnem, &buf[DS_VERS_OFFSET], DS_VERS_LEN);
	memcpy(datasheet->sizeMnem, &buf[DS_SIZE_OFFSET], DS_SIZE_LEN);
	memcpy(datasheet->mfgIdMnem, &buf[MAN_ID_OFFSET], MAN_ID_LEN);
	memcpy(datasheet->mfgModelIdMnem, &buf[MAN_MODEL_OFFSET],MAN_MODEL_LEN);
	memcpy(datasheet->mfgVersionMnem, &buf[MAN_VERS_OFFSET],MAN_VERS_LEN);
	memcpy(datasheet->mfgSerialNoMnem, &buf[SER_NUM_OFFSET],SER_NUM_LEN);
	memcpy(datasheet->mfgInstmentMnem, &buf[NAME_OFFSET], NAME_LEN);
	return swe_ok;
}


/*
 * Reads a chunk of puck memory
 */
int puck_read_memory(Interface* iface, uchar* buffer, uint buflen, ulong readlen){
	char command[32];
	int read_bytes;
	uint expected_bytes = readlen + PUCKRM_CHARS_EXTRA;
	if(buflen<expected_bytes){
		errmsg( "puck_read_memory: buffer too small");
		return swe_buffer_overflow;
	}
	sprintf(command, "%s %lu\r",PUCK_READ_MEMORY, readlen);
	read_bytes = PUCK_TRY(puck_send_bin_command(iface, (uchar*)command, strlen(command),
			(uchar*)buffer, buflen, expected_bytes, PUCK_TIMEOUT));

	if (read_bytes != readlen + 2) {
		errmsg("%d bytes read, expected %d", read_bytes, readlen);
		return swe_puck_error;
	}
	// ignore [ ]
	if((char)buffer[0]!='[' ){
		warnmsg( "invalid tokens!");
	}
	memmove(buffer, buffer+1, readlen);
	return swe_ok;
}


/*
 * Gets the descriptor
 */
int puck_get_descriptor(Interface* iface, Puck_descriptor* descriptor, ulong payloadSize) {
	uint state = 0;
	uint targetIndex = 0;
	uint nTagBytes = 0;
	uint nBytes = 0;
	char target[32];
	char tagBytes[MAX_TAG_BYTES];
	int nRequested;

	// Read PUCK payload and write to output file
    int nRead = 0;

	char global_buff[PUCK_MAX_READ_BYTES + PUCKRM_CHARS_EXTRA];

    memcpy(target, PAYLOAD_TAG_PREFIX, OPEN_TAG_LEN);			/** Look for tag end ('>') */
    memset(tagBytes, 0, sizeof(tagBytes));
    dmsg( "Searching descriptor...");
    while (nRead < payloadSize) {
    	nRequested = MIN(PUCK_MAX_READ_BYTES, (payloadSize - nRead));
		PUCK_TRY(puck_read_memory(iface,(uchar*)global_buff, sizeof(global_buff), nRequested));
		global_buff[nRequested]=0;
		nBytes=0;
		while(nBytes < nRequested) {
			char b = global_buff[nBytes];
			if (state == IN_TAG) {
				if (nTagBytes >= MAX_TAG_BYTES) {
					errmsg( "tag not found!!");
					return swe_puck_error;
				}
				tagBytes[nTagBytes++] = b;
			}
			if (b == target[targetIndex]) {
				targetIndex++;
			}
			else {
			  targetIndex = 0;
			// Might be the *real* start of target
			  if (b == target[0]) {
			    targetIndex++;
			  }
			}
			if (targetIndex == OPEN_TAG_LEN && state == LOOKING_FOR_TAG){
				state = IN_TAG;
				nTagBytes = 0;
				memcpy(target,CLOSE_TAG,CLOSE_TAG_LEN);			/** Look for tag end ('>') */
				targetIndex = 0;
			}

			if (targetIndex == CLOSE_TAG_LEN && state == IN_TAG) {	/** Must have found tag end; print full tag */
				descriptor->tagOffset = nTagBytes + OPEN_TAG_LEN;
				dmsg( "found tag end...");
				set_descriptor(descriptor, tagBytes);
				dmsg( "complete tag");
				return swe_ok;
			}
			nBytes++;
		}

		int i;
		for(i=0; i<256; i++) {

		}
		nRead += nRequested;
    }
    return swe_puck_error;
}


/*
 * Sets the PUCK descriptor
 */
int set_descriptor(Puck_descriptor* descriptor, char* tagBuf) {
	  const char* separator=" ";
	  char* s;
	  uint num_sub_str=0;
	  int i = 0;
	  char** sub_strings;
	  s = (char*)swe_malloc(sizeof(char) * (strlen(tagBuf)+1));
	  strcpy(s, tagBuf);
	  sub_strings = get_substrings(s, (char*)separator, &num_sub_str );
	  if(sub_strings!=NULL){
		char buff[32];
		for(i = 0; i < num_sub_str; i++) {
			memset(buff, 0, 32);
			switch(i){
				case 0:
					extract_string(sub_strings[i], descriptor->type, '"');
					break;
				case 1:
					extract_string(sub_strings[i], descriptor->name, '"');
					break;
				case 2:
					extract_string(sub_strings[i], buff, '"');
					descriptor->size=atoi(buff);
					break;
				case 3:
					extract_string(sub_strings[i], descriptor->md5Checksum, '"');
					break;
				case 4:
					extract_string(sub_strings[i], buff, '"');
					descriptor->nextAddress=atoi(buff);
					break;
				case 5:
					//version field is optional
					if(extract_string(sub_strings[i], buff, '"')==swe_ok) {
						descriptor->version=atoi(buff);
					}
					break;
				case 6:
					//do nothing, just closing tag
					break;
				default:
					warnmsg( "More fields than expected in descriptor %d", i);
					break;
			}
		}
	}
	swe_free(sub_strings);
	swe_free(s);
	return swe_ok;
}




/*
 * Write the input buffer to PUCK puck memory
 */
int puck_write_to_memory(Interface* iface, uchar* buffer, int size){
	int bytes_written=0;

	imsg( "writing %d bytes to PUCK payload...", size);
	uchar command[WRITE_CHUNK_SIZE + 11];

	while(bytes_written < size) {
		char response[32];
		int command_size; // total command size
		int header_size; // length of the header (PUCKWM 32\r)
		int chunk_size; // length of the
		chunk_size =  MIN(WRITE_CHUNK_SIZE, (size-bytes_written));
		memset(command, 0, sizeof(command));
		header_size = sprintf((char*)command, "%s %d\r", PUCK_WRITE_MEMORY, chunk_size);
		command_size = header_size + chunk_size;
		memcpy(&command[header_size], &buffer[bytes_written], chunk_size);
		PUCK_TRY(puck_send_bin_command(iface, command, command_size, (uchar*)response, sizeof(response), 0, PUCK_TIMEOUT));
		bytes_written += chunk_size;
		dmsg( "writing %d of %d bytes", bytes_written, size);
	}

	return swe_ok;
}


/*
 * Displays information in a PUCK datasheet
 */
int puck_display_datasheet(Puck_datasheet* datasheet) {
	msg_sub_title(CYN, "PUCK Datasheet");
	imsg("UUID: 0x%x", datasheet->uuidMnem);
	imsg("Instrument Name: %s",	 datasheet->mfgInstmentMnem);
	cimsg(CYN, "=================================\n");
	return swe_ok;
}



/*
 * Wrapper to simplify sending ASCII commands
 */
int puck_send_ascii_command(Interface* iface, char* command,  char* buffer, int bufflen, float timeout) {
	return puck_send_bin_command(iface, (uchar*)command, strlen(command), (uchar*)buffer, bufflen, 0, timeout);
}


/*
 * Sends a PUCK command and waits for instrument to response ending with the PUCKRDY\r tag.
 * The function fills the buffer with the response, except the PUCKRDY\r (and last \r if existing)
 * i.e.
 * 		Tx [PUCKSZ\r]
 * 		Rx [124000\rPUCKRDY\r]
 * 	buffer = "124000"
 *
 * 	The number of bytes in the response is returned (after erasing PUCRDY token)
 *  If a PUCK error is returned, an errorcode is returned
 *
 *
 */
int puck_send_bin_command(Interface* iface, uchar* command,  int commandlen,
		uchar* buffer, int lenbuffer, int expectedbytes, float timeout) {
	int nbytes = 0; // received bytes
	float elapsed_time = 0.0;
	ulong timeout_step_ms = 1; //
	uchar reply_complete = FALSE;
	iface_fflush(iface);
	DBGM(fdmsg("        PUCK SEND [%s]", command));
	PUCK_TRY(iface_send(iface, command, commandlen)); // Send the command
	memset(buffer, 0, lenbuffer); // empty buffer
	nbytes = 0; // byte count
	while (!reply_complete) {
		int n;
		// read one byte without timeout //
		n = iface_recv(iface, (uchar*)&buffer[nbytes], 1, 0);
		while (n != 0) {
			nbytes++;

			if (nbytes >= expectedbytes) {
				// If the string in buffer is PUCKRDY\r exit success
				if ((nbytes == PUCK_END_TOKEN_LENGTH) && (!memcmp(buffer, PUCK_END_TOKEN, PUCK_END_TOKEN_LENGTH))) {
					buffer[0] = 0;
					nbytes = 0;
					reply_complete = TRUE;
					break;
				}
				// the response ends with PUCKRDY\r
				else if ( (nbytes > PUCK_END_TOKEN_LENGTH) &&
						(!memcmp(&buffer[nbytes - PUCK_END_TOKEN_LENGTH], PUCK_END_TOKEN, PUCK_END_TOKEN_LENGTH)) ) {
					if (buffer[nbytes - PUCK_END_TOKEN_LENGTH -1] == '\r') { // if there's a previous \r erase it
						buffer[nbytes - PUCK_END_TOKEN_LENGTH -1] = 0;
						nbytes -=  (PUCK_END_TOKEN_LENGTH + 1);

					} else {
						buffer[nbytes - PUCK_END_TOKEN_LENGTH] = 0;
						nbytes -= PUCK_END_TOKEN_LENGTH;
					}
					reply_complete = TRUE;
					break;
				}
			}
			n = iface_recv(iface, (uchar*)&buffer[nbytes], 1, 0); // read without timeout
			// check if buffer is full //
			if (nbytes >= lenbuffer) {
				errmsg("PUCK response buffer overflow (%d bytes in buffer of length %d)", nbytes, lenbuffer);
				return swe_buffer_overflow;
			}
		}
		// no more bytes to read, wait
		if ( n == 0 ) {
			delayms(timeout_step_ms);
			elapsed_time += ((float)timeout_step_ms) / 1000;
			if (elapsed_time >= timeout ) {
				DBGM(cimsg(YEL,"        PUCK timeout %f seconds (%d bytes in buffer)", elapsed_time, nbytes));
				return swe_timeout_error;
			}
		}
	}
	DBGM(fdmsg( "        PUCK RECV [%s]", buffer));
	DBGM(cimsg(YEL,"        PUCK reply took %f seconds", elapsed_time));
	// Check PUCK error codes //
	if ( (nbytes >= 8) && (!memcmp("ERR", buffer, 3)) ){
		if (!memcmp("ERR 0004", buffer, 8)) {
			errmsg("PUCK ERR 0004: No command match");
		} else if (!memcmp("ERR 0010", buffer, 8)) {
			errmsg("PUCK ERR 0010: Invalid baud rate requested");
		} else if (!memcmp("ERR 0020", buffer, 8)) {
			errmsg("PUCK ERR 0020: Bytes requested for read or write out of range");
		} else if (!memcmp("ERR 0021", buffer, 8)) {
			errmsg("PUCK ERR 0021: Address requested out of range");
		} else if (!memcmp("ERR 0022", buffer, 8)) {
			errmsg("PUCK ERR 0022: Write attempted to read only memory");
		} else if (!memcmp("ERR 0023", buffer, 8)) {
			errmsg("PUCK ERR 0023: Write attempted to non-initialized memory");
		}else {
			errmsg("Unknown PUCK error");
		}
		return swe_puck_error;
	}
	return nbytes;
}

