/*
 * puck_commands.h
 *
 *  Created on: Oct 6, 2016
 *      Author: enoc
 */

#ifndef PUCK_COMMANDS_H_
#define PUCK_COMMANDS_H_

#include "puck/puck.h"
#include "swe_conf.h"


int puck_get_payload_size(Interface* iface);
int puck_get_prompt(Interface* iface);
int puck_send_softbreak(Interface* iface );
int puck_set_address(Interface* iface, ulong address);
int set_puck_mode(Interface* iface, int tries);
int set_instrument_mode(Interface* iface );
int puck_read_datasheet(Interface* iface, Puck_datasheet* datasheet);
int puck_read_memory(Interface* iface, uchar* buffer, uint buflen, ulong readlen);
int puck_get_descriptor(Interface* iface, Puck_descriptor* descriptor, ulong payloadSize);
int puck_write_to_memory(Interface* iface, uchar* buffer, int size);





int puck_erase_memory(Interface* iface );
int puck_get_type(Interface* iface, puck_type* type);
int puck_generate_tag();

int puck_display_datasheet(Puck_datasheet* datasheet);


#define PUCK_TRY(func) ({ \
	int __temp_error = func;\
	if (__temp_error < 0) { \
		errmsg("Errorcode %d: \"%s\" at %s, line %d",__temp_error,  swe_error_strings[abs(__temp_error)], __FILE__, __LINE__);\
		return __temp_error; \
	}\
	__temp_error; \
})

#endif /* PUCK_COMMANDS_H_ */
