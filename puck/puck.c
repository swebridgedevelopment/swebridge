/*
 * puck.c
 *
 *  Created on: Oct 7, 2016
 *      Author: enoc
 */

#include "puck/puck.h"
#include "common/swe_utils.h"
#include "puck/puck_commands.h"
#include "resources/resources.h"
#include "swe_conf.h"

int enter_puck(Interface* iface);

/*
 * Tries to get a SensorML file from a PUCK sensor (with iface)
 */
int puck_retrieve_sml_file(Interface* iface, char* filename){
	int payload_size;
	Puck_descriptor descriptor;
	Puck_datasheet datasheet;
	swe_file* output_file;
	msg_sub_title( BLU, "Start PUCK Protocol");
	if(enter_puck(iface)!=swe_ok){
		return swe_puck_error;
	}
	//Get payload size

	payload_size = PUCK_TRY(puck_get_payload_size(iface));
	imsg( "Payload has %d bytes", payload_size);
	cdmsg( BLU, "Reading PUCK datasheet...");
	memset(&datasheet, 0, sizeof(datasheet));
	PUCK_TRY(puck_read_datasheet(iface, &datasheet));
	puck_display_datasheet(&datasheet);
	PUCK_TRY(puck_get_descriptor(iface, &descriptor, payload_size));


	cimsg( GRN, "Opening input file %s...", descriptor.name);
	if ((output_file = swe_fopen(descriptor.name, swe_write_file)) == NULL) {
		errmsg( "Couldn't open input file \"%s\"\n", descriptor.name);
		return swe_filesystem_error;
	}

	uchar file_buffer[PUCK_MAX_READ_BYTES + PUCKRM_CHARS_EXTRA];

	//Read PUCK payload and write to output file
	PUCK_TRY(puck_set_address(iface, DATASHEET_SIZE + descriptor.tagOffset));

	cimsg( GRN,"Retrieveing file \"%s\" with size %d...", descriptor.name, descriptor.size);
	int nread=0;
	while (nread < descriptor.size) {
		int nRequested = MIN(PUCK_MAX_READ_BYTES, (descriptor.size - nread));
		PUCK_TRY(puck_read_memory(iface, file_buffer, sizeof(file_buffer),nRequested));
		nread += nRequested;
		dmsg( "retrieved %d of %d bytes...", nread , descriptor.size);
		swe_fwrite(file_buffer, nRequested*sizeof(uchar), output_file);
	}
	imsg( "completed file retrieval");
	swe_fclose(output_file);
	dmsg( "copying filename %s", descriptor.name);
	strcpy(filename, descriptor.name);
	set_instrument_mode(iface);
	cimsg( GRN, "PUCK SensorML retrieved successfully\n");
	return swe_ok;
}



/*
 * This function writes takes a file and writes it to the PUCK memory.
 *
 * STEP 1: Enter PUCK mode
 * STEP 2: Read the PUCK datasheet and store it
 * STEP 3: Erase the PUCK memory
 * STEP 4: Re-write the PUCK datasheet (if required9
 * STEP 5: Generate the file-tag
 * STEP 6: Write the file to the PUCK payload
 *
 */
const char* allowed_tags[] = {"SWE-SensorML", "SWE-SensorML-xml", "SWE-SensorML-exi", NULL};

int puck_write_file_to_memory(Interface* iface, char* filename, const char* tag_type){
	char md5_code[50];
	char tag_buffer[300];
	char token='"';
	int payload_size;
	int i;
	uchar valid_tag=FALSE;
	Puck_datasheet datasheet;
	puck_type pucktype;
	// check if tag is valid //
	for(i=0; allowed_tags[i]!=NULL; i++){
		if(!compare_strings(tag_type, (char*) allowed_tags[i])){
			valid_tag=TRUE;
		}
	}
	if(!valid_tag){
		errmsg( "Tag \"%s\"is not valid", tag_type);
		dmsgn( "possible tag values:");
		for(i=0; allowed_tags[i]!=NULL; i++){
			dmsgn( " \"%s\"", allowed_tags[i]);
		}
		dmsgn( "\n");
		return swe_invalid_arguments;
	}

	// Open the file we are going to write
	swe_file* file=swe_fopen(filename, swe_read_file);
	int file_size=swe_file_size(file);
	dmsg( "file %s size %d", filename, file_size);


	// STEP 1: Enter PUCK mode
	PUCK_TRY(enter_puck(iface));

	// STEP 2: Read the PUCK datasheet and store it
	memset(&datasheet, 0, DATASHEET_SIZE + PUCKRM_CHARS_EXTRA);
	payload_size = PUCK_TRY(puck_get_payload_size(iface));

	if (payload_size < file_size) {
		errmsg("PUCK payload is too small!, file: %d bytes, payload: %d bytes", file_size, payload_size);
		swe_fclose(file);
		return swe_invalid_arguments;
	}


	PUCK_TRY(puck_set_address(iface, 0));
	PUCK_TRY(puck_read_datasheet(iface, &datasheet));
	puck_display_datasheet(&datasheet);
	PUCK_TRY(puck_get_type(iface, &pucktype));

	if (pucktype == puck_read_write_datasheet) {
		imsg("PUCK type: read-write");
	}
	else if (pucktype == puck_read_only_datasheet) {
		imsg("PUCK type: read-only datasheet");
	}
	else if (pucktype == puck_external_memory) {
		imsg("PUCK type: external memory (assuming read-write)");

	} else { // puck_unknown_type
		errmsg("Unknown PUCK type");
		return swe_puck_error;

	}
	// STEP 3: Erase the PUCK memory
	imsg( "Erasing PUCK memory...");
	PUCK_TRY(puck_erase_memory(iface));

	// STEP 4: Re-write the PUCK datasheet (if required)

	if (pucktype == puck_read_only_datasheet) {
		// datasheet was not erased, so go to address 96
		dmsg("Setting address to beginning of payload...");
		puck_set_address(iface, DATASHEET_SIZE);
	}
	else {
		// datasheet was erased, write again datasheet
		dmsg("re-writing datasheet...");
		puck_set_address(iface, 0);
		PUCK_TRY(puck_write_to_memory(iface, (uchar*)&datasheet, DATASHEET_SIZE));
	}


	// STEP 5: Generate the file-tag
	generate_md5_code(filename, md5_code, 50);
	dmsg( "md5 code %s len %d", md5_code, strlen(md5_code));


	sprintf(tag_buffer, "<puck_payload type=%c%s%c", token, tag_type, token);
	sprintf(tag_buffer, "%s name=%c%s%c", tag_buffer, token, filename, token);
	sprintf(tag_buffer, "%s size=%c%d%c", tag_buffer, token, file_size, token);
	sprintf(tag_buffer, "%s md5=%c%s%c", tag_buffer, token, md5_code, token);
	sprintf(tag_buffer, "%s next_addr=%c%d%c/>", tag_buffer, token, -1, token);
	dmsg( "tag : %s", tag_buffer);
	PUCK_TRY(puck_write_to_memory(iface, (uchar*)tag_buffer, strlen(tag_buffer)));

	// STEP 6: Write the file to the PUCK payload
	char file_buffer[file_size];

	if(file==NULL) return swe_filesystem_error;
	if(swe_fread(file_buffer, file_size*sizeof(char), file)<file_size){
		errmsg( "couldn't read file");
	}
	PUCK_TRY(puck_write_to_memory(iface, (uchar*)file_buffer, file_size));
	swe_fclose(file);
	cimsg(GRN, "File writeen successfully in PUCK memory!");
	return swe_ok;
}

/*
 * Tries to enter in PUCK mode
 */
int enter_puck(Interface* iface){
	dmsg( "Trying to get PUCK prompt...");
	if(puck_get_prompt(iface) != swe_ok){
		dmsg( "prompt failed, trying to enter in PUCK mode");
		imsg( "entering puck mode...");
		if(set_puck_mode(iface,3)!=swe_ok){
			errmsg( "Couldn't set PUCK mode, exit");
			return swe_puck_error;
		}
	} else{
		imsg( "Already in PUCK mode");
	}
	return swe_ok;
}
