/*
 * puck.h
 *
 *  Created on: Oct 7, 2016
 *      Author: enoc
 */

#ifndef PUCK_H_
#define PUCK_H_

#include "resources/resources.h"


#define PUCK_DEFAULT_BAUDRATE 9600


//---- PUCK Commands ----//
#define PUCK_NULL_CMD "PUCK\r"
#define PUCK_PAYLOAD_SIZE "PUCKSZ\r"
#define PUCK_SET_ADDRESS "PUCKSA"
#define PUCK_READ_MEMORY "PUCKRM"
#define PUCK_SET_INSTRUMENT_MODE "PUCKIM\r"
#define PUCK_WRITE_MEMORY "PUCKWM"
#define PUCK_ERASE_MEMORY "PUCKEM\r"
#define PUCK_GET_TYPE "PUCKTY\r"

//PUck response end token
#define PUCK_END_TOKEN "PUCKRDY\r"
#define PUCK_END_TOKEN_LENGTH 8

//----- TAG Variables -----//
#define MAX_TAG_BYTES		256
#define PAYLOAD_TAG_PREFIX  	"<puck_payload "
#define OPEN_TAG_LEN		14
#define CLOSE_TAG_LEN  	2
#define CLOSE_TAG  	"/>"
#define DESCRIPTOR_TYPE_SENSORML "SWE-SensorML"
#define LOOKING_FOR_TAG 	0
#define IN_TAG		1
#define MAX_TAG_BYTES		256
#define PUCK_MAX_READ_BYTES 	256 //1024

#define WRITE_CHUNK_SIZE 32

//-------- PUCK Timeouts ---------//
#define PUCK_TIMEOUT 0.5 // default timeout in seconds
#define PUCK_PROMPT_TIMEOUT 0.1 // timeout in seconds for null cmd

#define PUCK_FM_TIMEOUT 30 // seconds
#define PUCK_EM_TIMEOUT 120 // seconds, the standard specifies 30, but let's go the safer way...


//---------- Datasheet Variables --------//
#define UUID_LEN  			16
#define UUID_OFFSET  		0

#define DS_VERS_LEN 		2
#define DS_VERS_OFFSET 	UUID_LEN + UUID_OFFSET

#define DS_SIZE_LEN 		2
#define DS_SIZE_OFFSET 	DS_VERS_LEN + DS_VERS_OFFSET

#define MAN_ID_LEN 		4
#define MAN_ID_OFFSET 		DS_SIZE_LEN + DS_SIZE_OFFSET

#define MAN_MODEL_LEN  	2
#define MAN_MODEL_OFFSET 	MAN_ID_LEN + MAN_ID_OFFSET

#define MAN_VERS_LEN 		2
#define MAN_VERS_OFFSET 	MAN_MODEL_LEN + MAN_MODEL_OFFSET

#define SER_NUM_LEN 		4
#define SER_NUM_OFFSET 	MAN_VERS_LEN + MAN_VERS_OFFSET

#define NAME_LEN 			64
#define NAME_OFFSET 		SER_NUM_LEN + SER_NUM_OFFSET

	/** instrument datasheet size in bytes */
#define DATASHEET_SIZE 	UUID_LEN + DS_VERS_LEN +DS_SIZE_LEN +MAN_ID_LEN +MAN_MODEL_LEN +MAN_VERS_LEN +SER_NUM_LEN +NAME_LEN

#define PUCKRM_CHARS_EXTRA 10

typedef struct{
	uchar uuidMnem[UUID_LEN];
	uchar versionMnem[DS_VERS_LEN];
	uchar sizeMnem[DS_SIZE_LEN];
	uchar mfgIdMnem[MAN_ID_LEN];
	uchar mfgModelIdMnem[MAN_MODEL_LEN];
	uchar mfgVersionMnem[MAN_VERS_LEN];
	uchar mfgSerialNoMnem[SER_NUM_LEN];
	uchar mfgInstmentMnem[NAME_LEN];
	//buffer
	uchar dataSheetBytes[DATASHEET_SIZE + PUCKRM_CHARS_EXTRA +1];

}Puck_datasheet;

typedef struct{
	char type[NAME_LEN]; 			/** Type can be "SENSORML", "TEDS", etc. */
	char name[NAME_LEN];			/** Name of payload instance, for example a filename */
	uint tagOffset;		/** Offset in bytes within PUCK */
	uint size;			/** Size of payload in bytes, not including opening and closing tags */
	char md5Checksum[NAME_LEN];	/** MD5 checksum of payload */
	int nextAddress;
	uint version;/** Total number of "files" in payload */
}Puck_descriptor;

/*
 * Defines the PUCK type flags
 */
typedef enum{
	puck_unknown_type = -1,
	puck_read_write_datasheet = 0,
	puck_read_only_datasheet = 1,
	puck_external_memory = 2
}puck_type;




int puck_retrieve_sml_file(Interface* iface ,char* filename);
int puck_write_file_to_memory(Interface* iface, char* filename, const char* tag_type);

#endif /* PUCK_H_ */
