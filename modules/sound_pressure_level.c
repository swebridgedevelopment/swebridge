/*
 * This module implements Sound Pressure level calculations
 */

#include "swe_conf.h"
#if ENABLE_HIGH_FREQUENCY_MODULES

#include "core/simple_process.h"
#include "modules/sound_pressure_level.h"
#include "resources/fft.h"
#include <math.h>
#include <time.h>



static const ExecutionModes spl_default_exec= {
										.previous_process=TRUE,
										.scheduler=FALSE,
										.iface_interrupt=FALSE,
										.initialization=FALSE};


BandFrequency* generate_band_frequency(double fcenter);
int bytes_to_float64(HighFreqStreamBuffer* streambuff, float64* outbuff);
float64 detrend_data(float64* data, uint len);
int samples_to_presure(float64* data, uint len, float64 coefficient);
int initialize_stream(SoundPressureLevelData* params, HighFreqStreamBuffer *streambuff );
int accumulate_incoming_samples(SoundPressureLevelData* params, HighFreqStreamBuffer* incoming);

// Power Spectrum Accumulator //
int psd_accumulate(SoundPressureLevelData* params);
int psd_normalize(PsdAccumulator* psd);
int psd_init(PsdAccumulator* psd, uint nfreq, uint ntarget);
int psd_reset(PsdAccumulator* psd);

int process_accumulated_samples(SoundPressureLevelData* params);
float64 spl_band_frequency(SoundPressureLevelData* params, BandFrequency* band);
float64 spl_fullband_frequency(SoundPressureLevelData* params);
int calculate_spl_values(SoundPressureLevelData* params);
SWE_Data* create_spl_field(const char* paramname, const char* bandname);
SchedulerData* create_spl_module_response(SoundPressureLevelData* params);
int reset_spl_buffers(SoundPressureLevelData* params);


/*
 * SoundPressureLevel Module Constructor
 */
int sound_pressure_level_constructor(SimpleProcess* conf, SchedulerProcess* process){
	int i;
	uint nbands;
	char** splitted_frequencies;
	char frequency_bands_list[2048] = "";
	SoundPressureLevelData* params;
	StateRegister* state_reg = &process->state_reg;
	state_reg->sampling_rate = -1.0;

	msg_sub_title( GRN, "Sound Pressure Level Constructor");

	dmsg( "allocating space for internal data...");
	params=swe_malloc(sizeof(SoundPressureLevelData));
	process->internal_data=(void*)params;
	params->initialized = FALSE;

	dmsg( "assigning execution pointer...");
	process->exec = sound_pressure_level_exec;
	process->flush = sound_pressure_level_flush;

	dmsg( "setting the default execution modes...");
	memcpy(&process->state_reg.exec_modes, &spl_default_exec, sizeof(ExecutionModes));


	// Set default values //
	params->fft_time = SPL_DEFAULT_FFT_TIME;
	params->integration_time = SPL_DEFAULT_INTEGRATION_TIME;
	params->sel = FALSE;
	params->rms = FALSE;
	params->use_hann_window = FALSE;

	dmsg("Adding settings");
	process->settings = add_settings_to_process(process->settings, SWE_Text, "parameters/frequencyBands", (void*)(&frequency_bands_list));
	process->settings = add_settings_to_process(process->settings, SWE_Boolean, "parameters/soundExposureLevel", (void*)(&params->sel));
	process->settings = add_settings_to_process(process->settings, SWE_Boolean, "parameters/sel", (void*)(&params->sel));
	process->settings = add_settings_to_process(process->settings, SWE_Boolean, "parameters/rootMeanSquare", (void*)(&params->rms));
	process->settings = add_settings_to_process(process->settings, SWE_Boolean, "parameters/rms", (void*)(&params->rms));
	process->settings = add_settings_to_process(process->settings, SWE_Boolean, "parameters/window", (void*)(&params->use_hann_window));
	process->settings = add_settings_to_process(process->settings, SWE_Quantity, "parameters/fftTime", (void*)(&params->fft_time));
	process->settings = add_settings_to_process(process->settings, SWE_Quantity, "parameters/integrationTime", (void*)(&params->integration_time));

	if(configure_process_settings(conf->settings, process)!=swe_ok) {
		errmsg( "configuration returned errors");
		return swe_invalid_arguments;
	}

	// Welch's method overlapping
	if ( params->use_hann_window == FALSE ) {
		params->fft_overlap = 0;
	}
	else {
		params->fft_overlap = 0.5;
	}


	// Calculate the number of power spectrums (FFTs) to accumulate //
	params->psd.ntarget = (uint) (params->integration_time / ((1 - params->fft_overlap)*params->fft_time)- params->fft_overlap);

	dmsg("PS to accumulate %u", params->psd.ntarget);
	dmsg("integration_time %f", params->integration_time);
	dmsg("Overlapping %f", params->fft_overlap);

	// Generate band frequencies according to frequencyBand parameter
	if ( frequency_bands_list[0] !=  0 ) {
		splitted_frequencies = get_substrings(frequency_bands_list, " ", &nbands);
		for (i=0; i<nbands ; i++){
			// If full bandwidth
			if ( !strcmp(splitted_frequencies[i], "full") ) {
				BandFrequency*  band = generate_band_frequency(USE_FULL_BANDWIDTH);
				imsg("Band freq full bandwidth");
				params->band_frequencies = ATTACH_TO_ARRAY(params->band_frequencies, band);
			}
			else {
				double fcenter = strtod(splitted_frequencies[i], NULL);
				if ( fcenter < 0.1 ) {
					errmsg("Frequency \"%s\" not valid (can't convert to double)", splitted_frequencies[i]);
					return swe_invalid_arguments;
				} else {
					BandFrequency*  band = generate_band_frequency(fcenter);
					imsg("Band freq %.2f Hz (from %.2f Hz to %.2f Hz)", band->fcenter, band->flow, band->fhigh);
					params->band_frequencies = ATTACH_TO_ARRAY(params->band_frequencies, band);

				}
			}
		}
	}
	else {
		warnmsg("No band frequencies configured");
		params->band_frequencies = swe_malloc(sizeof(BandFrequency**));
		params->band_frequencies[0] = NULL;
	}

	// Check that FFT time and Integration time are properly set
	if (params->fft_time < 0 )  {
		errmsg("FFT time not valid");

	}
	if( params->integration_time < 0 || params->fft_time > params->fft_time) {
		errmsg("Integration time not valid");
		return swe_invalid_arguments;
	}

	if ( params->fft_overlap < 0 || params->fft_overlap > 0.95 ) {
		errmsg("Overlapping must be between 0 and 0.95");
		return swe_invalid_arguments;
	}

	// Generate output
	params->data = TRY_NULL(create_spl_module_response(params));

	process->state_reg.exec_flag = 0;
	dmsgn( "\n==================================================\n\n");
	return swe_ok;
}



/*
 * Calculates the Sound Pressure Level at different frequency bands bands
 *
 */
SchedulerData* sound_pressure_level_exec(void* process_pointer, SchedulerData* datain, ProcessStatus *errorcode){
	SchedulerProcess* process=(SchedulerProcess*)process_pointer;
	SoundPressureLevelData* params=(SoundPressureLevelData*)process->internal_data;
	StateRegister* state_register = &process->state_reg;
	HighFreqStreamBuffer *incoming = datain->streambuff;
	uchar exec_flag = state_register->exec_flag;
	state_register->exec_flag = 0; // clear the flag, a local copy is still available

	*errorcode = process_chain_error;

	if (exec_flag & PREVIOUS_PROCESS_TRIGGER) {
		if ( params->initialized == FALSE ) {
			TRY_RET_NULL(initialize_stream(params, incoming));
		}
		SampleBuffer* buffer = params->buffer;
		PsdAccumulator* psd = &params->psd;
		// Process and accumulate samples //
		if ( accumulate_incoming_samples(params, incoming) < 0 ) {
			errmsg("Could not accumulate incoming samples, reset buffers...");
			reset_spl_buffers(params);
			return NULL;
		}
		// If we have enough samples, calculate power spectrum
		if ( buffer->nsamples >= params->accumulate_samples ) {
			if ( process_accumulated_samples(params) < 0 ) {
				errmsg("Could not process samples, reset buffers...");
				reset_spl_buffers(params);
				return NULL;
			}
			if ( psd->n >= params->psd.ntarget ) {
				// If we have accumulated enough Power Spectrums for the Welch's method, compute
				// the overall SPL values
				TRY_RET_NULL(calculate_spl_values(params));
				psd_reset(psd); // Clear the ps accumulator
				sample_buffer_reset(params->buffer); // Clear the sample buffer
				*errorcode = process_chain_continue;
			}
			else {
				// If we did not accumulate all the required power spectrums, overlap the input buffer
				TRY_RET_NULL(sample_buffer_overlap(params->buffer, params->sampling_rate, params->fft_overlap));
				*errorcode = process_chain_stop;
			}
		}
		else {
			*errorcode = process_chain_stop;
		}
	}

	return params->data;
}


/*
 * Flush handler
 */
int sound_pressure_level_flush(void* process_pointer){
	SchedulerProcess* process=(SchedulerProcess*)process_pointer;
	SoundPressureLevelData* params=(SoundPressureLevelData*)process->internal_data;
	imsg("SPL - Flush received, reset all buffers");
	reset_spl_buffers(params);
	return swe_ok;
}


/*
 * Flush handler
 */
int sound_pressure_level_error(void* process_pointer){
	SchedulerProcess* process=(SchedulerProcess*)process_pointer;
	SoundPressureLevelData* params=(SoundPressureLevelData*)process->internal_data;
	imsg("SPL - Error received, reset all buffers");
	reset_spl_buffers(params);
	return swe_ok;
}


/*
 * Resets all buffer in spl module
 */
int reset_spl_buffers(SoundPressureLevelData* params){
	if (params) {
		sample_buffer_reset(params->leftovers);
		warnmsg("reset leftovers");
		sample_buffer_reset(params->buffer);
		warnmsg("reset psd");
		psd_reset(&params->psd);
	}
	return swe_ok;
}


//---------------------------------------------------------------------------//
//------------------------- Sample Accumulator-------------------------------//
//---------------------------------------------------------------------------//
/*
 * Convert the samples to pressure and accumulate them into the SPL Module buffer.
 * If the incmoing samples do not fit into the buffer store them as "leftovers" for
 * the next iteration of the processing.
 */
int accumulate_incoming_samples(SoundPressureLevelData* params, HighFreqStreamBuffer* incoming){
	SampleBuffer* leftovers = params->leftovers;
	SampleBuffer* buffer = params->buffer;
	float64 pressure[incoming->nsamples]; // Temporal buffer to store incoming pressure samples
	float64 timestamp;

	// Calculate incoming data timestamp
	TRY_RET(calculate_sample_timestamp(incoming, 0, &timestamp));
	// Convert samples to pressure //
	TRY_RET(bytes_to_float64(incoming, pressure)); // Convert binary input to float64 samples
	// Convert from samples to pressure in µPa//
	TRY_RET(samples_to_presure(pressure, incoming->nsamples, incoming->conversion_coefficient)); // Convert to from raw data to pressure

	// First check if there are some samples left to process
	if ( leftovers->nsamples > 0 ) {
		// Copy from leftovers to buffer
		TRY_RET(sample_buffer_to_buffer(leftovers, buffer));

	}
	dmsg("SPL - Currently there are %u samples in buffer, received %u", buffer->nsamples, incoming->nsamples);

	// If all the incoming samples fit in buffer, copy them
	if ( buffer->nleft >= incoming->nsamples ) {
		TRY_RET(sample_buffer_put(buffer, pressure, incoming->nsamples, timestamp));
	}
	// If not all the samples fit in buffer copy some of them and put the rest on leftovers
	else {
		// Copy only the remaining bytes in buffer
		float64 leftovers_timestamp;
		uint ncopy = buffer->nleft; // number of bytes to copy to buffer
		uint nrest = incoming->nsamples - ncopy; // number of samples to put on leftovers
		dmsg("SPL - Copying only %d (%d put on leftovers)",  ncopy, nrest);
		// Copy to buffer //
		TRY_RET(sample_buffer_put(buffer, pressure,  ncopy, timestamp));
		// calculate the timestamp for the nth sample
		TRY_RET(calculate_sample_timestamp(incoming, ncopy, &leftovers_timestamp));
		// Put the rest on leftovers
		TRY_RET(sample_buffer_put(leftovers, &pressure[ncopy],  nrest, leftovers_timestamp));
	}
	return swe_ok;
}


/*
 * Processes all the samples in buffer by:
 * Remove offset (detrend data)
 * Calculate FFT
 * Calculate PSD Power Spectrum and accumulate into the power spectrum accumultar (for welch method)
 */
int process_accumulated_samples(SoundPressureLevelData* params){
	SampleBuffer* buffer = params->buffer;
	if ( buffer->nsamples != buffer->size ) {
		errmsg("Buffer is not full!");
		return swe_invalid_arguments;
	}
	uint length = buffer->size;
	float64 pressure[length];

	memcpy(pressure, buffer->samples, sizeof(float64)*length);
	detrend_data(pressure, length); // Remove the offset

	// Calculate the FFT //
	TRY_RET(rfft_calculate(params->fft_params, pressure, params->fft_params->abs_fft_out));

	// Compute the power spectrum and store and accumulate it
	TRY_RET(psd_accumulate(params));

	return swe_ok;
}


/*
 * Initializes the SPL module with the information of the incoming stream
 */
int initialize_stream(SoundPressureLevelData* params, HighFreqStreamBuffer *incoming ){

	if (incoming == NULL || params == NULL ) {
		return swe_invalid_arguments;
	}
	// Copy info from incoming buffer
	params->sampling_rate = incoming->sampling_rate;

	// Create a sample buffer for incoming samples
	params->accumulate_samples = round(params->fft_time * params->sampling_rate);
	params->buffer = sample_buffer_create(params->accumulate_samples);

	imsg("SPL - Sampling rate %f Hz", params->sampling_rate);
	imsg("SPL - Accumulating %d samples for each FFT", params->accumulate_samples);
	imsg("SPL - FFT time %.2f seconds (%d samples)", params->fft_time, params->accumulate_samples);
	imsg("SPL - FFT overlap %f ", params->fft_overlap);
	imsg("SPL - SPL time %f", params->integration_time);
	imsg("SPL - Allocating buffer for %u float64 elements", params->accumulate_samples);
	if ( params->use_hann_window ) {
		imsg("SPL - Using hann window");
	} else {
		imsg("SPL - Windowless FFT (boxcar)");
	}

	imsg("SPL - Setup FFT...");

	params->fft_params = rfft_setup(params->sampling_rate, params->accumulate_samples, params->use_hann_window);

	imsg("SPL - FFT has a resolution of %0.2f Hz", params->sampling_rate / ((float64)params->buffer->size));

	// Configure leftovers
	params->leftovers = sample_buffer_create(2*incoming->nsamples);

	// Configure the PSD
	uint accumulate_psd = (uint) (params->integration_time / ((1 - params->fft_overlap)*params->fft_time)- params->fft_overlap);
	TRY_RET(psd_init(&params->psd, params->fft_params->nfreq, accumulate_psd));

	params->initialized = TRUE;
	return swe_ok;
}


/*
 * This functions calculates the average of the array data and then this average is substracted
 * from each element. The average is returned. Offset is the point where to start the detrend
 * operation, to avoid detrending twice
 */
float64 detrend_data(float64* data, uint len) {
	float64 accumulator = 0.0;
	float64 average;
	int i;
	for ( i=0 ; i<len ; i++ ) {
		accumulator += data[i];
	}
	average = accumulator / (len);

	for ( i=0 ; i<len ; i++ ) {
		data[i] -= average;
	}
	return average;
}


/*
 * Converts from samples to pressure
 */
int samples_to_presure(float64* data, uint len, float64 coefficient) {
	int i;
	for ( i=0 ; i<len ; i++ ) {
		data[i] = data[i]*coefficient;
	}
	return swe_ok;
}


/*
 * Converts an array of bytes into an array of float64
 */
int bytes_to_float64(HighFreqStreamBuffer* streambuff, float64* outbuff){
	uchar* point = streambuff->data;
	int i;

	if ( streambuff == NULL || outbuff == NULL ){
		return swe_invalid_arguments;
	}

	for ( i=0 ; i<streambuff->nsamples ; i++ ) {
		outbuff[i] = convert_to_float64(point, streambuff->encoding);
		point += streambuff->sample_size;
	}
	return swe_ok;
}


//---------------------------------------------------------------------------//
//--------------------- Power Spectral Density Accumulator ------------------//
//---------------------------------------------------------------------------//

/*
 * Take the result of the FFT and calculate the power spectral density. Accumulate each term
 * of the power spectrum in the power spectrum accumulator.
 */
int psd_accumulate(SoundPressureLevelData* params){
	int i;
	PsdAccumulator* psd = &params->psd;
	FFTparams* fft = params->fft_params;

	if ( psd->nfreq != fft->nfreq){
		errmsg("FFT and PSD lengths do not match");
		return swe_error;
	}

	float64 psd_denominator;
	if ( fft->window != NULL ){
		// If windowed, the PSD denominator is the sum of the squared terms of the window function
		psd_denominator = fft->window->s2;
	}
	else {
		// If windowless, the PSD denominator is the number of points in the FFT
		psd_denominator = (float64)fft->n;
	}

	for ( i=0 ; i<psd->nfreq ; i++ ) {
		float64 psd_term; // Power spectrum term
		psd_term = pow(params->fft_params->abs_fft_out[i], 2); // Convert to power

		// Multiply by 2 to take into account symmetry (negative part not calculated in real fft)
		psd->accumulator[i] += psd_term / psd_denominator  ; // accummulator
	}

	if (psd->n == 0 ) {
		// The first timestamp in buffer
		params->psd.init_timestamp = params->buffer->timestamp;
	}
	psd->n += 1; // Increment the number of accumulated power spectrums
	psd->end_timestamp = params->buffer->timestamp + ((float64)params->buffer->nsamples)/params->sampling_rate;
	char init_date[256];
	ascii_datetime(init_date, params->psd.init_timestamp, TRUE);
	cdmsg(WHT, "Accumulating PSD from %s", init_date);
	return swe_ok;
}


/*
 * After several power spectrums have been accumulated, normalize them to
 * have a proper power spectral density estimation
 */
int psd_normalize(PsdAccumulator* psd){
	int i;
	for ( i=0 ; i<psd->nfreq ; i++ ) {
		// Divide by the number of power spectrum accumulated
		psd->accumulator[i] =  psd->accumulator[i] / ((float64)psd->n);
	}
	return swe_ok;
}

/*
 * Initializes the Power Spectral Density Accumulator.
 * nfreq : number of bins in the fft response
 * ntarget: number of PSD to accumulate before normalizing them
 */
int psd_init(PsdAccumulator* psd, uint nfreq, uint ntarget){
	psd->accumulator = swe_malloc(sizeof(float64)*nfreq);
	psd->nfreq = nfreq;
	psd->ntarget = ntarget;
	psd_reset(psd);
	return swe_ok;
}


/*
 * Resets the PowerSpectralDensity accumulator
 */
int psd_reset(PsdAccumulator* psd){
	psd->n = 0;
	psd->init_timestamp = -1.0;
	psd->end_timestamp = -1.0;
	memset(psd->accumulator, 0, psd->nfreq*sizeof(float64));
	return swe_ok;
}


//---------------------------------------------------------------------------//
//----------------- Sound Pressure Level Calculations -----------------------//
//---------------------------------------------------------------------------//

/*
 * Calculate the SPL values for each band frequency according to the Welch method (power
 * spectrums accumulated and averaged) and temporal samples.
 *
 * If SEL and/or RMS values are enabled output for each band will be
 *
 * SPLxx, SELxx, RMSxx
 *
 */
int calculate_spl_values(SoundPressureLevelData* params){
	PsdAccumulator* psd = &params->psd;
	int iband = 0; // band frequency index
	int ifields = 0; // data fields index

	TRY_RET(psd_normalize(psd)); // First of all, normalize all the PSDs
	char initdate[128]="";
	char enddate[128]="";
	ascii_datetime(initdate,psd->init_timestamp, TRUE);
	ascii_datetime(enddate,psd->end_timestamp, TRUE);
	cimsg(WHT, "SPL - Calculating SPL values during %.2f s", psd->end_timestamp- psd->init_timestamp);
	cimsg(WHT, "SPL - Start %s", initdate);
	cimsg(WHT, "SPL - End   %s", enddate);

	// Calculate all band frequencies //

	for ( iband=0 ; params->band_frequencies[iband]!=NULL ; iband++ ) {
		float64 spl;
		char bandname[64];
		BandFrequency* band = params->band_frequencies[iband];

		// Calculate BandFrequency SPL //
		if ( band->fcenter < 0 ) {
			sprintf(bandname, "full bw");
			// Full bandwidth
			spl = spl_fullband_frequency(params);
			dmsg("SPL - fullband %.4f dB (frequency domain)", spl);
		}
		else {
			// Frequency band
			sprintf(bandname, "%.2f Hz", band->fcenter);
			spl = spl_band_frequency(params, band);
		}
		cimsg(GRN, "SPL - SPL %s : %.4f dB", bandname, spl);
		memcpy(params->data->fields[ifields]->value, &spl, sizeof(float64));
		ifields++;

		// Calculate Band Frequency SEL //
		if ( params->sel == TRUE ) {
			// Sound Exposure Level SEL = 10·log10(E/p²T0) = SPL + 10·log10(T/T0) where T0 = 1 second
			// Time interval over which SPL was calculated
			float64 sel = spl + 10*log10(params->psd.end_timestamp - params->psd.init_timestamp);
			cimsg(GRN, "SPL - SEL %s : %.4f dB", bandname, sel);
			memcpy(params->data->fields[ifields]->value, &sel, sizeof(float64));
			ifields++;
		}

		// Calculate Band Frequency RMS //
		if ( params->rms == TRUE ) {
			// SPL = 20·log10(Prms/P0) ==> Prms = PO · 10^(SPL/20) where P0 is 1 µPa
			// Multiply by 1e-6 to convert µPa to Pa
			float64 rms = 1e-6*pow(10, spl/20);
			cimsg(GRN, "SPL - RMS %s : %.4f Pa", bandname, rms);
			memcpy(params->data->fields[ifields]->value, &rms, sizeof(float64));
			ifields++;
		}
	}
	// Copy the timestamp
	params->data->init_timestamp = params->psd.init_timestamp;
	params->data->end_timestamp = params->psd.end_timestamp;
	return swe_ok;
}




/*
 * Calculates the SPL in a specific frequency band. Returns the SPL value in dB
 */
float64 spl_band_frequency(SoundPressureLevelData* params, BandFrequency* band){
	int i;
	PsdAccumulator* psd = &params->psd;
	float64 df = params->fft_params->df;
	float64 accumulator = 0.0, spl_lin = 0.0;
	uint index_low = round(band->flow / df);
	uint index_high = round(band->fhigh / df);
	uint n = params->fft_params->n;

	// If frequency is negative calculate the spl for the whole spectrum
	if (band->fcenter < 0 ) {
		errmsg("Band not valid");
		return -1.0;
	}
	// Calculate power signal sum //
	for ( i=index_low ; i<=index_high ; i++ ) {
		accumulator += psd->accumulator[i];
	}
	spl_lin = 2*accumulator/n;
	dmsg("SPL - SPL at %.2f Hz %f (lineal)", band->fcenter, spl_lin);
	float64 spl_db = 10*log10(spl_lin);

	// Correct BandWidth correction
	float64 bwideal = band->fhigh - band->flow;
	float64 bwreal = df * (index_high + 1  - index_low);
	float64 bwcorrection = -10*log10(bwreal/bwideal);

	dmsg("SPL - SPL at %.2f Hz %f dB (uncorrected)", band->fcenter, spl_db);
	spl_db += bwcorrection;

	dmsg("SPL - BW real %f", bwreal);
	dmsg("SPL - BW ideal %f", bwideal);
	dmsg("SPL - BW correction %f dB", bwcorrection);
	return spl_db;
}


/*
 * Calculate the SPL from the power spectrum in all the bandwidth
 */
float64 spl_fullband_frequency(SoundPressureLevelData* params) {
	int i;
	float64 accumulator = 0.0;
	for ( i=0 ; i<params->psd.nfreq; i++ ) {
		accumulator += params->psd.accumulator[i];
	}
	dmsg("SPL - Accumulator is %f (lin)", accumulator);
	return 10*log10(2*accumulator/params->fft_params->n);
}


/*
 * This function returns a band frequency structure centered at fcenter
 */
BandFrequency* generate_band_frequency(double fcenter) {
	BandFrequency* band  = swe_malloc(sizeof(BandFrequency));
	band->fcenter = (float64)fcenter;
	band->fhigh = fcenter * pow(2, 1.0/6.0);
	band->flow = fcenter / pow(2, 1.0/6.0);
	return band;
}


//---------------------------------------------------------------------------//
//------------------------ Generate SPL Response ----------------------------//
//---------------------------------------------------------------------------//

/*
 * Generates a SchedulerData structure to store the results of the SPL module
 */
SchedulerData* create_spl_module_response(SoundPressureLevelData* params) {
	int i;
	SchedulerData* data = swe_malloc(sizeof(SchedulerData));

	// Loop through all bands
	for ( i=0 ; params->band_frequencies[i]!= NULL ; i++ ) {
		BandFrequency* band = params->band_frequencies[i];
		char bandname[64];

		if ( band->fcenter < 0 ) {
			// If band is negative take all bandwidth
			sprintf(bandname, "_all");
		} else {
			sprintf(bandname, "_%d", (int)band->fcenter);
		}

		SWE_Data* tmpfield = create_spl_field("SPL", bandname);
		data->fields = ATTACH_TO_ARRAY(data->fields, tmpfield);
		if ( params->sel == TRUE) {
			SWE_Data* tmpfield = create_spl_field("SPL", bandname);
			data->fields = ATTACH_TO_ARRAY(data->fields, tmpfield);
		}
		if ( params->rms == TRUE) {
			SWE_Data* tmpfield = create_spl_field("RMS", bandname);
			data->fields = ATTACH_TO_ARRAY(data->fields, tmpfield);
		}
	}
	data->count = ARRAY_LENGTH(data->fields);
	return data;
}

/*
 * Creates a Field to store SPL/SEL/RMS. Data is float64 and element count = 1
 */
SWE_Data* create_spl_field(const char* paramname, const char* bandname){
	char name[256];
	sprintf(name, "%s%s", paramname, bandname);
	SWE_Data* myfield = swe_malloc(sizeof(SWE_Data));
	myfield->encoding = double_data;
	myfield->element_size = sizeof(float64);
	myfield->value = swe_malloc(myfield->element_size );
	myfield->element_count = 1;
	myfield->name = set_string(name);
	myfield->total_size = myfield->element_size;
	imsg("Creating field %s", name);
	return myfield;
}


/*
 * Function used to plot signals and debug the module
 */
int external_plot(float64* x, uint length, float64 step, char* xlabel, char* ylabel, char* title, uchar uselog){
#ifdef LINUX
	char filename[256];
	char command[512];
	char buff[512];
	int i;
	sprintf(filename,"test_%s.csv", title);
	FILE* f = fopen(filename, "w");

	// Create header
	sprintf(buff, "%s, %s\n", xlabel, ylabel);
	fputs(buff,f);

	// Store signal
	for (i=0; i<length; i++) {
		float64 val = x[i];
		if (uselog) {
			val = log10(x[i]);
		}

		sprintf(buff, "%f, %f\n", ((float64)i)*step, val);
		fputs(buff, f);
	}
	fclose(f);

	// Call python plotter
	sprintf(command, "nohup python3 plotcsv.py %s \"%s\" > /dev/null 2>&1 &", filename, title);
	system(command);
#else
	warnmsg("This function works only under Linux alongside with the python script plotcsv.py");
#endif
	return swe_ok;
}

#endif // ENABLE_HIGH_FREQUENCY_MODULES //
