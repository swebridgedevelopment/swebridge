#ifndef MODULES_LINEAR_CALIBRATION
#define MODULES_LINEAR_CALIBRATION

#include "core/simple_process.h"
#include "swe_conf.h"

#define LINEAR_CALIBRATION_UID "swebridge:process:linearCalibration"
#define REGISTER_COEFFICIENT_PARAMETER_REF "parameters/addCalibration"

#define DOUBLE_TOO_SMALL_LIMIT 0.000001

/*
 * Calibration data corresponding to one field
 */
typedef struct {
	char* fieldName;
	float64 offset;
	float64 slope;
	int index; // index in the datastructure

}LinearCalibration;



/*
 * Internal Datafor Linear Calibration
 */
typedef struct {

	LinearCalibration **calibrations;

	SchedulerData* data;

	uchar initialized;

}LinearCalibrationData;

int linear_calibration_constructor(SimpleProcess* conf, SchedulerProcess* process);
SchedulerData* linear_calibration_exec(void* process_pointer, SchedulerData* data, ProcessStatus* errorcode);


#endif /* MODULES_LINEAR_CALIBRATION */
