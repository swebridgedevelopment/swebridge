/*
 * This module generates wav files
 */

#include "swe_conf.h"
#if ENABLE_HIGH_FREQUENCY_MODULES

#include "core/simple_process.h"
#include "modules/wav_generator.h"

int wav_generator_initialize_stream(WavGeneratorData* params, HighFreqStreamBuffer* incoming);
int put_samples_to_wav(WavGeneratorData* params, uchar* bytes, uint nbytes, float64 timestamp, int offset);
int create_new_wav(WavGeneratorData* params, float64 timestamp);
int write_wav_header(WavGeneratorData* params);
int finish_wav(WavGeneratorData* params);
int check_record_packet(WavGeneratorData* params, HighFreqStreamBuffer* incoming);
int write_id3_metadata(WavFile* wav);
WavFileChunk* create_id3_riff_chunk(PhysicalSystem* system);
WavFileChunk* create_id3_frame(const char* key, const char* value);
WavFileChunk* create_id3_tag(PhysicalSystem* system);
int locate_id3_frame(WavFileChunk* chunk, const char* key);

int wav_generator_flush(void* process_pointer);
int wav_generator_error(void* process_pointer);


/*
 * WAV Generator Module Constructor
 */
int wav_generator_constructor(SimpleProcess* conf, SchedulerProcess* process){
	WavGeneratorData* params;
	PhysicalSystem* ps = (PhysicalSystem*) conf->physical_system;
	msg_sub_title(WHT, "WAV Generator Constructor");

	dmsg( "allocating space for internal data...");
	params=swe_malloc(sizeof(WavGeneratorData));
	process->internal_data=(void*)params;
	strcpy(params->output_folder, DEFAULT_WAV_OUTPUT_FOLDER);
	strcpy(params->temp_folder, DEFAULT_WAV_TEMP_FOLDER);
	params->id3_metadata = DEFAULT_ID3_METADATA;
	params->recording_period = -1.0;
	strcpy(params->file_prefix, ps->id);

	dmsg( "assigning execution pointer...");
	process->exec = wav_generator_exec;
	process->flush = wav_generator_flush;
	process->error = wav_generator_error;

	dmsg("Adding settings");
	process->settings = add_settings_to_process(process->settings, SWE_Quantity, "parameters/recordingTime", (void*)(&params->recording_time));
	process->settings = add_settings_to_process(process->settings, SWE_Text, "parameters/outputFolder", (void*)(params->output_folder));
	process->settings = add_settings_to_process(process->settings, SWE_Text, "parameters/tempFolder", (void*)(params->temp_folder));
	process->settings = add_settings_to_process(process->settings, SWE_Text, "parameters/prefix", (void*)(params->file_prefix));
	process->settings = add_settings_to_process(process->settings, SWE_Boolean, "parameters/ID3metadata", (void*)(&params->id3_metadata));
	process->settings = add_settings_to_process(process->settings, SWE_Quantity, "parameters/recordingPeriod", (void*)(&params->recording_period));

	if(configure_process_settings(conf->settings, process)!=swe_ok) {
		errmsg( "configuration returned errors");
		return swe_invalid_arguments;
	}



	// If scheduled process, calculate the recording time
	if ( process->state_reg.exec_modes.scheduled_process == TRUE) {
		params->recording_time = process->state_reg.scheduler_period;
		imsg("Reording time %f", params->recording_time);
	}


	// Check configuration //
	if (params->recording_time < 1.0 ) {
		errmsg("Can't create wav files with recording time less than 1 second!");
		return swe_invalid_arguments;
	}

	// Crate temp folder //
	if (swe_create_dir(params->temp_folder) != swe_ok){
		warnmsg("Couldn't create temp path %s", params->temp_folder);
	}
	system_notify(SWEB_NOTIFY_OUTPUT_FOLDER, (void*)params->temp_folder);

	// Create output folder //
	if (swe_create_dir(params->output_folder) != swe_ok){
		warnmsg("Couldn't create the output path %s", params->output_folder);
	}
	system_notify(SWEB_NOTIFY_OUTPUT_FOLDER, (void*)params->output_folder);

	if ( params->id3_metadata ) {
		imsg("Adding ID3 metadata tag to wav files");
		params->wav.id3_chunk = TRY_NULL(create_id3_riff_chunk((PhysicalSystem*)conf->physical_system));

		// Look for the the timestamp position and store it
		params->wav.id3_timestamp_index = locate_id3_frame(params->wav.id3_chunk, ID3_INIT_TIMESTAMP_KEY);
		if ( params->wav.id3_timestamp_index < 0 ) {
			errmsg("Couldn't find timestamp point!");
			return -1;
		}
	}
	dmsgn( "\n==================================================\n\n");
	return swe_ok;
}


/*
 * Stores incoming packets into a WAV file.
 *
 * If recording period is set, only 1 file each n seconds will be generated.
 *
 *
 * - Check if the current packet should be recorded and if an offset is needed
 *      - offset 0 means "record all packet"
 *      - offset < 0 means "do not record this packet"
 *      - offset > 0 means "record this packet starting at point n"
 *
 */
SchedulerData* wav_generator_exec(void* process_pointer, SchedulerData* datain, ProcessStatus *errorcode){
	SchedulerProcess* process=(SchedulerProcess*)process_pointer;
	WavGeneratorData* params=(WavGeneratorData*)process->internal_data;
	StateRegister* state_register = &process->state_reg;
	WavFile* wav = &params->wav;
	uchar exec_flag = state_register->exec_flag;
	state_register->exec_flag = 0; // clear the flag, a local copy is still available
	int offset = 0;


	*errorcode = process_chain_error;

	if (exec_flag & PREVIOUS_PROCESS_TRIGGER) {
		HighFreqStreamBuffer *incoming = datain->streambuff;
		if ( params->initialized == FALSE ) {
			TRY_RET_NULL(wav_generator_initialize_stream(params, incoming));
		}
		if ( params->recording_time > 0 ) {
			// Check the recording options
			offset = check_record_packet(params, incoming);
			if ( params->recording == FALSE ) {
				// If we are not recording, stop here
				*errorcode = process_chain_stop;
				return NULL;
			}
		}
		float64 timestamp;
		// Calculate the first timestamp based on incoming buffer and offset //
		TRY_RET_NULL(calculate_sample_timestamp(incoming, offset*incoming->sample_size, &timestamp));

		// Store samples //
		int byteswritten = put_samples_to_wav(params, incoming->data, incoming->nbytes, timestamp, offset);
		if ( byteswritten < 0 ) {
			errmsg("Error writing bytes");
			*errorcode = process_chain_error;
			return NULL;
		}
		dmsg("WAV - Current wave size is %u (target size %u bytes)", wav->data_bytes_written, wav->target_data_bytes);

		// If file generation is not based on scheduled timer

		// Check if we need to finiwh the file
		if ( wav->data_bytes_written >= wav->target_data_bytes || params->finish_next == TRUE) {
			imsg("WAV - Finishing wav file");
			TRY_RET_NULL(finish_wav(params));
			*errorcode = process_chain_continue;
		}
		else {
			*errorcode = process_chain_stop;
		}

		if ( byteswritten < incoming->nbytes ) {
			// Not all packet was stored, check if we need to process it for the next iteration
			if ( params->recording_period < 0 ) {
				// If we are recording continuously put the files to a new wav
				TRY_RET_NULL(calculate_sample_timestamp(incoming, byteswritten*incoming->sample_size, &timestamp));
				put_samples_to_wav(params, incoming->data, incoming->nbytes, timestamp, byteswritten);
			}
		}
	}
	if (exec_flag & SCHEDULER_TRIGGER) {
		params->finish_next = TRUE;
		*errorcode = process_chain_stop;
	}

	return NULL;
}

/*
 * Flush handler
 */
int wav_generator_flush(void* process_pointer){
	SchedulerProcess* process=(SchedulerProcess*)process_pointer;
	WavGeneratorData* params=(WavGeneratorData*)process->internal_data;
	if (params->wav.file != NULL) {
		imsg("WAV - Flush received, finishing wav");
		TRY_RET(finish_wav(params));
	}
	return swe_ok;
}


/*
 * Flush handler
 */
int wav_generator_error(void* process_pointer){
	SchedulerProcess* process=(SchedulerProcess*)process_pointer;
	WavGeneratorData* params=(WavGeneratorData*)process->internal_data;
	if (params->wav.file != NULL) {
		imsg("WAV - Error received, finishing wav");
		TRY_RET(finish_wav(params));
	}

	return swe_ok;
}




/*
 * Initializes internal data according to the input stream
 */
int wav_generator_initialize_stream(WavGeneratorData* params, HighFreqStreamBuffer* incoming){
	WavFile* wav = &params->wav;
	float64 timestamp;
	imsg("WAV - initializing wav parameters...");

	imsg("WAV - Recording time %f", params->recording_time);

	// Calculate the number of samples //
	params->wav.target_data_bytes = incoming->sample_size* incoming->sampling_rate * params->recording_time;

	// store the first timestamp //
	TRY_RET(calculate_sample_timestamp(incoming, 0, &timestamp));
	wav->timestamp = timestamp;

	if ( params->recording_period > 0 ) {
		// If recording period is set calculate the next timestamp to store //
		params->next_timestamp = timestamp + params->recording_period;
	}


	// Initialize WavFile Header
	wav->sampling_rate = (unsigned_int)incoming->sampling_rate;
	wav->data_length = params->wav.target_data_bytes ;
	wav->bits_per_sample = incoming->sample_size * 8;
	wav->num_channels = 1;
	wav->byte_rate = wav->sampling_rate * wav->num_channels * incoming->sample_size;
	wav->block_align = wav->num_channels * incoming->sample_size;

	params->initialized = TRUE;
	params->recording = TRUE; // Set the recording flag to true
	return swe_ok;
}


/*
 * Checks if we should be recroding wav files or not according to wavfile length, incoming timestamp and
 * record period. Return values:
 * -1  : Do not record this packet
 *  0  : store full packet
 * > 0 : Returns the index from which to start recording this packet (in samples)
 */
int check_record_packet(WavGeneratorData* params, HighFreqStreamBuffer* incoming){
	if ( params->recording == TRUE){
		// wait until recording is set to FALSE
		return 0;
	}

	// Check if the incoming packet is greater than the next timestamp expected

	float64 init_timestamp; // timestamp of the first sample
	float64 end_timestamp;
	TRY_RET(calculate_sample_timestamp(incoming, 0, &init_timestamp));
	// calculate the timestamp of the last sample
	end_timestamp = init_timestamp + ((float64)incoming->nsamples)/incoming->sampling_rate;

	if ( end_timestamp >= params->next_timestamp ) {
		int offset;
		// calculate the offset of the packet
		offset = (int)((params->next_timestamp - init_timestamp)*incoming->sampling_rate);
		params->recording = TRUE;
		params->next_timestamp += params->recording_period;
		return offset * incoming->sample_size; // return offset in bytes
	}
	return -1;
}




/*
 * Creating a new wav file
 */
int create_new_wav(WavGeneratorData* params, float64 timestamp){
	DateTime datetime;
	int timestamp_len;
	WavFile* wav = &params->wav;
	char* temp_folder = params->temp_folder;
	char* prefix = params->file_prefix;

	// Store first timestamp //
	wav->timestamp = timestamp;
	TRY_RET(datetime_from_timestamp(&datetime, timestamp));

	// Update the ID3 timestamp //
	if (wav->id3_chunk != NULL ) {
		char ascii_timestamp[128]="Not initialized";
		TRY_RET(ascii_datetime(ascii_timestamp, timestamp, TRUE));
		timestamp_len = strlen(ascii_timestamp);
		memcpy(&wav->id3_chunk->buffer[wav->id3_timestamp_index], ascii_timestamp, timestamp_len);
	}

	// Create temporal file //
	sprintf(wav->filename,"%s%s%s_%02d-%02d-%02dT%02d:%02d:%02d.wav", temp_folder, PATH_SEPARATOR , prefix,
			datetime.year, datetime.month, datetime.day, datetime.hour, datetime.minute, datetime.second );


	wav->file = swe_fopen(wav->filename, swe_write_file);

	if ( wav->filename == NULL) {
		wav->file = NULL;
		errmsg("Can't open file %s", wav->filename);
		return swe_filesystem_error;
	}

	TRY_RET(write_wav_header(params));
	return swe_ok;
}


/*
 * Finish wav file.
 * Step 1: Check if the file size in the WAV header needs to be updated
 * Step 2: Close the file
 * Step 3: Reset the WavFile values
 * Step 4: Generate definitive name
 * Step 5: Move from temp to definitive wav file
 */
int finish_wav(WavGeneratorData* params){
	char new_filename[256];
	WavFile* wav = &params->wav;
	char* basename = wav->filename;
	const char* output_folder = params->output_folder;

	if ( params->id3_metadata ) {
		// Writing ID3 metadata to the end of the file //
		TRY_RET(write_id3_metadata(wav));
	}
	// Update the file size //

	imsg("WAV - Updating file size to %u", wav->bytes_written);

	uchar buff[4];
	uint32_t wavbytes = wav->bytes_written - 8; // size - RIFF<size> header

	// Write RIFF size
	buff[0] = (unsigned_byte)(wavbytes);
	buff[1] = (unsigned_byte)(wavbytes >> 8);
	buff[2] = (unsigned_byte)(wavbytes >> 16);
	buff[3] = (unsigned_byte)(wavbytes >> 24);
	TRY_RET(swe_set_pointer(wav->file, 4));
	TRY_RET(swe_fwrite(buff, 4, wav->file));

	// Write DATA size
	buff[0] = (unsigned_byte)((wav->data_bytes_written));
	buff[1] = (unsigned_byte)((wav->data_bytes_written)>> 8);
	buff[2] = (unsigned_byte)((wav->data_bytes_written)>> 16);
	buff[3] = (unsigned_byte)((wav->data_bytes_written)>> 24);
	TRY_RET(swe_set_pointer(wav->file, 40));
	TRY_RET(swe_fwrite(buff, 4, wav->file));


	// Close file
	TRY_RET(swe_fclose(wav->file));


	// Reset the WavFile counters //
	wav->file = NULL;
	wav->bytes_written = 0;
	wav->data_bytes_written = 0;

	// Generate definitive name //
	while ( memcmp(PATH_SEPARATOR, basename, 1) ) {
		basename++;
	}
	basename++; // skip path separator

	TRY_RET(sprintf(new_filename,"%s%s%s", output_folder, PATH_SEPARATOR , basename));
	cimsg(MAG,	"WAV - New file: %s", new_filename);
	// Move to definitive filename //
	TRY_RET(swe_fmove(wav->filename, new_filename));

	wav->filename[0] = 0; // clear the old name
	// Send new data notification
	system_notify(SWEB_NOTIFY_NEW_DATA_FILE, (void*)new_filename);

	// If recording period is set, put the current recording status to FALSE an d calculate next timestamp//
	if ( params->recording_period > 0 ) {
		params->recording = FALSE;
		wav->timestamp = -1;
	}
	params->finish_next = FALSE;
	return swe_ok;
}




/*
 * Puts samples to a wav file. If too many samples would be written, store
 * the remaining bytes in internal memory.
 *
 * bytes contains the binary buffer hosting samples
 * nbytes is the number of bytes in buffer
 * samplesize is the number of bytes for each sample
 * timstamp is the first timestamp (already calculated taking into account the buffer)
 * offset is the first byte of interest
 *
 *
 */
int put_samples_to_wav(WavGeneratorData* params, uchar* bytes, uint nbytes, float64 timestamp, int offset){
	WavFile* wav = &params->wav;
	uchar* buffpoint = &bytes[offset];
	int writebytes = 0;
	int remaining_bytes = wav->target_data_bytes - wav->data_bytes_written;
	char date[128];

	// If wav file does not exist create a new wavfile
	if (wav->file == NULL) {
		ascii_datetime(date, timestamp, TRUE);
		imsg("WAV - Creating new wav file starting at %s", date);
		TRY_RET(create_new_wav(params, timestamp));
	}

	writebytes = MIN(nbytes - offset, remaining_bytes - offset);
	TRY_RET(swe_fwrite(buffpoint, writebytes, wav->file));

	wav->bytes_written += writebytes ;
	wav->data_bytes_written += writebytes;


	return offset + writebytes;
}



/*
 * Writes the WAV header
 */
int write_wav_header(WavGeneratorData* params){
	WavFile* wav = &params->wav;
	uchar wav_header[WAVE_RIFF_CHUNK_SIZE];


	/* write chunkID, must be 'RIFF'  ------------------------------------------*/
	wav_header[0] = 'R';
	wav_header[1] = 'I';
	wav_header[2] = 'F';
	wav_header[3] = 'F';

	/* Write the file length ----------------------------------------------------*/
	/* The sampling time: this value will be be written back at the end of the
	recording opearation.  Example: 661500 Btyes = 0x000A17FC, byte[7]=0x00, byte[4]=0xFC */
	wav_header[4] = (unsigned_byte)(wav->file_size);
	wav_header[5] = (unsigned_byte)(wav->file_size >> 8);
	wav_header[6] = (unsigned_byte)(wav->file_size >> 16);
	wav_header[7] = (unsigned_byte)(wav->file_size >> 24);
	/* Write the file format, must be 'WAVE' -----------------------------------*/
	wav_header[8]  = 'W';
	wav_header[9]  = 'A';
	wav_header[10] = 'V';
	wav_header[11] = 'E';

	/* Write the format chunk, must be'fmt ' -----------------------------------*/
	wav_header[12]  = 'f';
	wav_header[13]  = 'm';
	wav_header[14]  = 't';
	wav_header[15]  = ' ';

	/* Write the length of the 'fmt' data, must be 0x10 ------------------------*/
	wav_header[16]  = 0x10;
	wav_header[17]  = 0x00;
	wav_header[18]  = 0x00;
	wav_header[19]  = 0x00;

	/* Write the audio format, must be 0x01 (PCM) ------------------------------*/
	wav_header[20]  = 0x01;
	wav_header[21]  = 0x00;

	/* Write the number of channels, ie. 0x01 (Mono) ---------------------------*/
	wav_header[22]  = 0x01; // Mono
	wav_header[23]  = 0x00;

	/* Write the Sample Rate in Hz ---------------------------------------------*/
	/* Write Little Endian ie. 8000 = 0x00001F40 => byte[24]=0x40, byte[27]=0x00*/
	wav_header[24]  = (uint8_t)((wav->sampling_rate & 0xFF));
	wav_header[25]  = (uint8_t)((wav->sampling_rate >> 8) & 0xFF);
	wav_header[26]  = (uint8_t)((wav->sampling_rate >> 16) & 0xFF);
	wav_header[27]  = (uint8_t)((wav->sampling_rate >> 24) & 0xFF);

	/* Write the Byte Rate -----------------------------------------------------*/
	wav_header[28]  = (uint8_t)((wav->byte_rate & 0xFF));
	wav_header[29]  = (uint8_t)((wav->byte_rate >> 8) & 0xFF);
	wav_header[30]  = (uint8_t)((wav->byte_rate >> 16) & 0xFF);
	wav_header[31]  = (uint8_t)((wav->byte_rate >> 24) & 0xFF);

	/* Write the block alignment -----------------------------------------------*/
	wav_header[32]  = (uint8_t)((wav->block_align & 0xFF));
	wav_header[33]  = (uint8_t)((wav->block_align >> 8) & 0xFF);

	/* Write the number of bits per sample -------------------------------------*/
	wav_header[34]  = (uint8_t)((wav->bits_per_sample & 0xFF));
	wav_header[35]  = 0x00;

	/* Write the Data chunk, must be 'data' ------------------------------------*/
	wav_header[36]  = 'd';
	wav_header[37]  = 'a';
	wav_header[38]  = 't';
	wav_header[39]  = 'a';

	/* Write the number of sample data -----------------------------------------*/
	/* This variable will be written back at the end of the recording operation */
	wav_header[40] = (uint8_t)(wav->data_length);
	wav_header[41] = (uint8_t)(wav->data_length >> 8);
	wav_header[42] = (uint8_t)(wav->data_length >> 16);
	wav_header[43] = (uint8_t)(wav->data_length >> 24);

	TRY_RET(swe_fwrite(wav_header, WAVE_RIFF_CHUNK_SIZE, wav->file));
	wav->bytes_written += WAVE_RIFF_CHUNK_SIZE;


	return swe_ok;
}


//-----------------------------------------------------------------//
//--------------------- ID3 Metadata Creation ---------------------//
//-----------------------------------------------------------------//


/*
 * Writes ID3 metadata to a file. Id3 metadata is structured as a group of
 * user-defined ID3 tags (TXXX) embedded within an ID3 tag. The ID3 tag
 * is embedded within a RIFF Chunk.
 *
 * ID3 RIFF Chunk
 * | - ID3 Tag (embedded within the RIFF chunk)
 *     | - ID3 Frame 1 (e.g. "name: hydrophone")
 *     | - ...
 *     | - ID3 Frame N
 */
int write_id3_metadata(WavFile* wav){
	WavFileChunk* id3chunk = wav->id3_chunk;
	TRY_RET(swe_fwrite(id3chunk->buffer, id3chunk->size, wav->file));
	wav->bytes_written += id3chunk->size;
	return swe_ok;
}


/*
 * The ID3 Chunk is the id3 tag with the RIFF Chunk header
 */
WavFileChunk* create_id3_riff_chunk(PhysicalSystem* system){

	WavFileChunk *id3_riff_chunk = swe_malloc(sizeof(WavFileChunk));
	WavFileChunk* id3tag;
	id3tag = CATCH_NULL(create_id3_tag(system));
	id3_riff_chunk->size = id3tag->size + 8;
	id3_riff_chunk->buffer = swe_malloc(sizeof(uchar)*id3_riff_chunk->size);
	uint32_t chunks_size = id3_riff_chunk->size -8;


	id3_riff_chunk->buffer[0] = 'i';
	id3_riff_chunk->buffer[1] = 'd';
	id3_riff_chunk->buffer[2] = '3';
	id3_riff_chunk->buffer[3] = ' ';
	int32_to_little_endian(chunks_size, &id3_riff_chunk->buffer[4]);

	memcpy(&id3_riff_chunk->buffer[8], id3tag->buffer, chunks_size);
	// free tag
	swe_free(id3tag->buffer);
	swe_free(id3tag);
	return id3_riff_chunk;
}


/*
 * Create the ID3 tag composed of several ID3 frames
 */
WavFileChunk* create_id3_tag(PhysicalSystem* system){
	int i, index;
	int32_t frames_size = 0, frames_size_synchsafe; // size of the overall id3 tag
	WavFileChunk* id3 = swe_malloc(sizeof(WavFileChunk));
	WavFileChunk** id3_frames = NULL;
	// Add all identifiers

	// Add all Identifiers as ID3 frames //
	if ( system->identifiers != NULL ) {
		for (i=0 ; system->identifiers[i]!=NULL ; i++ ){
			Identifier* iden = system->identifiers[i];
			WavFileChunk* frame =CATCH_NULL(create_id3_frame(iden->label, iden->value));
			id3_frames = ATTACH_TO_ARRAY(id3_frames, frame);
		}
	}
	// Add parameters //
	if ( system->parameters != NULL ) {
		for (i=0 ; system->parameters[i]!=NULL ; i++ ){
			Parameter* param = system->parameters[i];
			Field* pfield = param->field;
			if (check_if_basic(pfield->type)) {
				char tempval[512];
				const char* key;
				// Get field value as text //
				TRY_RET_NULL(field_component_value_get_ascii(pfield, tempval, 512));
				key = field_component_label_get(pfield);
				// If component does not have name use parameter name
				if ( key == NULL ) {
					key = param->name;
				}
				WavFileChunk *frame;
				frame = CATCH_NULL(create_id3_frame(key, tempval));
				id3_frames = ATTACH_TO_ARRAY(id3_frames, frame);
			}
		}
	}
	// Add location //
	if ( system->position != NULL ) {
		char temp[256];
		WavFileChunk* frame;
		// Latitude
		sprintf(temp, "%f", system->position->latitude);
		frame =CATCH_NULL(create_id3_frame("latitude", temp));
		id3_frames = ATTACH_TO_ARRAY(id3_frames, frame);
		// Longitude
		sprintf(temp, "%f", system->position->longitude);
		frame =CATCH_NULL(create_id3_frame("longitude", temp));
		id3_frames = ATTACH_TO_ARRAY(id3_frames, frame);
		// Depth
		sprintf(temp, "%f", system->position->depth);
		frame =CATCH_NULL(create_id3_frame("depth", temp));
		id3_frames = ATTACH_TO_ARRAY(id3_frames, frame);
	}

	// Attached To information
	if ( system->attached_to != NULL ) {
		WavFileChunk* frame;
		if ( system->attached_to->title != NULL ) {
			frame =CATCH_NULL(create_id3_frame("platform code", system->attached_to->title));
			id3_frames = ATTACH_TO_ARRAY(id3_frames, frame);
		}
		if ( system->attached_to->href != NULL ) {
			frame =CATCH_NULL(create_id3_frame("platform url", system->attached_to->href));
			id3_frames = ATTACH_TO_ARRAY(id3_frames, frame);
		}
	}

	// Add an extra frames for init timestamp. These will be updated in each
	// wav file generated
	WavFileChunk* frame;
	float64 currenttimestamp = 0.0;
	char ascii_timestamp[128];
	TRY_RET_NULL(ascii_datetime(ascii_timestamp, currenttimestamp, TRUE));
	// Since timestamps will always have the same length, put a timestamp and then update it
	// Once the file is created
	frame =CATCH_NULL(create_id3_frame(ID3_INIT_TIMESTAMP_KEY, ascii_timestamp));
	id3_frames = ATTACH_TO_ARRAY(id3_frames, frame);

	// Getting the size of all the frames //
	for ( i=0 ; id3_frames[i] != NULL ; i++ ) {
		frames_size += id3_frames[i]->size;
	}
	frames_size_synchsafe = to_synchsafe(frames_size);
	id3->size = frames_size + ID3_TAG_HEADER_SIZE;
	id3->buffer = swe_malloc(sizeof(uchar)*id3->size);
	memcpy(id3->buffer, "ID3", 3);// Copy the ID3 tag
	// Version
	id3->buffer[0] = 'I';
	id3->buffer[1] = 'D';
	id3->buffer[2] = '3';
	id3->buffer[3] = 0x04; // major version
	id3->buffer[4] = 0x00; // minor version
	id3->buffer[5] = 0x00; // No flags
	int32_to_big_endian(frames_size_synchsafe, &id3->buffer[6]);
	index = ID3_TAG_HEADER_SIZE;
	for (i=0 ; id3_frames[i]!=NULL ; i++ ) {
		WavFileChunk* frame = id3_frames[i];
		memcpy(&id3->buffer[index], frame->buffer, frame->size);
		index += frame->size;

		// Once copied, free the frame
		swe_free(frame->buffer);
		swe_free(frame);
	}
	swe_free(id3_frames);
	return id3;
}


/*
 * Create ID3 RIFF Chunk with custom information
 */
WavFileChunk* create_id3_frame(const char* key, const char* value){
	int index;
	uint32_t id3_frame_size, id3_frame_size_synchsafe;
	WavFileChunk* frame = swe_malloc(sizeof(WavFileChunk));
	if ( key == NULL || value == NULL ) {
		errmsg("NULL received");
		return NULL;
	}
	dmsg("ID3 Frame \"%s\" : \"%s\"", key, value);

	// Total size of the id3 frmae
	frame->size = ID3_FRAME_HEADER_SIZE + strlen(key) + 1 + strlen(value) + 1;
	id3_frame_size = frame->size - ID3_FRAME_HEADER_SIZE;
	id3_frame_size_synchsafe = to_synchsafe(id3_frame_size);
	// Size to be written in the id3 frame (real size - header size)

	// Allocate memory for tag
	frame->buffer = swe_malloc(sizeof(uchar)*(10 + strlen(key) + 1 + strlen(value) + 1));

	// TXXX is the FourCC code for User defined text information frame
	memcpy(frame->buffer, "TXXX", 4);
	int32_to_big_endian(id3_frame_size_synchsafe, &frame->buffer[4]);
	frame->buffer[8] = (uint8_t)0;  // WavFileChunk flags (set to 0)
	frame->buffer[9] = (uint8_t)0;

	index = ID3_FRAME_HEADER_SIZE;

	frame->buffer[index] = 0; // 0 at the beginning of string
	index++;
	memcpy(&frame->buffer[index], key, strlen(key)); // copy key
	index += strlen(key);

	frame->buffer[index] = 0; // 0 at the beginning of string
	index++;
	memcpy(&frame->buffer[index], value, strlen(value));
	index += strlen(value);
	return frame;
}



/*
 * Looks for an element with containing "key". If it is found the
 * position after key is returned, otherwise -1 is returned.
 */
int locate_id3_frame(WavFileChunk* chunk, const char* key){
	int i=0;
	int keylen = strlen(key);

	while ( i < chunk->size ){
		if ( !memcmp(key, &chunk->buffer[i], keylen)){
			// Found frame with tag
			return i + keylen + 1; // return position immediately after key
		}
		i++;
	}
	return -1;
}




#endif // ENABLE_HIGH_FREQUENCY_MODULES //

