/*
 * This file implements the High Frequency Stream module. This module focuses acquiring
 * real-time data streams (e.g. Hydrophone) and converting from raw data to an array of
 * values that can be processed in real-time by further modules.
 *
 * @author: Enoc Martínez
 * @institution: Universitat Politècnica de Catalunya (UPC)
 * @contact: enoc.martinez@upc.edu
 */

#include "swe_conf.h"

#if ENABLE_HIGH_FREQUENCY_MODULES
#include "modules/hydrophone_stream.h"
#include "resources/resources.h"
#include "resources/hydrophone.h"
#include "common/swe_utils.h"
#include "common/sensorml.h"
#include "common/ring_buffer.h"
#include "common/high_freq_stream.h"
#include <math.h>

#define DEBUG_THIS_MODULE ON

#if DEBUG_THIS_MODULE
/*
 * Wrap debug message functions
 */
#define DBGM(...) dmsg(__VA_ARGS__) // DEBUG this module
#else
#define DBGM(...)
#endif

//---- Private functions -----//
void stream_handler(void);


//---- Global Variables ----//
uchar high_freq_stream_set = FALSE;
SchedulerProcess* myprocess;
uchar reentrant_flag = 0;


void stream_handler(void);

/*
 * Constructor for Hydrophone Stream. This module processes incoming binary streaming data
 * from a hydrophone, converts it to pressure samples and stores it in a buffer for further
 * processes.
 *
 */
int hydrophone_stream_constructor(SimpleProcess* conf, SchedulerProcess* process){
	HydrohponeData* internal_data = swe_malloc(sizeof(HydrohponeData));
	HydrophoneParameters* params = &internal_data->params;
	PhysicalSystem* system = (PhysicalSystem*)conf->physical_system;
	HighFreqStreamResponse *resp;
	int i;
	uint sample_size = 0;
	// Hydrophone-specific variables

	float accumulate_time = -1.0; // time (in seconds) to accumulate data before processing
	int accumulate_samples = 0; // time (in seconds) to accumulate data before processing
	int nblocks = DEFAULT_HIGH_FREQ_STREAM_NBLOCKS;
	int blocksize;
	char frame_count_ref[256] = DEFAULT_FRAME_COUNT_FIELD_NAME;

	process->internal_data = internal_data;
	internal_data->initialized = FALSE; // set the init flag to FALSE
	msg_sub_title( WHT, "Hydrophone Stream Constructor");

	// Only one high frequency stream can be set, so we need to be sure that it has not
	// already been configured
	if (high_freq_stream_set == TRUE) {
		errmsg("Only one High Frequency Stream can be configured!");
		return swe_error;
	}
	// Assign pointers
	process->exec = hydrophone_stream_exec;
	myprocess = process;

	// Set default values to Hydrophone parameters //
	params->sensitivity = -1;
	params->amplifier_gain = -1;
	params->sampling_rate = -1;
	params->adc_bits = 0;
	params->adc_vref = -1;
	params->adc_bipolar = TRUE;

	// Adding settings to process //
	process->settings=add_settings_to_process(process->settings, SWE_Count, "parameters/bufferBlocks", (void*)(&nblocks));
	process->settings=add_settings_to_process(process->settings, SWE_Quantity, "parameters/accumulateTime", (void*)(&accumulate_time));
	process->settings=add_settings_to_process(process->settings, SWE_Count, "parameters/accumulateSamples", (void*)&accumulate_samples);
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/frameCountField", (void*)(&frame_count_ref));

	// Process Settings //
	dmsg( "Configuring settings...");
	if(configure_process_settings(conf->settings, process)!=swe_ok) {
		errmsg( "configuration failed");
		return swe_error;
	}

	// Assign the Interface //
	dmsg("Assigning the interface to the process...");
	process->iface = (Interface*)conf->interface;

	// Create response structure //
	imsg("Creating hydrophone response structure");
	if ( ARRAY_LENGTH(conf->outputs) != 1 ) {
		errmsg("Only 1 output is expected in stream, got %d", ARRAY_LENGTH(conf->outputs));
		return swe_error;
	}
	internal_data->response = TRY_NULL(high_freq_stream_response_constructor(conf->outputs[0]->field));
	resp = internal_data->response;
	imsg("Stream size is %u", internal_data->response->size);

	// Look for data samples and frame count based on response structure names //
	TRY_RET(assign_high_freq_stream_roles(internal_data->response, frame_count_ref));

	// get sample size

	for ( i=0 ; resp->elements[i]!=NULL ; i++ ) {
		if (resp->elements[i]->role == stream_samples) {
			sample_size = resp->elements[i]->element_size;
			break;
		}
	}

	TRY_RET(get_hydrophone_parameters(system, sample_size, params));

	// Show Hydrophone Parameters //
	cimsg(BLU,"Sensitivity %f dB re 1V/µPa", params->sensitivity);
	cimsg(BLU,"Amplifier gain %f dB", params->amplifier_gain);
	cimsg(BLU,"Sampling Rate %f Hz", params->sampling_rate);
	cimsg(BLU,"ADC %u bits", params->adc_bits);
	if (params->adc_bipolar) cimsg(BLU,"ADC bipolar: TRUE");
	else cimsg(BLU,"ADC bipolar: FALSE");
	cimsg(BLU,"ADC Vref %f V", params->adc_vref);
	TRY_RET(calculate_conversion_coefficient(params, &internal_data->conversion_coefficient));
	imsg("Conversion coefficient %f [µPa/counts]", internal_data->conversion_coefficient);

	// Allocate memory for SchedulerData //
	internal_data->data = swe_malloc(sizeof(SchedulerData));

	// Calculate the number of samples to accumulate //
	if ( accumulate_time < 0 && accumulate_samples <= 0) {
		errmsg("Accumulate time/samples not set");
		return swe_invalid_arguments;
	} else if ( accumulate_time > 0 && accumulate_samples > 0) {
		errmsg("accumulateTime accumulateSamples cannot be set simultaneously! choose one");
		return swe_invalid_arguments;
	}
	else if( accumulate_time > 0) {
		float npackets;
		dmsg("User aksed to accumulate %f seconds", accumulate_time);
		npackets = accumulate_time * params->sampling_rate / ((float64)resp->samples_per_packet);
		internal_data->accumulate_samples= (uint)(((float64)params->sampling_rate) * accumulate_time);
		dmsg("Packets %f", npackets);
		internal_data->accumulate_samples = (uint)((ceil)(npackets) * resp->samples_per_packet);
		dmsg("Accumulating %d packets", internal_data->accumulate_samples);
		dmsg("Real accumulation time %f seconds", internal_data->accumulate_samples / params->sampling_rate);
	} else if (accumulate_samples > 0 ){
		internal_data->accumulate_samples = (uint)accumulate_samples;
		dmsg("Accumulating %u samples");
	}

	// Create a StreamBuffer to pass data to next processes //
	internal_data->data->streambuff = high_freq_stream_buffer_constructor(internal_data->response,
			params, internal_data->accumulate_samples, MAX_PACKETS_LOST, internal_data->conversion_coefficient);

	// Creating RingBuff //
	blocksize = internal_data->response->size;
	imsg("Creating a BlockRingBuffer with %d blocks and blocksize %d bytes", nblocks, blocksize);
	imsg("Total BlockRingBuffer size is %d bytes", nblocks * blocksize);
	internal_data->ringbuff = ringbuff_constructor(nblocks, blocksize);


	if (process->state_reg.sampling_rate > 0) {
		warnmsg("Process SamplingRate option unimplemented!");
	}

	// Setting interruption //
	imsg("Configuring high priority interrupt on iface %p", process->iface);
	TRY_RET(iface_set_priority_interrupt(process->iface, stream_handler));
	process->state_reg.exec_modes.iface_interrupt = TRUE;

	// By default the timeout is set to 30 packets
	internal_data->timeout = 100  * internal_data->data->streambuff->time_between_packets;
	imsg("max timeout without error %.03f seconds", internal_data->timeout);
	process_timeout_set(&process->state_reg, internal_data->timeout);
	return swe_ok;
}


/*
 * Execution handler for High Frequency Stream module
 *
 * Execution:
 *     IFACE_INTERRUPT_TRIGGER: A new stream has been received and it needs to be processed and accumulated
 *     INTERNAL_RETRIGGER: This flag is set when the output buffer has been filled. Before
 *                         continue execution it has to be emptied
 *     INTERNAL_TIMEOUT: There has been a timeout error in the communications interface, reopen it
 */
SchedulerData* hydrophone_stream_exec(void* process_pointer, SchedulerData* nothing, ProcessStatus *errorcode){
	SchedulerProcess* process=(SchedulerProcess*)process_pointer;
	HydrohponeData* internal_data=(HydrohponeData*)process->internal_data;
	HighFreqStreamResponse *response = internal_data->response;
	HighFreqStreamBuffer *streambuff = internal_data->data->streambuff;
	StateRegister* state_register = &process->state_reg;
	BlockRingBuffer* ringbuff = internal_data->ringbuff;
	uchar exec_flag = state_register->exec_flag;
	state_register->exec_flag = 0; // clear the flag, a local copy is still available
	int i;

	// If INTERNAL_RETRIGGER is set it means that the output buffer (HighFreqStreamBuffer)
	// is full and needs to be emptied before processing new incoming data. It is assumed
	// that the rest of the processes have already processed the data
	if (exec_flag & INTERNAL_RETRIGGER) {
		streambuff->nbytes = 0;
		streambuff->nsamples = 0;
		streambuff->timestamp_count = 0;

		// Add remaining missing packets
		while (streambuff->remaining_empty_packets) {
			imsg("HYD - put remaining empty packet...");
			put_empty_packet(streambuff);
			streambuff->remaining_empty_packets--;
		}

		*errorcode = process_chain_stop;
	}

	// If there has been a timeout error in the communications interface, close & reopen it
	if ( (exec_flag & INTERNAL_TIMEOUT_TRIGGER) || (internal_data->iface_error == TRUE) )  {
		if (exec_flag & INTERNAL_TIMEOUT_TRIGGER ) {
			char date1[256];
			ascii_datetime(date1, swe_get_epoch_time(), TRUE);
			warnmsg("%s - Timer expired in hydrophone interface", date1);

		}
		else {
			errmsg("Iface error!");
		}
		// reopen iface
		internal_data->initialized = FALSE; // reset the init flag
		imsg("HYD - Closing iface");
		iface_close(process->iface);
		ringbuff_reset(ringbuff);
		// Wait 1 second. use 1000 1 ms sleeps to avoid being waked up by an interrupt / timer
		i = 1000;
		while (i--) {
			delayms(1);
		}
		imsg("HYD - Trying to reopen inteface...");
		if (iface_open(process->iface) != swe_ok) {
			*errorcode = process_chain_error;
			return NULL;
		}
		iface_set_priority_interrupt(process->iface, stream_handler);
		process_timeout_set(state_register, internal_data->timeout);
		internal_data->iface_error =  FALSE;
		*errorcode = process_chain_flush; // timing will not be maintained, flush next processes
		return NULL;
	}


	// if IFACE_INTERRUPT_TRIGGER is set a new stream has been received and it needs to be
	// processed and accumulated into SchedulerData
	if (exec_flag & IFACE_INTERRUPT_TRIGGER) {
		*errorcode = process_chain_stop;

		process_timeout_set(state_register, internal_data->timeout);
		int nblocks = ringbuff_available_blocks(internal_data->ringbuff);
		//dmsg("%d available blocks (total %d bytes)", nblocks, nblocks * ringbuff->blocksize);
		if (nblocks == 0) {
			errmsg("0 bytes to be read");
			return NULL;
		}
		// Get Each Block //
		for ( i=0 ; i<nblocks ; i++ ) {
			uchar tempbuff[ringbuff->blocksize];
			float64 timestamp=-2;
			int ret ;
			ret = ringbuff_get_block(ringbuff, tempbuff,ringbuff->blocksize, &timestamp);
			if ( ret < 0 ) {
				errmsg("Couldn't get data block from buffer!");
				ringbuff_reset(ringbuff);
				*errorcode = process_chain_stop;
				return NULL;
			}
			// Process incoming data, convert from binary stream to float64
			// and store it in SchdeulderData's high freq buffer to pass it
			// to other processes
			if (process_incoming_packet(streambuff, response, tempbuff,	timestamp, internal_data->initialized) < 0 ){
				errmsg("Could not process incoming packet!");
				internal_data->iface_error = TRUE;
				state_register->exec_flag |= INTERNAL_RETRIGGER;
				return NULL;
			}
			internal_data->initialized = TRUE;

			// Check if we have all the samples that we want!
			if ( streambuff->nsamples >= internal_data->accumulate_samples ) {
				float64 average = 0.0;

				// The stream buffer is filled!
				*errorcode = process_chain_continue; // Tell the scheduler to execute next processes
				dmsg("HYD - %d samples accumulated", internal_data->accumulate_samples);
				// Calculate the buffer average //
				for ( i=0 ; i<streambuff->length ; i++ ){
					average += streambuff->data[i];
				}
				average = average / streambuff->length;
				streambuff->average = average;

				// Set the INTERNAL_RETRIGGER to flush the buffer before processing new data
				state_register->exec_flag |= INTERNAL_RETRIGGER;
				break; // Do not process any other block from RingBuffer to avoid overflow
			}
		}
	}
	return internal_data->data;
}


/*
 * High frequency stream handler.
 */
void stream_handler(void) {
	HydrohponeData* internal_data = (HydrohponeData*) myprocess->internal_data;
	BlockRingBuffer* ringbuff = internal_data->ringbuff;
	Interface* iface = myprocess->iface;
	int response_size = internal_data->response->size;
	int recv_bytes;
	uchar temp[response_size];

	// Read all bytes available in the interface
	while ( (recv_bytes = iface_recv(iface, temp, response_size, 0)) > 0 ) {
		// put read data to the BlockRingBuffer
		if (ringbuff_put_block(ringbuff, temp, recv_bytes) < 0){
			ringbuff_reset(ringbuff);
		}
	}
	myprocess->state_reg.exec_flag |= IFACE_INTERRUPT_TRIGGER;

}







#endif // ENABLE_HIGH_FREQUENCY_MODULES //

