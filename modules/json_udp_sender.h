/*
 * Sends JSON data throught a UDP socket
 */
#ifndef JSON_UDP_SENDER_H_
#define JSON_UDP_SENDER_H_

#include "core/simple_process.h"
#include "swe_conf.h"
#include "resources/resources.h"

#define JSON_UDP_SENDER_UID "swebridge:modules:jsonUdpSender"

#define DEFAULT_JSON_UDP_DECIMAL_PRECISION 4

typedef struct {

	// Connection info //
	char destination[256];
	char* sensor_name;
	Interface* iface;

	int decimal_precision;
}JsonUdpSenderData;

int json_udp_sender_constructor(SimpleProcess* conf, SchedulerProcess* process);
SchedulerData* json_udp_sender_exec (void* process_pointer, SchedulerData* data, ProcessStatus *errorcode);


#endif /* JSON_UDP_SENDER_H_ */

