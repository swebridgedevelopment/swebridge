/*
 * internal_sensors.h
 *
 *  Created on: Oct 18, 2018
 *      Author: enoc
 */

#ifndef MODULES_INC_INTERNAL_SENSORS_H_
#define MODULES_INC_INTERNAL_SENSORS_H_

#include "common/stream_parser.h"
#include "core/simple_process.h"

#define INTERNAL_SENSORS_UID "swebridge:modules:internalSensors"
#define SENSOR_INDEX_DEFAULT 0
#define INTERNAL_SENSORS_SAMPLING_RATE_DEFAULT -1



typedef struct {

	int sensor_index;
	StreamBuffer buffer;
	uchar collapse_white_spaces;
	SchedulerData* data;
	ResponseStructure* response;
}InternalSensorsData;





int internal_sensors_constructor(SimpleProcess* conf, SchedulerProcess* process);
SchedulerData* internal_sensors_exec (void* process_pointer, SchedulerData* data, ProcessStatus *errorcode);



#endif /* MODULES_INC_INTERNAL_SENSORS_H_ */
