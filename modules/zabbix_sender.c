/**************************************************************************
 *	 	 	 	 	 	  Zabbix Sender
 *************************************************************************
 * This module sends acquired data to a zabbix service. The communication
 * pattern to the zabbix uses socket communications. In order to avoid
 * disruption of the scheduler timing, an external program has to be used,
 * e.g. zabbix_sender.py
 *************************************************************************/

#include "modules/zabbix_sender.h"
#include "common/swe_utils.h"
#include "core/simple_process.h"
#include "swe_conf.h"


/*
 * Constructor for the zabbix sender module. This module will send the incoming data to a Zabbix serer
 * using an external program.
 */
int zabbix_sender_constructor(SimpleProcess* conf, SchedulerProcess* process){
	char temp_ip[256] = "";
	char temp_hostname[256] = ""; // host or instrument (as registered in zabbix)
	char temp_zabbix_sender_program[512] = "";
	char temp_prefix[256] = ""; // host or instrument (as registered in zabbix)


#if (! ((defined GENERIC_LINUX ) || (defined SENSORBOX) || defined RASPBERRY_PI) )
	errmsg("Zabbix sender is not available in this platform");
	return swe_unimplemented;
#endif



	msg_sub_title(WHT, "Send to Zabbix Constructor");
	dmsg("allocating space for internal data...");
	ZabbixSenderData* internal_data=swe_malloc(sizeof(ZabbixSenderData));
	process->internal_data=(void*)internal_data;

	dmsg( "assigning execution pointer...");
	process->exec = zabbix_sender_exec;

	dmsg( "setting the default execution modes...");

	dmsg( "setting default values to the process internal settings...");
	internal_data->port = -1;

	//STEP 6: Make visible internal parameters with the function "add_settings_to_process"
	dmsg( "Adding configurable settings...");
	process->settings=add_settings_to_process(process->settings,SWE_Text, "parameters/IP", (void*)&temp_ip);
	process->settings=add_settings_to_process(process->settings, SWE_Count, "parameters/port", (void*)(&internal_data->port));
	//process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/zabbixSender", (void*)(internal_data->zabbix_sender_program));
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/zabbixSender", (void*)&temp_zabbix_sender_program);
	// hostname and instrument name are the same //
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/instrumentName", (void*)&temp_hostname);
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/hostName", (void*)&temp_hostname);


	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/prefix", (void*)&temp_prefix);


	dmsg( "Configuring settings...");
	if(configure_process_settings(conf->settings, process)!=swe_ok) {
		errmsg("configuration returned errors");
	}

	dmsg( "done");


	// Allocate IP //
	if (temp_ip[0] != 0) {
		internal_data->IP = set_string(temp_ip);
	} else {
		errmsg("IP is required!");
		return swe_invalid_arguments;
	}

	if (internal_data->port < 0) {
		errmsg("Port is required!");
	}

	// Allocate hostname //
	if (temp_hostname[0] != 0) {
		internal_data->hostname = set_string(temp_hostname);
	} else {
		errmsg("hostname is required!");
	}

	// Allocate sender //
	if (temp_zabbix_sender_program[0] != 0) {
		internal_data->zabbix_sender_program = set_string(temp_zabbix_sender_program);
	} else {
		errmsg("zabbixSender is required!");
	}

	// Allocate prefix //
	if ( temp_prefix[0]!= 0 ) {
		internal_data->prefix = set_string(temp_prefix);
	} else {
		internal_data->prefix = NULL;
	}

	cimsg(GRN, "send_to_zabbix process created successfully");
	return swe_ok;
}

/*
 * Zabbix Sender
 */
SchedulerData* zabbix_sender_exec(void* process_pointer, SchedulerData* data, ProcessStatus *errorcode){
	SchedulerProcess* process=(SchedulerProcess*)process_pointer;
	ZabbixSenderData* internal_data=(ZabbixSenderData*)process->internal_data;
	StateRegister* state_register = &process->state_reg;
	state_register->exec_flag = 0; // clear the flag, a local copy is still available
	char command[ZABBIX_COMMAND_LENGTH];
	dmsg("ZBX - Sending data to host %s", internal_data->hostname);
	int i;
	for (i=0 ; i < data->count ; i++) {
		char buffer[64]; // buffer to convert binary values to strings
		char* presult; // pointer to the result
		swe_data_encoding e = data->fields[i]->encoding;

		if (check_if_binary(e)) {
			swe_encoding_to_string(buffer, data->fields[i]->value, e, ZABBIX_DECIMAL_PRECISION);
			presult = buffer; // assign the result pointer to the buffer
		}
		else {
			presult = (char*)data->fields[i]->value; // assign the result pointer to the actual value
		}

		// If Instrument prefix is TRUE add the instrument name to the variable name, e.g.
		// mySensor.myVariable instead
		if(internal_data->prefix != NULL) {
			sprintf(command, "%s -z \"%s\" -p \"%u\" -s \"%s\" -k \"%s\".\"%s\" -o \"%s\" > /dev/null 2>&1 &",
				internal_data->zabbix_sender_program,
				internal_data->IP,
				internal_data->port,
				internal_data->hostname,
				internal_data->prefix,
				data->fields[i]->name,
				presult);
		} // do not add the prefix
		else {
			sprintf(command, "%s -z \"%s\" -p \"%u\" -s \"%s\" -k \"%s\" -o \"%s\" > /dev/null 2>&1 &",
				internal_data->zabbix_sender_program,
				internal_data->IP,
				internal_data->port,
				internal_data->hostname,
				data->fields[i]->name,
				presult);
		}
		// Pass the command to the operating system
		dmsg("ZBX - sending %s...", data->fields[i]->name);
		dmsg("ZBX - %s", command);
		if (system(command) < 0) {
			errmsg("System returned error!");
		}
	}
	return NULL; //End of the chain
}

