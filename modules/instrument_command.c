

#include "modules/instrument_command.h"
#include "common/sensorml.h"
#include "common/swe_data_model.h"
#include "common/swe_utils.h"
#include "core/simple_process.h"
#include "resources/resources.h"
#include "swe_conf.h"

/**************************************************************************
 *	 	 	 	 	 	instrument_command_constructor
 *************************************************************************
 * This structure contains all the necessary information to execute a
 * Simple_process, including its execution handler
 *
 * STEP 1: Allocate memory for the process and set its common information
 *
 * STEP 2: Allocate memory for the internal data structure
 *
 * STEP 3: Assign execution pointer
 *
 * STEP 4: Make visible internal parameters with the function
 * 			"add_settings_to_process"
 *
 * STEP 5 Auto-configure process settings with the function
 * 			"configure_process_settings"
 *
 *************************************************************************/

#define DEBUG_THIS_MODULE ON

#if DEBUG_THIS_MODULE
# define print_buffer(x) print_simple_buffer(x)
#else
# define print_buffer(x)
#endif


#if DEBUG_THIS_MODULE/*
 * Wrap debug message functions
 */
#define DBGM(...) dmsg(__VA_ARGS__) // DEBUG this module
#else
#define DBGM(...)
#endif


int add_basic_fields_to_settings(SimpleProcess* conf, SchedulerProcess* process);
int free_paths_from_process_settings(SchedulerProcess* process, int free_paths_from);


int instrument_command_configure_outputs(SimpleProcess* conf, SchedulerProcess* process, uchar data_record_block, uchar erase_junk);
int instrument_command_configure_inputs(SimpleProcess* conf, SchedulerProcess* process);
int update_burst_mode(SchedulerProcess* process, uchar exec_flag);
int check_duplicated_fields(ProcessSetting** settings, int start_from);


/*
 * Constructor for Instrument Command module
 */
int instrument_command_constructor(SimpleProcess* conf, SchedulerProcess* process){
	int i;
	uchar free_paths_from = 0;
	uchar check_data_integrity;
	uchar data_record_block;
	uchar ignore_leading_tokens = UNSET_BOOLEAN;
	uchar allow_empty_fields = UNSET_BOOLEAN;
	uchar erase_junk = FALSE;

	int user_defined_buffer_size = 0;

	msg_sub_title( WHT, "Instrument Command Constructor");
	dmsg( "allocating space for internal data...");
	InstrumentCommandData* internal_data=swe_malloc(sizeof(InstrumentCommandData));
	process->internal_data=(void*)internal_data;

	dmsg( "assigning execution pointer...");
	process->exec=instrument_command_exec;

	dmsg( "setting default values to the process internal settings...");
	internal_data->timeout = DEFAULT_TIMEOUT;
	process->state_reg.watchdog_period = DEFAULT_PROCESS_TIMEOUT;
	check_data_integrity = DEFAULT_CHECK_DATA_INTEGRITY;
	data_record_block = DEFAULT_DATA_RECORD_AS_BLOCK;
	internal_data->flush_before_query = DEFAULT_FLUSH_BEFORE_QUERY;
	internal_data->burst_measures = 0;
	internal_data->burst_cycle = -1.0;
	internal_data->burst_period = -1.0;


	// Check that we have inputs and/or outputs
	if (conf->outputs == NULL && conf->inputs == NULL) {
		errmsg("Instrument Command requires at least one input or one output!");
		return swe_error;
	}

	dmsg( "Adding configurable settings...");

	process->settings=add_settings_to_process(process->settings, SWE_Count, "parameters/bufferSize", (void*)(&user_defined_buffer_size));
	process->settings=add_settings_to_process(process->settings, SWE_Quantity, "parameters/timeOut", (void*)(&internal_data->timeout));
	// Deprecated names //
	process->settings=add_settings_to_process(process->settings, SWE_Quantity, "parameters/processTimeout", (void*)(&process->state_reg.watchdog_period));
	process->settings=add_settings_to_process(process->settings, SWE_Quantity, "parameters/watchdog", (void*)(&process->state_reg.watchdog_period));

	process->settings=add_settings_to_process(process->settings, SWE_Boolean, "parameters/checkDataIntegrity", (void*)(&check_data_integrity));
	process->settings=add_settings_to_process(process->settings, SWE_Boolean, "parameters/dataRecordBlock", (void*)(&data_record_block));
	process->settings=add_settings_to_process(process->settings, SWE_Boolean, "parameters/ignoreLeadingTokens", (void*)(&ignore_leading_tokens));
	process->settings=add_settings_to_process(process->settings, SWE_Boolean, "parameters/flushBeforeQuery", (void*)(&internal_data->flush_before_query));
	process->settings=add_settings_to_process(process->settings, SWE_Boolean, "parameters/eraseJunk", (void*)(&erase_junk));

	// Burst measurements
	process->settings=add_settings_to_process(process->settings, SWE_Count, "parameters/burstMeasures", (void*)(&internal_data->burst_measures));
	process->settings=add_settings_to_process(process->settings, SWE_Quantity, "parameters/burstPeriod", (void*)(&internal_data->burst_period));
	process->settings=add_settings_to_process(process->settings, SWE_Quantity, "parameters/burstCycle", (void*)(&internal_data->burst_cycle));

	free_paths_from = ARRAY_LENGTH(process->settings); // from this point, the new "ref" fields generated by the add_basic_fields_to_settings
		                                               // function are dynamic memory instead of

	dmsg("Registering DataStream fields to settings (enabled by default)...");
	TRY_RET(add_basic_fields_to_settings(conf, process));
	TRY_RET(check_duplicated_fields(process->settings, free_paths_from));

	// By default enable all fields from the stream
	for ( i=0 ; conf->field_list[i] != NULL ; i++ ){
		Field* f = conf->field_list[i];
		if (check_if_basic(f->type) == TRUE ) {
			f->status = ENABLED; // enable by default all outputs
		}
	}
	// Process Configuration //
	cdmsg(BLU,  "Configuring settings:");
	TRY_RET(configure_process_settings(conf->settings, process));
	cdmsg(GRN,"settings configured!");

	// Free the generated Paths //
	dmsg("freeing settings structure...");
	free_paths_from_process_settings(process, free_paths_from);

	// Assign the Interface //
	dmsg("Assigning the interface to the process...");
	process->iface = (Interface*)conf->interface;

	// Generate Command Structure//
	TRY_RET(instrument_command_configure_inputs(conf, process));

	if (process->state_reg.sampling_rate > 0.0 && internal_data->command == NULL) {
		warnmsg("Sampling rate set, but there isn't any command present");
		cimsg(YEL,"Disabling the scheduler execution mode, assuming instrument is working in stream mode");
		process->state_reg.exec_modes.scheduler = FALSE;
		process->state_reg.sampling_rate = -1;
	}

	// Generate Response Structure and Scheduler Data from Outputs //
	TRY_RET(instrument_command_configure_outputs(conf, process, data_record_block, erase_junk));
	// If Data Structure has been generated, add the configured settings
	if ( internal_data->response != NULL ) {
		set_check_data_integrity(internal_data->response, check_data_integrity);
		print_response_structure(internal_data->response);
		StreamBuffer* buffer = internal_data->buffer;

		// If user specified a different buffer size, allocate it //
		if ( user_defined_buffer_size > 0 ){
			dmsg("Forcing buffer size with %d bytes based on SensorML", user_defined_buffer_size);
			swe_free(buffer->buffer);
			buffer->size = user_defined_buffer_size;
			buffer->buffer = swe_malloc(sizeof(uchar)*user_defined_buffer_size);
		}
	}

	if (ignore_leading_tokens != UNSET_BOOLEAN) { // Check if this parameter has been set
		set_ignore_leading_tokens(internal_data->response, ignore_leading_tokens);
	}
	if (allow_empty_fields != UNSET_BOOLEAN) { // Check if this parameter has been set
		set_allow_empty_fields(internal_data->response, allow_empty_fields);
	}

	//---- Configure Burst Mode ----//
	if (internal_data->burst_measures > 0){
		// Burst Period should be smaller than burst cycle and busrt period should be greater than 0
		if ( internal_data->burst_period < 0 ) {
			errmsg("Cannot set burst mode, burstPeriod < 0");
			return swe_invalid_arguments;
		}  else if (internal_data->command == NULL) {
			errmsg("Burst mode requires a command to be sent!");
			return swe_invalid_arguments;
		} else if (internal_data->burst_measures < 2) {
			errmsg("Burst mode requires at least 2 measures");
			return swe_invalid_arguments;
		}
		cdmsg(BLU,"Configuring Burst Mode type");
		dmsg("Burst cycle %f seconds", internal_data->burst_cycle);
		dmsg("Burst period %f seconds", internal_data->burst_period);
		dmsg("Measurements per cycle %d", internal_data->burst_measures);

		// If the burst is triggered by a burst cycle
		if (internal_data->burst_cycle > 0 ) {
			// Set the sampling rate to burst period
			process->state_reg.sampling_rate = internal_data->burst_period;
			process->state_reg.exec_modes.scheduler = TRUE;
		} // Otherwise, if burst is activated by a previous process
		else {
			process->state_reg.sampling_rate = -1.0;
			process->state_reg.exec_modes.scheduler = FALSE;

		}
	}

	//---- Check Configuration ----//
	// If no input and output => streaming (interrupt on, scheduler off) //
	if ((internal_data->command == NULL) && (internal_data->response != NULL)) {
		process->state_reg.exec_modes.iface_interrupt = TRUE; // set iface interrupt
		internal_data->fill_buffer = TRUE;
		if (process->state_reg.sampling_rate > 0) {
			warnmsg("Ignoring scheduler in streaming process!");
		}
	}

	// If input but no output => commands (scheduler on, interrupt off) //
	else if ((internal_data->command == NULL) && (internal_data->response != NULL)) {

		// Check if it's configured as init process
		if (process->state_reg.exec_modes.initialization){
			// Do nothing
		} else if (process->state_reg.sampling_rate > 0) {
			// Set scheduler flag
			process->state_reg.exec_modes.scheduler = TRUE;
		} else {
			errmsg("Command configured, but sampling rate not set! (previous process FALSE)");
		}
	}
	// If input and output => query (scheduler on, interrupt on) //
	else if ((internal_data->command != NULL) && (internal_data->response != NULL)) {

		// Check if it's configured as init process
		if (process->state_reg.sampling_rate > 0) {
			// Set scheduler flag
			process->state_reg.exec_modes.scheduler = TRUE;
			process->state_reg.exec_modes.iface_interrupt = TRUE;

		} else if (process->state_reg.exec_modes.previous_process == TRUE){
			// If previous process activates the query, everything is OK
		}
		else if ( process->state_reg.exec_modes.scheduled_process == TRUE ){
			// if scheduled process, it's ok
		}
		else {

			errmsg("Can't set query process without valid sampling rate!");
			return swe_invalid_arguments;
		}
	}

	cimsg(GRN, "instrument command process created successfully");
	dmsgn( "\n==================================================\n\n");
	mem_status();
	return swe_ok;
}

/*
 * Execution function for Instrument Command module. If triggered by scheduler or previous process it
 * tries to send a command (if defined) and waits for the response. If it is triggered by an interrupt
 * on the comm's interface (or a timeout) it parses the incoming string
 */
SchedulerData* instrument_command_exec(void* process_pointer, SchedulerData* nothing, ProcessStatus *errorcode){
	SchedulerProcess* process=(SchedulerProcess*)process_pointer;
	InstrumentCommandData* internal_data=(InstrumentCommandData*)process->internal_data;
	SchedulerData* data =internal_data->data;
	StateRegister* state_register = &process->state_reg;
	StreamBuffer* buff = internal_data->buffer;
	uchar exec_flag = state_register->exec_flag;
	uchar stream_parsed_flag = FALSE;
	int ret, i;
	state_register->exec_flag = 0; // clear the flag, a local copy is still available

	*errorcode = process_chain_error;

	// Triggered by Scheduler or previous process//
	if ( (exec_flag & SCHEDULER_TRIGGER) || (exec_flag & PREVIOUS_PROCESS_TRIGGER)) {
		// If we are in burst mode, incremnt the counter
		if (internal_data->burst_measures > 0) {
			update_burst_mode(process, exec_flag);
		}
		// If there's a command send it
		if (internal_data->command != NULL) {
			CommandStructure* command = internal_data->command;

			// Check if the input buffer needs to flushed before sending a command //
			if ( internal_data->flush_before_query == TRUE && internal_data->response != NULL ) {
				dmsg("CMD - Flushing input buffer...");
				empty_buffer(internal_data->buffer);
			}

			imsgn("CMD - Sending command ");

			if (internal_data->command->binary == TRUE) {
				print_binary_array(command->data, command->size);
			} else {
				dmsgn("CMD - [");
				fmt_dmsg(MAG,"%s", (char*)command->data);
				dmsgn("]");
			}
			dmsg(" %d bytes", command->size);

			ret = iface_send(process->iface, command->data, command->size);
			if ( ret < 0 ){
				errmsg("iface_send returned %d", ret);
			}
			if ( internal_data->response != NULL ) {
				// If a response is expected after this command set the expecting_response flag,
				// set a timeout and stop the process chain here. The process chain will continue
				// once a timeout has expired or the response has been received.
				dmsg("CMD - waiting response...");
				state_register->expecting_response = TRUE;
				if (internal_data->timeout > 0 ) {
					dmsg("CMD - Settings timeout to %f seconds...", internal_data->timeout);
					process_timeout_set(state_register, internal_data->timeout);
				}
				*errorcode = process_chain_stop;
			}
			else {
				// If we do not expect a response just continue the process chain
				*errorcode = process_chain_continue;
			}
		}
	}

	// Interface Interrupt //
	if ( (exec_flag & IFACE_INTERRUPT_TRIGGER) || (exec_flag & INTERNAL_RETRIGGER) ){
		parser_retcode retcode;
		ResponseStructure* response = internal_data->response;

		// Reset watchdog timeout //
		process->state_reg.watchdog_current_ticks = 0; // reset the watchdog

		// Step 1: Fill the buffer (if we have iface interrupt)
		if (exec_flag & IFACE_INTERRUPT_TRIGGER) {
			if (fill_buffer(buff, process->iface) < 0) {
				errmsg("couldn't fill buffer");
				return NULL;
			}
		}

		// Step 2: Parse the stream
		retcode = stream_parser(buff, response);


		// Step 3a: If the stream is not complete set a timeout and exit
		if (retcode == stream_not_complete) {
			warnmsg("Stream not complete, setting timeout");
			process_timeout_set(state_register, internal_data->timeout);
			*errorcode = process_chain_stop;
			return NULL;
		}
		else if (retcode == stream_with_errors) {
			warnmsg("Stream with errors");
			dmsg("CMD - Emptying buffer...");
			empty_buffer(buff);
			state_register->expecting_response = FALSE; // response failed...
			*errorcode = process_chain_stop;
			return NULL;
		}
		else if (retcode == stream_successfully_parsed) {
			cimsg(GRN,"CMD - Stream successfully parsed");

			stream_parsed_flag = TRUE;
			*errorcode = process_chain_continue  ;

			// Get the timestamp //
			internal_data->data->init_timestamp = swe_get_epoch_time();

			// Print the values //
			for (i=0;  data->fields[i]!=NULL ; i++ ) {
				cdmsgn(MAG, "CMD - %s ", data->fields[i]->name);
				print_swe_data(data->fields[i]);
			}

			// cancel the timeout (if it wasn't active it has no effect)//
			process_timeout_cancel(state_register);
			state_register->expecting_response = FALSE; // response parsed

			if (readjust_buffer(buff) > 0 ) {
				// if there's still data in buffer it's possible to have a complete response
				process->state_reg.exec_flag |= INTERNAL_RETRIGGER; // Exec it again
				dmsg("CMD - Setting INTERNAL_RETRIGGER");
			}
		}
		else { //unknown_parser_error
			errmsg("Unknown parser error");
			readjust_buffer(buff);
			state_register->expecting_response = FALSE; // response failed...
			*errorcode = process_chain_stop;
			return NULL;
		}
	}
	// Timeout //
	if ( (exec_flag & INTERNAL_TIMEOUT_TRIGGER) && stream_parsed_flag == FALSE){
		// The timeout has expired, empty the buffer and exit
		warnmsg("%s timeout, emptying buffer", process->id);
		if (buff == NULL) {
			errmsg("Buffer is NULL!");
			*errorcode = process_chain_error;
			return NULL;
		}
		empty_buffer(buff); // empty the input buffer
		state_register->expecting_response = FALSE; // response failed...
		*errorcode = process_chain_stop; // Tell the scheduler that further processes do not need to be executed
	}


	return data;
}


//-----------------------------------------------------------------//
//---------------- DATASTREM PATH GENERATION ----------------------//
//-----------------------------------------------------------------//
static char** path = NULL;
int generate_and_attach_path(Field* f,  SchedulerProcess* process) {
	int i;
	if (f->name != NULL) { // attach the new name
		path = ATTACH_TO_ARRAY(path, f->name);
	}
	if (f->type == SWE_DataStream) {
		DataStream *ds = (DataStream*)f->component;
		generate_and_attach_path(ds->field, process);
	}
	else if (f->type == SWE_DataRecord) {
		DataRecord* dr = (DataRecord*)f->component;
		for ( i=0 ; dr->fields[i]!=NULL ; i++) {
			generate_and_attach_path(dr->fields[i], process);
		}
	}
	else if (f->type == SWE_DataArray) {
		DataArray* da = (DataArray*)f->component;
		for ( i=0 ; da->fields[i]!=NULL ; i++) {
			generate_and_attach_path(da->fields[i], process);
		}
	}
	else if (check_if_basic(f->type) == TRUE ){
		// Creating the path //
		char tmp_path[256];
		char* def_path;
		tmp_path[0] = 0;
		sprintf(tmp_path, "%s", path[0]);
		for( i=1 ; path[i]!=NULL ; i++) {

			sprintf(tmp_path, "%s/%s",tmp_path, path[i]);
		}
		def_path = set_string(tmp_path);

		// register field to settings
		f->status = ENABLED; // Enable by default
		process->settings=add_settings_to_process(process->settings, SWE_Boolean, def_path, (void*)(&f->status));
		dmsgn("Registering path \"");
		cdmsgn(CYN,"%s", def_path);
		dmsg("\"");
	}
	if (f->name != NULL) { // attach the new name
		path = REMOVE_LAST_ARRAY_ELEMENT(path);
	}

	return swe_ok;
}


/*
 * takes the input structure of SimpleProcess and registers all the status
 * flags from inputs and outputs basic fields into the settings structure
 */
int add_basic_fields_to_settings(SimpleProcess* conf, SchedulerProcess* process){
	int i, j;
	if (conf->outputs == NULL) {
		// no fields to add!
		return swe_ok;
	}
	// Adding Outputs //
	for ( i=0 ; conf->outputs[i] != NULL ; i++) {
		Output* o = conf->outputs[i];

		path = ATTACH_TO_ARRAY(path, set_string("outputs"));
		path = ATTACH_TO_ARRAY(path, set_string(o->name));
		path = ATTACH_TO_ARRAY(path, set_string("data"));
		generate_and_attach_path(o->field, process);

		for ( j = 0 ; path[j]!=NULL ; j++) {
			swe_free(path[j]);
		}
		swe_free(path);
		path = NULL;
	}
	return swe_ok;
}

/*
 * Frees the settings from the index "free_paths_from" to the end of the settings structure
 */
int free_paths_from_process_settings(SchedulerProcess* process, int free_paths_from){
	int i;
	// Freeing generated paths for enabling / disabling DataStream fields
	for (i= 0 ; process->settings[i]!=NULL ; i++ ){
		// start looping from the point when we started generating paths
		if ( i > free_paths_from) {
			char* freeable_ref = (char*)process->settings[i]->ref;
			swe_free(freeable_ref); // free the automatically generated paths
		}
		swe_free(process->settings[i]);
	}
	swe_free(process->settings);
	return swe_ok;
}




/*
 * Configures the command to be sent to the Instrument based on the SensorML inputs
 */
int instrument_command_configure_inputs(SimpleProcess* conf, SchedulerProcess* process){
	InstrumentCommandData* internal_data = (InstrumentCommandData*)process->internal_data;
	Input* input;
	DataStream* ds;
	CommandStructure* command;


	if( conf->inputs == NULL) {
		dmsg("No inputs found");
		return swe_ok;
	}
	else if(ARRAY_LENGTH(conf->inputs) > 1) {
		errmsg("Only one input per SimpleProcess is allowed");
		return swe_unimplemented;
	}
	dmsg("Configuring input (sensor command)");
	input = conf->inputs[0];
	// Check if the field is a DataStream //
	if (input->field->type != SWE_DataStream) {
		errmsg("Input field exepcted DataStream type (current type %s)", swe_component_type_text(input->field->type));
	}

	ds = (DataStream*)input->field->component;
	if (ds->values == 0) {
		errmsg("Expected values in input DataStream!");
		return swe_invalid_arguments;
	}

	command = swe_malloc(sizeof(CommandStructure));


	//---------------- Text Encoding ----------------//
	// If the command is encoded in ASCII add the blockSeparator at the end of the command values
	if (ds->encoding->type == SWE_TextEncoding) {
		TextEncoding *te = (TextEncoding*)ds->encoding->encoding;
		uint values_length = 0;
		uint block_length = 0;

		if ( ds->values != NULL ) {
			values_length = strlen(ds->values);
		}
		if (te->blockSeparator != NULL) {
			block_length = strlen(te->blockSeparator);
		}

		dmsg("Procesing command in TextEncoding...");
		command->binary = FALSE;
		command->size = values_length + block_length;
		command->data = swe_malloc((command->size+1) * sizeof(char)); // add 1 byte for the ending '0'
		command->data[command->size] = 0;
		memcpy(command->data, ds->values, values_length);
		if (block_length > 0 ) {
			memcpy(command->data + values_length, te->blockSeparator, block_length);
		}
		command->data[command->size] = 0; // finish the string
		dmsgn("Command to be sent [");
		fmt_dmsg(GRN, "%s", command->data);
		dmsg("]");
	}
	//---------------- Binary Encoding ----------------//
	if (ds->encoding->type == SWE_BinaryEncoding) {
		BinaryEncoding *be = (BinaryEncoding*)ds->encoding->encoding;
		dmsg("Procesing command in BinaryEncoding...");
		command->binary = TRUE;
		if (be->byteEncoding != base64) {
			errmsg("Binary data should be encoded in base64");
			return swe_invalid_arguments;
		}
		dmsg("Converting from base64 to binary");
		command->data = TRY_NULL(base64_decode(ds->values, &command->size));
		dmsg( "Base64 command: [%s]", ds->values);
		dmsgn("Binary command: ");
		print_binary_array(command->data, command->size);

	}

	// Asssign the command //
	internal_data->command = command;


	return swe_ok;
}



/*
 * Configure the Instrument response according to the SimpleProcess outputs
 */
int instrument_command_configure_outputs(SimpleProcess* conf, SchedulerProcess* process,
		uchar data_record_block, uchar erase_junk){
	InstrumentCommandData* internal_data = (InstrumentCommandData*)process->internal_data;
	Output* output;
	uint buffer_size;

	if( conf->outputs == NULL) {
		dmsg("No outputs found");
		return swe_ok;
	}
	else if(ARRAY_LENGTH(conf->outputs) > 1) {
		errmsg("Only one output per SimpleProcess is allowed");
		return swe_unimplemented;
	}
	dmsg("Configuring output (sensor response)");
	output = conf->outputs[0];


	// Creating Data Structure Result //
	TRY_NULL(internal_data->response = response_structure_constructor(output->field, data_record_block));

	// Activating Interface Interrupt (required to receive outputs) //
	process->state_reg.exec_modes.iface_interrupt = TRUE;


	//---- Search for Encoding ----//
	Field* rootfield = output->field;
	if ( conf->outputs[0]->field->type != SWE_DataStream ){
		errmsg("expected DataStream, got type %d", conf->outputs[0]->field->type);
		return swe_unimplemented;
	}

	DataStream* ds = (DataStream*)rootfield->component;

	// Check TextEncoding parameters //
	if ( ds->encoding->type == SWE_TextEncoding ) {
		dmsg("processing response in TextEncoding");
		TextEncoding* e = (TextEncoding*)ds->encoding->encoding;
		set_collapse_white_spaces(internal_data->response, e->collapseWhiteSpaces);
	}
	// Check BinaryEncoding parameters //
	else if (ds->encoding->type == SWE_BinaryEncoding) {}
	else if (ds->encoding->type == SWE_XMLEncoding) {}
	else {
		errmsg("Unimplemented Encoding Type");
		return swe_unimplemented;
	}

	//---- Create a StreamBuffer ----//
	buffer_size = calculate_buffer_size(internal_data->response);
	internal_data->buffer = stream_buffer_constructor(buffer_size, erase_junk);


	imsg("Generating data structure from output %s", conf->outputs[0]->name);
	internal_data->data = TRY_NULL(scheduler_data_from_response_structure(internal_data->response));
	return swe_ok;
}



/*
 * Updates the burst mode status
 */
int update_burst_mode(SchedulerProcess* process, uchar exec_flag) {
	InstrumentCommandData* internal_data=(InstrumentCommandData*)process->internal_data;
	StateRegister* state_register = &process->state_reg;
	ulong sampling_rate_us=0;


	internal_data->burst_counter++;

	// If we reached the total number of burst measures in this cycle
	// change the sampling rate to sleep for burst_cycle seconds
	if ( internal_data->burst_counter >= internal_data->burst_measures ) {

		// If there is no burst cycle, wait for a previous process to trigger another burst
		if (internal_data->burst_cycle < 0) {
			state_register->exec_modes.scheduler = FALSE;

		} else {
			// update the state register!
			int n = internal_data->burst_measures;
			state_register->exec_modes.scheduler = TRUE;
			state_register->sampling_rate = internal_data->burst_cycle - (n - 1)* internal_data->burst_period;
			sampling_rate_us = 1000000*(state_register->sampling_rate);
			state_register->trigger_ticks = sampling_rate_us/state_register->tick_rate_us;
			state_register->current_ticks = 0;
		}

		internal_data->burst_counter = 0;

	} else if (internal_data->burst_counter == 1) {
		// Set the sampling rate to burst period
		state_register->exec_modes.scheduler = TRUE;
		sampling_rate_us = 1000000*(internal_data->burst_period);
		state_register->trigger_ticks = sampling_rate_us/state_register->tick_rate_us;
		state_register->current_ticks = 0;
		internal_data->burst_counter = 0;
		internal_data->burst_counter++;
	}

	return swe_ok;
}


/*
 * This function checks all the field paths in Settings. If a reference is duplicated, an error
 * is returned
 */
int check_duplicated_fields(ProcessSetting** settings, int start_from) {
	int i, j;
	int errors = 0;

	for ( i=start_from; settings[i]!=NULL ; i++ ) {
		const char* path = settings[i]->ref;
		for (j=start_from; settings[j]!=NULL; j++) {
			if ( j != i ) { // Do not compare with itself
				if (!compare_strings(path, settings[j]->ref)) {
					errmsg("Duplicated field \"%s\"");
					errors++;
				}
			}
		}
	}
	if (errors > 0) {
		return swe_invalid_arguments;
	}
	return swe_ok;
}
