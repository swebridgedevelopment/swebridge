


#include "modules/subsampling.h"

#include "common/swe_utils.h"
#include "core/simple_process.h"
#include "swe_conf.h"


/*
 * Constructor for Subsampling module
 */
int subsampling_constructor(SimpleProcess* conf, SchedulerProcess* process){
	msg_sub_title( MAG, "Instrument Command Constructor");


	//STEP 2: Allocate memory for the internal data structure
	dmsg( "allocating space for internal data...");
	SubsamplingData* internal_data=swe_malloc(sizeof(SubsamplingData));


	process->internal_data=(void*)internal_data;
	//STEP 3: Assign execution pointer
	dmsg( "assigning execution pointer...");
	process->exec=subsampling_exec;

	//STEP 4: Assign execution pointer
	dmsg( "setting the default execution modes...");


	//STEP 5: Setting default values to the process internal parameters
	dmsg( "setting default values to the process internal settings...");
	process->state_reg.sampling_rate=SUBSAMPLING_PERIOD_DEFAULT;
	internal_data->subsampling_ratio=SUBSAMPLING_RATIO_DEFAULT;
	internal_data->ignore_first = 0; // 0 by default

	//STEP 6: Make visible internal parameters with the function "add_settings_to_process"
	dmsg( "Adding configurable settings...");
	//process->settings=add_settings_to_process(process->settings, Quantity, "parameters/subsamlingPeriod", (void*)(&internal_data->subsampling_period));
	process->settings=add_settings_to_process(process->settings, SWE_Quantity, "parameters/subsamplingPeriod", (void*)(&process->state_reg.sampling_rate));
	process->settings=add_settings_to_process(process->settings, SWE_Count,    "parameters/subsamplingRatio", (void*)(&internal_data->subsampling_ratio));
	process->settings=add_settings_to_process(process->settings, SWE_Count,    "parameters/ignoreFirst", (void*)(&internal_data->ignore_first));


	//STEP 7 Auto-configure process settings with the function "configure_process_settings"
	dmsg( "Configuring settings...");
	if(configure_process_settings(conf->settings, process)!=swe_ok) {
		errmsg( "configuration returned errors");
	}
	dmsg( "done");

	// If we have sampling rate activate scheduler //
	if (process->state_reg.sampling_rate > 0) {
		process->state_reg.exec_modes.scheduler = TRUE;
	}

	cimsg(GRN, "subsampling process created successfully");

	dmsgn( "\n==================================================\n\n");
	return swe_ok;
}

/*
 * If Sampling Ratio is set, one of each n measures is stored
 * If SamplingRate is set, only one measure will be stored every time the scheduler timer expires
 */
SchedulerData* subsampling_exec(void* process_pointer, SchedulerData* data, ProcessStatus *errorcode){
	SchedulerProcess* process=(SchedulerProcess*)process_pointer;
	SubsamplingData* internal_data=(SubsamplingData*)process->internal_data;
	StateRegister* state_register = &process->state_reg;
	uchar exec_flag = state_register->exec_flag;
	*errorcode = process_chain_error;
	state_register->exec_flag = 0; // clean the exec flag

	// The timer expired, accept next data
	if ( exec_flag & SCHEDULER_TRIGGER ) {
		internal_data->accept_next_data = TRUE;
		dmsg("SUB -Allowing next");
		*errorcode = process_chain_stop; // let the scheduler continue
	}

	// Check incoming data (passed by the previous process)
	else if (exec_flag & PREVIOUS_PROCESS_TRIGGER){
		// Ignore first measurements (if set)
		if (internal_data->ignore_first > 0) {
			internal_data->ignore_first--;
			dmsg("SUB -Ignoring measure (%d left to ignore)...", internal_data->ignore_first);
			*errorcode = process_chain_stop; // stop process chain
		} else {
			internal_data->ratio_counter++;
			// If we are subsampling by ratio
			if (internal_data->subsampling_ratio > 0 ) {
				if (internal_data->ratio_counter == 1) {
					*errorcode = process_chain_continue; // let the scheduler continue
					dmsg("SUB - pass measure...");
				} else {
					*errorcode = process_chain_stop; // stop process chain
					dmsg("SUB - ignoring measure...");
				}
				if (internal_data->ratio_counter == internal_data->subsampling_ratio) {
					internal_data->ratio_counter = 0;
				}
			}

			// If we are subsampling by period
			if (process->state_reg.sampling_rate > 0 ) {
				if (internal_data->accept_next_data) {
					*errorcode = process_chain_continue; // let the scheduler continue
					internal_data->accept_next_data = FALSE;
				} else {
					dmsg("SUB - ignoring measure...");
					*errorcode = process_chain_stop; // stop process chain
				}
			}
		}
	}
	return data;
}
