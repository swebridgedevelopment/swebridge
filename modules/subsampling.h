/*
 * subsampling.h
 *
 *  Created on: Feb 23, 2017
 *      Author: enoc
 */


#ifndef SUBSAMPLING_H_
#define SUBSAMPLING_H_

#include "core/simple_process.h"
#include "swe_conf.h"

#define SUBSAMPLING_UID "swebridge:modules:subsampling"

#define SUBSAMPLING_PERIOD_DEFAULT -1
#define SUBSAMPLING_RATIO_DEFAULT 0


typedef struct {
	/* public paramteres */

	int subsampling_ratio;
	int ignore_first;

	/*private parameters */
	int ratio_counter;
	ulong period_ticks;
	ulong current_ticks;
	uchar accept_next_data;


}SubsamplingData;

SchedulerData* subsampling_exec(void* process_pointer, SchedulerData* data, ProcessStatus* errorcode);
int subsampling_constructor(SimpleProcess* conf, SchedulerProcess* process);



#endif /* SUBSAMPLING_H_ */
