/*
 * This module generates wav files
 */

#ifndef MODULES_WAV_GENERATOR_H_
#define MODULES_WAV_GENERATOR_H_

#include "swe_conf.h"
#if ENABLE_HIGH_FREQUENCY_MODULES

#include "core/simple_process.h"


#define WAV_GENERATOR_UID "swebridge:modules:wavGenerator"
#define WAVE_RIFF_CHUNK_SIZE 44
#define DEFAULT_WAV_OUTPUT_FOLDER "wavs"
#define DEFAULT_WAV_TEMP_FOLDER "temp"
#define DEFAULT_ID3_METADATA TRUE

//------ ID3 TAG -------//
#define ID3_FRAME_HEADER_SIZE 10
#define ID3_TAG_HEADER_SIZE 10

#define ID3_INIT_TIMESTAMP_KEY "start timestamp"


/*
 * Structure holding a chunk of wav file data
 */
typedef struct {
	uchar* buffer; // data in chunk
	uint32_t size; // chunk size
}WavFileChunk;


/*
 * Structure to store WAV file params
 */
typedef struct {
	// Static Header info //
	unsigned_int sampling_rate;
	unsigned_int data_length;
	unsigned_int file_size;
	unsigned_short bits_per_sample;
	unsigned_short num_channels;
	unsigned_int byte_rate;
	unsigned_short block_align;

	// File //
	swe_file* file;
	char filename[512];
	float64 timestamp; // Timestamp of the beginning of the file

	// Counters //
	uint bytes_written; // Number of bytes written in the file (including samples, headers and ID3 chunk)

	uint data_bytes_written; // Number of data bytes written
	uint target_data_bytes; // Desired number of bytes to be written

	// ID3 Metadata //
	WavFileChunk* id3_chunk; // ID3 metadata chunk
	int id3_timestamp_index; // ID3 timestamp index
}WavFile;



/*
 * Internal structure for SoundPressureLevel module
 */
typedef struct {
	uchar initialized;
	//uchar* remaining_bytes; // bytes left
	//uint nbytes;   // remaining bytes count

	uchar id3_metadata; // if TRUE additional metadata will be saved in ID3 format

	uchar detrend_data;
	float64 recording_time; // Duration of each WAV file


	char output_folder[256];
	char temp_folder[256];
	char file_prefix[256];
	WavFile wav;

	uchar recording; // Flag that indicates that we are currently recording
	float64 recording_period;   // If set, record only one wav file each "record_period" seconds

	float64 next_timestamp; // If we are not recording, ignore incoming packets until we reach
							// the next_timestamp

	uchar finish_next; // If TRUE this packet indicates to end the WAV file upon the reception of the
	                   // next packet.

}WavGeneratorData;


int wav_generator_constructor(SimpleProcess* conf, SchedulerProcess* process);
SchedulerData* wav_generator_exec(void* process_pointer, SchedulerData* data, ProcessStatus* errorcode);

#endif // ENABLE_HIGH_FREQUENCY_MODULES //
#endif /* MODULES_WAV_GENERATOR_H_ */
