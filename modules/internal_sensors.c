/*
 * internal_sensors.c
 *
 *  Created on: Oct 18, 2018
 *      Author: enoc
 */


#include "modules/internal_sensors.h"

#include "common/sensorml.h"
#include "common/swe_data_model.h"
#include "common/swe_utils.h"
#include "core/simple_process.h"
#include "resources/resources.h"
#include "swe_conf.h"

#define DEBUG_THIS_MODULE ON

#if DEBUG_THIS_MODULE
# define print_buffer(x) print_simple_buffer(x)
#else
# define print_buffer(x)
#endif


#if DEBUG_THIS_MODULE/*
 * Wrap debug message functions
 */
#define DBGM(...) dmsg(__VA_ARGS__) // DEBUG this module
#else
#define DBGM(...)
#endif


int add_basic_fields_to_settings(SimpleProcess* conf, SchedulerProcess* process);



int internal_sensors_constructor(SimpleProcess* conf, SchedulerProcess* process){

	int i;
	int free_paths_from = 0;
	msg_sub_title( WHT, "Instrument Command Constructor");
	dmsg( "allocating space for internal data...");
	InternalSensorsData* internal_data=swe_malloc(sizeof(InternalSensorsData));
	process->internal_data=(void*)internal_data;
	dmsg( "assigning execution pointer...");
	process->exec=internal_sensors_exec;
	dmsg( "setting default values to the process internal settings...");
	internal_data->collapse_white_spaces = TRUE;

	//STEP 6: Make visible internal parameters with the function "add_settings_to_process"
	dmsg( "Adding configurable settings...");
	process->settings=add_settings_to_process(process->settings, SWE_Count, "parameters/sensorIndex", (void*)(&internal_data->sensor_index));
	free_paths_from = ARRAY_LENGTH(process->settings); // from this point, the new "ref" fields generated by the add_basic_fields_to_settings
		                                               // function are dynamic memory instead of
	cimsg(BLU, "Registering DataStream paths to settings (by default all enabled)");
	TRY_RET(add_basic_fields_to_settings(conf, process));
	// By default enable all fields from the stream
	for ( i=0 ; conf->field_list[i] != NULL ; i++ ){
		Field* f = conf->field_list[i];
		if (check_if_basic(f->type) == TRUE ) {
			f->status = ENABLED; // enable by default all outputs
		}
	}

	internal_data->sensor_index = SENSOR_INDEX_DEFAULT;
	process->state_reg.sampling_rate = INTERNAL_SENSORS_SAMPLING_RATE_DEFAULT;
	dmsg( "Configuring settings...");
	if(configure_process_settings(conf->settings, process)!=swe_ok) {
		errmsg( "configuration returned errors");
		return swe_error;
	}
	dmsg( "done");

	dmsg("Freeing settings structure");
	// Freeing generated paths for enabling / disabling DataStream fields
	for (i= 0 ; process->settings[i]!=NULL ; i++ ){
		// start looping from the point when we started generating paths
		if ( i > free_paths_from) {
			char* freeable_ref = (char*)process->settings[i]->ref;
			swe_free(freeable_ref); // free the automatically generated paths
		}
		swe_free(process->settings[i]);
	}
	swe_free(process->settings);
	process->settings = NULL;

	// Setting TextEncoding //
	if(conf->outputs == NULL) {
		errmsg("One output is expected at Internal Sensors module!");
		return swe_error;
	}
	if(ARRAY_LENGTH(conf->outputs) != 1) {
		errmsg("Only one output per SimpleProcess is allowed");
		return swe_unimplemented;
	}

	dmsg("Looking for encoding...");
	Output* output = conf->outputs[0];
	Field* rootfield = output->field;
	if (conf->outputs[0]->field->type != SWE_DataStream){
		errmsg("expected DataStream, got type %d", conf->outputs[0]->field->type);
		return swe_unimplemented;
	}
	DataStream* ds = (DataStream*)rootfield->component;
	if (ds->encoding->type != SWE_TextEncoding) {
		errmsg("Unimplemented Encoding Type");
		return swe_unimplemented;
	}
	// Get check data integrity option
	if (ds->encoding->type == SWE_TextEncoding) {
		TextEncoding* e = (TextEncoding*)ds->encoding->encoding;
		internal_data->collapse_white_spaces = e->collapseWhiteSpaces;
	} else {
		internal_data->collapse_white_spaces = FALSE; // collapseWhiteSpaces only useful for TextEncoding
	}

	if (ds->field->type != SWE_DataRecord ){
		return swe_unimplemented;
	}

	DataRecord* dr = (DataRecord*)ds->field->component;
	dmsg("DataRecord id %s", dr->id);
	dmsg("got %d fields", ARRAY_LENGTH(dr->fields));


	// Creating ResponseStructure //
	internal_data->response = response_structure_constructor(conf->outputs[0]->field, FALSE);
	if (internal_data->response == NULL ){
		errmsg("Couldn't create Response Structure, exit");
		return swe_error;
	}

	print_response_structure(internal_data->response);
	internal_data->buffer.size = calculate_buffer_size(internal_data->response);
	imsg("Allocating %d bytes for Instrument Command Buffer", internal_data->buffer.size);
	internal_data->buffer.buffer = swe_malloc(internal_data->buffer.size);
	internal_data->buffer.count = 0;
	internal_data->buffer.index = 0;


	// Creating a Data Structure for the result //
	SchedulerData *d = NULL;
	if (conf->outputs != NULL) {
		imsg("Generating data structure from output %s", conf->outputs[0]->name);
		d = TRY_NULL(scheduler_data_from_response_structure(internal_data->response));
		// print_scheduler_data(d);
	}
	internal_data->data = d;


	//internal_data->response = transform_scheduler_data_to_response_structure(internal_data->data);

	dmsg("Initializing Internal Sensor %d", internal_data->sensor_index);
	TRY_RET(internal_sensors_initialize(internal_data->sensor_index));
	cimsg(GRN, "internal sensors process created successfully");
	dmsgn( "\n==================================================\n\n");
	mem_status();

	return swe_ok;
}




SchedulerData* internal_sensors_exec(void* process_pointer, SchedulerData* nothing, ProcessStatus *errorcode){
	SchedulerProcess* process=(SchedulerProcess*)process_pointer;
	InternalSensorsData* internal_data=(InternalSensorsData*)process->internal_data;
	SchedulerData* data =internal_data->data;
	StateRegister* state_register = &process->state_reg;
	StreamBuffer* buff = &internal_data->buffer;
	char datetime[64];
	int ret;
	// uchar exec_flag = state_register->exec_flag;

	state_register->exec_flag = 0; // clear the flag, a local copy is still available


	*errorcode = process_chain_continue;

	internal_data->data->init_timestamp = swe_get_epoch_time();
	TRY_RET_NULL(ascii_datetime(datetime, internal_data->data->init_timestamp, FALSE));
	dmsg("timstamp %s", datetime);

	parser_retcode retcode;

	// Step 1: Fill the buffer with the internal sensors response
	empty_buffer(buff);
	imsg("Reading internal Sensors (index %d)...", internal_data->sensor_index);
	ret = internal_sensors_read(internal_data->sensor_index, buff->buffer, buff->size);
	if (ret < 1) {
		errmsg("internal_sensors_read returned less than 1 byte: %d", ret);
		*errorcode = process_chain_stop;
		return NULL;
	}
	buff->count = ret; // set the number of bytes in buffer

	// Step 2: Parse the stream
	retcode = stream_parser(buff, internal_data->response);
	// Step 3a: If the stream is not complete set a timeout and exit

	if (retcode == stream_successfully_parsed) {
		cimsg(GRN,"Stream successfully parsed");
		*errorcode = process_chain_continue  ;
		if (readjust_buffer(buff) > 0 ) {
			// if there's still data in buffer it's possible to have a complete response
			process->state_reg.exec_flag |= INTERNAL_RETRIGGER; // Exec it again
			dmsg("Setting INTERNAL_RETRIGGER");
		}
	} else {
		errmsg("Errors while parsing the internal sensors response");
		*errorcode = process_chain_stop;
		return NULL;

	}
	return data;
}


