/*
 ============================================================================
								insert_result.c
 ============================================================================
 This file contains the implementation of a module to generate inserResult
 files in XML or EXI formats.
 ============================================================================
 by SARTI
 Author: Enoc Martínez
 ============================================================================
 */

#include "modules/insert_result.h"

#include "common/swe_utils.h"
#include "core/simple_process.h"
#include "resources/resources.h"
#include "swe_conf.h"



#define DEBUG_THIS_MODULE OFF

#if DEBUG_THIS_MODULE
/*
 * Wrap debug message functions
 */
#define DBGM(...) dmsg(__VA_ARGS__) // DEBUG this module
#else
#define DBGM(...)
#endif



//******************* Private functions Declarations *******************//
int generate_new_output_file(SchedulerProcess* process);
int store_temp_data(InsertResultData* internal_data, char* buffer);
int write_exi_string_size(swe_file* file, uint next_string_size );
int check_valid_timestamp(InsertResultData* internal_data, SchedulerData* data);


//******************** Public functions functions  *********************//

/*************************************************************************
 	 	 	 	 	 	 	insert_result_constructor
 *************************************************************************
 * This function generates a new insert_result modules.
 *
 * STEP 1: Allocate memory for the process and set its common information
 * STEP 2: Allocate memory for the internal data structure
 * STEP 3: Assign execution pointer
 * STEP 4: Make visible internal parameters with the function
 * 		   "add_settings_to_process"
 * STEP 5: set the default values for the internal values
 * STEP 6 Auto-configure process settings with the function
 *        "configure_process_settings"
 * STEP 7 Setup file information
 *
 *************************************************************************/
int insert_result_constructor(SimpleProcess* conf, SchedulerProcess* process){
	//SchedulerProcess* process=NULL;

	char temp_template[MAX_TEMPLATE_SIZE];

	msg_sub_title( WHT, "Insert Result Constructor");

	//STEP 2: Allocate memory for the internal data structure
	dmsg( "allocating space for internal data...");
	InsertResultData* internal_data = swe_malloc(sizeof(InsertResultData));
	process->internal_data=(void*)internal_data;

	//STEP 3: Assign execution pointer
	dmsg( "assigning execution pointer...");
	process->exec=insert_result_exec; //<======== Assign there the execution handler


	//STEP 4: Make visible internal parameters with the function "add_settings_to_process"
	dmsg( "Adding settings...");
	process->settings=add_settings_to_process(process->settings, SWE_Count, "parameters/measureBufferSize", (void*)(&internal_data->measure_buffer_size));
	process->settings=add_settings_to_process(process->settings, SWE_Quantity, "parameters/recordingTime", (void*)(&process->state_reg.sampling_rate));
	process->settings=add_settings_to_process(process->settings, SWE_Count, "parameters/numberOfMeasures", (void*)(&internal_data->number_of_measures));
	process->settings=add_settings_to_process(process->settings, SWE_Quantity, "parameters/timeout", (void*)(&internal_data->timeout));
	process->settings=add_settings_to_process(process->settings, SWE_Quantity, "parameters/recordingTime", (void*)(&process->state_reg.sampling_rate));
	process->settings=add_settings_to_process(process->settings, SWE_Boolean, "parameters/EXIencoding", (void*)(&internal_data->exi_encoding));
	process->settings=add_settings_to_process(process->settings, SWE_Boolean, "parameters/msecTimestamp", (void*)(&internal_data->msec_timestamp));
	process->settings=add_settings_to_process(process->settings, SWE_Count, "parameters/maxFileSize", (void*)(&internal_data->max_file_size));
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/temporalPath", (void*)(&internal_data->temp_path));
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/outputPath", (void*)(&internal_data->out_path));
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/template", (void*)(&temp_template));
	process->settings=add_settings_to_process(process->settings, SWE_Boolean, "parameters/georeference", (void*)(&internal_data->georeference));
	process->settings=add_settings_to_process(process->settings, SWE_Boolean, "parameters/altitude", (void*)(&internal_data->altitude));
	process->settings=add_settings_to_process(process->settings, SWE_Count, "parameters/decimalPrecision", (void*)(&internal_data->float_precision));
	process->settings=add_settings_to_process(process->settings, SWE_Boolean, "parameters/timeRange", (void*)(&internal_data->time_range));


	//STEP 5: set the default values for the internal values
	internal_data->measure_buffer_size=DEFAULT_MEASURE_BUFFER_SIZE;
	internal_data->recording_time=DEFAULT_RECORDING_TIME;
	internal_data->exi_encoding=DEFAULT_EXI_ENCODING;
	internal_data->max_file_size=DEFAULT_MAX_FILE_SIZE;

	strcpy(internal_data->out_path, OUTPUT_FOLDER_PATH);
	strcpy(internal_data->temp_path, TEMP_FOLDER_PATH);

	internal_data->georeference = DEFAULT_GEOREFERENCED;
	internal_data->altitude = DEFAULT_DEPTH_REFERENCED;

	internal_data->float_precision = DEFAULT_FLOAT_PRECISION;

	temp_template[0] = 0;

	internal_data->stored_measures=0;
	internal_data->temp_file_size=0;

	//STEP 6 Auto-configure process settings with the function "configure_process_settings"
	dmsg( "Configuring settings...");
	TRY_RET(configure_process_settings(conf->settings, process));



	if(temp_template[0]==0) {
		errmsg( "template not specified. InsertResult file must have a template, exit");
		return swe_error;
	}


	// Check if the Exec Modes are correct, interface interrupt is not allowed
	if (process->state_reg.exec_modes.iface_interrupt){
		errmsg("Trying to set interface interrupt in Insert Result module! ignoring");
		process->state_reg.exec_modes.iface_interrupt = FALSE;
	}


	sprintf(internal_data->temp_filename, "%s%s%s%s",internal_data->temp_path, PATH_SEPARATOR, process->id, TEMP_FILE_FORMAT);

	//STEP 7 Setup file information:
	/*The output file has the following strcuture:
	 * 1. Header (fixed)
	 * 2. Template (dependant on the configuration)
	 * 3. After-template (fixed)
	 * 4. Measures (genererated by the swe_bridge)
	 * 5. Footer
	 *
	 * Depending on the encoding (EXI or XML) the header, after template
	 * and footer may change.
	 *
	 */
	if(internal_data->exi_encoding == FALSE){ // Use XML encoding

		internal_data->header = (uchar*)XML_INSERT_RESULT_HEADER;
		internal_data->header_size = XML_INSERT_RESULT_HEADER_SIZE;

		internal_data->after_template = (uchar*)XML_AFTER_TEMPLATE;
		internal_data->after_template_size = XML_AFTER_TEMPLATE_SIZE;

		internal_data->footer = (uchar*)XML_FOOTER;
		internal_data->footer_size = XML_FOOTER_SIZE;
	}
	else { //Use EXI encoding
		uchar header[] = EXI_HEADER;
		internal_data->header_size = EXI_HEADER_SIZE;
		internal_data->header = swe_malloc(internal_data->header_size);
		memcpy(internal_data->header, header, EXI_HEADER_SIZE);



		uchar after_template[] = EXI_AFTER_TEMPLATE;
		internal_data->after_template_size = EXI_AFTER_TEMPLATE_SIZE;
		internal_data->after_template = swe_malloc(EXI_AFTER_TEMPLATE_SIZE);
		memcpy(internal_data->after_template, after_template, EXI_AFTER_TEMPLATE_SIZE);


		uchar footer[] = EXI_FOOTER;
		internal_data->footer_size = EXI_FOOTER_SIZE;
		internal_data->footer = swe_malloc(EXI_FOOTER_SIZE);
		memcpy(internal_data->footer, footer, EXI_FOOTER_SIZE);


	}
		//Setting up the template
	internal_data->template = set_string(temp_template);
	internal_data->template_size = strlen(temp_template);

	internal_data->headers_size=internal_data->header_size + internal_data->template_size + internal_data->after_template_size + internal_data->footer_size;
	internal_data->temp_file_size=0;

	cimsg(GRN, "insert result process created successfully");

	// Create temporal path (if it did not exist)
	if (swe_create_dir(internal_data->temp_path) != swe_ok){
		warnmsg("Couldn't create the temporal path");
	}
	system_notify(SWEB_NOTIFY_TEMP_FOLDER, (void*)internal_data->temp_path);
	// Create output path (if it did not exist)
	if (swe_create_dir(internal_data->out_path) != swe_ok){
		warnmsg("Couldn't create the output path");
	}
	system_notify(SWEB_NOTIFY_OUTPUT_FOLDER, (void*)internal_data->out_path);

	// Check if there's a temp file already //
	dmsg("cheking if temp file still exists...");
	if (internal_data->max_file_size > 0) {
		swe_file *tmp_file = swe_fopen(internal_data->temp_filename, swe_read_file);
		if (tmp_file != NULL) {	 // do nothing
			// if there's a data file
			if ((internal_data->temp_file_size = swe_file_size(tmp_file)) < 0){
				errmsg("Couldn't get the exising temp file size");
				internal_data->temp_file_size = 0;

			} else {
				dmsg("%d bytes in temp file already!", internal_data->temp_file_size);
			}
			swe_fclose(tmp_file);
		}
	}

	// Check that that files are generated by timer or by size
	if ( ( process->state_reg.sampling_rate < 0 )  &&
	     ( internal_data->max_file_size <= 0 ) &&
		 ( internal_data->number_of_measures < 1 ) ) {
		errmsg("InsertResult %s should have or recordingTime or maxFileSize or numberOfMeasures option set!", process->name);
		return swe_invalid_arguments;
	}

	process->state_reg.exec_flag = 0;

	dmsgn( "==================================================\n\n");
	return swe_ok;
}




SchedulerData* insert_result_exec(void* process_pointer, SchedulerData* data, ProcessStatus *errorcode){

	SchedulerProcess* process=(SchedulerProcess*)process_pointer;
	InsertResultData* internal_data=(InsertResultData*)process->internal_data;
	uchar exec_flag = process->state_reg.exec_flag; // copy the exec flags
	int bytes;
	process->state_reg.exec_flag = 0;

	char buffer[internal_data->measure_buffer_size];
	*errorcode = process_chain_error;


	// Scheduler triggers time-based output files
	if (exec_flag & SCHEDULER_TRIGGER) {
		//if the process is triggered by tge scheduler, generate a new output file
		if(internal_data->stored_measures > 0) {
			cimsg(BLU,"IRE - Creating a new file (timer expired)");
			if (generate_new_output_file(process_pointer)!= swe_ok){
				errmsg("couldn't generate new output file");
				swe_fclose(internal_data->temp_file);
				internal_data->temp_file = NULL;
			}
			*errorcode = process_chain_continue; // Exec next process
		} else{
			warnmsg( "ignoring create output, nothing to save");
			*errorcode = process_chain_stop;
		}
	}

	// Scheduler triggers time-based output files
	if (exec_flag & INTERNAL_TIMEOUT_TRIGGER) {
		//if the process is triggered by tge scheduler, generate a new output file
		if(internal_data->stored_measures > 0) {
			cimsg(BLU,"IRE - Creating a new file (timeout expired)");
			if (generate_new_output_file(process_pointer)!= swe_ok){
				errmsg("couldn't generate new output file");
				swe_fclose(internal_data->temp_file);
				internal_data->temp_file = NULL;
			}
			*errorcode = process_chain_continue; // Exec next process
		} else{
			warnmsg( "ignoring create output, nothing to save");
			*errorcode = process_chain_stop;
		}
	}

	if (exec_flag & PREVIOUS_PROCESS_TRIGGER) {
		*errorcode = process_chain_stop;
		TRY_RET_NULL(check_valid_timestamp(internal_data, data));
		memset(buffer, 0, internal_data->measure_buffer_size); //empty the measure buffer
		dmsg( "IRE - generating a new data string");

		if (( bytes = data_string_from_scheduler_data(data, buffer, "#", "@", internal_data->georeference, internal_data->altitude,
				internal_data->float_precision, internal_data->msec_timestamp, internal_data->time_range)) < 0 ) {
			errmsg("error generating data string");
			return NULL;
		}
		DBGM("Measure: [%s]", buffer);
		DBGM("Length %d", bytes);

		// Check the Condition for Max File Size //
		// If Max File Size, first generate output, then store the measure in the new file
		if (internal_data->max_file_size > 0) {
			uint new_size = (internal_data->temp_file_size + internal_data->headers_size + strlen(buffer));
			dmsg("IRE - Checking size condition before storing new string (%d of %d)", new_size, internal_data->max_file_size);
			if (internal_data->max_file_size < new_size ) {
			//If the expected size (current size + footer size) is greater than the max
			//generate new file. If the max_size is 0, ignore this condition
				cimsg(BLU,"IRE - Creating a new file (max size %d)", internal_data->max_file_size);
				if (generate_new_output_file(process) != swe_ok) {
					errmsg("couldn't generate output file, abort");
					return NULL;
				}
				*errorcode = process_chain_continue; // Exec next process
			}
		}


		// Once the new file is created //
		imsg( "IRE - Saving data to a temporal file...");
		if(store_temp_data(internal_data, buffer)!=swe_ok){
			return NULL;
		}
		if ( *errorcode != process_chain_continue ) { // Do not overwrite success
			*errorcode = process_chain_stop;
		}

		if (internal_data->timeout > 0) {
			// Set the timeout to generate a file when the timer expires //
			process_timeout_set(&process->state_reg, internal_data->timeout);
		}

		// Check Conditions for Number of Measures //
		if (internal_data->number_of_measures > 0) {

			if(internal_data->stored_measures  >= internal_data->number_of_measures){
				// Generate output file //
				process_timeout_cancel(&process->state_reg); // file is generated, timeout can be disabled
				cimsg(BLU,"IRE - Creating a new file");
				if (generate_new_output_file(process) != swe_ok) {
					errmsg("couldn't generate output file, abort");
					return NULL;
				}
				*errorcode = process_chain_continue;
			}
			else if (internal_data->timeout > 0) {
				// Set the timeout //
				process_timeout_set(&process->state_reg, internal_data->timeout);
			}
		} else {

		}

	}

	return data;
}


/*************************************************************************
 	 	 	 	 	 	 	store_temp_data
 *************************************************************************
 * This function stores a measure (stored in the buffer) into the temporal
 * file pointed by the pointer internal_data->temp_file. If the file does
 * not exist, it is created.
 *************************************************************************/
int store_temp_data(InsertResultData* internal_data, char* buffer){
	int nbytes;
	if(internal_data->temp_file==NULL){
		//some error ocurred and there isn't temporal file

		internal_data->temp_file=swe_fopen(internal_data->temp_filename, swe_append_file);
		if(internal_data->temp_file==NULL){
			errmsg( "couldn't create temp file again");
			return swe_error;
		}
	}

	nbytes=swe_fwrite(buffer, strlen(buffer),internal_data->temp_file);
	if(nbytes < 0){
		errmsg( "swe_fwrite returned %d", nbytes);
		swe_fclose(internal_data->temp_file);
		internal_data->temp_file = NULL;
		return swe_filesystem_error;
	}
	internal_data->temp_file_size+=nbytes;
	internal_data->stored_measures++;
	dmsg("IRE - temp file size: %d bytes (%d measures)", internal_data->temp_file_size, internal_data->stored_measures);
	dmsg("IRE - Stored: [%s]", buffer);

	swe_fclose(internal_data->temp_file);
	internal_data->temp_file = NULL;
	return swe_ok;
}


/*
 * This functino copy "size" bytes from "infile" to "outfile". The amount
 * of bytes copied at a time is determined by the COPY_BUFFER_SIZE macro
 */
int temp_to_new_file(swe_file* infile, swe_file* outfile, uint size){
	char buffer[COPY_BUFFER_SIZE];
	int requested_bytes, remaining_bytes=size, ret, n;
	while(remaining_bytes>0){
		requested_bytes=MIN(COPY_BUFFER_SIZE, remaining_bytes);
		if((ret=swe_fread(buffer, requested_bytes, infile))<requested_bytes){
			errmsg("swe_fread returned %d, expected %d", ret, requested_bytes);
			return swe_filesystem_error;
		}
		remaining_bytes-=requested_bytes;
		if((n=swe_fwrite(buffer, ret,  outfile))!=requested_bytes){
			errmsg("swe_fread returned %d, expected %d", n, requested_bytes);
		}
		DBGM("writing %d bytes...", n);
	}
	DBGM("File copied");
	return swe_ok;
}

/*
 * This function fills the temp file with the footer (last element required in a valid O&M file),
 * and moves it from the temp filename to the proper output file
 */

int generate_new_output_file(SchedulerProcess* process){

	char new_filename[FILENAME_LENGTH];
	char date[TIMESTAMP_LENGTH];
	char* p;
	int ret;
	InsertResultData* data=(InsertResultData*)process->internal_data;
	swe_file* output_file = NULL;


	//---- Generate output file name ----//
	DateTime datetime;
	TRY_RET(swe_get_datetime(&datetime));
	sprintf(date, "%04d%02d%02d_%02d%02d%02d", datetime.year, datetime.month,
			datetime.day,	datetime.hour, datetime.minute, datetime.second);
	memset(new_filename, 0, FILENAME_LENGTH);
	p=new_filename;
	p=fast_strcat(p, data->out_path);
	p=fast_strcat(p, PATH_SEPARATOR);
	p=fast_strcat(p, process->id);
	p=fast_strcat(p, "_");
	p=fast_strcat(p, date);
	if(data->exi_encoding==FALSE){
		p=fast_strcat(p, (char*)XML_FORMAT);
	} else { //EXI encoding
		p=fast_strcat(p, (char*)EXI_FORMAT);
	}


	if (data->temp_file != NULL) {
		// Closing temp file (currently in append mode)
		dmsg("IRE - File was opened, closing");
		swe_fclose(data->temp_file);
	}
	// Open temp file in read mode
	if ((data->temp_file = swe_fopen(data->temp_filename, swe_read_file)) == NULL ) {
		errmsg("Couldn't open file %s", data->temp_filename);
		imsg("IRE - Trying to remove temp file...");
		swe_fremove(data->temp_filename);
		data->temp_file = NULL;
		data->stored_measures=0;
		data->temp_file_size=0;
		return swe_filesystem_error;
	}


	// Opening output file //
	imsg("IRE - Creating file: %s", new_filename);
	if ((output_file = swe_fopen(new_filename, swe_write_file)) == NULL) {
		errmsg("Couldn't open file %s", new_filename);
		swe_fclose(data->temp_file);
		return swe_filesystem_error;
	}

	DBGM("IRE - writing header...");
	if ((ret = swe_fwrite(data->header, data->header_size, output_file)) != data->header_size){
		errmsg("error in fwrite, returned %d (file %s line %d)",ret, __FILE__, __LINE__);
		swe_fclose(data->temp_file);
		swe_fclose(output_file);


	}

	if(data->exi_encoding==TRUE) {// if using EXI, write the string length.
		dmsg("IRE - Writing EXI string size...");
		write_exi_string_size(output_file, data->template_size);
	}
	DBGM("IRE - writing template...");
	if ((ret = swe_fwrite(data->template, data->template_size, output_file)) != data->template_size){
		errmsg("error in fwrite, returned %d (file %s line %d)",ret, __FILE__, __LINE__);
		swe_fclose(data->temp_file);
		swe_fclose(output_file);
	}

	DBGM("IRE - writing after template...");
	if ((ret = swe_fwrite(data->after_template, data->after_template_size, output_file)) != data->after_template_size ){
		errmsg("error in fwrite, returned %d (file %s line %d)",ret, __FILE__, __LINE__);
		swe_fclose(data->temp_file);
		swe_fclose(output_file);
	}



	// Write the length of the data string if EXI
	if( data->exi_encoding == TRUE ) {// if using EXI, write the string length
		DBGM("IRE - writing exi string size...");
		write_exi_string_size(output_file, data->temp_file_size);
	}

	// Copy from one file to the other
	DBGM("IRE - copying from one file to another..");

	if ((temp_to_new_file(data->temp_file, output_file ,data->temp_file_size)) != swe_ok){
		errmsg("error in fwrite, (file %s line %d)", __FILE__, __LINE__);
		swe_fclose(data->temp_file);
		swe_fclose(output_file);
	}


	// Write footer to the file
	if ((ret = swe_fwrite(data->footer, data->footer_size, output_file)) != data->footer_size){
		errmsg("error in fwrite, returned %d (file %s line %d)",ret, __FILE__, __LINE__);
		swe_fclose(data->temp_file);
		swe_fclose(output_file);
	}

	// Close the output file
	swe_fclose(output_file);

	// Close the temp file
	swe_fclose(data->temp_file);
	data->temp_file = NULL;

	DBGM("IRE - Removing old temp file");
	swe_fremove(data->temp_filename);
	data->temp_file = NULL;

	data->stored_measures=0;
	data->temp_file_size=0;
	system_notify(SWEB_NOTIFY_NEW_DATA_FILE, (void*)new_filename);
	return swe_ok;
}



/*************************************************************************
 	 	 	 	 	 	 	write_exi_string_size
 *************************************************************************
 * EXI strings are length-prefixed. This function takes a string length as
 * argument and encodes its length into a file. This function should be
 * called before writing a string into the EXI file.
 *
 * According to the EXI standard "The Unsigned Integer datatype representation
 * supports unsigned integer numbers of arbitrary magnitude. It is represented
 * as a sequence of octets terminated by an octet with its most significant
 * bit set to 0. The value of the unsigned integer is stored in the least
 * significant 7 bits of the octets as a sequence of 7-bit bytes, with the
 * least significant byte first"
 *
 *************************************************************************/
int write_exi_string_size(swe_file* file, uint next_string_size ){

	uchar string_token[10];
	uchar bytes=0;
	uint mask=127; //0b01111111
	next_string_size+=2;

	string_token[bytes]=mask&next_string_size; //force 7 bit octet

	while((next_string_size/=128)) { //divide by 128 and check if there's more bytes to be written
		string_token[bytes]|=128; // force the most significant bit to 1 to indicate that there's another byte
		bytes++;
		string_token[bytes]=mask&next_string_size;
	}
	bytes++;

	if(file!=NULL)swe_fwrite(string_token, bytes, file);
	else return swe_filesystem_error;

	return swe_ok;
}


/*
 * This functions checks if the new incoming timestamp is valid. If not checked
 * duplicated timestamps may result in a SOS error
 */
int check_valid_timestamp(InsertResultData* internal_data, SchedulerData* data){
	// Check last timestamp //
	ulong current_timestamp;
	if (internal_data->msec_timestamp == TRUE ) {
		current_timestamp = 1000*(ulong)data->init_timestamp;
	} else {
		current_timestamp = (ulong)data->init_timestamp;
	}

	if (current_timestamp == internal_data->last_timestamp) {
		warnmsg("Duplicated timestamp");
		return swe_timer_error;
	}

	// Copy the new timestamp
	internal_data->last_timestamp = current_timestamp;
	return swe_ok;
}


