/*
 ============================================================================
								insert_result.c
 ============================================================================
 Takes incoming data and applies linear calibration to this data:
 Linear calibration y = slope·x + offset
 ============================================================================
 by SARTI
 Author: Enoc Martínez
 ============================================================================
 */

#include "modules/linear_calibration.h"

#include "common/stream_parser.h"
#include "common/swe_utils.h"
#include "core/simple_process.h"
#include "swe_conf.h"


//******************* Private functions Declarations *******************//



int initialize_linear_calibration_data(LinearCalibration** calibrations, SchedulerData* data);
int apply_linear_calibration(SWE_Data* datafield, LinearCalibration* calibration);

int linear_calibration_constructor(SimpleProcess* conf, SchedulerProcess* process){
	//SchedulerProcess* process=NULL;
	int i;

	msg_sub_title( WHT, "Linear Calibration Result Constructor");

	//STEP 2: Allocate memory for the internal data structure
	dmsg( "allocating space for internal data...");
	LinearCalibrationData* internal_data=swe_malloc(sizeof(LinearCalibrationData));
	process->internal_data=(void*)internal_data;


	//STEP 3: Assign execution pointer
	dmsg( "assigning execution pointer...");
	process->exec=linear_calibration_exec; //<======== Assign there the execution handler



	//STEP 4: Make visible internal parameters with the function "add_settings_to_process"
	dmsg( "Adding settings...");
	// no settings,


	//STEP 5: set the default values for the internal values
	// no default values

	// Check for addCalibration parameters. This parameter registers a new calibration to the list
	// the value should be "FieldName slope offset" (<text> <quantity> <quantity>)
	for (i=0; conf->settings[i]!=NULL ; i++){
		uint n;
		Setting* s = conf->settings[i];
		// Check that we are dealing with inputs //
		if ( !compare_strings(s->ref, REGISTER_COEFFICIENT_PARAMETER_REF) ) {

			LinearCalibration * c = swe_malloc(sizeof(LinearCalibration));
			char** substrings = get_substrings(s->value, " ", &n);
			if ( n != 3) {
				errmsg("Expected 3 parameters, got %d", n);
				errmsg("addCalibration parameter should be \"fieldName slope offset\"");
				return swe_invalid_arguments;
			}
			c->fieldName = set_string(substrings[0]);
			c->slope = strtod(substrings[1], NULL);
			c->offset = strtod(substrings[2], NULL);
			imsgn("CAL - Field");
			cimsgn(BLU, " %s", c->fieldName);
			imsgn(", slope:  "); cimsgn(CYN, "%f", c->slope);
			imsgn(", offset: "); cimsg(CYN, "%f", c->offset);

			// Check for floats very very small (if strtod fails a 0.000000 is returned) //
			if (c->slope < DOUBLE_TOO_SMALL_LIMIT && c->slope > -DOUBLE_TOO_SMALL_LIMIT ) {
				warnmsg("\"%s\" slope is %f, possible error in conversion!", c->fieldName, c->slope);
			}
			if (c->offset < DOUBLE_TOO_SMALL_LIMIT && c->offset > -DOUBLE_TOO_SMALL_LIMIT ) {
				warnmsg("\"%s\" offset is %f, possible error in conversion!", c->fieldName, c->offset);
			}

			internal_data->calibrations = ATTACH_TO_ARRAY(internal_data->calibrations, c);


		} else {
			errmsg("Expected parameter with ref=\"%s\", however received ref=\"%s\"",
					REGISTER_COEFFICIENT_PARAMETER_REF, s->ref);
			imsg("Ignoring invalid parameter");
		}
	}

	dmsgn( "==================================================\n\n");
	return swe_ok;
}



/*
 * This function applies a linear calibration to all input parameters (valid data)
 *     y = coefficient*x + offset
 *
 *         where x is input data and y output
 */
SchedulerData* linear_calibration_exec(void* process_pointer, SchedulerData* scheduler_data, ProcessStatus *errorcode){
	SchedulerProcess* process=(SchedulerProcess*)process_pointer;
	LinearCalibrationData* internal_data=(LinearCalibrationData*)process->internal_data;
	int i;


	process->state_reg.exec_flag = 0;

	// Check if data exists //
	if (scheduler_data == NULL) {
		return NULL;
	}

	// Initialize the calibrations
	if (internal_data->initialized == FALSE) {
		TRY_RET_NULL(initialize_linear_calibration_data(internal_data->calibrations, scheduler_data ));
		internal_data->initialized = TRUE;
	}

	for ( i=0 ; internal_data->calibrations[i]!=NULL ; i++ ){
		LinearCalibration* c = internal_data->calibrations[i];
		TRY_RET_NULL(apply_linear_calibration(scheduler_data->fields[c->index], c));
	}
	for ( i=0 ; scheduler_data->fields[i]!=NULL ; i++) {
		SWE_Data* d = scheduler_data->fields[i];
		dmsgn("CAL - Field ");
		cdmsgn(BLU,"%s ", d->name);
		print_swe_data(d);
	}
	return scheduler_data;
}



/*
 * This functions assigns the scheduler data index to each LinearCalibration structure
 */
int initialize_linear_calibration_data(LinearCalibration** calibrations, SchedulerData* data){
	int i, j;
	imsg("CAL - Initializing Linear Calibration...");
	for (i=0 ; calibrations[i]!=NULL ; i++) {
		uchar found = FALSE;
		for ( j=0 ; data->fields[j] ; j++ ){
			if (!compare_strings(calibrations[i]->fieldName, data->fields[j]->name)){
				imsg("CAL - Assigning calibration to field %s", calibrations[i]->fieldName);
				found = TRUE;
				calibrations[i]->index = j; // assign the index
			}
		}
		if (found == FALSE) {
			errmsg("Field %s not found", calibrations[i]->fieldName);
			return swe_element_not_found;
		}
	}
	return swe_ok;
}



/*
 * Apply calibration to SWE Data
 */
int apply_linear_calibration(SWE_Data* datafield, LinearCalibration* calibration){
	float64 raw_data, processed_data;
	raw_data = swe_data_to_double(datafield);
	// Apply the calibration //
	processed_data = raw_data*calibration->slope + calibration->offset;
	TRY_RET(double_to_swe_data(processed_data, datafield));
	return swe_ok;
}

