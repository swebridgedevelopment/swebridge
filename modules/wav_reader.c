/*
 * This file implements a module to read WAV files and pass the samples
 * to further processes. It's main utility is validation of other processing
 * algorithms such as SPL.
 *
 * @author: Enoc Martínez
 * @institution: Universitat Politècnica de Catalunya (UPC)
 * @contact: enoc.martinez@upc.edu
 */

#include "swe_conf.h"
#if ENABLE_HIGH_FREQUENCY_MODULES
#include "resources/resources.h"
#include "resources/hydrophone.h"
#include "resources/formats/wave.h"
#include "common/swe_utils.h"
#include "common/sensorml.h"
#include "common/ring_buffer.h"
#include "common/high_freq_stream.h"
#include "modules/wav_reader.h"
#include <math.h>

//---- Private functions -----//
int generate_timestamp_pattern(const char* pattern_string, TimestampPattern* pattern);
int force_timezone(TimestampPattern* pattern, float timezone);
int wav_registry_free(WavRegistry* registry);
WavRegistry* wav_registry_generate(const char* dirpath, TimestampPattern* pattern);
int pattern_to_timestamp(const char* string, TimestampPattern* pattern, float64 *timestamp);

float64 init_process, end_process, init_total, end_total;



/*
 * Constructor for Hydrophone Stream. This module processes incoming binary streaming data
 * from a hydrophone, converts it to pressure samples and stores it in a buffer for further
 * processes.
 *
 */
int wav_reader_constructor(SimpleProcess* conf, SchedulerProcess* process){
	WavReaderData* params = swe_malloc(sizeof(WavReaderData));
	HydrophoneParameters* hydparams = &params->params;
	PhysicalSystem* system = (PhysicalSystem*)conf->physical_system;
	float64 timezone = -999.0;
	char pattern_string[512];
	process->internal_data = params;

	msg_sub_title( WHT, "WAV Reader Constructor");

	// Assign pointers
	process->exec = wav_reader_exec;

	// Set default values to Hydrophone parameters //
	hydparams->sensitivity = -1;
	hydparams->amplifier_gain = -1;
	hydparams->sampling_rate = -1;
	hydparams->adc_bits = 16;
	hydparams->adc_vref = -1;
	hydparams->adc_bipolar = TRUE;

	params->accumulate_samples = DEFAULT_ACCUMULATE_SAMPLES_WR;
	timezone = -999;

	// Adding settings to process //
	process->settings=add_settings_to_process(process->settings, SWE_Count, "parameters/accumulateSamples", (void*)&params->accumulate_samples);
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/inputFiles", (void*)(&params->dirpath));
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/timestampPattern", (void*)(&pattern_string));
	process->settings=add_settings_to_process(process->settings, SWE_Quantity, "parameters/timezone", (void*)(&timezone));

	// Process Settings //
	dmsg( "Configuring settings...");
	if(configure_process_settings(conf->settings, process)!=swe_ok) {
		errmsg( "configuration failed");
		return swe_error;
	}

	dmsg("Working on folder %s", params->dirpath);
	TRY_RET(generate_timestamp_pattern(pattern_string, &params->pattern));
	if (timezone > -999.0) {
		imsg("Forcing timezone to %f", timezone);
		TRY_RET(force_timezone(&params->pattern, timezone));
	}

	dmsg("WVR - Allocating memory for SchedulerData");
	params-> data = swe_malloc(sizeof(SchedulerData));

	imsg("Setting WAV reader as init process");
	process->state_reg.exec_modes.initialization = TRUE;


	imsg("WVR - Generating wav file registry...");
	params->registry = TRY_NULL(wav_registry_generate(params->dirpath, &params->pattern));

	dmsg("Using first WAV file sample size to determine the hydrophone sensitivity");

	if ( params->registry->count > 0 ) {
		WavRegistryFile* first = params->registry->files[0];
		WavReadFile* wavfile = TRY_NULL(wave_open(first->filename, 100000));
		uint sample_size = wavfile->header.sample_length;
		HighFreqStreamBuffer* streambuff;
		params->accumulate_samples = wavfile->header.sample_rate / 2;
		TRY_RET(wave_close(wavfile));
		TRY_RET(get_hydrophone_parameters(system, sample_size, hydparams));
		TRY_RET(calculate_conversion_coefficient(hydparams, &params->conversion_coefficient));
		imsg("Conversion coefficient %f [µPa/counts]", params->conversion_coefficient);
		imsg("Accumulating %d samples (0.25 seconds)", params->accumulate_samples);
		streambuff = high_freq_stream_buffer_from_wave(wavfile, params->accumulate_samples, params->conversion_coefficient);
		params->data->streambuff = streambuff;

	} else {
		errmsg("WAV file registry is empty");
		return swe_error;
	}

	init_total = swe_get_epoch_time();
	return swe_ok;

}


/*
 * Execution handler for Wav Reader module
 *
 * Execution:
 *     INTERNAL_RETRIGGER: This flag is set when the output buffer has been filled. Before
 *                         continue execution it has to be emptied
 *     INTERNAL_TIMEOUT: There has been a timeout error in the communications interface, reopen it
 */
SchedulerData* wav_reader_exec(void* process_pointer, SchedulerData* nothing, ProcessStatus *errorcode){
	SchedulerProcess* process=(SchedulerProcess*)process_pointer;
	WavReaderData* params=(WavReaderData*)process->internal_data;
	HighFreqStreamBuffer *streambuff = params->data->streambuff;
	StateRegister* state_register = &process->state_reg;
	WavRegistry* registry = params->registry;
	WavReadFile* file;
	uchar exec_flag = state_register->exec_flag;
	state_register->exec_flag = 0; // clear the flag, but a local copy i kept


	// If INTERNAL_RETRIGGER is set it means that the output buffer (HighFreqStreamBuffer)
	// is full and needs to be emptied before processing new incoming data. It is assumed
	// that the rest of the processes have already processed the data
	if ( (exec_flag & SCHEDULER_TRIGGER) || (exec_flag & INTERNAL_RETRIGGER)) {
		registry = params->registry;

		// Open next file in registry
		if ( registry->current_file == NULL && (registry->index < registry->count)) {
			WavRegistryFile* entry = registry->files[registry->index];
			char temp[256];
			ascii_datetime(temp, entry->timestamp, TRUE);
			imsg("WVR - Opening file %d in registry", registry->index);
			imsg("WVR - Timestamp %s", temp);
			WavReadFile* wavfile = CATCH_NULL(wave_open(entry->filename, entry->timestamp));
			init_process = swe_get_epoch_time();
			wave_show_info(wavfile);
			registry->current_file = wavfile;
		}
		// Reset the buffer
		streambuff->nbytes = 0;
		streambuff->nsamples = 0;
		streambuff->timestamps[0] = -1;
		streambuff->timestamp_count = 0;

		// Read enough samples to fill the buffer
		file = registry->current_file;
		if ( file->index < file->samples) {
			int read_samples = MIN((file->samples - file->index), params->accumulate_samples);
			float64 timestamp;
			int nbytes = wave_read_samples(file, streambuff->data, read_samples, &timestamp);
			if ( nbytes < 0 ) {
				errmsg("Wave Read error");
				return NULL;
			}
			streambuff->nbytes += nbytes;
			streambuff->nsamples += params->accumulate_samples;
			streambuff->timestamps[0] = timestamp;
			streambuff->timestamp_count = 1;
			state_register->exec_flag |= INTERNAL_RETRIGGER; // always retrigger, not working in real_time
		}
		else {
			imsg("WVR - Closing file");
			TRY_RET_NULL(wave_close(registry->current_file));
			end_process = swe_get_epoch_time();
			float64 percent = 100*(float64)(registry->index)/registry->count;
			warnmsg("%0.2f%% - %d of %d took %.02f seconds to process", percent, registry->index, registry->count, end_process - init_process);
			registry->current_file = NULL;
			registry->index++;
			*errorcode = process_chain_flush;

			// If all files have been processed, stop
			if ( registry->index == registry->count) {
				imsg("WVR - All files processed");
				end_total = swe_get_epoch_time();
				warnmsg("Total time %.02f s", end_total - init_total);

				registry->current_file = NULL;
				wav_registry_free(registry);
			} else {
				state_register->exec_flag |= INTERNAL_RETRIGGER; // always retrigger, not working in real_time
			}
		}
	}
	return params->data;
}






/*
 * Searches for the "subpattern" in string and fills the PatternElement structure if found. If
 * not found, element not found error is returned
 */
int search_pattern_element(const char* string, const char* subpattern, PatternElement* element){
	int i;
	int len = strlen(subpattern);
	for (i=0 ; i<strlen(string) - len; i++) {
		if (!memcmp(&string[i], subpattern, len)){
			element->length = len;
			element->position = i;
			return swe_ok;
		}
	}
	warnmsg("Timestamp subpattern %s not found!", subpattern);
	return swe_element_not_found;
}

/*
 * Takes a string like: Naxys_YYYY-MM-DDTmm:hh:ss.wav and lloks for the following elements:
 * YY / YYYY : year position (2 / 4 digits)
 * MM: Month
 * DD: Day
 * hh: hour
 * mm: minute
 * ss: second
 * +tz timezone (only hour and symbol e.g. +02, -08)
 *
 *
 * All other chars will be discarded so the generated pattern:
 *     Naxys_YYYY-MM-DDTmm:hh:ss+tz.wav
 * is the same than:
 *     ______YYYY_MM_DD_mm_hh_ss+tz____
 *
 * Timezone can also be forced by passing the tzone argument. If timezone
 *
 */
int generate_timestamp_pattern(const char* pattern_string, TimestampPattern* pattern){
	dmsg("Generating timestamp pattern...");

	// Year can be YY or YYYY
	if (search_pattern_element(pattern_string, "YYYY", &pattern->year) < 0)  {
		TRY_RET(search_pattern_element(pattern_string, "YY", &pattern->year));
	}

	TRY_RET(search_pattern_element(pattern_string, "MM", &pattern->month));
	TRY_RET(search_pattern_element(pattern_string, "DD", &pattern->day));
	TRY_RET(search_pattern_element(pattern_string, "hh", &pattern->hour));
	TRY_RET(search_pattern_element(pattern_string, "mm", &pattern->minute));
	TRY_RET(search_pattern_element(pattern_string, "ss", &pattern->second));
	if (search_pattern_element(pattern_string, "+tz", &pattern->timezone) < 0) {
		dmsg("Timezone not found in pattern, forcing timezone?");
	} else {
		pattern->force_timezone = FALSE;
	}
	dmsg("Timestamp pattern generated!");
	return swe_ok;
}


/*
 * Function to specify the timezone manually. Timezone is in hours
 */
int force_timezone(TimestampPattern* pattern, float timezone){
	dmsg("Forcing timezone to %f", timezone);

	if (timezone > 12 || timezone < -12 ) {
		errmsg("Timezone %d not valid!", timezone);
		return swe_timer_error;
	}

	pattern->force_timezone = TRUE;
	pattern->fixed_timezone = timezone;
	return swe_ok;
}



/*
 * Extracts pattern element from string and stores it in value.
 */
int extract_from_subpattern(const char* string, PatternElement e, int *value){
	char temp[64];
	if ( strlen(string) < e.length + e.position ) {
		errmsg("Can't extract pattern from string \"%s\" (position %d, length %d)", string, e.position, e.length);
		return swe_invalid_arguments;
	}
	memset(temp, 0, 64);
	// copy the pattern element to temp
	memcpy(temp, &string[e.position], e.length);
	*value = atoi(temp);
	if ( *value < 0 ){
		errmsg("Negative value when extracting pattern");
		return swe_timer_error;
	}
	return swe_ok;
}


/*
 * Converts the input string to a timestamp based on a pattern and stores it in *timestamp pointer
 */
int pattern_to_timestamp(const char* string, TimestampPattern* pattern, float64 *timestamp){
    struct tm t;
    float64 epoch_time;
    float64 input_timezone_offset;

    TRY_RET(extract_from_subpattern(string, pattern->year, &t.tm_year));
    if (pattern->year.length == 4 ) { //
    	t.tm_year -= 1900; // Year - 1900
    } else if (pattern->year.length == 2 ) {
    	t.tm_year = t.tm_year + 2000 - 1900; // convert to 4 digit and then to tm
    }

    TRY_RET(extract_from_subpattern(string, pattern->month, &t.tm_mon));
    t.tm_mon -= 1; // month go from 0 to 11 instead of from 1 to 12
    TRY_RET(extract_from_subpattern(string, pattern->day, &t.tm_mday));
    TRY_RET(extract_from_subpattern(string, pattern->hour, &t.tm_hour));
    TRY_RET(extract_from_subpattern(string, pattern->minute, &t.tm_min));
    TRY_RET(extract_from_subpattern(string, pattern->second, &t.tm_sec));

    if (pattern->force_timezone == FALSE ) {
    	int hours;
    	TRY_RET(extract_from_subpattern(string, pattern->month, &hours));
    	t.tm_gmtoff = hours * 3600; // convert from hours to seconds
    	input_timezone_offset = (float64)t.tm_gmtoff;
    } else {
        t.tm_gmtoff = (int)(pattern->fixed_timezone * 3600); // convert from hours to seconds
        input_timezone_offset = (float64)t.tm_gmtoff;
    }

    t.tm_isdst = -1;
	epoch_time = swe_mktime(&t);
	if (epoch_time < 0 ) {
		warnmsg("mktime return negative value %lld", (long long int)epoch_time);
		return swe_timer_error;
	}
    *timestamp = epoch_time + (float64)get_timezone_seconds() - input_timezone_offset;
    return swe_ok;
}


/*
 * Generates a WAV registry from a directory
 */
WavRegistry* wav_registry_generate(const char* dirpath, TimestampPattern* pattern) {
	int count, i, j;
	WavRegistry* registry;
	WavRegistryFile** pointers;
	CHECK_NULL(dirpath, NULL);
	CHECK_NULL(pattern, NULL);

	registry = swe_malloc(sizeof(WavRegistry));
	char** files = swe_list_dir(dirpath, &count);
	if ( count == 0 ){
		warnmsg("Empty directory!");
		registry->count = 0;
		registry->index = 0;
		registry->files = NULL;
		return registry;
	}

	// Allocate space for n files
	registry->files = NULL;
	registry->index = 0;
	registry->count = 0;


	for ( i=0 ; i<count ; i++ ) {
		float64 timestamp;
		WavRegistryFile* newfile;
		char* extension;

		int len = strlen(files[i]);
		if ( len < 5) {
			// If the name is too short
			continue;
		}
		extension = &files[i][len - 4];
		if ( memcmp(".wav", extension, 4)){
			// if the extension doesn't match
			continue;
		}

		if ( pattern_to_timestamp(files[i], pattern, &timestamp) < 0 ) {
			// If timestamp can't be extracted, ignore this file
			continue;
		}
		newfile = swe_malloc(sizeof(WavRegistryFile));
		newfile->processed = FALSE;
		newfile->timestamp = timestamp;
		sprintf(newfile->filename, "%s%s%s", dirpath, PATH_SEPARATOR, files[i]);
		registry->files = ATTACH_TO_ARRAY(registry->files, newfile);
		registry->count++;
	}
	FREE_ARRAY(files);

	// Sort by timestamp (lower to higher)
	pointers = swe_malloc(sizeof(WavRegistry*)*registry->count);

	// First copy all file pointers to "pointers"
	for ( i=0 ; i<count ; i++ ) {
		pointers[i] = registry->files[i];
	}

	for ( i=0 ; i < registry->count ; i++ ) {
		float64 timestamp = 1e12;
		int index = -1;
		for ( j=0 ; j< registry->count ; j++ ) {
			if ( pointers[j] != NULL ) {
				if ( pointers[j]->timestamp < timestamp ) {
					index = j;
					registry->files[i] = pointers[j];
					timestamp = pointers[j]->timestamp;
				}
			}
		}
		pointers[index] = NULL;
	}
	swe_free(pointers);
	return registry;
}


/*
 * Frees dynamic memory within a WavRegistry
 */
int wav_registry_free(WavRegistry* registry){
	FREE_ARRAY(registry->files);
	swe_free(registry);
	return swe_ok;
}


#endif // ENABLE_HIGH_FREQUENCY_MODULES //

