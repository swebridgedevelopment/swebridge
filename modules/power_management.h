#ifndef MODULES_POWER_MANAGEMENT
#define MODULES_POWER_MANAGEMENT

#include "core/simple_process.h"
#include "swe_conf.h"

#define POWER_MANAGEMENT_UID "swebridge:process:powerManagement"
#define DELAY_BEFORE_DEFAULT -1 //  By default do not wait
#define DELAY_AFTER_DEFAULT -1 //  By default do not wait


//Internal data structure
typedef struct {
	//---- Public variables ----//
	int sensor_index;
	uchar power;
}PowerManagementData;



int power_management_constructor(SimpleProcess* conf, SchedulerProcess* process);
SchedulerData* power_management_exec(void* process_pointer, SchedulerData* data, ProcessStatus* errorcode);


#endif /* MODULES_POWER_MANAGEMENT */
