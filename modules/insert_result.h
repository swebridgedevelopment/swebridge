/*
 * insertResult.h
 *
 *  Created on: Sep 8, 2016
 *      Author: enoc
 */

#ifndef MODULES_INC_INSERT_RESULT_H_
#define MODULES_INC_INSERT_RESULT_H_

#include "core/simple_process.h"
#include "swe_conf.h"

#define INSERT_RESULT_UID_OLD "urn:SARTI:swe:process:insertResult" // deprecated!
#define INSERT_RESULT_UID "swebridge:modules:insertResult"


// if OPEN_AND_CLOSE_FILE is enabled, the temp file is opened and closed
// each time a measure is saved. It may be useful to monitor the bridge
// execution. However, it requires additional CPU time to open and
// close the files each time
#define OPEN_AND_CLOSE_FILE ON

//char array lengths
#define FILENAME_LENGTH 100
#define MAX_TEMPLATE_SIZE 500
#define MAX_PATH_LENGTH 100


//Internal data structure
typedef struct {

	int measure_buffer_size; // size of the buffer to generate new measures
	float64 recording_time; // Output file generation period
	uint max_file_size; // Generate output files before reaching this size
	uchar time_range; // If set, instead of using a timestamp uses timestamp_init/timestamp_end

	int number_of_measures; // generates output files with a certain number of measures
	float64 timeout; // If number_of_measures has no been reached before the timeout expires,
	               // generate an output file anyway

	uchar msec_timestamp; // if TRUE timestamp will include msecs
	uchar exi_encoding; // use EXI encoding instead of XML

	char out_path[MAX_PATH_LENGTH]; // path where the output files will be stored
	char temp_path[MAX_PATH_LENGTH]; // path where the temporal files will be stored
	char* template; //template is stored in a temporal variable before being stored here
	int template_size; // template length
	int float_precision; // Number of decimals stored. Only used in binary data and position

	uchar georeference; // sensor position will be added to each measure
	uchar altitude; // depth will be added to each measure


	uchar* header;
	int header_size;

	uchar* after_template;
	int after_template_size;

	uchar* footer;
	int footer_size;



	uint temp_file_size; //size in bytes of the temp file
	uint headers_size; //sum of header, template, after template and footer sizes
	uint stored_measures; //number of measures
	char temp_filename[FILENAME_LENGTH]; //temp file filename
	ulong last_timestamp; 	// last timestamp stored. Used to prevent duplicated timestamps
		   	   	   	   	    // which may cause SOS server errors. Unis are seconds or msecs
							// depending on the useMilliseconds flag

	swe_file* temp_file; //file where the measures are temporally stored

}InsertResultData;

//buffer where the measure string is generated. If the number of fields is
#define DEFAULT_MEASURE_BUFFER_SIZE 1024

#define DEFAULT_RECORDING_TIME 60.0 //by default a new file is generated every minute
#define DEFAULT_EXI_ENCODING FALSE //by default use xml
#define DEFAULT_MAX_FILE_SIZE 0 //maximum file size before creating an output file, if 0 it is ignored
#define DEFAULT_GEOREFERENCED FALSE
#define DEFAULT_DEPTH_REFERENCED FALSE

#define DEFAULT_FLOAT_PRECISION 6


int insert_result_constructor(SimpleProcess* conf, SchedulerProcess* process);
SchedulerData* insert_result_exec(void* process_pointer, SchedulerData* data, ProcessStatus* errorcode);


#define TEMP_FILE_FORMAT ".tmp"//format of the temporal file

#define COPY_BUFFER_SIZE 500 //max bytes that can be copied from one file to another at a time

#define ELEMENT_SEPARATOR "#"
#define MEASURE_SEPARATOR "@"

#define OM_ELEMENT_NOT_VALID "NaN"


//------------ EXI Encoding variables ------------//
#define EXI_FORMAT ".exi"

#define EXI_HEADER  {\
	0x80, 0x00, 0x1E, 0x68, 0x74, 0x74, 0x70, 0x3A, 0x2F, 0x2F, \
	0x77, 0x77, 0x77, 0x2E, 0x6F, 0x70, 0x65, 0x6E, 0x67, 0x69, \
	0x73, 0x2E, 0x6E, 0x65, 0x74, 0x2F, 0x73, 0x6F, 0x73, 0x2F, \
	0x32, 0x2E, 0x30, 0x0D, 0x49, 0x6E, 0x73, 0x65, 0x72, 0x74, \
	0x52, 0x65, 0x73, 0x75, 0x6C, 0x74, 0x01, 0x01, 0x08, 0x73, \
	0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x05, 0x53, 0x4F, 0x53, \
	0x01, 0x01, 0x01, 0x08, 0x76, 0x65, 0x72, 0x73, 0x69, 0x6F, \
	0x6E, 0x07, 0x32, 0x2E, 0x30, 0x2E, 0x30, 0x02, 0x02, 0x04, \
	0x09, 0x74, 0x65, 0x6D, 0x70, 0x6C, 0x61, 0x74, 0x65, 0x03}
#define EXI_HEADER_SIZE 90 //size in bytes

#define EXI_AFTER_TEMPLATE {\
	0x00, 0x01, 0x00, 0x04, 0x0D, 0x72, 0x65, 0x73, 0x75, 0x6C, \
	0x74, 0x56, 0x61, 0x6C, 0x75, 0x65, 0x73, 0x03}
#define EXI_AFTER_TEMPLATE_SIZE 18 //size in bytes

#define EXI_FOOTER {0x00, 0x01}
#define EXI_FOOTER_SIZE 2 //size in bytes


//------------ XML Encoding variables ------------//
#define XML_FORMAT  ".xml"

#define XML_INSERT_RESULT_HEADER "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"\
		"<sos:InsertResult xmlns:sos=\"http://www.opengis.net/sos/2.0\"\n"\
		"xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" service=\"SOS\" version=\"2.0.0\"\n" \
		 "xsi:schemaLocation=\"http://www.opengis.net/sos/2.0 http://schemas.opengis.net/sos/2.0/sos.xsd\">\n"\
		 "\t<sos:template>"
#define XML_INSERT_RESULT_HEADER_SIZE strlen(XML_INSERT_RESULT_HEADER)

#define XML_AFTER_TEMPLATE "</sos:template>\n\t<sos:resultValues>"
#define XML_AFTER_TEMPLATE_SIZE strlen(XML_AFTER_TEMPLATE)

#define XML_FOOTER "</sos:resultValues>\n" \
							"</sos:InsertResult>"
#define XML_FOOTER_SIZE strlen(XML_FOOTER)


#endif /* MODULES_INC_INSERT_RESULT_H_ */
