/*
 * analog_measurement.h
 *
 *  Created on: Oct 18, 2018
 *      Author: enoc
 */

#ifndef MODULES_INC_ANALOG_MEASUREMENT_H_
#define MODULES_INC_ANALOG_MEASUREMENT_H_


#include "core/simple_process.h"

#define ANALOG_MEASUREMENT_UID "swebridge:modules:analogMeasurement"

typedef struct {
	SchedulerData* data;

	uint fields; // number of fields to be measured
	int* analog_channels; // array containig the analog_channels

}AnalogMeasurementData;





int analog_measurement_constructor(SimpleProcess* conf, SchedulerProcess* process);
SchedulerData* analog_measurement_exec (void* process_pointer, SchedulerData* data, ProcessStatus *errorcode);


#endif /* MODULES_INC_ANALOG_MEASUREMENT_H_ */
