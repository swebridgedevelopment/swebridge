/*
 * instrumentCommand.h
 *
 *  Created on: Sep 8, 2016
 *      Author: enoc
 */

#ifndef MODULES_INC_INSTRUMENT_COMMAND_H_
#define MODULES_INC_INSTRUMENT_COMMAND_H_

#include "common/stream_parser.h"
#include "core/simple_process.h"

#define INSTRUMENT_COMMAND_UID_OLD "urn:SARTI:swe:process:instrumentCommand" // Deprecated!
#define INSTRUMENT_COMMAND_UID "swebridge:modules:instrumentCommand"

#define DEFAULT_TIMEOUT 2
#define DEFAULT_XML_PARSER FALSE
#define DEFAULT_BUFFER_SIZE 200
#define DEFAULT_PROCESS_TIMEOUT -1.0
#define DEFAULT_RESPONSE_VALUE_SIZE 20
#define DEFAULT_CHECK_DATA_INTEGRITY FALSE
#define DEFAULT_COLLAPSE_WHITE_SPACES TRUE
#define BUFFER_SAFETY_COEFFICIENT 1.5
#define DEFAULT_DATA_RECORD_AS_BLOCK TRUE
#define DEFAULT_FLUSH_BEFORE_QUERY FALSE

/*
 * If the connection is TCP use a different timeout. If the connection dies, this is
 * the only way to exit.
 */
#define DEFAULT_PROCESS_TIMEOUT_TCP 300

/*
 * Command to be sent
 */
typedef struct{
	uchar* data; // command values
	uint size;
	uint response_timeout; // timeout in usecs

	uchar binary;

}CommandStructure;




typedef struct {
	/* public parameters */
	float64 timeout;

	CommandStructure* command; // Structure to encode a command to be sent
	ResponseStructure* response; // Structure to encode a response to be read
	uchar fill_buffer; 	// this flag indicates if the process needs to read instrument
						// data. If it set to false there's unprocessed data in the buffer
	StreamBuffer* buffer; // Buffer where incoming streams are stored
	SchedulerData* data; // Structure where the data is stored

	/*
	 * Burst Mode: If burstMeasures is greater than 0 the acquisition will be set to burst mode.
	 * When in burst mode the Instrument Command will take "burstMeasures", measures and afterwards
	 * it will wait "burstSleep" time before the next burst is activated.
	 *
	 */
	int burst_measures;       // Number of measures to be performed each in each burst
	float64 burst_period;   // Number of ticks to wait between burst measures
	float64 burst_cycle;    // Number of ticks to wait before activating a new burst

	int burst_counter; // counter to store the current number of burst measures
	/*
	 * Other Flags
	 */
	uchar flush_before_query; // If this flag is set to TRUE the input buffer will be flushed before
	                          // sending a new command, discarding unprocessed data in buffer


}InstrumentCommandData;



int instrument_command_constructor(SimpleProcess* conf, SchedulerProcess* process);
SchedulerData* instrument_command_exec (void* process_pointer, SchedulerData* data, ProcessStatus *errorcode);


#endif /* MODULES_INC_INSTRUMENT_COMMAND_H_ */
