/*
 *  CSV Generator Module: stores incoming data into a csv file
 *
 *  @author: Enoc Martínez
 *  @institution: Universitat Politècnica de Catalunya (UPC)
 *  @contact: enoc.martinez@upc.edu
 */

#include "modules/csv_generator.h"
#include "common/swe_utils.h"
#include "core/simple_process.h"
#include "resources/resources.h"
#include "swe_conf.h"


int generate_csv_filename(char *buffer, CsvGeneratorData* params, float64 timestamp);
int csv_new_file_required(CsvGeneratorData* params, SchedulerData* data);


/*
 * Constructor for generate CSV module
 */
int csv_generator_constructor(SimpleProcess* conf, SchedulerProcess* process){
	char periodicity[256];
	char file_prefix[256] = "";

	static const char* periodicity_year[]={"year", "yearly", "Year", "Yearly", NULL};
	static const char* periodicity_month[]={"month", "monthly", "Month", "Monthly", NULL};
	static const char* periodicity_day[]={"day", "daily", "Day", "Daily", NULL};
	static const char* periodicity_hour[]={"hour", "hourly", "Hour", "Hourly", NULL};
	static const char* periodicity_minute[]={"minute", "minutely", "min", "Minute", NULL};

	CsvGeneratorData* params;

	msg_sub_title( WHT, "CSV Generator Constructor");

	process->internal_data = swe_malloc(sizeof(CsvGeneratorData));
	params = process->internal_data;

	// set default values
	strcpy(params->output_folder, CSV_DEFAULT_OUTPUT_FOLDER);
	strcpy(params->delimiter, CSV_DEFAULT_DELIMITER);
	strcpy(params->endline, CSV_DEFAULT_ENDLINE);
	params->decimal_precision = CSV_DEFAULT_DECIMAL_PRECISION;


	process->exec=  csv_generator_exec;

	//STEP 4: Make visible internal parameters with the function "add_settings_to_process"
	dmsg( "Adding settings...");
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/periodicity", (void*)(periodicity));
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/delimiter", (void*)(params->delimiter));
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/endline", (void*)(params->endline));
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/prefix", (void*)(file_prefix));
	process->settings=add_settings_to_process(process->settings, SWE_Boolean, "parameters/msecTimestamp", (void*)(&params->msec_timestamp));
	process->settings=add_settings_to_process(process->settings, SWE_Boolean, "parameters/georeference", (void*)(&params->georeference));
	process->settings=add_settings_to_process(process->settings, SWE_Boolean, "parameters/altitude", (void*)(&params->depth));
	process->settings=add_settings_to_process(process->settings, SWE_Count, "parameters/decimalPrecision", (void*)(&params->decimal_precision));
	process->settings=add_settings_to_process(process->settings, SWE_Boolean, "parameters/timeRange", (void*)(&params->time_range));
	process->settings = add_settings_to_process(process->settings, SWE_Text, "parameters/outputFolder", (void*)(params->output_folder));
	process->state_reg.exec_flag = 0;

	// Process Configuration //
	cdmsg(BLU,  "Configuring settings:");
	TRY_RET(configure_process_settings(conf->settings, process));
	cdmsg(GRN,"settings configured!");

	// Check if there's user specified prefix
	if ( strlen(file_prefix) == 0 ) {
		dmsg("No file prefix specified, using process name : \"%s\"", process->id);
		strcpy(params->file_prefix, process->id);
	} else {
		strcpy(params->file_prefix, file_prefix);
	}

	// Check the periodicity //
	if ( string_in_list(periodicity, periodicity_minute)){
		params->periodicity =csv_periodicity_minute;
		imsg("Generating a CSV file each minute");
	}
	else if ( string_in_list(periodicity, periodicity_hour)){
		params->periodicity =csv_periodicity_hour;
		imsg("Generating a CSV file each hour");
	}
	else if ( string_in_list(periodicity, periodicity_day)){
		params->periodicity =csv_periodicity_day;
		imsg("Generating a CSV file each day");
	}
	else if ( string_in_list(periodicity, periodicity_month)){
		params->periodicity =csv_periodicity_month;
		imsg("Generating a CSV file per month");
	}
	else if ( string_in_list(periodicity, periodicity_year)){
		params->periodicity =csv_periodicity_year;
		imsg("Generating a CSV file per year");

	}

	else {
		errmsg("Could not understand periodicity value \"%s\"", periodicity);
		return swe_invalid_arguments;
	}

	// Create output path (if it did not exist)
	if (swe_create_dir(params->output_folder) != swe_ok){
		warnmsg("Couldn't create the output path");
	}


	dmsgn( "==================================================\n\n");
	return swe_ok;
}


/*
 * Execute Generate CSV Module.
 */
SchedulerData* csv_generator_exec(void* process_pointer, SchedulerData* data, ProcessStatus *errorcode){
	SchedulerProcess* process=(SchedulerProcess*)process_pointer;
	CsvGeneratorData* params=(CsvGeneratorData*)process->internal_data;
	uchar exec_flag = process->state_reg.exec_flag; // copy the exec flags
	process->state_reg.exec_flag = 0;

	*errorcode = process_chain_error;
	if (exec_flag & PREVIOUS_PROCESS_TRIGGER) {
		if (csv_new_file_required(params, data)){
			char filename[512];
			// If a previous file existed, close it
			if ( params->csv != NULL ) {
				TRY_RET_NULL(csv_finish(params->csv));
			}
			// new filename
			TRY_RET_NULL(generate_csv_filename(filename, params, data->init_timestamp));
			dmsg("CSV - Generating new file: %s", filename);
			// generate new file
			params->csv = CATCH_NULL(csv_create(filename, params->delimiter, params->endline, params->decimal_precision,
					data, params->georeference, params->depth, params->time_range, params->msec_timestamp, CSV_DEFAULT_OPEN_AND_CLOSE_FILE));
			// store first timestamp
			TRY_RET_NULL(datetime_from_timestamp(&params->init_datetime, data->init_timestamp));
		}
		TRY_RET_NULL(csv_append_measurement(data, params->csv));
		dmsg("CSV - There are %d measurements in CSV file ", params->csv->count);
		*errorcode = process_chain_continue;
	}
	else {
		errmsg("No implementation for flags %d", exec_flag);
		return NULL;
	}
	return data;
}


/*
 * Generates a filename based on a prefix and timing
 */
int generate_csv_filename(char *buffer, CsvGeneratorData* params, float64 timestamp) {
	uchar hour = FALSE;
	uchar min = FALSE;
	uchar sec = FALSE;

	if (params->periodicity == csv_periodicity_minute){
		hour = TRUE;
		min = TRUE;
	}
	else if (params->periodicity == csv_periodicity_hour) {
		hour = TRUE;
	}
	TRY_RET(generate_data_filename(buffer, params->output_folder, params->file_prefix, "csv", timestamp, hour, min, sec));
	return swe_ok;
}



/*
 * Checks if a new file has to be generated based on the periodicity configuration and last file timestamp
 * returns TRUE if a new file has to be generated
 * return FALLSE if not
 */
int csv_new_file_required(CsvGeneratorData* params, SchedulerData* data) {
	DateTime *init, now;

	// If currently there isn't a file, create it
	if ( params->csv == NULL ) {
		return TRUE;
	}
	init = &params->init_datetime;
	TRY_RET(datetime_from_timestamp(&now, data->init_timestamp));

	if ((params->periodicity == csv_periodicity_minute) && (init->minute != now.minute)){
		return TRUE;
	}
	else if ((params->periodicity == csv_periodicity_hour) && (init->hour != now.hour)){
		return TRUE;
	}
	else if ((params->periodicity == csv_periodicity_day) && (init->day != now.day)){
		return TRUE;
	}
	else if ((params->periodicity == csv_periodicity_month) && (init->month != now.month)){
		return TRUE;
	}
	else if ((params->periodicity == csv_periodicity_year) && (init->year != now.year)){
		return TRUE;
	}

	return FALSE;
}
