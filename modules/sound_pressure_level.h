/*
 * This module implements Sound Pressure Level algorithms
 */

#ifndef MODULES_SOUND_PRESSURE_LEVEL_H_
#define MODULES_SOUND_PRESSURE_LEVEL_H_

#include "swe_conf.h"
#if ENABLE_HIGH_FREQUENCY_MODULES

#include "core/simple_process.h"
#include "resources/fft.h"


#define SOUND_PRESSURE_LEVEL_UID "swebridge:modules:soundPressureLevel"
#define ADD_FREQUENCY_BAND_REF "parameters/frequencyBands"


#define USE_FULL_BANDWIDTH -1

// Default values //
#define SPL_DEFAULT_FFT_TIME 1 // seconds
#define SPL_DEFAULT_INTEGRATION_TIME 10 // seconds



/*
 * Structure to store band frequencies
 */
typedef struct {
	float64 flow;
	float64 fhigh;
	float64 fcenter; // If negative, it means that the whole bandwidth is

	float64 accumulator; // Here the partial SPL is stored for this Band frequency
					     // this is used to average the SPL value over a certain
	 	 	 	 	 	 // number of FFTs
}BandFrequency;


/*
 * Structure to store the Power Spectral density as they are calculated.
 * Once several of them are calculated, their are averaged to have the
 * Welch's method result
 */
typedef struct {
	float64 init_timestamp; // Start time accumulated
	float64 end_timestamp;  // End time
	uint n; // Number of PSD accumulated
	uint ntarget; // Number of PSD desired to accumulate
	uint nfreq; // Length of the PSD (equivalent to NFFT)
	float64* accumulator; // Buffer to accumulate PSDs
}PsdAccumulator;


/*
 * Internal struture for SoundPressureLevel module
 */
typedef struct {
	uchar initialized; //

	float64 sampling_rate;   // Incoming stream sample rate
	float64 fft_time;        // Number of time to accumualte samples in each FFT

	float64 integration_time; // Number of time after which SPL values will be computed

	// Incoming packets //
	SampleBuffer* leftovers; // Samples not yet processed from previous iterations
	SampleBuffer* buffer; // Buffer to store incoming samples
	uint accumulate_samples; // Number of samples to accumulate (equal to buffer size)
//	uint nsamples;

//	float64 *samples; // Buffer to store the input data
//	float64 sample_timestamp;
//	float64 first_timestamp; // first timestamp in buffer


	// Temporal SPL values //
	float64 temporal_spl_acc; // accumulator
	uint temporal_spl_n;      // number of samples stored


	// FFT Parameters //
	FFTparams* fft_params; // FFT parameters
	float64 fft_overlap; // overlapping between FFTs, between 0 and 0.95 (0.5 default fot hann window)
	uint overlap_index; // Index from which the samples are overlapped. It's important to avoid detrending
	                    // twice the same data
	uchar use_hann_window; // is TRUE a hann window will be applied

	// Power Spectrum Accumultaor //
	PsdAccumulator psd;




	// SPL Calculations //
	BandFrequency** band_frequencies; // array of band frequencies to be calculated
	uchar sel; // If TRUE compute also the Sound Exposure Level
	uchar rms; // if TRUE compute also the Root Mean Square pressure


	// Result Storage //
	SchedulerData* data; // structure to store the results for further processes

}SoundPressureLevelData;





int sound_pressure_level_constructor(SimpleProcess* conf, SchedulerProcess* process);
SchedulerData* sound_pressure_level_exec(void* process_pointer, SchedulerData* data, ProcessStatus* errorcode);
int sound_pressure_level_flush(void* process_pointer);
int sound_pressure_level_error(void* process_pointer);



#endif // ENABLE_HIGH_FREQUENCY_MODULES //
#endif /* MODULES_SOUND_PRESSURE_LEVEL_H_ */
