/**************************************************************************
 *	 	 	 	 	 	  Zabbix Sender
 *************************************************************************
 * This module sends acquired data to a zabbix service. The communication
 * pattern to the zabbix uses socket communications. In order to avoid
 * disruption of the scheduler timing, an external program has to be used,
 * e.g. zabbix_sender.py
 *************************************************************************/

#include "modules/json_udp_sender.h"
#include "common/swe_utils.h"
#include "core/simple_process.h"
#include "swe_conf.h"
#include "resources/resources.h"


/*
 * Constructor for the zabbix sender module. This module will send the incoming data to a Zabbix serer
 * using an external program.
 */
int json_udp_sender_constructor(SimpleProcess* conf, SchedulerProcess* process){
	char temp_sensor_name[256] = "";

	msg_sub_title(WHT, "JSON UDP Sender Constructor");
	dmsg("allocating space for internal data...");
	JsonUdpSenderData* params=swe_malloc(sizeof(JsonUdpSenderData));
	process->internal_data=(void*)params;

	dmsg( "assigning execution pointer...");
	process->exec = json_udp_sender_exec;

	params->decimal_precision = DEFAULT_JSON_UDP_DECIMAL_PRECISION;

	//STEP 6: Make visible internal parameters with the function "add_settings_to_process"
	dmsg( "Adding configurable settings...");
	process->settings=add_settings_to_process(process->settings,SWE_Text, "parameters/destination", (void*)params->destination);
	process->settings=add_settings_to_process(process->settings,SWE_Text, "parameters/sensorName", temp_sensor_name);
	process->settings=add_settings_to_process(process->settings,SWE_Count, "parameters/decimalPrecision", &params->decimal_precision);


	dmsg( "Configuring settings...");
	if(configure_process_settings(conf->settings, process)!=swe_ok) {
		errmsg("configuration returned errors");
	}

	dmsg( "done");


	params->iface = setup_udp_iface(-1);  // let OS choose the port
	TRY_RET(iface_open(params->iface));
	if ( temp_sensor_name[0] == 0 ) {
		// Get the PhysicalSystem ID from the SensorML
		PhysicalSystem* ps = conf->physical_system;
		imsg("Sensor name is %s", ps->id);
		params->sensor_name = set_string(ps->id);
	}
	else{
		params->sensor_name = set_string(temp_sensor_name);
	}

	cimsg(GRN, "json_udp_sender process created successfully");
	return swe_ok;
}


/*
 * Send data in JSON format
 */
SchedulerData* json_udp_sender_exec(void* process_pointer, SchedulerData* data, ProcessStatus *errorcode){
	SchedulerProcess* process=(SchedulerProcess*)process_pointer;
	JsonUdpSenderData* params=(JsonUdpSenderData*)process->internal_data;
	StateRegister* state_register = &process->state_reg;
	state_register->exec_flag = 0; // clear the flag, a local copy is still available

	if (data != NULL) {
		int i;
		char timestamp[64];
		char temp[512];

		char buffmem[4096];
		char* buffer = buffmem;
		memset(buffer, 0, 4096);

		buffer[0] = '{';

		// add sensor name
		TRY_RET_NULL(ascii_datetime(timestamp, data->init_timestamp, FALSE));

		sprintf(temp, "\"sensorName\": \"%s\",", params->sensor_name);
		buffer = fast_strcat(buffer, temp);

		sprintf(temp, "\"timestamp\": \"%s\",", timestamp);
		buffer = fast_strcat(buffer, temp);

		buffer = fast_strcat(buffer, "\"variables\": {");

		for ( i=0; i<data->count; i++){
			SWE_Data* f = data->fields[i];
			char strvalues[256];
			uchar* invalue = f->value;
			swe_encoding_to_string(strvalues, invalue, f->encoding, params->decimal_precision);
			sprintf(temp, "\"%s\": {\"timestamp\": \"%s\", \"value\": %s},", f->name, timestamp, strvalues);
			buffer = fast_strcat(buffer, temp);
		}
		buffer--;
		buffer[0] = '}';  // replace last comma for }
		buffer = fast_strcat(buffer, "}");
		imsg("JDP - %s", buffmem);
		dmsg("JDP - Sending %d bytes to %s", strlen(buffmem), params->destination);
		iface_send_to(params->iface, buffmem, strlen(buffmem), params->destination);
	}



	return NULL; //End of the chain
}
