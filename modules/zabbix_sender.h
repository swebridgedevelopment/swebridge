#ifndef ZABBIX_SENDER_H_
#define ZABBIX_SENDER_H_

#include "core/simple_process.h"
#include "swe_conf.h"

#define ZABBIX_SENDER_UID "swebridge:modules:zabbixSender"

#define ZABBIX_COMMAND_LENGTH 512


#define ZABBIX_PORT_DEFAULT 1051

#define ZABBIX_INSTRUMENT_PREFIX FALSE
#define ZABBIX_HOST_NAME FALSE

#define ZABBIX_DECIMAL_PRECISION 6

typedef struct {

	// Connection info //
	char* IP; // IP or hostname of the server (192.168.1.135 or "myserver.com")
	int port; // usually 10051

	// external sender program //
	char* zabbix_sender_program; //place where the zabbix_sender is located locally

	// external sender program //
	char* hostname; // host
	char* prefix; // prefix to be added to each item <prefix>.<item>, e.g. SBE37.sea_water_temperature
	uchar use_prefix;

}ZabbixSenderData;

int zabbix_sender_constructor(SimpleProcess* conf, SchedulerProcess* process);
SchedulerData* zabbix_sender_exec (void* process_pointer, SchedulerData* data, ProcessStatus *errorcode);


#endif /* ZABBIX_SENDER_H_ */

