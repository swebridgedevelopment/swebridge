/*
 * This file implements a module to read WAV files and pass the samples
 * to further processes. It's main utility is validation of other processing
 * algorithms such as SPL.
 *
 * @author: Enoc Martínez
 * @institution: Universitat Politècnica de Catalunya (UPC)
 * @contact: enoc.martinez@upc.edu
 */

#ifndef MODULES_INC_WAV_READER_H_
#define MODULES_INC_WAV_READER_H_

#include "swe_conf.h"
#if ENABLE_HIGH_FREQUENCY_MODULES

#define WAV_READER_UID "swebridge:modules:wavReader"

#include "core/simple_process.h"
#include "common/ring_buffer.h"
#include "common/high_freq_stream.h"



#define DEFAULT_ACCUMULATE_SAMPLES_WR 21000
#define DEFAULT_TIMEZONE_WR 0


/*
 * Element of the timestamp pattern
 */
typedef struct{
	int length;
	int position;
}PatternElement;


/*
 * Struct to convert from arbitrary timestamp to a formatted DateTime
 */
typedef struct{
	PatternElement year;
	PatternElement month;
	PatternElement day;
	PatternElement hour;
	PatternElement minute;
	PatternElement second;
	PatternElement timezone;

	// If the timezone is not in the filename but needs to be used:
	uchar force_timezone; // flag to determine if the timezone has to be forced (FALSE -> extract with pattern)
	float64 fixed_timezone;   // timezone that will be used in hours

}TimestampPattern;



/*
 * Info about a wav file
 */
typedef struct{
	char filename[512];
	float64 timestamp;
	uchar processed; // flag to determine if the file has been processed
	swe_file* file;
}WavRegistryFile;


/*
 * Registry of all the WAV files in the folder
 */
typedef struct{
	WavRegistryFile** files;
	int count; // max files
	int index; // current file
	WavReadFile* current_file; // shortcut to the current file
}WavRegistry;


// Hydrophone-specific defaults //
typedef struct {

	//---- Settings ----//
	uint accumulate_samples; // Number of samples to store before executing
	                         // next process
	char dirpath[512]; // Path where the wav files are (or will be generated)


	SchedulerData* data; // Only buffer is used from SchedulerData

	float64 conversion_coefficient; // Conversion constant
	HydrophoneParameters params;

	TimestampPattern pattern; // Pattern to extract the timestamp from the filename
	WavRegistry *registry;

}WavReaderData;




int wav_reader_constructor(SimpleProcess* conf, SchedulerProcess* process);
SchedulerData* wav_reader_exec(void* process_pointer, SchedulerData* nothing, ProcessStatus* errorcode);


#endif // ENABLE_HIGH_FREQUENCY_MODULES //
#endif /* MODULES_INC_WAV_READER_H_ */
