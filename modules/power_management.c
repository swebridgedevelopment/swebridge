/*
 ============================================================================
								insert_result.c
 ============================================================================
 This file contains the implementation of a module to generate inserResult
 files in XML or EXI formats.
 ============================================================================
 by SARTI
 Author: Enoc Martínez
 ============================================================================
 */

#include "modules/power_management.h"

#include "core/simple_process.h"
#include "resources/resources.h"
#include "swe_conf.h"


//******************* Private functions Declarations *******************//



int power_management_constructor(SimpleProcess* conf, SchedulerProcess* process){
	//SchedulerProcess* process=NULL;

	msg_sub_title( WHT, "Power Management Constructor");

	//STEP 2: Allocate memory for the internal data structure
	dmsg( "allocating space for internal data...");
	PowerManagementData* internal_data=swe_malloc(sizeof(PowerManagementData));
	process->internal_data=(void*)internal_data;

	//STEP 3: Assign execution pointer
	dmsg( "assigning execution pointer...");
	process->exec=power_management_exec; //<======== Assign there the execution handler

	internal_data->power = UNSET_BOOLEAN; // By default set power on
	internal_data->sensor_index = -1;


	//STEP 4: Make visible internal parameters with the function "add_settings_to_process"
	dmsg( "Adding settings...");

	process->settings=add_settings_to_process(process->settings, SWE_Count, "parameters/powerIndex", (void*)(&internal_data->sensor_index));
	process->settings=add_settings_to_process(process->settings, SWE_Boolean, "parameters/power", (void*)(&internal_data->power));




	//STEP 6 Auto-configure process settings with the function "configure_process_settings"
	dmsg( "Configuring settings...");
	if(configure_process_settings(conf->settings, process)!=swe_ok) {
		errmsg( "configuration failed");
		return swe_error;
	}


	if (internal_data->sensor_index == 123) {
		errmsg("Instrument Index option not set!");
		return swe_error;
	}

	if (internal_data->sensor_index == -1) {
		errmsg("Instrument Index option not set!");
		return swe_error;
	}
	if (process->state_reg.sampling_rate > 0) {
		process->state_reg.exec_modes.scheduler = TRUE;
	}

	if ( internal_data->power == UNSET_BOOLEAN ){
		errmsg("Power operation not specified! Use  parameters/power to set ON or OFF");
		return swe_invalid_arguments;
	}
	dmsg("initializing power...");
	TRY_RET(power_initialize(internal_data->sensor_index));

	dmsg("initialized?");
	dmsgn( "==================================================\n\n");
	return swe_ok;
}


/*
 * Waits for delayBefore. Turns on or off a sensor. Waits for delayAfter before returning success
 */
SchedulerData* power_management_exec(void* process_pointer, SchedulerData* data, ProcessStatus *errorcode){
	int ret;
	SchedulerProcess* process=(SchedulerProcess*)process_pointer;
	PowerManagementData* internal_data=(PowerManagementData*)process->internal_data;
	StateRegister* state_register = &process->state_reg;
	*errorcode = process_chain_continue;
	state_register->exec_flag = 0; // clean the exec flag
	// Executed by scheduler //
	if (internal_data->power == ON) {
		imsg("PWR - Setting power ON on sensor %d", internal_data->sensor_index);
		if ((ret=power_on_sensor(internal_data->sensor_index)) < 0){
			errmsg("Error %d trying to turn ON  sensor %d", ret, internal_data->sensor_index);
		}
	}
	else { // Power ON
		imsg("PWR - Setting power OFF on sensor %d", internal_data->sensor_index);
		if ((ret=power_off_sensor(internal_data->sensor_index)) < 0){
			errmsg("Error %d trying to turn ON  sensor %d", ret, internal_data->sensor_index);
		}
	}
	return data;
}



