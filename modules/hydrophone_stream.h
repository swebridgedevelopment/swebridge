/*
 * high_frequency_stream.h
 *
 *  Created on: Jun 4, 2019
 *      Author: enoc
 */

#ifndef MODULES_INC_HYDROPHONE_STREAM_H_
#define MODULES_INC_HYDROPHONE_STREAM_H_

#include "swe_conf.h"
#if ENABLE_HIGH_FREQUENCY_MODULES

#define HYDROPHONE_STREAM_UID "swebridge:modules:hydrophoneStream"
#define HYDROPHONE_STREAM_UID_OLD "swebridge:modules:highFrequencyStream"

#include "core/simple_process.h"
#include "common/ring_buffer.h"
#include "common/high_freq_stream.h"

#define DEFAULT_HIGH_FREQ_STREAM_NBLOCKS 128 // Number of blocks in buffer

#define MAX_PACKETS_LOST 10 // Allows a total of 10 packets lost, if more are lost
                            // error is returned

#define DEFAULT_SAMPLES_FIELD_NAME "samples"
#define DEFAULT_FRAME_COUNT_FIELD_NAME "count"


// Hydrophone-specific defaults //
typedef struct {
	//---- Settings ----//
	float64 conversion_coefficient; // Conversion constant

	// Private Variables //
	BlockRingBuffer* ringbuff;
	HydrophoneParameters params;

	uchar iface_error;

	SchedulerData* data; // Only buffer is used from SchedulerData
	HighFreqStreamResponse* response; // response structure

	uint accumulate_samples; // Number of samples to store before executing
	                         // next process

	uchar initialized; // This flag determines if the streaming has been initialized or not. When
	                   // set to FALSE the packet count will not be checked (it will be set to TRUE)
	                   // the first time it is executec

	float64 timeout;  // Timeout between packets if expires is considered an error

}HydrohponeData;



int hydrophone_stream_constructor(SimpleProcess* conf, SchedulerProcess* process);
SchedulerData* hydrophone_stream_exec(void* process_pointer, SchedulerData* nothing, ProcessStatus* errorcode);

int process_incoming_packet(HighFreqStreamBuffer* output, // Processed response
		HighFreqStreamResponse* resp, // Response structure
		uchar* input, // RingBuffer where data is located
		float64 timestamp,
		uchar check_count);

#endif // ENABLE_HIGH_FREQUENCY_MODULES //
#endif /* MODULES_INC_HYDROPHONE_STREAM_H_ */
