/*
 * adc_measrument.c
 *
 *  Created on: Oct 18, 2018
 *      Author: enoc
 */

#include "modules/analog_measurement.h"

#include "common/sensorml.h"
#include "common/swe_utils.h"
#include "core/simple_process.h"
#include "resources/resources.h"
#include "swe_conf.h"

#define DEBUG_THIS_MODULE ON

#if DEBUG_THIS_MODULE
# define print_buffer(x) print_simple_buffer(x)
#else
# define print_buffer(x)
#endif


#if DEBUG_THIS_MODULE
/*
 * Wrap debug message functions
 */
#define DBGM(...) dmsg(__VA_ARGS__) // DEBUG this module
#else
#define DBGM(...)
#endif


int get_adc_index_from_name(char* assigned_names[], char* name);
SWE_Data* create_adc_data_field(char* name);
int check_analog_measurement_settings(Output** outputs, ProcessSetting** settings, int settings_point);

/*
 * ADC Measurement can manage up to 8 adc and combine them into the same data structure
 *
 */
#define FIELD_NAME_LENGTH 128
#define NUMBER_OF_ADC_CHANNELS 16

int analog_measurement_constructor(SimpleProcess* conf, SchedulerProcess* process){
	int errors = 0;
	// Temporal strings for the adc assignements //
	char adc0_field_name[FIELD_NAME_LENGTH] = "";
	char adc1_field_name[FIELD_NAME_LENGTH] = "";
	char adc2_field_name[FIELD_NAME_LENGTH] = "";
	char adc3_field_name[FIELD_NAME_LENGTH] = "";
	char adc4_field_name[FIELD_NAME_LENGTH] = "";
	char adc5_field_name[FIELD_NAME_LENGTH] = "";
	char adc6_field_name[FIELD_NAME_LENGTH] = "";
	char adc7_field_name[FIELD_NAME_LENGTH] = "";
	char adc8_field_name[FIELD_NAME_LENGTH] = "";
	char adc9_field_name[FIELD_NAME_LENGTH] = "";
	char adc10_field_name[FIELD_NAME_LENGTH] = "";
	char adc11_field_name[FIELD_NAME_LENGTH] = "";
	char adc12_field_name[FIELD_NAME_LENGTH] = "";
	char adc13_field_name[FIELD_NAME_LENGTH] = "";
	char adc14_field_name[FIELD_NAME_LENGTH] = "";
	char adc15_field_name[FIELD_NAME_LENGTH] = "";


	// Group all the field names into a single string array //
	char* adc_channels_assignements[NUMBER_OF_ADC_CHANNELS] = {
			adc0_field_name,
			adc1_field_name,
			adc2_field_name,
			adc3_field_name,
			adc4_field_name,
			adc5_field_name,
			adc6_field_name,
			adc7_field_name,
			adc8_field_name,
			adc9_field_name,
			adc10_field_name,
			adc11_field_name,
			adc12_field_name,
			adc13_field_name,
			adc14_field_name,
			adc15_field_name
	};

	msg_sub_title( WHT, "ADC Measurement Constructor");
	dmsg( "allocating space for internal data...");
	AnalogMeasurementData* internal_data=swe_malloc(sizeof(AnalogMeasurementData));
	process->internal_data=(void*)internal_data;

	dmsg("In SimpeProcess there are %d outputs", ARRAY_LENGTH(conf->outputs));
	int noutputs = ARRAY_LENGTH(conf->outputs);
	int i;
	for (i=0 ; i<noutputs ; i++) {
		dmsg("Output %d of %d, name \"%s\"", i+1, noutputs, conf->outputs[i]->name);
	}

	dmsg( "assigning execution pointer...");
	process->exec=analog_measurement_exec;

	dmsg( "setting default values to the process internal settings...");

	// No default values

	dmsg( "Adding configurable settings...");

	int settings_point = ARRAY_LENGTH(process->settings); // Store the point before adding
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/ADC0", (void*)adc0_field_name);
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/ADC1", (void*)adc1_field_name);
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/ADC2", (void*)adc2_field_name);
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/ADC3", (void*)adc3_field_name);
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/ADC4", (void*)adc4_field_name);
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/ADC5", (void*)adc5_field_name);
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/ADC6", (void*)adc6_field_name);
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/ADC7", (void*)adc7_field_name);
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/ADC8", (void*)adc8_field_name);
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/ADC9", (void*)adc9_field_name);
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/ADC10", (void*)adc10_field_name);
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/ADC11", (void*)adc11_field_name);
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/ADC12", (void*)adc12_field_name);
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/ADC13", (void*)adc13_field_name);
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/ADC14", (void*)adc14_field_name);
	process->settings=add_settings_to_process(process->settings, SWE_Text, "parameters/ADC15", (void*)adc15_field_name);



	dmsg( "Configuring settings...");
	if(configure_process_settings(conf->settings, process)!=swe_ok) {
		errmsg( "configuration returned errors");
		return swe_error;
	}

	//----- Check if the Settings have been correctly set -----//
	if (check_analog_measurement_settings(conf->outputs, process->settings, settings_point)){
		return swe_error;
	}

	SchedulerData* data = swe_malloc(sizeof(SchedulerData)); // Allocate SchedulerData memory
	internal_data->analog_channels = swe_malloc(sizeof(int)*noutputs); // Allocate memory for channel array

	// Generate Scheduler Data //
	for ( i=0 ; i < noutputs ; i++ ) {
		int channel;
		if ((channel = get_adc_index_from_name(adc_channels_assignements, conf->outputs[i]->name)) < 0) {
			errmsg("Field %s has no channel assigned!");
			errors++;
		}
		imsg("Assigning ADC channel %d to field \"%s\"", channel, conf->outputs[i]->name);
		SWE_Data* field = create_adc_data_field(conf->outputs[i]->name);
		data->fields = ATTACH_TO_ARRAY(data->fields, field);
		data->count++;
		internal_data->analog_channels[i]=channel;
	}
	internal_data->data = data;

	// Initialize ADCs
	for ( i=0 ; i<data->count ; i++){
		dmsg("Initializing ADC %d", internal_data->analog_channels[i]);
		TRY_RET(adc_initialize(internal_data->analog_channels[i]));
	}

	// Activate the scheduler //
	if ( process->state_reg.sampling_rate > 0 ){
		process->state_reg.exec_modes.scheduler = TRUE;
	}

	// If there are errors, abort the constructor
	if (errors > 0) {
		return swe_error;
	}
	return swe_ok;
}


/*
 * Execution Handler for Analog Measurements
 */
SchedulerData* analog_measurement_exec(void* process_pointer, SchedulerData* nothing, ProcessStatus *errorcode){
	SchedulerProcess* process=(SchedulerProcess*)process_pointer;
	AnalogMeasurementData* internal_data=(AnalogMeasurementData*)process->internal_data;
	SchedulerData* data =internal_data->data;
	StateRegister* state_register = &process->state_reg;
	int i, ret;

	state_register->exec_flag = 0; // clear the flag, a local copy is still available
	*errorcode = process_chain_continue;
	internal_data->data->init_timestamp = swe_get_epoch_time();
	dmsg("ADC - timestamp %f", internal_data->data->init_timestamp);
	for ( i=0 ; i<data->count ; i++){
		float meas;
		float *value  =	(float*)data->fields[i]->value;
		ret = adc_read(internal_data->analog_channels[i], &meas);
		// Process errors //
		if (ret < 0) {
			errmsg("adc_read returned %d", ret);
			*errorcode = process_chain_stop;
			return NULL;
		}
		imsgn("ADC - Field ");
		cimsgn(BLU,"%s",data->fields[i]->name);
		imsgn(" (ADC %d) ", internal_data->analog_channels[i]);
		cimsg(GRN, "%f",data->fields[i]->name, internal_data->analog_channels[i], meas);
		*value = meas;
	}
	return data;
}

/*
 * Checks if "name" is in the "assigned_names" string list. If it is it's position + 1 is returned,
 * otherwise -1 is returned
 */
int get_adc_index_from_name(char* assigned_names[], char* name){
	int i;
	for ( i=0 ; i<NUMBER_OF_ADC_CHANNELS ; i++){
		if (!compare_strings(assigned_names[i], name)) {
			return i; // found channel
		}
	}
	return -1;
}


/*
 * Create a unsigned int field
 */
SWE_Data* create_adc_data_field(char* name){
	SWE_Data* d = swe_malloc(sizeof(SWE_Data));
	d->name = set_string(name);
	d->encoding = float_data;
	d->element_count = 1;
	d->element_size = sizeof(float32);
	d->total_size = d->element_size;
	d->value = swe_malloc(d->element_size);
	return d;
}

/*
 * Checks if the Settings have been correctly setup
 *     - Checks that all ADCs have a valid field name (a field in outputs)
 */
int check_analog_measurement_settings(Output** outputs, ProcessSetting** settings, int settings_point){
	int i, j;
	int errors = 0;
	// Check that we didn't configure more channels than outputs in the SensorML //
	for (i=settings_point  ; i < ARRAY_LENGTH(settings); i++) {
		uchar found = FALSE; // Flag to determine if the field has been foud in outputs
		uchar active = FALSE; // Flag to determine if the setting has been activated (non-null value)
		char* setting_value = (char*)settings[i]->value;
		for ( j=0 ; j < ARRAY_LENGTH(outputs) ; j++ ) {

			// Check if the setting is active (a value has been set)  //
			if (setting_value[0] != 0) {
				active = TRUE;
			}
			// Compare the settings with the outputs //
			if (!compare_strings(setting_value, outputs[j]->name)){
				found = TRUE;
			}
		}
		if (!found && active) {
			errmsg("Field \"%s\" in settings not found in SimpleProcess output" ,setting_value);
			errors++;
		}
	}
	return errors;
}
