/*
 *  CSV Generator Module: stores incoming data into a csv file
 *
 *  @author: Enoc Martínez
 *  @institution: Universitat Politècnica de Catalunya (UPC)
 *  @contact: enoc.martinez@upc.edu
 */


#ifndef MODULES_INC_CSV_GENERATOR_H_
#define MODULES_INC_CSV_GENERATOR_H_

#include "core/simple_process.h"
#include "resources/formats/csv.h"
#include "swe_conf.h"

#define CSV_GENERATOR_UID "swebridge:modules:csvGenerator"


// if OPEN_AND_CLOSE_FILE is enabled, the temp file is opened and closed
// each time a measure is saved. It may be useful to monitor the bridge
// execution. However, it requires additional CPU time to open and
// close the files each time
#define CSV_DEFAULT_OPEN_AND_CLOSE_FILE ON
#define CSV_DEFAULT_OUTPUT_FOLDER "csv"
#define CSV_DEFAULT_DELIMITER ","
#define CSV_DEFAULT_ENDLINE "\n"
#define CSV_DEFAULT_PERIODICITY "hour"
#define CSV_DEFAULT_DECIMAL_PRECISION 6


typedef enum {
	csv_periodicity_minute = 0,
	csv_periodicity_hour,
	csv_periodicity_day,
	csv_periodicity_month,
	csv_periodicity_year
}csv_periodicity;


/*
 * Internal CSV Generator data
 */
typedef struct {
	CsvFile* csv;
	DateTime init_datetime; // first timestamp in file
	uchar msecs_timestamp;  // if set to TRUE timestamps will be

	// naming and formatting //
	char file_prefix[256]; // prefix to be used in CSV files
	char output_folder[256]; // prefix to be used in CSV files
	char delimiter[4]; // measure separator
	char endline[4];   // end of line

	csv_periodicity periodicity; // defines how the CSV files will be generated, every minute, hour or every day
	uchar georeference; // sensor position will be added to each measure
	uchar depth; // depth will be added to each measure
	uchar msec_timestamp;
	uchar time_range;
	int decimal_precision;
}CsvGeneratorData;


int csv_generator_constructor(SimpleProcess* conf, SchedulerProcess* process);
SchedulerData* csv_generator_exec(void* process_pointer, SchedulerData* data, ProcessStatus* errorcode);


#endif /* MODULES_INC_CSV_GENERATOR_H_ */
