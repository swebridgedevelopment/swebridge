/*
 * scheduler.h
 *
 *  Created on: Sep 20, 2016
 *      Author: enoc
 */

#ifndef INC_NEW_SCHEDULER_H_
#define INC_NEW_SCHEDULER_H_

#include "core/simple_process.h"


/*
 * Structure to store scheduler options that are not related to instruments
 */
typedef struct {
	uchar deep_sleep;
	float deep_sleep_time; // minimum deep sleep time (seconds)
}SchedulerOptions;

/*
 * Scheduler Status
 */
#define SCHEDULER_STOPPED 0
#define SCHEDULER_RUNNING 1
#define SCHEDULER_PAUSED 2

/*
 * Scheduler structure where all the processes are stored
 */
typedef struct{
	ulong tick_rate_us;
	uchar status; // Flag to enable / disable the scheduler
	SchedulerProcess** process_array; //space where all the processes are stored

	SchedulerProcess** init_array; // Pointers to all processes involved in the init sequence
	SchedulerProcess** current_array; // Process array currently in use

	Interface* iface;
	SchedulerOptions options;
}Scheduler;



Scheduler* scheduler_setup(SchedulerProcess** process_array, SchedulerOptions* scheduler_options);

int scheduler_init();
int scheduler_start();
int scheduler_pause();
int scheduler_reset();
int scheduler_stop();
int scheduler_resume();



#endif /* INC_NEW_SCHEDULER_H_ */
