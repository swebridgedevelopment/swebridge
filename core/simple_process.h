/*
 *  This file contains the headers and structures of all functions related to the
 * "simple_process" structures. This processes are structures which contain
 *  data identifiers and handler functions that allow them to perform a specific
 *  operation. This processes may be concatenated into what is called a "process
 *  chain".  *
 *
 *  @author: Enoc Martínez
 *  @institution: Universitat Politècnica de Catalunya (UPC)
 *  @contact: enoc.martinez@upc.edu
 */

#ifndef INC_SIMPLE_PROCESS_H_
#define INC_SIMPLE_PROCESS_H_

#include "common/sensorml.h"
#include "common/stream_parser.h"
#include "common/high_freq_stream.h"
#include "resources/resources.h"
#include "swe_conf.h"

//---------------------------------------------------------------------------//
//------------------------------ DEFINITIONS --------------------------------//
//---------------------------------------------------------------------------//

//---- SchedulerProcess trigger codes ----//


// An interruption in the communication's intenrface occurred
#define IFACE_INTERRUPT_TRIGGER   1

// The scheduler triggered the process based on its sampling rate
#define SCHEDULER_TRIGGER        (2 << 0)

// The previous process triggered this
#define PREVIOUS_PROCESS_TRIGGER (2 << 1)

// An internal timeout expired
#define INTERNAL_TIMEOUT_TRIGGER (2 << 2)

// The process was internally retriggered (a process triggered itself to finish some ongoing job)
#define INTERNAL_RETRIGGER       (2 << 3)

// The process is triggered after the "DelayBefore" timer expires
#define DELAY_BEFORE_TRIGGER     (2 << 4)

// The process is triggered after the "DelayAfter" timer expires
#define DELAY_AFTER_TRIGGER       (2 << 5)



#define EXEC_FLAG_NAMES {	\
	"IFACE_INTERRUPT", \
	"SCHEDULER", \
	"PREVIOUS_PROCESS", \
	"INTERNAL_TIMEOUT", \
	"INTERNAL_RETRIGGER", \
	"DELAY_BEFORE_TRIGGER", \
	"DELAY_AFTER_TRIGGER", \
}


/*
 * The ProcessStatus flags indicate the exit status of a SchedulerProces. It is used to control the execution
 * flow, detect errors and communicate processes.
 */
typedef enum  {
	// Process requests to stop the execution of the process. Generally the process did not finish
	// all tasks or is waiting for some particular event
	process_chain_stop = 1,

	// Process requests the execution of next processes.
	process_chain_continue = 2,

	// Flush next processes. The current processes requests that next processes finish all ongoing tasks, e.g.
	// close opened files, clear buffers, etc.
	process_chain_flush = 3,

	// Generic error, a recoverable error
	process_chain_error = -1,

	// Unrecoverable error occurred in the process. The process requests to stop the scheduler
	process_chain_fatal_error = -2

} ProcessStatus;


//---------------------------------------------------------------------------//
//--------------------------- PROCESS STRUCTURES ----------------------------//
//---------------------------------------------------------------------------//

/*
 * Temporal structure used to link the SensorML SimpleProcess to the
 * SchedulerProcess variables
 */
typedef struct {
	const char* ref; //setting name
	void* value; //pointer to the value stored in the internal data structure
	swe_component_type type;
}ProcessSetting;



/*
 * Structure to store the Execution Modes
 */
typedef struct{
	uchar previous_process; // Allow execution by previous process
	uchar scheduler; // Allow execution by the scheduler as a free timer, controlled by sampling rate
	uchar iface_interrupt; // Allow execution on an interruption on comms interface
	uchar initialization;  // Allow execution at initialization step
	uchar scheduled_process;   // Executes the process at a specific time
}ExecutionModes;



#define SCHEDULED_CONF_STRINGS_LENGTH 64


/*
 * This structure hosts all the components required to delay the execution of a process before or after
 * being triggered
 */
typedef struct {
	float64 period; // time to wait before executing process (in seconds)
	ulong ticks_left; // ticks left to execute the process
	uchar exec_flag; // store execution flags here until the timer expires
	void* data; // pointer to store the reference to the data
}DelayExecution;


/*
 * The StateRegister contains all the information related to a process execution
 */
typedef struct {
	// Execution include a set of flag that allows the process to be triggered under certain
	// conditions (interrupt in the interface, periodically by the scheduler, etc.)
	ExecutionModes exec_modes;

	// Exec Flag determines if a process has to be executed or not. It contains the trigger condition
	// (or conditions) that led to its execution
	uchar exec_flag;
	uchar interrupt_flag; // Flag that determines that there has been an interruption on the iface

	float64 sampling_rate; //sampling rate in seconds, only used if the execution_mode has the Scheduler_timer set to true
	ulong trigger_ticks;
	ulong current_ticks;

	float64 watchdog_period; // Process watchdog timer period in seconds
	ulong watchdog_current_ticks;
	ulong watchdog_target_ticks;  // Number of ticks to be reached to trigger the watchdog. When the watchdog is triggered
	                              // the scheduler is stopped

	// Scheduled Process: Trigger process at specific time, e.g. trigger process every hour, or every minute **:13
	char scheduled_start_conf[SCHEDULED_CONF_STRINGS_LENGTH];
	char scheduled_period_conf[SCHEDULED_CONF_STRINGS_LENGTH];
	float64 scheduler_period;
	float64 scheduler_start;
	short start_at_hour; // start at hour (-1 if not defined)
	short start_at_min;  // start at min (-1 if not defined)
	short start_at_sec;  // start at second (-1 if not defined)

	// Delay between processes
	DelayExecution delay_before;
	DelayExecution delay_after;

	// Internal timeout to be used by each process
	float64 internal_timeout;


	// Scheduler tick rate in microseconds
	ulong tick_rate_us;
	uchar expecting_response; // This flag is set in query processes to indicate to the scheduler that a response
	                          // is being expected (e.g. to avoid go to deep sleep)
}StateRegister;





/*
 * Structure to store and manage data acquired during scheduler execution
 */
typedef struct{
	SWE_Data** fields;
	uint count; // number of SWE Data elements
	float64 init_timestamp; // Initial timestamp
	float64 end_timestamp; // End of observation timestamp

	// Scheduler Buffer for high frequency streaming data, only used in real-time
	// processing applications.
#if ENABLE_HIGH_FREQUENCY_MODULES
	HighFreqStreamBuffer* streambuff;
#endif
}SchedulerData;


/*
 * This structure contains all the necessary information to execute a
 * scheduler process. Each of these processes are a SWE Bridge implementation
 * of the SensorML's SimpleProcess element.
 */
typedef struct {
	char* name; //process name
	char* id; // Scheduler identifier (instrument_name-proces_name=
	StateRegister state_reg;
	void* internal_data; //pointer to the internal data structure
	void* input;   // Previous process (if any)
	void** output; // Array of processes connected to this one
	Interface* iface;
	ProcessSetting** settings; // temporal structure used to store and process settings
							   // after initialization is set to NULL

	// Handler methods
	SchedulerData* (*exec)(void* process, SchedulerData* data_in, ProcessStatus* errorcode); //execution function
	int (*flush)(void* process); // Finish all ongoing tasks (e.g. close files, reset buffers, etc.)
	int (*error)(void* process); // Manage errors ocurred in previous processes

}SchedulerProcess;


/*
 * ModuleConstructor relates a construction function with a uid
 */
typedef struct{
	const char* uid;
	int (*constructor)(SimpleProcess* conf, SchedulerProcess* process);
}ModuleConstructor;


//---------------------------------------------------------------------------//
//--------------------------------- FUNCTIONS -------------------------------//
//---------------------------------------------------------------------------//

//---- Generate, Setup & Execute Processes ----//
int execute_process_chain(SchedulerProcess* process, SchedulerData* data);
ProcessSetting** add_settings_to_process(ProcessSetting** settings, swe_component_type type, const char* ref, void* value);
int configure_process_settings(Setting** settings_conf, SchedulerProcess* process);
SchedulerProcess** get_process_chain(SchedulerProcess* start_process, SchedulerProcess** chain);

//---- Show Processes ----//
int show_process_chain(SchedulerProcess* start_process);
int show_all_process_chains(SchedulerProcess** processes);
int show_scheduler_data(SchedulerData* data);
int show_state_register(SchedulerProcess* process);


//---- Process Timeout ----//
int process_timeout_set(StateRegister* reg, float64 timeout);
int process_timeout_cancel(StateRegister* reg);
int process_watchdog_set(StateRegister* reg);

//---- Arrange Processes ----//
AggregateProcess* merge_instrument_with_mission(AggregateProcess* mission, PhysicalSystem* instrument);
SchedulerProcess** generate_processes_from_mission(AggregateProcess* mission);
SchedulerProcess** generate_shceduler_process_list(AggregateProcess** missions);
SchedulerProcess** merge_process_arrays(SchedulerProcess** array1, SchedulerProcess** array2);


//---- Trigger process at a specific time ----//
float64 calculate_scheduled_start_time(StateRegister* reg);

//---- Others ----//
SchedulerData* scheduler_data_from_response_structure(ResponseStructure* resp);

int data_string_from_scheduler_data(SchedulerData* data, char* buffer, char *delimiter, char *endline,
		uchar georeference, uchar altitude, uchar precision, uchar msecs, uchar timerange);
int generate_data_filename(char* buffer, char* folder, char* prefix, char* extension, float64 timestamp, uchar hour, uchar min, uchar sec);


#endif //INC_SIMPLE_PROCESS_H_
