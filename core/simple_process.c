/*
 *  This file contains the implementation of all functions related to the
 * "simple_process" anc "scheduler_process" structures. These are structures with
 *  data identifiers and handler functions that allow them to perform a specific
 *  operation. This processes may be concatenated into what is called a "process
 *  chain".
 *
 *  @author: Enoc Martínez
 *  @institution: Universitat Politècnica de Catalunya (UPC)
 *  @contact: enoc.martinez@upc.edu
 */

#include "core/simple_process.h"
#include "common/swe_utils.h"
#include "resources/mem_control.h"
#include "resources/resources.h"

// Include all modules //
#include "modules/analog_measurement.h"
#include "modules/insert_result.h"
#include "modules/instrument_command.h"
#include "modules/internal_sensors.h"
#include "modules/linear_calibration.h"
#include "modules/power_management.h"
#include "modules/subsampling.h"
#include "modules/zabbix_sender.h"
#include "modules/hydrophone_stream.h"
#include "modules/sound_pressure_level.h"
#include "modules/wav_generator.h"
#include "modules/wav_reader.h"
#include "modules/csv_generator.h"
#include "modules/json_udp_sender.h"



//----- Private Functions ----//
SchedulerProcess* new_scheduler_process(SimpleProcess* conf, char* instrument_name);
SchedulerProcess** connect_processes(SchedulerProcess** process_array, AggregateProcess* conf);
int setup_parameter(Setting* conf, SchedulerProcess* process);
const ModuleConstructor* get_module_constructor(SimpleProcess* conf);
SchedulerProcess* find_process_from_path(SchedulerProcess** array, char* process_id);
SchedulerData* scheduler_data_from_response_structure(ResponseStructure* resp);
int set_scheduler_process_execution_modes(SchedulerProcess* proc);

void show_process_trigger_flags(SchedulerProcess* process);

int flush_process_chain(SchedulerProcess* process);
int propagate_error_in_process_chain(SchedulerProcess* process);
int configure_scheduled_state_register(SchedulerProcess* process);

// Delay Before/After
int set_delay_before(SchedulerProcess* process, SchedulerData* data);
int set_delay_after(SchedulerProcess* process, SchedulerData* data);
SchedulerData* expire_delay_before(SchedulerProcess* process);
SchedulerData* expire_delay_after(SchedulerProcess* process);



/*
 * List of implemented modules, matching its unique identifier with its constructor
 */
const ModuleConstructor swe_module_list[]= {
		{INSTRUMENT_COMMAND_UID, instrument_command_constructor},
		{INSERT_RESULT_UID, insert_result_constructor},
		{SUBSAMPLING_UID, subsampling_constructor},
		{LINEAR_CALIBRATION_UID, linear_calibration_constructor},
		{POWER_MANAGEMENT_UID, power_management_constructor},
		{INTERNAL_SENSORS_UID, internal_sensors_constructor},
		{ANALOG_MEASUREMENT_UID, analog_measurement_constructor},
		{ZABBIX_SENDER_UID, zabbix_sender_constructor},
		{CSV_GENERATOR_UID, csv_generator_constructor},
		{JSON_UDP_SENDER_UID, json_udp_sender_constructor},
#if ENABLE_HIGH_FREQUENCY_MODULES
		{HYDROPHONE_STREAM_UID, hydrophone_stream_constructor},
		{SOUND_PRESSURE_LEVEL_UID, sound_pressure_level_constructor},
		{WAV_GENERATOR_UID, wav_generator_constructor},
		{WAV_READER_UID, wav_reader_constructor},
		{HYDROPHONE_STREAM_UID_OLD, hydrophone_stream_constructor},
#endif
		// Deprecated UIDs //
		{INSERT_RESULT_UID_OLD, insert_result_constructor},
		{INSERT_RESULT_UID_OLD, insert_result_constructor},
		{NULL, NULL}
};

const char* exec_flag_names[] = EXEC_FLAG_NAMES ;




//---------------------------------------------------------------------------//
//--------------------- GENERATE & EXECUTE PROCESSES ------------------------//
//---------------------------------------------------------------------------//

/*
 * This functions takes a scheduler process and executes its handler. After
 * if it has any connected processes at its outputs, executes their handlers
 * as well
 */
int execute_process_chain(SchedulerProcess* process, SchedulerData* data){
	ProcessStatus errorcode = process_chain_continue;
	int i;
	SchedulerData* new_data;
	SchedulerProcess* next_process;
	CHECK_NULL(process, swe_invalid_arguments);
	StateRegister* state_reg = &process->state_reg;

	show_process_trigger_flags(process);

	// Check if the process has a DelayBefore configured
	if ( state_reg->delay_before.period > 0 ){
		if (! (state_reg->exec_flag & DELAY_BEFORE_TRIGGER) ) {
			// If the processes has been triggered by any condition other than the delay before timer
			// setup delay before timer and exit
			cimsg(BLU, "Setting DelayBefore timer: %.02f seconds", state_reg->delay_before.period);
			return set_delay_before(process, data);
		}
		else {
			// Otherwise, expire the delay before timer
			cimsg(BLU, "DelayBefore expired");
			data = expire_delay_before(process);
		}
	}

	// Check if the process has a DelayAfter configured
	if ( state_reg->delay_after.period > 0 ){
		if ( (state_reg->exec_flag & DELAY_AFTER_TRIGGER) ) {
			// If process is triggered by delay after, expire the timer
			cimsg(MAG, "DelayAfter expired");
			new_data = expire_delay_after(process);
			 errorcode = process_chain_continue;
		}
		else {
			// if the process was triggered by any other condition, execute the process and then
			new_data = process->exec(process, data, &errorcode);

			if ( errorcode == process_chain_continue && process->output != NULL) {
				// Only set the delay after if are more processes in the chain
				cimsg(MAG, "Setting DelayAfter timer: %.02f seconds", state_reg->delay_after.period);
				return set_delay_after(process, new_data);
			}
		}
	}

	// If DelayAfter is not configured, just execute the handler
	else {
		new_data = process->exec(process, data, &errorcode);
	}



	if ( errorcode == process_chain_stop) {
		return swe_ok;
	}
	else if ( (errorcode == process_chain_continue) && (process->output != NULL)) {
		// Continue execution the process chain
		for(i=0; process->output[i]!= NULL; i++){
			next_process = (SchedulerProcess*)process->output[i];
			next_process->state_reg.exec_flag |= PREVIOUS_PROCESS_TRIGGER;
			TRY_RET(execute_process_chain(next_process, new_data));
		}
	}
	// Flush next processes
	else if ( (errorcode == process_chain_flush) && (process->output != NULL)) {
		for(i=0; process->output[i]!=NULL; i++){
			next_process = (SchedulerProcess*)process->output[i];
			TRY_RET(flush_process_chain(next_process));
		}
	}
	// Propagate error
	else if ( (errorcode == process_chain_error) && (process->output != NULL)) {
		for(i=0; process->output[i]!=NULL; i++){
			errmsg("Process %s returned error", process->id);
			next_process = (SchedulerProcess*)process->output[i];
			TRY_RET(propagate_error_in_process_chain(next_process));
		}
	}
	// Exit in error mode
	else if ( errorcode == process_chain_fatal_error ) {
		errmsg("Process %s returned fatal error!", process->id);
		return swe_error;
	}
	return swe_ok;
}



/*
 * Once the delay before expires, process it
 */
SchedulerData* expire_delay_register(SchedulerProcess* process, DelayExecution* delay_register) {
	StateRegister* sr = &process->state_reg;
	sr->exec_flag = delay_register->exec_flag;
	return (SchedulerData*)delay_register->data;
}

/*
 * Configures the delay after a scheduler process
 */
int set_delay_register(SchedulerProcess* process, SchedulerData* data, DelayExecution* delay_register) {
	StateRegister* sr = &process->state_reg;

	if ( delay_register->ticks_left > 0 ){
		warnmsg("Delay already set!");
		delay_register->exec_flag = sr->exec_flag; // store the execution flag
		sr->exec_flag = 0; // clear the execution flag
		return swe_ok;
	}

	delay_register->data = (void*)data; // store data
	delay_register->exec_flag = sr->exec_flag; // store the execution flag
	sr->exec_flag = 0; // clear the execution flag

	delay_register->ticks_left = (ulong) ((1000000*delay_register->period) /((float64)sr->tick_rate_us));
	return swe_ok;
}


/*
 * Configures the delay after a scheduler process
 */
int set_delay_before(SchedulerProcess* process, SchedulerData* data){
	return set_delay_register(process, data, &process->state_reg.delay_before);
}

int set_delay_after(SchedulerProcess* process, SchedulerData* data){
	return set_delay_register(process, data, &process->state_reg.delay_after);
}

SchedulerData* expire_delay_before(SchedulerProcess* process) {
	return expire_delay_register(process, &process->state_reg.delay_before);
}

SchedulerData* expire_delay_after(SchedulerProcess* process) {
	return expire_delay_register(process, &process->state_reg.delay_after);
}


/*
 * Execute all error handlers in the process chain
 */
int propagate_error_in_process_chain(SchedulerProcess* process){
	int i;
	SchedulerProcess* next_process;

	if ( process->error != NULL ) {
		cdmsgn(YEL,"Propagating error to process ID: %s    ", process->id);
		TRY_RET(process->error(process));
	}
	if ( process->output != NULL ) {
		for(i=0; process->output[i] != NULL; i++){
			next_process = (SchedulerProcess*)process->output[i];
			TRY_RET(propagate_error_in_process_chain(next_process));
		}
	}
	return swe_ok;
}




/*
 * Execute all error handlers in the process chain
 */
int flush_process_chain(SchedulerProcess* process){
	int i, ret = 0;
	SchedulerProcess* next_process;
	if ( process->flush != NULL ) {
		cimsg(MAG,"Flushing process ID: %s    ", process->id);
		ret = process->flush(process);
	}
	// if returned error propagate errors instead of flush
	if ( ret < 0 && process->output != NULL ) {
		for(i=0; process->output[i] != NULL; i++){
			next_process = (SchedulerProcess*)process->output[i];
			TRY_RET(propagate_error_in_process_chain(next_process));
		}
	}
	// else propagate flush
	else {
		if ( process->output != NULL ) {
			for(i=0; process->output[i] != NULL; i++){
				next_process = (SchedulerProcess*)process->output[i];
				TRY_RET(flush_process_chain(next_process));
			}
		}
	}
	return swe_ok;
}


/*
 * Shows the info of which project will be executed and its exec_flags
 */
void show_process_trigger_flags(SchedulerProcess* process){
	int i;
	cdmsgn(CYN,"\nProcess ID: %s    ", process->id);
	dmsgn("(");
	for ( i = 0; i < 7 ; i++ ){
		int mask = PWR(2,i);
		if ( mask & process->state_reg.exec_flag) {
			dmsgn("%s ", exec_flag_names[i] );
		}
	}
	dmsg(")");
}

/*
 * This functions looks at the Modules List and returns the appropriate
 * Module
 */
const ModuleConstructor* get_module_constructor(SimpleProcess* conf){
	int i;
	SimpleProcess* parent = NULL;


	// Loop through the module list and look for a matching uid
	for ( i=0 ; swe_module_list[i].uid != NULL ; i++ ) {
		if (!compare_strings(conf->typeOf, (char*)swe_module_list[i].uid)){
			dmsg("Constructor found \"%s\"", conf->typeOf);
			return &swe_module_list[i];
		}
	}

	// If the conf->typeOf did not match maybe it inherits from another process that does
	parent = (SimpleProcess*) conf->parent;

	while (parent != NULL ) {
		// Loop through the parents until a correct typeOf is found or until
		// the root SimpleProcess is reached
		for ( i=0 ; swe_module_list[i].uid != NULL ; i++ ) {
			if (!compare_strings(parent->typeOf, (char*)swe_module_list[i].uid)){
				return &swe_module_list[i];
			}
		}
		// If the the current process' typeof does not match, search for its parent
		parent = (SimpleProcess*) conf->parent;
	}

	errmsg("No module implementation found for typeOf \"%s\"", conf->typeOf);
	return NULL;
}


/*
 * This function generates a SchedulerProcess structure from a SimpleProcess.
 * All the common information is set, making it ready to be passed to a
 * module constructor
 */
SchedulerProcess* new_scheduler_process(SimpleProcess* conf, char* instrument_name){

	if (conf == NULL ){
		errmsg("new_scheduler_process: Received NULL SimpleProcess");
		return NULL;
	}

	if (instrument_name == NULL ){
		errmsg("new_scheduler_process: Received NULL instrument_name");
		return NULL;
	}

	SchedulerProcess* process = swe_malloc(sizeof(SchedulerProcess));
	// Assign identifiers //
	process->name = set_string(conf->name);
	/*process->uid=set_string(conf->uid);
	process->typeOf=set_string(conf->typeOf);*/
	// Generate SchedulerProcess Name //
	// The process id is instrument_name+process_name

	process->state_reg.sampling_rate = -1.0; // Set negative value by default
	process->state_reg.scheduled_start_conf[0] = 0; // Set null string at trigger_at_time by default


	process->id = swe_malloc(sizeof(char)*( strlen(instrument_name) + strlen(process->name) + 2) );
	sprintf(process->id, "%s-%s", instrument_name, process->name);



	// Generate the Generic Settings //
	process->settings=add_settings_to_process(process->settings,
			SWE_Quantity, // Settings Data Type
			"parameters/samplingRate", // Ref value
			(void*)(&process->state_reg.sampling_rate)); // Pointer where to where the variable is allocated


	// Scheduled Process //
	process->settings=add_settings_to_process(process->settings,
			SWE_Text,
			"parameters/schedulerStart",
			(void*)(&process->state_reg.scheduled_start_conf));

	process->settings=add_settings_to_process(process->settings,
			SWE_Text,
			"parameters/schedulerPeriod",
			(void*)(&process->state_reg.scheduled_period_conf));

	// Delay between processes //
	process->settings=add_settings_to_process(process->settings,
			SWE_Quantity,
			"parameters/delayAfter",
			(void*)(&process->state_reg.delay_after));

	process->settings=add_settings_to_process(process->settings,
			SWE_Quantity,
			"parameters/delayBefore",
			(void*)(&process->state_reg.delay_before));

	// Setting ExecutionModes //
	process->settings=add_settings_to_process(process->settings, SWE_Boolean,
			"parameters/executionModes/previousProcess",
			(void*)(&process->state_reg.exec_modes.previous_process));

	process->settings=add_settings_to_process(process->settings, SWE_Boolean,
			"parameters/executionModes/scheduler",
				(void*)(&process->state_reg.exec_modes.scheduler));

	process->settings=add_settings_to_process(process->settings, SWE_Boolean,
			"parameters/executionModes/interfaceInterrupt",
				(void*)(&process->state_reg.exec_modes.iface_interrupt));

	process->settings=add_settings_to_process(process->settings, SWE_Boolean,
			"parameters/executionModes/initialization",
			(void*)(&process->state_reg.exec_modes.initialization));

	process->exec = NULL;
	process->flush = NULL;
	process->error = NULL;

	return process;
}


/*
 * Finds a process from a process array bye comparing its path.
 */
SchedulerProcess* find_process_from_path(SchedulerProcess** array, char* process_id){
		int i;
		uint n;
		char* process_path= set_string(process_id); //creating a local copy of the path
		if(process_path==NULL){
			errmsg("Process path is NULL");
			swe_free(process_path);
			return NULL;
		}
		char** source_path=get_substrings(process_path, "/", &n);
		if (source_path==NULL || n<2){
			errmsg( "error with path %s", process_path);
		}

		char* name=source_path[1]; //the process name should be the second element in the path name
		for(i=0; array[i]!=NULL; i++) {
			if(array[i]->name!=NULL){
				if(!compare_strings(name, array[i]->name)){
					swe_free(process_path);
					return array[i];
				}
			} else {
				errmsg("Process %d name is a NULL pointer!");
			}
		}
		swe_free(process_path);
		return NULL;
}


/*
  * This functions takes a pointer to a variable (void* value) and registers
 * it in the Settings structure with its type and a reference (identifier
 * for this variable). After it is registered this variable can be accessed
 * and modified by the swe bridge by using the Settings functions.
 *
 * The new setting is attached to the existing settings array.
 */
ProcessSetting** add_settings_to_process(ProcessSetting** settings, swe_component_type type, const char* ref, void* value){
	ProcessSetting* setting = NULL;
	// Check arguments //
	if(!check_if_basic(type)) {
		errmsg( "add_settings_to_process: type not valid");
		return NULL; //return input value
	}
	else if(value==NULL ||  ref==NULL) {
		errmsg( "add_settings_to_process: NULL pointer received");
		return NULL; //return input value
	}

	// Generate New Setting //

	setting = swe_malloc(sizeof(ProcessSetting));
	setting->ref = ref;
	setting->value = value;
	setting->type = type;

	settings = ATTACH_TO_ARRAY(settings, setting);

	return settings;
}


/*
 * This functions configures the settings of a process (Simple_process) by
 * assignign the values present in the configuration structure
 * (Settings_conf).
 */
int setup_parameter(Setting* conf, SchedulerProcess* process){
	int j;

	for(j=0; process->settings[j]!=NULL; j++) {
		if(!compare_strings(conf->ref, process->settings[j]->ref)){
			ProcessSetting* set=process->settings[j];
			//---- Boolean (uchar with true or false) ----//
			if(set->type==SWE_Boolean) {
				uchar* value=(uchar*)set->value;
				*value=get_true_false(conf->value, NULL);

			}
			//---- Count is an integer ----//
			else if(set->type==SWE_Count){
				int value;
				//value = atoi(conf->value);
				value = string_to_int(conf->value);
				int *pointer=(int*)set->value;
				*pointer=value;

			}
			//---- Quantity is a float value ----//
			else if(set->type==SWE_Quantity){
				float64 value;
				value=(float64)atof(conf->value);
				float64 *pointer=(float64*)set->value;
				*pointer=value;

			}
			//---- Text is a string ----//
			else if(set->type==SWE_Text){
				strcpy((char*)set->value, (char*)conf->value);

			} else {
				errmsg( "Unknown type");
				return swe_error;
			}
			return swe_ok;
		}
	}
	errmsg( "configure_parameter: elements \"%s\" not found", conf->ref);
	return swe_element_not_found;
}


/*
 * This functions searches for a specific setting in a simple process and
 * returns a pointer to its value. The type pointer is filled with the
 * setting type
 */
int configure_process_settings(Setting** settings_conf, SchedulerProcess* process){
	int i;
	int error, errors=0;


	if(process==NULL || process->settings==NULL){
		errmsg( "configure_settings: NULL pointer received");
		return swe_error;
	}

	if (settings_conf == NULL){
		warnmsg("process %s has no settings", process->name);
		return swe_ok;
	}

	for(i=0; settings_conf[i]!=NULL; i++){

		imsgn("setting parameter \"");
		cimsgn(MAG, "%s", settings_conf[i]->ref);
		imsgn("\" to \"");
		cimsgn(CYN, "%s", settings_conf[i]->value);
		imsg("\"");
		error = setup_parameter(settings_conf[i], process);
		if(error) {
			errors++;
		}
	}
	if(errors) {
		errmsg("Errors while configuring settings!");
		return swe_invalid_arguments;
	}

	// Process trigger at time options
	if ( process->state_reg.scheduled_period_conf[0] != 0 ) {
		TRY_RET(configure_scheduled_state_register(process));
	}
	return swe_ok;
}


/*
 * Processes partially a time pattern for trigger at. It should be a string with length 1 or 2,
 */
int process_trigger_at_time_pattern(char* pattern, short *result){
	if ( strlen(pattern)!=2 ){
		errmsg("Hours, minutes and seconds of \"triggerAtTime \" pattern should have length of 2 chars");
		return swe_invalid_arguments;
	}
	// Check if '**' or 'XX' sequence is used to mark 'every'
	if ( !compare_strings(pattern, "**") || !compare_strings(pattern, "xx") || !compare_strings(pattern, "XX") ) {
		*result = -1; // -1 means trigger at "every" hour/minute/second
		return swe_ok;
	}

	// Make sure that both chars are wihtin 48-57 in ascii table ('0' - '9')
	else if ( pattern[0] < 48 ||  pattern[0] > 57 || pattern[1] < 48 ||  pattern[1] > 57 ) {
		errmsg("pattern not a number pattern \"%s\"", pattern);
		return swe_invalid_arguments;
	}

	*result = (short)string_to_int(pattern);
	return swe_ok;
}


/*
 * Processes the scheduled period pattern. only numbers followed by a time indicator are valid:
 *     e.g. "10min" "10 minutes" "10 Min" are valid
 * valid time indicators:
 *     seconds: "sec" "seconds" "Seconds" "s"
 *     minutes: "min" "minutes" "Minutes" "Minute" "m"
 *     hours:   "hour" "hours" "Hour" "Hours" "h"
 */
int process_scheduled_period_pattern(char* pattern, ulong *result){
	// string_in_list(periodicity, periodicity_year
	char number[32];
	char units[32];
	static const char* seconds[] = {"sec", "second", "seconds", "s", NULL};
	static const char* minutes[] = {"min", "minute", "minutes", "m", NULL};
	static const char* hours[]   = {"hour", "hours", "h", NULL};
	int i;
	to_lower_case(pattern);
	collapse_white_spaces2(pattern, SCHEDULED_CONF_STRINGS_LENGTH);

	// set to 0
	memset(number, 0, 32);
	memset(units, 0, 32);

	// Locate the point where numbers end
	for ( i=0 ; i<strlen(pattern) ; i++ ){
		if ( pattern[i] < 48 || pattern[i] > 57){
			break;
		}
	}
	memcpy(number, pattern, i);
	*result = (ulong)string_to_int(number);

	memcpy(units, &pattern[i], strlen(pattern) - i);

	if (string_in_list(units, seconds)){
		// result already in seconds
	}
	else if (string_in_list(units, minutes)){
		*result *= 60; // convert minutes to seconds
	}
	else if (string_in_list(units, hours)){
		*result *= 60*60; // convert hours to seconds
	}
	else {
		errmsg("time units not recognized \"%s\"", units);
	}
	return swe_ok;
}


/*
 * Checks if the "scheduled process" mode has to be activated. To configure a scheduled process the following pattern should be passed:
 *     "hh:mm:ss"
 * In example "12:00:00"
 *
 * If a process needs to be executed more than one time per day, ** pattern should be used:
 * "**:13:00" ==> it will be trigger at every hour, minute 13 second 00
 */
int configure_scheduled_state_register(SchedulerProcess* process){
	char buffer[16];
	char** substrings;
	char *hour, *minute, *second;
	uint nstrings;
	StateRegister* reg = &process->state_reg;


	if ( strlen(process->state_reg.scheduled_start_conf) == 8 ) {
		strcpy(buffer, process->state_reg.scheduled_start_conf);
		substrings = get_substrings(buffer, ":", &nstrings);

		hour = substrings[0];
		minute = substrings[1];
		second = substrings[2];

		TRY_RET(process_trigger_at_time_pattern(hour,  &reg->start_at_hour));
		TRY_RET(process_trigger_at_time_pattern(minute, &reg->start_at_min));
		TRY_RET(process_trigger_at_time_pattern(second, &reg->start_at_sec));
	}
	else if(strlen(process->state_reg.scheduled_start_conf) == 0){
		warnmsg("Start at time not specified, default **:**:00");
		reg->start_at_hour = -1;
		reg->start_at_min = -1;
		reg->start_at_sec = 0;
	}
	else {
		errmsg("Trigger At option length not valid: got %d, expected 8", strlen(process->state_reg.scheduled_start_conf) );
		return swe_invalid_arguments;
	}
	ulong seconds;
	TRY_RET(process_scheduled_period_pattern(process->state_reg.scheduled_period_conf, &seconds));
	reg->scheduler_period = (float64)seconds;

	// activate the exec modes
	process->state_reg.exec_modes.scheduled_process = TRUE;

	return swe_ok;
}


/*
 * Calculates the first timestamp when a process with triggerAt exec mode should be executed
 */
float64 calculate_scheduled_start_time(StateRegister* reg){
	float64 now, next, first_trigger;
	DateTime next_trigger;
	char date[64];
	now = swe_get_epoch_time();
	ascii_datetime_utc(date, now);
	imsg("Calculating first triggerAtTime option");
	imsg("Trigger at period %.0f seconds", reg->scheduler_period);

	ascii_datetime_utc(date, now);
	dmsg("   \t current time:\t %s", date);
	first_trigger = now -1; // force it to a past time
	next = now;
	// Brute force approach to calculate first trigger: add 60 seconds and truncate to user input to see
	// if the calculated date is in the future (not in the past), if not keep adding 60.
	while ( first_trigger <= now ){


		// calculate next DateTime
		TRY_RET(datetime_from_timestamp(&next_trigger, (float64)next));

		// truncate it according to the start pattern
		if ( reg->start_at_hour >= 0 ) next_trigger.hour = reg->start_at_hour;
		if ( reg->start_at_min >= 0 ) next_trigger.minute = reg->start_at_min;
		if ( reg->start_at_sec  >= 0 ) next_trigger.second = reg->start_at_sec;

		// Calculate the truncated DateTime
		first_trigger = (ulong)datetime_to_timestamp(next_trigger);

		next += 60; // next is the accumulator
	}

	next += reg->scheduler_period;
	ascii_datetime_utc(date, first_trigger);
	imsg("First trigger at time:\t %s", date);

	next += reg->scheduler_period;
	ascii_datetime_utc(date, first_trigger + reg->scheduler_period);
	dmsg("Next trigger at time:\t %s", date);

	return first_trigger;
}



/*
 * This functions searches for a specific setting in a simple process and
 * returns a pointer to its value. The type pointer is filled with the
 * setting type
 */
void* get_setting(SchedulerProcess* process, char* ref, uchar* type){
	//void* parameter_value;
	if(process==NULL || ref==NULL) {
		errmsg( "get_setting: NULL pointer received");
		return NULL;
	}
	int i;
	for (i=0; process->settings[i]!=NULL; i++) {
		if(!compare_strings(process->settings[i]->ref, ref)){
			if(type!=NULL) *type=process->settings[i]->type;
			return process->settings[i]->value;
		}
	}
	return NULL;
}


/*
 * This function takes a process and returns an array with all the processes connected
 * to the initial process (and it's sons). If chain argument is not NULL the new chain
 * will be attached to existing chain, otherwise it is created
 */
SchedulerProcess** get_process_chain(SchedulerProcess* start_process, SchedulerProcess** chain) {
	SchedulerProcess* cprocess = start_process;  // Current process
	int j;
	chain = ATTACH_TO_ARRAY(chain, start_process);

	if (cprocess->output != NULL ) {
		for ( j=0 ; cprocess->output[j]!=NULL  ; j++) {
			SchedulerProcess* next = (SchedulerProcess*)cprocess->output[j];
			chain = get_process_chain(next, chain); // Attach son
		}
	}
	return chain;
}




//---------------------------------------------------------------------------//
//--------------------------- SHOW PROCESSES --------------------------------//
//---------------------------------------------------------------------------//

/*
 * Displays a process chain
 */
static uchar show_chain_lvl = 0;
int show_process_chain(SchedulerProcess* process){
	int i;
	char spaces[100];
	memset(spaces, '-', 100);
	spaces[show_chain_lvl*3] = '>' ;
	spaces[show_chain_lvl*3 + 1] = ' ' ;
	spaces[show_chain_lvl*3 + 2] = 0 ;
	imsgn("%s", spaces);
	imsg("%s", process->name);
	if (process->output != NULL) {
		for ( i=0 ; process->output[i]!=NULL ; i++ ) {
			show_chain_lvl++;
			show_process_chain((SchedulerProcess*)process->output[i]);
			show_chain_lvl--;
		}
	}
	return swe_ok;
}


/*
 * Displays all process chains
 */
int show_all_process_chains(SchedulerProcess** processes){
	int i;
	int chain_count=0;
	msg_sub_title( BLU, "Showing process chains");
	for(i=0; processes[i]!=NULL; i++) {
		//uchar exec_mode=processes[i]->state_reg.exec_modes;
		if(!(processes[i]->state_reg.exec_modes.previous_process)){
			chain_count++;
			//, exec_mode_names[exec_mode]);
			show_process_chain(processes[i]);
		}
	}
	return 0;
}


/*
 * Displays all processes
 */
int show_processes(SchedulerProcess** array){
	int i;
	dmsg( "Process count %d", ARRAY_LENGTH(array));
	for (i=0; array[i]!=NULL; i++) {
		imsg( "\n====== %s ===========", array[i]->name);
		imsg( "process %d %p", i, array[i]);
		imsg( "id %s", array[i]->id);
		imsg("Execution Modes: ");
		if (array[i]->state_reg.exec_modes.iface_interrupt) {
			imsg("    Interrupt");
		}
		if (array[i]->state_reg.exec_modes.scheduler) {
			imsg("    Scheduler");
		}
		if (array[i]->state_reg.exec_modes.initialization) {
			imsg("    Init");
		}
		if (array[i]->state_reg.exec_modes.previous_process) {
			imsg("    Previous Process");
		}



		imsg( "====================================");
	}
	return swe_ok;
}


/*
 * Displays state register information
 */
int show_state_register(SchedulerProcess* process) {
	StateRegister *reg = &process->state_reg;
	ExecutionModes em = reg->exec_modes;
	float tick_ms = ((float)reg->tick_rate_us)/1000;
	dmsg("------------------------------");
	dmsg("Process: %s", process->name);
	dmsg("tick rate %f ms", ((float)reg->tick_rate_us)/1000);
	dmsg("ExecutionModes");
	if (em.initialization) 	dmsg("    initialization  %d", em.initialization);
	if (em.previous_process) dmsg("    previousProcess %d", em.previous_process);
	if (em.iface_interrupt) dmsg("    iface interrupt %d", em.iface_interrupt);
	if (em.scheduler) dmsg("    scheduler       %d", em.scheduler);

	dmsg("ExecutionFlag");
	if (reg->exec_flag == FALSE ) {
		dmsg("    Not set");
	} else {
		int i;
		dmsgn("    ");
		for ( i = 0; i < 5 ; i++ ){
			int mask = PWR(2,i);
			if ( mask & process->state_reg.exec_flag) {
				dmsg("%s", exec_flag_names[i] );
			}
		}
	}
	if (reg->expecting_response) {
		dmsg("Expecting response: YES!");
	}
	if (reg->trigger_ticks ) {
		dmsg("SamplingRate %f msec left", ((float)reg->trigger_ticks - reg->current_ticks)*tick_ms);
	}

	if (reg->watchdog_target_ticks) {
		dmsg("Watchdog %f msec left", ((float)reg->watchdog_target_ticks - reg->watchdog_current_ticks)*tick_ms);
	}

	if (reg->internal_timeout > 0) {
		dmsg("Timeout %f msec left", 1000*(reg->internal_timeout - swe_get_epoch_time()));
	}
	dmsg("----------------------------------");

	return swe_ok;
}


/*
 * Prints the contents of SchedulerData
 */
int show_scheduler_data(SchedulerData* data){

	int i;
	dmsg("Data has %u fields", data->count);
	for ( i=0 ; i<data->count ; i++) {
		SWE_Data* field = data->fields[i];
		dmsg("    Field: %s", field->name);
		dmsg("        element size: %u", field->element_size);
		dmsg("        element count: %u", field->element_count);
		dmsg("        total size: %u", field->total_size);
		dmsgn("        ");
		print_swe_value(field->value, field->encoding);
	}

	// Print timestamp if present //
	if ( data->init_timestamp > 0 ){
		char datestr[64];
		ascii_datetime(datestr, data->init_timestamp, FALSE);
		dmsg("Timestamp:  %s", datestr);
	}

	return swe_ok;
}


//---------------------------------------------------------------------------//
//-------------------------  PROCESSES TIMEOUTS -----------------------------//
//---------------------------------------------------------------------------//

/*
 * Sets a timeout in a SchedulerProcess. timeout units in seconds
 */
int process_timeout_set(StateRegister* reg, float64 timeout){

	if (reg == NULL ){
		return swe_invalid_arguments;
	}
	if (timeout < 0) {
		errmsg("Can't set a negative timeout (%f)", timeout);
		return swe_invalid_arguments;
	}
	reg->internal_timeout = swe_get_epoch_time() + timeout;
	return swe_ok;
}


/*
 * Disable process timeout
 */
int process_timeout_cancel(StateRegister* reg){
	reg->internal_timeout = -1;
	return swe_ok;
}


/*
 * Sets the watchdog timer
 */
int process_watchdog_set(StateRegister* reg) {
	ulong ticks;
	float timeout_us = 1000000*reg->watchdog_period;
	ticks = timeout_us / reg->tick_rate_us;
	reg->watchdog_target_ticks = ticks;
	return swe_ok;
}


//---------------------------------------------------------------------------//
//-------------------------  ARRANGE PROCESSES ------------------------------//
//---------------------------------------------------------------------------//

/*
 * This functions takes a list of missions and generates a list of scheduler processes
 * (all process from all missions)
 *
 * Step   I : Loop through each mission in the input list (repeat steps II, III and IV
 * Step  II : For each SimpleProcess in a list search for its implementation in modules list
 *            according to its typeOf property
 * Step III : Generate an SchedulerProperty by
 *
 */
SchedulerProcess** generate_shceduler_process_list(AggregateProcess** missions){
	int i, j, errors = 0;
	SchedulerProcess** global_process_list = NULL ;
	dmsg("Generating SchedulerProcesses from missions");
	for (i=0 ; missions[i]!=NULL ; i++){
		AggregateProcess* ap = missions[i];
		dmsg("Generating mission %d, id %s", i+1 , ap->id);
		SchedulerProcess** mission_processes = NULL;
		if ((mission_processes=generate_processes_from_mission(missions[i])) == NULL ) {
			errmsg("Failed to generate processes for mission %s", missions[i]->id);
			return NULL;
		}
		dmsg("Merging processes from mission %s to global list", ap->id);
		// This mission is no longer needed, free its memory
		free_aggregate_process(missions[i]);
		// Attach the process list from the mission to the global list
		global_process_list = merge_process_arrays(global_process_list, mission_processes);
	}
	// Check if there are 2 processes with the same name //
	for ( i=0 ; global_process_list[i]!=NULL ; i++){
		for ( j=0 ; global_process_list[j]!=NULL; j++) {
			if (j == i){
				continue;
			}
			else if (!compare_strings(global_process_list[i]->id, global_process_list[j]->id)) {
				errmsg("Process %d and %d have the same id (%s), abort", i, j, global_process_list[j]->id );
				errors++;
			}
		}
	}
	// If there have been errors return NULL //
	if (errors)  {
		return NULL;
	}
	cimsg(GRN,"All missions created successfully!");
	return global_process_list;
}


/*
 * Generates a scheduler Simple_process list from mission.
 *
 * Step 1: Generate SchedulerProcesses
 * Step 2: Attach the process to the array
 * Step 3: Connect processes
 * Step 4: Call module constructors
 * Step 5: Set the ExecutionFlags
 *
 */
SchedulerProcess** generate_processes_from_mission(AggregateProcess* mission){
	int i, ret;
	msg_title(BLU, "Generating Mission %s", mission->id);
	SchedulerProcess** process_array = NULL;
	if ( mission == NULL ) {
		errmsg("NULL pointer received!");
		return NULL;
	}

	// Generate SchedulerProcesses //
	for ( i=0 ; mission->processes[i]!=NULL ; i++) {
		SchedulerProcess* scheduler_process = NULL;
		SimpleProcess* simple_process = mission->processes[i];

		dmsg("Generating new process \"%s\"...", simple_process->name);
		scheduler_process = CATCH_NULL(new_scheduler_process(simple_process, mission->instrument_id)); // create a generic SchedulerProcess
		cimsg(GRN, "constructor called successfully\n");

		// Step 2: Attach the process to the array //
		process_array = ATTACH_TO_ARRAY(process_array, scheduler_process);
	}

	// Step 3: Connect Processes //
	process_array = CATCH_NULL(connect_processes(process_array, mission));

	// Step 4: Call module constructors //
	for ( i=0 ; mission->processes[i]!=NULL ; i++ ) {
		const ModuleConstructor* module = NULL;
		SimpleProcess* simple_process = mission->processes[i];  // Process configuration
		SchedulerProcess* scheduler_process = process_array[i]; // SchedulerProcess

		// Step I: Search for the constructor //
		dmsg("Looking constructor for process \"%s\"", simple_process->name);
		if ((module = get_module_constructor(simple_process)) == NULL ) {
			errmsg("Constructor not found for process %s", simple_process->typeOf);
			return NULL;
		}

		imsgn("Calling constructor for process: ");
		cimsg(CYN,"%s", scheduler_process->id);

		// Call the constructor //
		if ( ( ret = module->constructor(simple_process, scheduler_process )) < 0 ) {
			errmsg("Error while calling constructor for module %s", module->uid);
			errmsg("constructor returned error %d: \"%s\"", ret, get_errorcode(ret));
			return NULL;
		}
	}

	// Step 5: Set the ExecutionFlags //
	for ( i=0 ; process_array[i]!=NULL ; i++ ){
		if ( set_scheduler_process_execution_modes(process_array[i]) != swe_ok ) {
			errmsg("Couldn't set execution modes for process %s", process_array[i]->name);
		}
	}

	if (process_array == NULL) {
		errmsg("No process has been generated, aborting");
		return NULL;
	}
	dmsg("Showing process array");
	dmsg("---------------------");
	for (i=0 ; process_array[i]!=NULL ; i++) {
		dmsg("Process[%d] %s", i, process_array[i]->id);
	}
	dmsg("---------------------");
	return process_array;
}


/*
 * This function checks a process to set the ExecutionModes structure automatically
 */
int set_scheduler_process_execution_modes(SchedulerProcess* proc){
	StateRegister* statereg = &proc->state_reg;
	ExecutionModes* execmodes = &proc->state_reg.exec_modes;

	// If this process is connected with a previous process set the previous process flag
	if ( proc->input != NULL ) {
		execmodes->previous_process = TRUE;
	}

	// If sampling rate is greater than 0 set the scheduler flag
	if ( statereg->sampling_rate > 0.0 ) {
		execmodes->scheduler = TRUE;
		statereg->exec_flag |= SCHEDULER_TRIGGER;
	}

	return swe_ok;
}


/*
 * This functions connects the processes according to the connections
 * defined in the Brdige_conf structure. A connection is made by linking
 * the output field of a process into the execution handler of the next.
 * If a process doesn't have any output (output==NULL), then its the end of
 * the chain
 */
SchedulerProcess** connect_processes(SchedulerProcess** process_array, AggregateProcess* mission){
	int i;
	imsg("\nEstablising connections...");
	if ( mission->connections== NULL) {
		warnmsg("No connections in mission %s!", mission->id);
		return process_array;
	}
	for(i=0; mission->connections[i]!=NULL; i++){
		dmsg( "Connection %d of %d...", i+1, ARRAY_LENGTH(mission->connections));
		SchedulerProcess* source=find_process_from_path(process_array, mission->connections[i]->source);
		if(source==NULL) {
			errmsg( "Source %d not found (\"%s\")", i, mission->connections[i]->source);
			return NULL;
		}
		SchedulerProcess* destination=find_process_from_path(process_array, mission->connections[i]->destination);
		if(destination==NULL) {
			errmsg( "Destination %d not found (\"%s\")", i, mission->connections[i]->destination);
			return NULL;
		}

		source->output = ATTACH_TO_ARRAY(source->output, destination);
		destination->input = (void*)source;

	}
	cimsg(GRN, "Connections established...");
	return process_array;
}


/*
 * This function allows a SimpleProcess 'to' to inherit all the properties of 'from'.
 * If 'to' and 'from' have the same property (i.e. both have inputs), the 'to' properties
 * will prevail
 */
SimpleProcess* inherit_simple_process(SimpleProcess* to, SimpleProcess *from){
	if ( to->uid == NULL ) {
		char temp[512];

		PhysicalSystem* instrument = (PhysicalSystem*)from->physical_system;
		sprintf(temp, "%s:%s", instrument->id, to->name);
		to->uid = set_string(temp);
		imsg("Generating new UID \"%s\"", to->uid);
	}
	TRY_RET_NULL(inherit_inputs(to, from));
	TRY_RET_NULL(inherit_outputs(to, from));
	TRY_RET_NULL(inherit_parameters(to, from));

	if ( to->field_list == NULL && from->field_list != NULL ) {
		to->field_list = from->field_list;
		to->linked_fields = TRUE;
	}

	if ( to->interface == NULL && from->interface != NULL ) {
		to->interface = from->interface;
	}

	to->physical_system = from->physical_system;

	swe_free(to->typeOf); // Clear to typeOf
	to->typeOf = set_string(from->typeOf); // assign the new typeOf
	to->parent = (void*)from;
	return to;
}


/*
 * Merge the SimpleProcess from mission (AggregateProcess) with instrument (PhysicalSystem) if they match
 */
AggregateProcess* merge_instrument_with_mission(AggregateProcess* mission, PhysicalSystem* instrument){
	int i, j;
	imsg("Merging instruments with missions");

	if (mission == NULL || instrument == NULL) {
		errmsg("%s NULL pointer received", __func__);
		return NULL;
	}

	msg_sub_title(CYN, "Merging SimpleProcesses");

	// Link instrument structure with mission
	if (instrument->interface == NULL) {
		warnmsg("intstrument's interface is NULL");
		//return NULL;
	}

	// Assign the interface and the instrument id
	mission->interface = instrument->interface;
	instrument->interface = NULL;
	mission->instrument_id = set_string(instrument->id);
	mission->instrument = instrument;

	// Check for SimpleProcesses that need to be merged //
	for ( i=0 ; mission->processes[i]!=NULL ; i++ ){
		SimpleProcess *mp = mission->processes[i];
		mp->interface = mission->interface;
		if ( mp->physical_system == NULL ) {
			mp->physical_system = instrument;
		}
		if (mp->typeOf == NULL) {
			errmsg("Unknown implementation of SimpleProcess %s (typeof NULL)", mp->name);
			continue;
		}
		// Loop through the instrument's SimpleProcesses to check if they need to be merged
		for (j=0 ; instrument->processes[j]!=NULL ; j++){
			SimpleProcess *ip = instrument->processes[j];

			if (!compare_strings(mp->typeOf, ip->uid)){
				cimsg(GRN, "Merging process %s with %s", mp->name, ip->name);
				mission->processes[i]=inherit_simple_process(mp,ip); // merge processes
			}
		}
	}

	return mission;
}


/*
 * Takes two arrays and merges them into a new array. After that the input arrays pointers
 * are freed (only the pointers, not the contents). The contents are transfered to the new
 * array
 */
SchedulerProcess** merge_process_arrays(SchedulerProcess** array1, SchedulerProcess** array2) {
	int count1, count2;
	int i;
	int offset;
	int array_count;
	count1 = ARRAY_LENGTH(array1);
	count2 = ARRAY_LENGTH(array2);
	array_count = count1 + count2 + 1;
	SchedulerProcess** out = swe_malloc(sizeof(SchedulerProcess*)*array_count);
	// Copy pointers from array1 to out
	for ( i=0 ; i < count1 ; i++) {
		out[i] = array1[i];
	}

	// Copy pointers from array2
	offset = count1;
	for (i=0 ; i < count2 ; i++) {
		out[offset + i]= array2[i];
	}
	out[array_count-1] = NULL; // terminate the array with a null pointer
	swe_free(array1);
	swe_free(array2);

	return out;
}


//---------------------------------------------------------------------------//
//------------------------------ OTHERS -------------------------------------//
//---------------------------------------------------------------------------//

/*
 * Generates scheduler data from a ResponseStructure. Links the enabled_data values fields from the
 * response to the appropriate fields in SchedulerData, as well as their sizes.
 */
SchedulerData* scheduler_data_from_response_structure(ResponseStructure* resp){
	GET_RESPONSE_COMMON_PARAMETERS(resp);

	dmsg("Response length: %u", length);
	int i;
	SchedulerData* data = NULL;
	if (resp == NULL) {
		errmsg("Argument is NULL");
		return NULL;
	}
	data = swe_malloc(sizeof(SchedulerData));
	for ( i=0 ; i<count ; i++ ) {
		//dmsg("%d of %d", i+1, count);
		if ( roles[i] == enabled_data ){
			//dmsg("Enabled!");
			SWE_Data* datafield = swe_malloc(sizeof(SWE_Data));
			datafield->encoding = encodings[i];

			if ( (configuration_fields[i] != NULL)  && configuration_fields[i]->name != NULL) {
				datafield->name = set_string(configuration_fields[i]->name);
			} else {
				errmsg("Configuration Field [%d] Name not found", i);
			}

			// Link ResponseStructure to SchedulerData value
			datafield->total_size = element_sizes[i];
			datafield->element_size = get_size_by_encoding(encodings[i]);
			datafield->element_count = element_counts[i];
			datafield->value = values[i];


			// Attach to the fields array
			data->fields = ATTACH_TO_ARRAY(data->fields, datafield);
			data->count++;
		}
	}

	swe_free(configuration_fields);
	return data;
}


/*
 * Generates a data string ready to write to a O&M or CSV file from a SchedulerData structure and stores the result in buffer.
 *
 * data: SchedulerData structure
 * buffer: where the string will be stored (make sure that there's enough space!)
 * delimiter: character to be used as a delimiter (e.g. ',' in CSV, '#' in O&M, etc.)
 *
 * Flags
 * georeference: flag to add latitude and longitude
 * altitude: flag to add the z axis
 * precision: number of decimals (only applied when converting binary data to ascii data)
 * msecs: if TRUE the timestamps will contain milliseconds otherwise they will be rounded to the second
 * timerange: if TRUE the timestamp column will be "<start time>/<end time>" e.g. <2020-05-13T03:00:00Z/2020-05-13T03:00:10Z>
 *
 */
int data_string_from_scheduler_data(SchedulerData* data, char* buffer, char *delimiter, char *endline,
		uchar georeference, uchar altitude, uchar precision, uchar msecs, uchar timerange){
	int i;
	char *buff_point=buffer;
	char* buff_init = buffer;
	int len;
	char timestamp_str[64];

	TRY_RET(ascii_datetime(timestamp_str, data->init_timestamp, msecs));

	// Step 1: Timestamp
	buff_point=fast_strcat(buff_point, timestamp_str);

	if ( timerange == TRUE ){
		// If using time range, put the timestamp end, e.g. 2020-04-22T23:58:39Z/2020-04-22T23:58:49Z
		buff_point=fast_strcat(buff_point, "/");

		TRY_RET(ascii_datetime(timestamp_str, data->end_timestamp, msecs));
		buff_point=fast_strcat(buff_point, timestamp_str);
	}


	// Step 2: Add the georeference (latitude, longitude) if the georeference flag is set
	if ( georeference == TRUE) {
		float latitude, longitude;
		char temp[20];
		memset(temp, 0, 20);
		if (get_coordinates(&latitude, &longitude) == swe_ok) { // get the lat / lon from the platform
			// Add latitude
			TRY_RET(swe_encoding_to_string(temp, &latitude, float_data, precision));
			buff_point=fast_strcat(buff_point, delimiter);
			buff_point=fast_strcat(buff_point, temp);

			// Add longitude
			TRY_RET(swe_encoding_to_string(temp, &longitude, float_data, precision));
			buff_point=fast_strcat(buff_point, delimiter);
			buff_point=fast_strcat(buff_point, temp);
		}
		else {
			warnmsg("Couldn't get the GPS position, setting NaN");
			buff_point=fast_strcat(buff_point, delimiter);
			buff_point=fast_strcat(buff_point, "NaN");
			buff_point=fast_strcat(buff_point, delimiter);
			buff_point=fast_strcat(buff_point, "NaN");
		}
	}
	// Step 3: Add the altitude
	if ( altitude == TRUE) {
		float altitude;
		char temp[20];
		memset(temp, 0, 20);
		if (get_altitude(&altitude) == swe_ok){ // get the lat / lon from the platform
			TRY_RET(swe_encoding_to_string(temp, &altitude, float_data, precision));
			// Add the latitude
			buff_point=fast_strcat(buff_point, delimiter);
			buff_point=fast_strcat(buff_point, temp);
		}
		else {
			warnmsg("Couldn't get altitude, setting NaN");
			buff_point=fast_strcat(buff_point, delimiter);
			buff_point=fast_strcat(buff_point, "NaN");
		}
	}

	if (altitude || georeference ) {
		dmsg(""); // add new line
	}

	// Step 4: Add all the measures
	for(i=0; i < data->count; i++) 	{
		// ASCII Data //
		if ( check_if_binary(data->fields[i]->encoding) == FALSE ){
			if (  data->fields[i]->element_count != 1 ) {
				errmsg("Unimplemented Array in ASCII mode");
				return swe_unimplemented;
			}
			buff_point=fast_strcat(buff_point, delimiter);
			buff_point=fast_strcat(buff_point, (char*)data->fields[i]->value);
		}
		// Binary Data //
		else {
			SWE_Data* field = data->fields[i];
			char temp[512];
			int j;
			uchar* value = field->value;
			for ( j=0 ; j<field->element_count ; j++ ) {
				if (swe_encoding_to_string(temp, value, field->encoding, precision)){
					errmsg("Unimplemented data type %d", field->encoding);
					return swe_unimplemented;		// Print the process only
				}
				buff_point=fast_strcat(buff_point, delimiter);
				buff_point=fast_strcat(buff_point, temp);

				value += field->element_size; // Move to the next element
			}
		}
	}
	buff_point=fast_strcat(buff_point, endline);
	len = buff_point - buff_init;
	return len;
}


/*
 * Generates a fileaname based on folder, prefix, extension and time. The time format can be specified with the flags hour, min and secs
 * The generated result will be stored in buffer as <folder>/<prefix>_YYYY-MM-DD(_HH)(:mm)(:ss).<extension>
 * e.g.
 * folder = output
 * prefix = myfile
 * extension = csv
 * hour = TRUE
 * min = TRUE
 * sec = FALSE
 *
 * output: "folder/myfile_2020-12-31_04:12.csv"
 */
int generate_data_filename(char* buffer, char* folder, char* prefix, char* extension, float64 timestamp, uchar hour, uchar min, uchar sec){
	char datestring[512];
	char* buff;
	char temp[256];
	//---- Generate output file name ----//
	DateTime datetime;
	TRY_RET(datetime_from_timestamp(&datetime, timestamp));
	// Generate datestring
	buff = datestring;
	sprintf(temp, "%04d-%02d-%02d", datetime.year, datetime.month, datetime.day);
	buff = fast_strcat(buff, temp);
	if ( hour ) {
		sprintf(temp, "_%02d", datetime.hour);
		buff = fast_strcat(buff, temp);
		if ( min ) {
			sprintf(temp, ":%02d", datetime.minute);
			buff = fast_strcat(buff, temp);
			if ( sec ) {
				sprintf(temp, "%02d", datetime.second);
				buff = fast_strcat(buff, temp);
			}
		}
	}
	sprintf(buffer, "%s%s%s_%s.%s", folder, PATH_SEPARATOR, prefix, datestring, extension);
	return swe_ok;
}


