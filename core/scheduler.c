/*
 *  This file contains the SWE Bridge scheduler implementation
 *
 *  @author: Enoc Martínez
 *  @institution: Universitat Politècnica de Catalunya (UPC)
 *  @contact: enoc.martinez@upc.edu
 */


#include "core/scheduler.h"
#include "common/swe_utils.h"
#include "core/simple_process.h"
#include "resources/mem_control.h"
#include "resources/resources.h"
#include "decoder/decoder.h"


//---- Private Functions ----//
ulong calculate_scheduler_tick_rate(SchedulerProcess** process_array);
int scheduler_deep_sleep(Scheduler* scheduler);
void scheduler_timer_handler();
int tick_processes(int ticks);
int check_scheduled_processes(SchedulerProcess** process_array);
uchar check_tasks_done(SchedulerProcess** processes);
int scheduler_loop(Scheduler* scheduler,  uchar tasks_done);


//---------------------------------------------------------------------------//
//---------------------------- SCHEDULER INTERFACE --------------------------//
//---------------------------------------------------------------------------//

static Scheduler* global_scheduler = NULL;
static uchar scheduler_interrupt_event = FALSE; // Flag to determine if we had an interrupt


/*
 * Executes all initialize processes. If some processes are
 */
int scheduler_init(){
	int i;
	Scheduler* scheduler = global_scheduler;


	msg_sub_title(CYN, "Initializing Scheduler");
	if (scheduler == NULL) return swe_invalid_arguments;

	// Set the exec flag of all initialization processes
	for(i=0; scheduler->init_array[i]!=NULL; i++) {
		SchedulerProcess* process = scheduler->init_array[i];
		if (process->state_reg.exec_modes.initialization == TRUE) {
			process->state_reg.exec_flag |= SCHEDULER_TRIGGER;
		}
	}

	if (ARRAY_LENGTH(scheduler->init_array) == 0 ) {
		imsg("No Initialization processes found");
		return swe_ok;
	}
	scheduler->current_array = scheduler->init_array; // set the current array to init
	return scheduler_loop(scheduler, TRUE);
}


/*
 * Starts the scheduler loop in an infinite loop
 */
int scheduler_start(){
	int i, count=0;
	Scheduler* scheduler = global_scheduler;
	global_scheduler->current_array = global_scheduler->process_array;


	// Check that at least we have one process with sampling rate or interface interrupt,
	// otherwise nothing will ever be triggered
	for(i=0; scheduler->process_array[i]!=NULL; i++) {
		SchedulerProcess* process = scheduler->process_array[i];
		StateRegister* reg = &process->state_reg;
		if ( reg->sampling_rate > 0 || reg->exec_modes.iface_interrupt) {
			count++;
		}
	}
	if ( count == 0 ){
		imsg("No scheduler processes found, exit");
		return swe_ok;
	}

	msg_sub_title(CYN, "Starting Scheduler");
	return scheduler_loop(global_scheduler, FALSE);
}


/*
 * This function starts the Scheduler ticking periodically processes in current_array
 * array. The scheduler will be running until the scheduler.status is set to STOPPED or
 * all tasks are done (see "tasks_done" flag).
 *
 * If the "tasks_done" flag is set to TRUE the scheduler will stop once all the
 * tasks in the "current_array" array are executed. However, if there is any process
 * triggered periodically (SCHEDULER_TRIGGER / sampling rate is set) the scheduler
 * will keep running forever.
 *
 * If the scheduler is set to PAUSED the scheduler will keep running, but nothing
 * will be executed, it will just call standby platform (or deep sleep)
 * again and again until it is resumed
 */
int scheduler_loop(Scheduler* scheduler,  uchar tasks_done) {
	int i;
	if (scheduler == NULL){
		return swe_invalid_arguments;
	}

	// Setting all interfaces interrupts  in current process array //
	dmsg( "initializing interrupt processes...");
	for(i=0; scheduler->current_array[i]!=NULL; i++) {
		SchedulerProcess* proc = scheduler->current_array[i];
		StateRegister *reg = &proc->state_reg;
		if(proc->state_reg.exec_modes.iface_interrupt){
			if (proc->iface == NULL ) {
				errmsg("Can't set interrupt process! Communications iface is NULL");
				return swe_iface_error;
			}
			if (proc->iface->is_interrupt_set == FALSE) {
				dmsg( "setting interface interrupt for process %s...", proc->id);
				TRY_RET(iface_set_interrupt(proc->iface, &proc->state_reg.exec_flag));
			} else {
				dmsg( "interrupt already set...", proc->id);
			}

			// configure the watchdog timer timeout (if any)
			if (reg->watchdog_period > 0) {
				imsg("setting watchdog to %f seconds, (%d ticks at tickrate %d us)",
						reg->watchdog_period, reg->watchdog_target_ticks, reg->tick_rate_us);
				process_watchdog_set(reg);
			}
		}
	}

	dmsg("Initializing scheduled processes");
	for(i=0; scheduler->current_array[i]!=NULL; i++) {
		SchedulerProcess* proc = scheduler->current_array[i];
		StateRegister *reg = &proc->state_reg;
		if(proc->state_reg.exec_modes.scheduled_process){
			proc->state_reg.scheduler_start = calculate_scheduled_start_time(reg);
		}
	}



#if SIMULATE_SCHEDULER_TIMER
	// Use scheduler without timer
	warnmsg("Simulating timer with delay!");
	ulong tick_delay_ms = scheduler->tick_rate_us / 1000;
	dmsg("Tick delay is set to %u msecs", tick_delay_ms);
#elif SIMULATE_SCHEDULER_DRIFT
	// Use a timer but tweak the tick_rate to simulate a drift
	dmsgn( "Starting timer with drift...");
	TRY_RET(swe_set_timer((ulong)(0.95*(float64)scheduler->tick_rate_us) , scheduler_timer_handler));
	TRY_RET(swe_start_timer());
	cdmsg(GRN, "OK");
#else
	//Set the timer
	dmsgn( "Starting timer...");
	TRY_RET(swe_set_timer(scheduler->tick_rate_us,  scheduler_timer_handler));
	TRY_RET(swe_start_timer());
	cdmsg(GRN, "OK");
#endif /*SIMULATE_SCHEDULER_TIMER*/



	scheduler->status = SCHEDULER_RUNNING;
	//------------ Scheduler Infinite Loop ------------//
	while(scheduler->status != SCHEDULER_STOPPED ) {
		if (scheduler->status == SCHEDULER_RUNNING) {
			uchar  check_flags = TRUE;

			// Check if any flags have to be set by the trigger At Exec Modes
			check_scheduled_processes(scheduler->process_array);


			// While we have the flag set to true look for processes exec flags
			while ( check_flags ) {
				check_flags = FALSE;

				// Look for processes with exec floag set
				for(i=0; scheduler->current_array[i]!=NULL; i++) {
					SchedulerProcess* proc = scheduler->current_array[i];
					if( proc->state_reg.exec_flag ){
						// Executing Processes //
						TRY_RET(execute_process_chain(proc, NULL));
						check_flags = TRUE; // if a process has been executed maybe a retrigger has been set
					}
				}
			}
		}

		// If the tasks done flag is set, check if all the tasks in the process array have been
		// executed
		if (tasks_done) {
			if (check_tasks_done(scheduler->current_array)) {
				cimsg(GRN, "All tasks done!");
				scheduler->status = SCHEDULER_STOPPED;
				break;
			}
		}
		// power down the platform
		if ( scheduler->options.deep_sleep == FALSE ) {

#if SIMULATE_SCHEDULER_TIMER
			// If simulate timer, sleep and tick processes manually
			delayms(tick_delay_ms);
			tick_processes(1);
			scheduler_interrupt_event = TRUE;
#else
			standby_platform();
#endif
		}
		else {
			if (scheduler_deep_sleep(scheduler) < 0) {
				errmsg("Can't set scheduler to deep sleep");
			}
		}
		// Print point to show that the scheduler is alive
		if (scheduler_interrupt_event == TRUE) {
			dmsgn(".");
			scheduler_interrupt_event = FALSE;
		}
	}
	// Stop the timer once we exit the loop
#if (( SIMULATE_SCHEDULER_TIMER == FALSE ))
	TRY_RET(swe_stop_timer());
#endif
	return swe_ok;
}


/*
 * Sets all counters to 0
 */
int scheduler_reset(){
	int i;
	Scheduler* scheduler = global_scheduler;
	for( i=0 ; scheduler->process_array[i]!=NULL ; i++ ) {
		SchedulerProcess* proc=scheduler->process_array[i];
		StateRegister* sr = &proc->state_reg;
		sr->exec_flag = 0;
		sr->current_ticks = 0;
		sr->watchdog_current_ticks = 0;
		sr->internal_timeout = -1;
		sr->expecting_response = 0;
	}
	return swe_ok;
}


/*
 * Stops the scheduler timer, pausing everything. Note that Interfaces will continue
 * to receive interrupts and the exec flag set, but no one will execute them
 */
int scheduler_stop(){
	global_scheduler->status = SCHEDULER_STOPPED;
	return swe_ok;
}

/*
 * Pauses the scheduler timer, pausing everything. Note that Interfaces will continue
 * to receive interrupts and the exec flag set, but no one will execute them
 */
int scheduler_pause(){
	global_scheduler->status = SCHEDULER_PAUSED;
	return swe_ok;
}

/*
 * Stops the scheduler timer, pausing everything. Note that Interfaces will continue
 * to receive interrupts and the exec flag set, but no one will execute them
 */
int scheduler_resume(){
#if SIMULATE_SCHEDULER_TIMER
	// Use scheduler without timer
	warnmsg("Simulating timer with delay!");
	ulong tick_delay_ms = global_scheduler->tick_rate_us / 1000;
	dmsg("Tick delay is set to %u msecs", tick_delay_ms);
#else
	//Set the timer
	dmsgn( "Launching Timer...");
	TRY_RET(swe_set_timer(global_scheduler->tick_rate_us,  scheduler_timer_handler));
	TRY_RET(swe_start_timer());
	global_scheduler->status = SCHEDULER_RUNNING;
	cdmsg(GRN, "OK");
#endif
	return swe_ok;
}



//---------------------------------------------------------------------------//
//---------------------------- SETUP SCHEDULER --- --------------------------//
//---------------------------------------------------------------------------//

/*
 * Generates a scheduler from a process list
 */
Scheduler* scheduler_setup(SchedulerProcess** processes, SchedulerOptions* scheduler_options){
	int i, j;
	Scheduler* scheduler=swe_malloc(sizeof(Scheduler));
	global_scheduler=scheduler;
	// Copy scheduler options //
	memcpy(&scheduler->options, scheduler_options, sizeof(SchedulerOptions));
	if ( scheduler->options.deep_sleep == TRUE ) {
		imsg("Using platform deep sleep");
	}

	dmsg("Scheduler has %d processes", ARRAY_LENGTH(scheduler->process_array));
	scheduler->process_array=processes;
	dmsg( "calculating scheduler tick rate...");
	if((scheduler->tick_rate_us=calculate_scheduler_tick_rate(scheduler->process_array)) == 0){
		errmsg( "Couldn't set the sampling rate, exit");
		return NULL;
	}
	dmsg( "Tick rate is %u us (%.02f sec)", scheduler->tick_rate_us, ((float)scheduler->tick_rate_us)/1000000);
	dmsg( "Scheduler setup done");



	// Setup state register for scheduler processes //
	dmsg( "initializing scheduler processes...");
	for(i=0; scheduler->process_array[i]!=NULL; i++) {
		SchedulerProcess* proc = scheduler->process_array[i];
		// assign the scheduler tick rate
		proc->state_reg.tick_rate_us = scheduler->tick_rate_us;
		if(proc->state_reg.exec_modes.scheduler){
			float64 sampling_rate_seconds= scheduler->process_array[i]->state_reg.sampling_rate;
			ulong sampling_rate_us=0;
			sampling_rate_us=1000000*(sampling_rate_seconds);
			proc->state_reg.trigger_ticks=sampling_rate_us/scheduler->tick_rate_us;
		}
	}
	dmsg("Generate init array...");
	scheduler->init_array = swe_malloc(sizeof(SchedulerProcess*));
	*scheduler->init_array = NULL;
	// Generate an array containing all the process that need to be monit at
	// the init sequence (init processes and their outputs)
	for(i=0; scheduler->process_array[i]!=NULL; i++) {
		SchedulerProcess* process = scheduler->process_array[i];
		if (process->state_reg.exec_modes.initialization == TRUE) {
			// show_process_chain(process);
			SchedulerProcess** current_chain;
			current_chain = get_process_chain(process, NULL);

			for ( j=0 ; current_chain[j] != NULL; j++) {
				scheduler->init_array = ATTACH_TO_ARRAY(scheduler->init_array, current_chain[j]);
			}
		}
	}

	// Make sure that initialization processes do not have sampling_rate. This would
	// Cause an infinite startup sequence.
	dmsg("Check init sequence...");
	for(i=0; scheduler->init_array[i]!=NULL; i++) {
		SchedulerProcess* process = scheduler->init_array[i];
		if ( process->state_reg.sampling_rate > 0 ) {
			errmsg("Process in the init sequence can't have sampling rate! (proces %s)", process->name);
			return NULL;
		}
	}



	return scheduler;
}


/*
 * Tick processes is the function that keeps the scheduler alive and running. It updates
 * all the internal counters in the different processes and checks the execution conditions,
 * as well as timeouts. The processes within the process array are updated each time this
 * function is called.
 *
 * Usually it is used to increment by 1 tick each time a system_tick interrupt. However
 * under special circumstances (i.e. deep sleep) more than one tick needs can be applied.
 *
 */
int tick_processes(int ticks){
	int i;
	float64 now = swe_get_epoch_time();
	for(i=0; global_scheduler->current_array[i]!=NULL; i++){
		SchedulerProcess* proc = global_scheduler->current_array[i];
		StateRegister* reg = &proc->state_reg;


		// Check scheduler trigger //
		if (reg->exec_modes.scheduler) {
			reg->current_ticks += ticks; // Tick scheduler processes
			if( reg->trigger_ticks <= reg->current_ticks ){ // Check trigger condition
				reg->current_ticks=0;
				reg->exec_flag |= SCHEDULER_TRIGGER; // add the scheduler trigger
			}
		}

		// Check if the process has an internal timeout /
		if(reg->internal_timeout > 0) {
			if ( now > reg->internal_timeout) {
				reg->exec_flag |= INTERNAL_TIMEOUT_TRIGGER; // add the scheduler trigger
			}
		}

		// Check the Watchdog //
		if (reg->watchdog_target_ticks > 0) {
			reg->watchdog_current_ticks += ticks;
			if (reg->watchdog_target_ticks < reg->watchdog_current_ticks) {
				errmsg( "Watchdog timer %s expired (%f secs), stopping scheduler", proc->id, proc->state_reg.watchdog_period);
				global_scheduler->status = SCHEDULER_STOPPED;
			}
		}
		// Check if there has been an iface interrupt
		if (reg->interrupt_flag){
			reg->exec_flag |= IFACE_INTERRUPT_TRIGGER;
		}


		// Update delay after ticks (if active)
		if ( reg->delay_after.ticks_left  > 0 ){
			reg->delay_after.ticks_left--;
			if (reg->delay_after.ticks_left == 0) {
				reg->exec_flag |= DELAY_AFTER_TRIGGER;
			}
		}

		// Update delay before ticks (if active)
		if ( reg->delay_before.ticks_left  > 0 ){
			reg->delay_before.ticks_left--;
			if (reg->delay_before.ticks_left == 0) {
				reg->exec_flag |= DELAY_BEFORE_TRIGGER;
			}

		}
	}
	return swe_ok;
}


/*
 * Checks if any process with the scheduled flag activated has reached the scheduled time
 */
int check_scheduled_processes(SchedulerProcess** process_array) {
	int i;
	float64 now = -1;

	for(i=0; process_array[i]!=NULL; i++){
		SchedulerProcess* proc = process_array[i];
		StateRegister* reg = &proc->state_reg;
		if ( reg->exec_modes.scheduled_process ) {
			// If the time was not taken, take it
			if ( now < 0 ) {
				now = swe_get_epoch_time();
			}

			if ( reg->scheduler_start <= now ) {
				reg->exec_flag |= SCHEDULER_TRIGGER;
				reg->scheduler_start += reg->scheduler_period;
			}
		}
	}
	return swe_ok;
}


/*
 * Real-time timer handler
 */
void scheduler_timer_handler(){
	scheduler_interrupt_event = TRUE;
	tick_processes(1); // Increment 1 tick
}


/*
 * This function takes the sampling rate of each process and searches for the
 * greatest common divisor among them in order to obtain the greatest possible
 * period.
 */
ulong calculate_scheduler_tick_rate(SchedulerProcess** process_array){
	uint i;
	ulong scheduler_tick_rate = 1;
	ulong sampling_rate_us = 0;
	uchar scheduler_process = FALSE;
	if(process_array==NULL) {
		errmsg( "calculate_sampling_rate: NULL pointer received!");
		return swe_error;
	}
	scheduler_tick_rate=0;
	for (i=0; process_array[i]!=NULL; i++){
		// Loop through scheduler processes
		if(process_array[i]->state_reg.exec_modes.scheduler){
			scheduler_process = TRUE;
			float64 sampling_rate_seconds=process_array[i]->state_reg.sampling_rate;
			if(sampling_rate_seconds > 0) {
				sampling_rate_us=1000000*(sampling_rate_seconds);
				if (scheduler_tick_rate == 0) {
					scheduler_tick_rate = sampling_rate_us; // avoid empty first
				} else {
					scheduler_tick_rate = greatest_common_divisor(scheduler_tick_rate, sampling_rate_us);
				}
				dmsg("Greatest common divisor %u", scheduler_tick_rate);
			} else{
				errmsg( "setup_scheduler: process \"%s\" has scheduler exec mode but no sampling rate parameter!", process_array[i]->name);
				return 0;
			}
		}
	}

	// If there is no scheduler process use the max tick rate
	if (scheduler_process == FALSE) {
		scheduler_tick_rate= PLATFORM_MAX_TICK_RATE;
	}

	if (scheduler_tick_rate == 0) {
		errmsg("Error calculating scheduler_tick_rate");
		return 0;
	}

	// Avoid scheduler tick rate to high //
	if ( scheduler_tick_rate > PLATFORM_MAX_TICK_RATE ) {
		dmsg("Correcting scheduler_tick_rate (%u too high)", scheduler_tick_rate);
		scheduler_tick_rate = greatest_common_divisor(scheduler_tick_rate, PLATFORM_MAX_TICK_RATE);
	}

	// Avoid scheduler tick rate to small //
	if ( scheduler_tick_rate < PLATFORM_MIN_TICK_RATE ) {
		dmsg("Correcting scheduler_tick_rate (%u too low)", scheduler_tick_rate);
		scheduler_tick_rate = PLATFORM_MIN_TICK_RATE;
	}

	dmsg("usec period %u", scheduler_tick_rate);
	return scheduler_tick_rate;
}


float64 ticks_to_time(ulong ticks){
	return (float64)ticks * ((float64)(global_scheduler->tick_rate_us))/1000000; // Calculate time in seconds
}


/*
 *  This function takes the scheduler and calculates the time until the next event. If the time is
 *  greater than minimal deep sleep time set the platform to deep sleep. Otherwise use normal sleep
 *
 * 	Step   I. Loop through processes and get the time left to trigger the next event (scheduler process)
 *  Step  II. Get the time until next event
 *  Step III. Check if the time is less than min deep sleep time to avoid short deep sleep
 *  Step  IV. Call platform deep sleep wrapper
 *  Step   V. Update the current ticks
 */
int scheduler_deep_sleep(Scheduler* scheduler){
	int i=0;
	float64 scheduler_next_event = -1;
	SchedulerProcess** list = scheduler->current_array; // Process array currently in use
	ulong  ticks_to_next_event = 0xFFFFFFFF;

#if SIMULATE_SCHEDULER_TIMER
	errmsg("Cannot use deep sleep with option SIMULATE_SCHEDULER_TIMER!");
	return swe_timer_error;
#endif


	// Step   I. Loop through processes and get the ticks left to trigger for the next process //
	//           Check also for non-expired timeouts //
	for ( i=0 ; list[i]!=NULL ; i++) {
		float64 process_next_trigger = 1e20;
		if (list[i]->state_reg.exec_modes.scheduler == TRUE ) {
			// Calculate how ticks are left in the current process //
			ulong ticks_left = list[i]->state_reg.trigger_ticks - list[i]->state_reg.current_ticks;
			process_next_trigger = MIN(process_next_trigger, ticks_to_time(ticks_left));
		}
		// If there is a scheduled timeout check it. However, if the process has an interface
		// interrupt ignore this timeout (a streaming sensor would prevent deep sleep).
		if (list[i]->state_reg.internal_timeout > 0 &&
				list[i]->state_reg.exec_modes.iface_interrupt == FALSE) {

			process_next_trigger = MIN(process_next_trigger, list[i]->state_reg.internal_timeout);
		}

		if (list[i]->state_reg.exec_modes.scheduled_process ) {
			// the difference between the next target time and the current time
			process_next_trigger = MIN(process_next_trigger, list[i]->state_reg.scheduler_start - swe_get_epoch_time());


		}

		// Check if this process is expecting an interrupt (response) from a sensor
		if (list[i]->state_reg.expecting_response == TRUE) {
			// If the process is expecting a response do not go to deep sleep
			standby_platform();
			return swe_ok;
		}
		
		
		if ( process_next_trigger > 0 ){ // if we know the next execution time for the current process
			if ( scheduler_next_event < 0 ) { // if it wasn't set before, use the current process_next_trigger
				scheduler_next_event = process_next_trigger;
			}
			else if ( scheduler_next_event > process_next_trigger ) {
				scheduler_next_event = process_next_trigger;
			}
		}
	}

	if ( scheduler_next_event < 0 ) {
		errmsg("Can't go to deep sleep, time until next event unknown!");
	}


	// Step III. Check that the time is greater than platform minimum deep sleep //
	if (scheduler_next_event < scheduler->options.deep_sleep_time) {
		// Use regular sleep and exit
			standby_platform();
			return swe_ok;
	}
	dmsg("Time until next event %f s, calling deep sleep", scheduler_next_event);

	// Step  IV. Call platform deep sleep wrapper //
	TRY_RET(deep_sleep(scheduler_next_event));

	// Step V. Update the current ticks //
	tick_processes(ticks_to_next_event);
	check_scheduled_processes(global_scheduler->process_array);

	return swe_ok;
}


/*
 * Checks that all the processes in the array do no have any pending operation, i.e. timeouts
 * internal retriggers, etc.
 */
uchar check_tasks_done(SchedulerProcess** processes){
	int i;
	for (i=0 ; processes[i]!=NULL ; i++ ){
		StateRegister* state = &processes[i]->state_reg;
		if ((state->sampling_rate > 0) ||		// If the process is triggered periodically it will never be done...
			(state->exec_modes.iface_interrupt == TRUE) || // Maybe we are expecting a response
			(state->internal_timeout > 0 ) || // Or we have a timeout
			(state->delay_after.ticks_left > 0 ) || // Or we have a delay after
			(state->delay_before.ticks_left > 0 ) || // Or we have a delay before
			(state->exec_flag > 0 )) { // Or simply we have the exec flag set
			return FALSE;
		}
	}
	return TRUE;
}
