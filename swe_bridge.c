/*
 *  This file contains the SWE Bridge functions implementation
 *
 *  @author: Enoc Martínez
 *  @institution: Universitat Politècnica de Catalunya (UPC)
 *  @contact: enoc.martinez@upc.edu
 */

#include "swe_bridge.h"
#include "puck/puck.h"
#include "core/scheduler.h"
#include "decoder/decoder.h"
#include "resources/resources.h"

//---- Private Functions ----//
int init_scheduler_options(SchedulerOptions* options);
int swe_bridge_setup_scheduler(InstrumentConfig** instr_config_list, SchedulerOptions* scheduler_options, SWE_Bridge* swe_bridge);
int process_option_write_puck(SWE_Bridge_Option* option);
int process_option_set_log_level(SWE_Bridge_Option* option);
int process_option_save_log(SWE_Bridge_Option* option);
int process_option_deep_sleep(SWE_Bridge_Option* option, SchedulerOptions* schopts);
InstrumentConfig** process_option_puck_retrieve_file(SWE_Bridge_Option* option, InstrumentConfig** instrument_config_list);
InstrumentConfig** process_option_local_file(SWE_Bridge_Option* option,	InstrumentConfig** instrument_config_list);
int swe_bridge_show_help();

SWE_Bridge* swe_bridge = NULL; // global SWE_Bridge structure

/*
 * Executes all Initialize
 */
int swe_bridge_init(){
	if (swe_bridge == NULL || swe_bridge->scheduler == NULL) {
		errmsg("Scheduler setup not completed!");
		return swe_error;
	}
	swe_bridge_set_status(swe_bridge_scheduler_init, NULL);
	scheduler_init(swe_bridge->scheduler);
	return swe_ok;
}


/*
 * Starts the scheduler (infinite loop). This function will not return
 * until he scheduler is stopped.
 */
int swe_bridge_start(){
	if (swe_bridge == NULL || swe_bridge->scheduler == NULL) {
		errmsg("SWE Bridge setup not completed!");
		return swe_error;
	}
	swe_bridge_set_status(swe_bridge_scheduler_running, NULL);
	scheduler_start(); // Scheduler infinite loop
	return swe_ok;
}

/*
* Pauses the scheduler
*/
int swe_bridge_pause(){
	if (swe_bridge == NULL || swe_bridge->scheduler == NULL) {
		errmsg("SWE Bridge not initialized!");
		return swe_error;
	}

	imsg("Pausing Scheduler");
	swe_bridge_set_status(swe_bridge_scheduler_paused, NULL);
	scheduler_pause();
	return swe_ok;
}

/*
 * Resumes the scheduler from its previous state
 */
int swe_bridge_resume(){
	if (swe_bridge == NULL || swe_bridge->scheduler == NULL) {
		errmsg("SWE Bridge setup not completed!");
		return swe_error;
	}
	imsg("Resuming Scheduler Execution!");
	swe_bridge_set_status(swe_bridge_scheduler_running, NULL);
	scheduler_resume();
	return swe_ok;
}


/*
 * Resets all scheduler counters
 */
int swe_bridge_reset(){
	if (swe_bridge == NULL || swe_bridge->scheduler == NULL) {
		errmsg("SWE Bridge not initialized!");
		return swe_error;
	}
	imsg("Reset Scheduler");
	return scheduler_reset();
}


/*
 * Stops and deinitializes the SWE Bridge scheduler
 */
int swe_bridge_exit(){
	if (swe_bridge == NULL || swe_bridge->scheduler == NULL) {
		errmsg("SWE Bridge setup not completed!");
		return swe_error;
	}
	scheduler_stop();
	swe_bridge_set_status(swe_bridge_scheduler_stop, NULL);
	swe_free(swe_bridge);

	return swe_ok;
}

/*
 * Returns the SWE Bridge current status
 */
swe_bridge_status swe_bridge_get_status() {
	return swe_bridge->status;
}

/*
 * Changes the status of the SWE Bridge and sends a notification to the platform.
 * If required by the notification, the void* arg will be passed to the notify
 * function
 */
int swe_bridge_set_status(swe_bridge_status newstate, void* arg){
	if (swe_bridge == NULL ){
		return swe_invalid_arguments;
	}
	swe_bridge->status = newstate; // Update the state

	// Send a notification to the platform (if required)//
	if (newstate == system_check) {
		system_notify(SWEB_NOTIFY_SYSTEM_CHECK, arg);
	}
	else if (newstate == writing_puck_memory)  {
		system_notify(SWEB_NOTIFY_PUCK_WRITE, arg);
	}
	else if (newstate == extracting_puck_memory) {
		system_notify(SWEB_NOTIFY_PUCK_EXTRACT, arg);
	}
	else if (newstate == swe_bridge_scheduler_setup) {
		system_notify(SWEB_NOTIFY_SCHEDULER_SETUP, arg);
	}
	else if (newstate == swe_bridge_scheduler_init) {
		system_notify(SWEB_NOTIFY_SCHEDULER_INIT, arg);
	}
	else if (newstate == swe_bridge_scheduler_running) {
		system_notify(SWEB_NOTIFY_SCHEDULER_START, arg);
	}
	else if (newstate == swe_bridge_scheduler_paused) {
		system_notify(SWEB_NOTIFY_SCHEDULER_PAUSE, arg);
	}
	else if (newstate == swe_bridge_scheduler_stop) {
		system_notify(SWEB_NOTIFY_SCHEDULER_STOP, arg);
	}
	else if (newstate == swe_bridge_deep_sleep) {
		system_notify(SWEB_NOTIFY_ENTERING_DEEP_SLEEP, arg);
	}

	return swe_ok;
}


/*
 * SWE Bridge Launcher function. Checks the arguments and performs the appropriate
 * actions to retreive a set of SensorML files and setups the SWE Bridge's scheduler.
 *
 * The SWE_Bridge structure will contain the resulting SWE Bridge
 */
int swe_bridge_setup(int argc, char** argv){
	// SWE_Bridge* bridge;
	SWE_Bridge_Option **options;
	InstrumentConfig** instrument_config_list = NULL;
	SchedulerOptions scheduler_options;
	int i;
	char** platform_option_list = NULL;

	init_memory_control();

	if (swe_bridge != NULL) {
		errmsg("SWE Bridge already initialized!");
		return swe_invalid_arguments;
	}
	swe_bridge = swe_malloc(sizeof(SWE_Bridge));

	msg_title(CYN, "SWE Bridge");

	// Initializing options //
	TRY_RET(init_scheduler_options(&scheduler_options));


	dmsg("parsing arguments...");

	options = parse_options(argc, argv);
	if (options == NULL) {
		errmsg("invalid option");
		dmsg("for more info use --help");
		return swe_invalid_arguments;
	}

	//---- Process Platform Options ----//

	for ( i=0; options[i] != NULL; i++ ){
		if  (options[i]->type == platform_options) {
			platform_option_list = options[i]->values;
		}
	}

	// initialize the platform
	TRY_RET(init_platform(ARRAY_LENGTH(platform_option_list),platform_option_list));

	//---- Process Options ----//
	for ( i=0; options[i] != NULL; i++ ){
		// Show Help and exit //
		if (options[i]->type == show_help_option) {
			TRY_RET(swe_bridge_show_help());
			exit_platform();

		}
		// Check system compatibility //
		else if (options[i]->type == system_check_option) {
			TRY_RET(system_compatibility_check());
			exit_platform();
		}
		// Set Log Level  //
		else if (options[i]->type == set_log_level_option) {
			TRY_RET(process_option_set_log_level(options[i]));
		}
		// Set Save Log  //
		else if (options[i]->type == save_log_option) {
			TRY_RET(process_option_save_log(options[i]));
		}

		// Deep Sleep Option //
		else if (options[i]->type == deep_sleep_option) {
			TRY_RET(process_option_deep_sleep(options[i], &scheduler_options));
		}
		// Write PUCK memory //
		else if (options[i]->type == write_puck_option) {
			TRY_RET(process_option_write_puck(options[i]));
			return swe_ok;
		}
		// Use local SensorML file //
		else if (options[i]->type == local_file_option) {
			instrument_config_list = TRY_NULL(process_option_local_file(options[i],instrument_config_list));
		}
		// Retrive SensorML file from PUCK memory //
		if (options[i]->type == puck_option) {
			instrument_config_list = TRY_NULL(process_option_puck_retrieve_file(options[i], instrument_config_list));
		}
	}
	// With the list of SensorML files setup a the SWE Bridge //
	TRY_RET(swe_bridge_setup_scheduler(instrument_config_list, &scheduler_options, swe_bridge));

	cimsg( GRN, "swe_bridge_setup ended successfully");

	return swe_ok;
}

/*
 * Initializes all scheduler options
 */
int init_scheduler_options(SchedulerOptions* options) {
	memset(options, 0, sizeof(SchedulerOptions));
	options->deep_sleep = FALSE;
	options->deep_sleep_time = -1.0;
	return swe_ok;
}



//---------------------------------------------------------------------------//
//----------------------- SWE BRIDGE OPTIONS HANDLERS -----------------------//
//---------------------------------------------------------------------------//


/*
 * Process the Write Puck Option
 */
int process_option_write_puck(SWE_Bridge_Option* option) {
	int write_puck_args = ARRAY_LENGTH(option->values);
	char* puckfile;
	char* device;
	int baudrate = PUCK_DEFAULT_BAUDRATE;
	const char* tag = DESCRIPTOR_TYPE_SENSORML;
	uchar sw_flow_control = FALSE;
	Interface* iface;
	if (write_puck_args < 3 || write_puck_args > 5) {
		errmsg("wrong arguments in puck write");
		return swe_error;
	}
	puckfile = option->values[0];
	device = option->values[1];
	baudrate = atoi(option->values[2]);
	if (write_puck_args > 3) {
		tag = option->values[3];
	}
	if (write_puck_args > 4) {
		sw_flow_control = get_true_false(option->values[4], NULL);
	}
	msg_sub_title(MAG, "Writing PUCK memory");
	dmsg("File: %s", puckfile);
	dmsg("Device: %s", device);
	dmsg("Baudrate: %d", baudrate);
	dmsg("Tag: %s", tag);
	dmsg("Software flow control: %d", sw_flow_control);
	iface = setup_uart_iface(device, baudrate, sw_flow_control);
	TRY_RET(iface_open(iface));
	TRY_RET(puck_write_file_to_memory(iface, puckfile, tag));
	return swe_ok;
}

/*
 * Processes the set level option
 */
int process_option_set_log_level(SWE_Bridge_Option* option) {
	if (option->values == NULL ||
			ARRAY_LENGTH(option->values) != 1) {
		warnmsg("expected one argument after option %s", SET_LOG_LEVEL_OPTION);
	} else{
		set_log_level(atoi(option->values[0]));
	}
	return swe_ok;
}


/*
 * Processes the set save log option
 */
extern File_System filesystem;
extern SWE_Timer swe_timer;
int process_option_save_log(SWE_Bridge_Option* option) {
	if (option->values == NULL || ARRAY_LENGTH(option->values) != 2) {
		warnmsg("expected two argument after option %s", SAVE_LOG_OPTION);
		return swe_invalid_arguments;
	}

	// Check if we have the required wrappers //
	if ( filesystem.create_dir == NULL ) {
		errmsg("Cannot save log, platform_create_dir wrapper missing");
		return swe_unimplemented;
	}
	else if ( swe_timer.get_epoch_time == NULL ) {
		errmsg("Cannot save log, platform_get_time wrapper missing");
		return swe_unimplemented;
	}

	swe_bridge->log_folder = option->values[0];
	swe_bridge->log_prefix = option->values[1];
	swe_bridge->save_log = TRUE;


	// If last char is not path separator concatenate it ../mylogs ==> ../mylogs/
	if (strcmp(&swe_bridge->log_folder[strlen(swe_bridge->log_folder) -1 ], PATH_SEPARATOR )){
		char* oldname = swe_bridge->log_folder;
		swe_bridge->log_folder = swe_malloc(strlen(oldname) + 2);
		sprintf(swe_bridge->log_folder, "%s%s", oldname, PATH_SEPARATOR);
	}
	TRY_RET(swe_create_dir(swe_bridge->log_folder));
	system_printf("Switching to log folder %s with prefix %s\n", swe_bridge->log_folder, swe_bridge->log_prefix);
	return swe_ok;
}


/*
 * Processes the deep sleep option
 */
int process_option_deep_sleep(SWE_Bridge_Option* option, SchedulerOptions* schopts) {
	if (option->values == NULL || ARRAY_LENGTH(option->values) != 1) {
		errmsg("expected one argument after option %s", SET_LOG_LEVEL_OPTION);
		return swe_invalid_arguments;
	} else{
		schopts->deep_sleep = TRUE;
		schopts->deep_sleep_time = (float)atof(option->values[0]);
		if (schopts->deep_sleep_time <= 0) {
			errmsg("Deep sleep could not be set");
			return swe_invalid_arguments;
		}
	}
	return swe_ok;
}



/*
 * Retrieve a SensorML file using PUCK
 */
InstrumentConfig** process_option_puck_retrieve_file(SWE_Bridge_Option* option, InstrumentConfig** instrument_config_list) {
	char filename[256];
	char* device = option->values[0];
	int baudrate, nargs;
	Interface* iface;
	nargs = ARRAY_LENGTH(option->values);
	if (option->values == NULL || ( nargs != 2 ) ) {
		errmsg("Invalid arguments in %s option, expected 2 %d",USE_PUCK_OPTION, nargs);
		return swe_ok;
	}
	baudrate = atoi(option->values[1]);
	dmsg("Trying to open iface %s,and baudrate %d", device, baudrate);
	iface = setup_uart_iface(device, baudrate, FALSE);

	TRY_RET_NULL(iface_open(iface));

	// Trying to get the SensorML file //
	TRY_RET_NULL(puck_retrieve_sml_file(iface, filename));

	InstrumentConfig* instr_config = swe_malloc(sizeof(InstrumentConfig));
	instr_config->SensorML = set_string(filename);
	instr_config->iface = iface; // Interface hasn't been configured,
	instrument_config_list = ATTACH_TO_ARRAY(instrument_config_list, instr_config);
	cdmsg(GRN, "File %s retrieved successfully", filename);
	return instrument_config_list;
}


/*
 * Attach the filename to the config list
 */
InstrumentConfig** process_option_local_file(SWE_Bridge_Option* option,	InstrumentConfig** instrument_config_list) {
	int nargs = ARRAY_LENGTH(option->values);

	if (option->values == NULL ||  nargs != 1 ) {
		errmsg("Invalid arguments in %s , expected 1 but have %d", USE_LOCAL_FILE_OPTION, nargs);
		return instrument_config_list;
	} else {
		imsg("Using SensorML file %s", option->values[0]);
		InstrumentConfig* instr_config = swe_malloc(sizeof(InstrumentConfig));
		instr_config->SensorML  = set_string(option->values[0]); // first option is filename
		instr_config->iface = NULL; // Interface hasn't been configured,
		instrument_config_list = ATTACH_TO_ARRAY(instrument_config_list, instr_config);
	}


	return instrument_config_list;
}




/*
 * This function configures the swe_bridge structure, which contains all
 * the processes and handlers needed to operate. It takes the name of a
 * exi file as argument, decodes it, fills the Instrument_conf and the
 * Bridge_conf structures with the information contained in the file.
 *
 * Afterwards this information is used to generate the swe_bridge structure
 * which has a set of processes that can be executed.
 */
int swe_bridge_setup_scheduler(InstrumentConfig** instr_config_list, SchedulerOptions* scheduler_options, SWE_Bridge* swe_bridge){
	int  i;
	init_memory_control();
	PhysicalSystem** instruments = NULL;
	AggregateProcess** missions = NULL;


	if (instr_config_list == NULL || scheduler_options == NULL || swe_bridge == NULL ) {
		return swe_invalid_arguments;
	}

	// Loop through the files
	for ( i=0; instr_config_list[i] != NULL ; i++) {
		DecoderData* decoder_data = swe_malloc(sizeof(DecoderData)); // allocate space for the decoder struct
		TRY_RET(decode_files(instr_config_list[i]->SensorML, decoder_data));
		imsg("File %s decoded", instr_config_list[i]->SensorML);
		instruments = ATTACH_TO_ARRAY(instruments, decoder_data->instrument);
		missions = ATTACH_TO_ARRAY(missions, decoder_data->mission);
	}


	for (i=0; instruments[i]!=NULL; i++) {
		imsg("\nShowing instrument %d\n====================\n", i);
		print_physical_system(instruments[i]);
		TRY_RET(link_referenced_fields(instruments[i]));
		imsg("Instrument %p", instruments[i]);
		print_instrument_with_mission(instruments[i], missions[i]);
	}

	// Setup Interface for each instrument //
	for (i=0; instruments[i] != NULL ; i++) {
		Interface* iface;
		set_memory_table(MEM_IFACE);
		mem_status();

		// check if the interface needs to be configured or it has been previously configured by the PUCK protocol
		if (instr_config_list[i]->iface != NULL ){
			iface = instr_config_list[i]->iface;
			imsg("Interface already configured (type %s)", print_interface_type(iface->type));
			instruments[i]->interface = (void*)instr_config_list[i]->iface;
		}
		else if (instruments[i]->interface_conf != NULL){
			iface = TRY_NULL(setup_interface(instruments[i]->interface_conf));
			instruments[i]->interface = (void*)iface;
			cimsg(GRN,"Successfully configured interface for instrument %s", instruments[i]->id);
		}
		else {
			imsg("No interface info has been found");
		}
	}
	set_memory_table(MEM_SCHEDULER); // set the memory table (only active if MEMORY_CONTROL is active)

	for ( i=0 ; instruments[i]!=NULL ; i++){
		missions[i] = merge_instrument_with_mission(missions[i], instruments[i]);
	}

	imsg("Creating process list...");
	// missions and instruments are freed within generate scheduler process list
	swe_bridge->process_list = generate_shceduler_process_list(missions);
	if (swe_bridge->process_list == NULL){
		errmsg("Generate process list returned a NULL pointer!");
		return swe_invalid_arguments;
	}
	swe_free(instruments);
	swe_free(missions);

	imsg("Showing process list");
	for (i=0 ; swe_bridge->process_list[i]!=NULL; i++) {
		dmsg("process %d, id %s", i+1, swe_bridge->process_list[i]->id);
	}
	msg_sub_title( CYN, "Setup Scheduler");
	swe_bridge->scheduler = scheduler_setup(swe_bridge->process_list, scheduler_options);
	if ( swe_bridge->scheduler == NULL ) {
		return swe_scheduler_error;
	}
	mem_status();
	return swe_ok;
}






//---------------------------------------------------------------------------//
//---------------------------- SWE BRIDGE HELP ------------------------------//
//---------------------------------------------------------------------------//
/*
 * Macro used to print spaces to align options in show_help
 */
#define PRINT_SPACES(_width, _already_printed, _buff) do { \
	int _nspaces = _width - _already_printed; \
	memset(_buff, ' ', COLUMN_WIDTH);\
	if (_nspaces < 0 ) { \
		dmsgn("\n%s", _buff); \
	} else {\
		_buff[_nspaces] =0; \
		dmsgn("%s", _buff); \
	} \
 \
}while(0)

#define COLUMN_WIDTH 18


/*
 * Shows help
 */
int swe_bridge_show_help(){
	char spaces[COLUMN_WIDTH+1];
	char static_spaces[COLUMN_WIDTH+1];
	int ret = 0;

	spaces[COLUMN_WIDTH] = 0;
	memset(static_spaces, ' ', COLUMN_WIDTH);\
	static_spaces[COLUMN_WIDTH] = 0;

	msg_sub_title(BLU, "SWE Bridge");

	cimsgn( NRM,"version: ");cimsg( GRN,"%s", (char*)SOFT_VERSION);
	cimsgn( NRM,"Compilation date: "); cimsg( GRN,"%s", (char*)COMPILE_DATE);

	imsg("\nMain Options ");
	imsg(  "------------");

	ret = cimsgn( BLU, " %s <file>", USE_LOCAL_FILE_OPTION);
	PRINT_SPACES(COLUMN_WIDTH, ret, spaces);
	imsg(" : Use a local SensorML configuration file (in XML or EXI format)");

	ret = cimsgn( BLU, " %s <port> <baudarate>", USE_PUCK_OPTION);
	PRINT_SPACES(COLUMN_WIDTH, ret, spaces);
	imsg(" : Search for a PUCK-enabled instrument in the specified port and\n"
	"%s   retrieve its SensorML file", static_spaces);


	ret = cimsgn( BLU, " %s <file> <port> <baudrate> (tag) (sw flow control)", WRITE_PUCK_MEM_OPTION);
	PRINT_SPACES(COLUMN_WIDTH, ret, spaces);
	imsg(" : Write a new SensorML file into a PUCK memory. If tag is not set,\n"
         "%s   \"SWE-SensorML\" will be used. Software flow control is FALSE\n"
		 "%s   by default.", static_spaces, static_spaces);

	imsg("\nOther Options ");
	imsg(  "-------------");
	ret = cimsgn( BLU, " %s (...)", PLATFORM_OPTIONS);
	PRINT_SPACES(COLUMN_WIDTH, ret, spaces);
	imsg(" : Platform specific options (variable number of arguments)");

	ret = cimsgn( BLU, " %s <level>", SET_LOG_LEVEL_OPTION);
	PRINT_SPACES(COLUMN_WIDTH, ret, spaces);
	imsg(" : Sets the log level, 1 (debug), 2 (info), 3 (warning) or\n"
		 "%s   4 (errors)", static_spaces);

	ret = cimsgn( BLU, " %s <folder> <prefix>", SAVE_LOG_OPTION);
	PRINT_SPACES(COLUMN_WIDTH, ret, spaces);
	imsg(" : Stores the log in the specified folder. Log files have the \n"
		 "%s   specified prefix", static_spaces);


	ret = cimsgn( BLU, " %s <file>", USE_CONFIG_FILE);
	PRINT_SPACES(COLUMN_WIDTH, ret, spaces);
	imsg(" : Instead of using in-line arguments, use a configuration file, \n"
		 "%s   other command-line arguments will be ignored", static_spaces);

	ret = cimsgn( BLU, " %s <time>", DEEP_SLEEP_OPTION);
	//PRINT_SPACES(COLUMN_WIDTH, ret, spaces);
	imsg(": When the platform is idle for more than \"time\" seconds it goes\n"
		 "%s   to deep sleep rather than using regular standby.", static_spaces);


	ret = cimsgn( BLU, " %s", SHOW_HELP_OPTION);
	PRINT_SPACES(COLUMN_WIDTH, ret, spaces);
	imsg(" : Shows this help and exits\n");

	imsg("(arguments between parentheses () are optional, while arguments between guillemets\n"
			"< > are mandatory)\n\n");

	return swe_ok;
}


