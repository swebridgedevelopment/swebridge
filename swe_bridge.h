/*
 * This file contains the SWE Bridge structure and functions
 *
 *  @author: Enoc Martínez
 *  @institution: Universitat Politècnica de Catalunya (UPC)
 *  @contact: enoc.martinez@upc.edu
 */
#ifndef SWE_BRIDGE_H_
#define SWE_BRIDGE_H_

#include "swe_conf.h"
#include "core/scheduler.h"
#include "core/simple_process.h"



/*
 * SWE Bridge status
 */
typedef enum {
	swe_bridge_inital_state = 0, // Initial state
	system_check,            	 // Performing a system check
	writing_puck_memory,    	 // Writing a SensorML file to a PUCK memory
	extracting_puck_memory,  	 // Extracting a SensorML file from a PUCK memory
	decoding_sensorml_file, 	 // Decoding & Parsing a SensorML file
	swe_bridge_scheduler_setup,  // Setting up the scheduler
	swe_bridge_scheduler_init,   // Executing scheduler init processes
	swe_bridge_scheduler_running,// Scheduler is running
	swe_bridge_scheduler_paused, // Scheduler is paused
	swe_bridge_scheduler_stop,   // Scheduler is stopped
	swe_bridge_deep_sleep,   // Scheduler is stopped
}swe_bridge_status;

/*
 * SWE Bridge Structure
 */
typedef struct {
	SchedulerProcess** process_list;
	Scheduler* scheduler;
	swe_bridge_status status;

	// Log info //
	int log_level;  // Current level of logs
	uchar save_log; // Flag to determine if logs are saved or not
	char* log_folder; // Folder where the logs will be stored (if save_log == TRUE)
	char* log_prefix; // Prefix of the log files


}SWE_Bridge;

/*
 * SWE Bridge functions
 */
int swe_bridge_setup(int argc, char** argv);
int swe_bridge_init();
int swe_bridge_start();
int swe_bridge_reset();
int swe_bridge_pause();
int swe_bridge_resume();
int swe_bridge_exit();

int swe_bridge_set_status(swe_bridge_status newstate, void* arg);
swe_bridge_status swe_bridge_get_status();


#endif /* SWE_BRIDGE_H_ */
