/*
 * SWE Bridge logging system implementation
 *
 *  @author: Enoc Martínez
 *  @institution: Universitat Politècnica de Catalunya (UPC)
 *  @contact: enoc.martinez@upc.edu
 */


#include "resources/logging.h"
#include "swe_bridge.h"
#include "swe_conf.h"

extern SWE_Bridge* swe_bridge; // SWE Bridge structure


/*
 * Sets the log level
 */
int set_log_level(int new_log_level){
	if ( new_log_level > 4 || new_log_level < 1) {
		new_log_level=0; // set to debug by default
		warnmsg("Log level not recognized, valid levels are:\n"
			 "    1 : Debug\n"
			 "    2 : Info\n"
			 "    3 : Warnings & Errors\n"
			 "    4 : Errors only");
		dmsg("Setting default log level : Debug");
	}
	else {
		swe_bridge->log_level = new_log_level;
	}
	return swe_ok;
}

/*
 * Changes the log colour, only active if the PRINT_WITH_COLOUR option is set
 */
int set_log_colour(colour_list colour){
#if PRINT_WITH_COLOUR
	switch(colour){
		case RED:
			system_printf(KRED);
			break;
		case GRN:
			system_printf(KGRN);
			break;
		case YEL:
			system_printf(KYEL);
			break;
		case BLU:
			system_printf(KBLU);
			break;
		case MAG:
			system_printf(KMAG);
			break;
		case CYN:
			system_printf(KCYN);
			break;
		case WHT:
			system_printf(KWHT);
			break;
		case NRM:
			system_printf(KNRM);
			break;
		default:  //nrm
			system_printf(KRST);
			break;
	}
#endif
	return swe_ok;
}



#define GENERIC_MESSAGE_MACRO(_msg_level, _colour,  _endline, _prefix, _format) ({ \
	int _temp_ret = 0; \
	if (_msg_level < swe_bridge->log_level) { return swe_ok; } \
	if (_colour != NRM) set_log_colour(_colour); \
	va_list		__ap;  \
	va_start(__ap, _format); \
	if (_prefix != NULL ) _temp_ret += system_printf(_prefix); \
	_temp_ret += system_vprintf(_format, __ap); \
	va_end(__ap); \
	if ( _endline == TRUE ) _temp_ret += system_printf("\n"); \
	if (_colour != NRM) set_log_colour(NRM); \
	FFLUSH(); \
	_temp_ret; \
})


/*
 * flag to determine if timestamp has to be printed to logfile. Updated dynamically according to
 * new lines printed
 */
static uchar print_timestamp = TRUE; //

/*
 * This functions implements the basic message. If save log is active instead of printing
 * the message it is stored into a log file
 *
 * params
 * _msg_level: If msg level is lower than the current log level, the message is ignored
 * _colour:  If color is different from DEFAULT_COLOUR, the log changes its colour
 * endline:  If endline is TRUE a new line is added
 * prefix: prefix adds some string at the begginning (e.g. "ERROR :")
 * file: if not NULL the file name will be printed (useful to debug errors and warnings)
 * line: if not NULL the line number will be printed (useful to debug errors and warnings)
 * format is the formatting string to pass to printf
 * ap: variable argument list
 *
 */
int generic_message(int msg_level, uchar colour, uchar endline, char* prefix,
		const char* file, int line,  const char*format, va_list arglist) {
	int temp_ret = 0;
	if ( swe_bridge == NULL || msg_level < swe_bridge->log_level) {
		return swe_ok;
	}
	/*
	 * If save log is active, instead of printing it to the screen store it to a
	 * log file
	 */
	if ( swe_bridge->save_log == TRUE ) {

		char logfilename[256];
		swe_file* logfile = NULL;
		DateTime time;
		TRY_RET(swe_get_datetime(&time));
		sprintf(logfilename, "%s%s_%04d-%02d-%02d_%02d:00.log",swe_bridge->log_folder, swe_bridge->log_prefix,
				time.year, time.month, time.day, time.hour);

		logfile = TRY_NULL(swe_fopen(logfilename, swe_append_file));

		if ( print_timestamp ) {
			char timestamp[50];
			sprintf(timestamp, "%02d:%02d:%02d.%03d - ", time.hour, time.minute, time.second, time.useconds/1000);
			TRY_RET(swe_fputs(timestamp, logfile));
		}

		if ( prefix != NULL ) {
			TRY_RET(system_fprintf(logfile, prefix));
		}

		TRY_RET(system_vfprintf(logfile, format, arglist));
		if ( endline == TRUE ){
			temp_ret += swe_fputs("\n", logfile);
			print_timestamp = TRUE;
		} else {
			print_timestamp = FALSE;
		}

		// If filename has been passed, print it
		if ( file != NULL ) {
			TRY_RET(system_fprintf(logfile, " [file %s, line %d]\n", file, line));
			print_timestamp = TRUE;
		}

		TRY_RET(swe_fclose(logfile));
	}


	/*
	 * If save log is not active, just print it on the screen
	 */
	else {
		if (colour != NRM) set_log_colour(colour);
		if (prefix != NULL ) temp_ret += system_printf(prefix);
		temp_ret += system_vprintf(format, arglist);

		if (colour != NRM)  {
			set_log_colour(NRM);
		}
		if ( endline == TRUE ){
			temp_ret += system_printf("\n");
		}
		if ( file != NULL ) {
			temp_ret += system_printf(" [file %s, line %d]\n", file, line);
		}
		system_fflush();
	}
	return temp_ret;
}



//-------- DEBUG --------//

/* Debug message with <NL> at the end*/
int dmsg(const char *format, ...){
	int ret;
	va_list	__ap;  \
	va_start(__ap, format); \
	ret = generic_message(LOG_DEBUG, DEFAULT_COLOUR, TRUE, NULL, NULL, 0, format, __ap);
	va_end(__ap); \
	return ret;
}

/* Debug message without <NL> */
int dmsgn(const char *format, ...){
	int ret;
	va_list	__ap;
	va_start(__ap, format);
	ret = generic_message(LOG_DEBUG, DEFAULT_COLOUR, FALSE, NULL, NULL, 0, format,__ap);
	va_end(__ap); \
	return ret;
}

/* Debug message with colour and <NL> at the end */
int cdmsg(uchar colour, const char *format, ...){
	int ret;
	va_list	__ap;
	va_start(__ap, format);
	ret = generic_message(LOG_DEBUG, colour, TRUE, NULL, NULL, 0, format,__ap);
	va_end(__ap);
	return ret;
}

/* Debug message with colour without <NL> */
int cdmsgn(uchar colour, const char *format, ...){
	int ret;
	va_list	__ap;
	va_start(__ap, format); \
	ret = generic_message(LOG_DEBUG, colour, FALSE, NULL,  NULL, 0, format,__ap);
	va_end(__ap);
	return ret;
}


//-------- INFO --------//

/* Debug message with <NL> at the end*/
int imsg(const char *format, ...){
	int ret;
	va_list	__ap;
	va_start(__ap, format);
	ret = generic_message(LOG_INFO, DEFAULT_COLOUR, TRUE, NULL, NULL, 0, format, __ap);
	va_end(__ap);
	return ret;
}

/* Debug message without <NL> */
int imsgn(const char *format, ...){
	int ret;
	va_list	__ap;
	va_start(__ap, format);
	ret = generic_message(LOG_INFO, DEFAULT_COLOUR, FALSE, NULL, NULL, 0, format,__ap);
	va_end(__ap);
	return ret;
}

/* Debug message with colour and <NL> at the end */
int cimsg(uchar colour, const char *format, ...){
	int ret;
	va_list	__ap;
	va_start(__ap, format);
	ret = generic_message(LOG_INFO, colour, TRUE, NULL, NULL, 0,format,__ap);
	va_end(__ap);
	return ret;
}

/* Debug message with selectable colour without <NL> */
int cimsgn(uchar colour, const char *format, ...){
	int ret;
	va_list	__ap;
	va_start(__ap, format);
	ret = generic_message(LOG_INFO, colour, FALSE, NULL,NULL, 0, format, __ap);
	va_end(__ap);
	return ret;
}

/* Warning message */
int _warnmsg(const char* file, int line, const char *format, ...){
	va_list	__ap;
	va_start(__ap, format);
	int ret = generic_message(LOG_WARN, YEL, FALSE, "WARNING - ", file, line,format, __ap);
	va_end(__ap);
	return ret;
}

/* error message */
int _errmsg(const char* file, int line, const char *format, ...){
	va_list	__ap;
	va_start(__ap, format);
	int ret = generic_message(LOG_ERROR, RED, FALSE, "ERROR - ", file, line, format, __ap);
	va_end(__ap);
	return ret;
}


/*
 * This macro generates a string called formatted_string which is similar to
 * the result of a sprintf function, but with human readable strings
 * i.e. "hello\n" => "hello\\n"
 * 'h' 'e' 'l' 'l' 'o' '\n' => 'h' 'e' 'l' 'l' 'o' '\' 'n'
 */
#define FORMATTED_PRINT \
    va_list args; \
    va_start(args, format); \
    char formatted_string[512]; \
    memset(formatted_string, 0, 512); \
    char* buff = formatted_string; \
    while (*format != '\0') { \
    	char aux_buff[256]; \
    	if (format[0] == '%') { \
    		format++; \
    		if (*format == 'd' || *format == 'u') { \
				int i = va_arg(args, int); \
				sprintf(aux_buff,"%d", i); \
			} else if (*format == 'c') { \
				int c = va_arg(args, int); \
				sprintf(aux_buff, "%c", c); \
			} else if (*format == 'f') { \
				double d = va_arg(args, double); \
				sprintf(aux_buff,"%f", d); \
			} else if (*format == 's'){ \
				char* s = va_arg(args, char*); \
				format_string(s, aux_buff, 1024); \
			} \
    	 	memcpy(buff, (char*)aux_buff, strlen(aux_buff)); \
    	    buff+=strlen(aux_buff); \
    	} \
    	else { \
    		buff[0] = *format; \
    		buff+=1; \
    	} \
        format++; \
    } \
    va_end(args);



int fdmsg(const char* format, ...){
	if ( LOG_DEBUG < swe_bridge->log_level ) {
		return swe_ok;
	}
    va_list args;
    va_start(args, format);
    char formatted_string[512];
    memset(formatted_string, 0, 512);
    char* buff = formatted_string;
    while (*format != '\0') {
    	char aux_buff[256];
    	if (format[0] == '%') {
    		format++;
    		if (*format == 'd' || *format == 'u') {
				int i = va_arg(args, int);
				sprintf(aux_buff,"%d", i);
			} else if (*format == 'c') {
				int c = va_arg(args, int);
				sprintf(aux_buff, "%c", c);
			} else if (*format == 'f') {
				double d = va_arg(args, double);
				sprintf(aux_buff,"%f", d);
			} else if (*format == 's'){
				char* s = va_arg(args, char*);
				format_string(s, aux_buff, 1024);
			} \
    	 	memcpy(buff, (char*)aux_buff, strlen(aux_buff));
    	    buff+=strlen(aux_buff);
    	} \
    	else {
    		buff[0] = *format;
    		buff+=1;
    	} \
        format++;
    } \
    va_end(args);
	dmsgn("%s\n", formatted_string);
	return swe_ok;
}


/*
 * Formatted debug message : Prints debug messages but converting special chars
 * such as CR or NW into strings to print them. Useful to easily inspect the
 * content of buffers.
 */
int fmt_dmsg(uchar color, const char* format, ...){
	if ( LOG_DEBUG < swe_bridge->log_level ) {
		return swe_ok;
	}
    set_log_colour(color);

    va_list args;
	va_start(args, format);
	char formatted_string[512];
	memset(formatted_string, 0, 512);
	char* buff = formatted_string;
	while (*format != '\0') {
		char aux_buff[256];
		if (format[0] == '%') {
			format++;
			if (*format == 'd' || *format == 'u') {
				int i = va_arg(args, int);
				sprintf(aux_buff,"%d", i);
			} else if (*format == 'c') {
				int c = va_arg(args, int);
				sprintf(aux_buff, "%c", c);
			} else if (*format == 'f') {
				double d = va_arg(args, double);
				sprintf(aux_buff,"%f", d);
			} else if (*format == 's'){
				char* s = va_arg(args, char*);
				format_string(s, aux_buff, 256);
			} \
			memcpy(buff, (char*)aux_buff, strlen(aux_buff));
			buff+=strlen(aux_buff);
		} \
		else {
			buff[0] = *format;
			buff+=1;
		} \
		format++;
	} \
	va_end(args);



	dmsgn("%s", formatted_string);
	set_log_colour(NRM);
	return swe_ok;

}

// Colored Formatted Info Message no endline //
int cfimsgn(uchar color, const char* format, ...){
	if ( LOG_INFO < swe_bridge->log_level ) {
		return swe_ok;
	}
    set_log_colour(color);
	FORMATTED_PRINT
	imsgn("%s", formatted_string);
	set_log_colour(NRM);
	return swe_ok;
}


#define CONSOLE_MSG_TITLE_MAX_LENGTH 200
#define TITLE_HYPHENS_LENGTH 60
#define TITLE_HYPHENS "============================================================"
/*
 * Creates a title to in the console
 */
int msg_title(uchar colour, char* format, ...){
	char title[CONSOLE_MSG_TITLE_MAX_LENGTH];
	char spaces[CONSOLE_MSG_TITLE_MAX_LENGTH];
	va_list		ap;
	if ( LOG_INFO < swe_bridge->log_level ) {
		return swe_ok;
	}
	va_start(ap, format);
	memset(title, 0, CONSOLE_MSG_TITLE_MAX_LENGTH);
	memset(spaces, 0, CONSOLE_MSG_TITLE_MAX_LENGTH);

	va_list		__ap;
	va_start(__ap, format);
	vsprintf(title,format, __ap);
	va_end(__ap);

	/* Center the title */
	memset(spaces, ' ', (TITLE_HYPHENS_LENGTH-strlen(title))/2);
	set_log_colour(colour); //set the proper colour
	imsg("\n%s\n%s%s\n%s",TITLE_HYPHENS, spaces, title, TITLE_HYPHENS);
	set_log_colour(NRM); //set the proper colour

	return swe_ok;
}

/*
 * Creates a smaller title
 */
int msg_sub_title(uchar colour, char* format, ...){
	char title[400];
	int rtn=0;
    va_list		ap;
	if ( LOG_INFO < swe_bridge->log_level ) {
		return swe_ok;
	}

	va_start(ap, format);
	vsprintf(title,format, ap);
	set_log_colour(colour); //set the proper colour
	rtn+= imsgn("\n\r======== ");
	rtn+= imsgn("%s", title);
	rtn+= imsgn(" ========\n\r");
	set_log_colour(NRM); //reset console colour
	va_end(ap);

	return swe_ok;
}



