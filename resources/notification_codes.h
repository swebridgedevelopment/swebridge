/*
 * This file includes notifications codes that will be past to the platform hosting the
 * SWE Bridge. It allows the hosting system to know the step of the execution
 *
 *  @author: Enoc Martínez
 *  @institution: Universitat Politècnica de Catalunya (UPC)
 *  @contact: enoc.martinez@upc.edu
 */

#ifndef RESOURCES_INC_NOTIFICATION_CODES_H_
#define RESOURCES_INC_NOTIFICATION_CODES_H_

/* Execution Steps */
#define SWEB_NOTIFY_INIT 0              // Initial SWE Bridge state
#define SWEB_NOTIFY_SYSTEM_CHECK 1      // Performing a system check
#define SWEB_NOTIFY_PUCK_WRITE 2        // Using PUCK Protocol to retrieve a SensorML
#define SWEB_NOTIFY_PUCK_EXTRACT 3      // Using PUCK Protocol to retrieve a SensorML
#define SWEB_NOTIFY_DECODING_SENSORML 4
#define SWEB_NOTIFY_SCHEDULER_SETUP 5   // Setup scheduler
#define SWEB_NOTIFY_SCHEDULER_INIT  6   // Execute scheduler initialiation processes
#define SWEB_NOTIFY_SCHEDULER_START 7   // Scheduler is running
#define SWEB_NOTIFY_SCHEDULER_PAUSE 8   // Scheduler is running
#define SWEB_NOTIFY_SCHEDULER_STOP  9   // Stop scheduler
#define SWEB_NOTIFY_EXIT  10            // Exit SWE Bridge scheduler
#define SWEB_NOTIFY_NEW_DATA_FILE  11
#define SWEB_NOTIFY_ENTERING_DEEP_SLEEP 12 // The system is going to deep sleep
#define SWEB_NOTIFY_EXIT_DEEP_SLEEP 13     // The system exited deep sleep
#define SWEB_NOTIFY_OPEN_IFACE 14    // Opening communications interface
#define SWEB_NOTIFY_CLOSE_IFACE 15   // Closing communications interface
#define SWEB_NOTIFY_TEMP_FOLDER 16   // Notify to the platform which folder will be used to store temp files
#define SWEB_NOTIFY_OUTPUT_FOLDER 17 // Notify to the platform which folder will be used to store output files


#endif /* RESOURCES_INC_NOTIFICATION_CODES_H_ */
