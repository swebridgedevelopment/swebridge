/*
 *  This File contains the implementation of a set of wrapper for memory operation
 *  (malloc, realloc, calloc and free).
 *
 *  If the MEMORY_CONTROL option is set in swe_conf.h a set of static memory
 *  tables are defined to store all the operations. This increases the memory used
 *  by the SWE Bridge, but simplifies the debugging procedure.
 *
 *  @author: Enoc Martínez
 *  @institution: Universitat Politècnica de Catalunya (UPC)
 *  @contact: enoc.martinez@upc.edu
 */

#ifndef INC_MEM_CONTROL_H_
#define INC_MEM_CONTROL_H_

#include "common/swe_utils.h"
#include "swe_conf.h"

typedef enum { MEM_MAIN=0, MEM_PUCK, MEM_SML, MEM_DECODER, MEM_SCHEDULER, MEM_EXIP, MEM_IFACE, MEM_SXMLC} Mem_Slots;
char* set_string( const char*value);

/*
 * Declare memory resources as external functions
 */
extern void* system_malloc(uint size);
extern void* system_calloc(uint count, uint size);
extern void* system_realloc(void* old_pointer, uint size);
extern void  system_free(void* ptr);

#if (MEMORY_CONTROL == OFF)
/*
 * If Memory Control is OFF set up macros pointing to
 */
#define swe_malloc system_malloc
#define swe_calloc system_calloc
#define swe_realloc system_realloc

/*
 * System Free frees the pointer and sets it to NULL to prevent double
 * freeing the same pointer.
 */
#define swe_free(_ptr) do { \
		if (_ptr != NULL ) { \
			system_free(_ptr);\
			_ptr = NULL; \
		} \
	}while(0)

char* set_string(const char* string);

#define exip_malloc swe_malloc
#define exip_realloc swe_realloc
#define exip_calloc swe_calloc
#define exip_set_string set_string

#define sxmlc_malloc swe_malloc
#define sxmlc_realloc swe_realloc
#define sxmlc_calloc swe_calloc
#define sxmlc_set_string set_string


//---- Memory Control API ----//
/*
 * Define the functions as empty macros to avoid them to be compiled
 */
#define init_memory_control(x)
#define mem_status(x)
#define set_memory_table(x)
#define test_memory(x)



#else /* MEMORY_CONTROL == ON */


//--------Memory Tables Size--------//
#define NUMBER_OF_MEM_TABLES 8 //number of memory tables
#define MEM_TABLE_SIZE 4096 //maximum number of entries in the memory table

/*
 * when this option is set ON every memory operation will be displayed
 */
#define SHOW_MEM_OPERATIONS OFF

//--------Memory Table Structures--------//



//Memory Control Tables Slots
typedef struct {
	uint bytes;
	void* address;
	char where[256];
}MemoryTable;


//---- Memory Functions Implementation ----//
void* _swe_malloc(uint size, const char* where);
void* _swe_realloc(void* oldPointer, uint size, const char* where);
void* _swe_calloc(uint num, uint size, const char* where) ;
void  _swe_free(void* pointer);


//---- Memory Control API ----//
int init_memory_control();
int set_memory_table(int new_table);
int get_memory_table();
void _test_memory();
int mem_status();
int show_pointer_info(void* address);
int show_memory_table_contents(int table);
int show_all_memory_tables_contents();
const char* get_mem_table_name(int table);
int free_all_memory();




#if SHOW_MEM_OPERATIONS
/*
 * Define a macro to print memory operation
 */
#define PRINT_MEM_OPERATION(_operation, _size) ({ \
	int _table = get_memory_table(); \
	if (_operation == 1 )  { \
		cdmsg(_table,"                                           Malloc  %u bytes table \"%s\" (file %s, line %d)", _size, get_mem_table_name(_table), __FILE__, __LINE__);\
	}\
	else if (_operation == 2 )  { \
		cdmsg(_table,"                                           Realloc %u bytes table \"%s\" (file %s, line %d)", _size, get_mem_table_name(_table),__FILE__, __LINE__);\
	} \
	else if (_operation == 3 )  { \
		cdmsg(_table,"                                           Calloc  total %u bytes table %s (file %s, line %d)", _size,get_mem_table_name(_table), __FILE__, __LINE__);\
	} \
	else if (_operation == 4 )  { \
		cdmsg(_table,"                                           Free table \"%s\" (file %s, line %d)",  get_mem_table_name(_table),__FILE__,  __LINE__);\
	} \
})
#else
#define PRINT_MEM_OPERATION(_operation, _size) // Empty macro won't compile
#endif



#define GET_CALL_LOCATION() char __where[256]; sprintf(__where, "file %s, line %d", __FILE__, __LINE__);



#define say_hello() WHERE(); _say_hello(__where)
/*
 * Malloc Macro
 */
#define swe_malloc(_size) ({ \
	PRINT_MEM_OPERATION(1, _size);\
	GET_CALL_LOCATION() \
	void* ptr = _swe_malloc(_size, __where);\
	ptr; \
})

/*
 * Malloc Macro
 */
#define swe_realloc(_ptr, _size) ({ \
	PRINT_MEM_OPERATION(2, _size);\
	GET_CALL_LOCATION() \
	void* ptr = _swe_realloc(_ptr, _size, __where);\
	ptr; \
})

#define swe_calloc(_count, _size) ({ \
		PRINT_MEM_OPERATION(3, _size); \
		GET_CALL_LOCATION() \
		void* _ptr = _swe_calloc(_count, _size, __where); \
		_ptr; \
})


#define swe_free(_ptr) do { \
	PRINT_MEM_OPERATION(4, 0);\
	_swe_free(_ptr);\
	_ptr = NULL; \
}while(0)


#define set_string(_value)({ \
	uint _i,_len; \
	char* _string = NULL; \
	if(_value==NULL || _value[0] == 0){ \
	} \
	else { \
		_len=strlen(_value)+1; \
		_string=swe_malloc(sizeof(char)*_len); \
		for(_i=0; _i<(_len-1); _i++) _string[_i]=_value[_i]; \
		_string[_len-1]=0; \
	}\
	_string; \
})


#define test_memory(x) \
	dmsg("testing memory at func %s, file %s, line %d", __func__, __FILE__, __LINE__); \
	_test_memory();



//---------------------------------------------------------------------------//
//--------------------- MEM TABLE WARPPER FUNCTIONS -------------------------//
//---------------------------------------------------------------------------//
/*
 * The following macros are used to give direct access to a memory table and
 * are used in some parts of the code to isolate memory issues
 */

//-------- EXIP --------//
#define exip_malloc(_size) ({ \
		void* _ptr; \
		int _old_mem_slot = get_memory_table(); \
		set_memory_table(MEM_EXIP); \
		_ptr = swe_malloc(_size); \
		set_memory_table(_old_mem_slot); \
		_ptr; \
	})

#define exip_realloc(_old_point, _size) ({ \
		void* _ptr; \
		int _old_mem_slot = get_memory_table(); \
		set_memory_table(MEM_EXIP); \
		_ptr  = swe_realloc(_old_point, _size); \
		set_memory_table(_old_mem_slot); \
		_ptr; \
	})

#define exip_set_string(_string)  ({ \
		char* _ptr; \
		int _old_mem_slot = get_memory_table(); \
		set_memory_table(MEM_EXIP); \
		_ptr  = set_string(_old_point); \
		set_memory_table(_old_mem_slot); \
		_ptr; \
	})

//-------- SXMLC --------//
#define sxmlc_malloc(_size) ({ \
		void* _ptr; \
		int _old_mem_slot = get_memory_table(); \
		set_memory_table(MEM_SXMLC); \
		_ptr = swe_malloc(_size); \
		set_memory_table(_old_mem_slot); \
		_ptr; \
	})

#define sxmlc_realloc(_old_point, _size) ({ \
		void* _ptr; \
		int _old_mem_slot = get_memory_table(); \
		set_memory_table(MEM_SXMLC); \
		_ptr  = swe_realloc(_old_point, _size); \
		set_memory_table(_old_mem_slot); \
		_ptr; \
	})

#define sxmlc_calloc(_count, _size) ({ \
		void* _ptr; \
		int _old_mem_slot = get_memory_table(); \
		set_memory_table(MEM_SXMLC); \
		_ptr  = swe_calloc(_count	, _size); \
		set_memory_table(_old_mem_slot); \
		_ptr; \
	})


#define sxmlc_set_string(_string)  ({ \
		char* _ptr; \
		int _old_mem_slot = get_memory_table(); \
		set_memory_table(MEM_SXMLC); \
		_ptr  = set_string(_string); \
		set_memory_table(_old_mem_slot); \
		_ptr; \
	})






#endif /* MEMORY_CONTROL == ON */

#endif /* INC_MEM_CONTROL_H_ */
