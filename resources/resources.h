/*
 ============================================================================
								PlatformResources.h
 ============================================================================
 This aim of this library is to wrap the swe generic functions to the
 platform specific functions. If the project has to be ported to a new
 platform the wrappers have to be defined here.
 ============================================================================
 by SARTI
 Author: Enoc Martínez
 ============================================================================
 */
#ifndef PLATFORM_RESOURCES_H_
#define PLATFORM_RESOURCES_H_

#include "common/sensorml.h"
#include "resources/mem_control.h"
#include "resources/notification_codes.h"
#include "resources/platform_resources.h"
#include "swe_conf.h"

//---------------------------------------------------------------------------//
//------------------------------- FILESYSTEM --------------------------------//
//---------------------------------------------------------------------------//
typedef struct {
	swe_file* (*open)(const char* filename, swe_file_flags flags);
	int (*close)(swe_file* file);
	int (*write)(const void* data, int bytes, swe_file* file);
	int (*read)(void* read_data,int bytes, swe_file* file);
	int (*create_dir)(const char *dirname);
	int (*remove)(const char* filename);
	int (*file_size)(swe_file* filename);
	int (*move)(const char* oldname, const char* newname);
	int (*get_char)(swe_file* file);
	int (*eof)(swe_file* file);
	int (*set_pointer)(swe_file* file, int position);
	char** (*list_dir)(const char *dirpath, int *n);
	uchar (*file_exists)(const char *filename);
	swe_file** open_files;
}File_System;


swe_file* swe_fopen(const char* filename, swe_file_flags flags);
int swe_fclose(swe_file* file);
int swe_fwrite(const void* data, int bytes, swe_file* file);
int swe_fread (void* read_data, int bytes, swe_file* file);
int swe_create_dir(const char *dirname);
int swe_fremove(const char* filename);
int swe_file_size(swe_file* file);
int swe_fmove (const char* oldname, const char* newname);
int swe_getc(swe_file* file);
int swe_eof(swe_file* file);
int swe_set_pointer(swe_file* file, int position);
int swe_fputs (const char* string, swe_file* file);
int close_all_files();
char** swe_list_dir(const char * dirpath, int* n);
uchar swe_file_exists(const char* filename);



//-----------------------------------------------------------------//
//---------------------------- TIMING -----------------------------//
//-----------------------------------------------------------------//

typedef struct {
	int (*delayms)(ulong msecs);
	float64 (*get_epoch_time)();
	int (*set_timer)(ulong usec_period, void (*handler)());
	int (*start_timer)();
	int (*stop_timer)();
	float64 (*mktime)(struct tm * isoctime);
	struct tm* (*localtime)(const time_t * time);
	long int (*get_timezone_seconds)();
}SWE_Timer;



int delayms(ulong msecs);
int swe_get_datetime(DateTime* datetime);
int swe_get_time(DateTime* mytime);
float64 swe_get_epoch_time();
int swe_set_timer(ulong usec_period, void (*handler)());
int swe_start_timer();
int swe_stop_timer();
float64 swe_mktime(struct tm* isoctime);
struct tm swe_localtime(const time_t* time);
long int get_timezone_seconds();

//-----------------------------------------------------------------//
//-------------------------- INTERFACES ---------------------------//
//-----------------------------------------------------------------//

//----------- IMPLEMENTED INTERFACES ------------//
typedef enum {
	iface_unknown=0,
	iface_tcp=1,
	iface_udp=2,
	iface_uart=3,
}interface_type;

/*
 * Interface Structure. This structure contains the configuration options, the handlers and also the
 * internal implementation of the interface
 */
typedef struct {
	Interface_Options opts; 	// Interface Configuration Options

	// Handlers
	Interface_Descriptor* (*open)(Interface_Options* opts);
	int (*recv)(Interface_Descriptor* iface, uchar* buffer, uint len, ulong timeout);
	int (*send)(Interface_Descriptor* iface, void* buffer, uint len);
	int (*send_to)(Interface_Descriptor* iface, void* buffer, uint len, char* destination);

	int (*close)(Interface_Descriptor* iface);
	int (*fflush)(Interface_Descriptor* iface);
	int (*set_baudrate)(Interface_Descriptor* iface, ulong baudrate);
	int (*set_interrupt)(Interface_Descriptor* iface, uchar* flag);
	int (*set_priority_interrupt)(Interface_Descriptor* iface, void(*handler)(void));

	interface_type type; // flag which determines which kind of interface is stored in the Interface_Descriptor structure
	uchar is_interrupt_set; //flag that determines if an interrupt has been set for this Interface
	Interface_Descriptor* interface; // pointer to the platform-dependent structure (only used internally by the driver)
}Interface;


/*
 * Default Interface Options
 */
#define DEFAULT_BAUDRATE 115200
#define DEFAULT_INTERFACE_TIMEOUT 1000000 // in usecs
#define INSTRUMENT_BUFFER_SIZE 1000
#define DEFAULT_MAX_PACKET_SIZE 1000




#define RS232_INTERFACE_NAME "RS232"
#define UART_INTERFACE_NAME "UART"
#define TCP_INTERFACE_NAME "TCP"
#define UDP_INTERFACE_NAME "UDP"

//-----INTERFACE NAMES SML----//
#define INTERFACE_PORT_SML "portNumber"
#define INTERFACE_IP_SML "IP"
#define INTERFACE_MAX_PACKET_SIZE_SML "maxPacketSize"


// Setup Interfaces //
Interface* setup_interface(DataInterface* di);
Interface* setup_uart_iface(char* serial_device, int baudrate, uchar sw_flow_control);
Interface* setup_tcp_iface(char* IP, int port);
Interface* setup_udp_iface(int port);

const char* print_interface_type(interface_type t); // return a string with the interface type

// Interface availability //
uchar is_uart_available(char* device);
uchar is_udp_available(int port);
uchar is_tcp_available(char* IP, int port);

// Interface usage //
int iface_open(Interface* iface);
int iface_close(Interface* iface);
int iface_recv(Interface_Descriptor* iface,  uchar* buffer, uint len, ulong timeout);
int iface_set_interrupt(Interface*_iface, uchar*_flag);
int iface_set_priority_interrupt(Interface* iface, void(*handler)(void));
int close_all_interfaces();

/*
 * Interface macros to call its internal handlers as functions
 */
#define iface_send(_iface, _buffer,_bufflen) _iface->send(_iface->interface, _buffer,_bufflen)
#define iface_send_to(_iface, _buffer,_bufflen, _addr) _iface->send_to(_iface->interface, _buffer,_bufflen, _addr)
#define iface_fflush(_iface) _iface->fflush(_iface->interface)
#define iface_set_baudrate(_iface,_bd) _iface->set_baudrate(_iface->interface,_bd)
#define iface_puts(_iface, _str) _iface->send(_iface->interface, _str, strlen(_str))


//-----------------------------------------------------------------//
//----------------------------- PLATFORM --------------------------//
//-----------------------------------------------------------------//
typedef struct {
	int (*init)(int argc, char** argv);     // Initialize the platform
	int (*exit)(); // Deinit platform and exit
	int (*standby)(); // set the platform in standby (sleep or similar) until an event ie received (timer, interruption, etc.)
	int (*deep_sleep)(float time); // set the platform in standby (sleep or similar) until an event ie received (timer, interruption, etc.)
	int (*coordinates)(float* latitude, float* longitude);
	int (*altitude)(float* altitude);
	int (*system_monitor)(); // Displays info about the system status
	int (*vprintf)(const char* format, va_list args); // Platform implementation of printf
	int (*vfprintf)(swe_file* file, const char* format, va_list args); // Platform implementation of printf
	int (*fflush)(); // fflush stdout
	int (*notify)(const int code, void* arg); // Notify an event to the host platform
}Platform;

int init_platform(int argc, char** argv);
int exit_platform();
int standby_platform();
int deep_sleep(float time);
int get_coordinates(float* latitude, float* longitude);
int get_altitude(float* altitude);
int system_monitor();
int system_vprintf(const char* format, va_list args);
int system_vfprintf(swe_file* file, const char* format, va_list args);
int system_printf(const char* format, ...);
int system_fprintf(swe_file* file, const char* format, ...);
int system_fflush();
int system_notify(const int code, void* arg);


//-----------------------------------------------------------------//
//------------------------ MEMORY MANAGEMENT ----------------------//
//-----------------------------------------------------------------//
typedef struct {
	void* (*malloc)(size_t size);
	void* (*calloc)(size_t count, size_t size);
	void* (*realloc)(void* old_pointer, size_t size);
	void (*free)(void* ptr);
}Memory_Management;

void* system_malloc(uint size);
void* system_calloc(uint count, uint size);
void* system_realloc(void* old_pointer, uint size);
void  system_free(void* ptr);

//-----------------------------------------------------------------//
//---------------------- POWER MANAGEMENT -------------------------//
//-----------------------------------------------------------------//
/*
 * Power Management allows the platform to power ON or OFF sensors.
 * NOTE: It may be unimplemented in some platforms wihtout power control!
 */
typedef struct {
	int (*power_initialize)(int sensor_id);
	int (*power_on_sensor)(int sensor_id);
	int (*power_off_sensor)(int sensor_id);

}Power_Management;

int power_initialize(int sensor_id);
int power_on_sensor(int sensor_id);
int power_off_sensor(int sensor_id);


//-----------------------------------------------------------------//
//---------------------- INTERNAL SENSORS -------------------------//
//-----------------------------------------------------------------//
/*
 * Internal Sensors allows to read built-in sensors. To use this
 * function specific handlers for each platform have to be used
 */

typedef struct {
	int (*init)(int sensor_index);
	int (*read)(int sensor_index, void* buffer, int bufflen);
}Internal_Sensors;

int internal_sensors_initialize(int sensor_index);
int internal_sensors_read(int sensor_index, void* buffer, int bufflen);


//-----------------------------------------------------------------//
//------------------------ ADC MEASURE SENSORS --------------------//
//-----------------------------------------------------------------//
/*
 * Read the output of an ADC built-in converter
 */
typedef struct {
	int (*read)(int adc_index, float* measure);
	int (*init)(int adc_index);
}ADC_Measure;

int adc_read(int adc_index, float* measure);
int adc_initialize(int adc_index);



/*
 * Structure to link interface with SensorML files
 */
typedef struct {
	Interface* iface;
	char* SensorML;
	PhysicalSystem* instrument;
	AggregateProcess* mission;
}InstrumentConfig;


#endif //PLATFORM_RESOURCES_H_
