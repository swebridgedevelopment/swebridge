/*
 *  This File contains the implementation of a set of wrapper for memory operation
 *  (malloc, realloc, calloc and free).
 *
 *  If the MEMORY_CONTROL option is set in swe_conf.h a set of static memory
 *  tables are defined to store all the operations. This increases the memory used
 *  by the SWE Bridge, but simplifies the debugging procedure.
 *
 *  @author: Enoc Martínez
 *  @institution: Universitat Politècnica de Catalunya (UPC)
 *  @contact: enoc.martinez@upc.edu
 */


#include <string.h>

#include "swe_conf.h"
#include "resources/mem_control.h"

//---------------------------------------------------------------------------//
//------------------------ SIMPLE WRAPPER FUNCTIONS -------------------------//
//---------------------------------------------------------------------------//

#if (MEMORY_CONTROL == 0)

char* set_string( const char*value){
	uint i,len;
	char* string;
	if(value==NULL || value[0] == 0){
		return NULL;
	}
	len=strlen(value)+1;
	string=swe_malloc(sizeof(char)*len);
	//memset(string, 0, len);
	for(i=0; i<(len-1); i++) string[i]=value[i];
	string[len-1]=0;
	return string;
}

#else  // MEMORY_CONTROL == ON


#include "resources/resources.h"

//---------------------------------------------------------------------------//
//-------------------- MEMORY CONTROL WRAPPER FUNCTIONS ---------------------//
//---------------------------------------------------------------------------//


/*
 * The memory table that contains all the operations
 */
static MemoryTable memory_tables[NUMBER_OF_MEM_TABLES][MEM_TABLE_SIZE]; // All memory tables
static int current_table_index = MEM_MAIN;
static MemoryTable* current_mem_table = NULL; // Current Memory table

static const char* memory_table_names[]= { "MAIN", "PUCK", "SML", "PARSER", "SCHEDULER", "EXIP", "IFACE", "SXMLC"};


//---- Private Functions ----//
MemoryTable* get_memory_table_entry(void* pointer);
int get_free_slot();
int register_mem_operation(uint bytes, void* address, const char* where);
int memory_table_summary(MemoryTable* table, uint* total_bytes, uint* total_entries);

static uint current_memory = 0;
static uint memory_peak = 0;

//---------------------------------------------------------------------------//
//--------------------------- WARPPER FUNCTIONS -----------------------------//
//---------------------------------------------------------------------------//

/*
 * If the MEMORY_CONTROL flag is not set these functions simply call the
 * appropriate memory management function. However if the flag is set they also
 * update the information
 */

/*
 * Looks for a new slot in the current memory table and registers the transaction
 * Calls Malloc
 */
void* _swe_malloc(uint size, const char* where){
	void* ptr;

	if (size == 0 ){
		warnmsg("ignoring swe malloc size 0!");
		return NULL;
	}

	ptr = system_malloc(size);
	if (ptr == NULL) {
		errmsg("CRITICAL MEMORY ERROR: malloc returned NULL");
		platform_exit();
	}
	memset(ptr, 0, size);
	// Register the memory operation
	register_mem_operation(size, ptr, where);

	// Update the memory counters //
	current_memory += size;
	if (current_memory > memory_peak ) {
		memory_peak = current_memory;
	}
	return ptr;
}

/*
 * Looks for an old pointer, clears its registry
 * reallocates the pointer with new size
 * registers the new transaction
 */
void* _swe_realloc(void* old_pointer, uint size, const char* where){
	void* new_pointer;
	int old_size;
	MemoryTable* entry;

	if ( old_pointer == NULL ) {
		return _swe_malloc(size, where);
	}

	/* Get the old position */
	entry = CATCH_NULL(get_memory_table_entry(old_pointer));
	old_size = entry->bytes;

	new_pointer = system_realloc(old_pointer, size);
	if (new_pointer == NULL) {
		errmsg("CRITICAL MEMORY ERROR: realloc returned NULL");
		platform_exit();;
	}

	entry->bytes = size;
	entry->address = new_pointer;
	strcpy(entry->where, where);

	// Update global pointer
	current_memory = current_memory - old_size + size;
	if (current_memory > memory_peak ) {
		memory_peak = current_memory;
	}
	return new_pointer;
}


/*
 * Registers a new memory operation
 * calls calloc
 */
void* _swe_calloc(uint count, uint size, const char* where) {
	void* ptr;
	if (size == 0 ){
		warnmsg("ignoring swe malloc size 0!");
		return NULL;
	}
	ptr = system_calloc(count,size);
	if (ptr == NULL) {
		errmsg("CRITICAL MEMORY ERROR: calloc returned NULL");
		platform_exit();
	}
	// Register the memory operation
	register_mem_operation(size*count, ptr, where);

	// Update the memory counters
	current_memory += size;
	if (current_memory > memory_peak ) {
		memory_peak = current_memory;
	}
	return ptr;
}


/*
 * Looks for the entry in the memory table and erases it.
 * Calls system free
 */
void _swe_free(void* pointer){

	if (pointer!=NULL){
		MemoryTable* entry = get_memory_table_entry(pointer);
		current_memory  -= entry->bytes;
		entry->bytes = 0;
		entry->address = NULL;
		entry->where[0] = 0;
		system_free(pointer);
	}
}

/*
 * Changes the current_table
 */
int set_memory_table(int new_table){
	current_mem_table = memory_tables[new_table];
	current_table_index = new_table;
	return swe_ok;
}


/*
 * Returns the index of the current table
 */
int get_memory_table(){
	return current_table_index;
}


/*
 * Initializes to 0 all memory tables
 */
int init_memory_control(){
	uint i,j;
	for (i=0; i<NUMBER_OF_MEM_TABLES; i++) {
		MemoryTable* table = memory_tables[i];
		for(j=0; j<MEM_TABLE_SIZE; j++){
			MemoryTable* entry = &table[j];
			entry->address = NULL;
			entry->bytes = 0;
			entry->where[0] = 0;
		}
	}
	current_mem_table = memory_tables[MEM_MAIN];
	return swe_ok;
}

/*
 * Returns the name of a table
 */
const char* get_mem_table_name(int table){
	return memory_table_names[table];
}


/*
 * Prints the amount of memory stores in each memory table
 */
int mem_status(){
	uint j;
	for (j=0; j<NUMBER_OF_MEM_TABLES; j++) {
		MemoryTable* table = memory_tables[j];
		uint bytes=0;
		uint entries=0;
		TRY_RET(memory_table_summary(table, &bytes, &entries));

		if(entries > 0) {
			cdmsg( CYN,"MEMORY CONTROL: table %s has %u entries, total memory %u bytes",
				memory_table_names[j],entries, bytes);
		}
	}
	cdmsg(CYN, "MEMORY CONTROL: Total memory %u (peak memory is %u)", current_memory, memory_peak);
	return swe_ok;
}


/*
 * Allocates some memory and does some stuff to check if the memory is corrupt
 */
void _test_memory(){
	char* point=malloc(500);
	dmsgn( "MEMTEST----------------------------");
	strcpy(point, "allocating a lot of useless memory");
	point = realloc(point, 1000);
	strcpy(point+500, "even more useless memory\0");

	swe_file *prova=swe_fopen("test.txt", swe_write_file);
	swe_fwrite( "test", 4*sizeof(char), prova);
	swe_fclose(prova);
	swe_fremove("test.txt");
	free(point);
	dmsgn( "OK\n");
}



/*
 * Displays all memory table contents (as strings)
 */
int show_memory_table_contents(int table_num){
	int i;
	uint totalbytes;
	uint entries;
	MemoryTable* table = memory_tables[table_num];
	memory_table_summary(table, &totalbytes, &entries);

	if ( entries > 0 ) {
		msg_sub_title(CYN, "Memory Table %s", memory_table_names[table_num]);
		for ( i=0 ; i<MEM_TABLE_SIZE ; i++ ) {
			MemoryTable* entry = &table[i];
			if ( entry->bytes > 0 ) {
				dmsg("    %03d %u bytes %p (allocated at %s)", i, entry->bytes, entry->address,  entry->where);
			}
		}
		cdmsg(CYN, "Total %u entries, %u bytes", entries, totalbytes);
	}

	return swe_ok;
}

/*
 * Shows all memory tables
 */
int show_all_memory_tables_contents(){
	int i;
	dmsg("Showing all memory tables!");
	for ( i=0 ; i<NUMBER_OF_MEM_TABLES ; i++ ) {
		show_memory_table_contents(i);
	}
	return swe_ok;
}


/*
 * Looks for a pointer in memory tables and prints its info
 */
int show_pointer_info(void* address){
	MemoryTable* entry = get_memory_table_entry(address);
	if ( entry != NULL ){
		dmsg("Pointer %p, %d bytes, allocated at %d", entry->address, entry->bytes, entry->where);
	} else {
		errmsg("Pointer %p not found", address);
	}
	return swe_ok;
}


//---------------------------------------------------------------------------//
//--------------------------- PRIVATE FUNCTIONS -----------------------------//
//---------------------------------------------------------------------------//

/*
 * Prints a memory table
 */
int memory_table_summary(MemoryTable *table, uint* total_bytes, uint* total_entries){
	uint i;
	uint total_memory = 0;
	uint entries = 0;
	for(i=0; i<MEM_TABLE_SIZE; i++){
		if( table[i].address != NULL ){
			entries++;
			total_memory += table[i].bytes;
		}
	}
	*total_bytes = total_memory;
	*total_entries = entries;
	return swe_ok;
}


/*
 * Searches in all tables for 'pointer' and returns its table entry
 */
MemoryTable* get_memory_table_entry(void* pointer){
	int i, j;
	for (i=0; i < NUMBER_OF_MEM_TABLES ; i++) {
		for ( j=0 ; j<MEM_TABLE_SIZE ; j++ ) {
			if (memory_tables[i][j].address == pointer ){
				// Found memory position!
				return &memory_tables[i][j]; // Return memory entry
			}
		}
	}

	errmsg("CRITICAL MEMORY ERROR: pointer(%p) not found in memory tables", pointer);
	platform_exit();
	return NULL;
}

/*
 * Get the index of the first free memory slot in the current table
 */
int get_free_slot(){
	int i=0;
	while( ( i < MEM_TABLE_SIZE ) &&
			current_mem_table[i].address != NULL ){ //search the first empty space
		i++;
	}
	// Check if we exceed the size //
	if ( i >= MEM_TABLE_SIZE ){
		errmsg("CRITICAL MEMORY ERROR: not enough space in MEM_CONTROL memory tables, please increase the MEM_TABLE_SIZE");
		platform_exit();
	}
	return i;
}


/*
 * writes a memory operation into a the current memory table
 */
int register_mem_operation(uint bytes, void* address, const char* where){
	int slot = get_free_slot();
	current_mem_table[slot].bytes = bytes;
	current_mem_table[slot].address = address;
	strcpy(current_mem_table[slot].where, where);
	return swe_ok;
}


/*
 * Frees ALL memory from ALL tables. Very useful to detect memory corruption
 */
int free_all_memory(){
	int i, j, entries, bytes;
	for ( i=0 ; i<NUMBER_OF_MEM_TABLES ; i++ ) {
		bytes = 0;
		entries = 0;
		imsgn("Freeing table %s...", memory_table_names[i]);
		MemoryTable* m = memory_tables[i];
		for (j=0; j<MEM_TABLE_SIZE; j++){
			if ( m[j].bytes > 0 ) {
				bytes += m[j].bytes;
				swe_free(m[j].address);
				entries++;

			}
		}
		imsg("%d entries %d bytes", entries, bytes);

	}
	return swe_ok;
}



#endif /* MEMORY_CONTROL */
