/*
 ==============================================================================
							   	resources.c
 ==============================================================================
 This file contains the structures to store the handlers for all the required
 resources from the platform (file system, interfaces, timers, etc.) Each
 structure contains a set of handlers which are assigned depending on the
 platform.
 ==============================================================================
 by SARTI
 Author: Enoc Martínez
 ==============================================================================
 */


#include "resources/resources.h"
#include "common/sensorml.h"
#include "common/swe_data_model.h"
#include "common/swe_utils.h"
#include "swe_bridge.h"
#include "swe_conf.h"

//---------------------------------------------------------------------------//
//------------------------------- FILESYSTEM --------------------------------//
//---------------------------------------------------------------------------//
/*
 * Initialize the filesystem structure
 */
File_System filesystem = {
		.open = platform_fopen,
		.close = platform_fclose,
		.write = platform_fwrite,
		.read = platform_fread,
		.create_dir = platform_create_dir,
		.remove = platform_fremove,
		.file_size = platform_file_size,
		.move = platform_fmove,
		.get_char = platform_getc,
		.eof = platform_eof,
		.set_pointer = platform_set_pointer,
		.list_dir = platform_list_dir,
		.file_exists = platform_file_exists

};

/*
 * Call file open handler in filesystem structure
 */
swe_file* swe_fopen(const char* filename, swe_file_flags flags){
	swe_file* file;
	if (filesystem.open == NULL ) {
		return NULL;
	}
	if ((file = filesystem.open(filename, flags)) == NULL) {
		errmsg("Couldn't open file!");
		return NULL;
	}
	// update the opened files list
	filesystem.open_files = ATTACH_TO_ARRAY(filesystem.open_files, file);
	return file;
}

/*
 * Call file close in filesystem
 */
int swe_fclose(swe_file* file){
	if (filesystem.close == NULL ) {
		return swe_unimplemented;
	}
	filesystem.open_files = REMOVE_FROM_ARRAY(filesystem.open_files, file);
	return filesystem.close(file);
}

/*
 * call file write in filesystem
 */
int swe_fwrite(const void* data, int bytes, swe_file* file){
	if (filesystem.write == NULL ) {
		return swe_unimplemented;
	}
	return filesystem.write(data, bytes, file);
}

/*
 * call file read in filesystem
 */
int swe_fread (void* read_data, int bytes, swe_file* file){
	if (filesystem.read == NULL ) {
		return swe_unimplemented;
	}
	return filesystem.read(read_data, bytes, file);
}

/*
 * Write a string in file
 */
int swe_fputs(const char* string, swe_file* file) {
	if (filesystem.write == NULL ) {
		return swe_unimplemented;
	}
	return filesystem.write(string, strlen(string), file);
}

/*
 * Sets the write pointer to a specific position
 */
int swe_set_pointer(swe_file* file, int position){
	if ( filesystem.set_pointer == NULL ) {
		return swe_unimplemented;
	}
	return filesystem.set_pointer(file, position);
}


/*
 * Create a directory
 */
int swe_create_dir(const char *dirname){
	if (filesystem.create_dir == NULL ) {
		return swe_unimplemented;
	}
	return filesystem.create_dir(dirname);
}

/*
 * Remove a file
 */
int swe_fremove(const char* filename){
	if (filesystem.remove == NULL ) {
		return swe_unimplemented;
	}
	return filesystem.remove(filename);
}

/*
 * Get the size of an open file
 */
int swe_file_size(swe_file* file){
	if (filesystem.file_size == NULL ) {
		return swe_unimplemented;
	}
	return filesystem.file_size(file);
}


/*
 * Move a file, the path must be complete, i.e.
 * oldname = /oldfolder/myfile.txt
 * newname = /newfolder/mynewfilename.txt
 */
int swe_fmove(const char* oldname, const char* newname) {
	if (filesystem.move == NULL ) {
		return swe_unimplemented;
	}
	return filesystem.move(oldname, newname);
}


/*
 * Reads a char from a file
 */
int swe_getc(swe_file* file){
	if (filesystem.get_char == NULL ) {
		return swe_unimplemented;
	}
	return filesystem.get_char(file);
}


/*
 * Checks if the EOF has been reached
 */
int swe_eof(swe_file* file){
	if (filesystem.eof == NULL ) {
		return swe_unimplemented;
	}
	return filesystem.eof(file);
}


/*
 * Loops through the all opened files in the filesystem and closes them
 */
int close_all_files() {
	int i;
	if (filesystem.open_files != NULL ) {
		for (i=0 ; filesystem.open_files[i] != NULL ; i++ ){
			filesystem.close(filesystem.open_files[i]);
		}
	}
	return swe_ok;
}

/*
 * Return a list with all the contents of a directory
 */
char** swe_list_dir(const char *dirpath, int* n){
	if (filesystem.list_dir != NULL){
		return filesystem.list_dir(dirpath, n);
	}
	errmsg("Unimplemented!");
	return NULL;
}


/*
 * Check if a file exists
 */
uchar swe_file_exists(const char* filename){
	if (filesystem.file_exists != NULL){
		return filesystem.file_exists(filename);
	}
	errmsg("Unimplemented!");
	return FALSE;
}

//-----------------------------------------------------------------//
//---------------------------- TIMING -----------------------------//
//-----------------------------------------------------------------//

/*
 * SWE Timer structure that contains all handler for Timers, timestamps and delays
 */
SWE_Timer swe_timer = {
		.delayms = platform_delayms,
		.get_epoch_time = platform_get_epoch_time,
		.set_timer = platform_set_timer,
		.start_timer = platform_start_timer,
		.stop_timer = platform_stop_timer,
		.mktime = platform_mktime,
		.localtime = platform_localtime,
		.get_timezone_seconds = platform_get_timezone
};

/*
 * Sleeps the platform for x milliseconds
 */
int delayms(ulong msecs){
	if (swe_timer.delayms == NULL) {
		return swe_unimplemented;
	}
	return swe_timer.delayms(msecs);
}

/*
 * Returns a DateTime structure
 */
int swe_get_datetime(DateTime* datetime){
	float64 timestamp = swe_get_epoch_time();
	TRY_RET(datetime_from_timestamp(datetime, timestamp));
	return swe_ok;
}


/*
 * Returns the epoch time (precision depends on the platform)
 */
float64 swe_get_epoch_time(){
	if (swe_timer.get_epoch_time == NULL) {
		return -1.0;
	}
	return swe_timer.get_epoch_time();
}

/*
 * Sets a timer with "usec_perdiod", the handler will be executed each time
 * the timer expires
 */
int swe_set_timer(ulong usec_period, void (*handler)()){
	if (swe_timer.set_timer == NULL) {
		return swe_unimplemented;
	}

	return swe_timer.set_timer(usec_period, handler);
}

/*
 * Starts a timer (it needs to be previously configured)
 */
int swe_start_timer(){
	if (swe_timer.start_timer == NULL) {
		return swe_unimplemented;
	}

	return swe_timer.start_timer();
}

/*
 * Stops a timer
 */
int swe_stop_timer(){
	if (swe_timer.stop_timer == NULL) {
		return swe_unimplemented;
	}
	return swe_timer.stop_timer();
}


/*
 * Creates time froma a broken-down time
 */
float64 swe_mktime(struct tm* isoctime){
	if (swe_timer.mktime == NULL) {
		errmsg("Unimplemented mktime!");
	}
	return swe_timer.mktime(isoctime);
}

/*
 * Returns the system time in a struct tm
 */
struct tm swe_localtime(const time_t* time){
	if (swe_timer.localtime == NULL) {
		errmsg("Unimplemented localtime!");
	}
	return *swe_timer.localtime(time);
}

/*
 * Returns the timezone as seconds east of UTC
 */
long int get_timezone_seconds(){
	if (swe_timer.get_timezone_seconds == NULL) {
		errmsg("Unimplemented localtime!");
	}
	return swe_timer.get_timezone_seconds();
}


//-----------------------------------------------------------------//
//---------------------------- PLATFORM ---------------------------//
//-----------------------------------------------------------------//
/*
 * Static platform structure
 */
static Platform swe_platform = {
		.init = platform_init,
		.exit = platform_exit,
		.standby = platform_standby,
		.deep_sleep = platform_deep_sleep,
		.coordinates = platform_get_coordinates,
		.altitude = platform_get_altitude,
		.system_monitor = platform_system_monitor,
		.vprintf = platform_vprintf,
		.vfprintf = platform_vfprintf,
		.fflush = platform_fflush,
		.notify = platform_notify
};

/*
 * Initializes the platform
 */
int init_platform(int argc, char** argv){
	system_notify(SWEB_NOTIFY_INIT, NULL);
	if (swe_platform.init != NULL) {
		return swe_platform.init(argc, argv);
	}
	return swe_unimplemented;
}

/*
 * Deinits the platform and exits
 */
int exit_platform(){
	system_notify(SWEB_NOTIFY_EXIT, NULL);
	if (swe_platform.exit != NULL) {
		return swe_platform.exit();
	}
	return swe_unimplemented;
}

/*
 * Set the platform in standby (sleep or similar) until an event occurs (i.e. timer expires, interrupt in a UART received, etc.)
 */
int standby_platform(){
	if (swe_platform.standby != NULL) {
		return swe_platform.standby();
	} else {
		warnmsg("Standby not implemented!! Sleeping 1 second instead...");
		delayms(1000);
	}
	return swe_ok;
}

/*
 * Call deep sleep platform (time is in seconds)
 */
int deep_sleep(float seconds){
	int ret;
	if (seconds < 0) {
		errmsg("Can't use negative time!");
		return swe_error;
	}
	if (swe_platform.deep_sleep == NULL) {
		errmsg("Deep sleep wrapper not implemented!");
		return swe_unimplemented;
	}
	swe_bridge_status old_status = swe_bridge_get_status();
	swe_bridge_set_status(swe_bridge_deep_sleep, (void*)&seconds);
	ret = swe_platform.deep_sleep(seconds);
	swe_bridge_set_status(old_status, NULL);
	return ret;
}

/*
 * Get the platform latitude and longitude
 */
int get_coordinates(float* latitude, float* longitude) {
	int ret;
	if (latitude == NULL || longitude == latitude) {
		errmsg("Input argument is NULL");
		return swe_invalid_arguments;
	} else if (swe_platform.coordinates == NULL ) {
		errmsg("Get position not defined");
		return swe_unimplemented;
	}
	if ((ret=swe_platform.coordinates(latitude, longitude)) != 0) {
		errmsg("Couldn't get coordinates, handler returned %d", ret);
		return swe_error;
	}
	return swe_ok;
}

/*
 * Get the platform altitude (respect sea level)
 */
int get_altitude(float* altitude) {
	int ret;
	if (altitude == NULL ) {
		errmsg("Input argument is NULL");
		return swe_invalid_arguments;
	} else if (swe_platform.altitude == NULL ) {
		errmsg("Get altitude not defined");
		return swe_unimplemented;
	}
	if ((ret=swe_platform.altitude(altitude)) != 0) {
		errmsg("Couldn't get altitude, handler returned %d", ret);
		return swe_error;
	}
	return swe_ok;
}

/*
 * Displays platform info
 */
int system_monitor() {
	if (swe_platform.system_monitor != NULL) {
		return swe_platform.system_monitor();
	}
	return swe_unimplemented;
}

/*
 * Wrapper to call system vprintf
 */
int system_vprintf(const char* format, va_list args){
	int retval;
	if (swe_platform.vprintf != NULL) {
		retval = swe_platform.vprintf(format, args);
	} else {
		return swe_unimplemented;
	}
	return retval;
}

/*
 * Wrapper to call system vfprintf
 */
int system_vfprintf(swe_file* file, const char* format, va_list args){
	int retval;
	if (swe_platform.vfprintf != NULL) {
		retval = swe_platform.vfprintf(file,format, args);
	} else {
		return swe_unimplemented;
	}
	return retval;
}

/*
 * Wrapper to call system printf with variable args
 */
int system_printf(const char* format, ...){
	int retval;
	if (swe_platform.vprintf != NULL) {
		va_list args;
		va_start(args, format);
		retval = swe_platform.vprintf(format, args);
		va_end(args);
	} else {
		return swe_unimplemented;
	}
	return retval;
}


/*
 * Wrapper to call system fprintf with variable args
 */
int system_fprintf(swe_file* file, const char* format, ...){
	int retval;
	if (swe_platform.vfprintf != NULL) {
		va_list args;
		va_start(args, format);
		retval = swe_platform.vfprintf(file, format, args);
		va_end(args);
	} else {
		return swe_unimplemented;
	}
	return retval;
}

/*
 * Flush STDOUT
 */
int system_fflush(){
	if (swe_platform.fflush != NULL ) {
		return swe_platform.fflush();
	}
	return swe_unimplemented;
}


/*
 * Notifies a certain event to the host platform
 */
int system_notify(const int code, void* arg){
	if (swe_platform.notify != NULL) {
		return swe_platform.notify(code, arg);
	}
	return swe_unimplemented;
}



//-----------------------------------------------------------------//
//----------------------- POWER MANAGEMENT ------------------------//
//-----------------------------------------------------------------//

static Memory_Management memory_management = {
	.malloc = platform_malloc,
	.calloc = platform_calloc,
	.realloc = platform_realloc,
	.free = platform_free
};

/*
 * Wrapper to platform malloc
 */
void* system_malloc(uint size){
	void* ptr;
	if (size == 0 ){
		warnmsg("ignoring malloc with size 0!");
	    return NULL;
	}
	ptr = memory_management.malloc(size);
	memset(ptr, 0, size);
	return ptr;
}

/*
 * Wrapper to platform calloc
 */
void* system_calloc(uint count, uint size){
	return memory_management.calloc(count, size);
}

/*
 * Wrapper to platform realloc
 */
void* system_realloc(void* old_pointer, uint size){
	return memory_management.realloc(old_pointer, size);
}

/*
 * Wrapper to platform free
 */

void system_free(void* pointer){
	memory_management.free(pointer);
}

//-----------------------------------------------------------------//
//----------------------- POWER MANAGEMENT ------------------------//
//-----------------------------------------------------------------//

static Power_Management power_management = {
		.power_initialize = platform_power_initialize,
		.power_on_sensor = platform_power_on_sensor,
		.power_off_sensor = platform_power_off_sensor
};

int power_initialize(int sensor_id) {
	if (power_management.power_initialize == NULL ) {
		return swe_unimplemented;
	}
	return power_management.power_initialize(sensor_id);
}


// Call power on handler //
int power_on_sensor(int sensor_id) {
	if (power_management.power_off_sensor == NULL ) {
		return swe_unimplemented;
	}
	return power_management.power_on_sensor(sensor_id);
}

// Call Power Off handler //
int power_off_sensor(int sensor_id) {
	if (power_management.power_off_sensor== NULL) {
		return swe_unimplemented;
	}
	return power_management.power_off_sensor(sensor_id);
}


//-----------------------------------------------------------------//
//------------------- INTERFACE CONTROL ---------------------------//
//-----------------------------------------------------------------//


interface_type get_interface_type(Field* type_field);

static const char* _interface_type_list[4]={"unknown interface", "TCP/IP", "UDP", "UART"};

/*
 * Returns a string with tthe interface type
 */
const char* print_interface_type(interface_type t){
	if ( t < 0 || t > 3) {
		return "interface type out of bounds";
	}
	return _interface_type_list[(int)t];
}


Interface** interfaces_list = NULL;

/*
 * Configures an interface from DataInterface SensorML structure.
 */
Interface* setup_interface(DataInterface* di){
	Interface* interface = NULL;
	Field* port_type_field;
	interface_type type;
#if MEMORY_CONTROL
	int old_memory_table = get_memory_table();
	set_memory_table(MEM_IFACE);
#endif

	msg_sub_title(GRN, "Interface Setup");

	if (di == NULL){
		return NULL;
	}
	// Get the interface type
	port_type_field = get_field_by_name(di->fields, SML_PORT_TYPE);
	if (port_type_field == NULL) {
		errmsg("Port type not found");
		return NULL;
	}
	if(port_type_field->type != SWE_Category) {
		errmsg("%s field should be Category, got %d", SML_PORT_TYPE, (int)port_type_field->type);
		return NULL;
	}

	if ((type = get_interface_type(port_type_field)) == iface_unknown) {
		errmsg("unknown interface type");
	}
	// UART Interface //
	else if( type == iface_uart) {
		Field *field_serial_device, *field_baudrate, *field_sfc;

		int baudrate;
		char device[256];
		uchar sw_flow_control = FALSE;



		// Get the configuration fields //
		field_serial_device = CATCH_NULL(get_field_by_name(di->fields, SML_SERIAL_DEVICE));
		field_baudrate = CATCH_NULL( get_field_by_name(di->fields, SML_BAUDRATE));

		// Get the fields values //
		TRY_RET_NULL(field_text_value_get(field_serial_device, device, 256));
		TRY_RET_NULL(field_count_value_get(field_baudrate, &baudrate));



		// Optional fields //
		if ( (field_sfc = get_field_by_name(di->fields, SML_SOFTWARE_FLOW_CONTROL)) != NULL) {
			// Flow control is defined, get the value
			TRY_RET_NULL(field_boolean_value_get(field_sfc, &sw_flow_control));
		}


		if (is_uart_available(device) == FALSE) { 	// Check if it is availble
			errmsg("UART %s already in use!",  device);
			return NULL;
		}
		imsg("Interface type: UART");
		imsg("device: %s", device);
		imsg("baudrate: %d", baudrate);
		dmsg("Calling platform constructor...");
		interface = setup_uart_iface(device, baudrate, sw_flow_control); // Setup Interface
	}
	// UDP Interface //
	else if( type == iface_udp) { // UART Interface //
		int port;
		// Get the necessary parameters //
		TRY_RET_NULL(get_count_value_by_name(di->fields, SML_PORT, &port));

		if (is_udp_available(port) == FALSE) {
			errmsg("UDP port %d already in use!",  port);
			return NULL;
		}

		imsg("Interface type: UDP");
		imsg("port: %d", port);
		dmsg("Calling platform constructor...");
		interface = setup_udp_iface(port);
	}
	// TCP Interface //
	else if( type == iface_tcp) {
		char IP[64]="";
		int port;

		get_count_value_by_name(di->fields, SML_PORT, &port);
		get_category_value_by_name(di->fields, SML_IP_ADDRESS, IP, 64);

		if (IP == NULL) {
			errmsg("IP address not specified in TCP connection");
			return NULL;
		}

		if (is_tcp_available(IP, port) == FALSE) {
			errmsg("TCP Interface with IP %s and port %d already in use!",  IP, port);
			return NULL;
		}

		imsg("Interface type: TCP");
		imsg("IP: %s", IP);
		imsg("port: %d", port);
		dmsg("Calling platform constructor...");
		interface = setup_tcp_iface(IP, port);
	}

#if MEMORY_CONTROL
	set_memory_table(old_memory_table);
#endif

	TRY_RET_NULL(iface_open(interface));
	return interface;
}


/*
 * Setup UART Interface.
 */
Interface* setup_uart_iface(char* serial_device, int baudrate, uchar sw_flow_control){
	// Check input arguments //
	if (serial_device == NULL) {
		errmsg("setup_uart_iface received NULL serial_device");
		return NULL;
	}

	Interface* iface = swe_malloc(sizeof(Interface));
	iface->opts.serial_device = set_string(serial_device);
	iface->opts.baudrate = baudrate;
	iface->opts.sw_flot_control = sw_flow_control;
	iface->type = iface_uart;

	// Assign Handlers //
	iface->open=platform_open_uart;
	iface->recv=platform_read_uart;
	iface->send=platform_write_uart;
	iface->close=platform_close_uart;
	iface->fflush=platform_fflush_uart;
	iface->set_baudrate=platform_set_baudrate;
	iface->set_interrupt=platform_set_uart_interrupt;
	iface->set_priority_interrupt=platform_set_uart_priority_interrupt;
	return iface;
}

/*
 * Setup TCP Interface
 */
Interface* setup_tcp_iface(char* IP, int port){
	Interface* iface = swe_malloc(sizeof(Interface));
	iface->opts.IP = set_string(IP);
	iface->opts.port = port;
	iface->type = iface_tcp;

	// Assign Handlers //
	iface->open=platform_open_tcp;
	iface->recv=platform_read_tcp;
	iface->send=platform_write_tcp;
	iface->close=platform_close_tcp;
	iface->fflush=platform_fflush_tcp;
	iface->set_baudrate=platform_set_baudrate;
	iface->set_interrupt=platform_set_tcp_interrupt;
	iface->set_priority_interrupt=platform_set_tcp_priority_interrupt;
	return iface;
}

/*
 * Setup UDP Interface
 */
Interface* setup_udp_iface(int port){

	Interface* iface = swe_malloc(sizeof(Interface));
	iface->opts.port = port;
	iface->type = iface_udp;

	// Assign Handlers //

	iface->open=platform_open_udp;
	iface->recv=platform_read_udp;
	iface->send=NULL;
	iface->send_to=platform_send_to_udp;
	iface->close=platform_close_udp;
	iface->fflush=platform_fflush_udp;
	iface->set_baudrate=platform_set_baudrate;
	iface->set_interrupt=platform_set_udp_interrupt;
	iface->set_priority_interrupt=platform_set_udp_priority_interrupt;
	return iface;
}

/*
 * Takes a Category field and returns the interface type
 */

interface_type get_interface_type(Field* type_field) {
	char port_type[128];
	// Get the category value
	if ( field_category_value_get(type_field, port_type, 128) < 0) {
		return iface_unknown;
	}
	if (! compare_strings(RS232_INTERFACE_NAME, port_type)){
		return iface_uart;
	}
	else if (! compare_strings(UART_INTERFACE_NAME, port_type)){
		return iface_uart;
	}
	else if (! compare_strings(TCP_INTERFACE_NAME, port_type)){
		return iface_tcp;
	}
	else if (! compare_strings(UDP_INTERFACE_NAME, port_type)){
		return iface_udp;
	}
	return iface_unknown;
}


/*
 * Checks if a UART port is already in use
 */
uchar is_uart_available(char* device) {
	int i;
	// If there isn't any interface in the list the interface is available
	if (interfaces_list == NULL) {
		return TRUE;
	}
	// Check if a UART in the list has the same serial device
	for (i=0;  interfaces_list[i]!= NULL; i++ ) {
		if (interfaces_list[i]->type == iface_uart){
			if (!compare_strings(device, interfaces_list[i]->opts.serial_device)){
				return FALSE;
			}
		}
	}
	return TRUE;
}


/*
 * Checks if a UDP port is already in use
 */
uchar is_udp_available(int port) {
	int i;
	// If there isn't any interface in the list the interface is available
	if (interfaces_list == NULL) {
		return TRUE;
	}
	// Check if the same UDP port is in use
	for (i=0; interfaces_list[i]!= NULL; i++ ) {
		if (interfaces_list[i]->type == iface_udp){
			if (interfaces_list[i]->opts.port == port ){
				return FALSE;
			}
		}
	}
	return TRUE;
}

/*
 * Checks if a TCP connection to the IP and port is in use
 */
uchar is_tcp_available(char* IP, int port) {
	int i;
	// If there isn't any interface in the list the interface is available
	if (interfaces_list == NULL) {
		return TRUE;
	}
	// Check if the same UDP port is in use
	for (i=0; interfaces_list[i]!= NULL; i++ ) {
		if (interfaces_list[i]->type == iface_tcp){
			if ( !compare_strings(interfaces_list[i]->opts.IP, IP) ){
				if(interfaces_list[i]->opts.port == port) {
					return FALSE;
				}
			}
		}
	}
	return TRUE;
}

/*
 * Open communications Interface
 */
int iface_open(Interface* iface){
	if ((iface->interface = iface->open(&iface->opts))== NULL) {
		return swe_iface_error;
	}
	system_notify(SWEB_NOTIFY_OPEN_IFACE, NULL);
	// Attach the new interface to the interfaces_list
	if (iface != NULL){
		interfaces_list = ATTACH_TO_ARRAY(interfaces_list, iface);
	}
	return swe_ok;
}

/*
 * Close communications interface
 */
int iface_close(Interface* iface) {
	if (iface->close == NULL) {
		return swe_unimplemented;
	}
	system_notify(SWEB_NOTIFY_CLOSE_IFACE, NULL);
	interfaces_list = REMOVE_FROM_ARRAY(interfaces_list, iface);
	iface->is_interrupt_set = FALSE;
	return iface->close(iface->interface);
}


/*
 * Tries to read len bytes. If the timeout is 0 reads the maximum amount of bytes available
 * at the comm iface and returns immediately
 */
int iface_recv(void* iface,  uchar* buffer, uint len, ulong timeout){
	Interface* interface = (Interface*)iface;
	return interface->recv(interface->interface, buffer, len, timeout);
}

/*
 * Sets an interrupt on the specified interface (unless it has been previously set)
 */
int iface_set_interrupt(Interface* iface, uchar*flag){
	if (iface == NULL || flag == NULL) {
		return swe_invalid_arguments;
	}
	if (iface->is_interrupt_set == TRUE) {
		errmsg("Interrupt has already been set for iface %p", iface->interface);
		return swe_iface_error;
	}
	iface->is_interrupt_set = TRUE;
	return iface->set_interrupt(iface->interface, flag);
}

/*
 * High priority interrupt. This interrupt should be only used for high-frequency
 * streaming, such as hydrophones.
 */
int iface_set_priority_interrupt(Interface* iface, void(*handler)(void)) {
	if (iface == NULL || handler == NULL ) {
		errmsg("NULL pointer received");
		return swe_iface_error;
	}
	if (iface->is_interrupt_set == TRUE) {
		errmsg("Interrupt has already been set for iface %p", iface->interface);
		return swe_iface_error;
	}
	if (iface->set_priority_interrupt == NULL) {
		errmsg("function not implemented for this platform");
		return swe_unimplemented;
	}
	TRY_RET(iface->set_priority_interrupt(iface->interface, handler));
	iface->is_interrupt_set = TRUE;
	return swe_ok;
}


/*
 * Closes all opened ifaces
 */
int close_all_interfaces(){
	int i;
	if (interfaces_list != NULL ) {
		for ( i=0 ; interfaces_list[i]!=NULL ; i++ ) {
			interfaces_list[i]->close(interfaces_list[i]);
		}
	}
	return swe_ok;
}


//-----------------------------------------------------------------//
//-------------------- INTERNAL SENSORS ---------------------------//
//-----------------------------------------------------------------//
/*
 * Reads the internal sensors
 */
static Internal_Sensors  internal_sensors = {
		.init = platform_internal_sensors_initialize,
		.read = platform_internal_sensors_read
};


/*
 * Calls the init platform init internal sensor handler
 */

int internal_sensors_initialize(int sensor_index){
	if (internal_sensors.init == NULL ) {
		errmsg("%s unimplemented", __func__);
		return swe_unimplemented;
	}
	return internal_sensors.init(sensor_index);
}

/*
 * Reads the value of the sensor pointed by sensor index and stores
 * its measure in the "measure" pointer.
 */
int internal_sensors_read(int sensor_index, void* buffer, int bufflen){
	if (internal_sensors.read == NULL ) {
		errmsg("%s unimplemented", __func__);
		return swe_unimplemented;
	}
	if (buffer == NULL) {
		errmsg("Buffer is a NULL pointer");
		return swe_invalid_arguments;
	} else if (bufflen < 1) {
		errmsg("Bufflen is less than 1");
		return swe_invalid_arguments;
	}
	return internal_sensors.read(sensor_index, buffer, bufflen);
}


//-----------------------------------------------------------------//
//------------------------ ADC MEASURE  ---------------------------//
//-----------------------------------------------------------------//
/*
 * Reads the built-in ADC (if any)
 */
static ADC_Measure  adc_measure = {
		.read = platform_adc_read,
		.init = platform_adc_initialize

};

/*
 * Reads the value of the sensor pointed by sensor index and stores
 * its measure in the "measure" pointer.
 */
int adc_read(int adc_index, float* measure){
	if (adc_measure.read == NULL ) {
		errmsg("%s unimplemented", __func__);
		return swe_unimplemented;
	}
	if (measure == NULL) {
		errmsg("Measure is a NULL pointer");
		return swe_invalid_arguments;
	}
	return adc_measure.read(adc_index, measure);
}

/*
 * Initializes an ADC
 */
int adc_initialize(int adc_index){
	if (adc_measure.init == NULL ) {
		errmsg("%s unimplemented", __func__);
		return swe_unimplemented;
	}
	return adc_measure.init(adc_index);
}






