/*
 * platform_resources.h
 *
 *  Created on: Sep 17, 2018
 *      Author: enoc
 */

#ifndef RESOURCES_INC_PLATFORM_RESOURCES_H_
#define RESOURCES_INC_PLATFORM_RESOURCES_H_

#include "platform_config.h"

#ifdef GENERIC_LINUX
#include "platforms/linux/linux.h"
#elif defined COSTOF2
#include "costof2.h"
#elif defined PIROS
#include "platforms/piros/piros.h"
#elif defined SENSORBOX
#include "platforms/sensorbox/sensorbox.h"
#elif defined RASPBERRY_PI
#include "platforms/raspberry_pi/raspberry_pi.h"
//--- Add new platform include here ---//
#elif defined EXAMPLE
#include "platforms/example/example.h"

#endif


/*
 * The default handlers header file will fill the handlers that haven't been defined by the platform
 * with NULL pointers
 */
#include "resources/default_handlers.h"




#endif /* RESOURCES_INC_PLATFORM_RESOURCES_H_ */
