/*
 * Wrapper for FFT library
 *
 *  @author: Enoc Martínez
 *  @institution: Universitat Politècnica de Catalunya (UPC)
 *  @contact: enoc.martinez@upc.edu
 */
#include "swe_conf.h"
#if ENABLE_HIGH_FREQUENCY_MODULES

#include "resources/fft.h"
#include <math.h>


#define DEBUG_THIS_MODULE OFF

#if DEBUG_THIS_MODULE
/*
 * Wrap debug message functions
 */
#define DBGM(...) dmsg(__VA_ARGS__) // DEBUG this module
#else
#define DBGM(...)
#endif



WindowFunction* window_hann_create(uint nfft);
int apply_window_function(WindowFunction* window, float64* signal, uint len);
int window_function_free(WindowFunction* window);


/*
 * Declare library-specific wrappers
 */
#if (( FFT_LIBRARY == KISS_FFT))
	int kiss_rfft_setup(FFTparams* params);
	int kiss_rfft_calculate(FFTparams* params, float64* in, float64* out);
	int kiss_rfft_free(FFTparams* params);
#endif


/*
 * Wrapper to initialize a FFT structure. A hann windos is also createds
 *     sampling_rate: input data sampling rage
 *     nfft: FFT length
 *     hann_window: if TRUE a Hann window will be applied to the input signal
 */
FFTparams* rfft_setup(float64 sampling_rate, uint nfft, uchar hann_window){
	FFTparams* fftparams = swe_malloc(sizeof(FFTparams));
	fftparams->sampling_rate = sampling_rate;
	fftparams->n = nfft;
	fftparams->nfreq = nfft/2 + 1;
	fftparams->df = sampling_rate / (float64)nfft;
	fftparams->input = swe_malloc((sizeof(float64)*nfft));
	fftparams->abs_fft_out = swe_malloc(sizeof(float64)*fftparams->nfreq);

	if ( hann_window == TRUE ) {
		// create window
		fftparams->window = window_hann_create(nfft);

	} else {
		fftparams->window = NULL;
	}

//---- KISS FFT ----//
#if	 (( FFT_LIBRARY == KISS_FFT))
	if (kiss_rfft_setup(fftparams) < 0 ) {
		errmsg("Couldn't setup KISS FFT");
		swe_free(fftparams);
		return NULL;
	}
#endif
	return fftparams;
}


/*
 * Wrapper to calculate a real FFT. It returns the module of the complex
 * FFT's output. Output has size nfft/2 +1, where index 0 is the position
 * of 0 Hz and NFFT/2 is the Nyquist frequency.
 *
 * Output buffer should be provided (this function does not allocate memory)
 *
 */
int rfft_calculate(FFTparams* params, float64* in, float64* out){
#if (( FFT_LIBRARY == KISS_FFT))
	return kiss_rfft_calculate(params, in, out);
#endif
	return swe_unimplemented;
}

/*
 * Frees the RFFT structure
 */
int rfft_free(FFTparams* params){
#if (( FFT_LIBRARY == KISS_FFT))
	return kiss_rfft_free(params);
#endif
	swe_free(params->abs_fft_out);
	swe_free(params);
	return swe_ok;
}

//--------------------------------------------------------------//
//---------------------- Window Functions ----------------------//
//--------------------------------------------------------------//

/*
 * Creates a Hann Window function with length nfft. Also calculates the S1, S2 and NENBW constants
 * for normalization purposes
 *
 * if n is not a power of 2 the window is zero-padded until it reaches ntotal
 *
 */
WindowFunction* window_hann_create(uint nfft) {
	uint i;

	WindowFunction* window = swe_malloc(sizeof(WindowFunction));


	window->values = swe_malloc(nfft*sizeof(float64));
	window->n = nfft;
	window->s1 = 0;
	window->s2 = 0;


	// Create the window function
	for ( i=0 ; i<nfft ; i++ ) {
		if (i < nfft ) {
			window->values[i] = 0.5 * (1 - cos(2*PI_NUM* ((float64)i)/((float64)nfft)));
			window->s1 += window->values[i];
			window->s2 += pow(window->values[i], 2);
		}
	}

	window->nenbw = nfft*(window->s2 / pow(window->s1, 2));
	return window;
}


/*
 * Applies a window function on the incoming signal.
 */
int apply_window_function(WindowFunction* window, float64* signal, uint len) {
	int i;
	if ( len != window->n ) {
		errmsg("Window and signal lengths do not match! (%d and %d)", len, window->n);
		return swe_error;
	}

	for ( i=0 ; i<window->n ; i++ ) {
		signal[i] *= window->values[i];
	}
	return swe_ok;
}


/*
 * Frees a window function
 */
int window_function_free(WindowFunction* window){
	swe_free(window->values);
	swe_free(window);
	return swe_ok;
}


//--------------------------------------------------------------//
//------------------------- KISS FFT ---------------------------//
//--------------------------------------------------------------//
#if (( FFT_LIBRARY == KISS_FFT))

/*
 * Setup KISS FFT specific paramters
 */
int kiss_rfft_setup(FFTparams* params){

	params->cfg = kiss_fftr_alloc(params->n, FALSE, NULL, NULL);
	params->complex_out = swe_malloc(sizeof(kiss_fft_cpx)*params->nfreq);
	memset(params->complex_out, 0, sizeof(kiss_fft_cpx)*params->nfreq);
	return swe_ok;
}

/*
 * Calculate real FFT using KISS FFT library
 */
int kiss_rfft_calculate(FFTparams* params, float64* in, float64* out) {
	int i;
	// Normalization coefficient //


	// Apply Window //
	if ( params->window != NULL ) {
		DBGM("Applying window...");
		TRY_RET(apply_window_function(params->window, in, params->n));
	}
	else {
		DBGM("No window");
	}

	// Do the FFT //
	DBGM("Calling FFT... %p %p %p", params->cfg, in, params->complex_out);
	kiss_fftr(params->cfg, in, params->complex_out);

	// Absolute value of the FFT //
	DBGM("Calculating FFT module...", norm_coefficient);
	for (i=0; i<params->nfreq; i++) {
		kiss_fft_cpx cpx = params->complex_out[i];
		// Each bin is the module of the complex output, normalized by N
		out[i] = sqrt( pow(cpx.r, 2) + pow(cpx.i, 2));
	}

	return swe_ok;
}

/*
 * free_kiss_rfft
 */
int kiss_rfft_free(FFTparams* params){
	window_function_free(params->window);
	swe_free(params->complex_out);
	kiss_fftr_free(params->cfg);
	return swe_ok;
}


#endif // ENABLE_HIGH_FREQUENCY_MODULES //
#endif
