/********************************************************************
 *                        Default Handlers
 ********************************************************************
 *
 * Defines all the handlers as NULL if the haven't been previously
 * defined.
 *
 ********************************************************************/

#ifndef DEFAULT_HANDLERS_H_
#define DEFAULT_HANDLERS_H_

#include "swe_conf.h"


//---------------------------------------------------------------------------//
//----------------------- DEFINE GENERIC HANDLERS ---------------------------//
//---------------------------------------------------------------------------//

/* UART Functions */
#ifndef platform_open_uart
#define platform_open_uart NULL
#endif
#ifndef platform_read_uart
#define platform_read_uart NULL
#endif
#ifndef platform_write_uart
#define platform_write_uart NULL
#endif
#ifndef platform_close_uart
#define platform_close_uart NULL
#endif
#ifndef platform_fflush_uart
#define platform_fflush_uart NULL
#endif
#ifndef platform_set_baudrate
#define platform_set_baudrate NULL
#endif
#ifndef platform_set_uart_interrupt
#define platform_set_uart_interrupt NULL
#endif
#ifndef platform_set_uart_priority_interrupt
#define platform_set_uart_priority_interrupt NULL
#endif


/* TCP Functions */
#ifndef platform_open_tcp
#define platform_open_tcp NULL
#endif
#ifndef platform_read_tcp
#define platform_read_tcp NULL
#endif
#ifndef platform_write_tcp
#define platform_write_tcp NULL
#endif
#ifndef platform_close_tcp
#define platform_close_tcp NULL
#endif
#ifndef platform_fflush_tcp
#define platform_fflush_tcp NULL
#endif
#ifndef platform_set_tcp_interrupt
#define platform_set_tcp_interrupt NULL
#endif
#ifndef platform_set_tcp_priority_interrupt
#define platform_set_tcp_priority_interrupt NULL
#endif

/* UDP Functions */
#ifndef platform_open_udp
#define platform_open_udp NULL
#endif
#ifndef platform_read_udp
#define platform_read_udp NULL
#endif
#ifndef platform_write_udp
#define platform_write_udp NULL
#endif
#ifndef platform_close_udp
#define platform_close_udp NULL
#endif
#ifndef platform_fflush_udp
#define platform_fflush_udp NULL
#endif
#ifndef platform_set_udp_interrupt
#define platform_set_udp_interrupt NULL
#endif
#ifndef platform_set_udp_destination
#define platform_set_udp_destination NULL
#endif
#ifndef platform_set_udp_priority_interrupt
#define platform_set_udp_priority_interrupt NULL
#endif

/* FILESYSTEM Functions */
#ifndef platform_fopen
#define platform_fopen NULL
#endif
#ifndef platform_fclose
#define platform_fclose NULL
#endif
#ifndef platform_fwrite
#define platform_fwrite NULL
#endif
#ifndef platform_fread
#define platform_fread NULL
#endif
#ifndef platform_create_dir
#define platform_create_dir NULL
#endif
#ifndef platform_fremove
#define platform_fremove NULL
#endif
#ifndef platform_file_size
#define platform_file_size NULL
#endif
#ifndef platform_fmove
#define platform_fmove NULL
#endif
#ifndef platform_getc
#define platform_getc NULL
#endif
#ifndef platform_eof
#define platform_eof NULL
#endif
#ifndef platform_list_dir
#define platform_list_dir NULL
#endif
#ifndef platform_set_pointer
#define platform_set_pointer NULL
#endif
#ifndef platform_file_exists
#define platform_file_exists NULL
#endif



/* TIMING Functions */
#ifndef platform_delayms
#define platform_delayms NULL
#endif
#ifndef platform_get_epoch_time
#define platform_get_epoch_time NULL
#endif
#ifndef platform_set_timer
#define platform_set_timer NULL
#endif
#ifndef platform_start_timer
#define platform_start_timer NULL
#endif
#ifndef platform_stop_timer
#define platform_stop_timer NULL
#endif
#ifndef platform_set_timeout
#define platform_set_timeout NULL
#endif
#ifndef platform_disable_timeout
#define platform_disable_timeout NULL
#endif
#ifndef platform_get_timezone
#define platform_get_timezone NULL
#endif
#ifndef platform_mktime
#define platform_mktime NULL
#endif
#ifndef platform_localtime
#define platform_localtime NULL
#endif


/* Platform Functions */
#ifndef platform_init
#define platform_init NULL
#endif
#ifndef platform_exit
#define platform_exit NULL
#endif
#ifndef platform_standby
#define platform_standby NULL
#endif
#ifndef platform_deep_sleep
#define platform_deep_sleep NULL
#endif
#ifndef platform_get_coordinates
#define platform_get_coordinates NULL
#endif
#ifndef platform_get_altitude
#define platform_get_altitude NULL
#endif
#ifndef platform_system_monitor
#define platform_system_monitor NULL
#endif
#ifndef platform_vprintf
#define platform_vprintf NULL
#endif
#ifndef platform_vfprintf
#define platform_vfprintf NULL
#endif
#ifndef platform_fflush
#define platform_fflush NULL
#endif
#ifndef platform_notify
#define platform_notify NULL
#endif

/* Memory Management */
#ifndef platform_malloc
#define platform_malloc malloc
#endif
#ifndef platform_calloc
#define platform_calloc calloc
#endif
#ifndef platform_realloc
#define platform_realloc realloc
#endif
#ifndef platform_free
#define platform_free free
#endif


/* Power Management Functions */
#ifndef platform_power_initialize
#define platform_power_initialize NULL
#endif
#ifndef platform_power_on_sensor
#define platform_power_on_sensor NULL
#endif
#ifndef platform_power_off_sensor
#define platform_power_off_sensor NULL
#endif

/* ADC Measure */
#ifndef platform_adc_read
#define platform_adc_initialize NULL
#define platform_adc_read NULL
#endif

/* Internal Sensors */
#ifndef platform_internal_sensors_initialize
#define platform_internal_sensors_initialize NULL
#endif
#ifndef platform_internal_sensors_read
#define platform_internal_sensors_read NULL
#endif



#endif /* DEFAULT_HANDLERS_H_ */
