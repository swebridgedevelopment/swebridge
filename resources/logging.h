/*
 * Header file for logging system
 *
 *  @author: Enoc Martínez
 *  @institution: Universitat Politècnica de Catalunya (UPC)
 *  @contact: enoc.martinez@upc.edu
 */
#ifndef RESOURCES_LOGGING_H_
#define RESOURCES_LOGGING_H_

#include "swe_conf.h"

//-----------------------------------------------------------------//
//------------------------ CONSOLE MESSAGES -----------------------//
//-----------------------------------------------------------------//
// Log Levels //
typedef enum{
	LOG_UNKNWON=0,
	LOG_DEBUG,
	LOG_INFO,
	LOG_WARN,
	LOG_ERROR
}LogLevelList;


typedef enum{
	RST=0, NRM, RED, YEL,  WHT, GRY, GRN, BLU, MAG, CYN,
}colour_list;

#define DEFAULT_COLOUR NRM

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"
#define KRST "\033[0m"

extern int system_vprintf(const char* format, va_list args); // system_vprintf declared in resources.h
extern int system_printf(const char* format, ...);

//---- Debug messages ----//
int dmsg(const char *format, ...);
int dmsgn(const char *format, ...);
int cdmsg(uchar colour, const char *format, ...);
int cdmsgn(uchar colour, const char *format, ...);
//---- Info messages ----//
int imsg(const char *format, ...);
int imsgn(const char *format, ...);
int cimsg(uchar colour, const char *format, ...);
int cimsgn(uchar colour, const char *format, ...);
//---- Errors & Warnings ----//
int _warnmsg(const char* file, const int line, const char *format, ...);
int _errmsg(const char* file, const int line, const char *format, ...);
//---- Formatted messages ----//
int fmt_dmsg(uchar color, const char* format, ...);

//---- Titles ----//
int msg_title(uchar colour, char* format, ...);
int msg_sub_title(uchar colour, char* format, ...);

void print_binary_chunk(void* buff, int len, int columns);
int print_binary_array(void *invalue, uint size);
int set_log_level(int new_log_level);

/*
 * error message macro
 */
#define errmsg(...) ({ \
		_errmsg(__FILE__, __LINE__, __VA_ARGS__);\
})

/*
 * warning message macro
 */
#define warnmsg(...) ({ \
		_warnmsg(__FILE__, __LINE__,__VA_ARGS__);\
})

#endif /* RESOURCES_LOGGING_H_ */
