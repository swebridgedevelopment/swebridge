/*
 * This header file contatins the configuration for FFTs using different libraries
 *
 *  @author: Enoc Martínez
 *  @institution: Universitat Politècnica de Catalunya (UPC)
 *  @contact: enoc.martinez@upc.edu
 */
#ifndef RESOURCES_FFT_H_
#define RESOURCES_FFT_H_

#include "swe_conf.h"
#if ENABLE_HIGH_FREQUENCY_MODULES

/*
 * Choose a library to use from the following: KISS_FFT, FFTW
 */
#define FFT_LIBRARY KISS_FFT

#ifndef PI_NUM
#define PI_NUM 3.1415926535897932384626
#endif

/*
 * Include the library header file
 */
#if (( FFT_LIBRARY == KISS_FFT))
# include "resources/kiss_fft/kiss_fft.h"
# include "resources/kiss_fft/kiss_fftr.h"
#endif

typedef struct {
	uint n;
	float64* values;

	float64 s1; // Sum window values
	float64 s2; // sum squared window values
	float64 nenbw; // Normalized equivalent noise bandwidth

}WindowFunction;

/*
 * FFT params contains all the information and parameters required to perform a FFT. It also
 * contains the structures and the memory allocation required
 */
typedef struct{
	uint n; // length of the FFT input data
	uint nfreq; // length of the FFT output data (always N/2 +1)
	float64 sampling_rate;
	float64 df; // Frequency increment
	float64* abs_fft_out;   // array with size nfreq containing the FFT module

	float64* input; // array to store the input data for windowing and zero-padding
	WindowFunction* window;

#if (( FFT_LIBRARY == KISS_FFT))
	// KISS FFT parameters /
	kiss_fft_cpx* complex_out; // Complex output buffer
	kiss_fftr_cfg cfg; // FFT configuration;
#endif

}FFTparams;


FFTparams* rfft_setup(float64 sampling_rate, uint nfft, uchar hann_window);
int rfft_calculate(FFTparams* params, float64* in, float64* out);
int rfft_free(FFTparams* params);

float64* generate_hann_window(uint n);
float64* generate_hann_window(uint n);



#endif // ENABLE_HIGH_FREQUENCY_MODULES //
#endif /* RESOURCES_FFT_H_ */
