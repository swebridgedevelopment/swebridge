/*
 *  Simple CSV library implementation.
 *
 *  @author: Enoc Martínez
 *  @institution: Universitat Politècnica de Catalunya (UPC)
 *  @contact: enoc.martinez@upc.edu
 */

#include "csv.h"

int csv_process_header(SchedulerData *data, CsvFile* csv, uchar write);


CsvFile* csv_create(const char* filename, const char* delimiter, const char* endline, int decimal_precision,
		SchedulerData* data, uchar georeference, uchar depth, uchar timerange, uchar msecs, uchar open_close){
	CsvFile* csv;
	swe_file* file;
	uchar exists;
	CHECK_NULL(filename, NULL);
	CHECK_NULL(data, NULL);
	exists = swe_file_exists(filename);
	if (exists) {
		warnmsg("File %s already exists!", filename);
	}

	file = CATCH_NULL(swe_fopen(filename, swe_append_file));
	csv = swe_malloc(sizeof(CsvFile));
	csv->file = file;
	csv->filename = set_string(filename);
	strcpy(csv->delimiter, delimiter);
	strcpy(csv->endline, endline);
	// Counters
	csv->total_time = -1;
	csv->init_time = -1;
	csv->count = 0;
	csv->ncolumns = 0;
	csv->decimal_precision = decimal_precision;

	// Set flags
	csv->georeference = georeference;
	csv->depth = depth;
	csv->timerange = timerange;
	csv->open_close = open_close;
	csv->msecs = msecs;

	if (exists == FALSE) { // write header only if file does not exists
		TRY_RET_NULL(csv_process_header(data, csv, TRUE));
	} else {
		TRY_RET_NULL(csv_process_header(data, csv, FALSE));
	}

	if  ( csv->open_close ) {
		TRY_RET_NULL(swe_fclose(csv->file));
		csv->file = NULL;
	}
	return csv;
}


/*
 * Processes the header of the CSV file based on SchedulerData. If write is TRUE it is written to the file
 */
int csv_process_header(SchedulerData *data, CsvFile* csv, uchar write){
	int i;
	char temp[128 + data->count*64]; // assign tem
	char* point = temp;
	memset(temp, 0, 128 + data->count*64);

	if ( csv->timerange ) {
		point = fast_strcat(point, "timerange");
	}
	else {
		point = fast_strcat(point, "timestamp");
	}
	csv->ncolumns = 1;

	if ( csv->georeference ) {
		point = fast_strcat(point, csv->delimiter);
		point = fast_strcat(point, "latitude");
		point = fast_strcat(point, csv->delimiter);
		point = fast_strcat(point, "longitude");

		csv->ncolumns += 2;
	}
	if ( csv->depth ) {
		point = fast_strcat(point, csv->delimiter);
		point = fast_strcat(point, "depth");
		csv->ncolumns += 1;
	}

	for(i=0; i < data->count; i++) 	{
		SWE_Data* field = data->fields[i];
		point = fast_strcat(point, csv->delimiter);
		point = fast_strcat(point, field->name);
		csv->ncolumns += 1;
	}
	fast_strcat(point, csv->endline);
	if (write) {
		TRY_RET(swe_fputs(temp, csv->file));
	}
	return swe_ok;
}


/*
 * Appends a measurement
 */
int csv_append_measurement(SchedulerData* data, CsvFile* csv){
	char buffer[64*csv->ncolumns]; // 64 bytes per column should be more than enough

	memset(buffer, 0, 64*csv->ncolumns);
	if (csv->open_close) {
		csv->file = swe_fopen(csv->filename, swe_append_file);
	}

	TRY_RET(data_string_from_scheduler_data(data, buffer, csv->delimiter, csv->endline,
			csv->georeference, csv->depth, csv->decimal_precision, csv->msecs, csv->timerange));


	TRY_RET(swe_fputs(buffer, csv->file));
	csv->count++;

	// update the time
	if ( csv->init_time < 0 ){
		// Set the initial time
		csv->init_time = data->init_timestamp;
	}
	if ( csv->timerange && (data->end_timestamp > 0)) {
		// If we are using timerange calculate total time with end_timestamp
		csv->total_time = data->end_timestamp - csv->init_time;
	}
	else {
		// Otherwise calculate the total time with init_timestamp since end_timestamp is not used
		csv->total_time = data->end_timestamp - csv->init_time;
	}

	if (csv->open_close) {
		swe_fclose(csv->file);
		csv->file = NULL;
	}

	return swe_ok;
}


/*
 * Closes a CSV file and frees the memory in csv structure
 */
int csv_finish(CsvFile* csv){
	if ( csv->file != NULL ) {
		swe_fclose(csv->file);
		csv->file = NULL;
	}
	swe_free(csv->filename);
	swe_free(csv);
	return swe_ok;
}



