#include "swe_conf.h"
#include "common/swe_utils.h"
#include "wave.h"
#include "resources/resources.h"


#define DEBUG_THIS_MODULE FALSE

#if DEBUG_THIS_MODULE
/*
 * Wrap debug message functions
 */
#define DBGM(...) dmsg(__VA_ARGS__) // DEBUG this module
#else
#define DBGM(...)
#endif




int wave_read_uint32(swe_file* wavfile, uint *result);
int wave_read_uint16(swe_file* wavfile, uint *result);
int wave_read_int32(swe_file* wavfile, signed_int *result);
int wave_read_int16(swe_file* wavfile, signed_short *result);
int wave_read_text(swe_file* wavfile, uchar* where, int size);
int wave_read_int24(swe_file* wavfile, signed_int *result);


/*
 * Open a WAV file and fills the header
 */
WavReadFile* wave_open(const char* filename, float64 init_timestamp) {
	WavHeader* header = NULL;
	swe_file* file = NULL;
	WavReadFile* wavfile = NULL;
	uint bits;

	if (filename == NULL ) {
		errmsg("WAV filename is NULL");
		return NULL;
	}

	DBGM("Opening WAV File %s", filename);

	wavfile = swe_malloc(sizeof(WavReadFile));
	wavfile->file = swe_fopen(filename, swe_read_file);
	wavfile->init_timestamp = init_timestamp;
	if (wavfile->file == NULL ) {
		errmsg("Couldn't open file %s", filename);
		return NULL;
	}

	strcpy(wavfile->filename, filename);
	file = wavfile->file;
	header = &wavfile->header;
	memset(header, 0, sizeof(WavHeader));

	// RIFF Chunk Descriptor//
	TRY_RET_NULL(wave_read_text(file, header->riff, 4));
	TRY_RET_NULL(wave_read_uint32(file, &header->riff_size));
	TRY_RET_NULL(wave_read_text(file, header->wave, 4));
	if ( memcmp(header->wave, "WAVE", 4)) {
		errmsg("WAVE file type header not found!");
		errmsg("Probably this WAVE file does not start with a WAVE chunk, still unimplemented");
		return NULL;
	}

	// Loop until we find "fmt "
	TRY_RET_NULL(wave_read_text(file, header->fmt_chunk_marker, 4));
	while (  memcmp(header->fmt_chunk_marker, "fmt ", 4 ) ){
		header->fmt_chunk_marker[0] = header->fmt_chunk_marker[1];
		header->fmt_chunk_marker[1] = header->fmt_chunk_marker[2];
		header->fmt_chunk_marker[2] = header->fmt_chunk_marker[3];
		TRY_RET_NULL(wave_read_text(file, &header->fmt_chunk_marker[3], 1));
	}

	// fmt Chunk //
	TRY_RET_NULL(wave_read_uint32(file, &header->length_of_fmt));
	TRY_RET_NULL(wave_read_uint16(file, &header->format_type));
	TRY_RET_NULL(wave_read_uint16(file, &header->channels));
	TRY_RET_NULL(wave_read_uint32(file, &header->sample_rate));
	TRY_RET_NULL(wave_read_uint32(file, &header->byte_rate));
	TRY_RET_NULL(wave_read_uint16(file, &header->block_align));
	TRY_RET_NULL(wave_read_uint16(file, &bits));
	header->sample_length = bits / 8;
	TRY_RET_NULL(wave_read_text(file, header->data_chunk_header, 4));
	TRY_RET_NULL(wave_read_uint32(file, &header->data_size));

	// calculate number of samples
	wavfile->samples = (header->data_size) / (header->channels * header->sample_length);
	wavfile->index = 0;

	// timestep
	wavfile->tstep = 1/((float64)header->sample_rate);
	return wavfile;
}


/*
 * Close WAVE file
 */
int wave_close(WavReadFile* wavfile) {
	TRY_RET(swe_fclose(wavfile->file));
	wavfile->file = NULL;
	return swe_ok;
}


/*
 * Reads an 32-bit unsigned integer from the wav file and stores it in result
 */
int wave_read_uint32(swe_file* wavfile, uint *result){
	uchar buffer4[4];
	TRY_RET(swe_fread(buffer4, 4, wavfile));
	*result = buffer4[0] |
			(buffer4[1] << 8) |
			(buffer4[2] << 16) |
			(buffer4[3] << 24);
	return swe_ok;
}


/*
 * Reads an 16-bit unsigned integer from the wav file and stores it in result
 */
int wave_read_uint16(swe_file* wavfile, uint *result){
	uchar buffer2[2];
	TRY_RET(swe_fread(buffer2, 2, wavfile));
	*result = 0;
	*result = buffer2[0] |
			(buffer2[1] << 8);
	return swe_ok;
}


/*
 * Reads an 16-bit unsigned integer from the wav file and stores it in result
 */
int wave_read_int16(swe_file* wavfile, signed_short *result){
	uchar buffer2[2];
	TRY_RET(swe_fread(buffer2, 2, wavfile));
	*result = 0;
	*result = buffer2[0] |
			(buffer2[1] << 8);
	return swe_ok;
}


/*
 * Reads a 24-bit signed integer from the wav file and stores it as a 32-bit int
 */
int wave_read_int24(swe_file* wavfile, signed_int *result){
	uchar buffer[4];
	memset(buffer, 0, 4);
	TRY_RET(swe_fread(buffer, 3, wavfile));

	// Check if negative
	if ( 0b10000000 & buffer[2] ) {
		// The value is negative and it's in 24-bit 2's complement
		// First negate all bits
		buffer[0] = ~buffer[0];
		buffer[1] = ~buffer[1];
		buffer[2] = ~buffer[2];
		// shift bits
		*result = buffer[0] |
				(buffer[1] << 8) |
				(buffer[2] << 16) |
				(buffer[3] << 24);
		// the result is negative minus one
		*result = -*result - 1;
	} else {
		// Otherwise simply shift bits
		*result = buffer[0] |
				(buffer[1] << 8) |
				(buffer[2] << 16) |
				(buffer[3] << 24);
	}
	return swe_ok;
}


/*
 * Reads an 32-bit unsigned integer from the wav file and stores it in result
 */
int wave_read_int32(swe_file* wavfile, signed_int *result){
	uchar buffer4[4];
	TRY_RET(swe_fread(buffer4, 4, wavfile));
	*result = buffer4[0] |
			(buffer4[1] << 8) |
			(buffer4[2] << 16) |
			(buffer4[3] << 24);
	return swe_ok;
}




/*
 * Read "size" bytes of text and sotre them to "where"
 */
int wave_read_text(swe_file* wavfile, uchar* where, int size){
	TRY_RET(swe_fread(where, size, wavfile));
	return swe_ok;
}



/*
 * Reads nsamples from wavfile and store them to outbuffer (same format as in wav file).
 * If timestamp is not NULL the timestamp of the first sample is stored
 * Returns the number of bytes read.
 */
int wave_read_samples(WavReadFile* wavfile, uchar* outbuffer, int nsamples, float64* timestamp){
	uint i, init_index = 0;

	CHECK_NULL(wavfile, swe_invalid_arguments);
	WavHeader* header = &wavfile->header;
	init_index = wavfile->index;
	// 16 bit
	if (header->sample_length == 2 ) {
		for ( i=0 ; i<nsamples ; i++ ) {
			// Set the buffer position as a signed short
			signed_short* where = (signed_short*)(&outbuffer[i*2]);
			TRY_RET(wave_read_int16(wavfile->file, where));
			wavfile->index++;
		}
	}

	// 24 bit, read 3 bytes but return it as a 32-bit integer
	else if ( header->sample_length == 3 ) {
		for ( i=0 ; i<nsamples ; i++ ) {
			// Set the buffer position as a signed short
			signed_int* where = (signed_int*)(&outbuffer[i*4]);
			TRY_RET(wave_read_int24(wavfile->file, where));
			wavfile->index++;
		}
	}

	// 32 bit
	else if (header->sample_length == 4 ) {
		for ( i=0 ; i<nsamples ; i++ ) {
			signed_int* where = (signed_int*)(&outbuffer[i*4]);
			TRY_RET(wave_read_int32(wavfile->file, where));
			wavfile->index++;
		}
	}
	else {
		errmsg("Not implemented %d bytes per sample", header->sample_length);
		return swe_unimplemented;
	}

	if ( timestamp != NULL ) {
		*timestamp = wavfile->init_timestamp + ((float64)init_index)*wavfile->tstep;
	}

	return nsamples * header->sample_length;
}


/*
 * Shows info about a WAVE file
 */
int wave_show_info(WavReadFile* f){
	WavHeader* header = &f->header;
	char datetime[256] = "";
	ascii_datetime(datetime, f->init_timestamp, FALSE);
	imsg("Showing WAV file info: %s", f->filename);
	imsg("    Total Size: %u", header->riff_size);
	imsg("    Length of FMT: %u",   header->length_of_fmt);
	if (header->format_type == 1) {
		imsg("    Format Type: PCM (value %d)",   header->format_type);
	}
	else {
		warnmsg("    Format Type: unknown (value %d)",   header->format_type);
	}
	imsg("    Channels: %u",   header->channels);
	imsg("    Sample Rate %u Hz",   header->sample_rate);
	imsg("    Byte Rate: %u",   header->byte_rate);
	imsg("    Block align: %u",   header->block_align);
	imsg("    Bytes per sample: %u",   header->sample_length);
	imsg("    Data Size: %u bytes",   header->data_size);
	imsg("    Timestamp %s",  datetime);
	return swe_ok;
}



