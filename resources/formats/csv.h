/*
 *  Simple CSV library implementation.
 *
 *  @author: Enoc Martínez
 *  @institution: Universitat Politècnica de Catalunya (UPC)
 *  @contact: enoc.martinez@upc.edu
 */

#include "swe_conf.h"
#include "common/swe_utils.h"
#include "core/simple_process.h"

#define CSV_DEFAULT_Z_AXIS_NAME "depth"

/*
 * Structure to host all the info from a CSV file
 */
typedef struct {
	char* filename; // name of the document in the filesystem
	swe_file* file; // pointer to the file

	//---- Format ----//
	char delimiter[4];      // Character used to delimit columns, usually ',', ';', or '\t'
	char endline[4];      // Character used to end columns usually '\n' or '\r\n'
	uchar decimal_precision; // decimal precision (only used when converting from binary to ascii)

	//---- Counters ----//
	uint ncolumns;           // number of columns
	uint count; // number of measurements stored in the file
	float64 total_time; // total time included in the file (timestamp_last_meas - timestamp_first_meas)
	float64 init_time; // total time included in the file (timestamp_last_meas - timestamp_first_meas)

	//---- Flags ----//
	uchar timerange; // if  TRUE instead the timestamp column will be <start time>/<end time>
	uchar open_close;    // if TRUE the file will be opened and closed every time a new measure is appended
	uchar georeference;  // if TRUE latitude and longitude will be added in each measurement
	uchar depth;      // if TRUE the depth / altitude will be added
	uchar msecs;         // if TRUE timestamps are rounded to the millisecond (default round to second)
}CsvFile;



CsvFile* csv_create(const char* filename, const char* delimiter, const char* endline, int decimal_precision,
		SchedulerData* data, uchar georeference, uchar depth, uchar timerange, uchar msecs, uchar open_close);
int csv_append_measurement(SchedulerData* data, CsvFile* csv);
int csv_finish(CsvFile* csv);


