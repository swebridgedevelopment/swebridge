#ifndef RESOURCES_FORMATS_WAVE_H_
#define RESOURCES_FORMATS_WAVE_H_

#include "swe_conf.h"

// WAVE file header format
typedef struct {
	uchar riff[4];						// RIFF string
	uint riff_size;				// overall size of file in bytes

	uchar wave[4];						// WAVE string
	uchar fmt_chunk_marker[4];			// fmt string with trailing null char
	uint length_of_fmt;					// length of the format data
	uint format_type;					// format type. 1-PCM, 3- IEEE float, 6 - 8bit A law, 7 - 8bit mu law
	uint channels;						// no.of channels
	uint sample_rate;					// sampling rate (blocks per second)
	uint byte_rate;						// SampleRate * NumChannels * BitsPerSample/8
	uint block_align;					// NumChannels * BitsPerSample/8
	uint sample_length;					// bytes per sample, 1 - 8bits, 2- 16 bits etc
	uchar data_chunk_header[4];			// DATA string or FLLR string
	uint data_size;						// NumSamples * NumChannels * BitsPerSample/8 - size of the next chunk that will be read
}WavHeader;




typedef struct {
	WavHeader header;
	swe_file *file;
	char filename[512];
	float64 init_timestamp; // epoch timestamp (first sample)
	float64 tstep; // timestep is 1 / fs
	uint samples; // total number of samples
	uint index; // current sample
}WavReadFile;


WavReadFile* wave_open(const char* filename, float64 init_timestamp);
int wave_close(WavReadFile* wavfile);
int wave_read_samples(WavReadFile* wavfile, uchar* outbuffer, int nsamples, float64* timestamp);
int wave_show_info(WavReadFile* f);




#endif /* RESOURCES_FORMATS_WAVE_H_ */
