/*
 * hydrophone.h
 *
 *  Created on: Sep 12, 2020
 *      Author: enoc
 */

#ifndef RESOURCES_HYDROPHONE_H_
#define RESOURCES_HYDROPHONE_H_

#if ENABLE_HIGH_FREQUENCY_MODULES

#include "swe_conf.h"
#include "common/sensorml.h"

//--------------------------------------------------------------//
//------------------- Hydrophone Metadata Definitions ----------//
//--------------------------------------------------------------//
/*
 * This lists include all possible terms to identify hydrophone-related parameters such as
 * preamplifier gain, ADC reference voltage, sampling rate, etc.
 */
#define HYDROPHONE_PREAMPLIFIER_GAIN {\
	"http://mmisw.org/ont/ioos/passive_acoustic_metadata/preamplifier_gain", \
	"preamplifier_gain",\
	"gain",\
	NULL}

#define HYDROPHONE_REFERENCE_VOLTAGE { \
	"adc_reference_voltage",\
	"reference_voltage", \
	"vref",\
	"ref_voltage",\
	NULL}

#define HYDROPHONE_SAMPLING_RATE { \
	"http://mmisw.org/ont/ioos/passive_acoustic_metadata/sample_rate", \
	"sampling_rate", \
	"sample_rate", \
	NULL}

#define HYDROPHONE_SENSITIVITY { \
	"http://mmisw.org/ont/ioos/passive_acoustic_metadata/hydrophone_sensitivity", \
	"hydrophone_sensitivity", \
	"sensitivity", \
	NULL}

/*
 * Structure containing the typical acquisition parameters of a hydrophone
 */
typedef struct{
	float64 sampling_rate;  // hydrophone sampling rate
	float64 sensitivity;    // sensitivity in dB re 1V/µPa
	float64 amplifier_gain; // amplifier gain (if no amplifier set to 0)


	uchar adc_bits;       // ADC number of bits
	uchar adc_bipolar;    // TRUE if the ADC has a +/- reference
	float64 adc_vref;       // Reference voltage of the ADC

}HydrophoneParameters;

int get_hydrophone_parameters(PhysicalSystem* system, uint sample_size, HydrophoneParameters* params);
int  calculate_conversion_coefficient(HydrophoneParameters* params, float64* conversion);

#endif /* ENABLE_HIGH_FREQUENCY_MODULES */
#endif /* RESOURCES_HYDROPHONE_H_ */
