
#include "swe_conf.h"
#include "common/swe_utils.h"
#include "resources/hydrophone.h"
#include <math.h>

#if ENABLE_HIGH_FREQUENCY_MODULES


/*
 * Gets the hydrophone parameters from the PhyicalSystem parameters
 *
 *
 * sample size: size in bytes of each hydrophone sample
 */
int get_hydrophone_parameters(PhysicalSystem* system, uint sample_size, HydrophoneParameters* params) {
	const char* preamplifier_defs[] = HYDROPHONE_PREAMPLIFIER_GAIN;
	const char* vref_defs[] = HYDROPHONE_REFERENCE_VOLTAGE;
	const char* srate_defs[] = HYDROPHONE_SAMPLING_RATE;
	const char* sens_defs[] = HYDROPHONE_SENSITIVITY;
	Capability* temp_param = NULL;

	dmsg("Getting preamplifier gain...");
	if ((temp_param = get_parameter_by_definition(system->parameters, preamplifier_defs)) == NULL) {
		temp_param = TRY_NULL(get_capability_by_definition(system->capabilities, preamplifier_defs));
	}
	TRY_RET(field_quantity_value_get(temp_param->field, &params->amplifier_gain));

	dmsg("Getting ADC Vref...");
	if ((temp_param = get_parameter_by_definition(system->parameters, vref_defs)) == NULL) {
		temp_param = TRY_NULL(get_capability_by_definition(system->capabilities, vref_defs));
	}
	TRY_RET(field_quantity_value_get(temp_param->field, &params->adc_vref));


	dmsg("Getting sampling rate...");
	if ((temp_param = get_parameter_by_definition(system->parameters, srate_defs)) == NULL) {
		temp_param = TRY_NULL(get_capability_by_definition(system->capabilities, srate_defs));
	}
	TRY_RET(field_quantity_value_get(temp_param->field, &params->sampling_rate));

	dmsg("Getting Sensitivity ...");
	if ((temp_param = get_parameter_by_definition(system->parameters, sens_defs)) == NULL) {
		temp_param = TRY_NULL(get_capability_by_definition(system->capabilities, sens_defs));
	}
	TRY_RET(field_quantity_value_get(temp_param->field, &params->sensitivity));

	params->adc_bits = 8*sample_size;
	params->adc_bipolar = TRUE; // assume bipolar ADC (+/- Vref)

	imsg("Hydrophone Sensitivity: %f dB re 1 V/µPa", params->sensitivity);
	imsg("Hydrophone Gain: %f dB", params->amplifier_gain);
	imsg("Hydrophone ADC Bits: %d", params->adc_bits);
	imsg("Hydrophone ADC Vref: %f V", params->adc_vref);
	return swe_ok;
}


/*
 * Calculates the conversion coefficient (from counts to uPa) for a hydrophone.If it can
 * be calculated swe_ok is returned and the coefficient is stored at "conversion", otherwise
 * an error is returned.
 *
 * sensitivity: hydrophone sensitivity in dB re 1 uPa
 * gain: amplifier gain in dB
 * adc_bits: number of bits in the ADC
 * adc_ref: Reference voltage in the ADC
 * adc_bipolar: TRUE if the adc is bipolar (+/- Vref)
 */
int  calculate_conversion_coefficient(HydrophoneParameters* params, float64* conversion) {

	float64 sensitivity_lin;
	float64 gain_lin;
	float64 adc_conversion; // ADC conversion contant fro counts

	CHECK_NULL(params, swe_invalid_arguments);

	// Check arguments //
	if ((params->sensitivity > -20 ) || (params->amplifier_gain < 0)  || (params->adc_bits < 8) || (params->adc_vref < 0)){
		return swe_invalid_arguments;
	}

	/*
	 *  Calculate conversion constant (K) from counts to uPa:
	 *
	 * y(counts) = x(uPa) * sens(V/uPa) * gain(V/V) * ADC_conversion(Counts/V)
	 * K = 1 / (sens(V/uPa) * gain(V/V) * ADC_conversion(Counts/V))
	 * x(uPa) = K * y(uPa)
	 *
	 */

	sensitivity_lin = pow(10, params->sensitivity/20); // Convert from dB to lineal
	if (params->amplifier_gain > 0.000 ) {
		gain_lin = pow(10, params->amplifier_gain/20); // Convert from dB to lineal
	}
	else {
		gain_lin = 1;
	}


	// ADC Conversion
	if (params->adc_bipolar) {
		// If ADC is bipolar first bit is the sign
		adc_conversion = (pow(2, params->adc_bits) - 1) / (2*params->adc_vref);
	} else {
		adc_conversion = (pow(2, params->adc_bits) - 1) / params->adc_vref;
	}

	// Calculate conversion
	*conversion = 1 / (sensitivity_lin * gain_lin * adc_conversion);

	return swe_ok;
}

#endif /* ENABLE_HIGH_FREQUENCY_MODULES */
